# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to manage the serial connection
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
import codecs
import sys
import serial
import types
from threading import Lock, Thread
from datetime import datetime, timedelta
from serial.tools import hexlify_codec
from serial.tools.list_ports import comports
from apscheduler.schedulers.background import BackgroundScheduler

from .mac_frame import MacFrameControl, MacAckStatus, MacAckStatusDisplay
from .mac_frame import check_crc, compute_crc
from logger import get_logger

codecs.register(lambda c: hexlify_codec.getregentry() if c == 'hexlify' else None)


def ask_for_port():
    """\
    Show a list of ports and ask the user for a choice. To make selection
    easier on systems with long device names, also allow the input of an
    index.
    """
    sys.stderr.write('\n--- Available ports:\n')
    ports = []
    for n, (port, desc, hwid) in enumerate(sorted(comports()), 1):
        sys.stderr.write('--- {:2}: {:20} {!r}\n'.format(n, port, desc))
        ports.append(port)
    while True:
        port = input('--- Enter port index or full name: ')
        try:
            index = int(port) - 1
            if not 0 <= index < len(ports):
                sys.stderr.write('--- Invalid index!\n')
                continue
        except ValueError:
            pass
        else:
            port = ports[index]
        return port


class SerialDriver(object):
    """\
    Terminal application. Copy data from serial port to console and vice versa.
    Handle special keys from the console to show menu etc.
    """

    def __init__(self, serial_instance, queue_mac_process):
        self._serial_instance = serial_instance
        self._input_encoding = 'UTF-8'
        self._output_encoding = 'UTF-8'
        # Thread
        self._reader_alive = None
        self._receiver_thread = None
        self._rx_decoder = None
        self._tx_decoder = None
        self._tx_encoder = None
        # Read/Write management
        self._local_mac_sequence_number = 0
        self._external_mac_sequence_number = -1
        self._default_starter_frame = 0xFE
        self._queue = queue_mac_process
        # Send command/data management
        self._mutex = Lock()
        self._mutex_no_multiple_send = Lock()
        self._mac_scheduler = BackgroundScheduler()
        self._dict_mac_frame_in_progress = []

    @property
    def serial_instance(self):
        return self._serial_instance

    @serial_instance.setter
    def serial_instance(self, serial_instance):
        self._serial_instance = serial_instance

    def _start_reader(self):
        """Start reader thread"""
        self._reader_alive = True
        self._receiver_thread = Thread(target=self.reader, name='mac_rx', args=())
        self._receiver_thread.daemon = True
        self._receiver_thread.start()

    def _stop_reader(self):
        """Stop reader thread only, wait for clean exit of thread"""
        self._reader_alive = False

    def start(self):
        """start worker thread"""
        self._start_reader()
        # Start the scheduler
        self._mac_scheduler.start()

    def stop(self):
        """set flag to stop worker threads"""
        self._stop_reader()
        # remove jobs and stop scheduler
        self._mac_scheduler.remove_all_jobs()
        self._mac_scheduler.shutdown()

    def join(self):
        """wait for worker thread to terminate"""
        self._receiver_thread.join()

    def close(self):
        self._serial_instance.close()

    def reader(self):
        """loop and copy serial->console"""
        try:
            while self._reader_alive:

                # Read the start of frame
                start_of_frame = self._serial_instance.read(1)
                if len(start_of_frame) == 1:
                    # Start of frame detected
                    if start_of_frame[0] == self._default_starter_frame:
                        # Read length
                        length = self._serial_instance.read(1)
                        if len(length) == 1:
                            if length[0] > 3:
                                # Acquire mutex => we received one data; we need to treat it before sending other data
                                self._mutex.acquire()
                                # Read data from serial
                                data = self._serial_instance.read(length[0] - 1)
                                if len(data) == (length[0]-1):

                                    # We sent a command we are waiting for an acknowledgment
                                    # Check if this is an acknowledgment
                                    result = self._parse_ack_frame(data)
                                    # Report to applicative
                                    if result["is_ack"]:
                                        # Check if the ack corresponding to a command/data frames sent
                                        if self._mac_scheduler.get_job(str(result["mac_sequence_number"])) is not None:
                                            # Remove process of sending the command/data frame
                                            self._mac_scheduler.remove_job(str(result["mac_sequence_number"]))
                                            # remove current frame
                                            self._dict_mac_frame_in_progress.pop(0)
                                            # Check if there are job pending => if yes start it
                                            if len(self._dict_mac_frame_in_progress) >= 1:
                                                self._mac_scheduler.resume_job(
                                                    self._dict_mac_frame_in_progress[0].id_job)
                                                self._mac_scheduler.modify_job(
                                                    self._dict_mac_frame_in_progress[0].id_job,
                                                    next_run_time=(datetime.now() + timedelta(milliseconds=10)))
                                            # Send data in queue
                                            self._queue.put(data[:len(data) - 1])
                                        # Release mutex => Frame processed
                                        self._mutex.release()
                                    # Check if this is a command
                                    # We can receive a command while we are waiting for an acknowledgment
                                    # We need still need to treat it
                                    else:
                                        result = self._parse_frame(data)
                                        if result["is_data_control_frame"]:
                                            # Send data in queue
                                            self._queue.put(data[:len(data) - 1])
                                        # Send acknowledgment
                                        self._send_ack(result["ack_status_to_send"])
                                        # logger
                                        if result["ack_status_to_send"] != MacAckStatus.ACK_SUCCESS.value:
                                            get_logger()\
                                                .error('MAC_ACK_TO_SEND - {}'
                                                       .format(MacAckStatusDisplay()
                                                               .status_string_display[result["ack_status_to_send"]]))
                                        # Release mutex => Frame processed
                                        self._mutex.release()
                                else:
                                    # Release mutex => Didn't received the expected length of payload
                                    self._mutex.release()
        except serial.SerialException:
            self._reader_alive = False
            raise  # XXX handle instead of re-raise?

    # Return (is_ack, mac_sequence_number)
    def _parse_ack_frame(self, data):

        is_ack = False
        mac_sequence_number = -1

        # Check crc
        if check_crc(data):
            # Frame is an ACk ?
            if int(data[0]) == MacFrameControl.ACK.value:
                # Check if we are waiting an acknowledgment
                if len(self._dict_mac_frame_in_progress) >= 1:
                    # Check frame counter => Same as previous
                    if int(data[1]) == self._dict_mac_frame_in_progress[0].local_mac_sequence_number:
                        # Check if Error = CRC
                        if int(data[2]) != MacAckStatus.ACK_BAD_CRC.value:
                            is_ack = True
                            mac_sequence_number = int(data[1])

        return {"is_ack": is_ack, "mac_sequence_number": mac_sequence_number}

    # Return (Report_To_Applicative, ACK Status To Send)
    def _parse_frame(self, data):

        is_data_control_frame = False

        # Check crc
        if check_crc(data):
            # Compare two frames
            if (int(data[0]) == MacFrameControl.CONTROL.value) | (int(data[0]) == MacFrameControl.DATA.value):
                # Check frame counter
                if int(data[1]) != self._external_mac_sequence_number:
                    # Save frame counter
                    self._external_mac_sequence_number = int(data[1])
                    # Good frame
                    is_data_control_frame = True
                    ack_status_to_send = MacAckStatus.ACK_SUCCESS.value
                # Wrong Frame counter
                else:
                    ack_status_to_send = MacAckStatus.ACK_BAD_FRAME_COUNTER.value
            # ACK
            elif int(data[1]) == MacFrameControl.ACK.value:
                ack_status_to_send = MacAckStatus.SEND_NO_ACK.value
            # Wrong Frame
            else:
                ack_status_to_send = MacAckStatus.ACK_INVALID_COMMAND.value
        # Received frame got a bad CRC
        else:
            ack_status_to_send = MacAckStatus.ACK_BAD_CRC.value

        return {"is_data_control_frame": is_data_control_frame, "ack_status_to_send": ack_status_to_send}

    def _send_ack(self, result):

        # Don't need to send an acknowledgment when received an other acknowledgment
        if (result != MacAckStatus.SEND_NO_ACK.value) & (result != MacAckStatus.ACK_BAD_CRC.value):

            # Calculate length - Always 5
            # length - ACK - frame counter - status - CRC
            length = 5

            # Create frame
            frame = bytearray([self._default_starter_frame, length, MacFrameControl.ACK.value,
                               self._external_mac_sequence_number])

            # Add payload of the frame
            frame.append(result)

            # add crc
            crc = compute_crc(frame[2:], len(frame[2:]))
            frame.append(crc)

            # Write data on serial
            self._serial_instance.write(frame)

    def write_data(self, command, payload):

        # Calculate length
        # Length + Command + frame counter + payload + CRC
        length = len(payload) + 4

        # Increment frame counter
        if self._local_mac_sequence_number == 255:
            self._local_mac_sequence_number = 0
        else:
            self._local_mac_sequence_number += 1

        # Create frame
        frame = bytearray([self._default_starter_frame, length, command, self._local_mac_sequence_number])

        # Add payload of the frame
        for element in payload:
            frame.append(element)

        # add crc
        crc = compute_crc(frame[2:], len(frame[2:]))
        frame.append(crc)

        # Save Frame
        metadata_frame = types.SimpleNamespace()
        metadata_frame.local_mac_sequence_number = self._local_mac_sequence_number
        metadata_frame.id_job = str(self._local_mac_sequence_number)
        metadata_frame.number_retry = 0
        self._dict_mac_frame_in_progress.append(metadata_frame)

        # Schedule the jobs to send the command/data frame
        self._mac_scheduler.add_job(self._writer, trigger='interval', args=(frame,),
                                    seconds=0.2, id=str(self._local_mac_sequence_number))
        # Store the job
        self._mac_scheduler.pause_job(metadata_frame.id_job)

        # If no job are running => start it
        if len(self._dict_mac_frame_in_progress) == 1:
            self._mac_scheduler.resume_job(self._dict_mac_frame_in_progress[0].id_job)
            self._mac_scheduler.modify_job(self._dict_mac_frame_in_progress[0].id_job, next_run_time=datetime.now())

    def _writer(self, frame):

        # We don't send multiple frame at one
        self._mutex_no_multiple_send.acquire()

        try:

            # We are trying to send the frame 3 times
            if self._dict_mac_frame_in_progress[0].number_retry < 3:

                # increase retry counter
                self._dict_mac_frame_in_progress[0].number_retry += 1
                # Acquire mutex
                self._mutex.acquire()
                # Write data on serial
                self._serial_instance.write(frame)
                # Release mutex
                self._mutex.release()

            else:
                # Send error to applicative
                error = bytearray([MacFrameControl.ACK.value, 0x00, MacAckStatus.ACK_RETRY_EXPIRED.value])
                self._queue.put(error)
                # Remove process of sending the command/data frame
                self._mac_scheduler.remove_job(self._dict_mac_frame_in_progress[0].id_job)
                self._dict_mac_frame_in_progress.pop(0)
                # Check if there are job pending => if yes start it
                if len(self._dict_mac_frame_in_progress) >= 1:
                    self._mac_scheduler.resume_job(self._dict_mac_frame_in_progress[0].id_job)
                    self._mac_scheduler.modify_job(self._dict_mac_frame_in_progress[0].id_job,
                                                   next_run_time=(datetime.now() + timedelta(milliseconds=10)))
        except Exception:
            self._reader_alive = False
            raise

        # We finish to send the current frame
        self._mutex_no_multiple_send.release()

    def set_rx_encoding(self, encoding, errors='replace'):
        """set encoding for received data"""
        self._input_encoding = encoding
        self._rx_decoder = codecs.getincrementaldecoder(encoding)(errors)

    def set_tx_encoding(self, encoding, errors='replace'):
        """set encoding for transmitted data"""
        self._output_encoding = encoding
        self._tx_encoder = codecs.getincrementalencoder(encoding)(errors)

    def change_encoding(self):
        """change encoding on the serial port"""
        sys.stderr.write('\n--- Enter new encoding name [{}]: '.format(self._input_encoding))
        new_encoding = sys.stdin.readline().strip()
        if new_encoding:
            try:
                codecs.lookup(new_encoding)
            except LookupError:
                sys.stderr.write('--- invalid encoding name: {}\n'.format(new_encoding))
            else:
                self.set_rx_encoding(new_encoding)
                self.set_tx_encoding(new_encoding)
            sys.stderr.write('--- serial input encoding: {}\n'.format(self._input_encoding))
            sys.stderr.write('--- serial output encoding: {}\n'.format(self._output_encoding))

    def change_port(self):
        """Have a conversation with the user to change the serial port"""
        try:
            port = ask_for_port()
        except KeyboardInterrupt:
            port = None
        if port and port != self._serial_instance.port:
            # reader thread needs to be shut down
            self._stop_reader()
            # save settings
            settings = self._serial_instance.getSettingsDict()
            new_serial = None
            try:
                new_serial = serial.serial_for_url(port, do_not_open=True)
                # restore settings and open
                new_serial.applySettingsDict(settings)
                new_serial.rts = self._serial_instance.rts
                new_serial.dtr = self._serial_instance.dtr
                new_serial.open()
                new_serial.break_condition = self._serial_instance.break_condition
            except Exception as e:
                sys.stderr.write('--- ERROR opening new port: {} ---\n'.format(e))
                new_serial.close()
            else:
                self._serial_instance.close()
                self._serial_instance = new_serial
                sys.stderr.write('--- Port changed to: {} ---\n'.format(self._serial_instance.port))
            # and restart the reader thread
            self._start_reader()
