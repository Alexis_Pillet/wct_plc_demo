/******************************************************************************
 *                          ZAB TEST APPLICATION
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the ZCL handlers for the test app.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.03.00  16-Jan-14   MvdB   Use common header block
 * 00.00.06.00  18-Aug-14   MvdB   Add support for callback based metering cluster plugin
 * 00.00.06.06  30-Oct-14   MvdB   artf74202: Support callbacks in Nova plugins
 * 002.001.000  20-Apr-15   MvdB   ARTF131071: Add appZcl_ClusterCmdHandler for testing reception of Nova Online/Offline ntf
 * 002.001.001  15-Jul-15   MvdB   Add appZcl_GetZclStatusString() and appZcl_GetSzlAddressString()
 * 002.001.003  16-Jul-15   MvdB   ARTF132827: Include source address in cluster command handlers
 * 002.002.020  18-Mar-16   MvdB   Extern appZcl_PrintAppAttribute for use form other files
 * 002.002.021  21-Apr-16   MvdB   ARTF167807: Support Multi-Cluster Attribute Read/Write for GP
 * 002.002.035  11-Jan-17   MvdB   ARTF170823: Make Read/Write Reporting configuration data (SZL_AttributeReportData_t) consistent with Read/Write data (SZL_AttributeData_t)
 * 002.002.049  14-Feb-17   MvdB   Add appZcl_GetClusterString()
 * 002.002.054  20-Jul-17   SMon   ARTF214292 Manage timeout for default response
 *****************************************************************************/

#ifndef APP_ZCL_H_
#define APP_ZCL_H_

#include "zabCoreService.h"

#include "szl_zab_types.h"

#include "basic_cluster.h"
#include "ecb_cluster.h"
#include "elec_meas_cluster.h"
#include "identify_cluster.h"
#include "on_off_cluster.h"
#include "metering_cluster.h"

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Store a new Timeout for a szl cmdreq
 ******************************************************************************/
extern 
void appZcl_SetTimeout(zabService* Service,szl_uint32 tid,szl_uint8 command, szl_uint8 timeout,szl_uint8  nbRetry);

/******************************************************************************
 * Print out attribute data
 ******************************************************************************/
void appZcl_PrintAppAttribute(zabService* Service, SZL_ZIGBEE_DATA_TYPE_t DataType, void* Data, szl_uint16 DataLength);

/**************************************************************************
 * Print an SZL function result with TID
 **************************************************************************/
void appZcl_PrintSzlResult(zabService* Service, char* Description, SZL_RESULT_t Result, szl_uint8 Tid);

/******************************************************************************
 * Get a string for an SZL Result
 ******************************************************************************/
extern
char* appZcl_GetSzlResultString(SZL_RESULT_t result);

/******************************************************************************
 * Get a string for a ZCL_STATUS
 ******************************************************************************/
extern
char* appZcl_GetZclStatusString(ZCL_STATUS ZclStatus);

/******************************************************************************
 * Get a string for a SZL_ADDRESS_MODE_t
 ******************************************************************************/
extern
char* appZcl_GetSzlAddressModeString(SZL_ADDRESS_MODE_t AddressMode);

/******************************************************************************
 * Get an SZL Address Struct as a string
 ******************************************************************************/
extern
char* appZcl_GetSzlAddressString(SZL_Addresses_t* Address, char* AddrString, unsigned16 MaxStringLength);


/******************************************************************************
 * Get a string for a ZCL Cluster ID
 ******************************************************************************/
extern 
char* appZcl_GetClusterString(unsigned16 ClusterId);

/******************************************************************************
 * SZL Attibute Changed Notification Handler
 ******************************************************************************/
extern
void appZcl_AttributeChangedNotificationHandler(zabService* Service, struct _SZL_AttributeChangedNtfParams_t* Params);

/******************************************************************************
 * Identify Handler
 ******************************************************************************/
extern
void appZcl_SzlIdentifyPluginHandler(zabService* Service, szl_uint8 endpoint, szl_bool identifying);

/******************************************************************************
 * On/Off Handler
 ******************************************************************************/
extern
void appZcl_SzlOnOffPluginHandler(zabService* Service, szl_uint8 endpoint, szl_bool onOff);

/******************************************************************************
 * UTC Time Handler
 ******************************************************************************/
extern
void appZcl_SzlTimePluginHandler(szl_uint32* UtcTime);

/******************************************************************************
 * Cluster Command Response Handler
 ******************************************************************************/
extern
void appZcl_ClusterCmdRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ClusterCmdRespParams_t *Params, szl_uint8 TransactionId);

/******************************************************************************
 * Cluster Command Response Handler Timeout Mgmt
 ******************************************************************************/
extern
void appZcl_ClusterCmdRspTimeOutMgmtHandler(zabService* Service, SZL_STATUS_t Status, SZL_ClusterCmdRespParams_t *Params, szl_uint8 TransactionId);

/******************************************************************************
 * Read Attributes Response Handler
 ******************************************************************************/
extern
void appZcl_ReadAttrRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_AttributeReadRespParams_t *Params, szl_uint8 TransactionId);

/******************************************************************************
 * Multi Cluster Read Attributes Response Handler
 ******************************************************************************/
extern
void appZcl_MultiCluster_ReadAttrRspHandler(zabService* Service, SZL_STATUS_t Status, SZLEXT_MultiCluster_AttributeReadRespParams_t *Params, szl_uint8 TransactionId);

/******************************************************************************
 * Write Attributes Response Handler
 ******************************************************************************/
extern
void appZcl_WriteAttrRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_AttributeWriteRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * Multi Cluster Write Attributes Response Handler
 ******************************************************************************/
extern
void appZcl_MultiCluster_WriteAttrRspHandler(zabService* Service, SZL_STATUS_t Status, SZLEXT_MultiCluster_AttributeWriteRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * Read Reporting Configuration Response Handler
 ******************************************************************************/
extern
void appZcl_ReadReportingConfigRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_AttributeReadReportCfgRespParams_t *Params, szl_uint8 TransactionId);

/******************************************************************************
 * Discover Attributes Response Handler
 ******************************************************************************/
extern
void appZcl_DiscoverAttrRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_AttributeDiscoverRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * Configure Reporting Response Handler
 ******************************************************************************/
extern
void appZcl_ConfigureReportingHandler(zabService* Service, SZL_STATUS_t Status, SZL_AttributeReportCfgRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * Report Attributes Handler
 ******************************************************************************/
extern
void appZcl_ReportAttrHandler(zabService* Service, SZL_AttributeReportNtfParams_t *Params);

/******************************************************************************
 * Received RSSI Handler
 ******************************************************************************/
extern
void appZcl_ReceivedSignalStrengthHandler(zabService* Service, struct _SZL_ReceivedSignalStrengthNtfParams_t* Params);

/******************************************************************************
 * Raw ZCL Handler
 * Do not suppress further processing
 ******************************************************************************/
extern
void appZcl_RawZclHandler(erStatus* Status,
                         zabService* Service,
                         zab_bool* suppressProcessing,
                         zabMsgProDataInd* dataInd);

/******************************************************************************
 * Generic Cluster Command Handler
 ******************************************************************************/
extern
szl_bool appZcl_ClusterCmdHandler(zabService* Service,
                                  SZL_EP_t DestinationEndpoint,
                                  szl_bool ManufacturerSpecific,
                                  szl_uint16 ClusterID,
                                  szl_uint8 CmdID, szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize,
                                  szl_uint8* CmdOutID, szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize,
                                  szl_uint8 TransactionId,
                                  SZL_Addresses_t SourceAddress,
                                  SZL_ADDRESS_MODE_t DestAddressMode);

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif /* APP_ZCL_H_ */
