/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the OTA Upgrade Server.
 *   This plugin is originally designed for MiGenie.
 *
 * USAGE:
 * 
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         05-Jul-16   MvdB   Original
 *****************************************************************************/

#ifndef _OTA_SERVER_
#define _OTA_SERVER_

#include "zabTypes.h"
#include "szl.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* Supported version of the OTA header */
#define SZL_PLUGIN_OTA_SERVER_FILE_HEADER_VERSION           ( 0x0100 )

/* Min/Max sizes of header block (Version 0x0100) */
#define SZL_PLUGIN_OTA_SERVER_FILE_HEADER_BLOCK_SIZE_MIN    ( 56 )
#define SZL_PLUGIN_OTA_SERVER_FILE_HEADER_BLOCK_SIZE_MAX    ( 65 )

/* Length of the string in the OTA header */
#define SZL_PLUGIN_OTA_SERVER_FILE_HEADER_STRING_LENGTH     ( 32 )


/* Invalid value for hardware version */
#define SZL_PLUGIN_OTA_SERVER_HARDWARE_VERSION_INVALID      ( 0xFFFF )

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/**
 * OTA Upgrade Cluster Server - ZigBee Stack Version
 */
typedef enum
{
  SzlPlugin_OtaServer_ZigBeeStackVersion_2006 = 0x0000,
  SzlPlugin_OtaServer_ZigBeeStackVersion_2007 = 0x0001,
  SzlPlugin_OtaServer_ZigBeeStackVersion_Pro  = 0x0002,
  SzlPlugin_OtaServer_ZigBeeStackVersion_IP   = 0x0003,
}SzlPlugin_OtaServer_ZigBeeStackVersion_t;

/**
 * OTA Upgrade Cluster Server - Security Credential Version
 */
typedef enum
{
  SzlPlugin_OtaServer_SecurityCredentialVersion_SE_1_0 = 0x00,
  SzlPlugin_OtaServer_SecurityCredentialVersion_SE_1_1 = 0x01,
  SzlPlugin_OtaServer_SecurityCredentialVersion_SE_2_0 = 0x02,
  SzlPlugin_OtaServer_SecurityCredentialVersion_SE_1_2 = 0x03,
}SzlPlugin_OtaServer_SecurityCredentialVersion_t;

/**
 * OTA Upgrade Cluster Server - File Header Block type
 */
typedef struct
{
  szl_uint16 OtaHeaderVersion;              /* The version of the header and provides compatibility information. Currently 0x0100. */
  szl_uint16 OtaHeaderLength;               /* Full length of the OTA header in bytes. */
  union
    {
      szl_uint16 OptWord;
      struct
        {
          szl_uint16 SecurityCredentialVersionPresent : 1;  /* Indicates SecurityCredentialVersion field is present and valid */
          szl_uint16 DeviceSpecificFile : 1;                /* Indicates UpgradeFileDestination field is present and valid */
          szl_uint16 HardwareVersionsPresent : 1;           /* Indicates MinimumHardwareVersion and MaximumHardwareVersion fields are present and valid */
          szl_uint16 Reserved : 13;
        } optBit;
    } OtaHeaderFieldControl;
  szl_uint16 ManufacturerCode;              /* ZigBee assigned identifier of Manufacturer */
  szl_uint16 ImageType;                     /* Image type of the file - defines which device it is for */
  szl_uint32 FileVersion;                   /* Version of the file */
  SzlPlugin_OtaServer_ZigBeeStackVersion_t ZigBeeStackVersion; /* Version of the ZigBee Stack used in this file */
  szl_uint8 OTAHeaderString[SZL_PLUGIN_OTA_SERVER_FILE_HEADER_STRING_LENGTH]; /* Manufacturer Specific string. Human readable. Zero terminated. */
  szl_uint32 TotalImageSize;                /* Total size of the file including header */

  /* Optional extended data */
  SzlPlugin_OtaServer_SecurityCredentialVersion_t SecurityCredentialVersion;  /* Security Credential required by client before it installs the image. Valid if SecurityCredentialVersionPresent is set */
  szl_uint64 UpgradeFileDestination;        /* IEEE address of device this file is intended for. Valid if DeviceSpecificFile is set */
  szl_uint16 MinimumHardwareVersion;        /* Earliest hardware platform version this image SHOULD be used on. Valid if HardwareVersionsPresent is set */
  szl_uint16 MaximumHardwareVersion;        /* Latest hardware platform version this image SHOULD be used on. Valid if HardwareVersionsPresent is set */
} SzlPlugin_OtaServer_FileHeader_t;

/**
 * OTA Upgrade Cluster Server - Query Next Image Request handler
 * 
 * This callback will be called on receipt of a Query Next Image Request.
 * This notifies the application  of the node's image details and allows the return of
 * information about any new image the server has available.
 * 
 * @param[in]  Service                      Service Instance Pointer
 * @param[in]  SourceNetworkAddress         Network address of the source of the command
 * @param[in]  ManufacturerCode             Device’s assigned manufacturer code
 * @param[in]  ImageType                    Image type assigned to the device by the manufacturer
 * @param[in]  CurrentFileVersion           Device’s current running image version
 * @param[in]  HardwareVersion              Device’s current running hardware version. SZL_PLUGIN_OTA_SERVER_HARDWARE_VERSION_INVALID if not specified.
 * @param[in]  NewImageBlockLengthRequired  Length of image data required to be copied from start of upgrade file to NewImageBlock
 * @param[out] NewImageBlockLength          Length of image data copied from start of upgrade file to NewImageBlock. Should be equal to NewImageBlockLengthRequired if successful.
 * @param[out] NewImageBlock                Pointer where image data must be copied by application
 * 
 * @return     SZL_STATUS_t                 SZL_STATUS_SUCCESS if image available and data at NewImageBlock is valid.
 *                                          SZL_STATUS_OTA_NO_IMAGE_AVAILABLE if image not available at this time. Client will periodically retry.
 *                                          SZL_STATUS_NOT_AUTHORIZED if the server is not authorized to upgrade the client.
 */
typedef SZL_STATUS_t (*SzlPlugin_OtaServer_QueryNextImage_CB_t) (zabService* Service,
                                                                 szl_uint16 SourceNetworkAddress,
                                                                 szl_uint16 ManufacturerCode,
                                                                 szl_uint16 ImageType,
                                                                 szl_uint32 CurrentFileVersion,
                                                                 szl_uint16 HardwareVersion,
                                                                 szl_uint8  NewImageBlockLengthRequired,
                                                                 szl_uint8* NewImageBlockLength,
                                                                 szl_uint8* NewImageBlock);

/**
 * OTA Upgrade Cluster Server - Image Block Request handler
 * 
 * This callback will be called on receipt of an Image Block Request.
 * This requests from the application a chunk of image data to be returned to the upgrading device.
 * 
 * @param[in]  Service                      Service Instance Pointer
 * @param[in]  SourceNetworkAddress         Network address of the source of the command
 * @param[in]  ManufacturerCode             Device’s assigned manufacturer code
 * @param[in]  ImageType                    Image type assigned to the device by the manufacturer
 * @param[in]  FileVersion                  Version of the file that the device is requesting data from
 * @param[in]  FileOffset                   Offset within the file that the device is requesting data from
 * @param[in]  MaxDataLength                Maximum length of data requested from the application.
 * @param[out] DataLength                   Length of image data copied from FileOffset in upgrade file to Data. Must not exceed MaxDataLength.
 * @param[out] Data                         Pointer where image data must be copied by application
 * 
 * @return     SZL_STATUS_t                 SZL_STATUS_SUCCESS if image available and Data is valid.
 *                                          SZL_STATUS_OTA_ABORT if image not available at this time.
 */
typedef SZL_STATUS_t (*SzlPlugin_OtaServer_ImageBlockReq_CB_t) (zabService* Service,
                                                                szl_uint16 SourceNetworkAddress,
                                                                szl_uint16 ManufacturerCode,
                                                                szl_uint16 ImageType,
                                                                szl_uint32 FileVersion,
                                                                szl_uint32 FileOffset,
                                                                szl_uint8  MaxDataLength,
                                                                szl_uint8* DataLength,
                                                                szl_uint8* Data);

/**
 * OTA Upgrade Cluster Server - Upgrade End Request handler
 * 
 * This callback will be called on receipt of an Upgrade End Request.
 * This tells the application that the device has completed the transfer and the image is
 * valid (or not). The application can then specify when the update should be applied,
 * for example: immediately, at midnight etc.
 * 
 * @param[in]  Service                      Service Instance Pointer
 * @param[in]  SourceNetworkAddress         Network address of the source of the command
 * @param[in]  Status                       Status of the file transfer:
 *                                           - SZL_STATUS_SUCCESS
 *                                           - SZL_STATUS_OTA_IMAGE_INVALID
 *                                           - SZL_STATUS_OTA_REQUIRE_MORE_IMAGE
 *                                           - SZL_STATUS_OTA_ABORT
 * @param[in]  ManufacturerCode             Device’s assigned manufacturer code
 * @param[in]  ImageType                    Image type assigned to the device by the manufacturer
 * @param[in]  FileVersion                  Version of the file that has been transfered
 * @param[out] CurrentTime                  Current UTC time. Only used if Status == SZL_STATUS_SUCCESS.
 * @param[out] UpgradeTime                  UTC time at which to apply the update. Only used if Status == SZL_STATUS_SUCCESS.
 */
typedef void (*SzlPlugin_OtaServer_UpgradeEndReq_CB_t) (zabService* Service,
                                                        szl_uint16 SourceNetworkAddress,
                                                        SZL_STATUS_t Status,
                                                        szl_uint16 ManufacturerCode,
                                                        szl_uint16 ImageType,
                                                        szl_uint32 FileVersion,
                                                        szl_uint32* CurrentTime,
                                                        szl_uint32* UpgradeTime);


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/ 

/**
 * OTA Cluster Cluster Server - Initialization
 *
 * This function must be called from the application after the Library has been initialized.
 * 
 * @param[in]  Service                Service Instance Pointer
 * @param[in]  Endpoint               Endpoint on which cluster will be run
 * @param[in]  QueryNextImageCallback Callback function for received Query Next Image commands
 * @param[in]  ImageBlockReqCallback  Callback function for received Image Block Request commands
 * @param[in]  UpgradeEndReqCallback  Callback function for received Upgrade End Request commands
 * 
 * @return     SZL_RESULT_t           SZL_RESULT_SUCCESS if successful.
 */
extern 
SZL_RESULT_t SzlPlugin_OtaServer_Init(zabService* Service, 
                                      SZL_EP_t Endpoint,
                                      SzlPlugin_OtaServer_QueryNextImage_CB_t QueryNextImageCallback,
                                      SzlPlugin_OtaServer_ImageBlockReq_CB_t ImageBlockReqCallback,
                                      SzlPlugin_OtaServer_UpgradeEndReq_CB_t UpgradeEndReqCallback);


/**
 * OTA Cluster Cluster Server - Destroy
 * 
 * @param[in]  Service                Service Instance Pointer
 * 
 * @return     SZL_RESULT_t           SZL_RESULT_SUCCESS if successful.
 */
extern 
SZL_RESULT_t SzlPlugin_OtaServer_Destroy(zabService* Service);


/**
 * OTA Cluster Cluster Server - Parse a file header
 * 
 * @param[in]  Service        Service Instance Pointer
 * @param[in]  FileDataLength Length of FileData provided. Must be at least SZL_PLUGIN_OTA_SERVER_FILE_HEADER_BLOCK_SIZE_MAX.
 * @param[in]  FileData       Buffer containing file contents from the start of file
 * @param[out] FileHeader     Structure that will be populated with file data if SZL_RESULT_SUCCESS returned
 * 
 * @return     SZL_RESULT_t   SZL_RESULT_SUCCESS if successful.
 */
extern 
SZL_RESULT_t SzlPlugin_OtaServer_ParseFileHeader(zabService* Service,
                                                 szl_uint32 FileDataLength, 
                                                 szl_uint8* FileData, 
                                                 SzlPlugin_OtaServer_FileHeader_t* FileHeader);


#ifdef __cplusplus
}
#endif
#endif /*_OTA_SERVER_*/