/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL Device Manager plugin.
 *   This plugin is originally designed for MiGenie.
 *
 * USAGE:
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         08-Jul-16   MvdB   Original
 * 002.002.047  31-Jan-17   MvdB   Support optional parameters on DeviceManager gets()
 *****************************************************************************/
#ifndef _DEVICE_MANAGER_H_
#define _DEVICE_MANAGER_H_

#include "szl.h"
#include "szl_zdo_types.h"


#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 *                      ******************************
 *                 *****          CONSTANTS           *****
 *                      ******************************
 ******************************************************************************/

/* Unknown address values */
#define SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN                  ( 0xFFFF )
#define SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN                     ( 0xFFFFFFFFFFFFFFFF )

/* Minimum and default values for IncompleteDiscoveryRetryTime */
#define SZL_PLUGIN_DEV_MAN_INCOMPLETE_DISCOVERY_RETRY_TIME_MIN      ( 60 )
#define SZL_PLUGIN_DEV_MAN_INCOMPLETE_DISCOVERY_RETRY_TIME_DEFAULT  ( 600 )

/* Minimum and default values for DiscoveryRefreshTime */
#define SZL_PLUGIN_DEV_MAN_DISCOVERY_REFRESH_TIME_MIN               ( 600 )
#define SZL_PLUGIN_DEV_MAN_DISCOVERY_REFRESH_TIME_DEFAULT           ( 24 * 60 * 60 )

/* Value of CommsTimeThreshold when disabled */
#define SZL_PLUGIN_DEV_MAN_COMMS_TIME_THRESHOLD_DISABLED            ( 0xFFFFFFFF )

/* Value of TimeSinceLastComms when it is unknown */
#define SZL_PLUGIN_DEV_MAN_TIME_SINCE_LAST_COMMS_UNKNOWN            ( 0xFFFFFFFF )

/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/**
 * Device Manager - Address Changed handler - OPTIONAL
 *
 * This callback will be called on change of any Network Address to IEEE Address mapping.
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  NetworkAddress     Network address of the node, or SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN
 * @param[in]  IeeeAddress        IEEE Address of the node, or SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN
 */
typedef void (*SzlPlugin_DevMan_AddressChanged_CB_t) (zabService* Service,
                                                      szl_uint16 NetworkAddress,
                                                      szl_uint64 IeeeAddress);

/**
 * Device Manager - Communications from un-tracked node handler - OPTIONAL
 *
 * This callback will be called if a message is received from a node that is not
 * being tracked in the device manager.
 * The application can then choose to load the node for tracking or not.
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  NetworkAddress     Network address of the node, or SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN
 * @param[in]  IeeeAddress        IEEE Address of the node, or SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN
 */
typedef void (*SzlPlugin_DevMan_UntrackedNodeComms_CB_t) (zabService* Service,
                                                          szl_uint16 NetworkAddress,
                                                          szl_uint64 IeeeAddress);

/**
 * Device Manager - Device Left handler - OPTIONAL
 *
 * This callback will be called if a node is detected leaving the network and will
 * not automatically rejoin. Any corresponding entries in the device manager will be
 * deleted after this callback has returned.
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  NetworkAddress     Network address of the node, or SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN
 * @param[in]  IeeeAddress        IEEE Address of the node, or SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN
 */
typedef void (*SzlPlugin_DevMan_DeviceLeft_CB_t) (zabService* Service,
                                                  szl_uint16 NetworkAddress,
                                                  szl_uint64 IeeeAddress);

/**
 * Device Manager - Time since last communication exceeds threshold handler - OPTIONAL
 *
 * This callback will be called once a node has failed to communicate for the time
 * specified during device manager initialisation.
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  NetworkAddress     Network address of the node, or SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN
 * @param[in]  IeeeAddress        IEEE Address of the node, or SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN
 * @param[in]  TimeSinceLastComms Time in seconds since last message was received from the device
 */
typedef void (*SzlPlugin_DevMan_TimeSinceLastCommsExceedsThreshold_CB_t) (zabService* Service,
                                                                          szl_uint16 NetworkAddress,
                                                                          szl_uint64 IeeeAddress,
                                                                          szl_uint32 TimeSinceLastComms);


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/**
 * Device Manager - Initialization
 *
 * This function must be called from the application after the Library has been initialized.
 *
 * @param[in]  Service                        Service Instance Pointer
 * @param[in]  MaxSupportedNodes              Number of nodes supported by the manager
 *                                             - RAM required will scale linearly
 * @param[in]  MaxRequestsOnAir               Number of discovery requests the manager may have in process at a time
 *                                             - Controls how many transaction buffers are used by the manager
 *                                             - Recommend a fraction of SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS
 * @param[in]  IncompleteDiscoveryRetryTime   Time after which addresses of a node with either address unknown will be retried
 *                                             - Must not be less than SZL_PLUGIN_DEV_MAN_INCOMPLETE_DISCOVERY_RETRY_TIME_MIN
 *                                             - Recommend SZL_PLUGIN_DEV_MAN_INCOMPLETE_DISCOVERY_RETRY_TIME_DEFAULT
 * @param[in]  DiscoveryRefreshTime           Time after which addresses of a node with known addresses will be refreshed
 *                                             - Must not be less than SZL_PLUGIN_DEV_MAN_DISCOVERY_REFRESH_TIME_MIN
 *                                             - Recommend SZL_PLUGIN_DEV_MAN_DISCOVERY_REFRESH_TIME_DEFAULT
 * @param[in]  CommsTimeThreshold             Time in seconds of no communication after which application will be notified via TimeExceedsThresholdCallback
 *                                             - SZL_PLUGIN_DEV_MAN_COMMS_TIME_THRESHOLD_DISABLED if not required
 * @param[in]  AddressChangedCallback         OPTIONAL - Called on change of any Network Address to IEEE Address mapping
 * @param[in]  UntrackedNodeCommsCallback     OPTIONAL - Called if a message is received from a node that is not being tracked
 * @param[in]  DeviceLeftCallback             OPTIONAL - Called if a node is detected leaving the network
 * @param[in]  TimeExceedsThresholdCallback   OPTIONAL - Called if a node has failed to communicate for the time threshold
 *
 * @return     SZL_RESULT_t                   SZL_RESULT_SUCCESS if successful.
 */
extern
SZL_RESULT_t SzlPlugin_DevMan_Init(zabService* Service,
                                   szl_uint16 MaxSupportedNodes,
                                   szl_uint16 MaxRequestsOnAir,
                                   szl_uint32 IncompleteDiscoveryRetryTime,
                                   szl_uint32 DiscoveryRefreshTime,
                                   szl_uint32 CommsTimeThreshold,
                                   SzlPlugin_DevMan_AddressChanged_CB_t AddressChangedCallback,
                                   SzlPlugin_DevMan_UntrackedNodeComms_CB_t UntrackedNodeCommsCallback,
                                   SzlPlugin_DevMan_DeviceLeft_CB_t DeviceLeftCallback,
                                   SzlPlugin_DevMan_TimeSinceLastCommsExceedsThreshold_CB_t TimeExceedsThresholdCallback);

/**
 * Device Manager - Destroy
 *
 * @param[in]  Service                Service Instance Pointer
 *
 * @return     SZL_RESULT_t           SZL_RESULT_SUCCESS if successful.
 */
extern
SZL_RESULT_t SzlPlugin_DevMan_Destroy(zabService* Service);

/**
 * Device Manager - Load by Network Address
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  NetworkAddress     Network address of the node
 *
 * @return     SZL_RESULT_t       SZL_RESULT_SUCCESS if successful
 *                                SZL_RESULT_ALREADY_EXISTS if already in the device manager
 *                                Other on error
 */
extern
SZL_RESULT_t SzlPlugin_DevMan_LoadByNetworkAddress(zabService* Service, szl_uint16 NetworkAddress);

/**
 * Device Manager - Load by IEEE Address
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  IeeeAddress        IEEE Address of the node
 *
 * @return     SZL_RESULT_t       SZL_RESULT_SUCCESS if successful.
 *                                SZL_RESULT_ALREADY_EXISTS if already in the device manager.
 *                                Other on error.
 */
extern
SZL_RESULT_t SzlPlugin_DevMan_LoadByIeeeAddress(zabService* Service, szl_uint64 IeeeAddress);

/**
 * Device Manager - Remove by Network Address
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  NetworkAddress     Network address of the node
 *
 * @return     SZL_RESULT_t       SZL_RESULT_SUCCESS if successful
 *                                Other on error.
 */
extern
SZL_RESULT_t SzlPlugin_DevMan_RemoveByNetworkAddress(zabService* Service, szl_uint16 NetworkAddress);

/**
 * Device Manager - Remove by IEEE Address
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  IeeeAddress        IEEE Address of the node
 *
 * @return     SZL_RESULT_t       SZL_RESULT_SUCCESS if successful
 *                                Other on error.
 */
extern
SZL_RESULT_t SzlPlugin_DevMan_RemoveByIeeeAddress(zabService* Service, szl_uint64 IeeeAddress);

/**
 * Device Manager - Get IEEE Address and Time Since Last Comms from Network Address
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  NetworkAddress     Network address of the node
 * @param[out] IeeeAddress        IEEE Address of the node
 *                                 - Set NULL if not required
 *                                 - SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN if not found
 * @param[out] TimeSinceLastComms Time in seconds since last message was received from the device
 *                                 - Set NULL if not required
 *                                 - SZL_PLUGIN_DEV_MAN_TIME_SINCE_LAST_COMMS_UNKNOWN if not found no never communicated
 *
 * @return     SZL_RESULT_t       SZL_RESULT_SUCCESS if successful
 *                                Other on error.
 */
extern
SZL_RESULT_t SzlPlugin_DevMan_GetByNetworkAddress(zabService* Service, szl_uint16 NetworkAddress, szl_uint64* IeeeAddress, szl_uint32* TimeSinceLastComms);

/**
 * Device Manager - Get Network Address and Time Since Last Comms from IEEE Address
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  IeeeAddress        IEEE Address of the node
 * @param[out] NetworkAddress     Network address of the node
 *                                 - Set NULL if not required
 *                                 - SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN if not found
 * @param[out] TimeSinceLastComms Time in seconds since last message was received from the device
 *                                 - Set NULL if not required
 *                                 - SZL_PLUGIN_DEV_MAN_TIME_SINCE_LAST_COMMS_UNKNOWN if not found no never communicated
 *
 * @return     SZL_RESULT_t       SZL_RESULT_SUCCESS if successful
 *                                Other on error.
 */
extern
SZL_RESULT_t SzlPlugin_DevMan_GetByIeeeAddress(zabService* Service, szl_uint64 IeeeAddress, szl_uint16* NetworkAddress, szl_uint32* TimeSinceLastComms);

/**
 * Device Manager - Get Network Address and Time Since Last Comms from IEEE Address
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  Index              Table index to read from
 * @param[out] NetworkAddress     Network address of the node
 *                                 - Set NULL if not required
 *                                 - SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN if not found
 * @param[out] IeeeAddress        IEEE Address of the node
 *                                 - Set NULL if not required
 *                                 - SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN if not found
 * @param[out] TimeSinceLastComms Time in seconds since last message was received from the device
 *                                 - Set NULL if not required
 *                                 - SZL_PLUGIN_DEV_MAN_TIME_SINCE_LAST_COMMS_UNKNOWN if not found no never communicated
 *
 * @return     SZL_RESULT_t       SZL_RESULT_SUCCESS if successful
 *                                SZL_RESULT_OUT_OF_RANGE is beyond the limits of the table
 *                                Other on error.
 */
extern
SZL_RESULT_t SzlPlugin_DevMan_GetByIndex(zabService* Service, szl_uint16 Index, szl_uint16* NetworkAddress, szl_uint64* IeeeAddress, szl_uint32* TimeSinceLastComms);

#ifdef __cplusplus
}
#endif
#endif /*_DEVICE_MANAGER_H_*/
