/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file provides function to manage status.
 *
 *   Almost every ZAB function has as a first parameter type called Status.
 *   This is similar to a function return code value, but much more comprehensive
 *   for error detection and recovery.
 *   Once Status is set with an error, it can not be overwritten with another error
 *   until it is cleared. All other processing shall be suspended until the error
 *   is processed and cleared. The service code always checks the status at function
 *   entry and if there is an error, it will return immediately.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Cam Williams
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *  00.00.00.02 30-Oct-13   MvdB   Remove erStatusCreate() as it just duplicates erStatusClear()
 *                                 Tidy up the macro-mess
 * 002.000.001  03-Feb-15   MvdB   ARTF104099: Add service pointer to status for print function for multi instance apps.
 * 002.002.031  09-Jan-17   MvdB   ARTF172881: Add ER_CHECK_STATUS_VALUE used to avoid work() returning zero due to errors like szl not initialised
 *****************************************************************************/

#ifndef __ER_STATUS_H__
#define __ER_STATUS_H__


#include "erStatusConfigure.h"
#include "erStatusTypes.h"

/******************************************************************************
 *                      ******************************
 *                 *****            MACROS            *****
 *                      ******************************
 ******************************************************************************/

/* Error Access */
#define erStatusGetError( S )     ((zabError) (S)->Error)

/* Basic status checking */
#define erStatusIsOk( S )         ((S)->Error == ZAB_SUCCESS)
#define erStatusIsError( S )      ((S)->Error != ZAB_SUCCESS)

#define erStatusService( S )      ((S)->Service)

/* Status checking with returns on fail */
#define ER_CHECK_STATUS( s )        while (erStatusIsError( s )) { printError(erStatusService( s ), "ERROR Detected!!!!!!!!!!0x%04X!!!!!!!!!!!!\n", (s)->Error); return;          }
#define ER_CHECK_STATUS_ZERO( s )   while (erStatusIsError( s )) { printError(erStatusService( s ), "ERROR Detected!!!!!!!!!!0x%04X!!!!!!!!!!!!\n", (s)->Error); return 0;        }
#define ER_CHECK_STATUS_VALUE( s, v ) while (erStatusIsError( s )) { printError(erStatusService( s ), "ERROR Detected!!!!!!!!!!0x%04X!!!!!!!!!!!!\n", (s)->Error); return (v);        }
#define ER_CHECK_STATUS_TRUE( s )   while (erStatusIsError( s )) { printError(erStatusService( s ), "ERROR Detected!!!!!!!!!!0x%04X!!!!!!!!!!!!\n", (s)->Error); return zab_true;     }
#define ER_CHECK_STATUS_FALSE( s )  while (erStatusIsError( s )) { printError(erStatusService( s ), "ERROR Detected!!!!!!!!!!0x%04X!!!!!!!!!!!!\n", (s)->Error); return zab_false;    }

/* Null pointer checking
 * WARNING: THESE MACROS MAY BE USED TO VALIDATE FUNCTION PARAMETERS ONLY.
 *          THEY MUST NOT BE USED FOR CONFIRMING THE RESULTS OF ALLOCATIONS, AS THEY CAN BE DISABLED!
 */
#ifdef ER_CHECK_NULL_ERROR
/* User wants checking of null pointers in parameters */
#define ER_CHECK_NULL( s, p )           while ((p)==NULL) { erStatusSet( (s), SAP_ERROR_NULL_PTR ); return;      }  // returns  if pointer is null
#define ER_CHECK_NULL_VALUE( s, p, v )  while ((p)==NULL) { erStatusSet( (s), SAP_ERROR_NULL_PTR ); return v; }  // returns value if pointer is null
#define ER_CHECK_NULL_TRUE( s, p )      ER_CHECK_NULL_VALUE( (s), (p), zab_true)  // returns zab_true if pointer is null
#define ER_CHECK_NULL_FALSE( s, p )     ER_CHECK_NULL_VALUE( (s), (p), zab_false)  // zab_false if pointer is null
#define ER_CHECK_NULL_ZERO( s, p )      ER_CHECK_NULL_VALUE( (s), (p), 0)  // returns 0 if pointer is null
#else
/* User does not want checking of null pointers in parameters */
#define ER_CHECK_NULL( s, p )
#define ER_CHECK_NULL_TRUE( s, p )
#define ER_CHECK_NULL_FALSE( s, p )
#define ER_CHECK_NULL_ZERO( s, p )
#endif

/* Check both status and null (if null checking is enabled) */
#define ER_CHECK_STATUS_NULL( s, p )        do { ER_CHECK_STATUS( s );          ER_CHECK_NULL( s, p );          } while (0)
#define ER_CHECK_STATUS_NULL_TRUE( s, p )   do { ER_CHECK_STATUS_TRUE( s );     ER_CHECK_NULL_TRUE( s, p );     } while (0)
#define ER_CHECK_STATUS_NULL_FALSE( s, p )  do { ER_CHECK_STATUS_FALSE( s );    ER_CHECK_NULL_FALSE( s, p );    } while (0)
#define ER_CHECK_STATUS_NULL_ZERO( s, p )   do { ER_CHECK_STATUS_ZERO( s );     ER_CHECK_NULL_ZERO( s, p );     } while (0)

/* Equal checks */
#define ER_CHECK_EQUAL( s, x, y )       while ((x)!=(y)) { erStatusSet( s, SAP_ERROR_MSG_INVALID_LENGTH ); return;    }  // returns if values are not equal
#define ER_CHECK_EQUAL_ZERO( s, x, y )  while ((x)!=(y)) { erStatusSet( s, SAP_ERROR_MSG_INVALID_LENGTH ); return 0;    }  // returns 0 if values are not equal

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Clear an error
 ******************************************************************************/
extern
void erStatusClear( erStatus* Status, void* Service );

/******************************************************************************
 * Set an error
 ******************************************************************************/
extern
void erStatusSet( erStatus* Status, unsigned16 Error );

/******************************************************************************
 * Get a string description of an Error
 ******************************************************************************/
extern
char* erStatusUtility_GetErrorString(zabError errorNumber);

/******************************************************************************
 * Prototypes for Application notification when error is set
 * The application must implement this function if ER_STATUS_USER_SET is defined
 ******************************************************************************/
#ifdef ER_STATUS_USER_SET
void erStatusUserSet( erStatus* Status, unsigned16 Error ); // call back to user
#endif

#ifdef __cplusplus
}
#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif // __ER_STATUS_H__

