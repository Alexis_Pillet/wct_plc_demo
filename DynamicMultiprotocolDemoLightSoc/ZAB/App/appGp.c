/******************************************************************************
 *                          ZAB TEST APPLICATION
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the Green Power callbacks.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.06.00  24-Jul-14   MvdB   Original
 * 01.00.00.02  29-Jan-15   MvdB   Support GPD De-commissioning
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 * 002.000.004  01-Apr-15   MvdB   ConsoleTest - add boolean to enable/disable acceptance of GPDs
 * 002.001.001  15-Jul-15   MvdB   ARTF130228: Add unique address mode for GpSrcId
 *                                 ARTF136585: Add transaction IDs to over the air commands/responses
 * 002.002.001  19-Aug-15   MvdB   ARTF146858: Fix crash in print in appGp_ClientPlugin_SinkTableRemoveGpdRsp_Handler()
 * 002.002.002  01-Sep-15   MvdB   ARTF147441: Support Key Mode in appGp_GpdCommissioningNtf_Handler()
 * 002.002.003  01-Sep-15   MvdB   ARTF147441: Fix issue with SZL_GP_COMMISSIONING_KEY_MODE_DEFAULT
 * 002.002.013  14-Oct-15   MvdB   ARTF151072: Add service pointer to osTimeGetMilliseconds() and osTimeGetSeconds()
 * 002.002.019  24-Nov-15   MvdB   Support ZGP TransTable Req & Update for testing
 * 002.002.030  09-Jan-17   MvdB   ARTF198434: Upgrade SZL_GP_GpdCommissioningNtfParams_t to include command and cluster lists
 * 002.002.044  23-Jan-17   MvdB   Add greenPowerFilterByModelId
 * 002.002.051  16-Mar-17   MvdB   ConsoleTest: Add selective GP commissioning by source ID
 * 002.002.052  12-Apr-17   SMon   ConsoleTest: Add a decommissioning all command
 *****************************************************************************/

#include "zabCoreService.h"
#include "szl.h"
#include "szl_gp.h"
#include "appConfig.h"
#include "appZcl.h"
#include "gp_client.h"

#ifdef APP_CONFIG_ENABLE_GREEN_POWER

typedef struct
{
    szl_uint32 SrcId;                                   /**< Gp Source Id of the node */
    szl_uint8 DeviceId;                                 /**< GP Device Id */
    unsigned32 LastCommsTime;
    unsigned32 LongestTimeBetweenComms;
    unsigned32 PacketCount;
    szl_uint8  LeaveOnGoing;
    unsigned32 LastLeaveTime;
} gpdInfo_t;

#define M_MAX_GPDS 250




static gpdInfo_t gpdInfo[M_MAX_GPDS];

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/
int zgp_leave_cmd(zabService* Service, SZL_Addresses_t leave_adress)
{
    SZL_RESULT_t result = SZL_RESULT_FAILED;
    char addrString[APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH];
    
    SZL_GP_CmdReqParams_t* gpCmdReq;

    /* Malloc parameters with required payload space */
    gpCmdReq = (SZL_GP_CmdReqParams_t *)APP_MEM_GLUE_MALLOC(SZL_GP_CmdReqParams_t_SIZE(0),
                                                            appMain_GetServiceId(Service),
                                                            MALLOC_ID_APP_GENERAL);
    if (gpCmdReq != NULL)
    {
        gpCmdReq->SourceEndpoint = appConfig_Config.greenPowerEndpoint;
        gpCmdReq->Address = leave_adress;
        gpCmdReq->GpCommand = 0xFD;
        gpCmdReq->PayloadLength = 0;

        printApp(Service, "CMD: Sending ZGP Schneider Leave (0xFD): Dest = %s\n",
                 appZcl_GetSzlAddressString(&gpCmdReq->Address, addrString, sizeof(addrString)));

        result = SZL_GP_CmdReq(Service, gpCmdReq);
        if (result != SZL_RESULT_SUCCESS)
          {
            printError(Service, "Send ZGP Leave failed with SZL_RESULT_t = 0x%02X\n", result);
          }

        /* Free up malloced params */
        APP_MEM_GLUE_FREE(gpCmdReq, appMain_GetServiceId(Service));
    }   
    return result;
}
/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/


void appGp_Init(void)
{
  memset(gpdInfo, 0, sizeof(gpdInfo));
}

void appGp_CommsReceived(zabService* Service, unsigned32 SrcId)
{
  int i;
  unsigned32 timeNow;
  osTimeGetSeconds(Service, &timeNow);

  /* First try to find existing device */
  for (i = 0; i < M_MAX_GPDS; i++)
    {
      if (gpdInfo[i].SrcId == SrcId)
        {
          if ( (timeNow - gpdInfo[i].LastCommsTime) > gpdInfo[i].LongestTimeBetweenComms)
            {
              gpdInfo[i].LongestTimeBetweenComms = timeNow - gpdInfo[i].LastCommsTime;
            }
          gpdInfo[i].LastCommsTime = timeNow;
          gpdInfo[i].PacketCount++;
          
           /* decommissionig mode enabled */
      if(appConfig_Config.greenPowerDecommissioning == zab_true) 
      {
        if(gpdInfo[i].LeaveOnGoing == zab_false)
        {
            printApp(Service, "appZcl: should decommission SrcAddr = %x\n",
                    SrcId); 
            SZL_Addresses_t leave_adress;
            leave_adress.AddressMode = SZL_ADDRESS_MODE_GP_SOURCE_ID;
            leave_adress.Addresses.GpSourceId.SourceId = SrcId;

            if(zgp_leave_cmd(Service,leave_adress) == SZL_RESULT_SUCCESS)
            {
              gpdInfo[i].LeaveOnGoing = zab_true;
              gpdInfo[i].LastLeaveTime = timeNow;
            }
        }
        else
        {
           if((timeNow-gpdInfo[i].LastLeaveTime) > gpdInfo[i].LongestTimeBetweenComms)
           {
              gpdInfo[i].LeaveOnGoing = zab_false; 
           }
            
        }
          
      }

          return;
        }
    }

  /* If not found, add it if possible */
  for (i = 0; i < M_MAX_GPDS; i++)
    {
      if (gpdInfo[i].SrcId == 0)
        {
          printApp(Service, "appGp_GpdCommissionedNtf_Handler: Device (SrcId 0x%08X) added by comms\n", SrcId);
          gpdInfo[i].SrcId = SrcId;
          gpdInfo[i].LastCommsTime = timeNow;
          gpdInfo[i].LongestTimeBetweenComms = 0;
          gpdInfo[i].PacketCount = 1;
          gpdInfo[i].LeaveOnGoing = zab_false;
          gpdInfo[i].LastLeaveTime = 0;
          return;
        }
    }
}

/******************************************************************************
 * Process a Commissioning Notification
 * This is called when a GPD wishes to join the network.
 * The application should analyse the Params then decide to accept or reject
 * the GPD.
 * Return:
 *  - szl_true to accept
 *  - szl_false to reject
 ******************************************************************************/
szl_bool appGp_GpdCommissioningNtf_Handler(zabService* Service, SZL_GP_GpdCommissioningNtfParams_t* Params, SZL_GP_COMMISSIONING_KEY_MODE_t* KeyMode)
{
  szl_bool gpdAccepted = szl_false;
  szl_uint8 index;
  
  szl_bool gpdWhiteListOk = szl_true;
  szl_uint8 gpWhiteListElement;

  if (Params->Address.AddressMode == SZL_ADDRESS_MODE_GP_SOURCE_ID)
    {

      /* Only accept GPDs if the global enable is on */
      if (appConfig_Config.greenPowerAcceptGpdCommissioning == zab_true)
        {
          /* check white list */
          if(appConfig_Config.greenPowerFilterByWhiteList > 0)
          {
            gpdWhiteListOk = szl_false;
            for(gpWhiteListElement=0;gpWhiteListElement<appConfig_Config.greenPowerFilterByWhiteList;gpWhiteListElement)
            {
              if(appConfig_Config.greenPowerWhiteList[gpWhiteListElement] == Params->Address.Addresses.GpSourceId.SourceId)
              {
                gpdWhiteListOk = szl_true;
              }
            }
          }
          
          /* Apply model and source id filters */
          if ( ( (appConfig_Config.greenPowerFilterByModelId == 0xFFFF) ||
                 (appConfig_Config.greenPowerFilterByModelId == Params->ManufacturerModelId) ) &&
               ( (appConfig_Config.greenPowerFilterBySourceId == 0xFFFFFFFF) ||
                 (appConfig_Config.greenPowerFilterBySourceId == Params->Address.Addresses.GpSourceId.SourceId) ) && gpdWhiteListOk )
            {
              /* Do fancy stuff for testing */
              switch(appConfig_Config.greenPowerKeyMode)
                {
                  /* GPD can always do no security */
                  case SZL_GP_COMMISSIONING_KEY_MODE_NO_SECURITY:
                    *KeyMode = SZL_GP_COMMISSIONING_KEY_MODE_NO_SECURITY;
                    gpdAccepted = szl_true;
                    break;

                  /* GPD can do shared key if options.GpSecurityKeyReq is set */
                  case SZL_GP_COMMISSIONING_KEY_MODE_SHARED:
                    if (Params->Options.GpSecurityKeyReq)
                      {
                        *KeyMode = SZL_GP_COMMISSIONING_KEY_MODE_SHARED;
                        gpdAccepted = szl_true;
                      }
                    break;

                  /* GPD can do OOB key if ExtendedOptions.GpdKeyPresent is set and KeyType is OOB */
                  case SZL_GP_COMMISSIONING_KEY_MODE_OOB:
                    if ( Params->ExtendedOptions.GpdKeyPresent &&
                         (Params->ExtendedOptions.KeyType == SZL_GP_SECURITY_KEY_TYPE_OUT_OF_BOX) )
                      {
                        *KeyMode = SZL_GP_COMMISSIONING_KEY_MODE_OOB;
                        gpdAccepted = szl_true;
                      }
                    break;

                  case SZL_GP_COMMISSIONING_KEY_MODE_DEFAULT:
                    *KeyMode = SZL_GP_COMMISSIONING_KEY_MODE_DEFAULT;
                    gpdAccepted = szl_true;
                    break;

                  default:
                    gpdAccepted = szl_false;
                }
            }
          else
            {
              printApp(Service, "Rejecting GPD due to model/source ID filter:\n");
              printApp(Service, "  - Model ID Filter = 0x%04X (0xFFFF = All), GPD = 0x%04X\n",
                       appConfig_Config.greenPowerFilterByModelId,
                       Params->ManufacturerModelId);
              printApp(Service, "  - Source ID Filter = 0x%08X (0xFFFFFFFF = All), GPD = 0x%08X\n",
                       appConfig_Config.greenPowerFilterBySourceId,
                       Params->Address.Addresses.GpSourceId.SourceId);
            }
        }
      else
        {
          printApp(Service, "Rejecting GPD as global accept disabled (greenPowerAcceptGpdCommissioning)\n");
        }

      printApp(Service, "appGp_GpdCommissioningNtf_Handler: New GPD Commissioning Request: SrcId = 0x%08X, DeviceId = 0x%02X, ManId = 0x%04X, ManModelId = 0x%04X - %s\n",
               Params->Address.Addresses.GpSourceId.SourceId,
               Params->DeviceId,
               Params->ManufacturerId,
               Params->ManufacturerModelId,
               (gpdAccepted == szl_true) ? "ACCEPTED" : "REJECTED");

      printApp(Service, "      Options: MacSNCap = %d, RxOn = %d, AppInfo = %d, PanReq = %d, KeyReq = %d, Fixed = %d, ExtOpt = %d\n",
               Params->Options.MacSeqNumCap,
               Params->Options.RxOnCap,
               Params->Options.AppInfoPresent,
               Params->Options.PanIdRequest,
               Params->Options.GpSecurityKeyReq,
               Params->Options.FixedLocation,
               Params->Options.ExtOptionsPresent);
      printApp(Service, "      ExtOpts: SecLvlCap = 0x%01X, KeyType = 0x%01X, KeyPres = %d, KeyEnc = %d, OutCntPres = %d\n",
               Params->ExtendedOptions.SecurityLevelCapabilities,
               Params->ExtendedOptions.KeyType,
               Params->ExtendedOptions.GpdKeyPresent,
               Params->ExtendedOptions.GpdKeyEncryption,
               Params->ExtendedOptions.GpdOutgoingCounterPresent);
      printApp(Service, "      AppInfo: ManIdPres = %d, ModIdPres = %d, CmdListPres = %d, ClusterListPres = %d\n",
               Params->AppInfo.ManufacturerIdPresent,
               Params->AppInfo.ModelIdPresent,
               Params->AppInfo.GpdCommandsPresent,
               Params->AppInfo.ClusterListPresent);

      printApp(Service, "      Commands Supported (%d) = ", Params->GpdCommandIdListLength);
      for(index = 0; index < Params->GpdCommandIdListLength; index++)
        {
          printApp(Service, "0x%02X, ", Params->GpdCommandIdList[index]);
        }
      printApp(Service, "\n");

      printApp(Service, "      Server Clusters (%d) = ", Params->ServerClusterListLength);
      for(index = 0; index < Params->ServerClusterListLength; index++)
        {
          printApp(Service, "0x%04X, ", Params->ServerClusterList[index]);
        }
      printApp(Service, "\n");

      printApp(Service, "      Client Clusters (%d) = ", Params->ClientClusterListLength);
      for(index = 0; index < Params->ClientClusterListLength; index++)
        {
          printApp(Service, "0x%04X, ", Params->ClientClusterList[index]);
        }
      printApp(Service, "\n");
    }
  return gpdAccepted;
}

/******************************************************************************
 * Process a Commissioned Notification
 * This is called when a GPD has successfully been commissioned in to the network.
 ******************************************************************************/
void appGp_GpdCommissionedNtf_Handler(zabService* Service, SZL_GP_GpdCommissionedNtfParams_t* Params)
{
  int i;
  unsigned32 timeNow;
  osTimeGetSeconds(Service, &timeNow);

  if (Params->Address.AddressMode == SZL_ADDRESS_MODE_GP_SOURCE_ID)
    {

      if (Params->Event == ZGP_COMMISSIONED_EVENT_COMMISSIONED)
        {
          printApp(Service, "appGp_GpdCommissionedNtf_Handler: GPD Commissioned: SrcId = 0x%08X, DeviceId = 0x%02X\n",
                   Params->Address.Addresses.GpSourceId.SourceId ,
                   Params->DeviceId);

          /* Test app maintains a table to GPDs to do some command tracking.
           * First see if it already exists */
          for (i = 0; i < M_MAX_GPDS; i++)
            {
              if (gpdInfo[i].SrcId == Params->Address.Addresses.GpSourceId.SourceId)
                {
                  printApp(Service, "appGp_GpdCommissionedNtf_Handler: Device already active\n");
                  return;
                }
            }

          /* If it doesn't already exist try to add it */
          for (i = 0; i < M_MAX_GPDS; i++)
            {
              if (gpdInfo[i].SrcId == 0)
                {
                  printApp(Service, "appGp_GpdCommissionedNtf_Handler: Device added\n");
                  gpdInfo[i].SrcId = Params->Address.Addresses.GpSourceId.SourceId;
                  gpdInfo[i].DeviceId = Params->DeviceId;
                  gpdInfo[i].LastCommsTime = timeNow;
                  gpdInfo[i].LongestTimeBetweenComms = 0;
                  gpdInfo[i].PacketCount = 0;

                  return;
                }
            }

          printApp(Service, "appGp_GpdCommissionedNtf_Handler: WARNING GPD table full!\n");
        }
      else if (Params->Event == ZGP_COMMISSIONED_EVENT_DECOMMISSIONED)
        {
          printApp(Service, "appGp_GpdCommissionedNtf_Handler: GPD Decommissioned: SrcId = 0x%08X, DeviceId = 0x%02X\n",
                   Params->Address.Addresses.GpSourceId.SourceId,
                   Params->DeviceId);

          /* Delete it if it exists */
          for (i = 0; i < M_MAX_GPDS; i++)
            {
              if (gpdInfo[i].SrcId == Params->Address.Addresses.GpSourceId.SourceId)
                {
                  printApp(Service, "appGp_GpdCommissionedNtf_Handler: Device deleted\n");
                  memset(&gpdInfo[i], 0, sizeof(gpdInfo_t));
                }
            }
        }
      else
        {
          printApp(Service, "appGp_GpdCommissionedNtf_Handler: ERROR - Unknown Event 0x%02X\n",
                   (unsigned8)Params->Event);
        }
    }
}

#endif // APP_CONFIG_ENABLE_GREEN_POWER
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/