#ifndef _SZL_TYPES_H_
#define _SZL_TYPES_H_

#include "szl_external.h"
#include "osTypes.h"

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef szl_true
  #define szl_false zab_false
  #define szl_true zab_true
#endif

#if (szl_true != 1)
#error "szl_true must be 1"
#endif

#ifndef MAX
#define MAX(a,b)      (( (a) > (b) )? a : b)
#endif

#ifndef MIN
#define MIN(a,b)      (( (a) > (b) )? b : a)
#endif

// 24 bit is same size as 32
#define szl_uint24 szl_uint32

typedef struct
{
    szl_uint16 lo;
    szl_uint8  hi;
} le_uint24;

typedef struct
{
    szl_uint32 lo;
    szl_uint8  hi;
} le_uint40;

typedef struct
{
    szl_uint32 lo;
    szl_uint16 hi;
} le_uint48;

typedef struct
{
    szl_uint32 lo;
    szl_uint16 hi_lo;
    szl_uint8  hi_hi;
} le_uint56;

typedef struct
{
    szl_uint32 lo;
    szl_uint32 hi;
} le_uint64;

#if !SUPPORTS_64_BIT_DATATYPES

typedef struct
{
#ifdef SZL_BIG_ENDIANNESS
   szl_uint16 reserved;
   szl_uint8  reserved2;
   szl_uint8  hi;
   szl_uint32 lo;
#else
   szl_uint32 lo;
   szl_uint8 hi;
   szl_uint8 reserved2;
   szl_uint16 reserved;
#endif
} szl_uint40;

typedef struct
{
#ifdef SZL_BIG_ENDIANNESS
   szl_uint16 reserved;
   szl_uint16 hi;
   szl_uint32 lo;
#else
   szl_uint32 lo;
   szl_uint16 hi;
   szl_uint16 reserved;
#endif
} szl_uint48;

typedef struct
{
#ifdef SZL_BIG_ENDIANNESS
   szl_uint8  reserved;
   szl_uint8  hi_hi;
   szl_uint16 hi_lo;
   szl_uint32 lo;
#else
   szl_uint32 lo;
   szl_uint16 hi_lo;
   szl_uint8  hi_hi;
   szl_uint8  reserved;
#endif
} szl_uint56;

typedef struct
{
#ifdef SZL_BIG_ENDIANNESS
   szl_uint32 hi;
   szl_uint32 lo;
#else
   szl_uint32 lo;
   szl_uint32 hi;
#endif
} szl_uint64;

typedef struct
{
#ifdef SZL_BIG_ENDIANNESS
   szl_int32 hi;
   szl_uint32 lo;
#else
   szl_uint32 lo;
   szl_int32 hi;
#endif
} szl_int64;

#else
  // 40 bit is same as 64
  typedef szl_int64 szl_int40;
  typedef szl_uint64 szl_uint40;
  // 48 bit is same as 64
  typedef szl_int64 szl_int48;
  typedef szl_uint64 szl_uint48;
  // 56 bit is same as 64
  typedef szl_int64 szl_int56;
  typedef szl_uint64 szl_uint56;
#endif /* !SUPPORTS_64_BIT_DATATYPES */

#ifdef __cplusplus
}
#endif

#endif /* _SZL_TYPES_H_ */
