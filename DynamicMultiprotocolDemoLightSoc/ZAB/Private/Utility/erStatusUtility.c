/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file provides function to manage status.
 *
 *   Almost every ZAB function has as a first parameter type called Status.
 *   This is similar to a function return code value, but much more comprehensive
 *   for error detection and recovery.
 *   Once Status is set with an error, it can not be overwritten with another error
 *   until it is cleared. All other processing shall be suspended until the error
 *   is processed and cleared. The service code always checks the status at function
 *   entry and if there is an error, it will return immediately.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Cam Williams
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.00.02  30-Oct-13   MvdB   Remove erStatusCreate() as it just duplicates erStatusClear()
 * 002.000.001  03-Feb-15   MvdB   ARTF104099: Add service pointer to status for print function for multi instance apps.
 * 002.002.020  18-Mar-16   MvdB   Remove unused SZL_ERROR_UNKNOWN_DATA_TYPE
 * 002.002.038  12-Jan-17   MvdB   ARTF113872: Add ZAB_ERROR_DEVICE_TYPE_INVALID
 * 002.002.045  26-Jan-17   MvdB   Error tidy up: Remove unused errors, update descriptions, merge silabs and ti errors into common vendor codes
 *****************************************************************************/

#include "zabCoreService.h"
#include "erStatusUtility.h"


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Clear an error
 ******************************************************************************/
void erStatusClear( erStatus* Status, void* Service )
{
  Status->Error = ZAB_SUCCESS;
  Status->Service = (void*)Service;
}

/******************************************************************************
 * Set an error
 ******************************************************************************/
void erStatusSet( erStatus* Status, unsigned16 Error )
{
  if (Status == NULL)
    {
      return;
    }
  if ((Status->Error == ZAB_SUCCESS) && (Error != ZAB_SUCCESS))  // setting error
  {
    Status->Error = Error;

#ifdef ER_STATUS_USER_SET
    erStatusUserSet( Status, Error ); // call back to user
#endif
  }
}

/******************************************************************************
 * Get a string description of an Error
 ******************************************************************************/
char* erStatusUtility_GetErrorString(zabError errorNumber)
{
#ifdef ER_ENABLE_VERBOSE_ERROR_DESCRIPTIONS
  switch(errorNumber)
    {
      /*** General Errors ***/
      case ER_ERROR_BUFFER_OVERFLOW:                  return "Buffer overflow";

      /*** OS Errors ***/
      case OS_ERROR_MEM_NONE:                         return "Malloc failed - out of memory";
      case OS_ERROR_MEM_INVALID:                      return "Invalid pointer passed to memory free/set/compare function";

      /*** Service Errors ***/
      case SRV_ERROR_ASK_NO_ITEM:                     return "Service: Failed to get asked item";

      /*** SAP Errors ***/
      case SAP_ERROR_NULL_PTR:                        return "SAP: Null pointer";
      case SAP_ERROR_ACCESS_OVERFLOW:                 return "SAP: Insufficient sapAccessStruct";
      case SAP_ERROR_MSG_NOT_AVAILABLE:               return "SAP: No message buffer available";
      case SAP_ERROR_MSG_USE_OVERFLOW:                return "SAP: Message use overflow";
      case SAP_ERROR_MSG_USE_UNDERFLOW:               return "SAP: Message use underflow";
      case SAP_ERROR_MSG_INVALID_LENGTH:              return "SAP: Invalid message data length";

      /*** ZAB Errors ***/
      case ZAB_ERROR_CFG_INVALID:                     return "Error in config or asked value";
      case ZAB_ERROR_ACTION_INVALID:                  return "Invalid action";
      case ZAB_ERROR_ASKED_VALUE_INVALID:             return "Ask returned an invalid value";

      /*** Message Errors ***/
      case ZAB_ERROR_MSG_TYPE_INVALID:                return "Internal message type invalid";
      case ZAB_ERROR_MSG_APP_INVALID:                 return "Internal message application invalid";
      case ZAB_ERROR_MSG_PARAM_INVALID:               return "Internal message parameter invalid";

      /*** Vendor Layer Errors ***/
      case ZAB_ERROR_VENDOR_NULL:                     return "Serial port access failed";
      case ZAB_ERROR_VENDOR_SENDING:                  return "Vendor tx failed";
      case ZAB_ERROR_VENDOR_INVALID_OPEN_STATE:       return"Invalid open state";
      case ZAB_ERROR_INVALID_NETWORK_STATE:           return "Invalid network state";
      case ZAB_ERROR_VENDOR_NETWORK_ACTION_IN_PROGRESS: return "Network action already in progress";
      case ZAB_ERROR_VENDOR_ERASE_NETWORK_SETTINGS_FAILED: return"Vendor failed to erase network settings";
      case ZAB_ERROR_VENDOR_FORM_NETWORK_FAILED:      return "Vendor failed to form a network";
      case ZAB_ERROR_VENDOR_JOIN_NETWORK_FAILED:      return "Vendor failed to join a network";
      case ZAB_ERROR_VENDOR_GO_TO_BOOTLOADER_FAILED:  return "The vendor failed to change to the bootloader";
      case ZAB_ERROR_VENDOR_UNKNOWN_NETWORK_PROCESSOR: return "The vendor cannot recognise the network processor";
      case ZAB_ERROR_VENDOR_INVALID_CLONE_STATE:      return "Invalid clone state";

      /*** ZigBee Message Errors ***/
      case ZAB_ERROR_MSG_APS_INVALID:                 return "APS message not properly formed";
      case ZAB_ERROR_ACTION_FAILED:                   return "Action failed";

      /*** More Vendor Layer Errors ***/
      case ZAB_ERROR_VENDOR_MSG_INVALID:              return "Vendor message not property formed";
      case ZAB_ERROR_VENDOR_BUSY:                     return "Vendor network processor link busy";
      case ZAB_ERROR_VENDOR_TIMEOUT:                  return "Timeout in vendor/network processor communications";
      case ZAB_ERROR_VENDOR_TYPE_INVALID:             return "Invalid Vendor message type";
      case ZAB_ERROR_VENDOR_REQ_INVALID:              return "Invalid Vendor request";
      case ZAB_ERROR_VENDOR_PARSE:                    return "Error in parsing Vendor message";
      case ZAB_ERROR_VENDOR_DATA_CONFIRM_ERROR:       return "Error in AF Data Confirm";
      case ZAB_ERROR_VENDOR_NWK_PROC_RESET:           return "Network processor reset";

      /*** Serial Glue Errors Errors ***/
      case ZAB_ERROR_SERIAL_GLUE_ACTION_TIMEOUT:      return "Serial glue action timed out";

      /*** Bootloader Errors ***/
      case ZAB_ERROR_BOOTLOADER_VERIFY_FAILED:        return "Firmware update: Verify failed";
      case ZAB_ERROR_BOOTLOADER_WRITE_FAILED:         return "Firmware update: Write failed";
      case ZAB_ERROR_BOOTLOADER_IMAGE_INVALID:        return  "Firmware update: Image invalid";
      case ZAB_ERROR_BOOTLOADER_ACTION_IN_PROGRESS:   return  "Firmware update: Action in progress";

      /*** Action Errors ***/
      case ZAB_ERROR_ACTION_KEY_SEQ_NUMBER_INVALID:   return "Action: Key seq num invalid";

      /*** Clone Errors ***/
      case ZAB_ERROR_CLONE_STORE_CALLBACK_INVALID:    return "Clone: Store callback invalid";
      case ZAB_ERROR_CLONE_RETRIEVE_CALLBACK_INVALID: return "Clone: Retrieve callback invalid";
      case ZAB_ERROR_CLONE_COMMAND_TIMEOUT:           return "Clone: Command timeout";

      /* SZL Errors */
      case SZL_ERROR_PLUGIN_NOT_UNREGISTERED:         return "SZL plugin was not unregistered";

      default: return "No verbose translation";
    }
#else
  return "";
#endif
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
