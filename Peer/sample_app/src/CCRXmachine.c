/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/******************************************************************************
* File Name     : CCRXmachine.c
* Device(s)     : RX631
* Tool-chain    : KPIT GNURX-ELF 15.01
* H/W platform  : G-CPX / EU-CPX2 / G-CPX3
* Description   : Sample software
******************************************************************************/
#include <stdint.h>
#include <stddef.h>
#include "CCRXmachine.h"

/******************************************************************************
Macro definitions
******************************************************************************/
#define HEAPSIZE (0x0000)

/******************************************************************************
Typedef definitions
******************************************************************************/
typedef union {
    int64_t dummy ;
    int8_t heap[HEAPSIZE];
} heap_type_t;

/******************************************************************************
Exported global variables
******************************************************************************/
/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/
/******************************************************************************
Private global variables and functions
******************************************************************************/
static heap_type_t heap_area ;
static signed char *pbrk=(signed char *)&heap_area;

/******************************************************************************
Functions
******************************************************************************/

signed long max(signed long data1, signed long data2)
{
    return (data1 > data2)? data1 : data2;
}

signed long min(signed long data1, signed long data2)
{
    return (data1 < data2)? data1 : data2;
}

void xchg(signed long *data1, signed long *data2)
{
    signed long temp = *data1;
    *data1 = *data2;
    *data2 = temp;
}

long long rmpab(long long init, unsigned long count, signed char *addr1, signed char *addr2)
{
    long long result = init;
    unsigned long index;
    for(index = 0; index < count; index++)
    {
        result += addr1[index] * addr2[index];
    }
    return result;
}

long long rmpaw(long long init, unsigned long count, short *addr1, short *addr2)
{
    long long result = init;
    unsigned long index;
    for(index = 0; index < count; index++)
    {
        result += addr1[index] * addr2[index];
    }
    return result;
}

long long rmpal(long long init, unsigned long count, long *addr1, long *addr2)
{
    long long result = init;
    unsigned long index;
    for(index = 0; index < count; index++)
    {
        result += addr1[index] * addr2[index];
    }
    return result;
}

unsigned long rolc(unsigned long data)
{
    __asm("rolc %0":"=r"(data) : "r"(data):); 
    return data;
}

unsigned long rorc(unsigned long data)
{
    __asm("rorc %0":"=r"(data) : "r"(data):);
    return data;
}

unsigned long rotl(unsigned long data, unsigned long num)
{
    __asm("rotl %1, %0":"=r"(data) : "r"(num),"0"(data) :); 
    return data;
}

unsigned long rotr(unsigned long data, unsigned long num)
{
    __asm("rotr %1, %0":"=r"(data) : "r"(num),"0"(data) :); 
    return data;
}

void set_ipl(signed long level)
{
    unsigned int psw = __builtin_rx_mvfc(0x0);
    psw &= 0xF0FFFFFF;
    psw |= ((level << 24) & 0x0F000000);
    __builtin_rx_mvtc(0x0, psw);
}

unsigned char get_ipl(void)
{
    unsigned int psw = __builtin_rx_mvfc(0x0);
    return (psw & 0x0F000000) >> 24;
}

signed long long emul(signed long data1, signed long data2)
{
    return ((signed long long)data1) * ((signed long long)data2);
}

unsigned long long emulu(unsigned long data1, unsigned long data2)
{
    return ((unsigned long long)data1) * ((unsigned long long)data2);
}

void chg_pmusr(void)
{
    __asm("MVFC   PSW,R1" : : :"r1");
    __asm("OR     #00100000h,R1" : : :"r1");
    __asm("PUSH.L R1" : : :"r1");
    __asm("MVFC   PC,R1" : : :"r1");
    __asm("ADD    #10,R1" : : :"r1");
    __asm("PUSH.L R1" : : :"r1");
    __asm("RTE");
}

void set_acc(signed long long data)
{
    __builtin_rx_mvtachi(data >> 32);
    __builtin_rx_mvtaclo(data & 0xFFFFFFFF);
}

signed long long get_acc(void)
{
    signed long long result = ((signed long long)__builtin_rx_mvfachi()) << 32;
    result |= (((signed long long)__builtin_rx_mvfacmi()) << 16) & 0xFFFF0000;
    return result;
}

void setpsw_i(void)
{
    unsigned int psw = __builtin_rx_mvfc(0x0);
    psw |= 0x00010000;
    __builtin_rx_mvtc(0x0, psw);
}

void clrpsw_i(void)
{
    unsigned int psw = __builtin_rx_mvfc(0x0);
    psw &= 0xFFFEFFFF;
    __builtin_rx_mvtc(0x0, psw);
}

long macl(short* data1, short* data2, unsigned long count)
{
    unsigned long index;
    __builtin_rx_mullo(0, 0);
    for(index = 0; index < count; index++)
    {
        __builtin_rx_maclo(data1[index], data2[index]);
        __builtin_rx_machi(data1[index], data2[index]);
    }
    return (long)(get_acc() >> 16);
}

short macw1(short* data1, short* data2, unsigned long count)
{
    unsigned long index;
    __builtin_rx_mullo(0, 0);
    for(index = 0; index < count; index++)
    {
        __builtin_rx_maclo(data1[index], data2[index]);
        __builtin_rx_machi(data1[index], data2[index]);
    }
    __builtin_rx_racw(1);
    return (short)(get_acc() >> 16);
}

short macw2(short* data1, short* data2, unsigned long count)
{
    unsigned long index;
    __builtin_rx_mullo(0, 0);
    for(index = 0; index < count; index++)
    {
        __builtin_rx_maclo(data1[index], data2[index]);
        __builtin_rx_machi(data1[index], data2[index]);
    }
    __builtin_rx_racw(2);
    return (short)(get_acc() >> 16);
}

void _CALL_INIT(void)
{
}
 
void _CALL_END(void)
{
}

void nop(void)
{
    __asm("nop");
}

extern unsigned int _SECTOP_FL_PRG,_SECEND_FL_PRG,_SECTOP_PFL_CODE,_SECTOP_FL_BSS,_SECEND_FL_BSS,data,mdata,edata,bss,ebss;
void _INITSCT(void)
{

    unsigned int *src,*dst;
    
    src = &_SECTOP_PFL_CODE;
    for(dst = &_SECTOP_FL_PRG;dst < &_SECEND_FL_PRG;){
        *(dst++) = *(src++);
    }

    src = &mdata;
    for(dst = &data;dst < &edata;){
        *(dst++) = *(src++);
    }

    for(dst = &bss;dst < &ebss;){
        *(dst++) = 0;
    }

    for(dst = &_SECTOP_FL_BSS;dst < &_SECEND_FL_BSS;){
        *(dst++) = 0;
    }
}

signed char *sbrk(size_t size)
{
    signed char *ptmp;
    if((pbrk+size) > (heap_area.heap+HEAPSIZE)){
        ptmp = (signed char *)-1;
    }else{
        ptmp = pbrk;
        pbrk += size;
    }
    return ptmp;
}


