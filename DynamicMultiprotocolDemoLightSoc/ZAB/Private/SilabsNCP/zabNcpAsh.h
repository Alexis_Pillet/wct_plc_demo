/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the ASH (A-Synchronous Host) service
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.002  29-Jun-16   MvdB   Original
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/

#ifndef __ZAB_NCP_ASH_H__
#define __ZAB_NCP_ASH_H__

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      *****************************
 *                 *****     EXTERNAL VARIABLES      *****
 *                      *****************************
 ******************************************************************************/

/* Some flags for causing errors and confirming they are handled during development testing */
#ifdef ZAB_DEVELOPER_TEST_MODE
zab_bool zabNcpAsh_DropNextIncomingFrame;
zab_bool zabNcpAsh_BreakNextIncomingCrc;
zab_bool zabNcpAsh_DropNextOutgoingingFrame;
zab_bool zabNcpAsh_BreakNextOutgoingCrc;
zab_bool zabNcpAsh_BreakAllOutgoingCrc;
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise the ASH service
 ******************************************************************************/
void zabNcpAsh_InitService(erStatus* Status, zabService* Service);

/******************************************************************************
 * Send an ASH Reset
 ******************************************************************************/
void zabNcpAsh_Reset(erStatus* Status, zabService* Service);

/******************************************************************************
 * Send ASH Data
 ******************************************************************************/
void zabNcpAsh_Data(erStatus* Status, zabService* Service, unsigned8 BufferLength, unsigned8* BufferData, zab_bool Resend);

/******************************************************************************
 * Process incoming ASH messages
 ******************************************************************************/
void zabNcpAsh_ProcessMessageIn(erStatus* Status, zabService* Service, sapMsg* Message);

#ifdef __cplusplus
}
#endif

#endif // __ZAB_NCP_ASH_H__
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/