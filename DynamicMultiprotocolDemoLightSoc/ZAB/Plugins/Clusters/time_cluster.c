/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the Time Cluster.
 *   This plugin is originally designed for ComX (partner).
 * 
 *   It supports:
 *    - Operation on a single endpoint
 *    - Mandatory attributes only
 *    - Operation as a time master only. Time MAY NOT be set accross ZigBee.
 *
 * USAGE:
 *   Usage of this plugin is very simple:
 *    1. Initialise it with SzlPlugin_TimeCluster_Init(), specifying the 
 *       endpoint and a function for the plugin to get the UTC Time
 * 
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         23-Apr-14   MvdB   Original
 *    2         20-Jul-15   MvdB   ARTF134686: Update allocation method to be generic and protect against multiple initialisation
 * 002.001.006  27-Jul-15   MvdB   ARTF134686: Standardise on SZL_RESULT_t for plugin init return value
 *                                 ARTF138318: Improve plugin destroy
 *****************************************************************************/

#include <stdio.h>
#include <stddef.h>
#include <stddef.h>
#include <string.h>
#include "zabCoreService.h"
#include "zabCorePrivate.h"
#include "szl.h"
#include "szl_plugin.h"

#include "time_cluster.h"

/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Cluster ID */
#define ZCL_CLUSTER_ID_TIME     0x000A

/* Undefined value of time */
#define M_UTC_TIME_UNDEFINED 0xFFFFFFFF

/* Attributes Identifiers */
typedef enum
{
    ATTRID_TIME                         = 0x0000,
    ATTRID_TIME_STATUS                  = 0x0001,
} TIME_CLUSTER_ATTRIBUTES;

/* Parameters for the service */
typedef struct
{
  szl_uint8 endpoint;
  SzlPlugin_TimeCluster_UtcTime_CB_t Callback;
}SzlPlugin_Time_Params_t;
#define SzlPlugin_Time_Params_t_SIZE (sizeof(SzlPlugin_Time_Params_t))


static SZL_RESULT_t timeReadCallback(zabService* Service, SZL_EP_t Endpoint, szl_bool ManufacturerSpecific, szl_uint16 ClusterID, szl_uint16 AttributeID, void* Data, szl_uint8* DataLength);

/* 
 * The set of attributes for the cluster.
 * This const data is used to initialise the majority of the attribute table.
 * Once it is copied into RAM, Endpoint and Data Access pointers are set for each attribute.
 * 
 * Read only attributes use direct access by SZL.
 * Writable attributes use Read/Write callbacks, as the need the write callback to notify the plugin that
 *   a write is happening, so it can notify the app.
 * 
 * New attributes must be added here.
 */
const SZL_DataPoint_t timeDatapointConfig[] = {
  {ZCL_CLUSTER_ID_TIME, ATTRID_TIME,          0, SZL_ZB_DATATYPE_UTC,     4, {DP_METHOD_CALLBACK, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.Callback.Read = timeReadCallback, .Callback.Write = NULL}},
  {ZCL_CLUSTER_ID_TIME, ATTRID_TIME_STATUS,   0, SZL_ZB_DATATYPE_BITMAP8, 1, {DP_METHOD_CALLBACK, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.Callback.Read = timeReadCallback, .Callback.Write = NULL}}
};
#define TIME_M_NUM_ATTRIBUTES_PER_ENDPOINT ( sizeof(timeDatapointConfig) / sizeof(SZL_DataPoint_t))




/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * UTC time read callback.
 * Its much easier to jus task for UTC time when its needed, rather than require
 * it to be incremented every second.
 ******************************************************************************/
static SZL_RESULT_t timeReadCallback(zabService* Service, SZL_EP_t Endpoint, szl_bool ManufacturerSpecific, szl_uint16 ClusterID, szl_uint16 AttributeID, void* Data, szl_uint8* DataLength)
{
  szl_uint32 UtcTime;
  SzlPlugin_Time_Params_t * pluginParams = NULL;
  
  /* Validate inputs */
  if ( (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_TIME, (void**)&pluginParams) == SZL_RESULT_SUCCESS) ||
       (pluginParams != NULL) )
    {
      if ( (pluginParams->endpoint == Endpoint) &&
           (ManufacturerSpecific == szl_false) &&
           (ClusterID == ZCL_CLUSTER_ID_TIME) &&
           (pluginParams->Callback != NULL) )
        {
          if (AttributeID == ATTRID_TIME)
            {
              *DataLength = 4;
              *(szl_uint32*)Data = M_UTC_TIME_UNDEFINED;
              pluginParams->Callback((szl_uint32*)Data);
              return SZL_RESULT_SUCCESS;
            }
          else if (AttributeID == ATTRID_TIME_STATUS)
            {
              UtcTime = M_UTC_TIME_UNDEFINED;
              pluginParams->Callback(&UtcTime);
              *DataLength = 1;
              if (UtcTime == M_UTC_TIME_UNDEFINED)
                {
                  /* Time InValid:
                   * b0 - Master = 0
                   * b1 - Synchronized Over ZigBee = 0
                   * b2 - MasterZoneDst = 0
                   * b3 - Superseding = 0 */
                  *(szl_uint8*)Data = 0x00;   
                }
              else
                {
                  /* Time Valid:
                   * b0 - Master = 1
                   * b1 - Synchronized Over ZigBee = 0
                   * b2 - MasterZoneDst = 0
                   * b3 - Superseding = 1 */
                  *(szl_uint8*)Data = 0x09;          
                }
              return SZL_RESULT_SUCCESS;
            }
        }
    }
  
  return SZL_RESULT_NOT_FOUND;
}


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Plugin Initialisation
 * This function must be called from the application after the Library has been initialized.
 * 
 * Parameters:
 *  Service: Instance of library
 *  Endpoint: The endpoint on which the cluster will be run
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_TimeCluster_Init(zabService* Service, 
                                        szl_uint8 Endpoint,
                                        SzlPlugin_TimeCluster_UtcTime_CB_t Callback)
{
  SZL_RESULT_t result;
  SzlPlugin_Time_Params_t* pluginParams = NULL;
  SZL_DataPoint_t serverAttributes[TIME_M_NUM_ATTRIBUTES_PER_ENDPOINT];

  /* Validate inputs */
  if ( (Service == NULL) || (Endpoint == 0) || (Callback == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /* Malloc parameters the plugin needs to store and link from the SZL */
  result = SZL_PluginRegister(Service, SZL_PLUGIN_ID_TIME, SzlPlugin_Time_Params_t_SIZE, (void**)&pluginParams);
  if ( (result == SZL_RESULT_SUCCESS) && (pluginParams != NULL) )
    {

      /* Set parameters that are not per endpoint */
      pluginParams->endpoint = Endpoint;
      pluginParams->Callback = Callback;

      /* Init table of attributes to be registered for each endpoint
       * This is mostly the same for each EP, but we set the endpoint number and data pointers differently */
      szl_memcpy( (szl_uint8*) serverAttributes,
                  (szl_uint8*) timeDatapointConfig,
                  sizeof(timeDatapointConfig) );

      serverAttributes[0].Endpoint = Endpoint;
      serverAttributes[1].Endpoint = Endpoint;

      /* Register the cluster attributes */
      result = SZL_AttributesRegister(Service, serverAttributes, TIME_M_NUM_ATTRIBUTES_PER_ENDPOINT);

      /* Keep boolean to show if any register worked, as if it did we must ensure the data it points to is valid */
      if(result == SZL_RESULT_SUCCESS || result == SZL_RESULT_ALREADY_EXISTS)
        {
          printInfo(Service, "PLUGIN TIME: Init Successful\n");
          return SZL_RESULT_SUCCESS;
        }
      else
        {
          /* Failed. Cleanup */
          printError(Service, "PLUGIN TIME: Init failed - Result = 0x%02X\n", result);
          SZL_PluginUnregister(Service, SZL_PLUGIN_ID_TIME);
        }
    }
  
  return result;
}

/******************************************************************************
 * Plugin Destruction
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_TimeCluster_Destroy(zabService* Service)
{
  SZL_RESULT_t result;
  szl_uint8 attr;
  SZL_EP_t endpoint;
  SzlPlugin_Time_Params_t* pluginParams = NULL;
  SZL_DataPoint_t serverAttributes[TIME_M_NUM_ATTRIBUTES_PER_ENDPOINT];
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_TIME, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
  
  /* Get the endpoint number */
  endpoint = pluginParams->endpoint;

  /* Init table of attributes to be de-registered for each endpoint
   * This is mostly the same for each EP, but we set the endpoint number differently */
  szl_memcpy(serverAttributes,
             timeDatapointConfig,
             sizeof(timeDatapointConfig));

  /* Set the endpoint number. For deregister we don't care about the access types etc. */
  for (attr = 0; attr < TIME_M_NUM_ATTRIBUTES_PER_ENDPOINT; attr++)
    {
      serverAttributes[attr].Endpoint = endpoint;
    }   

  /* De-Register the cluster attributes.
   * Don't care too much about the result as we will make best effort only */
  result = SZL_AttributesDeregister(Service, serverAttributes, TIME_M_NUM_ATTRIBUTES_PER_ENDPOINT);
  if (result != SZL_RESULT_SUCCESS)
    {
      printError(Service, "PLUGIN TIME: WARNING: Deregister failed for EP 0x%02X with Result 0x%02X\n",
                 endpoint,
                 result);
    }
  
  /* Now unregister the plugin */
  return SZL_PluginUnregister(Service, SZL_PLUGIN_ID_TIME);
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/