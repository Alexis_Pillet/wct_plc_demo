
"""
mac.mac_process
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to manage management of serialDriver & frame generation/management

"""
import serial
import sys
import time
from mac import *
from queue import Queue, Empty
from threading import Thread
from logger import LoggerManager
log = LoggerManager.get_logger(__name__)


class SerialProcess:
    """Serial process to send/receive mac frames"""

    def __init__(self):
        """Initialization of the serial process"""
        self._serial_driver = None
        self._mac_queue = None

    def start(self, queue):
        """Start the serial process"""
        self._mac_queue = queue
        return self._start_serial_driver()

    def stop(self):
        """Stop the serial process"""
        log.info('Clear program & close serialDriver...')
        self._stop_serial_driver()
        log.info('Done!')

    def _stop_serial_driver(self):
        """Stop the serial process"""
        self._serial_driver.stop()
        self._serial_driver.close()

    def _start_serial_driver(self, default_baud_rate=115200, default_rts=None, default_dtr=None, port='COM9'):
        """Start the serial process"""

        # Suppress non-error messages
        quiet = False
        # Show Python traceback on error
        develop = False

        while True:
            try:
                try:
                    port = ask_for_port()
                except KeyboardInterrupt:
                    sys.stderr.write('\n')

                serial_instance = serial.serial_for_url(
                    'spy://' + port + '?file=serial_log.log',
                    default_baud_rate,
                    parity='N',
                    rtscts=False,
                    xonxoff=False,
                    do_not_open=True,
                    timeout=0.05)

                if not hasattr(serial_instance, 'cancel_read'):
                    # enable timeout for alive flag polling if cancel_read is not available
                    serial_instance.timeout = 1

                if default_dtr is not None:
                    if not quiet:
                        sys.stderr.write('--- forcing DTR {}\n'.format('active' if default_dtr else 'inactive'))
                    serial_instance.dtr = default_dtr
                if default_rts is not None:
                    if not quiet:
                        sys.stderr.write('--- forcing RTS {}\n'.format('active' if default_rts else 'inactive'))
                    serial_instance.rts = default_rts
                serial_instance.open()
            except serial.SerialException as e:
                sys.stderr.write('could not open port {!r}: {}\n'.format(port, e))
                if develop:
                    raise
                sys.exit(1)
            else:
                break

        self._serial_driver = SerialDriver(
            serial_instance,
            self._mac_queue)
        self._serial_driver.set_rx_encoding('hexlify')
        self._serial_driver.set_tx_encoding('hexlify')

        if not quiet:
            sys.stderr.write('--- Miniterm on {p.name}  {p.baudrate},{p.bytesize},{p.parity},{p.stopbits} ---\n'.format(
                p=self._serial_driver.serial_instance))

        self._serial_driver.start()
        return self._serial_driver


class MacProcess:
    """Mac process to send/receive mac frames

    Manage the automate of the initialisation of the plc communication
    """

    def __init__(self):
        """Initialization of the mac process"""
        self._serial_process = SerialProcess()
        self._current_state = self.UNINITIALIZED_WAITING_DEVICE_STATE
        self._serial_driver = None
        self._queue = Queue()
        self._network_queue = None
        self._mac_frame_generator = None
        self._device_number = []
        self._mac_process = None
        self._tread_alive = False
        self._automate_function = {  # Association between state and function to execute
            self.UNINITIALIZED_WAITING_DEVICE_STATE: self.process_uninitialized_waiting_device_state,
            self.UNINITIALIZED_WAITING_ACK_CONFIGURATION_STATE:
                self.process_uninitialized_waiting_ack_configuration_state,
            self.UNINITIALIZED_WAITING_STATUS_NONE_STATE: self.process_uninitialized_waiting_status_none_state,
            self.NETWORKING_STATE: self.process_networking_state,
            self.NETWORKED_STATE: self.process_networked_state}

    # State of automate
    UNINITIALIZED_WAITING_DEVICE_STATE = 0
    UNINITIALIZED_WAITING_ACK_CONFIGURATION_STATE = 1
    UNINITIALIZED_WAITING_STATUS_NONE_STATE = 2
    NONE_STATE = 3
    NETWORKING_STATE = 4
    NETWORKED_STATE = 5

    @property
    def network_queue(self):
        return self._network_queue

    @network_queue.setter
    def network_queue(self, network_queue):
        self._network_queue = network_queue

    @property
    def mac_frame_generator(self):
        return self._mac_frame_generator

    @mac_frame_generator.setter
    def mac_frame_generator(self, frame_generator):
        self._mac_frame_generator = frame_generator

    @property
    def current_state(self):
        return self._current_state

    @current_state.setter
    def current_state(self, current_state):
        self._current_state = current_state

    def process_uninitialized_waiting_device_state(self, data):
        """Check if we received an uninitialized state frame

        Args:
            data: date of the received frame
        """
        if is_uninitialized_state_frame(data):
            self._current_state = self.UNINITIALIZED_WAITING_ACK_CONFIGURATION_STATE
            # Sleep for 10 ms => little delay in order that PLC Concentrator process the frame
            time.sleep(0.01)
            self._mac_frame_generator.send_configuration_concentrator()
            log.info('Status - PLC Network UNINITIALIZED')
        else:
            log.error('BAD_FRAME - PLC Network UNINITIALIZED')

    def process_uninitialized_waiting_ack_configuration_state(self, data):
        """Check if we received an Success ack after sending the Configuration Concentrator frame

        Args:
            data: date of the received frame
        """
        if is_ack_success_frame(data):
            self._current_state = self.UNINITIALIZED_WAITING_STATUS_NONE_STATE
            log.info('Status - PLC Network CONFIGURED')
        # It is possible to received a uninitialized device frame at this state
        # that means the concentrator reset => we need to also reset the automate
        elif is_uninitialized_state_frame(data):
            self.process_uninitialized_waiting_device_state(data)
        else:
            log.error('BAD_FRAME - PLC Network CONFIGURED')

    def process_uninitialized_waiting_status_none_state(self, data):
        """Check if we received an none state frame

        Args:
            data: date of the received frame
        """
        if is_none_state_frame(data):
            self._current_state = self.NONE_STATE
            # No action to perform or waiting data for state None
            self.process_none_state()
            log.info('Status - PLC Network NONE')
        # It is possible to received a uninitialized device frame at this state
        # that means the concentrator reset => we need to also reset the automate
        elif is_uninitialized_state_frame(data):
            self.process_uninitialized_waiting_device_state(data)
        else:
            log.error('BAD_FRAME - PLC Network NONE')

    def process_none_state(self):
        """Process of the None State

        At the moment, no action are needed. We go to the next state of the automate
        """
        self._current_state = self.NETWORKING_STATE

    def process_networking_state(self, data):
        """Check if we received an networking state frame

        Args:
            data: date of the received frame
        """
        if is_networking_state_frame(data):
            self._current_state = self.NETWORKED_STATE
            log.info('Status - PLC Network CREATED')
        # It is possible to received a uninitialized device frame at this state
        # that means the concentrator reset => we need to also reset the automate
        elif is_uninitialized_state_frame(data):
            self.process_uninitialized_waiting_device_state(data)
        else:
            log.error('BAD_FRAME - PLC Network CREATED')

    def process_networked_state(self, data):
        """Check if we received an networked state frame

        Args:
            data: date of the received frame
        """
        # It is possible to received a uninitialized device frame at this state
        # that means the concentrator reset => we need to also reset the automate
        if is_uninitialized_state_frame(data):
            self.process_uninitialized_waiting_device_state(data)
        else:
            result = is_networked_state_frame(data)
            if result["is_networked_frame"]:
                log.info('Status - PLC Network RUNNING')
                log.debug('Device number - {}, Mac address - {}'.format(result["device_number"],
                                                                        result["mac_address"].hex()))
                self._device_number.append(result["device_number"])
            else:
                result = is_data_frame(data)
                if result["is_data_frame"]:
                    log.debug('DATA - {} - {}'.format(result["payload"][0], result["payload"][1:].hex()))
                    # Send Data frame to network layout
                    self._network_queue.put({"device_number": result["payload"][0], "frame": result["payload"][1:]})

    def start(self):
        """Start serial process thread/mac generator/ receiver thread"""
        self._serial_driver = self._serial_process.start(self._queue)
        self._mac_frame_generator = MacFrameGenerator(self._serial_driver)
        # start thread to treat frame from serial driver
        self._tread_alive = True
        self._mac_process = Thread(target=self._treat_data, name='treat', args=())
        self._mac_process.daemon = True
        self._mac_process.start()

    def stop(self):
        """Stop serial process thread/mac generator/ receiver thread"""
        self._serial_process.stop()
        self._tread_alive = False
        self._mac_process.join()

    def _treat_data(self):
        """Treat data from serial process"""
        while self._tread_alive:
            try:
                data = self._queue.get(True, 0.1)
            except Empty:
                continue

            self._automate_function[self._current_state](data)
            # log error: if we received one
            result = is_ack_error_frame(data)
            if result["is_error_ack"]:
                log.error('MAC_RECEIVED - {}'.format(Mac.display_ack()[result["ack_status"]]))
            self._queue.task_done()
