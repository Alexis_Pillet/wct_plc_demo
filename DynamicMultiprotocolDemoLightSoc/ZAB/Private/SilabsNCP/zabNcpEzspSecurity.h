/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Security Commands/Responses - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.017  26-Jul-16   MvdB   Support GetKey
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/

#ifndef __ZAB_NCP_EZSP_SECURITY_H__
#define __ZAB_NCP_EZSP_SECURITY_H__

#include "ember-types.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Set Initial Security State Request + Response Handler
 ******************************************************************************/
void zabNcpEzspSecurity_SetInitialState(erStatus* Status, zabService* Service);
void zabNcpEzspSecurity_SetInitialStateResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Get Current Security State Request + Response Handler
 ******************************************************************************/
void zabNcpEzspSecurity_GetCurrentSecurityState(erStatus* Status, zabService* Service);
void zabNcpEzspSecurity_GetCurrentSecurityStateResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Get Key Request + Response Handler
 ******************************************************************************/
void zabNcpEzspSecurity_GetKey(erStatus* Status, zabService* Service, EmberKeyType KeyType);
void zabNcpEzspSecurity_GetKeyResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Add Transient Link Key Request + Response Handler
 ******************************************************************************/
void zabNcpEzspSecurity_AddTransientLinkKey(erStatus* Status, zabService* Service);
void zabNcpEzspSecurity_AddTransientLinkKeyResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

#ifdef __cplusplus
}
#endif

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif