 /******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file provides functions to build and parse a ZNP message.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.00.02  31-Oct-13   MvdB   Tidy up from original
 * 00.00.07.01  03-Nov-14   MvdB   ARTF107076: Replication Get Fails
 *****************************************************************************/

#include "zabZnpService.h"


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/  

/******************************************************************************
 * Create a ZNP Serial SAP Out Message
 * This will be sent over the serial link with SOF and FCS.
 ******************************************************************************/
sapMsg* zabParseZNPCreateRequest( erStatus* Status, zabService *Service, unsigned16 DataLength )
{
  sapMsg* Msg = NULL;

  ER_CHECK_STATUS_ZERO(Status);
  
  Msg = sapMsgAllocateData( Status, zabCoreSapSerial(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, ZNPI_MSG_HEADER_LENGTH + DataLength );
  ER_CHECK_STATUS_NULL_ZERO( Status, Msg );

  sapMsgSetAppType( Status, Msg, ZAB_MSG_APP_ZNP);
  sapMsgSetAppTransactionId(Status, Msg, 0);
  
  return Msg;
}


/******************************************************************************
 * Create a ZNP Serial SAP IN Message
 * This is for received data. Data should be stripped of SOF and FCS, but include
 * the ZNP Length, Cmd[0] and Cmd[1]
 ******************************************************************************/
sapMsg* zabParseZNPCreateResponse( erStatus* Status, zabService *Service, unsigned16 DataLength ) // allocate and init response header from original request
{
  sapMsg* Msg = NULL;

  ER_CHECK_STATUS_ZERO(Status);
  
  Msg = sapMsgAllocateData( Status, zabCoreSapSerial(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, /*ZNPI_MSG_HEADER_LENGTH +*/ DataLength );
  ER_CHECK_STATUS_NULL_ZERO( Status, Msg );

  sapMsgSetAppType( Status, Msg, ZAB_MSG_APP_ZNP);
  sapMsgSetAppTransactionId(Status, Msg, 0);

  return Msg;
}

/******************************************************************************
 * Create and send an Application Error Data SAP IN Message
 * 
 * Note: This function deliberately does not use a status parameter, as it
 * is typically called upon detection of bad status, so using that status will
 * cause the error report to fail!
 * Error reports are "best effort" only. If this function fails then there
 * is no status returned and hence there will never be the opportunity to retry.
 ******************************************************************************/
void zabParseZNP_AppErrorIn(zabService *Service, unsigned8 TransactionId, zabError Error)
{
  erStatus errorStatus;
  sapMsg* Msg = NULL;

  erStatusClear(&errorStatus, Service);
  
  Msg = sapMsgAllocateData( &errorStatus, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, sizeof(zabError));
  ER_CHECK_STATUS_NULL( &errorStatus, Msg );

  sapMsgSetAppType(&errorStatus, Msg, ZAB_MSG_APP_CMD_ERROR);
  sapMsgSetAppTransactionId(&errorStatus, Msg, TransactionId);
  sapMsgCopyAppDataTo(&errorStatus, Msg, (unsigned8*)&Error, sizeof(zabError));
  sapMsgPutFree(&errorStatus, zabCoreSapData(Service), Msg);
}

/******************************************************************************
 * Create a RAW Serial SAP Out Message
 * This will be sent over the serial link with no SOF or FCS.
 * It is intended for flushing serial buffers and should normally not be used.
 ******************************************************************************/
sapMsg* zabParseZNPCreateRaw( erStatus* Status, zabService *Service, unsigned16 DataLength )
{
  sapMsg* Msg = NULL;

  ER_CHECK_STATUS_ZERO(Status);
  
  Msg = sapMsgAllocateData( Status, zabCoreSapSerial(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, /*ZNPI_MSG_HEADER_LENGTH +*/ DataLength);
  ER_CHECK_STATUS_NULL_ZERO( Status, Msg );

  sapMsgSetAppType( Status, Msg, ZAB_MSG_APP_RAW );
  sapMsgSetAppDataLength (Status, Msg, DataLength);
  
  if (erStatusIsError( Status ))
    {
      sapMsgFree( Status, Msg );
      Msg = NULL;
    }
  return Msg;
}

/******************************************************************************
 * Get/Set ZNP Length field of a message
 ******************************************************************************/
void zabParseZNPGetLength( erStatus* Status, sapMsg* Message, unsigned8* Length )
{
  ZAB_CHECK_MSG_ZNP( Status, Message );
  *Length = ZAB_MSG_ZNP( Message )->Length;
}
void zabParseZNPSetLength( erStatus* Status, sapMsg* Message, unsigned8 Length )
{
  ZAB_CHECK_MSG_ZNP( Status, Message );
  ZAB_MSG_ZNP( Message )->Length = Length;
}

/******************************************************************************
 * Get/Set ZNP Type in Cmd[0] field of a message
 ******************************************************************************/
void zabParseZNPGetType( erStatus* Status, sapMsg* Message, ZNPI_TYPE *type )
{
  ZAB_CHECK_MSG_ZNP( Status, Message );
  *type = (ZNPI_TYPE)(ZAB_MSG_ZNP( Message )->Cmd0 & ZNPI_MSG_CMD_0_TYPE_MASK);
}
void zabParseZNPSetType( erStatus* Status, sapMsg* Message, ZNPI_TYPE type )
{
  ZAB_CHECK_MSG_ZNP( Status, Message );
  ZAB_MSG_ZNP( Message )->Cmd0 &= (unsigned8)(~ZNPI_MSG_CMD_0_TYPE_MASK);
  ZAB_MSG_ZNP( Message )->Cmd0 |= (unsigned8)((unsigned8)type  & ZNPI_MSG_CMD_0_TYPE_MASK);
}

/******************************************************************************
 * Get/Set ZNP Subsystem in Cmd[0] field of a message
 ******************************************************************************/
void zabParseZNPSetSubSystem( erStatus* Status, sapMsg* Message, ZNPI_SUBSYSTEM subsystem )
{
  ZAB_CHECK_MSG_ZNP( Status, Message );
  ZAB_MSG_ZNP( Message )->Cmd0 &= (unsigned8)(~ZNPI_MSG_CMD_0_SUBSYSTEM_MASK);
  ZAB_MSG_ZNP( Message )->Cmd0 |= (unsigned8)(subsystem  & ZNPI_MSG_CMD_0_SUBSYSTEM_MASK);
}
void zabParseZNPGetSubSystem( erStatus* Status, sapMsg* Message, ZNPI_SUBSYSTEM *subsystem )
{
  ZAB_CHECK_MSG_ZNP( Status, Message );
  *subsystem = (ZNPI_SUBSYSTEM)(ZAB_MSG_ZNP( Message )->Cmd0 & ZNPI_MSG_CMD_0_SUBSYSTEM_MASK);
}

/******************************************************************************
 * Get/Set ZNP Command ID in Cmd[1] field of a message
 ******************************************************************************/
void zabParseZNPSetCommandID( erStatus* Status, sapMsg* Message, unsigned8 cmdId )
{
  ZAB_CHECK_MSG_ZNP( Status, Message );
  ZAB_MSG_ZNP( Message )->Cmd1 = cmdId;
}
void zabParseZNPGetCommandID( erStatus* Status, sapMsg* Message, unsigned8 *cmdId )
{
  ZAB_CHECK_MSG_ZNP( Status, Message );
  *cmdId = ZAB_MSG_ZNP( Message )->Cmd1;
}

/******************************************************************************
 * Get/Set the length and a pointer to the data of a ZNP message
 ******************************************************************************/
void zabParseZNPGetData( erStatus* Status, sapMsg* Message, unsigned8* LengthPtr, unsigned8** DataPtr )
{
  ER_CHECK_STATUS_NULL( Status, LengthPtr );
  ER_CHECK_STATUS_NULL( Status, DataPtr );

  zabParseZNPGetLength(Status, Message, LengthPtr);
  ER_CHECK_STATUS(Status);

  *DataPtr = (ZAB_MSG_ZNP(Message)->Data);
}
void zabParseZNPSetData( erStatus* Status, sapMsg* Message, unsigned8 Length, unsigned8* Data)
{
  ZAB_CHECK_MSG_ZNP( Status, Message );
  zabParseZNPSetLength(Status, Message, Length);
  osMemCopy( Status, ZAB_MSG_ZNP( Message )->Data, Data, Length );
}
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/