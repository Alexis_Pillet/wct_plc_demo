/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the SiLabs NCP Network State Machine - HEADER TYPES
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.005  30-Jun-16   MvdB   Support energy scan for quietest channel during form
 * 000.000.016  25-Jul-16   MvdB   Support ZAB_ACTION_CHANGE_CHANNEL and other a-synchronous network parameter changes
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 * 002.002.039  17-Jan-17   MvdB   ARTF175875: In progress (binding table copy synchronised but not yet used)
 *                                  - Support policy setting in Nwk init and set binding policy to EZSP_ALLOW_BINDING_MODIFICATION
 *                                  - Clear binding table on network join/form. Read binding table on network start.
 *****************************************************************************/

#ifndef __ZAB_NCP_NWK_TYPES_H__
#define __ZAB_NCP_NWK_TYPES_H__

#include "osTypes.h"
#include "zabTypes.h"
#include "ember-types.h"


/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/


/* Supported channel range */
#define ZAB_ZNP_NWK_M_CHANNEL_MIN 11
#define ZAB_ZNP_NWK_M_CHANNEL_MAX 26

/* Value used to indicate bind does not exist at any index */
#define ZAB_NCP_NWK_BINDING_INDEX_NONE  ( 0xFF )

/* Current Item of the state machine */
typedef enum
{
  ZAB_NWK_CURRENT_ITEM_NONE,

  ZAB_NWK_CURRENT_ITEM_INIT_SET_CONFIG,
  ZAB_NWK_CURRENT_ITEM_INIT_SET_POLICY,
  ZAB_NWK_CURRENT_ITEM_INIT_SET_MS_CODE,
  ZAB_NWK_CURRENT_ITEM_NCP_INIT,
  ZAB_NWK_CURRENT_ITEM_INIT_WAIT_FOR_UP,
  ZAB_NWK_CURRENT_ITEM_LEAVE_TO_NOT_RESUME,
  ZAB_NWK_CURRENT_ITEM_LEAVE_WAIT_FOR_DOWN,


  ZAB_NWK_CURRENT_ITEM_NWK_DISCOVER,
  ZAB_NWK_CURRENT_ITEM_ACTIVE_SCAN_IN_PROGRESS,

  ZAB_NWK_CURRENT_FORM_OR_JOIN_CLEAR_BINDS,
  ZAB_NWK_CURRENT_FORM_OR_JOIN_SEC_INIT,
  ZAB_NWK_CURRENT_ITEM_ENERGY_SCAN_BEFORE_FORM_STARTING,
  ZAB_NWK_CURRENT_ITEM_ENERGY_SCAN_IN_PROGRESS,
  ZAB_NWK_CURRENT_ITEM_FORM,
  ZAB_NWK_CURRENT_ITEM_FORM_WAIT_UP,

  ZAB_NWK_CURRENT_ITEM_JOIN,
  ZAB_NWK_CURRENT_ITEM_JOIN_WAIT_UP,

  /* Network info (network address and other IDs as:
   *  - Part of a join/form/init
   *  - A Get Network Info action
   *  - A channel change action
   *  - An internal update due to a event like a channel change received OTA */
  ZAB_NWK_CURRENT_ITEM_INIT_FORM_JOIN_NODE_NODE_ID,
  ZAB_NWK_CURRENT_ITEM_INIT_FORM_JOIN_NODE_NWK_PARAMS,
  ZAB_NWK_CURRENT_ITEM_GET_NETWORK_INFO_NODE_ID,
  ZAB_NWK_CURRENT_ITEM_GET_NETWORK_INFO_NWK_PARAMS,
  ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE_NODE_ID,
  ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE_NWK_PARAMS,
  ZAB_NWK_CURRENT_ITEM_INTERNAL_NODE_ID,
  ZAB_NWK_CURRENT_ITEM_INTERNAL_NWK_PARAMS,

  ZAB_NWK_CURRENT_ITEM_GET_BINDING_TABLE,

  ZAB_NWK_CURRENT_ITEM_PJ_SET_LOCAL,
  ZAB_NWK_CURRENT_ITEM_PJ_SET_NETWORK,

  ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE,

  ZAB_NWK_CURRENT_ITEM_KEY_BROADCAST,
  ZAB_NWK_CURRENT_ITEM_DELAY_BEFORE_KEY_SWITCH,
  ZAB_NWK_CURRENT_ITEM_KEY_SWITCH,

  ZAB_NWK_CURRENT_ITEM_TX_POWER_SET,
  ZAB_NWK_CURRENT_ITEM_TX_POWER_GET
} zabNwkCurrentItem;

/* State information for this state machine */
typedef struct
{
  zabNwkState nwkState;                       /* Current public state */
  zabNwkCurrentItem currentItem;              /* Current internal state - PRIVATE*/
  unsigned8 configItemIndex;                  /* Index of configuration item being written - PRIVATE*/
  unsigned32 timeoutExpiryMs;                 /* Timeout for confirms - PRIVATE */
  unsigned32 formChannelMask;                 /* ChannelMask asked during form - PRIVATE */
  unsigned8 formLowestEnergyChannel;          /* Lowest Energy Channel found during form */
  signed8 formLowestEnergyLevel;              /* Lowest Energy Level found during form */
  unsigned16 nwkAddr;                         /* Nwk Address of the ZNP - PRIVATE */
  unsigned16 permitJoinTimeRemaining;         /* Remaining Permit Join time in seconds - PRIVATE */
  unsigned8 currentPermitJoinTimeRemaining;   /* Remaining time of the current Permit Join req in seconds - PRIVATE */
  unsigned8 getNetworkInfoRequired;           /* Get network info is required as something changed */
  unsigned8 channel;                          /* Operating Channel */
  unsigned8 nwkUpdateId;                      /* Network Update Id*/

  EmberBindingTableEntry bindingTable[ZAB_VENDOR_BINDING_TABLE_SIZE]; /* Binding Table: Maintained synchronised with NV copy in network processor*/
  unsigned8 bindSearchLastIndex;
  unsigned8 bindSearchLastTid;
} zabNwkInfo_t;

#endif /* __ZAB_NCP_NWK_TYPES_H__ */
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
