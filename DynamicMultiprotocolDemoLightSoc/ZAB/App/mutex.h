/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains manages Mutexes for the ZAB ConsoleTest Application
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 *                                            Major upgrade and improvements to handle services
 *****************************************************************************/
#ifndef __MUTEX_H__
#define __MUTEX_H__


/******************************************************************************
 *                      *****************************
 *                 *****        CONFIGURATION        *****
 *                      *****************************
 ******************************************************************************/

#define MUTEX_MAX          20      //max mutex supported


#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/
  
/******************************************************************************
 * Initialise mutex management
 ******************************************************************************/
extern void MutexInit(void);

/******************************************************************************
 * Destroy all mutexes.
 ******************************************************************************/
extern void MutexDestroy(void);

/******************************************************************************
 * Create a new Mutex for a Service.
 * Return index+1 if successful, or zero if unsuccessful
 ******************************************************************************/
extern unsigned8 MutexCreate(void* Service);

/******************************************************************************
 * Release a Mutex for a Service.
 ******************************************************************************/
extern void MutexRelease(void* Service, unsigned8 MutexId);

/******************************************************************************
 * Enter a Mutex for a Service.
 ******************************************************************************/
extern void MutexEnter(void* Service, unsigned8 MutexId);

/******************************************************************************
 * Exit a Mutex for a Service.
 ******************************************************************************/
extern void MutexExit(void* Service, unsigned8 MutexId);


#ifdef __cplusplus
}
#endif
#endif

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/