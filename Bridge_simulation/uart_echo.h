/*
 * uart_echo.h
 *
 *  Created on: 2016.04.20.
 *      Author: baadamff
 */

#ifndef UART_ECHO_H_
#define UART_ECHO_H_

void UART_Init(void);
void UART_Send(  uint8_t *data, uint8_t size );

#endif /* UART_ECHO_H_ */
