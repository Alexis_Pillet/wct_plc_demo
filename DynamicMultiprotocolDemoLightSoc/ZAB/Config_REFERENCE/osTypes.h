/*
  Name:    OS Types
  Author:  ZigBee Excellence Center
  Company: Schneider Electric

  Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

  Description:
    This file is included in all Error module implementations erXxxNnnnn.c
 *
 *
 * MODIFICATION HISTORY:
 * Core Rev     Date     Author  Change Description
 * 00.00.06.00  10-Jul-14   MvdB   ARTF67933: Add ZAB_M_ALIGNMENT_BYTES
 *
*/

#ifndef __OS_TYPES_H__
#define __OS_TYPES_H__


#include <stdint.h>     /* For data types e.g. uint8_t */
//#include <assert.h>     /* For assert */

#ifdef __cplusplus
extern "C" {
#endif

/** A compile time assertion check.
 *
 *  Validate at compile time that the predicate is true without
 *  generating code. This can be used at any point in a source file
 *  where typedef is legal.
 *
 *  On success, compilation proceeds normally.
 *
 *  On failure, attempts to typedef an array type of negative size. The
 *  offending line will look like
 *      typedef assertion_failed_file_h_42[-1]
 *  where file is the content of the second parameter which should
 *  typically be related in some obvious way to the containing file
 *  name, 42 is the line number in the file on which the assertion
 *  appears, and -1 is the result of a calculation based on the
 *  predicate failing.
 *
 *  \param predicate The predicate to test. It must evaluate to
 *  something that can be coerced to a normal C boolean.
 *
 *  \param file A sequence of legal identifier characters that should
 *  uniquely identify the source file in which this condition appears.
 */
#define USE_REAL_ASSERT
#ifdef USE_REAL_ASSERT
#define CASSERT(predicate, file) _impl_CASSERT_LINE(predicate,__LINE__,file)

#define _impl_PASTE(a,b) a##b
#define _impl_CASSERT_LINE(predicate, line, file) \
    typedef char _impl_PASTE(assertion_failed_##file##_,line)[2*!!(predicate)-1];
#else
#define CASSERT(predicate, file)
#endif

/******************************************************************************
 * Hardware Configuration
 ******************************************************************************/

/* Endianess */
#define ZAB_M_LITTLE_ENDIAN
// #define ZAB_M_BIG_ENDIAN


/* Alignment size. Set to highest memory alignment required for the hardware.
 *                 Set to zero if alignment not required. */
#define ZAB_M_ALIGNMENT_BYTES 4


/* Initialiser value for variable length arrays.
 * 0 reduces computation, but C++ compilers don't like 0 so use 1 instead*/
#define VLA_INIT                        ( 0 )

/******************************************************************************
 * Data types - Defined for your hardware
 ******************************************************************************/

typedef uint8_t zab_bool;
#ifndef zab_true
  #define zab_false 0
  #define zab_true  !0
#endif
CASSERT(sizeof(zab_bool) == 1, zabTypes_h);
CASSERT(zab_false == 0, zabTypes_h);
CASSERT(zab_true == 1, zabTypes_h);

#ifndef NULL
  #define NULL  0
#endif /* NULL */

#ifndef signed8
typedef int8_t signed8;
#endif
CASSERT(sizeof(signed8) == 1, zabTypes_h);

#ifndef unsigned8
typedef uint8_t unsigned8;
#endif
CASSERT(sizeof(unsigned8) == 1, zabTypes_h);

#ifndef signed16
typedef int16_t signed16;
#endif
CASSERT(sizeof(signed16) == 2, zabTypes_h);

#ifndef unsigned16
typedef uint16_t unsigned16;
#endif
CASSERT(sizeof(unsigned16) == 2, zabTypes_h);

#ifndef signed32
typedef int32_t signed32;
#endif
CASSERT(sizeof(signed32) == 4, zabTypes_h);

#ifndef unsigned32
typedef uint32_t unsigned32;
#endif
CASSERT(sizeof(unsigned32) == 4, zabTypes_h);

#ifndef unsigned64
typedef int64_t signed64;
#endif
#ifndef unsigned64
typedef uint64_t unsigned64;
#endif
CASSERT(sizeof(unsigned64) == 8, zabTypes_h);

#define unsigned8_max 0xFF


/******************************************************************************
 *                      ******************************
 *                 *****   RANDOM NUMBER GENERATION   *****
 *                      ******************************
 ******************************************************************************/

/* WARNING: Beware of your random number generator.
 *          The app is responsible for any seeding required!!! */
#include <stdlib.h>
#define ZAB_RAND_16() ( (unsigned16)rand() )

/******************************************************************************
 * Derived Constants and error checking
 * Do not change unless you know what you are doing!!!
 ******************************************************************************/

#if defined(ZAB_M_LITTLE_ENDIAN) && \
    defined(ZAB_M_BIG_ENDIAN)
  #error "Big and little endianess both defined in osTypes.h"
#endif
#if !defined(ZAB_M_LITTLE_ENDIAN) && \
    !defined(ZAB_M_BIG_ENDIAN)
  #error "Big or little endianess not defined in osTypes.h"
#endif


/* Calculated bit mask for alignment - do not change unless you know what you are doing!!!*/
#if (ZAB_M_ALIGNMENT_BYTES == 0)
#define ZAB_M_ALIGNMENT_MASK 0
#else
#define ZAB_M_ALIGNMENT_MASK (ZAB_M_ALIGNMENT_BYTES-1)
#endif





#ifdef __cplusplus
}
#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif /* __OS_TYPES_H__ */
