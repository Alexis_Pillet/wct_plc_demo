/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the Green Power functions for the ZNP.
 *   This is as developed for the GPZNP.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * xx.xx.xx.xx  28-Apr-14   MvdB   Original
 * 002.000.009  17-Apr-15   MvdB   ARTF131022: Remove SZL_GP_Initialize() and associated functions as it is now obsolete
 * 002.002.002  01-Sep-15   MvdB   ARTF147441: Support Key Mode in zabZnpGp_CommissioningReplyReq()
 *****************************************************************************/
#ifndef __ZAB_ZNP_GP_H__
#define __ZAB_ZNP_GP_H__


//#include "zabZnpService.h"
#include "zabVendorConfigure.h"

#ifdef ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER  

#ifdef __cplusplus
extern "C" {
#endif
  
  
/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/
  
/* GP Parameters */
typedef enum
{
  ZNP_GP_PARAM_ID_PRE_COM_GROUP_ID         = 0x00,
  ZNP_GP_PARAM_ID_LOOPBACK_DST_EP          = 0x01,
  ZNP_GP_PARAM_ID_TX_Q_ENTRY_LIFETIME      = 0x02,
  ZNP_GP_PARAM_ID_TX_Q_EXPIRY_IND          = 0x03,
} ZNP_GP_PARAM_ID; 

/* GP Commissioning Mode */
typedef enum
{
  ZNP_GP_COMMISSIONING_MODE_EXIT                = 0x00,
  ZNP_GP_COMMISSIONING_MODE_NORMAL              = 0x01,   // Enter commissioning, all commissioning accepted, proxy to use broadcast for commissioning notification
  ZNP_GP_COMMISSIONING_MODE_RESTRICTIVE         = 0x02,   // Enter commissioning, application validates commissioning, proxy to use broadcast for commissioning notification
  ZNP_GP_COMMISSIONING_MODE_NORMAL_UNICAST      = 0x03,   // Enter commissioning, all commissioning accepted, proxy to use unicast for commissioning notification
  ZNP_GP_COMMISSIONING_MODE_RESTRICTIVE_UNICAST = 0x04    // Enter commissioning, application validates commissioning, proxy to use unicast for commissioning notification
} ZNP_GP_COMMISSIONING_MODE; 

/* GP Commissioning Exit Mode bit fields 
 * Only one of ZNP_GP_COMMISSIONING_EXIT_MODE_PAIRING_SUCSESS and ZNP_GP_COMMISSIONING_EXIT_MODE_PROXY_EXIT may be set at the same time.
 * ZNP_GP_COMMISSIONING_EXIT_MODE_WINDOW_EXPIRATION may be combined with either of the other flags. */
#define ZNP_GP_COMMISSIONING_EXIT_MODE_WINDOW_EXPIRATION  0x01
#define ZNP_GP_COMMISSIONING_EXIT_MODE_PAIRING_SUCCESS    0x02
#define ZNP_GP_COMMISSIONING_EXIT_MODE_PROXY_EXIT         0x04


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/  

/******************************************************************************
 * Clear all GP-related tables on the ZigBee network device, including the 
 * Sink Table and the Translation Table
 ******************************************************************************/
extern 
void zabZnpGp_ClearAll(erStatus* Status, 
                       zabService* Service);

/******************************************************************************
 * Set a GP-related parameter on the ZigBee stack that is not accessible through 
 * a specified GPEP attribute, e.g. Pre-commissioned group ID
 ******************************************************************************/
extern 
void zabZnpGp_SetParameter(erStatus* Status, 
                           zabService* Service, 
                           ZNP_GP_PARAM_ID ParamId, 
                           void* Data);

/******************************************************************************
 * Instruct the GPS to enter or exit commissioning mode.
 * 
 * CommissioningMode:
 *  - Normal Mode = Stack will accept any GPD when commissioning window open
 *  - Restrictive = Stack will callback to host t confirm acceptance of GPD
 * 
 * CommissioningModeExit may be set using the flags:
 *  - ZNP_GP_COMMISSIONING_EXIT_MODE_WINDOW_EXPIRATION
 *  - ZNP_GP_COMMISSIONING_EXIT_MODE_PAIRING_SUCCESS
 *  - ZNP_GP_COMMISSIONING_EXIT_MODE_PROXY_EXIT
 ******************************************************************************/
extern 
void zabZnpGp_SetCommissioningMode(erStatus* Status, 
                                   zabService* Service, 
                                   ZNP_GP_COMMISSIONING_MODE CommissioningMode, 
                                   unsigned8 CommissioningModeExit, 
                                   unsigned16 CommissioningWindow);

/******************************************************************************
 * Instruct the GPS to inject a Commissioning Reply command to the 
 * TempMaster’s gpTxQueue, while in restrictive commissioning mode
 ******************************************************************************/
extern 
void zabZnpGp_CommissioningReplyReq(erStatus* Status, 
                                    zabService* Service, 
                                    unsigned64 GpdId, 
                                    unsigned8 Options,
                                    unsigned8 KeyMode);

/******************************************************************************
 * GP Commissioning Reply Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern
void zabZnpGp_CommissioningReplyReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * Process incoming GP messages
 ******************************************************************************/
extern 
void zabZnpGp_ProcessIncomingMessage(erStatus* Status, 
                                     zabService* Service, 
                                     sapMsg* Message);

#ifdef __cplusplus
}
#endif

#endif // ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER
#endif // __ZAB_ZNP_GP_H__
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/