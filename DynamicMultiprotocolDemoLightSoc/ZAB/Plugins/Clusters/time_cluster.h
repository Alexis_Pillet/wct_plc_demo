/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the Time Cluster.
 *   This plugin is originally designed for ComX (partner).
 * 
 *   It supports:
 *    - Operation on a single endpoint
 *    - Mandatory attributes only
 *    - Operation as a time master only. Time MAY NOT be set accross ZigBee.
 *
 * USAGE:
 *   Usage of this plugin is very simple:
 *    1. Initialise it with SzlPlugin_TimeCluster_Init(), specifying the 
 *       endpoint and a function for the plugin to get the UTC Time
 * 
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         23-Apr-14   MvdB   Original
 * 002.001.006  27-Jul-15   MvdB   ARTF134686: Standardise on SZL_RESULT_t for plugin init return value
 *****************************************************************************/
#ifndef _TIME_CLUSTER_H_
#define _TIME_CLUSTER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "zabTypes.h"
#include "szl.h"

/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/
  
/* Callback for plugin to get UTC Time from the application.
 * This time standard is the number of seconds since 0 hrs 0 mins 0 sec on 
 * 1st January 2000 UTC (Universal Coordinated Time).*/
typedef void (*SzlPlugin_TimeCluster_UtcTime_CB_t) (szl_uint32* UtcTime);


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Plugin Initialisation
 * This function must be called from the application after the Library has been initialized.
 * 
 * Parameters:
 *  Service: Instance of library
 *  Endpoint: The endpoint on which the cluster will be run
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_TimeCluster_Init(zabService* Service, 
                                        szl_uint8 Endpoint,
                                        SzlPlugin_TimeCluster_UtcTime_CB_t Callback);

/******************************************************************************
 * Plugin Destruction
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_TimeCluster_Destroy(zabService* Service);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /*_TIME_CLUSTER_H_*/
