/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the Electrical Circuit Breaker Cluster.
 *   This plugin is originally designed for Nova (partner).
 * 
 *   It supports:
 *    - Multiple, application specified endpoints
 *    - Standard attributes
 *    - =S= MS attributes defined in ZigBee&GreenPower_Invariants_Partner_Products_V05.pdf
 *    - Default report configuration for time based reports (fast or slow)
 *    - Callback notification to app when a writable attribute is changed
 * 
 *   It does not (currently) support:
 *    - Non-volatile backing of any data.
 *    - Report on change. These are not required for Nova, but would be required for certification.
 *
 * USAGE:
 *   Usage of this plugin is very simple:
 *    1. Initialise it with SzlPlugin_EcbCluster_Init(), specify the endpoints and a write notification callback
 *    2. Use SzlPlugin_EcbCluster_GetAttributePointer() to get access to the data for each endpoint.
 *    3. Populate the data
 *    4. Call SzlPlugin_EcbCluster_ConfigureReporting() to start attribute reporting
 *    5. Update data as it changes, the plugin will do the rest.
 * 
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         18-Apr-13   MvdB   Original
 *    2         19-May-14   MvdB   Remove endpoint number from public attribute data
 *    3         30-Oct-14   MvdB   artf74202: Support callbacks in Nova plugins
 *    4         07-Nov-14   MvdB   Fix missing memcpy in init when using callbacks
 *    5         19-Feb-15   MvdB   Add attributes ATTRID_ECB_TU_SERVICE_LIFE, ATTRID_ECB_TU_ADAPTATION_INFO
 *    6         01-Apr-15   MvdB   ARTF116256: Ensure SZL uses szl_memset and szl_memcpy rather than memset and memcpy
 *    7         20-Jul-15   MvdB   ARTF134686: Update allocation method to be generic and protect against multiple initialisation
 * 002.001.006  27-Jul-15   MvdB   ARTF134686: Standardise on SZL_RESULT_t for plugin init return value
 *                                 ARTF138318: Improve plugin destroy, add disable reporting function
 *****************************************************************************/

#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include "zabCoreService.h"
#include "zabCorePrivate.h"
#include "szl.h"
#include "szl_plugin.h"
#include "szl_report.h"

#include "ecb_cluster.h"

/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/

/* Fast and slow reporting periods
 * Only time is applied, reportable change is not used */
#define ECB_M_SLOW_REPORT_TIME_S 60

/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Cluster ID */
#define ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER     0xFF14

typedef struct
{
  SzlPlugin_ECB_Attributes_t AttributeData;
  szl_uint8 EndpointNumber;
}SzlPlugin_ECB_Endpoint_t;

/* Parameters for the service */
typedef struct
{
  szl_uint8 numEndpoints;
#ifdef ECB_M_USE_DIRECT_ACCESS
  SzlPlugin_ECB_AttributeWriteNotification_CB_t AttributeWriteCallback;
  SzlPlugin_ECB_Endpoint_t EcbEndpoints[VLA_INIT];
#else
  szl_uint8 Endpoints[VLA_INIT];
#endif
}SzlPlugin_ECB_Params_t;

#ifdef ECB_M_USE_DIRECT_ACCESS
#define SzlPlugin_ECB_Params_t_SIZE(numEndpoints) (sizeof(SzlPlugin_ECB_Params_t) - (VLA_INIT * sizeof(SzlPlugin_ECB_Endpoint_t))+ (sizeof(SzlPlugin_ECB_Endpoint_t) * (numEndpoints)))
#else
#define SzlPlugin_ECB_Params_t_SIZE(numEndpoints) (sizeof(SzlPlugin_ECB_Params_t) - (VLA_INIT * sizeof(szl_uint8))+ (sizeof(szl_uint8) * (numEndpoints)))
#endif

/* 
 * The set of attributes for the cluster.
 * This const data is used to initialise the majority of the attribute table.
 * Once it is copied into RAM, Endpoint and Data Access pointers are set for each attribute.
 * 
 * Read only attributes use direct access by SZL.
 * Writable attributes use Read/Write callbacks, as the need the write callback to notify the plugin that
 *   a write is happening, so it can notify the app.
 * 
 * New attributes must be added here.
 */
const SZL_DataPoint_t ecbDatapointConfig[] = {
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_STATUS,                0, SZL_ZB_DATATYPE_BITMAP8,   1,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrStatus)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_STATUS_ALARM,          0, SZL_ZB_DATATYPE_BITMAP8,   1,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrStatusAlarm)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_STATUS_ALARM_MASK,     0, SZL_ZB_DATATYPE_BITMAP8,   1,                              {DP_METHOD_DIRECT, DP_ACCESS_RW,   szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrStatusAlarmMask)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_TRIP_BASIC_ALARM,      0, SZL_ZB_DATATYPE_BITMAP16,  2,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrTripBasicAlarm)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_TRIP_BASIC_ALARM_MASK, 0, SZL_ZB_DATATYPE_BITMAP16,  2,                              {DP_METHOD_DIRECT, DP_ACCESS_RW,   szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrTripBasicAlarmMask)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_SETPOINT_ALARM,        0, SZL_ZB_DATATYPE_BITMAP16,  2,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrSetpointAlarm)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_SETPOINT_ALARM_MASK,   0, SZL_ZB_DATATYPE_BITMAP16,  2,                              {DP_METHOD_DIRECT, DP_ACCESS_RW,   szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrSetpointAlarmMask)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_NOMINAL_CURRENT,          0, SZL_ZB_DATATYPE_UINT16,    2,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, NominalCurrent)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_SETTINGS_ALARM,        0, SZL_ZB_DATATYPE_BITMAP16,  2,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrSettingsAlarm)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_SETTINGS_ALARM_MASK,   0, SZL_ZB_DATATYPE_BITMAP16,  2,                              {DP_METHOD_DIRECT, DP_ACCESS_RW,   szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrSettingsAlarmMask)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_IDENTIFIER,            0, SZL_ZB_DATATYPE_ENUM16,    2,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrIdentifier)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_TYPE,                  0, SZL_ZB_DATATYPE_ENUM8,     1,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrType)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_REFERENCE,             0, SZL_ZB_DATATYPE_CHAR_STR,  ECB_M_REFERENCE_LENGTH,         {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrReference)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_FAMILY,                0, SZL_ZB_DATATYPE_ENUM8,     1,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrFamily)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_RANGE,                 0, SZL_ZB_DATATYPE_CHAR_STR,  ECB_M_RANGE_LENGTH,             {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrRange)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_STANDARD,              0, SZL_ZB_DATATYPE_CHAR_STR,  ECB_M_STANDARD_LENGTH,          {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrStandard)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_SERIAL_NUMBER,         0, SZL_ZB_DATATYPE_CHAR_STR,  ECB_M_SERIAL_NUMBER_LENGTH,     {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrSerialNumber)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_PERFORMANCE,           0, SZL_ZB_DATATYPE_CHAR_STR,  ECB_M_PERFORMANCE_LENGTH,       {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrPerformance)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_NB_POLES,              0, SZL_ZB_DATATYPE_UINT8,     1,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrNbPoles)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_INSTALLATION,          0, SZL_ZB_DATATYPE_ENUM8,     1,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrInstallation)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_HW_VERSION,            0, SZL_ZB_DATATYPE_CHAR_STR,  ECB_M_HARDWARE_VERSION_LENGTH,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrHWVersion)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_USAGE,                 0, SZL_ZB_DATATYPE_CHAR_STR,  ECB_M_USAGE_LENGTH,             {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrUsage)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_RATING,                0, SZL_ZB_DATATYPE_UINT8,     1,                              {DP_METHOD_DIRECT, DP_ACCESS_RW,   szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrRating)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_NB_OPERATION_ELECT,       0, SZL_ZB_DATATYPE_UINT16,    2,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, NbOperationElect)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_NB_OPERATION_MECHA,       0, SZL_ZB_DATATYPE_UINT16,    2,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, NbOperationMecha)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_BR_HEALTH_STATUS,         0, SZL_ZB_DATATYPE_ENUM8,     1,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, BrHealthStatus)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_TU_SERVICE_LIFE,          0, SZL_ZB_DATATYPE_ENUM8,     1,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, TuServiceLife)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_PROTECTION_TYPE,          0, SZL_ZB_DATATYPE_ENUM16,    2,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, ProtectionType)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_APPLICATION_TYPE,         0, SZL_ZB_DATATYPE_ENUM16,    2,                              {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, EcbApplicationType)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER,  ATTRID_ECB_TU_ADAPTATION_INFO,       0, SZL_ZB_DATATYPE_CHAR_STR,  ECB_M_TU_ADAPTATION_INFO_LENGTH,{DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_ECB_Attributes_t, TuAdaptationInfo)}}},
};
#define ECB_M_NUM_ATTRIBUTES_PER_ENDPOINT ( sizeof(ecbDatapointConfig) / sizeof(SZL_DataPoint_t))


/* Reporting Configuration Struct - used to define default reports */
typedef struct
{
  szl_uint16 AttributeID;
  szl_uint16 MinTime;
  szl_uint16 MaxTime;
  /*
  // Reportable change values - not supported for now as Nova only requires timed reports
  union 
    {
      szl_uint16 RCuint16;
      szl_int32 RCint32;
      szl_uint32 RCuint32;
    };*/
}ReportingConfiguration_t;

/* Default Reporting Configuration
 * If an attribute is to be reported, it should be added to this table. */
static const ReportingConfiguration_t DefaultReportingConfiguration[] = {
{ATTRID_ECB_BR_STATUS_ALARM,                          ECB_M_SLOW_REPORT_TIME_S, ECB_M_SLOW_REPORT_TIME_S},
{ATTRID_ECB_BR_TRIP_BASIC_ALARM,                      ECB_M_SLOW_REPORT_TIME_S, ECB_M_SLOW_REPORT_TIME_S},
{ATTRID_ECB_BR_SETTINGS_ALARM,                        ECB_M_SLOW_REPORT_TIME_S, ECB_M_SLOW_REPORT_TIME_S},
{ATTRID_ECB_NB_OPERATION_ELECT,                       ECB_M_SLOW_REPORT_TIME_S, ECB_M_SLOW_REPORT_TIME_S},
{ATTRID_ECB_NB_OPERATION_MECHA,                       ECB_M_SLOW_REPORT_TIME_S, ECB_M_SLOW_REPORT_TIME_S},
{ATTRID_ECB_BR_HEALTH_STATUS,                         ECB_M_SLOW_REPORT_TIME_S, ECB_M_SLOW_REPORT_TIME_S},
};
#define ECB_M_NUM_DEFAULT_REPORTS (sizeof(DefaultReportingConfiguration) / sizeof(ReportingConfiguration_t))



/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Attribute Changed Notification Handler
 ******************************************************************************/
#ifdef ECB_M_USE_DIRECT_ACCESS
static void AttributeChangedNotificationHandler(zabService* Service, struct _SZL_AttributeChangedNtfParams_t* Params)
{
  SzlPlugin_ECB_Params_t * pluginParams = NULL;
  
  /* Validate inputs */
  if ( (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_ECB, (void**)&pluginParams) == SZL_RESULT_SUCCESS) &&
       (pluginParams != NULL) &&
       (pluginParams->AttributeWriteCallback != NULL) )
    {
      pluginParams->AttributeWriteCallback(Service,
                                           Params->Endpoint,
                                           Params->AttributeID);
    }
}
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/
#define M_NUM_ATTRIBUTES_PER_REGISTRATION 10
/******************************************************************************
 * Plugin Initialisation
 * This function must be called from the application after the Library has been initialized.
 * 
 * Parameters:
 *  Service: Instance of library
 *  numEndpoints: The length of endpointList
 *  endpointList: The endpoints to be initialised with this cluster
 *  WriteAttributesCallback: Callback will be called to notify application that a writable attribute has been written from ZigBee.
 *                           It is optional. If the app does not want to be notified this may be set to NULL.
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_EcbCluster_Init(zabService* Service, 
                                       szl_uint8 numEndpoints, 
                                       szl_uint8 * endpointList, 
#ifdef ECB_M_USE_DIRECT_ACCESS
                                       SzlPlugin_ECB_AttributeWriteNotification_CB_t WriteAttributesCallback
#else                                           
                                       SZL_CB_DataPointRead_t ReadCallback,
                                       SZL_CB_DataPointWrite_t WriteCallback
#endif
                                       )
{
    SZL_RESULT_t result;
    szl_uint8 epIndex;
    szl_uint8 attrIndex;
    szl_uint8 attrChunkIndex;
    SzlPlugin_ECB_Params_t * pluginParams = NULL;
    SZL_DataPoint_t serverAttributes[M_NUM_ATTRIBUTES_PER_REGISTRATION];
    szl_bool anyRegisterWorked;
    
    /* Validate inputs */
    if ( (Service == NULL) || (numEndpoints == 0) || (endpointList == NULL) 
#ifndef ECB_M_USE_DIRECT_ACCESS 
          || (ReadCallback == NULL) || (WriteCallback == NULL)                                             
#endif
                                                      )
      {
        return SZL_RESULT_INVALID_DATA;
      }
    
    /* Malloc parameters the plugin needs to store and link from the SZL */
    result = SZL_PluginRegister(Service, SZL_PLUGIN_ID_ECB, SzlPlugin_ECB_Params_t_SIZE(numEndpoints), (void**)&pluginParams);
    if ( (result == SZL_RESULT_SUCCESS) && (pluginParams != NULL) )
      {
        /* Set parameters that are not per endpoint */
        pluginParams->numEndpoints = numEndpoints;

#ifdef ECB_M_USE_DIRECT_ACCESS
        pluginParams->AttributeWriteCallback = WriteAttributesCallback;
        if (WriteAttributesCallback != NULL)
          {
            if (SZL_AttributeChangedNtfRegisterCB(Service, AttributeChangedNotificationHandler) == SZL_RESULT_TABLE_FULL)
              {
                printError(Service, "PLUGIN ECB: Init failed - callback table full\n");
                SZL_PluginUnregister(Service, SZL_PLUGIN_ID_ECB);
                return SZL_RESULT_TABLE_FULL;
              }
          }
#else
        szl_memcpy(pluginParams->Endpoints, endpointList, numEndpoints);
#endif

        anyRegisterWorked = szl_false;
        result = SZL_RESULT_SUCCESS;
        for (epIndex = 0; 
             (epIndex < numEndpoints) && (result == SZL_RESULT_SUCCESS || result == SZL_RESULT_ALREADY_EXISTS);  
             epIndex++)
          {

            /* Initialise endpoint number in data
             * All other data is un-initialised - this is up to the app unless we choose to add NVM support later */
#ifdef ECB_M_USE_DIRECT_ACCESS
            pluginParams->EcbEndpoints[epIndex].EndpointNumber = endpointList[epIndex];
#else     
            pluginParams->Endpoints[epIndex] = endpointList[epIndex];       
#endif

            /* Register attributes in chunks to keep the stack usage under control */
            for (attrIndex = 0; attrIndex < ECB_M_NUM_ATTRIBUTES_PER_ENDPOINT; attrIndex += M_NUM_ATTRIBUTES_PER_REGISTRATION)
              {

                /* Copy the next chunk of attributes to be registered to RAM
                 * This is mostly the same for each EP, but we set the endpoint number and data pointers differently */
                szl_memcpy(serverAttributes,
                           &ecbDatapointConfig[attrIndex],
                           sizeof(serverAttributes));

                /* Set the data pointers in to params correctly, based on the endpoint number and attribute ID*/
                for (attrChunkIndex = 0; 
                     (attrChunkIndex < M_NUM_ATTRIBUTES_PER_REGISTRATION) && ((attrIndex + attrChunkIndex) < ECB_M_NUM_ATTRIBUTES_PER_ENDPOINT);
                     attrChunkIndex++)
                  {
                    serverAttributes[attrChunkIndex].Endpoint = endpointList[epIndex];

#ifdef ECB_M_USE_DIRECT_ACCESS
                    serverAttributes[attrChunkIndex].AccessType.DirectAccess.Data = (void*)((szl_uint8*)serverAttributes[attrChunkIndex].AccessType.DirectAccess.Data + (size_t)&pluginParams->EcbEndpoints[epIndex].AttributeData);
#else
                    serverAttributes[attrChunkIndex].Property.Method = DP_METHOD_CALLBACK;
                    serverAttributes[attrChunkIndex].AccessType.Callback.Read = ReadCallback;
                    serverAttributes[attrChunkIndex].AccessType.Callback.Write = WriteCallback;
#endif
                  }   

                /* Register the cluster attributes */
                result = SZL_AttributesRegister(Service, serverAttributes, attrChunkIndex);

                /* Keep boolean to show if any register worked, as if it did we must ensure the data it points to is valid */
                if(result == SZL_RESULT_SUCCESS || result == SZL_RESULT_ALREADY_EXISTS)
                  {
                    anyRegisterWorked = szl_true;
                  }
              } 
          } 

        /* If any register worked, we must keep Params to ensure data pointers are valid. If all failed we can free */
        if(anyRegisterWorked == szl_true)
          {
            printInfo(Service, "PLUGIN ECB: Init Successful\n");
            return SZL_STATUS_SUCCESS;
          }
        else
          {
            /* Failed. Cleanup */
            SZL_PluginUnregister(Service, SZL_PLUGIN_ID_ECB);
            result = SZL_RESULT_FAILED;
          }
      }
    
    return result;
}

/******************************************************************************
 * Plugin Destruction
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_EcbCluster_Destroy(zabService* Service)
{
  SZL_RESULT_t result;
  szl_uint8 epIndex;
  SZL_EP_t endpoint;
  szl_uint8 attrIndex;
  szl_uint8 attrChunkIndex;
  SzlPlugin_ECB_Params_t * pluginParams = NULL;
  SZL_DataPoint_t serverAttributes[M_NUM_ATTRIBUTES_PER_REGISTRATION];
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_ECB, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
    
  
  for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
    {
      /* Get the endpoint number */
#ifdef ECB_M_USE_DIRECT_ACCESS 
      endpoint = pluginParams->EcbEndpoints[epIndex].EndpointNumber;
#else
      endpoint = pluginParams->Endpoints[epIndex];
#endif

      /* Register attributes in chunks to keep the stack usage under control */
      for (attrIndex = 0; attrIndex < ECB_M_NUM_ATTRIBUTES_PER_ENDPOINT; attrIndex += M_NUM_ATTRIBUTES_PER_REGISTRATION)
        {
          /* Copy the next chunk of attributes to be registered to RAM
           * This is mostly the same for each EP, but we set the endpoint number differently */
          szl_memcpy(serverAttributes,
                     &ecbDatapointConfig[attrIndex],
                     sizeof(serverAttributes));

          /* Set the data pointers in to params correctly, based on the endpoint number and attribute ID*/
          for (attrChunkIndex = 0; 
               (attrChunkIndex < M_NUM_ATTRIBUTES_PER_REGISTRATION) && ((attrIndex + attrChunkIndex) < ECB_M_NUM_ATTRIBUTES_PER_ENDPOINT);
               attrChunkIndex++)
            {
              serverAttributes[attrChunkIndex].Endpoint = endpoint;
            }   

          /* De-Register the cluster attributes.
           * Don't care too much about the result as we will make best effort only */
          result = SZL_AttributesDeregister(Service, serverAttributes, attrChunkIndex);
          if (result != SZL_RESULT_SUCCESS)
            {
              printError(Service, "PLUGIN ECB: WARNING: Deregister failed for EP 0x%02X with Result 0x%02X\n",
                         endpoint,
                         result);
            }
        }
    }
  
  /* Now unregister the plugin */
  return SZL_PluginUnregister(Service, SZL_PLUGIN_ID_ECB);
}

/******************************************************************************
 * Get pointer to Electrical Circuit Breaker Cluster Attributes for an endpoint
 * This can be used to get access to the endpoints parameters for the local 
 *  application to read or write them
 * Return NULL if not found
 ******************************************************************************/
#ifdef ECB_M_USE_DIRECT_ACCESS
SzlPlugin_ECB_Attributes_t* SzlPlugin_EcbCluster_GetAttributePointer(zabService* Service, szl_uint8 endpoint)
{
  szl_uint8 epIndex;
  SzlPlugin_ECB_Params_t * pluginParams = NULL;
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_ECB, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return NULL;
    }
  
  for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
    {
      if (pluginParams->EcbEndpoints[epIndex].EndpointNumber == endpoint)
        {
          return &pluginParams->EcbEndpoints[epIndex].AttributeData;
        }
    }
  return NULL;
}
#endif

/******************************************************************************
 * Configure reporting for the cluster.
 * Data must be initialised before this function is called to avoid reporting uninitialised values
 * 
 * This function currently only configures a time based default report.
 * If report on change is required the plugin must be extended.
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_EcbCluster_ConfigureReporting(zabService* Service)
{
  szl_uint8 epIndex;
  szl_uint8 reportIndex;
  szl_uint8 attrIndex;
  szl_uint8 endpoint;
  SZL_STATUS_t sts;
  szl_bool manufacturerSpecific;
  SzlPlugin_ECB_Params_t * pluginParams = NULL;
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_ECB, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /*
   * Configure default reports:
   * For each default report in DefaultReportingConfiguration[]
   * Find the matching attribute in ecbDatapointConfig[]
   * Use values from ecbDatapointConfig and DefaultReportingConfiguration to configure reports
   * Repeat for each endpoint
   */
  for (reportIndex = 0; reportIndex < ECB_M_NUM_DEFAULT_REPORTS; reportIndex++)
    {
      for (attrIndex = 0; attrIndex < ECB_M_NUM_ATTRIBUTES_PER_ENDPOINT; attrIndex++)
        {
          if (DefaultReportingConfiguration[reportIndex].AttributeID == ecbDatapointConfig[attrIndex].AttributeID)
            {
              //Match found
              break;
            }
        }
      
      // If we found a match, then attrIndex will be < ECB_M_NUM_ATTRIBUTES_PER_ENDPOINT
      if (attrIndex < ECB_M_NUM_ATTRIBUTES_PER_ENDPOINT)
        {
        
          /* Register the default report for each endpoint */
          for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
            {
#ifdef ECB_M_USE_DIRECT_ACCESS
              endpoint = pluginParams->EcbEndpoints[epIndex].EndpointNumber;
#else
              endpoint = pluginParams->Endpoints[epIndex];
#endif
              
              if (ecbDatapointConfig[attrIndex].Property.ManufacturerSpecific == szl_true)
                {
                  manufacturerSpecific = szl_true;
                }
              else
                {
                  manufacturerSpecific = szl_false;
                }
              

              sts = Szl_ReportSendingConfigure(Service, 
                                                  endpoint, 
                                                  manufacturerSpecific,
                                                  ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER, 
                                                  DefaultReportingConfiguration[reportIndex].AttributeID,         
                                                  ecbDatapointConfig[attrIndex].DataType, 
                                                  DefaultReportingConfiguration[reportIndex].MinTime, 
                                                  DefaultReportingConfiguration[reportIndex].MaxTime, 
                                                  NULL); // Reportable change not used for now as not required by Nova
              if (sts != SZL_STATUS_SUCCESS)
                {
                  printError(Service, "PLUGIN ECB: ERROR - Default report configuration for AttrID 0x%04X failed: 0x%02X\n", DefaultReportingConfiguration[reportIndex].AttributeID, sts);
                  return SZL_RESULT_FAILED;
                }
            }        
        }
      else
        {
          printError(Service, "PLUGIN ECB: ERROR - Default report configuration for unknown attribute ID 0x%04X\n", DefaultReportingConfiguration[reportIndex].AttributeID);  
          return SZL_RESULT_INVALID_DATA;
        } 
    }
  
  // If or when we need to support report configuration in NVM, this is probably the place to load them out
  
  return SZL_RESULT_SUCCESS;
}


/******************************************************************************
 * Disable reporting for the cluster
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_EcbCluster_DisableReporting(zabService* Service)
{
  szl_uint8 epIndex;
  szl_uint8 reportIndex;
  szl_uint8 attrIndex;
  szl_uint8 endpoint;
  SZL_STATUS_t sts;
  SZL_RESULT_t result = SZL_RESULT_SUCCESS;
  szl_bool manufacturerSpecific;
  SzlPlugin_ECB_Params_t * pluginParams = NULL;
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_ECB, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /*
   * Configure default reports:
   * For each default report in DefaultReportingConfiguration[]
   * Find the matching attribute in DatapointConfig[]
   * Set SZL_REPORT_M_REPORT_DISABLED_TIME
   * Repeat for each endpoint
   */
  for (reportIndex = 0; reportIndex < ECB_M_NUM_DEFAULT_REPORTS; reportIndex++)
    {
      for (attrIndex = 0; attrIndex < ECB_M_NUM_ATTRIBUTES_PER_ENDPOINT; attrIndex++)
        {
          if (DefaultReportingConfiguration[reportIndex].AttributeID == ecbDatapointConfig[attrIndex].AttributeID)
            {
              //Match found
              break;
            }
        }
      
      // If we found a match, then attrIndex will be < ECB_M_NUM_ATTRIBUTES_PER_ENDPOINT
      if (attrIndex < ECB_M_NUM_ATTRIBUTES_PER_ENDPOINT)
        {
        
          /* Disable the default report for each endpoint */
          for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
            {
#ifdef ECB_M_USE_DIRECT_ACCESS
              endpoint = pluginParams->EcbEndpoints[epIndex].EndpointNumber;
#else
              endpoint = pluginParams->Endpoints[epIndex];
#endif
              
              if (ecbDatapointConfig[attrIndex].Property.ManufacturerSpecific == szl_true)
                {
                  manufacturerSpecific = szl_true;
                }
              else
                {
                  manufacturerSpecific = szl_false;
                }
              
              
              /* Configure report with disabled time */
              sts = Szl_ReportSendingConfigure(Service, 
                                                  endpoint, 
                                                  manufacturerSpecific,
                                                  ZCL_CLUSTER_ID_ELECTRCIAL_CIRCUIT_BREAKER, 
                                                  DefaultReportingConfiguration[reportIndex].AttributeID,         
                                                  ecbDatapointConfig[attrIndex].DataType, 
                                                  SZL_REPORT_M_REPORT_DISABLED_TIME, 
                                                  SZL_REPORT_M_REPORT_DISABLED_TIME, 
                                                  NULL);
              if (sts != SZL_STATUS_SUCCESS)
                {
                  result = SZL_RESULT_FAILED;
                  printError(Service, "PLUGIN ECB: ERROR - Disable report configuration for AttrID 0x%04X failed: 0x%02X\n", DefaultReportingConfiguration[reportIndex].AttributeID, sts);
                }
            }        
        }
    }
  
  return result;
}
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/