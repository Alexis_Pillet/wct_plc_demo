/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the ZAB Test Cluster.
 *   This plugin is designed for ZAB testing only.
 * 
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 * 002.002.015  22-Oct-15   MvdB   Original
 *****************************************************************************/
#ifndef _ZAB_TEST_CLUSTER_H_
#define _ZAB_TEST_CLUSTER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "zabTypes.h"
#include "szl.h"

  
/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/


/* Set Data High or Low */
typedef enum
{
  SzlPlugin_ZabTestSetData_Min,
  SzlPlugin_ZabTestSetData_Max,
  SzlPlugin_ZabTestSetData_Typical,
} SzlPlugin_ZabTestSetData_t;  
  
/* Attributes Identifiers */
typedef enum
{
  ATTRID_ZABTEST_NO_DATA         = 0x0000,
  ATTRID_ZABTEST_DATA8           = 0x0008,
  ATTRID_ZABTEST_DATA16          = 0x0009,
  ATTRID_ZABTEST_DATA24          = 0x000a,
  ATTRID_ZABTEST_DATA32          = 0x000b,
  ATTRID_ZABTEST_DATA40          = 0x000c,
  ATTRID_ZABTEST_DATA48          = 0x000d,
  ATTRID_ZABTEST_DATA56          = 0x000e,
  ATTRID_ZABTEST_DATA64          = 0x000f,
  ATTRID_ZABTEST_BOOLEAN         = 0x0010,
  ATTRID_ZABTEST_BITMAP8         = 0x0018,
  ATTRID_ZABTEST_BITMAP16        = 0x0019,
  ATTRID_ZABTEST_BITMAP24        = 0x001a,
  ATTRID_ZABTEST_BITMAP32        = 0x001b,
  ATTRID_ZABTEST_BITMAP40        = 0x001c,
  ATTRID_ZABTEST_BITMAP48        = 0x001d,
  ATTRID_ZABTEST_BITMAP56        = 0x001e,
  ATTRID_ZABTEST_BITMAP64        = 0x001f,
  ATTRID_ZABTEST_UINT8           = 0x0020,
  ATTRID_ZABTEST_UINT16          = 0x0021,
  ATTRID_ZABTEST_UINT24          = 0x0022,
  ATTRID_ZABTEST_UINT32          = 0x0023,
  ATTRID_ZABTEST_UINT40          = 0x0024,
  ATTRID_ZABTEST_UINT48          = 0x0025,
  ATTRID_ZABTEST_UINT56          = 0x0026,
  ATTRID_ZABTEST_UINT64          = 0x0027,
  ATTRID_ZABTEST_INT8            = 0x0028,
  ATTRID_ZABTEST_INT16           = 0x0029,
  ATTRID_ZABTEST_INT24           = 0x002a,
  ATTRID_ZABTEST_INT32           = 0x002b,
  ATTRID_ZABTEST_INT40           = 0x002c,
  ATTRID_ZABTEST_INT48           = 0x002d,
  ATTRID_ZABTEST_INT56           = 0x002e,
  ATTRID_ZABTEST_INT64           = 0x002f,
  ATTRID_ZABTEST_ENUM8           = 0x0030,
  ATTRID_ZABTEST_ENUM16          = 0x0031,
  ATTRID_ZABTEST_SEMI_PREC       = 0x0038,
  ATTRID_ZABTEST_SINGLE_PREC     = 0x0039,
  ATTRID_ZABTEST_DOUBLE_PREC     = 0x003a,
  ATTRID_ZABTEST_OCTET_STR       = 0x0041,
  ATTRID_ZABTEST_CHAR_STR        = 0x0042,
  ATTRID_ZABTEST_LONG_OCTET_STR  = 0x0043,
  ATTRID_ZABTEST_LONG_CHAR_STR   = 0x0044,
  ATTRID_ZABTEST_ARRAY           = 0x0048,
  ATTRID_ZABTEST_STRUCT          = 0x004c,
  ATTRID_ZABTEST_SET             = 0x0050,
  ATTRID_ZABTEST_BAG             = 0x0051,
  ATTRID_ZABTEST_TOD             = 0x00e0,
  ATTRID_ZABTEST_DATE            = 0x00e1,
  ATTRID_ZABTEST_UTC             = 0x00e2,
  ATTRID_ZABTEST_CLUSTER_ID      = 0x00e8,
  ATTRID_ZABTEST_ATTR_ID         = 0x00e9,
  ATTRID_ZABTEST_BAC_OID         = 0x00ea,
  ATTRID_ZABTEST_IEEE_ADDR       = 0x00f0,
  ATTRID_ZABTEST_128_BIT_SEC_KEY = 0x00f1,
  ATTRID_ZABTEST_UNKNOWN         = 0x00ff,
} ZABTEST_CLUSTER_ATTRIBUTES;
  
/* Length limits for strings. ZAB will allocate an extra byte for the terminator */
#define ZABTEST_STRING_LENGTH                  32



/* Attribute Data storage
 * All strings are allocated an extra byte for a zero terminator.*/
typedef struct
{
  szl_uint8  ZtNoData;
  szl_uint8  ZtData8;
  szl_uint16 ZtData16;
  szl_uint32 ZtData24;
  szl_uint32 ZtData32;
  szl_uint64 ZtData40;
  szl_uint64 ZtData48;
  szl_uint64 ZtData56;
  szl_uint64 ZtData64;
  szl_bool   ZtBoolean;
  szl_uint8  ZtBitmap8;
  szl_uint16 ZtBitmap16;
  szl_uint32 ZtBitmap24;
  szl_uint32 ZtBitmap32;
  szl_uint64 ZtBitmap40;
  szl_uint64 ZtBitmap48;
  szl_uint64 ZtBitmap56;
  szl_uint64 ZtBitmap64;
  szl_uint8  ZtUInt8;
  szl_uint16 ZtUInt16;
  szl_uint32 ZtUInt24;
  szl_uint32 ZtUInt32;
  szl_uint64 ZtUInt40;
  szl_uint64 ZtUInt48;
  szl_uint64 ZtUInt56;
  szl_uint64 ZtUInt64;
  szl_int8   ZtInt8;
  szl_int16  ZtInt16;
  szl_int32  ZtInt24;
  szl_int32  ZtInt32;
  szl_int64  ZtInt40;
  szl_int64  ZtInt48;
  szl_int64  ZtInt56;
  szl_int64  ZtInt64;
  szl_int8   ZtEnum8;
  szl_int16  ZtEnum16;
  //         ZtSemiPrec;
  float      ZtSinglePrec;
  double     ZtDoublePrec;
  char       ZtOctetStr[ZABTEST_STRING_LENGTH+1];
  char       ZtCharStr[ZABTEST_STRING_LENGTH+1];
  //         ZtLongOctetStr;
  //         ZtLongCharStr;
  //         ZtArray;
  //         ZtStruct;
  //         ZtSet;
  //         ZtBag;
  szl_uint32 ZtTOD;
  szl_uint32 ZtDate;
  szl_uint32 ZtUTC;
  szl_uint16 ZtClusterId;
  szl_uint16 ZtAttrId;
  szl_uint32 ZtBACOID;
  szl_uint64 ZtIeeeAddr;
  szl_uint8  Zt128BitSecKey[16];
  szl_uint8  ZtUnknown;
}SzlPlugin_ZabTest_Attributes_t;

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Plugin Initialisation
 * This function must be called from the application after the Library has been initialized.
 * 
 * Parameters:
 *  Service: Instance of library
 *  numEndpoints: The length of endpointList
 *  endpointList: The endpoints to be initialised with this cluster
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_ZabTestCluster_Init(zabService* Service, 
                                           szl_uint8 numEndpoints, 
                                           szl_uint8 * endpointList);

/******************************************************************************
 * Plugin Destruction
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_ZabTestCluster_Destroy(zabService* Service);

/******************************************************************************
 * Get pointer to ZabTest Cluster Attributes for an endpoint
 * This can be used to get access to the endpoints parameters for the local 
 *  application to read or write them
 * Return NULL if not found
 ******************************************************************************/
extern 
SzlPlugin_ZabTest_Attributes_t* SzlPlugin_ZabTestCluster_GetAttributePointer(zabService* Service, szl_uint8 endpoint);

/******************************************************************************
 * Set values to min, max or typical values
 ******************************************************************************/
extern 
void SzlPlugin_ZabTestCluster_SetData(zabService* Service, szl_uint8 endpoint, SzlPlugin_ZabTestSetData_t SetData);


/******************************************************************************
 * CLIENT: Read all attributes from a ZabTestCluster server
 ******************************************************************************/
extern 
void SzlPlugin_ZabTestCluster_ReadAll(zabService* Service, SZL_EP_t SourceEndpoint, SZL_Addresses_t DestAddrMode);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /*_ZAB_TEST_CLUSTER_H_*/
