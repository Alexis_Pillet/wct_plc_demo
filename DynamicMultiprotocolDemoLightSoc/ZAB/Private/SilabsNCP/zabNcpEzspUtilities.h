/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Utility Commands/Responses - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.012  20-Jul-16   MvdB   Original
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/
#ifndef __ZAB_NCP_EZSP_UTILITIES_H__
#define __ZAB_NCP_EZSP_UTILITIES_H__

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Get Manufacturing Token Request + Response Handler
 ******************************************************************************/
void zabNcpEzspUtilities_GetMfgToken(erStatus* Status, zabService* Service, EzspMfgTokenId TokenId);
void zabNcpEzspUtilities_GetMfgTokenResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Set Manufacturing Token Request + Response Handler
 ******************************************************************************/
void zabNcpEzspUtilities_SetMfgToken(erStatus* Status, zabService* Service, EzspMfgTokenId TokenId, unsigned8 TokenDataLength, unsigned8* TokenData);
void zabNcpEzspUtilities_SetMfgTokenResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Get Local IEEE Address Request + Response Handler
 ******************************************************************************/
void zabNcpEzspUtilities_GetNodeIeee(erStatus* Status, zabService* Service);
void zabNcpEzspUtilities_GetNodeIeeeResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Get Local Network Address Request + Response Handler
 ******************************************************************************/
void zabNcpEzspUtilities_GetNodeId(erStatus* Status, zabService* Service);
void zabNcpEzspUtilities_GetNodeIdResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Stack Token Changed Indication Handler
 ******************************************************************************/
void zabNcpEzspUtilities_StackTokenChangedHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/