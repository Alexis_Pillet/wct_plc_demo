/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/******************************************************************************
* File Name     : r_bsp_api.c
* Device(s)     : RX631
* Tool-chain    : KPIT GNURX-ELF 16.01 / CC-RX 2.05
* H/W platform  : G-CPX3
* Description   : Sample software
******************************************************************************/

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/
#if defined(__RENESAS__)
/* Intrinsic functions provided by compiler. */
#include <machine.h>
#else
#include "CCRXmachine.h"
#endif

#include "r_config.h"
#if MCU_RX631 == 1
#include "iorx631.h"
#else
#error "Please specify MCU type."
#endif
#include "r_typedefs.h"
#include "r_bsp_api.h"
#include "r_timer_api.h"
#include "vect.h"
#include "secaddr.h"

/******************************************************************************
   Macro definitions
******************************************************************************/
#define TX_BUSY             (1u) /* UART transmission busy     */
#define TX_NOT_BUSY         (0u) /* UART transmission not busy */
#define RX_FULL             (1u) /* UART reception full        */
#define RX_NOT_FULL         (0u) /* UART reception not full    */
#define RX_EMPTY            (1u) /* UART reception empty       */
#define RX_NOT_EMPTY        (0u) /* UART reception not empty   */
#define RX_ON               (1u) /* UART reception on          */
#define RX_OFF              (0u) /* UART reception off         */

#define COM_XON             (0x11u) /* Xon command */
#define COM_XOFF            (0x13u) /* Xoff command */

#define R_BAUD_RATE_TH      (13000u) /* threshold:13000bps */

/******************************************************************************
   Typedef definitions
******************************************************************************/
/*!
   \enum r_bsp_sci_t
   \brief Enumeration for the SCI channels
 */
typedef enum
{
    R_BSP_SCI_0  = 0,  // SCI 0
    R_BSP_SCI_1  = 1,  // SCI 1
    R_BSP_SCI_2  = 2,  // SCI 2
    R_BSP_SCI_3  = 3,  // SCI 3
    R_BSP_SCI_4  = 4,  // SCI 4
    R_BSP_SCI_5  = 5,  // SCI 5
    R_BSP_SCI_6  = 6,  // SCI 6
    R_BSP_SCI_7  = 7,  // SCI 7
    R_BSP_SCI_8  = 8,  // SCI 8
    R_BSP_SCI_9  = 9,  // SCI 9
    R_BSP_SCI_10 = 10, // SCI 10
    R_BSP_SCI_11 = 11, // SCI 11
    R_BSP_SCI_12 = 12  // SCI 12

} r_bsp_sci_t;

/*!
   \enum r_bsp_uart_tx_t
   \brief UART transmission structure
 */
typedef struct
{
    const uint8_t*  pdata; // Pointer to the data to be sent
    uint32_t        size;   // Number of bytes to send
    uint32_t        cnt;    // Number of bytes already transmitted
    r_bsp_tx_busy_t busy;   // Flag of UART Tx busy or free

} r_bsp_uart_tx_t;

/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/

/******************************************************************************
Private global variables and functions
******************************************************************************/

/******************************************************************************
   Private global variables and functions
******************************************************************************/
static r_bsp_sci_t                      cpx_uart_ch;
static volatile r_bsp_uart_tx_t*        pcpx_uart_tx_data = NULL;
static volatile struct st_sci0**        pcpx_sci_channel = NULL;
static r_bsp_sci_t                      host_uart_ch;
static volatile r_bsp_uart_tx_t*        phost_uart_tx_data = NULL;
static volatile struct st_sci0**        phost_sci_channel = NULL;
static r_bsp_sci_t                      debug_uart_ch;
static volatile r_bsp_uart_tx_t*        pdebug_uart_tx_data = NULL;
static volatile struct st_sci0**        pdebug_sci_channel = NULL;

static r_bsp_sci_rx_callback_t          uart0_rx_finished_callback      = NULL; // UART 0 Rx finished callback pointer
static r_bsp_sci_rx_callback_t          uart9_rx_finished_callback      = NULL; // UART 2 Rx finished callback pointer
static r_bsp_sci_rx_callback_t          uart5_rx_finished_callback      = NULL; // UART 5 Rx finished callback pointer
static r_bsp_callback_t                 uart0_tx_finished_callback      = NULL; // UART 0 Tx finished callback pointer
static r_bsp_callback_t                 uart9_tx_finished_callback      = NULL; // UART 2 Tx finished callback pointer
static r_bsp_callback_t                 uart5_tx_finished_callback      = NULL; // UART 5 Tx finished callback pointer

static r_bsp_callback_t                 cmtimer_tick_callback[2] = {NULL, NULL}; // CM Timer callback pointer
static r_bsp_callback_t                 timer_tick_callback = NULL;              // Timer callback pointer

static volatile struct st_sci0*         psci0_channel = NULL; // SCI 0 structure pointer
static volatile struct st_sci0*         psci9_channel = NULL; // SCI 9 structure pointer
static volatile struct st_sci0*         psci5_channel = NULL; // SCI 5 structure pointer

static volatile r_bsp_uart_tx_t         uart0_tx_data = {NULL, 0u, 0u, R_BSP_TX_NOT_BUSY}; // UART 0 tx data structure
static volatile r_bsp_uart_tx_t         uart9_tx_data = {NULL, 0u, 0u, R_BSP_TX_NOT_BUSY}; // UART 2 tx data structure
static volatile r_bsp_uart_tx_t         uart5_tx_data = {NULL, 0u, 0u, R_BSP_TX_NOT_BUSY}; // UART 5 tx data structure

static r_bsp_board_type_t               local_board_type = R_BOARD_NOT_SET;  // Locally used board type
static volatile uint8_t                 cleared_byte     = 0x00u;              // Variable used by R_EXCEP_IcuGroup12, must be global

/******************************************************************************
* Local function headers
******************************************************************************/

/***********************************************************************
* Function Name     : bsp_send_sci0_handle
* Description       : Handling of a transmitted byte on UART 0
* Argument          : none
* Return Value      : none
***********************************************************************/
/*!
   \fn          void bsp_send_sci0_handle(void)
   \brief       Handling of a transmitted byte on UART 0
   \param[in]   None
   \return      None
 */
static void bsp_send_sci0_handle(void);

/***********************************************************************
* Function Name     : bsp_send_sci9_handle
* Description       : Handling of a transmitted byte on UART 9
* Argument          : none
* Return Value      : none
***********************************************************************/
/*!
   \fn          void bsp_send_sci9_handle(void)
   \brief       Handling of a transmitted byte on UART 2
   \param[in]   None
   \return      None
 */
static void bsp_send_sci9_handle(void);

/***********************************************************************
* Function Name     : bsp_send_sci5_handle
* Description       : Handling of a transmitted byte on UART 5
* Argument          : none
* Return Value      : none
***********************************************************************/
/*!
   \fn          void bsp_send_sci5_handle(void)
   \brief       Handling of a transmitted byte on UART 5
   \param[in]   None
   \return      None
 */
static void bsp_send_sci5_handle(void);

/******************************************************************************
* Extern variables
******************************************************************************/
#if !defined(__RENESAS__)
extern char g_ustack;
extern char g_istack;
#endif

/******************************************************************************
* Global variables
******************************************************************************/

/******************************************************************************
Function implementations
******************************************************************************/

/******************************************************************************
* GENERAL PURPOSE FUNCTIONS
******************************************************************************/
/******************************************************************************
* Function Name:R_BSP_InitLeds
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void R_BSP_InitLeds(void)
{
    if (R_BOARD_G_CPX3 == local_board_type)
    {
        /* Initial RSK LED */
        PORT2.PODR.BIT.B3 = 1u; /* P23 = High(LED4 off) */
        PORT2.PDR.BIT.B3  = 1u; /* Set to output. */
        PORT2.PODR.BIT.B4 = 1u; /* P24 = High (LED5 off) */
        PORT2.PDR.BIT.B4  = 1u; /* Set to output. */
    }
    else
    {
        /* nothing */
    }
}
/******************************************************************************
   End of function  R_BSP_InitLeds
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_LedOn
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void R_BSP_LedOn(r_bsp_led_t led)
{
    if (R_BOARD_G_CPX3 == local_board_type)
    {
        switch (led)
        {
            case R_BSP_LED_4:
                PORT2.PODR.BIT.B3 = 0u; /* P23 = Low (LED4 on) */
                break;

            case R_BSP_LED_5:
                PORT2.PODR.BIT.B4 = 0u; /* P24 = Low (LED5 on) */
                break;

            default:
                break;
        }
    }
    else
    {
        /* nothing */
    }
}
/******************************************************************************
   End of function  R_BSP_LedOn
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_LedOff
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void R_BSP_LedOff(r_bsp_led_t led)
{
    if (R_BOARD_G_CPX3 == local_board_type)
    {
        switch (led)
        {
            case R_BSP_LED_4:
                PORT2.PODR.BIT.B3 = 1u; /* P23 = Hi (LED4 off) */
                break;

            case R_BSP_LED_5:
                PORT2.PODR.BIT.B4 = 1u; /* P24 = Hi (LED5 off) */
                break;

            default:
                break;
        }
    }
    else
    {
        /* nothing */
    }
}
/******************************************************************************
   End of function  R_BSP_LedOff
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_ToggleLed
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void R_BSP_ToggleLed(r_bsp_led_t led)
{
    uint8_t led_state;

    if (R_BOARD_G_CPX3 == local_board_type)
    {
        switch (led)
        {
            case R_BSP_LED_4:
                led_state          = PORT2.PODR.BIT.B3;
                PORT2.PODR.BIT.B3 = led_state ^ 1u; /* LED 4 on G-CPX board */
                break;

            case R_BSP_LED_5:
                led_state          = PORT2.PODR.BIT.B4;
                PORT2.PODR.BIT.B4 = led_state ^ 1u; /* LED 5 on G-CPX board */
                break;
                
            default:
                break;
        }
    }
    else
    {
        /* nothing */
    }
}
/******************************************************************************
   End of function  R_BSP_ToggleLed
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_SetBootMode
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void R_BSP_SetBootMode(r_bsp_boot_t mode)
{
    if (R_BOARD_G_CPX3 == local_board_type)
    {
        PORTD.PMR.BIT.B0 = 0;  /* bit port */
        PORTD.PDR.BIT.B0 = 1;  /* Output */
        
        switch (mode)
        {
            case R_BSP_BOOT_UART:
                PORTD.PODR.BIT.B0 = 1; /* UART Boot */
                break;

            case R_BSP_BOOT_SROM:
                PORTD.PODR.BIT.B0 = 0; /* SROM Boot */
                break;
                
            default:
                break;
        }
    }
    else
    {
        /* nothing */
    }
}
/******************************************************************************
   End of function  R_BSP_SetBootMode
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_SetBoardType
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_BSP_SetBoardType(r_bsp_board_type_t board_type)
{
    if (R_BOARD_G_CPX3 == board_type)
    {
        local_board_type    = board_type;
        
        /* UART between RX and CPX  */
        cpx_uart_ch         = R_BSP_SCI_9;
        pcpx_uart_tx_data   = &uart9_tx_data;
        pcpx_sci_channel    = &psci9_channel;

        /* UART between RX and host */
        host_uart_ch        = R_BSP_SCI_0;
        phost_uart_tx_data  = &uart0_tx_data;
        phost_sci_channel   = &psci0_channel;

        debug_uart_ch       = R_BSP_SCI_5;
        pdebug_uart_tx_data = &uart5_tx_data;
        pdebug_sci_channel  = &psci5_channel;
        
        return R_RESULT_SUCCESS;
    }
    
    return R_RESULT_FAILED;
}
/******************************************************************************
   End of function  R_BSP_SetBoardType
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_Cpx3Reset
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void R_BSP_Cpx3Reset(void)
{
    if (R_BOARD_G_CPX3 == local_board_type)
    {
        PORTC.PODR.BIT.B0 = 1u;

        PORTC.PDR.BIT.B0 = 0u;

        /* Reset signal high time >25ms */
        R_TIMER_BusyWait(30u);

        PORTC.PDR.BIT.B0 = 1u;
    }
    else
    {
        /* Invalid */
        return;
    }

}
/******************************************************************************
   End of function  R_BSP_Cpx3Reset
******************************************************************************/

/******************************************************************************
* TIMER FUNCTIONS
******************************************************************************/
/******************************************************************************
* Function Name:R_BSP_SetClock
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void R_BSP_SetClock(void)
{
    /* Configure the MCU clocks:
       PLL input divided x1, output multiplied x16
       Clocks sourced from main clock oscillator
       System clk: PLL/2
       Periph. clk B: PLL/4
       Ext. bus clk: PLL/16
       Flash. bus clk: PLL/4
       BCLK output at P53 (conditional at compile time) */

    volatile uint16_t i;

    SYSTEM.PRCR.WORD = 0xA503u; /* Protection off */

    /* BCLK output */
    SYSTEM.SCKCR.BIT.PSTOP1 = 0x01u; /* BCLK pin output is disabled */

#if MCU_RX631 == 1
    /* SDCLK output */
    SYSTEM.SCKCR.BIT.PSTOP0 = 0x01u; /* SDCLK(exernal-SDRAM Clock) pin output is disabled */
#endif

    /* Clock configuration */
    SYSTEM.SOSCCR.BIT.SOSTP = 0x01u; /* Sub-clock oscillator is stopped */
    SYSTEM.MOSCWTCR.BYTE    = 0x0Du; /* Main oscillator: Oscillation waiting time = 131072 cycles */
    SYSTEM.PLLWTCR.BYTE     = 0x0Fu; /* PLL: Waiting time = 4194304 cycles */
    SYSTEM.PLLCR.BIT.PLIDIV = 0x00u; /* PLL Input Frequency: x1 */
    SYSTEM.PLLCR.BIT.STC    = 0x0Fu; /* Frequency Multiplication: x16 */
    SYSTEM.MOSCCR.BIT.MOSTP = 0x00u; /* Main clock oscillator is operating */
    SYSTEM.PLLCR2.BIT.PLLEN = 0x00u; /* PLL is operating */

    /* Wait over 12 ms */
    for (i = 0 ; i < 500u ; i++)
    {
        nop();
    }

    SYSTEM.SCKCR.LONG       = 0x21042211; /* Clock selections (maximum for FCLK is 50MHz) */
    SYSTEM.SCKCR3.BIT.CKSEL = 4;          /* Change clock source LOCO -> PLL */
    SYSTEM.PRCR.WORD        = 0xA500u;     /* Protection on */

    /* Enable interrupts */
    R_BSP_EnableInterrupt();

}
/******************************************************************************
   End of function  R_BSP_SetClock
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_ConfigureTimer
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_BSP_ConfigureTimer(uint32_t pclk,
                                    uint32_t tick_ms,
                                    uint8_t priority,
                                    r_bsp_clk_out_t timer_out,
                                    r_bsp_callback_t callback)
{
    r_result_t ret;
    uint32_t       timer_ms_count;
    uint32_t       timer_period;
    const uint16_t timer_divider[7] = { 1, 2, 8, 32, 64, 1024, 8192 };
    const uint8_t  timer_div_val[7] = { 0x08u, 0x09u, 0x0Au, 0x0Bu, 0x0Cu, 0x0Du, 0x0Eu };
    uint16_t       idx;

        /* Callbacks registration */
    timer_tick_callback = callback;

    /* Start divider search algorithm */
    timer_ms_count  = pclk / 1000u;
    timer_ms_count *= tick_ms;
    idx             = 0;

    do
    {
        timer_period = timer_ms_count / timer_divider[idx];
        idx++;
    } while ((idx < 7u) && (timer_period > 65535u));

    /* Check whether divider found is valid */
    if (timer_period > 65535u)
    {
        ret = R_RESULT_FAILED;
    }
    else
    {
        idx--;
        ret             = R_RESULT_SUCCESS;
    }

    /* Proceed to timer configuration */
    if (R_RESULT_SUCCESS == ret )
    {
        /* Timer configuration */
        /* Protection off */
        SYSTEM.PRCR.WORD = 0xA503u;

        /* Get module out of stop */
        MSTP(TMR0) = 0;
        MSTP(TMR1) = 0;

        /* Protection on */
        SYSTEM.PRCR.WORD = 0xA500u;

        /* Disable interrupts */
        TMR0.TCR.BIT.CMIEB = 0; //Compare match B interrupt requests (CMIBm) are disabled
        TMR0.TCR.BIT.CMIEA = 0; //Compare match A interrupt requests (CMIAm) are disabled
        TMR0.TCR.BIT.OVIE  = 0; //Overflow interrupt requests (OVIm) are disabled

        /* Put timer match signal to output */
        if ( R_BSP_CLK_OUT_ON == timer_out )
        {
            /* Assign pin to TDO0 */
            PORT2.PMR.BIT.B2 = 1; //Assign pin to peripheral

            /* Assign SCI to peripheral */
            MPC.PWPR.BIT.B0WI   = 0; //Writing to the PFSWE bit is enabled
            MPC.PWPR.BIT.PFSWE  = 1; //Writing to the PFS register is enabled
            MPC.P22PFS.BIT.PSEL = 5; //Assign to TDO0
            MPC.PWPR.BIT.B0WI   = 1; //Writing to the PFSWE bit is disabled
            MPC.PWPR.BIT.PFSWE  = 0; //Writing to the PFS register is disabled
        }
        else
        {
            /* Do not modify */
        }

        /* Clear any pending ISR */
        IR(TMR0, CMIA0) = 0;
        IR(TMR0, CMIB0) = 0;
        IR(TMR0, OVI0)  = 0;

        /* Set interrupt priority */
        IPR(TMR0, CMIA0) = priority;
        IPR(TMR0, CMIB0) = priority;
        IPR(TMR0, OVI0)  = priority;

        /* Enable interrupt source */
        IEN(TMR0, CMIA0) = 1;
        IEN(TMR0, CMIB0) = 1;
        IEN(TMR0, OVI0)  = 1;

        /* Configure the timer, cascading TMR0-TMR1 */
        TMR01.TCNT        = 0x0000u;                         //Clear the counter
        TMR01.TCORA       = (uint16_t)(timer_period);       //Initialize compare register A
        TMR01.TCORB       = (uint16_t)(timer_period / 10u);  //Initialize compare register B
        TMR1.TCCR.BYTE    = timer_div_val[idx];             //Uses internal clock calculated divider
        TMR0.TCCR.BYTE    = 0x18u;                           //Counts at TMR1.TCNT overflow signal
        TMR0.TCR.BIT.CCLR = 1;                              //Cleared by compare match A
        TMR0.TCSR.BIT.OSA = 2;                              //High is output when compare match A occurs
        TMR0.TCSR.BIT.OSB = 1;                              //Low is output when compare match B occurs

        /* Enable interrupts */
        TMR0.TCR.BIT.CMIEA = 1; //Compare match A interrupt requests (CMIAm) are enabled
    }

    return ret;
}
/******************************************************************************
   End of function  R_BSP_ConfigureTimer
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_TimerOff
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void R_BSP_TimerOff(void)
{
    /* Disable interrupts */
    TMR0.TCR.BIT.CMIEA = 0; //Compare match A interrupt requests (CMIAm) are disabled
}
/******************************************************************************
   End of function  R_BSP_TimerOff
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_ConfigureCMTimer
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_BSP_ConfigureCMTimer(uint32_t pclk,
                                  uint32_t tick_ms,
                                  uint8_t priority,
                                  r_bsp_cmt_timer_id_t timer_id,
                                  r_bsp_callback_t callback)
{
    r_result_t     ret;
    uint32_t       timer_ms_count;
    uint32_t       timer_match;
    const uint16_t timer_divider[4] = { 8u, 32u, 128u, 512u };
    const uint8_t  timer_div_val[4] = { 0u, 1u, 2u, 3u };
    uint8_t        idx;

    /* Timer data initialization */

    /* Callbacks registration */
    cmtimer_tick_callback[timer_id] = callback;

    /* Start divider search algorithm */
    timer_ms_count  = pclk / 1000u;
    timer_ms_count *= tick_ms;
    idx             = 0u;

    do
    {
        timer_match = timer_ms_count / timer_divider[idx];
        idx++;
    } while ((idx < 4u) && (timer_match > 65535u));

    /* Check whether divider found is valid */
    if (timer_match > 65535u)
    {
        ret = R_RESULT_FAILED;
    }
    else
    {
        idx--;
        ret = R_RESULT_SUCCESS;
    }

    /* Proceed to timer configuration */
    if (R_RESULT_SUCCESS == ret)
    {
        /* Timer configuration */
        /* Protection off */
        SYSTEM.PRCR.WORD = 0xA503u;

        if (R_BSP_CMT_TIMER_ID_0 == timer_id)
        {
            /* Get module out of stop */
            MSTP(CMT0) = 0u;

            /* Protection on */
            SYSTEM.PRCR.WORD = 0xA500u;

            /* Disable interrupts */
            CMT0.CMCR.BIT.CMIE = 0u; //Compare match interrupt (CMIn) disabled

            /* Clear any pending ISR */
            IR(CMT0, CMI0) = 0u;

            /* Set interrupt priority */
            IPR(CMT0, CMI0) = priority;

            /* Enable interrupt source */
            IEN(CMT0, CMI0) = 1u;

            /* Configure the timer */
            CMT0.CMCNT        = 0x0000u;             //Clear the counter
            CMT0.CMCR.BIT.CKS = timer_div_val[idx]; //Uses internal clock calculated divider
            CMT0.CMCOR        = (uint16_t)timer_match;        //Uses internal clock calculated divider

            /* Enable interrupts */
            CMT0.CMCR.BIT.CMIE = 1u; //Compare match interrupt (CMIn) enabled
        }
        else
        {
            /* Get module out of stop */
            MSTP(CMT1) = 0u;

            /* Protection on */
            SYSTEM.PRCR.WORD = 0xA500u;

            /* Disable interrupts */
            CMT1.CMCR.BIT.CMIE = 0u; //Compare match interrupt (CMIn) disabled

            /* Clear any pending ISR */
            IR(CMT1, CMI1) = 0u;

            /* Set interrupt priority */
            IPR(CMT1, CMI1) = priority;

            /* Enable interrupt source */
            IEN(CMT1, CMI1) = 1u;

            /* Configure the timer */
            CMT1.CMCNT        = 0x0000u;             //Clear the counter
            CMT1.CMCR.BIT.CKS = timer_div_val[idx]; //Uses internal clock calculated divider
            CMT1.CMCOR        = (uint16_t)timer_match;        //Uses internal clock calculated divider

            /* Enable interrupts */
            CMT1.CMCR.BIT.CMIE = 1u; //Compare match interrupt (CMIn) enabled
        }
    }

    return ret;
}
/******************************************************************************
   End of function  R_BSP_ConfigureCMTimer
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_CMTimerOn
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void R_BSP_CMTimerOn(r_bsp_cmt_timer_id_t timer_id)
{
    if ( R_BSP_CMT_TIMER_ID_0 == timer_id )
    {
        CMT.CMSTR0.BIT.STR0 = 0;      //CMT0.CMCNT count is stopped
        CMT0.CMCNT          = 0x0000u; //Clear the counter
        CMT.CMSTR0.BIT.STR0 = 1;      //CMT0.CMCNT count is started
    }
    else
    {
        CMT.CMSTR0.BIT.STR1 = 0;      //CMT0.CMCNT count is stopped
        CMT1.CMCNT          = 0x0000u; //Clear the counter
        CMT.CMSTR0.BIT.STR1 = 1;      //CMT0.CMCNT count is started
    }
}
/******************************************************************************
   End of function  R_BSP_CMTimerOn
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_CMTimerOff
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void R_BSP_CMTimerOff(r_bsp_cmt_timer_id_t timer_id)
{
    if (R_BSP_CMT_TIMER_ID_0 == timer_id)
    {
        CMT.CMSTR0.BIT.STR0 = 0u; //CMT0.CMCNT count is stopped
    }
    else
    {
        CMT.CMSTR0.BIT.STR1 = 0u; //CMT0.CMCNT count is stopped
    }
}
/******************************************************************************
   End of function  R_BSP_CMTimerOff
******************************************************************************/

/******************************************************************************
* UART COMMUNICATION FUNCTIONS
******************************************************************************/
/******************************************************************************
* Function Name:R_BSP_ConfigureUart
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_BSP_ConfigureUart(uint32_t pclk,
                               uint32_t baudrate,
                               r_bsp_uart_t uart_type,
                               uint8_t tx_priority,
                               uint8_t rx_priority,
                               r_bsp_clk_out_t sck_out,
                               r_bsp_callback_t tx_finished_cb,
                               r_bsp_sci_rx_callback_t rx_finished_cb)
{
    uint8_t                  br_value;
    r_result_t               ret;
    volatile uint32_t        i;
    volatile struct st_sci0* psci_channel;
    r_bsp_sci_t              channel = R_BSP_SCI_0; /* will be set lower to other value */

    if (((0u == baudrate) || (NULL == tx_finished_cb)) || (NULL == rx_finished_cb))
    {
        return R_RESULT_FAILED;
    }

    if (R_BSP_RX_CPX_UART == uart_type)
    {
        channel = cpx_uart_ch;
    }
    else if (R_BSP_RX_HOST_UART == uart_type)
    {
        channel = host_uart_ch;
    }
    else if (R_BSP_RX_DEBUG_UART == uart_type)
    {
        channel = debug_uart_ch;
    }
    else
    {
        /* nothing */
    }

    /* Select SCI channel */
    switch (channel)
    {
        case R_BSP_SCI_0: //SCI 0

            /* Protection off */
            SYSTEM.PRCR.WORD = 0xA503u;

            /* Get module out of stop */
            MSTP(SCI0) = 0u;

            /* Protection on */
            SYSTEM.PRCR.WORD = 0xA500u;

            /* Put baud clock signal to output */
            if (R_BSP_CLK_OUT_ON == sck_out)
            {
                /* Assign pins to peripheral*/
                PORT2.PMR.BIT.B2 = 1u; //SckD0

                /* Assign SCI to peripheral */
                MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
                MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
                MPC.P22PFS.BIT.PSEL = 10u; //Assign to SckD0
                MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
                MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled
            }
            else
            {
                /* Do not modify */
            }

            /* Assign pins to peripheral*/
            PORT2.PMR.BIT.B1 = 1u; //RxD0
            PORT2.PMR.BIT.B0 = 1u; //TxD0

            /* Assign SCI to peripheral */
            MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
            MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
            MPC.P21PFS.BIT.PSEL = 10u; //Assign to RxD0
            MPC.P20PFS.BIT.PSEL = 10u; //Assign to TxD0
            MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
            MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled

            /* Clear any pending ISR */
            IR(SCI0, RXI0) = 0u;
            IR(SCI0, TXI0) = 0u;
            IR(SCI0, TEI0) = 0u;

            /* Set interrupt priority */
            IPR(SCI0, RXI0) = rx_priority;
            IPR(SCI0, TXI0) = tx_priority;
            IPR(SCI0, TEI0) = tx_priority;

            /* Set interrupt priority */
            ICU.IPR[114].BYTE = rx_priority; /* The interrupt priority level of ERIx */

            /* Enable interrupt source */
            ICU.IER[0x0E].BIT.IEN2 = 1u; /* Enable ERIx interrupt */
            ICU.GEN[12].BIT.EN0    = 1u; /* Enable ERI0 of group interrupt */
            IEN(SCI0, RXI0)        = 1u;
            IEN(SCI0, TXI0)        = 1u;

            /* Set channel */
            psci_channel = (volatile struct st_sci0*)&SCI0;
            psci0_channel = psci_channel;

            /* Callbacks registration */
            uart0_tx_finished_callback = tx_finished_cb;
            uart0_rx_finished_callback = rx_finished_cb;
            ret                            = R_RESULT_SUCCESS;
            break;

        case R_BSP_SCI_1: //SCI 1

            /* Protection off */
            SYSTEM.PRCR.WORD = 0xA503u;

            /* Get module out of stop */
            MSTP(SCI1) = 0u;

            /* Protection on */
            SYSTEM.PRCR.WORD = 0xA500u;

            /* Put baud clock signal to output */
            if (R_BSP_CLK_OUT_ON == sck_out)
            {
                /* Assign pins to peripheral*/
                PORT1.PMR.BIT.B7 = 1u; //SckD1

                /* Assign SCI to peripheral */
                MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
                MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
                MPC.P17PFS.BIT.PSEL = 10u; //Assign to SckD1
                MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
                MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled
            }
            else
            {
                /* Do not modify */
            }

            /* Assign pins to peripheral*/
            PORT1.PMR.BIT.B5 = 1u; //RxD1
            PORT1.PMR.BIT.B6 = 1u; //TxD1

            /* Assign SCI to peripheral */
            MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
            MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
            MPC.P15PFS.BIT.PSEL = 10u; //Assign to RxD1
            MPC.P16PFS.BIT.PSEL = 10u; //Assign to TxD1
            MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
            MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled

            /* Clear any pending ISR */
            IR(SCI1, RXI1) = 0u;
            IR(SCI1, TXI1) = 0u;
            IR(SCI1, TEI1) = 0u;

            /* Set interrupt priority */
            IPR(SCI1, RXI1) = rx_priority;
            IPR(SCI1, TXI1) = tx_priority;
            IPR(SCI1, TEI1) = tx_priority;

            /* Set interrupt priority */
            ICU.IPR[114].BYTE = rx_priority; /* The interrupt priority level of ERIx */

            /* Enable interrupt source */
            ICU.IER[0x0E].BIT.IEN2 = 1u; /* Enable ERIx interrupt */
            ICU.GEN[12].BIT.EN1    = 1u; /* Enable ERI1 of group interrupt */
            IEN(SCI1, RXI1) = 1u;
            IEN(SCI1, TXI1) = 1u;

            /* Set channel */
            psci_channel  = (volatile struct st_sci0*)&SCI1;
            ret             = R_RESULT_SUCCESS;
            break;

        case R_BSP_SCI_2: //SCI 2

            /* Protection off */
            SYSTEM.PRCR.WORD = 0xA503u;

            /* Get module out of stop */
            MSTP(SCI2) = 0u;

            /* Protection on */
            SYSTEM.PRCR.WORD = 0xA500u;

            /* Put baud clock signal to output */
            if (R_BSP_CLK_OUT_ON == sck_out)
            {
                /* Assign pins to peripheral*/
                PORT5.PMR.BIT.B1 = 1u; //SckD2

                /* Assign SCI to peripheral */
                MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
                MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
                MPC.P51PFS.BIT.PSEL = 10u; //Assign to SckD2
                MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
                MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled
            }
            else
            {
                /* Do not modify */
            }

            /* Assign pins to peripheral*/
            PORT5.PMR.BIT.B2 = 1u; //RxD2
            PORT5.PMR.BIT.B0 = 1u; //TxD2

            /* Assign SCI to peripheral */
            MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
            MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
            MPC.P52PFS.BIT.PSEL = 10u; //Assign to RxD2
            MPC.P50PFS.BIT.PSEL = 10u; //Assign to TxD2
            MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
            MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled

            /* Clear any pending ISR */
            IR(SCI2, RXI2) = 0u;
            IR(SCI2, TXI2) = 0u;
            IR(SCI2, TEI2) = 0u;

            /* Set interrupt priority */
            IPR(SCI2, RXI2) = rx_priority;
            IPR(SCI2, TXI2) = tx_priority;
            IPR(SCI2, TEI2) = tx_priority;

            /* Set interrupt priority */
            ICU.IPR[114].BYTE = rx_priority; /* The interrupt priority level of ERIx */

            /* Set interrupt priority */
            ICU.IER[0x0E].BIT.IEN2 = 1u; /* Enable ERIx interrupt */
            ICU.GEN[12].BIT.EN2    = 1u; /* Enable ERI2 of group interrupt */
            IEN(SCI2, RXI2)        = 1u;
            IEN(SCI2, TXI2)        = 1u;

            /* Set channel */
            psci_channel  = (volatile struct st_sci0*)&SCI2;
            ret                            = R_RESULT_SUCCESS;
            break;

        case R_BSP_SCI_3: //SCI 3

            /* Protection off */
            SYSTEM.PRCR.WORD = 0xA503u;

            /* Get module out of stop */
            MSTP(SCI3) = 0u;

            /* Protection on */
            SYSTEM.PRCR.WORD = 0xA500u;

            /* Put baud clock signal to output */
            if ( R_BSP_CLK_OUT_ON == sck_out )
            {
                /* Assign pins to peripheral*/
                PORT2.PMR.BIT.B4 = 1u; //SckD3

                /* Assign SCI to peripheral */
                MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
                MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
                MPC.P24PFS.BIT.PSEL = 10u; //Assign to SckD3
                MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
                MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled
            }
            else
            {
                /* Do not modify */
            }

            /* Assign pins to peripheral*/
            PORT2.PMR.BIT.B5 = 1u; //RxD3
            PORT2.PMR.BIT.B3 = 1u; //TxD3

            /* Assign SCI to peripheral */
            MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
            MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
            MPC.P25PFS.BIT.PSEL = 10u; //Assign to RxD3
            MPC.P23PFS.BIT.PSEL = 10u; //Assign to TxD3
            MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
            MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled

            /* Clear any pending ISR */
            IR(SCI3, RXI3) = 0u;
            IR(SCI3, TXI3) = 0u;
            IR(SCI3, TEI3) = 0u;

            /* Set interrupt priority */
            IPR(SCI3, RXI3) = rx_priority;
            IPR(SCI3, TXI3) = tx_priority;
            IPR(SCI3, TEI3) = tx_priority;

            /* Set interrupt priority */
            ICU.IPR[114].BYTE = rx_priority; /* The interrupt priority level of ERIx */

            /* Enable interrupt source */
            ICU.IER[0x0E].BIT.IEN2 = 1u; /* Enable ERIx interrupt */
            ICU.GEN[12].BIT.EN3    = 1u; /* Enable ERI3 of group interrupt */
            IEN(SCI3, RXI3) = 1u;
            IEN(SCI3, TXI3) = 1u;

            /* Set channel */
            psci_channel = (volatile struct st_sci0*)&SCI3;
            ret             = R_RESULT_SUCCESS;
            break;

        case R_BSP_SCI_4: //SCI 4

            /* Protection off */
            SYSTEM.PRCR.WORD = 0xA503u;

            /* Get module out of stop */
            MSTP(SCI4) = 0u;

            /* Protection on */
            SYSTEM.PRCR.WORD = 0xA500u;

            /* Put baud clock signal to output */
            if ( R_BSP_CLK_OUT_ON == sck_out )
            {
                /* Assign pins to peripheral*/
                PORTB.PMR.BIT.B3 = 1u; //SckD4

                /* Assign SCI to peripheral */
                MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
                MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
                MPC.PB3PFS.BIT.PSEL = 10u; //Assign to SckD4
                MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
                MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled
            }
            else
            {
                /* Do not modify */
            }

            /* Assign pins to peripheral*/
            PORTB.PMR.BIT.B0 = 1u; //RxD4
            PORTB.PMR.BIT.B1 = 1u; //TxD4

            /* Assign SCI to peripheral */
            MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
            MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
            MPC.PB0PFS.BIT.PSEL = 10u; //Assign to RxD4
            MPC.PB1PFS.BIT.PSEL = 10u; //Assign to TxD4
            MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
            MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled

            /* Clear any pending ISR */
            IR(SCI4, RXI4) = 0u;
            IR(SCI4, TXI4) = 0u;
            IR(SCI4, TEI4) = 0u;

            /* Set interrupt priority */
            IPR(SCI4, RXI4) = rx_priority;
            IPR(SCI4, TXI4) = tx_priority;
            IPR(SCI4, TEI4) = tx_priority;

            /* Set interrupt priority */
            ICU.IPR[114].BYTE = rx_priority; /* The interrupt priority level of ERIx */

            /* Enable interrupt source */
            ICU.IER[0x0E].BIT.IEN2 = 1u; /* Enable ERIx interrupt */
            ICU.GEN[12].BIT.EN4    = 1u; /* Enable ERI4 of group interrupt */
            IEN(SCI4, RXI4) = 1u;
            IEN(SCI4, TXI4) = 1u;

            /* Set channel */
            psci_channel = (volatile struct st_sci0*)&SCI4;
            ret             = R_RESULT_SUCCESS;
            break;

        case R_BSP_SCI_5: //SCI 5

            /* Protection off */
            SYSTEM.PRCR.WORD = 0xA503u;

            /* Get module out of stop */
            MSTP(SCI5) = 0u;

            /* Protection on */
            SYSTEM.PRCR.WORD = 0xA500u;

            /* Put baud clock signal to output */
            if (R_BSP_CLK_OUT_ON == sck_out)
            {
                /* Assign pins to peripheral*/
                PORTC.PMR.BIT.B4 = 1u; //SckD5

                /* Assign SCI to peripheral */
                MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
                MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
                MPC.PC4PFS.BIT.PSEL = 10u; //Assign to SckD5
                MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
                MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled
            }
            else
            {
                /* Do not modify */
            }

            /* Assign pins to peripheral*/
            PORTC.PMR.BIT.B2 = 1u; //RxD5
            PORTC.PMR.BIT.B3 = 1u; //TxD5

            /* Assign SCI to peripheral */
            MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
            MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
            MPC.PC2PFS.BIT.PSEL = 10u; //Assign to RxD5
            MPC.PC3PFS.BIT.PSEL = 10u; //Assign to TxD5
            MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
            MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled

            /* Clear any pending ISR */
            IR(SCI5, RXI5) = 0u;
            IR(SCI5, TXI5) = 0u;
            IR(SCI5, TEI5) = 0u;

            /* Set interrupt priority */
            IPR(SCI5, RXI5) = rx_priority;
            IPR(SCI5, TXI5) = tx_priority;
            IPR(SCI5, TEI5) = tx_priority;

            /* Set interrupt priority */
            ICU.IPR[114].BYTE = rx_priority; /* The interrupt priority level of ERIx */

            /* Enable interrupt source */
            ICU.IER[0x0E].BIT.IEN2 = 1u; /* Enable ERIx interrupt */
            ICU.GEN[12].BIT.EN5    = 1u; /* Enable ERI5 of group interrupt */
            IEN(SCI5, RXI5) = 1u;
            IEN(SCI5, TXI5) = 1u;

            /* Set channel */
            psci_channel = (volatile struct st_sci0*)&SCI5;
            psci5_channel = psci_channel;
            uart5_tx_finished_callback = tx_finished_cb;
            uart5_rx_finished_callback = rx_finished_cb;
            ret             = R_RESULT_SUCCESS;
            break;

        case R_BSP_SCI_6: //SCI 6

            /* Protection off */
            SYSTEM.PRCR.WORD = 0xA503u;

            /* Get module out of stop */
            MSTP(SCI6) = 0u;

            /* Protection on */
            SYSTEM.PRCR.WORD = 0xA500u;

            /* Put baud clock signal to output */
            if (R_BSP_CLK_OUT_ON == sck_out)
            {
                /* Assign pins to peripheral*/
                PORT3.PMR.BIT.B4 = 1u; //SckD6

                /* Assign SCI to peripheral */
                MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
                MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
                MPC.P34PFS.BIT.PSEL = 10u; //Assign to SckD6
                MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
                MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled
            }
            else
            {
                /* Do not modify */
            }

            /* Assign pins to peripheral*/
            PORT3.PMR.BIT.B3 = 1u; //RxD6
            PORT3.PMR.BIT.B2 = 1u; //TxD6

            /* Assign SCI to peripheral */
            MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
            MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
            MPC.P33PFS.BIT.PSEL = 10u; //Assign to RxD6
            MPC.P32PFS.BIT.PSEL = 10u; //Assign to TxD6
            MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
            MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled

            /* Clear any pending ISR */
            IR(SCI6, RXI6) = 0u;
            IR(SCI6, TXI6) = 0u;
            IR(SCI6, TEI6) = 0u;

            /* Set interrupt priority */
            IPR(SCI6, RXI6) = rx_priority;
            IPR(SCI6, TXI6) = tx_priority;
            IPR(SCI6, TEI6) = tx_priority;

            /* Set interrupt priority */
            ICU.IPR[114].BYTE = rx_priority; /* The interrupt priority level of ERIx */

            /* Enable interrupt source */
            ICU.IER[0x0E].BIT.IEN2 = 1u; /* Enable ERIx interrupt */
            ICU.GEN[12].BIT.EN6    = 1u; /* Enable ERI6 of group interrupt */
            IEN(SCI6, RXI6) = 1u;
            IEN(SCI6, TXI6) = 1u;

            /* Set channel */
            psci_channel = (volatile struct st_sci0*)&SCI6;
            ret             = R_RESULT_SUCCESS;
            break;

        case R_BSP_SCI_7: //SCI 7

            /* Protection off */
            SYSTEM.PRCR.WORD = 0xA503u;

            /* Get module out of stop */
            MSTP(SCI7) = 0u;

            /* Protection on */
            SYSTEM.PRCR.WORD = 0xA500u;

            /* Put baud clock signal to output */
            if (R_BSP_CLK_OUT_ON == sck_out)
            {
                /* Assign pins to peripheral*/
                PORT9.PMR.BIT.B1 = 1u; //SckD7

                /* Assign SCI to peripheral */
                MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
                MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
                MPC.P91PFS.BIT.PSEL = 10u; //Assign to SckD7
                MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
                MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled
            }
            else
            {
                /* Do not modify */
            }

            /* Assign pins to peripheral*/
            PORT9.PMR.BIT.B2 = 1u; //RxD7
            PORT9.PMR.BIT.B0 = 1u; //TxD7

            /* Assign SCI to peripheral */
            MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
            MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
            MPC.P92PFS.BIT.PSEL = 10u; //Assign to RxD7
            MPC.P90PFS.BIT.PSEL = 10u; //Assign to TxD7
            MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
            MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled

            /* Clear any pending ISR */
            IR(SCI7, RXI7) = 0u;
            IR(SCI7, TXI7) = 0u;
            IR(SCI7, TEI7) = 0u;

            /* Set interrupt priority */
            IPR(SCI7, RXI7) = rx_priority;
            IPR(SCI7, TXI7) = tx_priority;
            IPR(SCI7, TEI7) = tx_priority;

            /* Set interrupt priority */
            ICU.IPR[114].BYTE = rx_priority; /* The interrupt priority level of ERIx */

            /* Enable interrupt source */
            ICU.IER[0x0E].BIT.IEN2 = 1u; /* Enable ERIx interrupt */
            ICU.GEN[12].BIT.EN7    = 1u; /* Enable ERI7 of group interrupt */
            IEN(SCI7, RXI7) = 1u;
            IEN(SCI7, TXI7) = 1u;

            /* Set channel */
            psci_channel = (volatile struct st_sci0*)&SCI7;
            ret             = R_RESULT_SUCCESS;
            break;

        case R_BSP_SCI_8: //SCI 8

            /* Protection off */
            SYSTEM.PRCR.WORD = 0xA503u;

            /* Get module out of stop */
            MSTP(SCI8) = 0u;

            /* Protection on */
            SYSTEM.PRCR.WORD = 0xA500u;

            /* Put baud clock signal to output */
            if (R_BSP_CLK_OUT_ON == sck_out)
            {
                /* Assign pins to peripheral*/
                PORTC.PMR.BIT.B5 = 1u; //SckD8

                /* Assign SCI to peripheral */
                MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
                MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
                MPC.PC5PFS.BIT.PSEL = 10u; //Assign to SckD8
                MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
                MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled
            }
            else
            {
                /* Do not modify */
            }

            /* Assign pins to peripheral*/
            PORTC.PMR.BIT.B6 = 1u; //RxD8
            PORTC.PMR.BIT.B7 = 1u; //TxD8

            /* Assign SCI to peripheral */
            MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
            MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
            MPC.PC6PFS.BIT.PSEL = 10u; //Assign to RxD8
            MPC.PC7PFS.BIT.PSEL = 10u; //Assign to TxD8
            MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
            MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled

            /* Clear any pending ISR */
            IR(SCI8, RXI8) = 0u;
            IR(SCI8, TXI8) = 0u;
            IR(SCI8, TEI8) = 0u;

            /* Set interrupt priority */
            IPR(SCI8, RXI8) = rx_priority;
            IPR(SCI8, TXI8) = tx_priority;
            IPR(SCI8, TEI8) = tx_priority;

            /* Set interrupt priority */
            ICU.IPR[114].BYTE = rx_priority; /* The interrupt priority level of ERIx */

            /* Enable interrupt source */
            ICU.IER[0x0E].BIT.IEN2 = 1u; /* Enable ERIx interrupt */
            ICU.GEN[12].BIT.EN8    = 1u; /* Enable ERI8 of group interrupt */
            IEN(SCI8, RXI8) = 1u;
            IEN(SCI8, TXI8) = 1u;

            /* Set channel */
            psci_channel = (volatile struct st_sci0*)&SCI8;
            ret             = R_RESULT_SUCCESS;
            break;

        case R_BSP_SCI_9: //SCI 9

            /* Protection off */
            SYSTEM.PRCR.WORD = 0xA503u;

            /* Get module out of stop */
            MSTP(SCI9) = 0u;

            /* Protection on */
            SYSTEM.PRCR.WORD = 0xA500u;

            /* Put baud clock signal to output */
            if (R_BSP_CLK_OUT_ON == sck_out)
            {
                /* Assign pins to peripheral*/
                PORTB.PMR.BIT.B5 = 1u; //SckD9

                /* Assign SCI to peripheral */
                MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
                MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
                MPC.PB5PFS.BIT.PSEL = 10u; //Assign to SckD9
                MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
                MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled
            }
            else
            {
                /* Do not modify */
            }

            /* Assign pins to peripheral*/
            PORTB.PCR.BIT.B6 = 1u; //Enables an input pull-up
            PORTB.PMR.BIT.B6 = 1u; //RxD9
            PORTB.PMR.BIT.B7 = 1u; //TxD9

            /* Assign SCI to peripheral */
            MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
            MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
            MPC.PB6PFS.BIT.PSEL = 10u; //Assign to RxD9
            MPC.PB7PFS.BIT.PSEL = 10u; //Assign to TxD9
            MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
            MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled

            /* Clear any pending ISR */
            IR(SCI9, RXI9) = 0u;
            IR(SCI9, TXI9) = 0u;
            IR(SCI9, TEI9) = 0u;

            /* Set interrupt priority */
            IPR(SCI9, RXI9) = rx_priority;
            IPR(SCI9, TXI9) = tx_priority;
            IPR(SCI9, TEI9) = tx_priority;

            /* Set interrupt priority */
            ICU.IPR[114].BYTE = rx_priority; /* The interrupt priority level of ERIx */

            /* Enable interrupt source */
            ICU.IER[0x0E].BIT.IEN2 = 1u; /* Enable ERIx interrupt */
            ICU.GEN[12].BIT.EN9    = 1u; /* Enable ERI9 of group interrupt */
            IEN(SCI9, RXI9) = 1u;
            IEN(SCI9, TXI9) = 1u;

            /* Set channel */
            psci_channel = (volatile struct st_sci0*)&SCI9;

            psci9_channel = psci_channel;

            /* Callbacks registration */
            uart9_tx_finished_callback = tx_finished_cb;
            uart9_rx_finished_callback = rx_finished_cb;
            ret                        = R_RESULT_SUCCESS;
            break;

        case R_BSP_SCI_10: //SCI 10

            /* Protection off */
            SYSTEM.PRCR.WORD = 0xA503u;

            /* Get module out of stop */
            MSTP(SCI10) = 0u;

            /* Protection on */
            SYSTEM.PRCR.WORD = 0xA500u;

            /* Put baud clock signal to output */
            if (R_BSP_CLK_OUT_ON == sck_out)
            {
                /* Assign pins to peripheral*/
                PORT8.PMR.BIT.B0 = 1u; //SckD10

                /* Assign SCI to peripheral */
                MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
                MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
                MPC.P80PFS.BIT.PSEL = 10u; //Assign to SckD10
                MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
                MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled
            }
            else
            {
                /* Do not modify */
            }

            /* Assign pins to peripheral*/
            PORT8.PMR.BIT.B1 = 1u; //RxD10
            PORT8.PMR.BIT.B2 = 1u; //TxD10

            /* Assign SCI to peripheral */
            MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
            MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
            MPC.P81PFS.BIT.PSEL = 10u; //Assign to RxD10
            MPC.P82PFS.BIT.PSEL = 10u; //Assign to TxD10
            MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
            MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled

            /* Clear any pending ISR */
            IR(SCI10, RXI10) = 0u;
            IR(SCI10, TXI10) = 0u;
            IR(SCI10, TEI10) = 0u;

            /* Set interrupt priority */
            IPR(SCI10, RXI10) = rx_priority;
            IPR(SCI10, TXI10) = tx_priority;
            IPR(SCI10, TEI10) = tx_priority;

            /* Set interrupt priority */
            ICU.IPR[114].BYTE = rx_priority; /* The interrupt priority level of ERIx */

            /* Enable interrupt source */
            ICU.IER[0x0E].BIT.IEN2 = 1u; /* Enable ERIx interrupt */
            ICU.GEN[12].BIT.EN10   = 1u; /* Enable ERI10 of group interrupt */
            IEN(SCI10, RXI10) = 1u;
            IEN(SCI10, TXI10) = 1u;

            /* Set channel */
            psci_channel = (volatile struct st_sci0*)&SCI10;
            ret          = R_RESULT_SUCCESS;
            break;

        case R_BSP_SCI_11: //SCI 11

            /* Protection off */
            SYSTEM.PRCR.WORD = 0xA503u;

            /* Get module out of stop */
            MSTP(SCI11) = 0u;

            /* Protection on */
            SYSTEM.PRCR.WORD = 0xA500u;

            /* Put baud clock signal to output */
            if (R_BSP_CLK_OUT_ON == sck_out)
            {
                /* Assign pins to peripheral*/
                PORT7.PMR.BIT.B5 = 1u; //SckD11

                /* Assign SCI to peripheral */
                MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
                MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
                MPC.P75PFS.BIT.PSEL = 10u; //Assign to SckD11
                MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
                MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled
            }
            else
            {
                /* Do not modify */
            }

            /* Assign pins to peripheral*/
            PORT7.PMR.BIT.B6 = 1u; //RxD11
            PORT7.PMR.BIT.B7 = 1u; //TxD11

            /* Assign SCI to peripheral */
            MPC.PWPR.BIT.B0WI   = 0u;  //Writing to the PFSWE bit is enabled
            MPC.PWPR.BIT.PFSWE  = 1u;  //Writing to the PFS register is enabled
            MPC.P76PFS.BIT.PSEL = 10u; //Assign to RxD11
            MPC.P77PFS.BIT.PSEL = 10u; //Assign to TxD11
            MPC.PWPR.BIT.B0WI   = 1u;  //Writing to the PFSWE bit is disabled
            MPC.PWPR.BIT.PFSWE  = 0u;  //Writing to the PFS register is disabled

            /* Clear any pending ISR */
            IR(SCI11, RXI11) = 0u;
            IR(SCI11, TXI11) = 0u;
            IR(SCI11, TEI11) = 0u;

            /* Set interrupt priority */
            IPR(SCI11, RXI11) = rx_priority;
            IPR(SCI11, TXI11) = tx_priority;
            IPR(SCI11, TEI11) = tx_priority;

            /* Set interrupt priority */
            ICU.IPR[114].BYTE = rx_priority; /* The interrupt priority level of ERIx */

            /* Enable interrupt source */
            ICU.IER[0x0E].BIT.IEN2 = 1u; /* Enable ERIx interrupt */
            ICU.GEN[12].BIT.EN11   = 1u; /* Enable ERI11 of group interrupt */
            IEN(SCI11, RXI11) = 1u;
            IEN(SCI11, TXI11) = 1u;

            /* Set channel */
            psci_channel = (volatile struct st_sci0*)&SCI11;
            ret             = R_RESULT_SUCCESS;
            break;

        default: /* keeping case even if not reachable */
            ret = R_RESULT_FAILED;
            break;
    }

    /* Proceed to UART configuartion */
    if (R_RESULT_SUCCESS == ret )
    {
        /* Baud rate register calculation */
        if (baudrate > R_BAUD_RATE_TH)
        {
            br_value = (uint8_t)((pclk / (16u * baudrate)) - 1u);
        }
        else
        {
            br_value = (uint8_t)((pclk / (32u * baudrate)) - 1u);
        }

        /* Disable interrupts, tx, and rx */
        psci_channel->SCR.BIT.TIE  = 0u; //Disable transmit interrupts
        psci_channel->SCR.BIT.RIE  = 0u; //Disable receive interrupts
        psci_channel->SCR.BIT.TE   = 0u; //Disable transmitter
        psci_channel->SCR.BIT.RE   = 0u; //Disable receiver
        psci_channel->SCR.BIT.TEIE = 0u; //Disable transmit end interrupts

        /* Clear flags */
        psci_channel->SSR.BIT.TEND = 0u;
        psci_channel->SSR.BIT.PER  = 0u;
        psci_channel->SSR.BIT.FER  = 0u;
        psci_channel->SSR.BIT.ORER = 0u;

        /* Configure the UART */
        psci_channel->SCMR.BIT.SMIF = 0u; //SmartCard mode disabled (SCI mode enabled)
        psci_channel->SCR.BIT.CKE   = 1u; //Use internal clock, set SCK pin as output pin
        psci_channel->SMR.BIT.CM    = 0u; //Set for asynchronous mode
        psci_channel->SMR.BIT.CHR   = 0u; //8-bit data
        psci_channel->SMR.BIT.PE    = 0u; //Parity mode disabled
        psci_channel->SMR.BIT.PM    = 0u; //No parity
        psci_channel->SMR.BIT.STOP  = 0u; //1 stop bit

        /* Configure the baud rate */
        psci_channel->SMR.BIT.CKS   = 0u;        //Clock source PCLK/1
        psci_channel->BRR           = br_value; //Baud rate
        if (baudrate > R_BAUD_RATE_TH)
        {
            psci_channel->SEMR.BIT.ABCS = 1u;       //ABCS = 1
        }
        else
        {
            psci_channel->SEMR.BIT.ABCS = 0u;       //ABCS = 0
        }
        
//        if (channel != debug_uart_ch)
//        {
            /* Enable receive interrupts */
            psci_channel->SCR.BYTE    = 0u;    //Disable all
            psci_channel->SCR.BIT.RE  = 1u;    //Enable receive
            psci_channel->SCR.BIT.TE  = 1u;
            psci_channel->SCR.BIT.RIE = 1u;    //Enable receive interrupts
//        }
//        else
//        {
//            psci_channel->SCR.BYTE    = 0u;    //Disable all
//            psci_channel->SCR.BIT.TE  = 1u;
//        }
    }

    return ret;
}
/******************************************************************************
   End of function  R_BSP_ConfigureUart
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_SendUart
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_BSP_SendUart(const uint8_t* pdata,
                          uint32_t size,
                          r_bsp_uart_t channel)
{
    volatile r_bsp_uart_tx_t* puart_tx_data;
    volatile struct st_sci0*  psci_channel;

    if (R_BSP_RX_HOST_UART == channel)
    {
        puart_tx_data  = phost_uart_tx_data;
        psci_channel = *phost_sci_channel;
    }
    else if (R_BSP_RX_CPX_UART == channel)
    {
        puart_tx_data  = pcpx_uart_tx_data;
        psci_channel = *pcpx_sci_channel;
    }
    else if (R_BSP_RX_DEBUG_UART == channel)
    {
        puart_tx_data  = pdebug_uart_tx_data;
        psci_channel = *pdebug_sci_channel;
    }
    else
    {
        return R_RESULT_FAILED;
    }

    if (NULL == psci_channel || NULL == puart_tx_data)
    {
        return R_RESULT_FAILED;
    }

    /* Wait for previous transmission to finish */
    while (R_BSP_TX_NOT_BUSY != puart_tx_data->busy)
    {
        /* Do Nothing */;
    }

    /* UART was not busy transmitting, put data in UART Tx structure */
    puart_tx_data->size = size;
    puart_tx_data->cnt = 0u;
    puart_tx_data->busy = R_BSP_TX_BUSY;

    /* Set up pointer */
    puart_tx_data->pdata = pdata;

    /* Check reception error flags */
    while (0x00u != (psci_channel->SSR.BYTE & 0x38u) )
    {
        /* Make sure that the flags get cleared */
        psci_channel->SSR.BYTE &= 0xC8u;
    }

    /* Start tx by enabling interrupt and sending first byte */
    psci_channel->SCR.BIT.TE   = 1u;
    psci_channel->SCR.BIT.TIE  = 1u;

    /* Increment sent counter */
    (puart_tx_data->cnt)++;

    /* Send first data symbol */
    psci_channel->TDR = puart_tx_data->pdata[0];

    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_BSP_SendUart
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_UartXon
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_BSP_UartXon(r_bsp_uart_t channel)
{
    volatile struct st_sci0*  psci_channel;

    if (R_BSP_RX_HOST_UART == channel)
    {
        psci_channel = *phost_sci_channel;
    }
    else if (R_BSP_RX_CPX_UART == channel)
    {
        psci_channel = *pcpx_sci_channel;
    }
    else
    {
        return R_RESULT_FAILED;
    }

    if (NULL != psci_channel)
    {
        psci_channel->TDR = COM_XON;

        return R_RESULT_SUCCESS;
    }
    else
    {
        return R_RESULT_FAILED;
    }
}
/******************************************************************************
   End of function  R_BSP_UartXon
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_UartXoff
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_BSP_UartXoff(r_bsp_uart_t channel)
{
    volatile struct st_sci0*  psci_channel;

    if (R_BSP_RX_HOST_UART == channel)
    {
        psci_channel = *phost_sci_channel;
    }
    else if (R_BSP_RX_CPX_UART == channel)
    {
        psci_channel = *pcpx_sci_channel;
    }
    else
    {
        return R_RESULT_FAILED;
    }

    if (NULL != psci_channel)
    {
        psci_channel->TDR = COM_XOFF;

        return R_RESULT_SUCCESS;
    }
    else
    {
        return R_RESULT_FAILED;
    }
}
/******************************************************************************
   End of function  R_BSP_UartXoff
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_GetUartSendStatus
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
uint8_t R_BSP_GetUartSendStatus(r_bsp_uart_t channel, r_bsp_tx_busy_t *status)
{
    volatile r_bsp_uart_tx_t*   puart_tx_data;
    volatile struct st_sci0*    psci_channel;

    if (R_BSP_RX_HOST_UART == channel)
    {
        puart_tx_data  = phost_uart_tx_data;
        psci_channel = *phost_sci_channel;
    }
    else if (R_BSP_RX_CPX_UART == channel)
    {
        puart_tx_data  = pcpx_uart_tx_data;
        psci_channel = *pcpx_sci_channel;
    }
    else if (R_BSP_RX_DEBUG_UART == channel)
    {
        puart_tx_data  = pdebug_uart_tx_data;
        psci_channel = *pdebug_sci_channel;
    }
    else
    {
        return R_RESULT_FAILED;
    }

    if (NULL == psci_channel || NULL == puart_tx_data)
    {
        return R_RESULT_INVALID_REQUEST;
    }
    
    *status = puart_tx_data->busy;
    
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_BSP_GetUartSendStatus
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_EnableInterrupt
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
/*!
 * \anchor bsp_enableinterrupt
 * \defgroup enableinterrupt Enable all interrupts
 * \ingroup enableinterrupt
 * The R_BSP_EnableInterrupt() function enables all interrupts.
 */
void R_BSP_EnableInterrupt(void)
{
    setpsw_i();
}
/******************************************************************************
   End of function  R_BSP_EnableInterrupt
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_DisableInterrupt
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
/*!
 * \anchor bsp_disableinterrupt
 * \defgroup disableinterrupt Disable all interrupts
 * \ingroup disableinterrupt
 * The R_BSP_DisableInterrupt() function disables all interrupts.
 */
void R_BSP_DisableInterrupt(void)
{
    clrpsw_i();
}
/******************************************************************************
   End of function  R_BSP_DisableInterrupt
******************************************************************************/


/******************************************************************************
* CRC COMPUTATION FUNCTIONS
******************************************************************************/
/******************************************************************************
* Function Name:R_BSP_ConfigureCrc
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_BSP_ConfigureCrc(uint8_t lms,
                                  uint8_t gps)
{
    if ((lms > 1u) || (gps > 3u))
    {
        return R_RESULT_FAILED;
    }

    /* Protection off */
    SYSTEM.PRCR.WORD = 0xA503u;

    /* turn CRC computation on */
    MSTP(CRC) = 0u;

    /* Protection on */
    SYSTEM.PRCR.WORD = 0xA500u;

    /* Selet CRC polynomial */
    CRC.CRCCR.BIT.DORCLR = 1u; /* clear the CRCDOR register */
    CRC.CRCCR.BIT.LMS    = lms;
    CRC.CRCCR.BIT.GPS    = gps;

    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_BSP_ConfigureCrc
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_ComputeCrc
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_BSP_ComputeCrc(const uint8_t input[],
                                uint16_t length,
                                uint8_t lms,
                                uint8_t gps,
                                uint8_t* pcrc_result)
{
    uint16_t i;

    if (((((R_BSP_CRC_LSB != lms) && (R_BSP_CRC_MSB != lms)) ||
          (((R_BSP_CRC_POLY_8    != gps) && (R_BSP_CRC_POLY_1_16 != gps)) && (R_BSP_CRC_POLY_2_16 != gps))) ||
          (NULL == input)) ||
          (NULL == pcrc_result))
    {
        return R_RESULT_FAILED;
    }

    /* Protection off */
    SYSTEM.PRCR.WORD = 0xA503u;

    /* turn CRC computation on */
    MSTP(CRC) = 0u;

    /* Protection on */
    SYSTEM.PRCR.WORD = 0xA500u;

    /* Selet CRC polynomial */
    CRC.CRCCR.BIT.LMS    = lms;
    CRC.CRCCR.BIT.GPS    = gps;

    CRC.CRCCR.BIT.DORCLR = 1u; /* clear the CRCDOR register */

    /* CRC computation */
    for (i = 0u ; i < length ; i++)
    {
        CRC.CRCDIR = input[i];
    }

    /* read output */
    if (R_BSP_CRC_POLY_8 == gps)
    {
        *pcrc_result = (uint8_t)(CRC.CRCDOR & 0x00FFu);
    }
    else
    {
        *(uint16_t*)pcrc_result = CRC.CRCDOR;
    }

    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_BSP_ComputeCrc
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_ValidateCrc
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_BSP_ValidateCrc(const uint8_t input[],
                                 uint16_t length,
                                 uint8_t lms,
                                 uint8_t gps)
{
    uint16_t i;

    if (((lms > 1u) || (gps > 3u)) || (NULL == input))
    {
        return R_RESULT_FAILED;
    }

    /* Protection off */
    SYSTEM.PRCR.WORD = 0xA503u;

    /* turn CRC computation on */
    MSTP(CRC) = 0u;

    /* Protection on */
    SYSTEM.PRCR.WORD = 0xA500u;

    /* Selet CRC polynomial */
    CRC.CRCCR.BIT.LMS    = lms;
    CRC.CRCCR.BIT.GPS    = gps;

    CRC.CRCCR.BIT.DORCLR = 1u; /* clear the CRCDOR register */

    /* CRC is computed only over data part of packet */
    for (i = 0u ; i < (length - 2u) ; i++)
    {
        CRC.CRCDIR = input[i];
    }

    /* copy CRC value to data in */
    CRC.CRCDIR = input[length - 1u];
    CRC.CRCDIR = input[length - 2u];

    if (0u == CRC.CRCDOR)
    {
        return R_RESULT_SUCCESS;
    }
    else
    {
        return R_RESULT_FAILED;
    }
}
/******************************************************************************
   End of function  R_BSP_ValidateCrc
******************************************************************************/

/******************************************************************************
* STACK CHECKING FUNCTIONS
******************************************************************************/
/******************************************************************************
* Function Name:R_BSP_CheckInStack
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_BSP_CheckInStack(const uint8_t* pparameter)
{
    uint8_t* pstack_begin;
    uint8_t* pstack_end;
    
#if defined(__RENESAS__)
    pstack_begin = (uint8_t*)__sectop("SU");
    pstack_end   = (uint8_t*)__sectop("SI");
#else
    pstack_begin = (uint8_t*)&g_ustack;
    pstack_end   = (uint8_t*)&g_istack;
#endif
    
    if ((pparameter >= pstack_begin) && (pparameter <= pstack_end))
    {
        return R_RESULT_SUCCESS;
    }
    else
    {
        return R_RESULT_FAILED;
    }
}
/******************************************************************************
   End of function  R_BSP_CheckInStack
******************************************************************************/

/******************************************************************************
* RESET FUNCTION
******************************************************************************/
/******************************************************************************
* Function Name:R_BSP_SoftReset
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void R_BSP_SoftReset(void)
{
    /* Protection off */
    SYSTEM.PRCR.WORD = 0xA503u;

    /* Write soft reset register */
    SYSTEM.SWRR = 0xA501u;

    /* Protection on */
    SYSTEM.PRCR.WORD = 0xA500u;
}
/******************************************************************************
   End of function  R_BSP_SoftReset
******************************************************************************/

/******************************************************************************
* IWDT FUNCTIONS
******************************************************************************/
/******************************************************************************
* Function Name:R_BSP_IwdtInit
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void R_BSP_IwdtInit(void)
{
    /* Protection off */
    SYSTEM.PRCR.WORD = 0xA503u;

    /* Activate IWDT oscillator */
    SYSTEM.ILOCOCR.BIT.ILCSTP = 0u;

    /* IWDT control register */
    IWDT.IWDTCR.WORD = 0x3353u; /* use the maximum period of exactly 33.554432 seconds (16384 cycles of (IWDTCLK/256)) for the IWDT */

    /* IWDTRCR */
    IWDT.IWDTRCR.BYTE = 0x80u; /* reset when timer expires */

    /* IWDTCSTPR */
    IWDT.IWDTCSTPR.BYTE = 0x00u; /* count stop is disabled */

    /* Protection on */
    SYSTEM.PRCR.WORD = 0xA500u;
}
/******************************************************************************
   End of function  R_BSP_IwdtInit
******************************************************************************/

/******************************************************************************
* Function Name:R_BSP_IwdtRefresh
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void R_BSP_IwdtRefresh(void)
{
    IWDT.IWDTRR = 0x00u; /* refresh sequence */
    IWDT.IWDTRR = 0xFFu; /* refresh sequence */
}
/******************************************************************************
   End of function  R_BSP_IwdtRefresh
******************************************************************************/


/******************************************************************************
* Function Name:bsp_send_sci0_handle
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
static void bsp_send_sci0_handle(void)
{
    uint8_t    next_byte;

    if ((NULL == psci0_channel) || (NULL == uart0_tx_data.pdata))
    {
        return;
    }

    /* Check reception error flags */
    while (0x00u != (psci0_channel->SSR.BYTE & 0x38u) )
    {
        /* Make sure that the flags get cleared */
        psci0_channel->SSR.BYTE &= 0xC8u;
    }

    if (uart0_tx_data.cnt < uart0_tx_data.size)
    {
        /* Get next element from buffer */
        next_byte = uart0_tx_data.pdata[(uart0_tx_data.cnt)];

        /* Increment counter */
        (uart0_tx_data.cnt)++;

        psci0_channel->TDR = next_byte;
    }
    else
    {
        /* Disable tx interrupt, re-enable the UART */
        psci0_channel->SCR.BIT.TIE = 0u;
        uart0_tx_data.cnt          = 0u;
        uart0_tx_data.size         = 0u;
        uart0_tx_data.busy         = R_BSP_TX_NOT_BUSY;

        if (NULL != uart0_tx_finished_callback)
        {
            /* Call the callback after finishing transmission*/
            uart0_tx_finished_callback();
        }
    }
}
/******************************************************************************
   End of function  bsp_send_sci0_handle
******************************************************************************/

/******************************************************************************
* Function Name:bsp_send_sci9_handle
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
static void bsp_send_sci9_handle(void)
{
    uint8_t          next_byte;

    if ((NULL == psci9_channel) || (NULL == uart9_tx_data.pdata))
    {
        return;
    }

    /* Check reception error flags */
    while (0x00u != (psci9_channel->SSR.BYTE & 0x38u))
    {
        /* Make sure that the flags get cleared */
        psci9_channel->SSR.BYTE &= 0xC8u;
    }

    if (uart9_tx_data.cnt < uart9_tx_data.size)
    {
        /* Get next element from buffer */
        next_byte = uart9_tx_data.pdata[(uart9_tx_data.cnt)];

        /* Increment counter */
        (uart9_tx_data.cnt)++;

        psci9_channel->TDR = next_byte;
    }
    else
    {
        /* Disable tx interrupt, re-enable the UART */
        psci9_channel->SCR.BIT.TIE = 0u;
        uart9_tx_data.cnt          = 0u;
        uart9_tx_data.size         = 0u;
        uart9_tx_data.busy         = R_BSP_TX_NOT_BUSY;

        if (NULL != uart9_tx_finished_callback)
        {
            /* Call the callback after finishing transmission*/
            uart9_tx_finished_callback();
        }
    }
}
/******************************************************************************
   End of function  bsp_send_sci9_handle
******************************************************************************/

/******************************************************************************
* Function Name:bsp_send_sci5_handle
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
static void bsp_send_sci5_handle(void)
{
    uint8_t next_byte;

    if ((NULL == psci5_channel) || (NULL == uart5_tx_data.pdata))
    {
        return;
    }

    /* Check reception error flags */
    while (0x00u != (psci5_channel->SSR.BYTE & 0x38u))
    {
        /* Make sure that the flags get cleared */
        psci5_channel->SSR.BYTE &= 0xC8u;
    }

    if (uart5_tx_data.cnt < uart5_tx_data.size)
    {
        /* Get next element from buffer */
        next_byte = uart5_tx_data.pdata[(uart5_tx_data.cnt)];

        /* Increment counter */
        (uart5_tx_data.cnt)++;

        /* Send byte */
        psci5_channel->TDR = next_byte;
    }
    else
    {
        /* Disable tx interrupt, re-enable the UART */
        psci5_channel->SCR.BIT.TIE = 0u;
        uart5_tx_data.cnt          = 0u;
        uart5_tx_data.size         = 0u;
        uart5_tx_data.busy         = R_BSP_TX_NOT_BUSY;

        if (NULL != uart5_tx_finished_callback)
        {
            /* Call the callback after finishing transmission*/
            uart5_tx_finished_callback();
        }
    }
}
/******************************************************************************
   End of function  bsp_send_sci5_handle
******************************************************************************/

/******************************************************************************
* Interupt handlers
******************************************************************************/

/******************************************************************************
* Function Name:R_EXCEP_Cmt0Cmi0
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
/* CMT0_CMI0 */
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Cmt0Cmi0(void)
{
    /* Stop timer for the time of processing. */
    R_BSP_CMTimerOff(R_BSP_CMT_TIMER_ID_0);

    if (NULL != cmtimer_tick_callback[R_BSP_CMT_TIMER_ID_0])
    {
        /* Call user callback */
        cmtimer_tick_callback[R_BSP_CMT_TIMER_ID_0]();
    }

    /* Re-start timer with counter reset. */
    R_BSP_CMTimerOn(R_BSP_CMT_TIMER_ID_0);
}
/******************************************************************************
   End of function  R_EXCEP_Cmt0Cmi0
******************************************************************************/

/******************************************************************************
* Function Name:R_EXCEP_Cmt1Cmi1
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
/* CMT1_CMI1 */
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Cmt1Cmi1(void)
{
    /* Stop timer for the time of processing. */
    R_BSP_CMTimerOff(R_BSP_CMT_TIMER_ID_1);

    if (NULL != cmtimer_tick_callback[R_BSP_CMT_TIMER_ID_1])
    {
        /* Call user callback */
        cmtimer_tick_callback[R_BSP_CMT_TIMER_ID_1]();
    }

    /* Re-start timer with counter reset. */
    R_BSP_CMTimerOn(R_BSP_CMT_TIMER_ID_1);
}
/******************************************************************************
   End of function  R_EXCEP_Cmt1Cmi1
******************************************************************************/

/******************************************************************************
* Function Name:R_EXCEP_IcuGroup12
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
/* ICU_GROUPL0 interrupt routine for error indication */
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_IcuGroup12(void)
{

    uint8_t          value;

    value = ICU.GRP[12].BIT.IS0;

    if (1u == value) /* ERI0 interrupt? */
    {
        if ((uint8_t)1u == (uint8_t)SCI0.SSR.BIT.ORER)
        {
            R_BSP_ToggleLed(R_BSP_LED_4);   /* LED4 on */
            cleared_byte       = SCI0.RDR;  /* Receive data is annulled.  */
        }

        if ((uint8_t)1u == (uint8_t)SCI0.SSR.BIT.FER)
        {
            R_BSP_ToggleLed(R_BSP_LED_4);   /* LED4 on */
        }

        SCI0.SSR.BYTE = (SCI0.SSR.BYTE & 0xC7u) | 0xC0u;    /* ORER,FER,PER = 0 */

        while (0x00 != (SCI0.SSR.BYTE & 0x38u))
        {
            /* Confirm that bit is actually 0 */
        }
    }

    value = ICU.GRP[12].BIT.IS5;

    if (1u == value) /* ERI5 interrupt? */
    {
        if ((uint8_t)1u == (uint8_t)SCI5.SSR.BIT.ORER)
        {
            R_BSP_ToggleLed(R_BSP_LED_4);   /* LED4 on */
            cleared_byte       = SCI5.RDR;  /* Receive data is annulled.  */
        }

        if ((uint8_t)1u == (uint8_t)SCI5.SSR.BIT.FER)
        {
            R_BSP_ToggleLed(R_BSP_LED_4);   /* LED4 on */
        }

        SCI5.SSR.BYTE = (SCI5.SSR.BYTE & 0xC7u) | 0xC0u;    /* ORER,FER,PER = 0 */

        while (0x00 != (SCI5.SSR.BYTE & 0x38u))
        {
            /* Confirm that bit is actually 0 */
        }
    }

    value = ICU.GRP[12].BIT.IS9;

    if (1u == value) /* ERI9 interrupt? */
    {
        if ((uint8_t)1u == (uint8_t)SCI9.SSR.BIT.ORER)
        {
            R_BSP_ToggleLed(R_BSP_LED_4);   /* LED4 on */
            cleared_byte       = SCI9.RDR;  /* Receive data is annulled.  */
        }

        if ((uint8_t)1u == (uint8_t)SCI9.SSR.BIT.FER)
        {
            R_BSP_ToggleLed(R_BSP_LED_4);   /* LED4 on */
        }

        SCI9.SSR.BYTE = (SCI9.SSR.BYTE & 0xC7u) | 0xC0u;    /* ORER,FER,PER = 0 */

        while (0x00 != (SCI9.SSR.BYTE & 0x38u))
        {
            /* Confirm that bit is actually 0 */
        }
    }
    
    ICU.IR[114].BIT.IR = 0u;

}
/******************************************************************************
   End of function  R_EXCEP_IcuGroup12
******************************************************************************/

/******************************************************************************
* Function Name:R_EXCEP_Tmr0Cmia0
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
/* TMR0_CMIA0 */
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Tmr0Cmia0(void)
{
    if (NULL != timer_tick_callback)
    {
        timer_tick_callback();
    }
}
/******************************************************************************
   End of function  R_EXCEP_Tmr0Cmia0
******************************************************************************/

/******************************************************************************
* Function Name:R_EXCEP_Sci0Rxi0
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
/* SCI0_RXI0 */
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci0Rxi0(void)
{
    uint8_t rx_byte;

    if ((NULL != uart0_rx_finished_callback) && (NULL != psci0_channel))
    {
        rx_byte = psci0_channel->RDR;
        uart0_rx_finished_callback(rx_byte);
    }
}
/******************************************************************************
   End of function  R_EXCEP_Sci0Rxi0
******************************************************************************/

/******************************************************************************
* Function Name:R_EXCEP_Sci0Txi0
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
/* SCI0_TXI0 */
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci0Txi0(void)
{
    bsp_send_sci0_handle();
}
/******************************************************************************
   End of function  R_EXCEP_Sci0Txi0
******************************************************************************/

/******************************************************************************
* Function Name:R_EXCEP_Sci0Tei0
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
/* SCI0_TEI0 */
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci0Tei0(void)
{
    /* Disable TX. */
    SCI0.SCR.BIT.TE = 0u;
    SCI0.SCR.BIT.TIE = 0u;
    SCI0.SCR.BIT.TEIE = 0u;
}
/******************************************************************************
   End of function  R_EXCEP_Sci0Tei0
******************************************************************************/

/******************************************************************************
* Function Name:R_EXCEP_Sci5Rxi5
* Description :
* Arguments :
* Return Value :
******************************************************************************/
/* SCI5_RXI5 */
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci5Rxi5(void)
{
    uint8_t rx_byte;

    if ((NULL != uart5_rx_finished_callback) && (NULL != psci5_channel))
    {
        rx_byte = psci5_channel->RDR;
        uart5_rx_finished_callback(rx_byte);
    }
}
/******************************************************************************
   End of function  R_EXCEP_Sci5Rxi5
******************************************************************************/

/******************************************************************************
* Function Name:R_EXCEP_Sci5Txi5
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
/* SCI5_RXI5 */
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci5Txi5(void)
{
    bsp_send_sci5_handle();
}
/******************************************************************************
   End of function  R_EXCEP_Sci5Txi5
******************************************************************************/

/******************************************************************************
* Function Name:R_EXCEP_Sci5Tei5
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
/* SCI5_TEI5 */
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci5Tei5(void)
{
    /* Disable TX. */
    SCI5.SCR.BIT.TE = 0u;
    SCI5.SCR.BIT.TIE = 0u;
    SCI5.SCR.BIT.TEIE = 0u;
}
/******************************************************************************
   End of function  R_EXCEP_Sci5Tei5
******************************************************************************/

/******************************************************************************
* Function Name:R_EXCEP_Sci9Rxi9
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
/* SCI9_RXI9 */
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci9Rxi9(void)
{
    uint8_t rx_byte;

    if ((NULL != uart9_rx_finished_callback) && (NULL != psci9_channel))
    {
        rx_byte = psci9_channel->RDR;
        uart9_rx_finished_callback(rx_byte);
    }
}
/******************************************************************************
   End of function  R_EXCEP_Sci9Rxi9
******************************************************************************/

/******************************************************************************
* Function Name:R_EXCEP_Sci9Txi9
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
/* SCI9_RXI9 */
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci9Txi9(void)
{
    bsp_send_sci9_handle();
}
/******************************************************************************
   End of function  R_EXCEP_Sci9Txi9
******************************************************************************/

/******************************************************************************
* Function Name:R_EXCEP_Sci9Tei9
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
/* SCI9_TEI9 */
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci9Tei9(void)
{
    /* Disable TX. */
    SCI9.SCR.BIT.TE = 0u;
    SCI9.SCR.BIT.TIE = 0u;
    SCI9.SCR.BIT.TEIE = 0u;
}
/******************************************************************************
   End of function  R_EXCEP_Sci9Tei9
******************************************************************************/
