/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the NCP Serial Boot Loader manager - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/
#ifndef _ZAB_NCP_SBL_H_
#define _ZAB_NCP_SBL_H_

#include "zabNcpSblTypes.h"

#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 * Create
 * Initialises SBL state machine
 ******************************************************************************/
void zabNcpSbl_Create(erStatus* Status, zabService* Service);

/******************************************************************************
 * Update timeout
 * Check for timeouts within the state machine
 ******************************************************************************/
unsigned32 zabNcpSbl_UpdateTimeout(erStatus* Status, zabService* Service, unsigned32 Time);

/******************************************************************************
 * Action Handler
 * Handles Update action for the SBL state machines
 ******************************************************************************/
void zabNcpSbl_Action(erStatus* Status, zabService* Service, unsigned8 Action);

/******************************************************************************
 * Process an incoming XModem response
 ******************************************************************************/
void zabNcpSbl_XModemResponseHandler(erStatus* Status, zabService* Service, zabSblResponse_t SblResponse);

#ifdef __cplusplus
}
#endif

#endif /* _ZAB_NCP_SBL_H_ */
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/