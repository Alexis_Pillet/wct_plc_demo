/*
 Name:    Private ZNP Shared Definitions
 Author:  ZigBee Excellence Center
 Company: Schneider Electric

 Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

 Description:
    Private for implementation that we do not want to expose outside the module.
 * 
 * 002.002.021  20-Apr-16   MvdB   ARTF167736: Add retry count and message pointer for serial retries
 *                                             Expand function like macros for managing SRSP tracking
*/

#ifndef __ZAB_ZNP_PRIVATE_H__
#define __ZAB_ZNP_PRIVATE_H__
  
#ifdef __cplusplus
extern "C" {
#endif
#include "zabZnpClone.h"
#include "zabZnpOpenTypes.h"
#include "zabVendorConfigureZnp.h"
#include "zabZnpNwkTypes.h"
#include "zabZnpSBLUtilityTypes.h"
#include "zabSerialService.h"
#include "zabZnpSysUtility.h"
#include "wtbUtility.h"
#include "zabZnpRftUtility.h"

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* Max possible value of a (32-bit) Milli-Second timer.
 * This is used when handling wrapped ms time */
#define ZAB_ZNP_MS_TIMEOUT_MAX  (0x80000000)
  
/* Identifier used to indicate an MT extension is from Schneider.
 * Must match value defined in ZStack build */
#define SCHNEIDER_MT_EXTENSION_IDENTIFIER_VERSION1 (0x80)


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/
  
/* Data for SREQ timeout management */
typedef struct
{
  unsigned8 subsystem;
  unsigned8 commandId;
  unsigned8 tid;  
  unsigned8 retryCount;           // Number of times the commadn has been retried. First tx sets this to zero.
  sapMsg* message;                // Pointer to the active message. Thsi must be freed when message comepleted (fail, success, or ZAB destroy!)
  unsigned32 timeoutTimeMs;       // Timeout time for current ZNP SREQ
} zabZnpPrivate_SreqTimeout;

/* Buffer for receiving serial data from glue */
typedef struct
{
  unsigned8 Length;
  unsigned8 Data[ZAB_VENDOR_MAX_DATA_RX];
} SerialReceiveBuffer_t;

/* ZNP Vendor Service state information */
typedef struct
{   
  znpHardwareType networkProcessorHardwareType;
  unsigned32 lastOneSecondTickMs;
  
  /* Data for SREQ timeout management */
  zabZnpPrivate_SreqTimeout sreqTimeOut;
    
  zabOpenInfo_t zabOpenInfo;      // Vendor Open state information
  zabNwkInfo_t zabNwkInfo;        // Vendor network state information
  zabSblInfo_t zabSblInfo;        // Vendor bootloader information
  zabCloneInfo_t zabCloneInfo;    // Vendor cloning information
    
#ifdef ZAB_VND_CFG_ZNP_ENABLE_RFT
  zabZnpRftUtility_Params zabRftInfo;   // Radio Factory Test
#endif // ZAB_VND_CFG_ZNP_ENABLE_RFT
    
  SerialReceiveBuffer_t SerialReceiveBuffer;
  zabSerialService_ActionOutCallback_t ActionOutCallback;
  zabSerialService_SerialOutCallback_t SerialOutCallback;
  
#ifdef ZAB_VND_CFG_ENABLE_WIRELESS_TEST_BENCH  
  wtbUtility_DataReqNotification_t DataRequestNotificationCallback;
  wtbUtility_ZnpDataCnfNotification_t DataConfirmNotificationCallback;
  wtbUtility_DataIndNotification_t DataIndicationNotificationCallback;
#endif
} zabVendorServiceStruct;


/******************************************************************************
 *                      *****************************
 *                 *****            MACROS           *****
 *                      *****************************
 ******************************************************************************/

// Get vendor Service
#define ZAB_SERVICE_VENDOR( sv ) ((zabVendorServiceStruct*)(ZAB_SRV( (sv) ) ->Vendor))

// Get the ZNP vendor service
#define ZAB_SERVICE_ZNP(srv)  (ZAB_SERVICE_VENDOR(srv))

// Get/Set SRSP Timeout Active/Idle
#define ZAB_SERVICE_ZNP_SET_SRSP_TIMEOUT_ACTIVE(srv, msg, ssreq, cmdreq, tid, timeoutTime, attempt)    do { ZAB_SERVICE_ZNP((srv))->sreqTimeOut.message = (msg); ZAB_SERVICE_ZNP((srv))->sreqTimeOut.subsystem = (ssreq); ZAB_SERVICE_ZNP((srv))->sreqTimeOut.commandId = (cmdreq); ZAB_SERVICE_ZNP((srv))->sreqTimeOut.tid = (tid); ZAB_SERVICE_ZNP((srv))->sreqTimeOut.timeoutTimeMs = (timeoutTime); ZAB_SERVICE_ZNP((srv))->sreqTimeOut.retryCount = (attempt);} while (0)
#define ZAB_SERVICE_ZNP_SET_SRSP_TIMEOUT_IDLE(srv)                                       do { ZAB_SERVICE_ZNP((srv))->sreqTimeOut.subsystem = 0;       ZAB_SERVICE_ZNP((srv))->sreqTimeOut.commandId = 0; } while (0)
#define ZAB_SERVICE_ZNP_UPDATE_SRSP_TIMEOUT(srv, timeoutTime, attempt)    do { ZAB_SERVICE_ZNP((srv))->sreqTimeOut.timeoutTimeMs = (timeoutTime); ZAB_SERVICE_ZNP((srv))->sreqTimeOut.retryCount = (attempt);} while (0)


#define ZAB_SERVICE_ZNP_GET_SRSP_TIMEOUT_ACTIVE(srv)  ((ZAB_SERVICE_ZNP((srv))->sreqTimeOut.subsystem != 0) || (ZAB_SERVICE_ZNP((srv))->sreqTimeOut.commandId != 0))
#define ZAB_SERVICE_ZNP_GET_SRSP_TIMEOUT_IDLE(srv)    ((ZAB_SERVICE_ZNP((srv))->sreqTimeOut.subsystem == 0) && (ZAB_SERVICE_ZNP((srv))->sreqTimeOut.commandId == 0))

#ifdef __cplusplus
}
#endif

#endif
