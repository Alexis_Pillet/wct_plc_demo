# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to create ZigBee Device object (abstract factory)
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from abc import ABC, abstractmethod
from enum import Enum
from data_model.zigbee_pro.iact.iact import IACT
from data_model.zigbee_green_power.power_tag.power_tag_1p import PowerTag1P
from data_model.zigbee_green_power.power_tag.power_tag_3p import PowerTag3P
from data_model.zigbee_green_power.power_tag.power_tag_3pn import PowerTag3PN


class ModelID(Enum):
    IACT = '0b0000'
    POWER_TAG_1P = '0b0001'
    POWER_TAG_3P = '0b0002'
    POWER_TAG_3PN = '0b0003'


class ZigBeeDeviceFactory(ABC):

    def __init__(self):
        super(ZigBeeDeviceFactory, self).__init__()

    @staticmethod
    def get_factory(model_id):
        return factory_storage[model_id]

    @abstractmethod
    def create(self, address, id_wireless_bridge):
        pass


class IACTFactory(ZigBeeDeviceFactory):
    def __init__(self):
        super(IACTFactory, self).__init__()

    def create(self, address, id_wireless_bridge):
        return IACT(address, id_wireless_bridge)


class PowerTag1PFactory(ZigBeeDeviceFactory):
    def __init__(self):
        super(PowerTag1PFactory, self).__init__()

    def create(self, address, id_wireless_bridge):
        return PowerTag1P(address, id_wireless_bridge)


class PowerTag3PFactory(ZigBeeDeviceFactory):
    def __init__(self):
        super(PowerTag3PFactory, self).__init__()

    def create(self, address, id_wireless_bridge):
        return PowerTag3P(address, id_wireless_bridge)


class PowerTag3PNFactory(ZigBeeDeviceFactory):
    def __init__(self):
        super(PowerTag3PNFactory, self).__init__()

    def create(self, address, id_wireless_bridge):
        return PowerTag3PN(address, id_wireless_bridge)


factory_storage = {  # Association between ZigBeeDevice Factory and Model_ID
            ModelID.IACT.value: IACTFactory(),
            ModelID.POWER_TAG_1P.value: PowerTag1PFactory(),
            ModelID.POWER_TAG_3P.value: PowerTag3PFactory(),
            ModelID.POWER_TAG_3PN.value: PowerTag3PNFactory()}



