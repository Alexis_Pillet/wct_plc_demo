
"""
mac.mac_frame.mac_frame_generator
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to parse the frame received from PLC

"""
from .mac_frame_structure import Mac


def is_ack_success_frame(data):
    """Check if the frame is a Success Mac Ack

    Args:
        data: data of the frame

    Returns:
        False is this is not a good frame, True otherwise
    """
    if int(data[0]) != Mac.FrameControl.ACK:
        return False

    if int(data[2]) != Mac.AckStatus.SUCCESS:
        return False

    # Good frame
    return True


def is_ack_error_frame(data):
    """Check if the frame is a Error Mac Ack

    Args:
        data: data of the frame

    Returns:
        A dict with the result of the control
        "is_error_ack": If the frame is a good frame or not
        "ack_status": Status of the mac ack
        example:
            >>> {"is_error_ack": True, "ack_status": 0x01}
    """
    result = {"is_error_ack": False, "ack_status": Mac.AckStatus.SUCCESS}

    if int(data[0]) != Mac.FrameControl.ACK:
        return result

    if int(data[2]) == Mac.AckStatus.SUCCESS:
        return result

    # Good frame
    result["is_error_ack"] = True
    result["ack_status"] = int(data[2])
    return result


def is_uninitialized_state_frame(data):
    """Check if the frame is an uninitialized_state_frame

    Args:
        data: data of the frame

    Returns:
        False is this is not a good frame, True otherwise
    """
    master_frame = bytearray([Mac.Type.STATE, Mac.Network.NETWORK, Mac.NetworkState.UNINITIALIZED])

    if int(data[0]) != Mac.FrameControl.CONTROL:
        return False

    if data[2:] != master_frame:
        return False

    # Good frame
    return True


def is_none_state_frame(data):
    """Check if the frame is an none_state_frame

    Args:
        data: data of the frame

    Returns:
        False is this is not a good frame, True otherwise
    """
    return _check_frame(data, Mac.NetworkState.NONE)


def is_networking_state_frame(data):
    """Check if the frame is an networking_state_frame

    Args:
        data: data of the frame

    Returns:
        False is this is not a good frame, True otherwise
    """
    return _check_frame(data, Mac.NetworkState.NETWORKING)


def is_networked_state_frame(data):
    """Check if the frame is a networked_state_frame

    Args:
        data: data of the frame

    Returns:
        A dict with the result of the control
        "is_networked_frame": If the frame is a good frame or not
        "device_number": Device number of the PLC Slave
        "mac_address": Mac address of the PLC Slave

        example:
            >>> {"is_networked_frame": True, "device_number": 0x01, "mac_address": bytearray([0x00, 0x00]) }
    """
    master_frame = bytearray([Mac.Type.STATE, Mac.Network.NETWORK, Mac.NetworkState.NETWORKED])

    result = {"is_networked_frame": False, "device_number": -1, "mac_address": None}

    if int(data[0]) != Mac.FrameControl.CONTROL:
        return result

    if data[2:len(data)-3] != master_frame:
        return result

    # Good frame
    result["is_networked_frame"] = True
    result["device_number"] = int(data[5])
    result["mac_address"] = bytearray([data[6], data[7]])
    return result


def is_data_frame(data):
    """Check if the frame is a data_frame

    Args:
        data: data of the frame

    Returns:
        A dict with the result of the control
        "is_data_frame": If the frame is a good frame or not
        "payload": Payload of the data frame

        example:
            >>> {"is_data_frame": True, "payload": 0x01}
    """
    result = {"is_data_frame": False, "payload": None}

    if int(data[0]) != Mac.FrameControl.DATA:
        return result

    # Good frame
    result["is_data_frame"] = True
    result["payload"] = data[2:]
    return result


def _check_frame(data, network_status):
    """Check if the frame correspond to the specified network status frame

    Args:
        data: data of the frame

    Returns:
        False is this is not a good frame, True otherwise
    """
    master_frame = bytearray([Mac.Type.STATE, Mac.Network.NETWORK, network_status])

    if int(data[0]) != Mac.FrameControl.CONTROL:
        return False

    if data[2:] != master_frame:
        return False

    return True


def compute_crc(data, length):
    """Calculate the CRC of the data specified

    Args:
        data: data of the frame
        length: length of the data

    Returns:
        CRC of the specified frame
    """
    crc = 0

    # XOR of all bytes
    for i in range(length):
        crc ^= int(data[i])

    return crc


def check_crc(frame):
    """Check if the CRC of the frame is good

    Args:
        frame: data of the frame

    Returns:
        True is the CRC is good, False otherwise
    """
    received_crc = int(frame[len(frame) - 1])

    # We don't consider the crc at the end of the frame
    length_frame = len(frame) - 1
    # local calculation of the CRC
    local_crc = compute_crc(frame, length_frame)

    return received_crc == local_crc
