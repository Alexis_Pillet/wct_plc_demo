#ifndef __PLATFORM__
#define __PLATFORM__

// external signals sent to the main loop
#define SIGNAL_SWITCH_STACK0       0x04
#define SIGNAL_SWITCH_STACK1       0x08

void hwInit();

#endif // __PLATFORM__
