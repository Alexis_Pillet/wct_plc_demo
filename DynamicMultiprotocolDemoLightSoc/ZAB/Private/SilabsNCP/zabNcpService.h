/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the main interface for the ZAB EZSP Vendor - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.002  29-Jun-16   MvdB   Permit join working ok
 *                                 zabNcpAsh added
 * 000.000.003  29-Jun-16   MvdB   Move zabNcpAsh statics to vendor service stuct
 *                                 Support link locking and timeouts in zabNcpEzsp
 * 000.000.004  30-Jun-16   MvdB   Queue generated ZDO responses to respect serial link management
 * 000.000.005  01-Jul-16   MvdB   Support energy scan for quietest channel during form
 *                                 Support mandatory ZDO client commands for MiGenie:
 *                                  - SZL_ZDO_MgmtLqiReq, SZL_ZDO_MgmtBindReq, SZL_ZDO_BindReq, SZL_ZDO_UnBindReq,
 *                                  - SZL_NwkLeaveReq, SZL_ZDO_NwkAddrReq, SZL_ZDO_PowerDescriptorReq ,SZL_ZDO_NodeDescriptorReq
 * 000.000.006  04-Jul-16   MvdB   Add Poll Control Cluster Client plugin
 * 000.000.007  06-Jul-16   MvdB   Add OTA Server Plugin and sample app
 *                                 Upgrade Szl_BuildClustersListFromDataPoints() and SZL_ClusterCmdRegister() to also look at registered commands
 * 000.000.008  11-Jul-16   MvdB   Add Device Manager Plugin and sample app
 *                                 Forward leave indications to SZL
 * 000.000.009  12-Jul-16   MvdB   ARTF174575: Parametise SZL_CB_ClusterCmd_t *PayloadOutSize with max data length application may provide
 *                                             Use in OTA Server plugin
 *                                             Support MAX_APS_PAYLOAD_LENGTH in vendor and SZL
 *                                 Notify errors in Af Messaging acks and data confirms for faster timeouts
 *                                 Give IEEE during open action
 *                                 Give network address during network Init/Form actions
 *                                 Support ZAB_ACTION_NWK_GET_INFO - INCOMPLETE SiLabs Case 110845 - does not currently give good network address or device type before network is initialised
 * 000.000.010  12-Jul-16   MvdB   General upgrade, review and test of zabNcpAsh to handle errors and retries
 *                                  - Add ZAB_DEVELOPER_TEST_MODE for creating errors during testing
 * 000.000.011  19-Jul-16   MvdB   SBL Upgrades: Version checking, Failure management, Detect bootloader active during open
 * 000.000.012  21-Jul-16   MvdB   Support RFT for Silabs
 *                                  - Add zabNcpEzspMfglib, zabNcpEzspUtilities, zabNcpRft
 * 000.000.013  21-Jul-16   MvdB   Support Network Discover and Join
 *                                 Upgrade ZDO responses to use correct network address, not just 0
 * 000.000.014  22-Jul-16   MvdB   Support ZAB_ACTION_SET_TX_POWER - incomplete!
 *                                 Add: zabNcpZdo_MatchDescReq, zabNcpZdo_MgmtNwkUpdateReq, zabNcpZdo_MgmtRtgReq, zabNcpZdo_UserDescReq, zabNcpZdo_UserDescSetReq
 *                                 Add zabNcpEzspMfglib_GetPower for test
 * 000.000.015  25-Jul-16   MvdB   Support multiple endpoints
 * 000.000.016  25-Jul-16   MvdB   Support ZAB_ACTION_CHANGE_CHANNEL and other a-synchronous network parameter changes
 * 000.000.017  26-Jul-16   MvdB   Support ZAB_ACTION_CHANGE_NETWORK_KEY
 * 000.000.018  26-Jul-16   MvdB   Support AF Broadcasts and Multicasts
 * 000.000.019  26-Jul-16   MvdB   Tidy up permit join notifications
 *                                 Replace memcpy with osMemCopy, memset with osMemSet
 *                                 Notify upwards on timeout of data request acks
 * 000.000.020  27-Jul-16   MvdB   Add network processor ping
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 * 000.000.022  29-Jul-16   MvdB   Further cleanup to ready for ZNP/NCP branch merge
 * 000.000.023  02-Aug-16   MvdB   Support better getting of TxPower
 *                                 artf175914: Set MS Code in node descriptor
 *                                 Handle leave requests from network cleanly
 *                                 Fix ZAB_ACTION_EZ_MODE_NWK_STEER
 *****************************************************************************/

/*
 TODO List SiLabs
  - ZAB_ACTION_NWK_GET_INFO - INCOMPLETE SiLabs Case 110845 - ARTF175872
    - Does not currently give good network address or device type before network is initialised
  - ZAB_ACTION_SET_TX_POWER - SILABS #113030 - ARTF175873
   - Get max tx power somehow and use as default
   - After setting during networked operation we cannot get using zabNcpEzspMfglib_GetPower() but thats a bit dirty as it could be compiled out!
  - Send via bind not supported - ARTF175875
  - User descriptor not supported by SiLabs stack
  - Recovery of missed channel change
 */


#ifndef __ZAB_NCP_SERVICE_H__
#define __ZAB_NCP_SERVICE_H__

#include "zabCoreService.h"
#include "zabCorePrivate.h"
#include "zabVendorService.h"


#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************
 *                      ******************************
 *                 *****  VENDOR SERVICE VERSIONING   *****
 *                      ******************************
 ******************************************************************************/
#define ZAB_VENDOR_VERSION_MAJOR   0   // non-backward compatible changes
#define ZAB_VENDOR_VERSION_MINOR   2   // backward compatible changes/enhancements
#define ZAB_VENDOR_VERSION_RELEASE 2   // given to QA for testing new version or fixes
#define ZAB_VENDOR_VERSION_BUILD   49  // internal developer build, not shown


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Calculate a CRC16 across a buffer with supplied seed
 ******************************************************************************/
unsigned16 zabNcpService_CalculateCrc16(unsigned16 seed, unsigned8 Length, unsigned8* Buffer);


#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/