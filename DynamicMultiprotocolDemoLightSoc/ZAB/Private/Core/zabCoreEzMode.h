/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZ Mode networking state machine
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *  02.0.00.01  29-Jul-13   MvdB   Original
 * 002.000.009  16-Apr-15   MvdB   Return timeout from zabCoreEzMode_Work()
 ******************************************************************************/

#ifndef _ZAB_CORE_EZ_MODE_H_
#define _ZAB_CORE_EZ_MODE_H_

#include "osTypes.h"

#ifdef ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING

#ifdef __cplusplus
extern "C" {
#endif
  
/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/
  
/* Current Item of the state machine */
typedef enum
{
  ZAB_CORE_EZ_MODE_STATE_IDLE,
  ZAB_CORE_EZ_MODE_STATE_FORM_JOIN,
  ZAB_CORE_EZ_MODE_STATE_NETWORK_DISCOVERY,
  ZAB_CORE_EZ_MODE_STATE_NETWORK_STEER,                    
} zabCoreEzModeState;  
 
/* Result of Network Discovery */
typedef enum
{
  ZAB_CORE_EZ_MODE_DISCOVERY_NO_BEACONS,
  ZAB_CORE_EZ_MODE_DISCOVERY_ONE_NETWORK,
  ZAB_CORE_EZ_MODE_DISCOVERY_MANY_NETWORKS,
  ZAB_CORE_EZ_MODE_DISCOVERY_ONE_SCHNEIDER_NETWORK,
  ZAB_CORE_EZ_MODE_DISCOVERY_MANY_SCHNEIDER_NETWORK,
} zabCoreEzModeDiscoveryResult;  
  
/* State information for this state machine */
typedef struct
{
  zabCoreEzModeState state;           /* Current state of the state machine */
  BeaconFormat_t beacon;
  zabCoreEzModeDiscoveryResult discoveryResult;
} zabCoreEzModeInfo_t;
  
  
/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/ 

/******************************************************************************
 * Out Action Handler
 * This handles EZ Mode actions from the application
 * 
 * Note: For now this is just when actions are put into ZAB
 *       It could be run off the sap is necessary
 ******************************************************************************/
extern
void zabCoreEzMode_ActionOutHandler(erStatus* Status, zabService* Service, zabAction Action);

/******************************************************************************
 * Ask Handler
 * Sets configuration values ASKED by the ZAB, if they are for an EZ-Mode process
 * that is running.
 * 
 * Return Value:
 *  true: Ask has been handled by EzMode. Application should not be asked.
 *  false: Ask was not answered by EzMode. Application should not be asked.
 ******************************************************************************/
extern 
zab_bool zabCoreEzMode_AskCfgHandler(erStatus* Status, zabService* Service, zabAskId CfgId, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer);

/******************************************************************************
 * Give Handler
 * Gets configuration values GIVEN by the ZAB, if they are for an EZ-Mode process
 * that is running.
 * 
 * Return Value:
 *  true: Give has been handled by EzMode. Application should not be given.
 *  false: Give was not answered by EzMode. Application should be given.
 ******************************************************************************/
extern
zab_bool zabCoreEzMode_GiveCfgHandler(erStatus* Status, zabService* Service, zabGiveId CfgId, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer);

/******************************************************************************
 * EZ Mode Work
 * Runs in the core work context
 ******************************************************************************/
extern 
void zabCoreEzMode_NwkStateNotificationHandler(erStatus* Status, zabService* Service, zabNwkState nwkState);

/******************************************************************************
 * EZ Mode Work
 * Runs in the core work context
 ******************************************************************************/
extern
unsigned32 zabCoreEzMode_Work(erStatus* Status, zabService* Service);

/******************************************************************************
 * Initialise EZ Mode
 ******************************************************************************/
extern
void zabCoreEzMode_Init(erStatus* Status, zabService* Service);


/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#ifdef __cplusplus
}
#endif

#endif // ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING
#endif  /* _ZAB_CORE_EZ_MODE_H_ */
