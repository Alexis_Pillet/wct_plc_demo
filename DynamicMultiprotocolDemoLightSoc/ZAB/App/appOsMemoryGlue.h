#ifndef _APP_OS_MEMORY_GLUE_H_
#define _APP_OS_MEMORY_GLUE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "osMemUtility.h"
#include "osTypes.h"

// Memory manager parameters for this project, determined by using histogram
// functionality

// WARNING: if the application needs memory blocks aligned on N-byte boundaries
// (4-byte alignment for instance), it is the developer responsibility to choose
// number and size of blocks giving this result, knowing that all blocks of
// all sizes are placed consecutively in memory. The beginning of the reserved
// memory area is guaranteed to be 8-byte aligned.

// Number of memory block sizes to use (1 to 10)
// Then block sizes must be ordered from smallest to biggest
#define GZIGBEE_MALLOC_NB_BLOCK_SIZES     ( 8U )

// Number and size of blocks for size 0
#define GZIGBEE_MALLOC_BLOCK_NUMBER_0     ( 32U )
#define GZIGBEE_MALLOC_BLOCK_SIZE_0       ( 4U )

// Number and size of blocks for size 1
#define GZIGBEE_MALLOC_BLOCK_NUMBER_1     ( 32U )
#define GZIGBEE_MALLOC_BLOCK_SIZE_1       ( 32U )

// Number and size of blocks for size 2
#define GZIGBEE_MALLOC_BLOCK_NUMBER_2     ( 16U )
#define GZIGBEE_MALLOC_BLOCK_SIZE_2       ( 64U )

// Number and size of blocks for size 3
#define GZIGBEE_MALLOC_BLOCK_NUMBER_3     ( 16U )
#define GZIGBEE_MALLOC_BLOCK_SIZE_3       ( 256U )

// Number and size of blocks for size 4
#define GZIGBEE_MALLOC_BLOCK_NUMBER_4     ( 8U )
#define GZIGBEE_MALLOC_BLOCK_SIZE_4       ( 1024U )

// Number and size of blocks for size 5
#define GZIGBEE_MALLOC_BLOCK_NUMBER_5     ( 2U )
#define GZIGBEE_MALLOC_BLOCK_SIZE_5       ( 2448U )

// Number and size of blocks for size 6
#define GZIGBEE_MALLOC_BLOCK_NUMBER_6     ( 4U )
#define GZIGBEE_MALLOC_BLOCK_SIZE_6       ( 10000U )

// Number and size of blocks for size 7
#define GZIGBEE_MALLOC_BLOCK_NUMBER_7     ( 1U )
#define GZIGBEE_MALLOC_BLOCK_SIZE_7       ( 23000U )

/******************************************************************************
 *                      *****************************
 *                 *****          MACROS           *****
 *                      *****************************
 ******************************************************************************/

/* These macros should be used for accessing appMemGlue_Malloc() and appMemGlue_Free() */
#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
#define APP_MEM_GLUE_MALLOC( _len, _srv_id, _mal_id ) appMemGlue_Malloc( _len, _srv_id, _mal_id )
#else
#define APP_MEM_GLUE_MALLOC( _len, _srv_id, _mal_id ) appMemGlue_Malloc( _len )
#endif

#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
#define APP_MEM_GLUE_FREE( _ptr, _srv_id ) appMemGlue_Free( _ptr, _srv_id )
#else
#define APP_MEM_GLUE_FREE( _ptr, _srv_id ) appMemGlue_Free( _ptr )
#endif

/******************************************************************************
*                      ******************************
*                 ***** EXTERN FUNCTION DECLARATIONS *****
*                      ******************************
******************************************************************************/
/******************************************************************************
* Initialize Malloc Simulator
******************************************************************************/
void appOsMemoryGlue_LocalMallocInit( void );

/******************************************************************************
* Check allocated memory for timeout.
* Warning - only works if GZIGBEE_DEBUG and OS_MEM_UTILITY_USE_MALLOC_ID are defined
******************************************************************************/
void appOsMemoryGlue_LocalMallocCheck( void );

/******************************************************************************
* Simulate Malloc in static memory
******************************************************************************/
#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
  void * appMemGlue_Malloc( size_t  size,
                            unsigned8 ServiceId,
                            MallocId Id );
#else
  void * appMemGlue_Malloc( size_t size );
#endif
/******************************************************************************
* Simulate Free in static memory
******************************************************************************/
#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
  void appMemGlue_Free( void* ptr,
                        unsigned8 ServiceId );
#else
  void appMemGlue_Free( void* ptr );
#endif
  
/******************************************************************************
*                      ******************************
*                 *****          END OF FILE         *****
*                      ******************************
******************************************************************************/
#endif /* _APP_OS_MEMORY_GLUE_H_ */
