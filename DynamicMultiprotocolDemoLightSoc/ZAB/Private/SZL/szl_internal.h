#ifndef _SZL_INTERNAL_H_
#define _SZL_INTERNAL_H_
#include "szl.h"
#include "szl_gp.h"
#include "zabParseProPrivate.h"

#ifdef __cplusplus
extern "C" {
#endif
  
typedef enum
{
    SZL_ADDR_NWK_NONE           = 0xFFFF, /**< Not specified */
    SZL_ADDR_NWK_USE_EXT        = 0xFFFE, /**< Use extended address */
    SZL_ADDR_NWK_RX_IDLE        = 0xFFFD, /**< RxOnWhenIdle */
    SZL_ADDR_NWK_ROUTE          = 0xFFFC, /**< Routers and Coordinators */
    SZL_ADDR_NWK_COORDINATOR    = 0x0000, /**< The Coordinator for the network */
} SZL_ADDR_NWK_RESERVED_t;

typedef struct
{
    SZL_EP_t Ep;
    szl_uint16 ClusterID;
} SZL_ClientCluster_t;


void Szl_InternalInitialize(zabService* Service);
void Szl_InternalInitialized(zabService* Service);
SZL_RESULT_t Szl_InternalIsLibInitialized(zabService* Service);
SZL_RESULT_t Szl_IsRequestValid(zabService* service,  szl_uint16 appFeature );


szl_bool Szl_EndpointRegistered(zabService* service, SZL_EP_t ep);

int Szl_Uint16Compare(const void* a, const void* b);
int Szl_ClientClustersCompare(const void* a, const void* b);
int Szl_DatapointsCompare(const void* a, const void* b);
int Szl_ClusterCmdsCompare(const void* a, const void* b);

SZL_ClusterCmd_t* Szl_ClusterCmdFind(zabService* service, int* startIdx, SZL_EP_t ep, szl_bool manufacturerSpecific, szl_uint16 clusterID, szl_uint8 cmdID, szl_bool responseTypesOnly, szl_bool matchEmptyEntry);
ENUM_SZL_DP_ACCESS_t Szl_ClusterCmdOperation2AccessType(SZL_CLU_OP_TYPE_t opType);

// Multiple entry callbacks
SZL_STATUS_t Szl_AttributeChangedNtfCB(zabService* service, SZL_DataPoint_t* dp);
SZL_STATUS_t Szl_AttributeReportNtfCB(zabService* service, SZL_AttributeReportNtfParams_t* Params);
SZL_STATUS_t Szl_ReceivedRssiNtfCB(zabService* service, SZL_ReceivedSignalStrengthNtfParams_t* Params);


#ifdef SZL_CFG_ENABLE_GREEN_POWER
szl_bool Szl_GpdCommissioningNtfCB(zabService* Service, SZL_GP_GpdCommissioningNtfParams_t* Params, SZL_GP_COMMISSIONING_KEY_MODE_t* KeyMode);
SZL_STATUS_t Szl_GpdCommissionedNtfCB(zabService* Service, SZL_GP_GpdCommissionedNtfParams_t* Params);
#endif

SZL_STATUS_t Szl_NwkLeaveNtfCB(zabService* service, SZL_NwkLeaveNtfParams_t* Params);

SZL_DataPoint_t* Szl_DataPointFind(zabService* service, SZL_EP_t ep, szl_bool manufacturerSpecific, szl_uint16 clusterID, szl_uint16 attributeID, szl_bool matchEmptyEntry);
SZL_STATUS_t Szl_DataPointReadZB(zabService* service, SZL_EP_t endpoint, szl_bool manufacturerSpecific, szl_uint16 clusterID, szl_uint16 attributeID, szl_uint8* data_type, void* data, szl_uint8* lengthInOut);
SZL_STATUS_t Szl_DataPointReadNative(zabService* service, SZL_EP_t endpoint, szl_bool manufacturerSpecific, szl_uint16 clusterID, szl_uint16 attributeID, szl_uint8* data_type, void* data, szl_uint8* lengthInOut);
SZL_STATUS_t Szl_DataPointWriteZB(zabService* service, SZL_EP_t endpoint, szl_bool manufacturerSpecific, szl_uint16 clusterID, szl_uint16 attributeID, szl_uint8 data_type, void* data, szl_uint8 length);
SZL_STATUS_t Szl_DataPointWriteNative(zabService* service, SZL_EP_t endpoint, szl_bool manufacturerSpecific, szl_uint16 clusterID, szl_uint16 attributeID, szl_uint8 data_type, void* data, szl_uint8 length);

SZL_STATUS_t Szl_GetInputClusters(zabService* service, SZL_EP_t ep, szl_uint8 ListMaxSize, szl_uint16* List, szl_uint8* Count);
SZL_STATUS_t Szl_GetOutputClusters(zabService* service, SZL_EP_t ep, szl_uint8 ListMaxSize, szl_uint16* List, szl_uint8* Count);
SZL_STATUS_t Szl_GetAttributes(zabService* service, ENUM_SZL_DP_DIRECTION_t direction, szl_uint16 clusterID, SZL_EP_t ep, szl_bool manufacturerSpecific, szl_uint16 startAttributeID, szl_uint8 listMaxAttributes, szl_bool* listCompleted, void* list, szl_uint8* listCount);

SZL_STATUS_t Szl_ClusterCommandHandler(zabService* service, szl_uint16 leClusterID, szl_uint8 cmd, szl_uint8 ep, szl_bool manufacturerSpecific, szl_uint8* leDataIn, szl_uint8 leDataInLen, szl_uint8* cmdOut, szl_uint8* leDataOut, szl_uint8* leDataOutLen, szl_bool serverToClient, szl_uint8 TransactionId, SZL_Addresses_t sourceAddress, SZL_ADDRESS_MODE_t destAddressMode);

#ifdef __cplusplus
}
#endif
#endif /*_SZL_INTERNAL_H_*/
