/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the main interface for the ZAB serial service
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.02.01  10-Dec-13   MvdB   Original
 * 00.00.04.00  25-Apr-14   MvdB   Fix issue with data in with no FCS
 * 002.000.007  xx-Apr-15   MvdB   ARTF110329: Review handling of serial transmit failures and close when detected.
 * 002.002.021  20-Apr-16   MvdB   ARTF167734: Improve receive parser to drop invalid commands and recover faster
 *                                 ARTF167736: Add ZAB_ACTION_INTERNAL_SERIAL_FLUSH
 * 002.002.037  11-Jan-17   MvdB   ARTF165759: Upgrade zabSerialService_SerialReceiveHandler() to return zabSerialService_ReceiveStatus_t to indicate when work is required
 *****************************************************************************/

#include "zabCoreService.h"
#include "zabNcpPrivate.h"
#include "zabSerialService.h"
#include "zabSerialServicePrivate.h"

#include "ash-protocol.h"

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

#define SERIAL_ACTION_OUT_CALLBACK( S ) (ZAB_SERVICE_VENDOR( S )->ActionOutCallback)
#define SERIAL_DATA_OUT_CALLBACK( S )   (ZAB_SERVICE_VENDOR( S )->SerialOutCallback)



/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                 *****         VENDOR PUBLIC        *****
 *                      ******************************
 ******************************************************************************/

#define SERIAL_RX_BUFFER( S ) (ZAB_SERVICE_VENDOR( S )->SerialReceiveBuffer)


/******************************************************************************
 * Serial Receive Handler
 * Application serial glue must feed ZAB serial bytes received from the network
 * processor via this function.
 * There are no restriction on how bytes are fed to ZAB (one at a time, or in blocks):
 * ZAB will handle all queues, checksums, start of frame etc.
 *
 * Returns zabSerialService_ReceiveStatus_t:
 *  - ZAB_SERIAL_RECEIVE_WORK_NOT_REQUIRED:
 *    - Serial data did not complete a command that requires processing.
 *      No further action required.
 *  - ZAB_SERIAL_RECEIVE_WORK_READY
 *    - Serial data completed a command that requires processing.
 *      zabCoreWork() should be called as soon as possible.
 ******************************************************************************/
zabSerialService_ReceiveStatus_t zabSerialService_SerialReceiveHandler(erStatus* Status,
                                                                       zabService* Service,
                                                                       unsigned8 DataLength,
                                                                       unsigned8* DataPointer)
{
  unsigned16 index;
  sapMsg* Msg;
  zabSerialService_ReceiveStatus_t rxStatus = ZAB_SERIAL_RECEIVE_WORK_NOT_REQUIRED;

  ER_CHECK_STATUS_VALUE(Status, rxStatus);


/*
  print(Service, "ZNP Serial Rx Handler: Received 0x%02X bytes: ", DataLength);
  for (index = 0; index < DataLength; index++)
    {
      printVendor(Service, " %02X", DataPointer[index]);
    }
  printVendor(Service, "\n");
*/

  if (SERIAL_RX_BUFFER(Service).BootloaderMode == zab_true)
    {

      /* XMODEM - Direct pass through */
      Msg = sapMsgAllocateData( Status, zabCoreSapSerial(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, (unsigned16) DataLength );
      if ( erStatusIsOk(Status) && (Msg != NULL) )
        {
          sapMsgSetAppType( Status, Msg, ZAB_MSG_APP_EZSP_XMODEM);
          sapMsgSetAppTransactionId(Status, Msg, 0);
          sapMsgSetAppDataLength(Status, Msg, (unsigned16) DataLength);
          sapMsgCopyAppDataTo(Status, Msg, DataPointer, (unsigned16) DataLength);
          sapMsgPutFree(Status, zabCoreSapSerial(Service), Msg);
        }
      else
        {
          printVendor(Service, "zabSerialService_SerialReceiveHandler: BootloaderMode: Serial data dropped\n");
        }

      /* Whether we were successful or not, ask for work.
       * The only normal reason for failing would be if all buffers were full, in which case work is a good thing to clear them anyway! */
      rxStatus = ZAB_SERIAL_RECEIVE_WORK_READY;
    }
  else
    {


      /* ASH Protocol */
      for (index = 0; index < DataLength; index++)
        {
          switch(DataPointer[index])
            {
              case ASH_FLAG:  /* Frame delimiter (Frame ready)*/
                Msg = sapMsgAllocateData( Status, zabCoreSapSerial(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, (unsigned16) SERIAL_RX_BUFFER(Service).Length );
                if ( erStatusIsOk(Status) && (Msg != NULL) )
                  {
                    sapMsgSetAppType( Status, Msg, ZAB_MSG_APP_EZSP_ASH_CRC);
                    sapMsgSetAppTransactionId(Status, Msg, 0);
                    sapMsgSetAppDataLength(Status, Msg, (unsigned16) SERIAL_RX_BUFFER(Service).Length);
                    sapMsgCopyAppDataTo(Status, Msg, SERIAL_RX_BUFFER(Service).Data, (unsigned16) SERIAL_RX_BUFFER(Service).Length);
                    sapMsgPutFree(Status, zabCoreSapSerial(Service), Msg);
                  }

                /* Whether we were successful or not, ask for work.
                 * The only normal reason for failing would be if all buffers were full, in which case work is a good thing to clear them anyway! */
                rxStatus = ZAB_SERIAL_RECEIVE_WORK_READY;

                SERIAL_RX_BUFFER(Service).Length = 0;
                break;

              case ASH_ESC:   /* byte stuffing escape byte */
                SERIAL_RX_BUFFER(Service).EscapeReceived = zab_true;
                break;

              case ASH_XON:   /* flow control byte - means resume transmission */
              case ASH_XOFF:  /* flow control byte - means suspend transmission */
                /* Not yet supported */
                break;

              case ASH_SUB:   /* replaces bytes w framing, overrun or overflow errors */
              case ASH_CAN:   /* frame cancel byte */
                SERIAL_RX_BUFFER(Service).Length = 0;
                break;

              default:
                if (SERIAL_RX_BUFFER(Service).EscapeReceived == zab_true)
                  {
                    SERIAL_RX_BUFFER(Service).EscapeReceived = zab_false;
                    SERIAL_RX_BUFFER(Service).Data[SERIAL_RX_BUFFER(Service).Length] = (DataPointer[index] ^ ASH_FLIP);
                  }
                else
                  {
                    SERIAL_RX_BUFFER(Service).Data[SERIAL_RX_BUFFER(Service).Length] = DataPointer[index];
                  }
                SERIAL_RX_BUFFER(Service).Length++;
            }
        }
    }

  return rxStatus;
}

/******************************************************************************
 * Open State Notification
 * Application serial glue must feed ZAB notifications via this function.
 ******************************************************************************/
void zabSerialService_NotifyOpenState(erStatus* Status,
                                      zabService* Service,
                                      zabOpenState OpenState)
{
  ER_CHECK_STATUS(Status);
  sapMsgPutNotifyOpenState(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_IN, OpenState);
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                 *****         VENDOR PRIVATE       *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise the Serial SAP In/Out Service
 * Clears all handlers and empties all queues
 ******************************************************************************/
void zabSerialService_Init(erStatus* Status, zabService* Service,
                           zabSerialService_ActionOutCallback_t ActionOutCallback,
                           zabSerialService_SerialOutCallback_t SerialOutCallback)
{
  ER_CHECK_NULL(Status, ActionOutCallback);
  ER_CHECK_NULL(Status, SerialOutCallback);
  SERIAL_ACTION_OUT_CALLBACK(Service) = ActionOutCallback;
  SERIAL_DATA_OUT_CALLBACK(Service) = SerialOutCallback;
  zabSerialService_Reset(Status, Service);
}

/******************************************************************************
 * Reset the Serial SAP In/Out Service
 * Clears data but leaves handlers intact
 ******************************************************************************/
void zabSerialService_Reset(erStatus* Status, zabService* Service)
{
  SERIAL_RX_BUFFER(Service).Length = 0;
  SERIAL_RX_BUFFER(Service).EscapeReceived = zab_false;
  SERIAL_RX_BUFFER(Service).BootloaderMode = zab_false;
}

/******************************************************************************
 * Serial SAP Out Handler
 * Converts the following into nicely formatted function calls for app serial glue:
 *  - Actions: Open/Close
 *  - Application data frames
 ******************************************************************************/
void zabSerialService_OutHandler (erStatus* Status, sapHandle Sap, sapMsg* Message)
{
  zabService* Service;
  sapMsgType Type;
  zabAction Action;
  erStatus glueStatus;
  ER_CHECK_NULL(Status, Message);
  Service = sapCoreGetService(Sap);
  erStatusClear(&glueStatus, Service);

  /* First see if it is an action or application data */
  Type = sapMsgGetType(Message);
  switch (Type)
    {
      case SAP_MSG_TYPE_ACTION:
        /* Perform open or close actions and notify result */
        Action = sapMsgGetAction(Message);
        switch (Action)
          {
            case ZAB_ACTION_OPEN:
            case ZAB_ACTION_CLOSE:
              /* Send the message to the serial glue. Catch any errors and return them as ZAB_ERROR_VENDOR_SENDING */
              if (SERIAL_ACTION_OUT_CALLBACK(Service) != NULL)
                {
                  SERIAL_ACTION_OUT_CALLBACK(Service)(&glueStatus, Service, Action);
                  if (erStatusIsError(&glueStatus))
                    {
                      erStatusSet(Status, ZAB_ERROR_VENDOR_SENDING);
                    }
                }
              else
                {
                  erStatusSet(Status, ZAB_ERROR_VENDOR_SENDING);
                }
              break;

            /* Flush the serial queue on request from vendor.
             * This is done after a command failed to get a response.
             * It may result in another frames getting dropped, but if we do not do it we can have cases where the serial parser is expecting
             * a long frame, so multiple serial SRSP's get consumed by it, and even several retries fail */
            case ZAB_ACTION_INTERNAL_SERIAL_FLUSH:
              printVendor(Service, "ZNP Serial: Rx Flush!!!\n");
              SERIAL_RX_BUFFER(Service).Length = 0;
              break;


            case ZAB_ACTION_INTERNAL_SERIAL_MODE_NORMAL:
              printVendor(Service, "ZNP Serial: NORMAL MODE!!!\n");
              SERIAL_RX_BUFFER(Service).BootloaderMode = zab_false;
              SERIAL_RX_BUFFER(Service).Length = 0;
              break;

            case ZAB_ACTION_INTERNAL_SERIAL_MODE_BOOTLOADER:
              printVendor(Service, "ZNP Serial: BOOTLOADER MODE!!!\n");
              SERIAL_RX_BUFFER(Service).BootloaderMode = zab_true;
              SERIAL_RX_BUFFER(Service).Length = 0;
              break;


            default:
              printError(Service, "zabSerialService_SerialOutHandler: Unknown Action = 0x%02X\n", (unsigned8)Action);
              break;
          }
        break;

      case SAP_MSG_TYPE_APP:
        /* Send serial data */
        if (sapMsgGetAppType(Message) == ZAB_MSG_APP_RAW)
          {
            /* Send the message to the serial glue. Catch any errors and return them as ZAB_ERROR_VENDOR_SENDING */
            if (SERIAL_DATA_OUT_CALLBACK(Service) != NULL)
              {
                SERIAL_DATA_OUT_CALLBACK(Service)(&glueStatus, Service, sapMsgGetAppDataLength(Message), sapMsgGetAppData(Message));
                if (erStatusIsError(&glueStatus))
                  {
                    erStatusSet(Status, ZAB_ERROR_VENDOR_SENDING);
                  }
              }
            else
              {
                erStatusSet(Status, ZAB_ERROR_VENDOR_SENDING);
              }
          }
        else
          {
            printError(Service, "appSerialCallBack: Unknown App Msg Type = 0x%02X\n", sapMsgGetAppType(Message));
          }
        break;

      default:
        printError(Service, "appSerialCallBack: Unknown Msg Type = 0x%02X\n", (unsigned8)Type);
        break;
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/