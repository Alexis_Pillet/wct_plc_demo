 var disable_button = function($id) {
     $('#' + $id).attr('class', 'button button-command-disabled');
     $('#' + $id).attr('disabled', 'true');
   };

    const detail_iact = ({
      name, status, id_wireless_bridge, address, type_device,
      manufacturer_name, model_identifier, application_firmware_version, application_hardware_version,
      product_serial_number, product_identifier, product_model,
      rssi, lqi
    }) => `
    <div>
    <p id="title_device">Wireless Bridge ${id_wireless_bridge + 1} - ${name}</p>
    <section class="box grid">
        <div class="row_1 column_1">
          <p id="title_data">Status: <span class="text-green">${status}</<span></p>
        </div>
        <div class="row_1 column_2">
          <p id="title_data">Manufacturer Name: <span class="text-green">${manufacturer_name}</<span></p>
          <p id="title_data">Model Identifier: <span class="text-green">${model_identifier}</<span></p>
          <p id="title_data">Application Firmware Version: <span class="text-green">${application_firmware_version}</<span></p>
          <p id="title_data">Application Hardware Version: <span class="text-green">${application_hardware_version}</<span></p>
        </div>
        <div class="row_1 column_3">
          <p id="title_data">Product Serial Number: <span class="text-green">${product_serial_number}</<span></p>
          <p id="title_data">Product Identifier: <span class="text-green">${product_identifier}</<span></p>
          <p id="title_data">Product Model: <span class="text-green">${product_model}</<span></p>
        </div>
        <div class="row_2 column_3 margin_top_1">
          <p id="title_detail">Address: <span class="text-green">${address}</<span></p>
          <p id="title_detail">RSSI: <span class="text-green">${rssi} dBm</<span></p>
          <p id="title_detail">LQI: <span class="text-green">${lqi}</<span></p>
        </div>
        <div class="row_3 column_3">
          <iframe name="votar" style="display:none;"></iframe>
          <div class="div_multiple_button">
            <form id="iact_discovery" action="/WirelessBridge" method="post" target="votar">
                <input id='button_discovery_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="DISCOVERY">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="discovery">
            </form>
          </div>
          <div class="div_multiple_button">
            <form id="iact_action" action="/WirelessBridge" method="post" target="votar">
                <input id='button_open_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="OPEN">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="open">
            </form>
          </div>
          <div class="div_multiple_button">
            <form id="iact_action" action="/WirelessBridge" method="post" target="votar">
                <input id='button_close_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="CLOSE">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="close">
            </form>
          </div>
        </div>
    </section>
    </div>`;

    const detail_temp = ({
      name, id_wireless_bridge, address, type_device, co2, temperature, humidity,
      manufacturer_name, model_identifier, application_firmware_version, application_hardware_version,
      product_serial_number, product_identifier, product_model,
      rssi, lqi
    }) => `
    <div>
    <p id="title_device">Wireless Bridge ${id_wireless_bridge + 1} - ${name}</p>
    <section class="box grid">
        <div class="row_1 column_1">
          <p id="title_data">CO2: <span class="text-green">${co2} PPM</<span></p>
          <p id="title_data">Temperature: <span class="text-green">${temperature} °C</<span></p>
          <p id="title_data">Humidity: <span class="text-green">${humidity} %RH</<span></p>
        </div>
        <div class="row_1 column_2">
          <p id="title_data">Manufacturer Name: <span class="text-green">${manufacturer_name}</<span></p>
          <p id="title_data">Model Identifier: <span class="text-green">${model_identifier}</<span></p>
          <p id="title_data">Application Firmware Version: <span class="text-green">${application_firmware_version}</<span></p>
          <p id="title_data">Application Hardware Version: <span class="text-green">${application_hardware_version}</<span></p>
        </div>
        <div class="row_1 column_3">
          <p id="title_data">Product Serial Number: <span class="text-green">${product_serial_number}</<span></p>
          <p id="title_data">Product Identifier: <span class="text-green">${product_identifier}</<span></p>
          <p id="title_data">Product Model: <span class="text-green">${product_model}</<span></p>
        </div>
        <div class="row_2 column_3 margin_top_1">
          <p id="title_detail">Address: <span class="text-green">${address}</<span></p>
          <p id="title_detail">RSSI: <span class="text-green">${rssi} dBm</<span></p>
          <p id="title_detail">LQI: <span class="text-green">${lqi}</<span></p>
        </div>
        <div class="row_3 column_3">
          <iframe name="votar" style="display:none;"></iframe>
          <form id="co2_discovery" action="/WirelessBridge" method="post" target="votar">
              <input id='button_discovery_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="DISCOVERY">
              <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
              <input type="hidden" name="address" value="${address}">
              <input type="hidden" name="type_device" value="${type_device}">
              <input type="hidden" name="name_function" value="discovery">
          </form>
        </div>
    </section>
    </div>`;

    const detail_power_tag_1p = ({
      name, status, id_wireless_bridge, address, type_device,
      manufacturer_name, model_identifier, application_firmware_version, application_hardware_version,
      product_serial_number, product_identifier, product_model,
      rssi, lqi,
      voltage_pha, current_pha,
      cumulated_energy, partial_energy, phase_sequence, ac_alarms_mask, alarms_mask
    }) => `
    <div>
    <p id="title_device">Wireless Bridge ${id_wireless_bridge + 1} - ${name}</p>
    <section class="box grid">
        <div class="row_1 column_1">
          <p id="title_data">Voltage Pha: <span class="text-green">${voltage_pha} V</<span></p>
        </div>

        <div class="row_1 column_2">
          <p id="title_data">Current Pha: <span class="text-green">${current_pha} A</<span></p>
        </div>

        <div class="row_2 column_1 margin_top_1">
          <p id="title_data">Cumulated Energy: <span class="text-green">${cumulated_energy} kWh</<span></p>
          <p id="title_data">Partial Energy: <span class="text-green">${partial_energy} kWh</<span></p>
          <p id="title_data">Phase Sequence: <span class="text-green">${phase_sequence}</<span></p>
          <p id="title_data">AC Alarms Mask: <span class="text-green">${ac_alarms_mask}</<span></p>
          <p id="title_data">Alarms Mask: <span class="text-green">${alarms_mask}</<span></p>
        </div>

        <div class="row_2 column_2 margin_top_1">
          <p id="title_data">Manufacturer Name: <span class="text-green">${manufacturer_name}</<span></p>
          <p id="title_data">Model Identifier: <span class="text-green">${model_identifier}</<span></p>
          <p id="title_data">Application Firmware Version: <span class="text-green">${application_firmware_version}</<span></p>
          <p id="title_data">Application Hardware Version: <span class="text-green">${application_hardware_version}</<span></p>
        </div>

        <div class="row_2 column_3 margin_top_1">
          <p id="title_data">Product Serial Number: <span class="text-green">${product_serial_number}</<span></p>
          <p id="title_data">Product Identifier: <span class="text-green">${product_identifier}</<span></p>
          <p id="title_data">Product Model: <span class="text-green">${product_model}</<span></p>
        </div>

        <div class="row_3 column_3">
          <p id="title_detail">Address: <span class="text-green">${address}</<span></p>
          <p id="title_detail">RSSI: <span class="text-green">${rssi} dBm</<span></p>
          <p id="title_detail">LQI: <span class="text-green">${lqi}</<span></p>
        </div>

        <div class="row_4 column_3">
          <iframe name="votar" style="display:none;"></iframe>
          <div class="div_multiple_button">
            <form id="iact_discovery" action="/WirelessBridge" method="post" target="votar">
                <input id='button_discovery_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="DISCOVERY">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="discovery">
            </form>
          </div>
          <div class="div_multiple_button">
            <form id="iact_discovery" action="/WirelessBridge" method="post" target="votar">
                <input id='button_discovery_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="RESET PARTIAL ENERGY">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="reset_partial_energy">
            </form>
          </div>
        </div>
    </section>
    </div>`;

    const detail_power_tag_3p = ({
      name, status, id_wireless_bridge, address, type_device,
      manufacturer_name, model_identifier, application_firmware_version, application_hardware_version,
      product_serial_number, product_identifier, product_model,
      rssi, lqi,
      voltage_pha_phb, voltage_phb_phc, voltage_phc_pha, current_pha, current_phb, current_phc,
      active_power_pha, active_power_phb, active_power_phc, total_active_power,
      cumulated_energy, partial_energy, phase_sequence, ac_alarms_mask, alarms_mask
    }) => `
    <div>
    <p id="title_device">Wireless Bridge ${id_wireless_bridge + 1} - ${name}</p>
    <section class="box grid">
        <div class="row_1 column_1">
          <p id="title_data">Voltage Pha-Phb: <span class="text-green">${voltage_pha_phb} V</<span></p>
          <p id="title_data">Voltage Phb-Phc: <span class="text-green">${voltage_phb_phc} V</<span></p>
          <p id="title_data">Voltage Phc-Pha: <span class="text-green">${voltage_phc_pha} V</<span></p>
        </div>

        <div class="row_1 column_2">
          <p id="title_data">Current Pha: <span class="text-green">${current_pha} A</<span></p>
          <p id="title_data">Current Phb: <span class="text-green">${current_phb} A</<span></p>
          <p id="title_data">Current Phc: <span class="text-green">${current_phc} A</<span></p>
        </div>

        <div class="row_1 column_3">
          <p id="title_data">Active Power Pha: <span class="text-green">${active_power_pha} W</<span></p>
          <p id="title_data">Active Power Phb: <span class="text-green">${active_power_phb} W</<span></p>
          <p id="title_data">Active Power Phc: <span class="text-green">${active_power_phc} W</<span></p>
          <p id="title_data">Total Active Power: <span class="text-green">${total_active_power} kW</<span></p>
        </div>

        <div class="row_2 column_1 margin_top_1">
          <p id="title_data">Cumulated Energy: <span class="text-green">${cumulated_energy} kWh</<span></p>
          <p id="title_data">Partial Energy: <span class="text-green">${partial_energy} kWh</<span></p>
          <p id="title_data">Phase Sequence: <span class="text-green">${phase_sequence}</<span></p>
          <p id="title_data">AC Alarms Mask: <span class="text-green">${ac_alarms_mask}</<span></p>
          <p id="title_data">Alarms Mask: <span class="text-green">${alarms_mask}</<span></p>
        </div>

        <div class="row_2 column_2 margin_top_1">
          <p id="title_data">Manufacturer Name: <span class="text-green">${manufacturer_name}</<span></p>
          <p id="title_data">Model Identifier: <span class="text-green">${model_identifier}</<span></p>
          <p id="title_data">Application Firmware Version: <span class="text-green">${application_firmware_version}</<span></p>
          <p id="title_data">Application Hardware Version: <span class="text-green">${application_hardware_version}</<span></p>
        </div>

        <div class="row_2 column_3 margin_top_1">
          <p id="title_data">Product Serial Number: <span class="text-green">${product_serial_number}</<span></p>
          <p id="title_data">Product Identifier: <span class="text-green">${product_identifier}</<span></p>
          <p id="title_data">Product Model: <span class="text-green">${product_model}</<span></p>
        </div>

        <div class="row_3 column_3">
          <p id="title_detail">Address: <span class="text-green">${address}</<span></p>
          <p id="title_detail">RSSI: <span class="text-green">${rssi} dBm</<span></p>
          <p id="title_detail">LQI: <span class="text-green">${lqi}</<span></p>
        </div>

        <div class="row_4 column_3">
          <iframe name="votar" style="display:none;"></iframe>
          <div class="div_multiple_button">
            <form id="iact_discovery" action="/WirelessBridge" method="post" target="votar">
                <input id='button_discovery_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="DISCOVERY">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="discovery">
            </form>
          </div>
          <div class="div_multiple_button">
            <form id="iact_discovery" action="/WirelessBridge" method="post" target="votar">
                <input id='button_discovery_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="RESET PARTIAL ENERGY">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="reset_partial_energy">
            </form>
          </div>
        </div>
    </section>
    </div>`;

    const detail_power_tag_3pn = ({
      name, status, id_wireless_bridge, address, type_device,
      manufacturer_name, model_identifier, application_firmware_version, application_hardware_version,
      product_serial_number, product_identifier, product_model,
      rssi, lqi,
      voltage_pha, voltage_phb, voltage_phc, current_pha, current_phb, current_phc,
      active_power_pha, active_power_phb, active_power_phc, total_active_power,
      cumulated_energy, partial_energy, phase_sequence, ac_alarms_mask, alarms_mask
    }) => `
    <div>
    <p id="title_device">Wireless Bridge ${id_wireless_bridge + 1} - ${name}</p>
    <section class="box grid">
        <div class="row_1 column_1">
          <p id="title_data">Voltage Pha: <span class="text-green">${voltage_pha} V</<span></p>
          <p id="title_data">Voltage Phb: <span class="text-green">${voltage_phb} V</<span></p>
          <p id="title_data">Voltage Phc: <span class="text-green">${voltage_phc} V</<span></p>
        </div>

        <div class="row_1 column_2">
          <p id="title_data">Current Pha: <span class="text-green">${current_pha} A</<span></p>
          <p id="title_data">Current Phb: <span class="text-green">${current_phb} A</<span></p>
          <p id="title_data">Current Phc: <span class="text-green">${current_phc} A</<span></p>
        </div>

        <div class="row_1 column_3">
          <p id="title_data">Active Power Pha: <span class="text-green">${active_power_pha} W</<span></p>
          <p id="title_data">Active Power Phb: <span class="text-green">${active_power_phb} W</<span></p>
          <p id="title_data">Active Power Phc: <span class="text-green">${active_power_phc} W</<span></p>
          <p id="title_data">Total Active Power: <span class="text-green">${total_active_power} kW</<span></p>
        </div>

        <div class="row_2 column_1 margin_top_1">
          <p id="title_data">Cumulated Energy: <span class="text-green">${cumulated_energy} kWh</<span></p>
          <p id="title_data">Partial Energy: <span class="text-green">${partial_energy} kWh</<span></p>
          <p id="title_data">Phase Sequence: <span class="text-green">${phase_sequence}</<span></p>
          <p id="title_data">AC Alarms Mask: <span class="text-green">${ac_alarms_mask}</<span></p>
          <p id="title_data">Alarms Mask: <span class="text-green">${alarms_mask}</<span></p>
        </div>

        <div class="row_2 column_2 margin_top_1">
          <p id="title_data">Manufacturer Name: <span class="text-green">${manufacturer_name}</<span></p>
          <p id="title_data">Model Identifier: <span class="text-green">${model_identifier}</<span></p>
          <p id="title_data">Application Firmware Version: <span class="text-green">${application_firmware_version}</<span></p>
          <p id="title_data">Application Hardware Version: <span class="text-green">${application_hardware_version}</<span></p>
        </div>

        <div class="row_2 column_3 margin_top_1">
          <p id="title_data">Product Serial Number: <span class="text-green">${product_serial_number}</<span></p>
          <p id="title_data">Product Identifier: <span class="text-green">${product_identifier}</<span></p>
          <p id="title_data">Product Model: <span class="text-green">${product_model}</<span></p>
        </div>

        <div class="row_3 column_3">
          <p id="title_detail">Address: <span class="text-green">${address}</<span></p>
          <p id="title_detail">RSSI: <span class="text-green">${rssi} dBm</<span></p>
          <p id="title_detail">LQI: <span class="text-green">${lqi}</<span></p>
        </div>

        <div class="row_4 column_3">
          <iframe name="votar" style="display:none;"></iframe>
          <div class="div_multiple_button">
            <form id="iact_discovery" action="/WirelessBridge" method="post" target="votar">
                <input id='button_discovery_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="DISCOVERY">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="discovery">
            </form>
          </div>
          <div class="div_multiple_button">
            <form id="iact_discovery" action="/WirelessBridge" method="post" target="votar">
                <input id='button_discovery_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="RESET PARTIAL ENERGY">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="reset_partial_energy">
            </form>
          </div>
        </div>
    </section>
    </div>`;


    var set_on_changed_action = function() {

      $('#jstree_demo_div').on('ready.jstree', function(e, data) {
          if(data.instance.get_node('WB_1')){
          $('#date').html(data.instance.get_node('WB_1').data.date);
        }else{
          $('#date').html('');
        }
      });


      $('#jstree_demo_div')
        // listen for event
        .on('changed.jstree', function(e, data) {
          var i, j, r = [];
          data_product_iact = [];
          data_product_temp = [];
          data_product_power_tag_1p = [];
          data_product_power_tag_3p = [];
          data_product_power_tag_3pn = [];

          for (i = 0, j = data.selected.length; i < j; i++) {

            if (data.instance.get_node(data.selected[i]).type == 'iact') {
              data_product_iact.push({
                name: data.instance.get_node(data.selected[i]).text,
                type_device: data.instance.get_node(data.selected[i]).type,

                manufacturer_name: data.instance.get_node(data.selected[i]).data.product.manufacturer_name,
                model_identifier: data.instance.get_node(data.selected[i]).data.product.model_identifier,
                application_firmware_version: data.instance.get_node(data.selected[i]).data.product.application_firmware_version,
                application_hardware_version: data.instance.get_node(data.selected[i]).data.product.application_hardware_version,
                product_serial_number: data.instance.get_node(data.selected[i]).data.product.product_serial_number,
                product_identifier: data.instance.get_node(data.selected[i]).data.product.product_identifier,
                product_model: data.instance.get_node(data.selected[i]).data.product.product_model,

                rssi: data.instance.get_node(data.selected[i]).data.product.rssi,
                lqi: data.instance.get_node(data.selected[i]).data.product.lqi,

                status: data.instance.get_node(data.selected[i]).data.status == 'OPEN' ? 'OPEN' : 'CLOSE',

                id_wireless_bridge: data.instance.get_node( data.instance.get_node(data.selected[i]).parents[0]).data.device_number,
                address: data.instance.get_node(data.selected[i]).data.product.address,
              })
            }

            if (data.instance.get_node(data.selected[i]).type == 'co2_multisensor') {
              data_product_temp.push({
                name: data.instance.get_node(data.selected[i]).text,
                type_device: data.instance.get_node(data.selected[i]).type,

                manufacturer_name: data.instance.get_node(data.selected[i]).data.product.manufacturer_name,
                model_identifier: data.instance.get_node(data.selected[i]).data.product.model_identifier,
                application_firmware_version: data.instance.get_node(data.selected[i]).data.product.application_firmware_version,
                application_hardware_version: data.instance.get_node(data.selected[i]).data.product.application_hardware_version,
                product_serial_number: data.instance.get_node(data.selected[i]).data.product.product_serial_number,
                product_identifier: data.instance.get_node(data.selected[i]).data.product.product_identifier,
                product_model: data.instance.get_node(data.selected[i]).data.product.product_model,

                rssi: data.instance.get_node(data.selected[i]).data.product.rssi,
                lqi: data.instance.get_node(data.selected[i]).data.product.lqi,

                co2: data.instance.get_node(data.selected[i]).data.co2,
                temperature: data.instance.get_node(data.selected[i]).data.temperature,
                humidity: data.instance.get_node(data.selected[i]).data.relative_humidity,

                id_wireless_bridge: data.instance.get_node( data.instance.get_node(data.selected[i]).parents[0]).data.device_number,
                address: data.instance.get_node(data.selected[i]).data.product.address,
              })
            }

            if (data.instance.get_node(data.selected[i]).type == 'power_tag_1p') {
              data_product_power_tag_1p.push({
                name: data.instance.get_node(data.selected[i]).text,
                type_device: data.instance.get_node(data.selected[i]).type,

                manufacturer_name: data.instance.get_node(data.selected[i]).data.product.manufacturer_name,
                model_identifier: data.instance.get_node(data.selected[i]).data.product.model_identifier,
                application_firmware_version: data.instance.get_node(data.selected[i]).data.product.application_firmware_version,
                application_hardware_version: data.instance.get_node(data.selected[i]).data.product.application_hardware_version,
                product_serial_number: data.instance.get_node(data.selected[i]).data.product.product_serial_number,
                product_identifier: data.instance.get_node(data.selected[i]).data.product.product_identifier,
                product_model: data.instance.get_node(data.selected[i]).data.product.product_model,

                rssi: data.instance.get_node(data.selected[i]).data.product.rssi,
                lqi: data.instance.get_node(data.selected[i]).data.product.lqi,

                voltage_pha: data.instance.get_node(data.selected[i]).data.electrical_measurement.voltage_pha,
                current_pha: data.instance.get_node(data.selected[i]).data.electrical_measurement.current_pha,

                cumulated_energy: data.instance.get_node(data.selected[i]).data.metering.cumulated_energy,
                partial_energy: data.instance.get_node(data.selected[i]).data.metering.partial_energy,
                phase_sequence: data.instance.get_node(data.selected[i]).data.electrical_measurement.phase_sequence,
                ac_alarms_mask: data.instance.get_node(data.selected[i]).data.electrical_measurement.ac_alarms_mask,
                alarms_mask: data.instance.get_node(data.selected[i]).data.electrical_measurement.alarms_mask,

                id_wireless_bridge: data.instance.get_node( data.instance.get_node(data.selected[i]).parents[0]).data.device_number,
                address: data.instance.get_node(data.selected[i]).data.product.address,
              })
            }

            if (data.instance.get_node(data.selected[i]).type == 'power_tag_3p') {
              data_product_power_tag_3p.push({
                name: data.instance.get_node(data.selected[i]).text,
                type_device: data.instance.get_node(data.selected[i]).type,

                manufacturer_name: data.instance.get_node(data.selected[i]).data.product.manufacturer_name,
                model_identifier: data.instance.get_node(data.selected[i]).data.product.model_identifier,
                application_firmware_version: data.instance.get_node(data.selected[i]).data.product.application_firmware_version,
                application_hardware_version: data.instance.get_node(data.selected[i]).data.product.application_hardware_version,
                product_serial_number: data.instance.get_node(data.selected[i]).data.product.product_serial_number,
                product_identifier: data.instance.get_node(data.selected[i]).data.product.product_identifier,
                product_model: data.instance.get_node(data.selected[i]).data.product.product_model,

                rssi: data.instance.get_node(data.selected[i]).data.product.rssi,
                lqi: data.instance.get_node(data.selected[i]).data.product.lqi,

                voltage_pha_phb: data.instance.get_node(data.selected[i]).data.electrical_measurement.voltage_pha_phb,
                voltage_phb_phc: data.instance.get_node(data.selected[i]).data.electrical_measurement.voltage_phb_phc,
                voltage_phc_pha: data.instance.get_node(data.selected[i]).data.electrical_measurement.voltage_phc_pha,

                current_pha: data.instance.get_node(data.selected[i]).data.electrical_measurement.current_pha,
                current_phb: data.instance.get_node(data.selected[i]).data.electrical_measurement.current_phb,
                current_phc: data.instance.get_node(data.selected[i]).data.electrical_measurement.current_phc,

                active_power_pha: data.instance.get_node(data.selected[i]).data.electrical_measurement.active_power_pha,
                active_power_phb: data.instance.get_node(data.selected[i]).data.electrical_measurement.active_power_phb,
                active_power_phc: data.instance.get_node(data.selected[i]).data.electrical_measurement.active_power_phc,
                total_active_power: data.instance.get_node(data.selected[i]).data.electrical_measurement.total_active_power,

                cumulated_energy: data.instance.get_node(data.selected[i]).data.metering.cumulated_energy,
                partial_energy: data.instance.get_node(data.selected[i]).data.metering.partial_energy,
                phase_sequence: data.instance.get_node(data.selected[i]).data.electrical_measurement.phase_sequence,
                ac_alarms_mask: data.instance.get_node(data.selected[i]).data.electrical_measurement.ac_alarms_mask,
                alarms_mask: data.instance.get_node(data.selected[i]).data.electrical_measurement.alarms_mask,

                id_wireless_bridge: data.instance.get_node( data.instance.get_node(data.selected[i]).parents[0]).data.device_number,
                address: data.instance.get_node(data.selected[i]).data.product.address,
              })
            }

            if (data.instance.get_node(data.selected[i]).type == 'power_tag_3pn') {
              data_product_power_tag_3pn.push({
                name: data.instance.get_node(data.selected[i]).text,
                type_device: data.instance.get_node(data.selected[i]).type,

                manufacturer_name: data.instance.get_node(data.selected[i]).data.product.manufacturer_name,
                model_identifier: data.instance.get_node(data.selected[i]).data.product.model_identifier,
                application_firmware_version: data.instance.get_node(data.selected[i]).data.product.application_firmware_version,
                application_hardware_version: data.instance.get_node(data.selected[i]).data.product.application_hardware_version,
                product_serial_number: data.instance.get_node(data.selected[i]).data.product.product_serial_number,
                product_identifier: data.instance.get_node(data.selected[i]).data.product.product_identifier,
                product_model: data.instance.get_node(data.selected[i]).data.product.product_model,

                rssi: data.instance.get_node(data.selected[i]).data.product.rssi,
                lqi: data.instance.get_node(data.selected[i]).data.product.lqi,

                voltage_pha: data.instance.get_node(data.selected[i]).data.electrical_measurement.voltage_pha,
                voltage_phb: data.instance.get_node(data.selected[i]).data.electrical_measurement.voltage_phb,
                voltage_phc: data.instance.get_node(data.selected[i]).data.electrical_measurement.voltage_phc,

                current_pha: data.instance.get_node(data.selected[i]).data.electrical_measurement.current_pha,
                current_phb: data.instance.get_node(data.selected[i]).data.electrical_measurement.current_phb,
                current_phc: data.instance.get_node(data.selected[i]).data.electrical_measurement.current_phc,

                active_power_pha: data.instance.get_node(data.selected[i]).data.electrical_measurement.active_power_pha,
                active_power_phb: data.instance.get_node(data.selected[i]).data.electrical_measurement.active_power_phb,
                active_power_phc: data.instance.get_node(data.selected[i]).data.electrical_measurement.active_power_phc,
                total_active_power: data.instance.get_node(data.selected[i]).data.electrical_measurement.total_active_power,

                cumulated_energy: data.instance.get_node(data.selected[i]).data.metering.cumulated_energy,
                partial_energy: data.instance.get_node(data.selected[i]).data.metering.partial_energy,
                phase_sequence: data.instance.get_node(data.selected[i]).data.electrical_measurement.phase_sequence,
                ac_alarms_mask: data.instance.get_node(data.selected[i]).data.electrical_measurement.ac_alarms_mask,
                alarms_mask: data.instance.get_node(data.selected[i]).data.electrical_measurement.alarms_mask,

                id_wireless_bridge: data.instance.get_node( data.instance.get_node(data.selected[i]).parents[0]).data.device_number,
                address: data.instance.get_node(data.selected[i]).data.product.address,
              })
            }
          }

          $('#detail-product').html(data_product_iact.map(detail_iact).join(''));
          $('#detail-product').append(data_product_temp.map(detail_temp).join(''));
          $('#detail-product').append(data_product_power_tag_1p.map(detail_power_tag_1p).join(''));
          $('#detail-product').append(data_product_power_tag_3p.map(detail_power_tag_3p).join(''));
          $('#detail-product').append(data_product_power_tag_3pn.map(detail_power_tag_3pn).join(''));
        })

    }

    var load_tree = function() {
        var $treeview = $("#jstree_demo_div");
        $.getJSON('./topology.json?nocache=' + (new Date()).getTime(), function (data) {
        $treeview.jstree({
                  "state": {
                    "key": "123456"
                  },
                  "checkbox": {
                    "keep_selected_style": true
                  },
                  "state": {
                    "key": "zigbeeView"
                  },
                  "types": {
                    "default": {
                      "icon": "./images/logo_wb.png"
                    },
                    "iact": {
                      "icon": "./images/logo_iact.png"
                    },
                    "co2_multisensor": {
                      "icon": "./images/logo_temp.png"
                    },
                    "power_tag_1p": {
                      "icon": "./images/logo_power_tag.png"
                    },
                    "power_tag_3p": {
                      "icon": "./images/logo_power_tag.png"
                    },
                    "power_tag_3pn": {
                      "icon": "./images/logo_power_tag.png"
                    }

                  },
                  "plugins": ["checkbox", "sort", "state", "types"],
                  "core": {
                    "themes": {
                      "name": "default",
                      "dots": true,
                      "icons": true
                    },
                    "data": data
                  }
                });
                localStorage.setItem("topology-data",JSON.stringify(data));
        });
    };

    var reload_tree = function($id) {
      $.getJSON('./topology.json?nocache=' + (new Date()).getTime(), function (data) {
        if(localStorage.getItem("topology-data") != JSON.stringify(data)){
          localStorage.setItem("topology-data",JSON.stringify(data));
          var jsTree = $('#jstree_demo_div').jstree(true);
          jsTree.save_state();
          $json = jsTree.get_json();
          $("#jstree_demo_div").jstree("destroy");
          load_tree();
          set_on_changed_action();
        }
      });
    };

    setInterval(function() {
      reload_tree();
    }, 5000);

    $(function() {
      load_tree();
      set_on_changed_action();
    });
