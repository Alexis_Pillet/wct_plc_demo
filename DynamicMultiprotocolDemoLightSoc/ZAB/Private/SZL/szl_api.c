/*******************************************************************************
  Filename    : szl_api.c
  $Date       : 2013-03-21                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : This is the API functions for the SZL


 * ZAB MODIFICATION HISTORY:
 *   Rev           Date     Author  Change Description
 * 002.001.003  16-Jul-15   MvdB   ARTF131073: Differentiate by direction when registering Cluster Command Handlers
 * 002.001.004  21-Jul-15   MvdB   ARTF134686: Update plugin allocation method to be generic and protect against multiple initialisation
 *                                             Add Szl_InternalIsLibInitialized() and use before allowing registration.
 *                                             Protect against SZL_Initialize() with null pointer
 * 002.001.006  27-Jul-15   MvdB   Fix TID returned by SZL_NwkLeaveReq()
 *                                 ARTF138318: Add SZL_AttributesDeregister() and SZL_ClusterCmdDeregister()
 * 002.002.010  09-Oct-15   MvdB   ARTF150980: Support Service ID in memory allocation functions for WTB
 * 002.002.021  21-Apr-16   MvdB   ARTF167808: Improve Attribute Read/Write/Configure Responses to always list all attributes requested
 *
 * ---SILABS DEV---
 * 000.000.007  06-Jul-16   MvdB   Upgrade Szl_BuildClustersListFromDataPoints() and SZL_ClusterCmdRegister() to also look at registered commands
 *
 * 002.002.031  09-Jan-17   MvdB   ARTF152098: SZL: Improve update of simple descriptors when attributes/commands are registered/deregistered
 * 002.002.032  09-Jan-17   MvdB   ARTF172065: Rationalise duplicated private functions into public SZLEXT_ZigBeeDataTypeIsAnalog()
 * 002.002.034  10-Jan-17   MvdB   ARTF171185: Correct printing of IEE in SZL_NwkLeaveReq()
 * 002.002.035  11-Jan-17   MvdB   ARTF170823: Make Read/Write Reporting configuration data (SZL_AttributeReportData_t) consistent with Read/Write data (SZL_AttributeData_t)
 * 002.002.054  20-Jul-17   SMon   ARTF214292 Manage timeout for default response
 *                                 ARTF214298 Manage write secured key attribut
*******************************************************************************/
/*   _________      .__                  .__    .___
    /   _____/ ____ |  |__   ____   ____ |__| __| _/___________
    \_____  \_/ ___\|  |  \ /    \_/ __ \|  |/ __ |/ __ \_  __ \   ______
    /        \  \___|   Y  \   |  \  ___/|  / /_/ \  ___/|  | \/  /_____/
   /_______  /\___  >___|  /___|  /\___  >__\____ |\___  >__|
           \/     \/     \/     \/     \/        \/    \/
   __________.__      __________
   \____    /|__| ____\______   \ ____   ____
     /     / |  |/ ___\|    |  _// __ \_/ __ \    ______
    /     /_ |  / /_/  >    |   \  ___/\  ___/   /_____/
   /_______ \|__\___  /|______  /\___  >\___  >
           \/  /_____/        \/     \/     \/
   .____    ._____.
   |    |   |__\_ |______________ _______ ___.__.
   |    |   |  || __ \_  __ \__  \\_  __ <   |  |   ______
   |    |___|  || \_\ \  | \// __ \|  | \/\___  |  /_____/
   |_______ \__||___  /__|  (____  /__|   / ____|
           \/       \/           \/       \/
      _____ __________.___
     /  _  \\______   \   |
    /  /_\  \|     ___/   |
   /    |    \    |   |   |
   \____|__  /____|   |___|
           \/

     _S_           _Z_           _L_
    (o o)         (o o)         (o o)
ooO--(_)--Ooo-ooO--(_)--Ooo-ooO--(_)--Ooo-

Schneider - ZigBee - Library. (SZL)
Copyright (c) 2013 - Schneider-Electric R&D
(textArt from http://patorjk.com )
*/
#include "zabCorePrivate.h"
#include "data_converter.h"
#include "endianness.h"
#include "sort.h"
#include "szl.h"
#include "szl_gp.h"
#include "szl_timer.h"
#include "szl_report.h"
#include "szl_zab.h"
#include "zcl_def.h"
#include "zcl_manager_zab.h"
#include "szl_api.h"
/*
.  ________          _____.__       .__  __  .__                               .
.  \______ \   _____/ ____\__| ____ |__|/  |_|__| ____   ____   ______         .
.   |    |  \_/ __ \   __\|  |/    \|  \   __\  |/  _ \ /    \ /  ___/         .
.   |    `   \  ___/|  |  |  |   |  \  ||  | |  (  <_> )   |  \\___ \          .
.  /_______  /\___  >__|  |__|___|  /__||__| |__|\____/|___|  /____            .
.          \/     \/              \/                        \/     \/          .
*/



/*
.  ___________                   __  .__                                       .
.  \_   _____/_ __  ____   _____/  |_|__| ____   ____   ______                 .
.   |    __)|  |  \/    \_/ ___\   __\  |/  _ \ /    \ /  ___/                 .
.   |     \ |  |  /   |  \  \___|  | |  (  <_> )   |  \\___ \                  .
.   \___  / |____/|___|  /\___  >__| |__|\____/|___|  /____  >                 .
.       \/             \/     \/                    \/     \/                  .
*/







/**
 * Initialize
 *
 * This is called at startup to initialize the library
 * The app will have to fill all the mandatory fields,
 * and provide the optional fields that is of interest to the app.
 */
SZL_RESULT_t SZL_Initialize(zabService* Service, SZL_Initialize_t *InitPtr)
{
  szl_uint8 index;

  // Validate inputs
  if (!InitPtr)
    {
      return SZL_RESULT_DATA_MISSING;
    }

  Szl_InternalInitialize(Service);
  SzlPT_Initialize(Service);
  Szl_TimersInitialize(Service);
  CbHandler_Initialize(Service);
  Szl_ReportInitialize(Service);

  // check for proper version of API
  if (InitPtr->Version != SZLAPIVersion) // maybe only compare major version number ?
    {
      // Warn during development...
      printError(Service, "SZL: ERROR: Initialise version does not match\n");
      return SZL_RESULT_API_INVALID_VERSION;
    }

  if ( (InitPtr->NumberOfEndpoints > SZL_CFG_ENDPOINT_CNT) ||
       (InitPtr->NumberOfEndpoints == 0) )
    {
      printError(Service, "SZL: ERROR: NumberOfEndpoints invalid %d\n", InitPtr->NumberOfEndpoints);
      return SZL_RESULT_INVALID_DATA;
    }

  /* Validate endpoint are in legal range */
  for (index = 0; index < InitPtr->NumberOfEndpoints; index++)
    {
      if ( (InitPtr->Endpoints[index] == 0) ||
           (InitPtr->Endpoints[index] >=0xF0) )
        {
          printError(Service, "SZL: ERROR: Invalid endpoint 0x%02X\n", InitPtr->Endpoints[index]);
          return SZL_RESULT_INVALID_DATA;
        }
    }

  /* Take a copy of the init data */
  szl_memcpy(&(ZAB_SRV(Service)->szlService.initData), InitPtr, sizeof(SZL_Initialize_t));

  Szl_InternalInitialized(Service);

  /* Now register the endpoint(s) with the network processor.
   * Do it immediately here (rather than using work flag) to ensure endpoints are registered quickly and following commands will not fail */
  ZAB_SRV(Service)->szlService.EndpointUpdateRequired = szl_false;
  return SzlZab_EndpointRegister(Service);
}

/**
 * Destroy
 *
 * This is called to destroy an instance of SZL.
 * The Application MUST have destroyed each plugin first!
 */
SZL_RESULT_t SZL_Destroy(zabService* Service)
{
  szl_uint8 idx;

  /* Ensure we do not bother to update endpoints */
  LIB_DATA(Service).EndpointUpdateRequired = szl_false;

  // Confirm all plugins are destroyed before proceeding, otherwise their allocated data will be lost!
  for (idx = 0; idx < SZL_CFG_MAX_PLUGINS; idx++)
    {
      if (LIB_DATA(Service).Plugins[idx].pluginId != SZL_PLUGIN_ID_NONE)
        {
          return SZL_RESULT_OPERATION_NOT_POSSIBLE;
/*
          // DANGER - If we just free here we will lose any additional data the plugin has malloced.
          // The plugin MUST be unregistered
          szl_mem_free(LIB_DATA(Service).Plugins[idx].pluginData);
          LIB_DATA(Service).Plugins[idx].pluginData = NULL;
          LIB_DATA(Service).Plugins[idx].pluginId = SZL_PLUGIN_ID_NONE;
*/
        }
    }

  Szl_InternalInitialize(Service);
  SzlPT_Initialize(Service);
  Szl_TimersInitialize(Service);
  CbHandler_Initialize(Service);
  Szl_ReportInitialize(Service);

  return SZL_RESULT_SUCCESS;
}



/**
 * Attribute Discover Request
 *
 * Discover attributes for a specific cluster on a remote device
 */
SZL_RESULT_t SZL_AttributeDiscoverReq(zabService* Service, SZL_CB_AttributeDiscoverResp_t Callback, SZL_AttributeDiscoverReqParams_t* Params, szl_uint8* TransactionId)
{
  unsigned8 zcl_buffer[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
  syncCallback syncCB;
  unsigned8 tid;
  SZL_AttributeDiscoverRespParams_t* errorRspParams;
  SZL_RESULT_t result = Szl_IsRequestValid(Service,  SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED );

    if (SZL_RESULT_SUCCESS == result)
    {
        ZCL_PAYLOAD_t* zcl;
        ZCL_COMMON_PART_t* common = NULL;
        ZCL_DISCOVER_ATTRIBUTES_t* zda = NULL;
        szl_uint16 zcl_payload_size = ZCL_PAYLOAD_SIZE(Params->ManufacturerSpecific, ZCL_DISCOVER_ATTRIBUTES_SIZE );

        if ( zcl_payload_size >= SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH)
        {
            return SZL_RESULT_DATA_TOO_BIG;
        }

        tid = szl_zab_GetTransactionId(Service);
        if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
          {
            return SZL_RESULT_NO_FREE_TID;
          }
        if (TransactionId != NULL)
          {
            *TransactionId = tid;
          }

        /* Create the response that will be used in case of no response from the destination */
        errorRspParams = (SZL_AttributeDiscoverRespParams_t*)szl_mem_alloc(SZL_AttributeDiscoverRespParams_t_SIZE(0),
                                                                           MALLOC_ID_SZL_DISCOVER_ATTR_FAKE_RSP);
        if (errorRspParams == NULL)
          {
            return SZL_RESULT_INTERNAL_LIB_ERROR;
          }
        szl_zab_GetResponseAddrFromRequestDestination(&Params->DestAddrMode, &errorRspParams->SourceAddress);
        errorRspParams->DestinationEndpoint = Params->SourceEndpoint;
        errorRspParams->ManufacturerSpecific = Params->ManufacturerSpecific;
        errorRspParams->ClusterID = Params->ClusterID;
        errorRspParams->ListComplete = szl_true;
        errorRspParams->NumberOfAttributes = 0;

        // fill the ZCL header
        zcl = (ZCL_PAYLOAD_t*)zcl_buffer;
        zcl->frame_type.frame_control.frame_type = ZCL_FRAME_TYPE_PROFILE_COMMAND;
        zcl->frame_type.frame_control.manufacture_specific = Params->ManufacturerSpecific;
        zcl->frame_type.frame_control.direction = ZCL_FRAME_DIR_CLIENT_SERVER;
        zcl->frame_type.frame_control.disable_default_rsp = ZCL_FRAME_DEFAULT_RESPONSE_ALWAYS; // we always need a reply to get the CB called.
        zcl->frame_type.frame_control.reserved = 0;

        // fill manufacture specific data
        if (zcl->frame_type.frame_control.manufacture_specific)
            zcl->frame_type.manufacture_frame.manufacture_code = UINT16_TO_LE(ZB_SCHNEIDER_MANUFACTURE_ID);

        // fill common header part
        common = ZCL_GET_COMMON_PART_PTR(zcl);
        common->tr_id = tid;
        common->cmd = ZCL_CMD_ID_DISCOVER_ATTRIBUTES;

        // fill in the attributes
        zda = (ZCL_DISCOVER_ATTRIBUTES_t*)common->payload;
        zda->start_attribute_id = UINT16_TO_LE(Params->StartAttributeID);
        zda->maximum_attribute_ids = Params->MaxAttributes;


        /* If it sent ok, then add the transaction. If both ok, return success */
        result = ZclM_SendCommand(Service,
                                  Params->SourceEndpoint,
                                  &Params->DestAddrMode,
                                  Params->ClusterID,
                                  tid,
                                  zcl_payload_size,
                                  zcl_buffer);
        if (result == SZL_RESULT_SUCCESS)
          {
            syncCB.SZL_CB_AttributeDiscoverResp = Callback;
            result = SzlZab_AddSynchronousTransaction(Service,
                                          SZL_ZAB_SYNC_TRANS_TYPE_ZCL_DISCOVER_ATTRIBUTE_REQ,
                                          tid,
                                          &syncCB,
                                          errorRspParams);
          }
        if (result != SZL_RESULT_SUCCESS)
          {
            szl_mem_free(errorRspParams);
          }
    }

    return result;
}


/**
 * Attribute Read Request
 *
 * This function is used to read one or more attributes from a specific cluster
 */
SZL_RESULT_t SZL_AttributeReadReq(zabService* Service,
                                  SZL_CB_AttributeReadResp_t Callback,
                                  SZL_AttributeReadReqParams_t* Params,
                                  szl_uint8* TransactionId)
{
    int i;
    szl_uint8 tid;
    SZL_AttributeReadRespParams_t* errorRspParams;
    szl_uint8 zclBuffer[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
    SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

    if (SZL_RESULT_SUCCESS == result)
    {
        szl_uint16 totalZclLength;
        ZCL_PAYLOAD_t* zcl;
        ZCL_COMMON_PART_t* common;
        ZCL_READ_ATTRIBUTES_t* attr;
        szl_bool greenPowerCmd = szl_false;

        if (Params->NumberOfAttributes == 0)
        {
            return SZL_RESULT_INVALID_DATA;
        }

        tid = szl_zab_GetTransactionId(Service);
        if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
          {
            return SZL_RESULT_NO_FREE_TID;
          }
        if (TransactionId != NULL)
          {
            *TransactionId = tid;
          }

        if (Params->DestAddrMode.AddressMode == SZL_ADDRESS_MODE_GP_SOURCE_ID)
          {
            greenPowerCmd = szl_true;

            /* Length = GpCmdId(1) + Options(1) + ManId(0/2) + Cluster (2) + NumBytes(1) + Attributes(2n) */
            if (Params->ManufacturerSpecific == szl_true)
              {
                totalZclLength = 7 + (sizeof(ZCL_READ_ATTRIBUTE_ID_t) * Params->NumberOfAttributes);
              }
            else
              {
                totalZclLength = 5 + (sizeof(ZCL_READ_ATTRIBUTE_ID_t) * Params->NumberOfAttributes);
              }
          }
        else
          {
            totalZclLength = ZCL_PAYLOAD_SIZE(Params->ManufacturerSpecific, (sizeof(ZCL_READ_ATTRIBUTE_ID_t) * Params->NumberOfAttributes) );
          }
        if ( totalZclLength >= SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH)
          {
            return SZL_RESULT_DATA_TOO_BIG;
          }


        /* Create the response that will be used in case of no response from the destination */
        errorRspParams = (SZL_AttributeReadRespParams_t*)szl_mem_alloc(SZL_AttributeReadRespParams_t_SIZE(Params->NumberOfAttributes),
                                                                       MALLOC_ID_SZL_READ_ATTR_FAKE_RSP);
        if (errorRspParams == NULL)
          {
            return SZL_RESULT_INTERNAL_LIB_ERROR;
          }

        szl_zab_GetResponseAddrFromRequestDestination(&Params->DestAddrMode, &errorRspParams->SourceAddress);
        errorRspParams->DestinationEndpoint = Params->SourceEndpoint;
        errorRspParams->ManufacturerSpecific = Params->ManufacturerSpecific;
        errorRspParams->ClusterID = Params->ClusterID;
        errorRspParams->NumberOfAttributes = Params->NumberOfAttributes;
        for(i=0; i < Params->NumberOfAttributes; i++)
          {
            errorRspParams->Attributes[i].AttributeID = Params->AttributeIDs[i];
            errorRspParams->Attributes[i].Status = SZL_STATUS_NO_RESPONSE;
            errorRspParams->Attributes[i].DataType = SZL_ZB_DATATYPE_NO_DATA;
            errorRspParams->Attributes[i].DataLength = 0;
            errorRspParams->Attributes[i].Data = NULL;
          }


        // fill the ZCL header
        zcl = (ZCL_PAYLOAD_t*)zclBuffer;
        if (greenPowerCmd == szl_true)
          {
            unsigned8 index;

            zcl->frame_type.zgp_frame.cmd = 0xF2;
            if (Params->ManufacturerSpecific)
              {
                zcl->frame_type.zgp_frame.payload[0] = 0x02;
                COPY_OUT_16_BITS(&zcl->frame_type.zgp_frame.payload[1], ZB_SCHNEIDER_MANUFACTURE_ID);
                index = 3;
              }
            else
              {
                zcl->frame_type.zgp_frame.payload[0] = 0x00;
                index = 1;
              }
            COPY_OUT_16_BITS(&zcl->frame_type.zgp_frame.payload[index], Params->ClusterID); index += 2;
            zcl->frame_type.zgp_frame.payload[index++] = sizeof(ZCL_READ_ATTRIBUTE_ID_t) * Params->NumberOfAttributes;


            attr = (ZCL_READ_ATTRIBUTES_t*)&zcl->frame_type.zgp_frame.payload[index];

          }
        else
          {
            zcl->frame_type.frame_control.frame_type = ZCL_FRAME_TYPE_PROFILE_COMMAND;
            zcl->frame_type.frame_control.manufacture_specific = Params->ManufacturerSpecific;
            zcl->frame_type.frame_control.direction = ZCL_FRAME_DIR_CLIENT_SERVER;
            zcl->frame_type.frame_control.disable_default_rsp = ZCL_FRAME_DEFAULT_RESPONSE_ALWAYS; // we always need a reply to get the CB called.
            zcl->frame_type.frame_control.reserved = 0;

            // fill manufacture specific data
            if (zcl->frame_type.frame_control.manufacture_specific)
                zcl->frame_type.manufacture_frame.manufacture_code = UINT16_TO_LE(ZB_SCHNEIDER_MANUFACTURE_ID);

            // fill common header part
            common = ZCL_GET_COMMON_PART_PTR(zcl);
            common->tr_id = tid;
            common->cmd = ZCL_CMD_ID_READ_ATTRIBUTES;

            // fill in the attributes
            attr = (ZCL_READ_ATTRIBUTES_t*)common->payload;

          }

        for(i=0; i < Params->NumberOfAttributes; i++)
            attr->attributes[i].attribute_id = UINT16_TO_LE(Params->AttributeIDs[i]);

        /* Send command. If it sent ok, then add the transaction. If both ok, return success */
        result = ZclM_SendCommand(Service,
                                  Params->SourceEndpoint,
                                  &Params->DestAddrMode,
                                  Params->ClusterID,
                                  tid,
                                  totalZclLength,
                                  zclBuffer);
        if (result == SZL_RESULT_SUCCESS)
          {
            syncCallback syncCB;
            syncCB.SZL_CB_AttributeReadResp = Callback;

            if (greenPowerCmd == szl_true)
              {
                result = SzlZab_AddGreenPowerSynchronousTransaction(Service,
                                                                    SZL_ZAB_SYNC_TRANS_TYPE_ZCL_READ_ATTRIBUTE_REQ,
                                                                    tid,
                                                                    Params->DestAddrMode.Addresses.GpSourceId.SourceId,
                                                                    &syncCB,
                                                                    errorRspParams);
              }
            else
              {
                result = SzlZab_AddSynchronousTransaction(Service,
                                              SZL_ZAB_SYNC_TRANS_TYPE_ZCL_READ_ATTRIBUTE_REQ,
                                              tid,
                                              &syncCB,
                                              errorRspParams);
              }

          }

        if (result != SZL_RESULT_SUCCESS)
          {
            szl_mem_free(errorRspParams);
          }
    }

    return result;
}

/**
 * Attribute Write Request
 *
 * This function is used to read one or more attributes from a specific cluster
 */
SZL_RESULT_t SZL_AttributeWriteReq(zabService* Service,
                                   SZL_CB_AttributeWriteResp_t Callback,
                                   SZL_AttributeWriteReqParams_t* Params,
                                   szl_uint8* TransactionId)
{
    szl_uint8 tid;
    int i;
    SZL_AttributeWriteRespParams_t* errorRspParams;
    szl_uint8 zclBuffer[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
    SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED );

    if (SZL_RESULT_SUCCESS == result)
    {
        szl_uint16 data_size = 0;
        szl_uint16 totalZclLength;
        ZCL_PAYLOAD_t* zcl;
        ZCL_COMMON_PART_t* common;
        ZCL_WRITE_ATTRIBUTE_t* attr;
        szl_bool greenPowerCmd = szl_false;

        if (Params->NumberOfAttributes == 0)
        {
            return SZL_RESULT_INVALID_DATA;
        }

        tid = szl_zab_GetTransactionId(Service);
        if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
          {
            return SZL_RESULT_NO_FREE_TID;
          }
        if (TransactionId != NULL)
          {
            *TransactionId = tid;
          }

        /* Calculate size of Write Attribute items */
        for(i=0; i < Params->NumberOfAttributes; i++)
        {
            szl_uint8 converted_data_size = ZbDataSize(Params->Attributes[i].DataType, &Params->Attributes[i].DataLength);

            if (converted_data_size == 0)
            {
                return SZL_RESULT_INVALID_DATA;
            }

            // get the size for the converted native to zigbee type
            data_size += ZCL_WRITE_ATTRIBUTE_SIZE(converted_data_size);
        }

        /* Add GP or ZCL header */
        if (Params->DestAddrMode.AddressMode == SZL_ADDRESS_MODE_GP_SOURCE_ID)
          {
            greenPowerCmd = szl_true;

            /* Length = GpCmdId(1) + Options(1) + ManId(0/2) + Cluster (2) + NumBytes(1) + Attributes(data_size) */
            if (Params->ManufacturerSpecific == szl_true)
              {
                totalZclLength = 7 + data_size;
              }
            else
              {
                totalZclLength = 5 + data_size;
              }
          }
        else
          {
            totalZclLength = ZCL_PAYLOAD_SIZE(Params->ManufacturerSpecific, data_size);
          }
        if ( totalZclLength >= SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH)
          {
            return SZL_RESULT_DATA_TOO_BIG;
          }

        /* Create the response that will be used in case of no response from the destination */
        errorRspParams = (SZL_AttributeWriteRespParams_t*)szl_mem_alloc(SZL_AttributeWriteRespParams_t_SIZE(Params->NumberOfAttributes),
                                                                        MALLOC_ID_SZL_WRITE_ATTR_FAKE_RSP);
        if (errorRspParams == NULL)
          {
            return SZL_RESULT_INTERNAL_LIB_ERROR;
          }
        szl_zab_GetResponseAddrFromRequestDestination(&Params->DestAddrMode, &errorRspParams->SourceAddress);
        errorRspParams->DestinationEndpoint = Params->SourceEndpoint;
        errorRspParams->ManufacturerSpecific = Params->ManufacturerSpecific;
        errorRspParams->ClusterID = Params->ClusterID;
        errorRspParams->NumberOfAttributes = Params->NumberOfAttributes;
        for(i=0; i < Params->NumberOfAttributes; i++)
          {
            errorRspParams->Attributes[i].AttributeID = Params->Attributes[i].AttributeID;
            errorRspParams->Attributes[i].Status = SZL_STATUS_NO_RESPONSE;
          }

        // fill the ZCL header
        zcl = (ZCL_PAYLOAD_t*)zclBuffer;

        if (greenPowerCmd == szl_true)
          {
            unsigned8 index;

            zcl->frame_type.zgp_frame.cmd = 0xF1;
            if (Params->ManufacturerSpecific)
              {
                zcl->frame_type.zgp_frame.payload[0] = 0x02;
                COPY_OUT_16_BITS(&zcl->frame_type.zgp_frame.payload[1], ZB_SCHNEIDER_MANUFACTURE_ID);
                index = 3;
              }
            else
              {
                zcl->frame_type.zgp_frame.payload[0] = 0x00;
                index = 1;
              }
            COPY_OUT_16_BITS(&zcl->frame_type.zgp_frame.payload[index], Params->ClusterID); index += 2;
            zcl->frame_type.zgp_frame.payload[index++] = data_size;

            attr = (ZCL_WRITE_ATTRIBUTE_t*)&zcl->frame_type.zgp_frame.payload[index];
          }
        else
          {
            zcl->frame_type.frame_control.frame_type = ZCL_FRAME_TYPE_PROFILE_COMMAND;
            zcl->frame_type.frame_control.manufacture_specific = Params->ManufacturerSpecific;
            zcl->frame_type.frame_control.direction = ZCL_FRAME_DIR_CLIENT_SERVER;
            zcl->frame_type.frame_control.disable_default_rsp = ZCL_FRAME_DEFAULT_RESPONSE_ALWAYS; // we always need a reply to get the CB called.
            zcl->frame_type.frame_control.reserved = 0;

            // fill manufacture specific data
            if (zcl->frame_type.frame_control.manufacture_specific)
              {
                zcl->frame_type.manufacture_frame.manufacture_code = UINT16_TO_LE(ZB_SCHNEIDER_MANUFACTURE_ID);
              }

            // fill common header part
            common = ZCL_GET_COMMON_PART_PTR(zcl);
            common->tr_id = tid;
            common->cmd = ZCL_CMD_ID_WRITE_ATTRIBUTES;

            attr = (ZCL_WRITE_ATTRIBUTE_t*)common->payload;
          }

        // fill in the attributes
        for(i=0; i < Params->NumberOfAttributes; i++)
        {
            szl_uint8 zb_data_len = 0;
            attr->attribute_id = UINT16_TO_LE(Params->Attributes[i].AttributeID);
            attr->data_type = Params->Attributes[i].DataType;

            switch (attr->data_type)
            {
                case SZL_ZB_DATATYPE_CHAR_STR:
                case SZL_ZB_DATATYPE_OCTET_STR:
                    zb_data_len = Params->Attributes[i].DataLength + 1;
                    attr->data[0] = Params->Attributes[i].DataLength; // the length field is in index[0]
                    szl_memcpy((szl_uint8*)attr->data + 1,Params->Attributes[i].Data, Params->Attributes[i].DataLength);// the real data is in index[1]
                    break;
                case SZL_ZB_DATATYPE_128_BIT_SEC_KEY:
                    zb_data_len = Params->Attributes[i].DataLength ;
                    szl_memcpy((szl_uint8*)attr->data ,Params->Attributes[i].Data, Params->Attributes[i].DataLength);// the real data is in index[1]
                    break;
                default:
                    if (!DataTypeIsPrimitive( attr->data_type ) )
                    {
                        //SNP_DATA_BUF_FREE(payload);
                        return SZL_RESULT_INVALID_DATA;
                    }
                    // we converts the native stream into LE type
                    zb_data_len = NativeToZb(Params->Attributes[i].Data, Params->Attributes[i].DataLength, Params->Attributes[i].DataType, attr->data);
                    break;
            }

            attr = (ZCL_WRITE_ATTRIBUTE_t*)((szl_uint8*)attr + ZCL_WRITE_ATTRIBUTE_SIZE(zb_data_len) );
        }

        /* Send command. If it sent ok, then add the transaction. If both ok, return success */
        result = ZclM_SendCommand(Service,
                                  Params->SourceEndpoint,
                                  &Params->DestAddrMode,
                                  Params->ClusterID,
                                  tid,
                                  totalZclLength,
                                  zclBuffer);
        if (result == SZL_RESULT_SUCCESS)
          {
            syncCallback syncCB;
            syncCB.SZL_CB_AttributeWriteResp = Callback;

            if (greenPowerCmd == szl_true)
              {
                result = SzlZab_AddGreenPowerSynchronousTransaction(Service,
                                                                    SZL_ZAB_SYNC_TRANS_TYPE_ZCL_WRITE_ATTRIBUTE_REQ,
                                                                    tid,
                                                                    Params->DestAddrMode.Addresses.GpSourceId.SourceId,
                                                                    &syncCB,
                                                                    errorRspParams);

              }
            else
              {
                result = SzlZab_AddSynchronousTransaction(Service,
                                              SZL_ZAB_SYNC_TRANS_TYPE_ZCL_WRITE_ATTRIBUTE_REQ,
                                              tid,
                                              &syncCB,
                                              errorRspParams);
              }
          }

        if (result != SZL_RESULT_SUCCESS)
          {
            szl_mem_free(errorRspParams);
          }
    }

    return result;
}


/*
 * Attribute Read Report Configuration request
 *
 * This is the function used by the application to read the report configure from a device.
 */
SZL_RESULT_t SZL_AttributeReadReportCfgReq(zabService* Service,
                                           SZL_CB_AttributeReadReportCfgResp_t Callback,
                                           SZL_AttributeReadReportCfgReqParams_t* Params,
                                           szl_uint8* TransactionId)
{
  int i;
  szl_uint8 tid;
  SZL_AttributeReadReportCfgRespParams_t* errorRspParams;
  szl_uint8 zclBuffer[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
  {
      szl_uint16 totalZclLength;
      ZCL_PAYLOAD_t* zcl ;
      ZCL_COMMON_PART_t* common;
      ZCL_READ_REPORTING_CFG_t* cfg;

      if (Params->NumberOfAttributes == 0)
        {
          return SZL_RESULT_INVALID_DATA;
        }

      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      totalZclLength = ZCL_PAYLOAD_SIZE(Params->ManufacturerSpecific, (sizeof(ZCL_READ_REPORTING_CFG_t) * Params->NumberOfAttributes) );
      if ( totalZclLength >= SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH)
        {
          return SZL_RESULT_DATA_TOO_BIG;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_AttributeReadReportCfgRespParams_t*)szl_mem_alloc(SZL_AttributeReadReportCfgRespParams_t_SIZE(Params->NumberOfAttributes),
                                                                              MALLOC_ID_SZL_READ_REP_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }

      szl_zab_GetResponseAddrFromRequestDestination(&Params->DestAddrMode, &errorRspParams->SourceAddress);
      errorRspParams->DestinationEndpoint = Params->SourceEndpoint;
      errorRspParams->ManufacturerSpecific = Params->ManufacturerSpecific;
      errorRspParams->ClusterID = Params->ClusterID;
      errorRspParams->NumberOfAttributes = Params->NumberOfAttributes;
      for(i=0; i < Params->NumberOfAttributes; i++)
        {
          errorRspParams->Attributes[i].AttributeID = Params->Attributes[i];
          errorRspParams->Attributes[i].Status = SZL_STATUS_NO_RESPONSE;
          errorRspParams->Attributes[i].DataType = SZL_ZB_DATATYPE_NO_DATA;
          errorRspParams->Attributes[i].LenRC = 0;
          errorRspParams->Attributes[i].ReportableChangeData = NULL;
        }

      // fill the ZCL header
      zcl = (ZCL_PAYLOAD_t*)zclBuffer;
      zcl->frame_type.frame_control.frame_type = ZCL_FRAME_TYPE_PROFILE_COMMAND;
      zcl->frame_type.frame_control.manufacture_specific = Params->ManufacturerSpecific;
      zcl->frame_type.frame_control.direction = ZCL_FRAME_DIR_CLIENT_SERVER;
      zcl->frame_type.frame_control.disable_default_rsp = ZCL_FRAME_DEFAULT_RESPONSE_ALWAYS; // we always need a reply to get the CB called.
      zcl->frame_type.frame_control.reserved = 0;

      // fill manufacture specific data
      if (zcl->frame_type.frame_control.manufacture_specific)
          zcl->frame_type.manufacture_frame.manufacture_code = UINT16_TO_LE(ZB_SCHNEIDER_MANUFACTURE_ID);

      // fill common header part
      common = ZCL_GET_COMMON_PART_PTR(zcl);
      common->tr_id = tid;
      common->cmd = ZCL_CMD_ID_READ_REPORTING_CONFIGURATION;

      // fill in the attributes
      cfg = (ZCL_READ_REPORTING_CFG_t*)common->payload;
      for(i=0; i < Params->NumberOfAttributes; i++)
        {
          /* We only support reading of sent reports, not expected */
          cfg[i].direction = ZCL_CONFIGURE_REPORTING_SENDING;
          cfg[i].attribute_id = UINT16_TO_LE(Params->Attributes[i]);
        }

      /* Send command. If it sent ok, then add the transaction. If both ok, return success */
      result = ZclM_SendCommand(Service,
                                Params->SourceEndpoint,
                                &Params->DestAddrMode,
                                Params->ClusterID,
                                tid,
                                totalZclLength,
                                zclBuffer);
      if (result == SZL_RESULT_SUCCESS)
        {
          syncCallback syncCB;
          syncCB.SZL_CB_AttributeReadReportCfgResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                        SZL_ZAB_SYNC_TRANS_TYPE_ZCL_READ_REPORT_CONFIG_REQ,
                                        tid,
                                        &syncCB,
                                        errorRspParams);
        }

      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
  }

  return result;
}


/******************************************************************************
 * Attribute Report Configure request
 ******************************************************************************/
#define M_REPORT_CFG_SIZE_WITHOUT_RC 8
SZL_RESULT_t SZL_AttributeReportCfgReq(zabService* Service,
                                       SZL_CB_AttributeReportCfgResp_t Callback,
                                       SZL_AttributeReportCfgReqParams_t* Params,
                                       szl_uint8* TransactionId)
{
  unsigned8 attrIndex;
  unsigned16 totalZclLength;
  unsigned8 zclBuffer[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
  unsigned8 zclIndex;
  unsigned8 tid;
  SZL_AttributeReportCfgRespParams_t* errorRspParams;
  szl_uint8 zb_data_len;
  syncCallback syncCB;
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED );

  if (SZL_RESULT_SUCCESS == result)
    {

      ZCL_PAYLOAD_t* zcl ;
      ZCL_COMMON_PART_t* common ;
      if ((Callback == NULL) || (Params == NULL) || (Params->NumberOfAttributes == 0))
        {
          return SZL_RESULT_INVALID_DATA;
        }

      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Calculate total length and confirm it is reasonable */
      totalZclLength = ZCL_FRAME_SIZE(Params->ManufacturerSpecific, (Params->NumberOfAttributes * M_REPORT_CFG_SIZE_WITHOUT_RC));
      for (attrIndex = 0; attrIndex < Params->NumberOfAttributes; attrIndex++)
        {
          if (SZLEXT_ZigBeeDataTypeIsAnalog(Params->Attributes[attrIndex].DataType) == szl_true)
            {
              if (Params->Attributes[attrIndex].ReportableChangeData != NULL)
                {
                  totalZclLength += ZbDataSize(Params->Attributes[attrIndex].DataType, Params->Attributes[attrIndex].ReportableChangeData);
                }
              else
                {
                  return SZL_RESULT_INVALID_DATA;
                }
            }
        }
      if (totalZclLength > SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH)
        {
          return SZL_RESULT_DATA_TOO_BIG;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_AttributeReportCfgRespParams_t*)szl_mem_alloc(SZL_AttributeReportCfgRespParams_t_SIZE(Params->NumberOfAttributes),
                                                                          MALLOC_ID_SZL_CFG_REP_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      szl_zab_GetResponseAddrFromRequestDestination(&Params->DestAddrMode, &errorRspParams->SourceAddress);
      errorRspParams->DestinationEndpoint = Params->SourceEndpoint;
      errorRspParams->ManufacturerSpecific = Params->ManufacturerSpecific;
      errorRspParams->ClusterID = Params->ClusterID;
      errorRspParams->NumberOfAttributes = Params->NumberOfAttributes;
      for(attrIndex = 0; attrIndex < Params->NumberOfAttributes; attrIndex++)
        {
          errorRspParams->Attributes[attrIndex].AttributeID = Params->Attributes[attrIndex].AttributeID;
          errorRspParams->Attributes[attrIndex].Status = SZL_STATUS_NO_RESPONSE;
        }

      /* Set up the ZCL header, starting with Frame Control & Manufacturer Code */
      zcl = (ZCL_PAYLOAD_t*)zclBuffer;
      zcl->frame_type.frame_control.frame_type = ZCL_FRAME_TYPE_PROFILE_COMMAND;
      zcl->frame_type.frame_control.manufacture_specific = Params->ManufacturerSpecific;
      zcl->frame_type.frame_control.direction = ZCL_FRAME_DIR_CLIENT_SERVER;
      zcl->frame_type.frame_control.disable_default_rsp = ZCL_FRAME_DEFAULT_RESPONSE_ALWAYS;
      zcl->frame_type.frame_control.reserved = 0;

      if (zcl->frame_type.frame_control.manufacture_specific)
        {
          zcl->frame_type.manufacture_frame.manufacture_code = UINT16_TO_LE(ZB_SCHNEIDER_MANUFACTURE_ID);
        }

      // fill common header part
      common = ZCL_GET_COMMON_PART_PTR(zcl);
      common->tr_id = tid;
      common->cmd = ZCL_CMD_ID_CONFIGURE_REPORTING;

      zclIndex = 0;
      for (attrIndex = 0; attrIndex < Params->NumberOfAttributes; attrIndex++)
        {
          common->payload[zclIndex++] = (unsigned8)ZCL_CFG_REPORT_DIRECTION_SEND_REPORT;
          COPY_OUT_16_BITS(&common->payload[zclIndex], Params->Attributes[attrIndex].AttributeID); zclIndex += MACRO_DATA_SIZE_WORD;
          common->payload[zclIndex++] = Params->Attributes[attrIndex].DataType;
          COPY_OUT_16_BITS(&common->payload[zclIndex], Params->Attributes[attrIndex].MinInterval); zclIndex += MACRO_DATA_SIZE_WORD;
          COPY_OUT_16_BITS(&common->payload[zclIndex], Params->Attributes[attrIndex].MaxInterval); zclIndex += MACRO_DATA_SIZE_WORD;

          /* Add Reportable Change if datatype is analog */
          if (SZLEXT_ZigBeeDataTypeIsAnalog(Params->Attributes[attrIndex].DataType) == szl_true)
            {

              if (!DataTypeIsPrimitive(Params->Attributes[attrIndex].DataType) )
                {
                  szl_mem_free(errorRspParams);
                  return SZL_RESULT_INVALID_DATA;
                }
              /* Convert the native stream into LE type */
              zb_data_len = NativeToZb(Params->Attributes[attrIndex].ReportableChangeData, Params->Attributes[attrIndex].LenRC, Params->Attributes[attrIndex].DataType, &common->payload[zclIndex]);
              zclIndex += zb_data_len;

            }
        }

      /* Send command. If it sent ok, then add the transaction. If both ok, return success */
      result = ZclM_SendCommand(Service,
                                Params->SourceEndpoint,
                                &Params->DestAddrMode,
                                Params->ClusterID,
                                tid,
                                totalZclLength,
                                zclBuffer);
      if (result == SZL_RESULT_SUCCESS)
        {
          syncCB.SZL_CB_AttributeReportCfgResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                        SZL_ZAB_SYNC_TRANS_TYPE_ZCL_CONFIGURE_REPORT_REQ,
                                        tid,
                                        &syncCB,
                                        errorRspParams);
        }

      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}




/******************************************************************************
 * Default Response Request - ADDED BY MVDB
 ******************************************************************************/
SZL_RESULT_t SZL_DefaultResponseReq(zabService* Service,
                                    SZL_DefaultRspReqParams_t* Params)
{
  #define M_DEFAULT_RSP_PAYLOAD_LENGTH 2
  unsigned8 zclBuffer[SZL_SZL_M_SZL_HEADER_SIZE_MAX + M_DEFAULT_RSP_PAYLOAD_LENGTH];
  SZL_Addresses_t destAddr;
  ZCL_PAYLOAD_t* zcl;
  ZCL_COMMON_PART_t* common;

  if (Params == NULL)
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /* Set up the ZCL header, starting with Frame Control & Manufacturer Code */
  zcl = (ZCL_PAYLOAD_t*)zclBuffer;
  zcl->frame_type.frame_control.frame_type = ZCL_FRAME_TYPE_PROFILE_COMMAND;
  zcl->frame_type.frame_control.manufacture_specific = Params->ManufacturerSpecific;
  zcl->frame_type.frame_control.direction = Params->Direction;
  zcl->frame_type.frame_control.disable_default_rsp = ZCL_FRAME_DEFAULT_RESPONSE_ON_ERROR;
  zcl->frame_type.frame_control.reserved = 0;
  if (zcl->frame_type.frame_control.manufacture_specific)
    {
      zcl->frame_type.manufacture_frame.manufacture_code = UINT16_TO_LE(Params->ManufacturerCode);
    }

  // fill common header part
  common = ZCL_GET_COMMON_PART_PTR(zcl);
  common->tr_id = Params->TransactionId;
  common->cmd = ZCL_CMD_ID_DEFAULT_RSP;

  // Copy in the data
  common->payload[0] = Params->CommandId;
  common->payload[1] = Params->Status;

  /* Configure address and send command */
  destAddr.AddressMode = SZL_ADDRESS_MODE_NWK_ADDRESS;
  destAddr.Addresses.Nwk.Address = Params->DestinationNwkAddress;
  destAddr.Addresses.Nwk.Endpoint = Params->DestinationEndpoint;

  return ZclM_SendCommand(Service,
                          Params->SourceEndpoint,
                          &destAddr,
                          Params->ClusterID,
                          Params->TransactionId,
                          ZCL_FRAME_SIZE(Params->ManufacturerSpecific, M_DEFAULT_RSP_PAYLOAD_LENGTH),
                          zclBuffer);
}



/**
 * Attribute Changed Indication
 *
 * This function is used to tell the library that the APP has changed the attribute
 */
SZL_RESULT_t SZL_AttributeChangedInd(zabService* Service, SZL_AttributeChangedIndParams_t* Params)
{
    // Not required by ZAB
    return SZL_RESULT_SUCCESS;
}

/**
 * ClientCluster Register
 *
 * This function is used to register client clusters per endpoint
 */
SZL_RESULT_t SZL_ClientClusterRegister(zabService* Service, SZL_EP_t Endpoint, const szl_uint16* ClientClusters, szl_uint8 Count)
{
    szl_uint8 ccCount;
    int i;
    SZL_RESULT_t result = Szl_InternalIsLibInitialized(Service);
    if (result == SZL_RESULT_SUCCESS)
    {
        if ( (Count > 0) && (ClientClusters != NULL) )
          {
            // calculate used entries
            for (ccCount = 0; ccCount < SZL_CFG_MAX_CLIENT_CLUSTERS; ccCount++)
            {
                if ((LIB_DATA(Service).Lib.ClientClusters[ccCount].Ep == 0xFF) &&
                    (LIB_DATA(Service).Lib.ClientClusters[ccCount].ClusterID == 0xFFFF))
                  {
                    break;
                  }
            }

            // check boundary for client binding cluster
            if ((ccCount + Count) > SZL_CFG_MAX_CLIENT_CLUSTERS)
            {
                return SZL_RESULT_OUT_OF_RANGE;
            }

            for(i=0; i < Count; i++)
            {
                LIB_DATA(Service).Lib.ClientClusters[ccCount].Ep = Endpoint;
                LIB_DATA(Service).Lib.ClientClusters[ccCount].ClusterID = ClientClusters[i];
                ccCount ++;
            }

            // sort the table
            bubble_sort(LIB_DATA(Service).Lib.ClientClusters, ccCount, sizeof(SZL_ClientCluster_t), Szl_ClientClustersCompare);

            /* ARTF152098: Now flag for updating of endpoint simple descriptor */
            LIB_DATA(Service).EndpointUpdateRequired = szl_true;
            return SZL_RESULT_SUCCESS;
          }

        return SZL_RESULT_INVALID_DATA;
    }
    return result;
}





/**
 * Handler add AttributeChangedNtf
 *
 */
SZL_RESULT_t SZL_AttributeChangedNtfRegisterCB(zabService* Service, SZL_CB_AttributeChangedNtf_t Callback)
{
    return CbHandler_Add(Service, CB_FUNC_ATTRIBUTE_CHANGED_NTF, (CBQueue_CallbackProto)Callback);
}

/**
 * Handler remove AttributeReportNtf
 *
 */
SZL_RESULT_t SZL_AttributeChangedNtfUnregisterCB(zabService* Service, SZL_CB_AttributeChangedNtf_t Callback)
{
    return CbHandler_Remove(Service, CB_FUNC_ATTRIBUTE_CHANGED_NTF, (CBQueue_CallbackProto)Callback);
}

/**
 * Handler add AttributeReportNtf
 *
 */
SZL_RESULT_t SZL_AttributeReportNtfRegisterCB(zabService* Service, SZL_CB_AttributeReportNtf_t Callback)
{
  return CbHandler_Add(Service, CB_FUNC_ATTRIBUTE_REPORT_NTF, (CBQueue_CallbackProto)Callback);
}

/**
 * Handler remove AttributeReportNtf
 *
 */
SZL_RESULT_t SZL_AttributeReportNtfUnregisterCB(zabService* Service, SZL_CB_AttributeReportNtf_t Callback)
{
    return CbHandler_Remove(Service, CB_FUNC_ATTRIBUTE_REPORT_NTF, (CBQueue_CallbackProto)Callback);
}


/**
 * Handler add NwkLeaveNtf
 *
 */
SZL_RESULT_t SZL_NwkLeaveNtfRegisterCB(zabService* Service, SZL_CB_NwkLeaveNtf_t Callback)
{
  return CbHandler_Add(Service, CB_FUNC_NWK_LEAVE_NTF, (CBQueue_CallbackProto)Callback);
}

/**
 * Handler remove NwkLeaveNtf
 *
 */
SZL_RESULT_t SZL_NwkLeaveNtfUnregisterCB(zabService* Service, SZL_CB_NwkLeaveNtf_t Callback)
{
    return CbHandler_Remove(Service, CB_FUNC_NWK_LEAVE_NTF, (CBQueue_CallbackProto)Callback);
}

/**
 * Handler add ReceivedSignalStrengthNtf
 *
 */
SZL_RESULT_t SZL_ReceivedSignalStrengthNtfRegisterCB(zabService* Service, SZL_CB_ReceivedSignalStrengthNtf_t Callback)
{
  return CbHandler_Add(Service, CB_FUNC_NWK_RX_SIG_STR_NTF, (CBQueue_CallbackProto)Callback);
}

/**
 * Handler remove SZL_ReceivedSignalStrengthNtf
 *
 */
SZL_RESULT_t SZL_ReceivedSignalStrengthNtfUnregisterCB(zabService* Service, SZL_CB_ReceivedSignalStrengthNtf_t Callback)
{
    return CbHandler_Remove(Service, CB_FUNC_NWK_RX_SIG_STR_NTF, (CBQueue_CallbackProto)Callback);
}

/**
 * Network Leave Request
 *
 * Leave a network
 */
SZL_RESULT_t SZL_NwkLeaveReq(zabService* Service, SZL_CB_NwkLeaveResp_t Callback, SZL_NwkLeaveReqParams_t* Params, szl_uint8* TransactionId)
{
  syncCallback syncCB;
  unsigned8 length;
  szl_uint8 tid;
  SZL_RESULT_t result;
  SZL_NwkLeaveRespParams_t* errorRspParams;
  erStatus localStatus;
  sapMsg* Msg;
  erStatusClear(&localStatus, Service);

  if ( (Service == NULL) || (Callback == NULL) || (Params == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }

  printInfo(Service, "SZL: Leave Request: DstAddr = 0x%04X, DstIeee = 0x%016llX, Rejoin = 0x%02X\n",
            Params->DeviceNwkAddress,
            Params->DeviceIEEEAddress,
            Params->Rejoin);

  /* ARTF111291: Check short address will not be converted to a broadcast by TI stack. Reject broadcast leaves. */
  if (Params->DeviceNwkAddress >= 0xFFF8)
    {
      return SZL_RESULT_INVALID_ADDRESS_MODE;
    }

  tid = szl_zab_GetTransactionId(Service);
  if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
    {
      return SZL_RESULT_NO_FREE_TID;
    }
  if (TransactionId != NULL)
    {
      *TransactionId = tid;
    }

  /* Create the response that will be used in case of no response from the destination */
  errorRspParams = (SZL_NwkLeaveRespParams_t*)szl_mem_alloc(SZL_NwkLeaveRespParams_t_SIZE,
                                                            MALLOC_ID_ZDO_LEAVE_FAKE_RSP);
  if (errorRspParams == NULL)
    {
      return SZL_RESULT_INTERNAL_LIB_ERROR;
    }
  errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
  errorRspParams->DeviceNwkAddress = Params->DeviceNwkAddress;

  /* Allocate SAP message, with enough data for the Params */
  length = SZL_NwkLeaveReqParams_t_SIZE;
  Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
  if ( (erStatusIsError(&localStatus)) || (Msg == NULL) )
    {
      szl_mem_free(errorRspParams);
      return SZL_RESULT_QUEUE_FULL;
    }

  /* Set message parameters */
  sapMsgSetAppDataMax (&localStatus, Msg, length);
  sapMsgSetAppDataLength(&localStatus, Msg, length);
  sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_MGMT_LEAVE_REQ);
  sapMsgSetAppTransactionId(&localStatus, Msg, tid);

  osMemCopy(&localStatus, sapMsgGetAppData(Msg), (unsigned8*)Params, length);

  sapMsgPutFree(&localStatus, ZAB_SRV(Service)->DataSAP, Msg);

  /* If it sent ok, then add the transaction. If both ok, return success */
  result = SZL_RESULT_FAILED;
  if (erStatusIsOk(&localStatus))
    {
      syncCB.SZL_CB_NwkLeaveResp = Callback;
      result = SzlZab_AddSynchronousTransaction(Service,
                                              SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_LEAVE_REQ,
                                              tid,
                                              &syncCB,
                                              errorRspParams);
    }
  if (result != SZL_RESULT_SUCCESS)
    {
      szl_mem_free(errorRspParams);
    }
  return result;
}


/**
 * SZL_AttributesDeregister
 *
 * This function is used in order to deregister one or more cluster attributes
 * The function can be called multiple times
 */
SZL_RESULT_t SZL_AttributesDeregister(zabService* Service, const SZL_DataPoint_t *DataPoints, szl_uint8 Count)
{
    int idx;
    SZL_DataPoint_t *DataPointMatch;
    szl_bool anyDpRemoved = szl_false;
    SZL_RESULT_t result = Szl_InternalIsLibInitialized( Service );
    if (result != SZL_RESULT_SUCCESS)
        return result;

    if (Count == 0 || DataPoints == NULL)
    {
        return SZL_RESULT_DATA_MISSING;
    }

    for (idx = 0; idx < Count; ++idx)
    {
        DataPointMatch = Szl_DataPointFind(Service, DataPoints[idx].Endpoint, DataPoints[idx].Property.ManufacturerSpecific, DataPoints[idx].ClusterID, DataPoints[idx].AttributeID, szl_false);
        // check if already exist
        if (DataPointMatch)
        {
            anyDpRemoved = szl_true;
            DataPointMatch->Endpoint = 0xFF;
            DataPointMatch->ClusterID = 0xFFFF;
            DataPointMatch->AttributeID = 0xFFFF;
        }
        else
        {
            result = SZL_RESULT_OPERATION_NOT_POSSIBLE;
        }
    }

    // If anything changed sort the data points for quick reference
    if (anyDpRemoved == szl_true)
      {
        bubble_sort(LIB_DATA(Service).Lib.DataPoints, SZL_CFG_MAX_DATAPOINTS, sizeof(SZL_DataPoint_t), Szl_DatapointsCompare);

        /* ARTF152098: Now flag for updating of endpoint simple descriptor */
        LIB_DATA(Service).EndpointUpdateRequired = szl_true;
      }

    return result;
}

/**
 * Attributes Register
 *
 * This should be called after the library has been initialized
 * in order to register one or more attributes
 * The function can be called multiple times
 */
SZL_RESULT_t SZL_AttributesRegister(zabService* Service, const SZL_DataPoint_t* DataPoints, szl_uint8 Count)
{
    int Used = 0;
    int idx;

    SZL_RESULT_t result = Szl_InternalIsLibInitialized(Service);
    if (result == SZL_RESULT_SUCCESS)
    {
        if (Count == 0 || DataPoints == NULL)
        {
            return SZL_RESULT_DATA_MISSING;
        }

        // find first free slot
        for (; Used < SZL_CFG_MAX_DATAPOINTS; Used++)
        {
            if (LIB_DATA(Service).Lib.DataPoints[Used].Endpoint == 0xFF &&
                LIB_DATA(Service).Lib.DataPoints[Used].ClusterID == 0xFFFF &&
                LIB_DATA(Service).Lib.DataPoints[Used].AttributeID == 0xFFFF )
            {
                break; // empty entry
            }
        }

        for ( idx = 0; idx < Count; ++idx)
        {
            // check if already exist
            if (Szl_DataPointFind(Service, DataPoints[idx].Endpoint, DataPoints[idx].Property.ManufacturerSpecific, DataPoints[idx].ClusterID, DataPoints[idx].AttributeID, szl_true))
            {
                if (result == SZL_RESULT_SUCCESS)
                    result = SZL_RESULT_ALREADY_EXISTS;
                continue;
            }

            // validate data
            if ( DataPoints[idx].Property.Access == DP_ACCESS_NONE || !DataPoints[idx].DataSize)
            {
                result = SZL_RESULT_INVALID_DATA;
                continue;
            }

            // Validate the methods
            switch ( DataPoints[idx].Property.Method)
            {
                case DP_METHOD_CALLBACK:
                    if ( ( ((DataPoints[idx].Property.Access & DP_ACCESS_READ) && !DataPoints[idx].AccessType.Callback.Read) ||
                           ((DataPoints[idx].Property.Access & DP_ACCESS_WRITE) && !DataPoints[idx].AccessType.Callback.Write) ) )
                    {
                        result = SZL_RESULT_INVALID_DATA;
                        continue;
                    }
                    break;

                case DP_METHOD_DIRECT:
                    if ( DataPoints[idx].AccessType.DirectAccess.Data == NULL)
                    {
                        result = SZL_RESULT_INVALID_DATA;
                        continue;
                    }
                    break;
            }

            // check if room for more
            if (Used >= SZL_CFG_MAX_DATAPOINTS)
            {
                result = SZL_RESULT_TABLE_FULL;
                break;
            }

            // save to global table
            LIB_DATA(Service).Lib.DataPoints[Used++] = DataPoints[idx];
        }

        // Sort the data points for quick reference
        bubble_sort(LIB_DATA(Service).Lib.DataPoints, Used, sizeof(SZL_DataPoint_t), Szl_DatapointsCompare);

        /* ARTF152098: Now flag for updating of endpoint simple descriptor */
        LIB_DATA(Service).EndpointUpdateRequired = szl_true;
    }
    return result;
}

/**
 * Cluster Command Register
 *
 * This should be called after the library has been initialized
 * in order to register one or more cluster commands
 * The function can be called multiple times
 */
SZL_RESULT_t SZL_ClusterCmdRegister(zabService* Service,
                                    const SZL_ClusterCmd_t *ClusterCmds,
                                    szl_uint8 Count)
{
    int Used;
    int idx;
    SZL_RESULT_t result = Szl_InternalIsLibInitialized(Service);

    if (result == SZL_RESULT_SUCCESS)
    {
        if (Count == 0 || ClusterCmds == NULL)
        {
            return SZL_RESULT_DATA_MISSING;
        }

        // find first free slot
        for (Used = 0; Used < SZL_CFG_MAX_CLUSTER_CMDS; Used++)
        {
            if (LIB_DATA(Service).Lib.ClusterCmds[Used].Endpoint == 0xFF &&
                LIB_DATA(Service).Lib.ClusterCmds[Used].ClusterID == 0xFFFF &&
                LIB_DATA(Service).Lib.ClusterCmds[Used].CmdID == 0xFF )
            {
                break; // empty entry
            }
        }

        for ( idx = 0; idx < Count; ++idx)
        {
            // check if already exist
            if (Szl_ClusterCmdFind(Service, NULL, ClusterCmds[idx].Endpoint, ClusterCmds[idx].ManufacturerSpecific, ClusterCmds[idx].ClusterID, ClusterCmds[idx].CmdID, ClusterCmds[idx].OperationType == SZL_CLU_OP_TYPE_CUSTOM_SERVER_TO_CLIENT, szl_true))
            {
                result = SZL_RESULT_ALREADY_EXISTS;
                continue;
            }

            // check if room for more
            if (Used >= SZL_CFG_MAX_CLUSTER_CMDS)
            {
                return SZL_RESULT_OUT_OF_RANGE;
            }

            // validate data
            switch ( ClusterCmds[idx].OperationType)
            {
                case SZL_CLU_OP_TYPE_CUSTOM_CLIENT_TO_SERVER:
                case SZL_CLU_OP_TYPE_CUSTOM_SERVER_TO_CLIENT:
                    if ( ClusterCmds[idx].OperationAccess.Callback == NULL)
                    {
                        result = SZL_RESULT_INVALID_DATA;
                        continue;
                    }
                    break;

                default:
                    // do validation
                    if ( ClusterCmds[idx].OperationType < SZL_CLU_OP_TYPE_MIN_ ||
                         ClusterCmds[idx].OperationType > SZL_CLU_OP_TYPE_MAX_ )
                    {
                        result = SZL_RESULT_INVALID_DATA;
                        continue;
                    }

                    // find the required callback methods for the command
                    ENUM_SZL_DP_ACCESS_t dpAccessType = Szl_ClusterCmdOperation2AccessType(ClusterCmds[idx].OperationType);

                    if ( dpAccessType == DP_ACCESS_NONE)
                    {
                        result = SZL_RESULT_INVALID_DATA;
                        continue;
                    }

                    //FIXME - need to add szl_bool manufactureSpecific, for the operator attribute as well as the cluster command itself
                    SZL_DataPoint_t* dp = Szl_DataPointFind(Service, SZL_ADDRESS_EP_ALL, ClusterCmds[idx].OperationAccess.Attribute.ManufacturerSpecific , ClusterCmds[idx].ClusterID, ClusterCmds[idx].OperationAccess.Attribute.AttributeID, szl_true);

                    // Check that methods are fulfilled
                    if (dp == NULL ||                                                                           // Datapoint not found
                        ( (dp->Property.Method == DP_METHOD_CALLBACK) &&                                        // callback datapoint and
                          ( ( (dpAccessType & DP_ACCESS_WRITE) && (dp->AccessType.Callback.Write == NULL)) ||   //      Write required but not provided
                            ( (dpAccessType & DP_ACCESS_READ)  && (dp->AccessType.Callback.Read  == NULL)) )    // or   Read required but not provided
                        ) ||
                        ( (dp->Property.Method == DP_METHOD_DIRECT) &&                                          // direct datapoint and
                           dp->AccessType.DirectAccess.Data == NULL                                             //      pointer NULL
                        )
                       )
                    {
                        result = SZL_RESULT_OPERATION_NOT_POSSIBLE;
                        continue;
                    }
                    break;
            }

            // save to global table
            LIB_DATA(Service).Lib.ClusterCmds[Used++] = ClusterCmds[idx];
        }

        // Sort the data points for quick reference
        bubble_sort(LIB_DATA(Service).Lib.ClusterCmds, Used, sizeof(SZL_ClusterCmd_t), Szl_ClusterCmdsCompare);

        /* ARTF152098: Now flag for updating of endpoint simple descriptor */
        LIB_DATA(Service).EndpointUpdateRequired = szl_true;
    }
    return result;
}


/**
 * Cluster Command Deregister
 *
 * This function is used in order to deregister one or more cluster commands
 * The function can be called multiple times
 */
SZL_RESULT_t SZL_ClusterCmdDeregister(zabService* Service, const SZL_ClusterCmd_t *ClusterCmds, szl_uint8 Count)
{
    int idx;
    szl_bool anyCmdRemoved = szl_false;
    SZL_ClusterCmd_t *ClusterCmdsMatch;

    SZL_RESULT_t result = Szl_InternalIsLibInitialized(Service);
    if (result != SZL_RESULT_SUCCESS)
        return result;

    if (Count == 0 || ClusterCmds == NULL)
    {
        return SZL_RESULT_DATA_MISSING;
    }

    for (idx = 0; idx < Count; ++idx)
    {
        ClusterCmdsMatch = Szl_ClusterCmdFind(Service, NULL, ClusterCmds[idx].Endpoint, ClusterCmds[idx].ManufacturerSpecific, ClusterCmds[idx].ClusterID, ClusterCmds[idx].CmdID, ClusterCmds[idx].OperationType == SZL_CLU_OP_TYPE_CUSTOM_SERVER_TO_CLIENT, szl_false);
        // check if already exist
        if (ClusterCmdsMatch)
        {
            anyCmdRemoved = szl_true;
            ClusterCmdsMatch->Endpoint = 0xFF;
            ClusterCmdsMatch->CmdID = 0xFF;
            ClusterCmdsMatch->ClusterID = 0xFFFF;
        }
        else
        {
            result = SZL_RESULT_OPERATION_NOT_POSSIBLE;
        }
    }

    // Sort the data points for quick reference
    if (anyCmdRemoved == szl_true)
      {
        bubble_sort(LIB_DATA(Service).Lib.ClusterCmds, SZL_CFG_MAX_CLUSTER_CMDS, sizeof(SZL_ClusterCmd_t), Szl_ClusterCmdsCompare);

        /* ARTF152098: Now flag for updating of endpoint simple descriptor */
        LIB_DATA(Service).EndpointUpdateRequired = szl_true;
      }

    return result;
}

/**
 * Cluster Cmd Request
 *
 * This performs a cluster command on a device
 */
SZL_RESULT_t SZL_ClusterCmdReq(zabService* Service,
                               SZL_CB_ClusterCmdResp_t Callback,
                               SZL_ClusterCmdReqParams_t* Params,
                               szl_uint8* TransactionId)
{
   return  SZL_ClusterCmdReqTimeout(Service,
                               Callback,
                               Params,
                               TransactionId,
                               SZL_ZAB_M_SYNCHRONOUS_TRANSACTION_TIMEOUT_S,
                               ZCL_FRAME_DEFAULT_RESPONSE_ALWAYS);
}
/**
 * Cluster Cmd Request
 *
 * This performs a cluster command on a device
 */
SZL_RESULT_t SZL_ClusterCmdReqTimeout(zabService* Service,
                               SZL_CB_ClusterCmdResp_t Callback,
                               SZL_ClusterCmdReqParams_t* Params,
                               szl_uint8* TransactionId,
                               szl_uint8 TimeoutForResp,
                               szl_uint8 DefaultRespDisabled)
{
  unsigned16 totalZclLength;
  unsigned8 zclBuffer[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
  unsigned8 tid;
  SZL_ClusterCmdRespParams_t* errorRspParams;
  syncCallback syncCB;
  SZL_RESULT_t result;

  if ( (Service == NULL) || (Callback == NULL) || (Params == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }

  result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED );
  if (SZL_RESULT_SUCCESS == result)
    {
      ZCL_PAYLOAD_t* zcl;
      ZCL_COMMON_PART_t* common;

      printInfo(Service, "SZL ZCL: Cluster Command Request: SrcEp = 0x%02X, DstAddr = ?, DstEp = ?, Cl = 0x%04X, Cmd = 0x%02X, DataLength = 0x%02X, Data =",
                Params->SourceEndpoint,
                Params->ClusterID,
                Params->Command,
                Params->PayloadLength);
      for (totalZclLength = 0; totalZclLength < Params->PayloadLength; totalZclLength++)
        {
          printInfo(Service, " %02X", Params->Payload[totalZclLength]);
        }
      printInfo(Service, "\n");


      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Calculate total length and confirm it is reasonable */
      totalZclLength = ZCL_FRAME_SIZE(Params->ManufacturerSpecific, Params->PayloadLength);
      if (totalZclLength > SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH)
        {
          printError(Service, "SZL ZCL WARNING: Cluster Command Request: Data length 0x%02X exceeds max of 0x%02X\n", totalZclLength, SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH);
          return SZL_RESULT_DATA_TOO_BIG;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ClusterCmdRespParams_t*)szl_mem_alloc(SZL_ClusterCmdRespParams_t_SIZE(0),
                                                                  MALLOC_ID_SZL_CLUSTER_CMD_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      szl_zab_GetResponseAddrFromRequestDestination(&Params->DestAddrMode, &errorRspParams->SourceAddress);
      errorRspParams->DestinationEndpoint = Params->SourceEndpoint;
      errorRspParams->ManufacturerSpecific = Params->ManufacturerSpecific;
      errorRspParams->ClusterID = Params->ClusterID;
      errorRspParams->Command = 0xFF;
      errorRspParams->PayloadLength = 0;

      /* Set up the ZCL header */
      zcl = (ZCL_PAYLOAD_t*)zclBuffer;
      zcl->frame_type.frame_control.frame_type = ZCL_FRAME_TYPE_CLUSTER_COMMAND;
      zcl->frame_type.frame_control.manufacture_specific = Params->ManufacturerSpecific;
      zcl->frame_type.frame_control.direction = Params->Direction;
      zcl->frame_type.frame_control.disable_default_rsp = DefaultRespDisabled; // we always need a reply to get the CB called.
      zcl->frame_type.frame_control.reserved = 0;
      if (zcl->frame_type.frame_control.manufacture_specific)
        {
          zcl->frame_type.manufacture_frame.manufacture_code = UINT16_TO_LE(ZB_SCHNEIDER_MANUFACTURE_ID);
        }

      // fill common header part
      common = ZCL_GET_COMMON_PART_PTR(zcl);
      common->tr_id = tid;
      common->cmd = Params->Command;

      // Copy in the data
      szl_memcpy(common->payload, Params->Payload, Params->PayloadLength);

      /* Send command. If it sent ok, then add the transaction. If both ok, return success */
      result = ZclM_SendCommand(Service,
                                Params->SourceEndpoint,
                                &Params->DestAddrMode,
                                Params->ClusterID,
                                tid,
                                totalZclLength,
                                zclBuffer);
      if (result == SZL_RESULT_SUCCESS)
        {
          syncCB.SZL_CB_ClusterCmdResp = Callback;
          result = SzlZab_AddSynchronousTransactionTimeout(Service,
                                        SZL_ZAB_SYNC_TRANS_TYPE_ZCL_CLUSTER_REQ,
                                        tid,
                                        &syncCB,
                                        errorRspParams,
                                        TimeoutForResp);
        }

      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}

/**
 *
 * Plugin Register
 *
 * Register a plugin with the SZL.
 * Requested data will be allocated for the Service.
 */
SZL_RESULT_t SZL_PluginRegister(zabService* Service, SZL_PLUGIN_ID_t PluginId, szl_uint32 PluginDataSize, void** PluginDataPtr)
{
    int idx;
    int freeIdx;
    void* dataPtr = NULL;
    SZL_RESULT_t result = Szl_InternalIsLibInitialized(Service);
    if (result == SZL_RESULT_SUCCESS)
    {
        // Validate parameters
        if ( (PluginDataSize == 0) || (PluginDataPtr == NULL) )
        {
            return SZL_RESULT_DATA_MISSING;
        }
        if ( (PluginId == SZL_PLUGIN_ID_NONE) || (PluginId == SZL_PLUGIN_ID_RESERVED) )
        {
            return SZL_RESULT_INVALID_DATA;
        }

        // Search table:
        //  - If plugin Id already registered, return error.
        //  - Save first free index to use otherwise.
        freeIdx = SZL_CFG_MAX_PLUGINS;
        for (idx = 0; idx < SZL_CFG_MAX_PLUGINS; idx++)
        {
            if (LIB_DATA(Service).Plugins[idx].pluginId == PluginId)
            {
                return SZL_RESULT_ALREADY_EXISTS;
            }
            if ( (LIB_DATA(Service).Plugins[idx].pluginId == SZL_PLUGIN_ID_NONE) &&
                 (freeIdx >= SZL_CFG_MAX_PLUGINS) )
            {
                freeIdx = idx;
            }
        }

        if (freeIdx < SZL_CFG_MAX_PLUGINS)
        {
            /* Malloc data requested by the plugin. Initialise it to zero if successful*/
            dataPtr = szl_mem_alloc(PluginDataSize,
                                    MALLOC_ID_CFG_PLUGIN);
            if (dataPtr == NULL)
              {
                return SZL_RESULT_INTERNAL_LIB_ERROR;
              }
            szl_memset(dataPtr, 0, PluginDataSize);

            // Everything is good, so register data in datable and return success
            LIB_DATA(Service).Plugins[freeIdx].pluginId = PluginId;
            LIB_DATA(Service).Plugins[freeIdx].pluginData = dataPtr;
            *PluginDataPtr = dataPtr;
            return SZL_RESULT_SUCCESS;
        }

        return SZL_RESULT_TABLE_FULL;
    }
    return result;
}

/**
 *
 * Plugin Unregister
 *
 * Unregister a plugin from the SZL.
 * Previously allocated data will be free'd
 */
SZL_RESULT_t SZL_PluginUnregister(zabService* Service, SZL_PLUGIN_ID_t PluginId)
{
    int idx;
    SZL_RESULT_t result = Szl_InternalIsLibInitialized(Service);
    if (result == SZL_RESULT_SUCCESS)
    {
        // Validate parameters
        if ( (PluginId == SZL_PLUGIN_ID_NONE) || (PluginId == SZL_PLUGIN_ID_RESERVED) )
        {
            return SZL_RESULT_INVALID_DATA;
        }

        // Find, free and clear the plugin data
        for (idx = 0; idx < SZL_CFG_MAX_PLUGINS; idx++)
        {
            if (LIB_DATA(Service).Plugins[idx].pluginId == PluginId)
            {
                szl_mem_free(LIB_DATA(Service).Plugins[idx].pluginData);
                LIB_DATA(Service).Plugins[idx].pluginData = NULL;
                LIB_DATA(Service).Plugins[idx].pluginId = SZL_PLUGIN_ID_NONE;
                return SZL_RESULT_SUCCESS;
            }
        }

        return SZL_RESULT_NOT_FOUND;
    }
    return result;
}

/**
 *
 * Plugin Get Data Pointer
 *
 * Get the data pointer for a registered plugin
 */
SZL_RESULT_t SZL_PluginGetDataPointer(zabService* Service, SZL_PLUGIN_ID_t PluginId, void** PluginDataPtr)
{
    int idx;
    SZL_RESULT_t result = Szl_InternalIsLibInitialized(Service);
    if (result == SZL_RESULT_SUCCESS)
    {
        // Validate parameters
        if (PluginDataPtr == NULL)
        {
            return SZL_RESULT_DATA_MISSING;
        }
        if ( (PluginId == SZL_PLUGIN_ID_NONE) || (PluginId == SZL_PLUGIN_ID_RESERVED) )
        {
            return SZL_RESULT_INVALID_DATA;
        }

        // Find, free and clear the plugin data
        for (idx = 0; idx < SZL_CFG_MAX_PLUGINS; idx++)
        {
            if (LIB_DATA(Service).Plugins[idx].pluginId == PluginId)
            {
                *PluginDataPtr = LIB_DATA(Service).Plugins[idx].pluginData;
                return SZL_RESULT_SUCCESS;
            }
        }

        return SZL_RESULT_NOT_FOUND;
    }
    return result;
}