/*
Name:    Message Utility
Author:  ZigBee Excellence Center
Company: Schneider Electric

Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

Description:


 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 00.00.06.02  30-Sep-14   MvdB   artf55512: Support network key change action
 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 * 002.000.004  01-Apr-15   MvdB   ARTF56504: Support missed channel change recovery, with actions/notifications/asks/gives to get/set parameters.
 * 002.002.005  11-Sep-15   MvdB   ARTF112436: Add sapMsgPutNotifyNetworkInfoState()
 * 002.002.011  12-Oct-15   MvdB   ARTF104108: Add notification of progress for Clone actions
 * 002.002.020  18-Mar-16   MvdB   Use a localStatus in sapMsgPutNotifyError() as status is probably already bad
*/
#include "osTypes.h"
#include "zabCoreService.h"
#include "sapMsgUtility.h"    // message functions
#include "sapCorePrivate.h"   // SAP private
#include "zabCoreConfigure.h"
#include "sapMsgPrivate.h"    // message functions

#include <string.h>



#define SAP_SAFE_LOCK( Service, id )   while (id) { ZAB_PROTECT_ENTER( (void*)(Service), (unsigned8)(id) ); break; }    // enters a mutex if id > 0
#define SAP_SAFE_UNLOCK( Service, id ) while (id) { ZAB_PROTECT_EXIT(  (void*)(Service), (unsigned8)(id) ); break; }    // exits a mutex if id > 0

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/ 


/******************************************************************************
 *                      ******************************
 *                 *****      SERVICE FUNCTIONS       *****
 *                      ******************************
 ******************************************************************************/ 

/******************************************************************************
 * Register Allocate and Free function pointers for a SAP
 ******************************************************************************/
void sapMsgRegAllocate (erStatus* Status, sapHandle SAP, sapMsgAllocateFunc Allocate, sapMsgFreeFunc Free)
{
    SAP_STRUCT(SAP)->Allocate = (void*) Allocate;
    SAP_STRUCT(SAP)->Free = (void*) Free;
}



/******************************************************************************
 *                      ******************************
 *                 *****  DATA MANAGEMENT FUNCTIONS   *****
 *                      ******************************
 ******************************************************************************/ 

/******************************************************************************
 * Allocate a new message to be sent via a SAP, with a specified data length
 ******************************************************************************/
sapMsg* sapMsgAllocateData (erStatus* Status, sapHandle Sap, sapMsgType Type, sapMsgDirection Direction, unsigned32 DataLength)
{
    sapMsgAllocateFunc Allocate = (sapMsgAllocateFunc) SAP_STRUCT(Sap)->Allocate;
    sapMsg* Msg;
    
    ER_CHECK_NULL_ZERO(Status, Allocate);
    
    Msg = (sapMsg*) (*Allocate)(Status, Sap, Type, Direction, DataLength);
    if (Msg == NULL)
      {
        printError(SAP_STRUCT(Sap)->Service, "SAP ERROR: Failed to allocate data!!!\n");
        erStatusSet( Status, SAP_ERROR_NULL_PTR );
      }
    else
      {
        SAP_MSG(Msg)->UseCount = 1;
      }
    return Msg;
}

/******************************************************************************
 * Allocate a new message to be sent via a SAP, with no data.
 * Typically used for actions and notifications
 ******************************************************************************/
sapMsg* sapMsgAllocate (erStatus* Status, sapHandle Sap, sapMsgType Type, sapMsgDirection Direction)
{
    return sapMsgAllocateData(Status, Sap, Type, Direction, 0);
}

/******************************************************************************
 * Free a message after the user is finished with it.
 * This actually decrements a use count and only frees if use count equals zero,
 * allowing multiple tasks to use the message.
 ******************************************************************************/
void sapMsgFree (erStatus* Status, sapMsg* Message)
{
    ER_CHECK_NULL(Status, Message);

    SAP_SAFE_LOCK(sapMsgService(Message), sapMsgMutexId(Message));
    if (SAP_MSG(Message)->UseCount > 0)
    {
        SAP_MSG(Message)->UseCount--;
    }
    else
    {
        erStatusSet(Status, SAP_ERROR_MSG_USE_UNDERFLOW);
        SAP_SAFE_UNLOCK(sapMsgService(Message), sapMsgMutexId(Message));
        return;
    }
    if ((SAP_MSG(Message)->UseCount == 0))
    {
        sapHandle Sap = SAP_MSG(Message)->Sap;

        sapMsgFreeFunc Free = (sapMsgFreeFunc) SAP_STRUCT(Sap)->Free;

        (*Free)(Status, Sap, (void*) Message);
    }

    SAP_SAFE_UNLOCK(sapMsgService(Message), sapMsgMutexId(Message));
}

/******************************************************************************
 * Flag usage of a message.
 * This allows a task to register it's interest in the message, then free
 * it when finished with it.
 * The message will only actually be freed once all tasks have finished using it.
 ******************************************************************************/
void sapMsgUse (erStatus* Status, sapMsg* Message)
{
    SAP_SAFE_LOCK(sapMsgService(Message), sapMsgMutexId(Message));

    if (SAP_MSG(Message)->UseCount < 0xFF)
    {
        SAP_MSG(Message)->UseCount++;
    }
    else
    {
        erStatusSet(Status, SAP_ERROR_MSG_USE_OVERFLOW);
    }

    SAP_SAFE_UNLOCK(sapMsgService(Message), sapMsgMutexId(Message));
}




/******************************************************************************
 *                      ******************************
 *                 *****  DATA TRANSMISSION FUNCTIONS *****
 *                      ******************************
 ******************************************************************************/ 

/******************************************************************************
 * Put a message across a SAP
 ******************************************************************************/
void sapMsgPut(erStatus* Status, sapHandle Sap, sapMsg* Message)
{
    ER_CHECK_NULL(Status, Message);
    sapCorePut(Status, Sap, sapMsgGetDirection(Message), Message);
}

/******************************************************************************
 * Put a message across a SAP and free it afterwards
 ******************************************************************************/
void sapMsgPutFree(erStatus* Status, sapHandle Sap, sapMsg* Message)
{
    ER_CHECK_NULL(Status, Message);
    sapCorePut(Status, Sap, sapMsgGetDirection(Message), Message);
    sapMsgFree(Status, Message);
}




/******************************************************************************
 *                      ******************************
 *                 *****        MESAGE PARSING        *****
 *                      ******************************
 ******************************************************************************/ 


/******************************************************************************
 * Get the mutex id of a message
 ******************************************************************************/
unsigned8 sapMsgMutexId(sapMsg* Message)
{
  return sapCoreGetMutexId(SAP_MSG(Message)->Sap);
}

/******************************************************************************
 * Get the Service of a message
 ******************************************************************************/
sapService sapMsgService(sapMsg* Message)
{
  return sapCoreGetService(SAP_MSG(Message)->Sap);
}

/******************************************************************************
 * Get/Set the direction of a message
 ******************************************************************************/
sapMsgDirection sapMsgGetDirection(sapMsg* Message)
{
  return SAP_MSG(Message)->FlagsType.MessageDirection;
}
void sapMsgSetDirection(erStatus* Status, sapMsg* Message, sapMsgDirection Direction)
{
  ER_CHECK_NULL(Status, Message);
  SAP_MSG(Message)->FlagsType.MessageDirection = Direction;
}

/******************************************************************************
 * Get/Set the type of a message
 ******************************************************************************/
sapMsgType sapMsgGetType(sapMsg* Message)
{
  return SAP_MSG(Message)->FlagsType.MessageType;
}
void sapMsgSetType(erStatus* Status, sapMsg* Message, sapMsgType Type)
{
    ER_CHECK_NULL(Status, Message);
    SAP_MSG(Message)->FlagsType.MessageType = Type;
}

/******************************************************************************
 * Get/Set the type of action
 ******************************************************************************/
zabAction sapMsgGetAction(sapMsg* Message)
{
  return (zabAction)SAP_MSG(Message)->Data.Action.Action;
}
void sapMsgSetAction(erStatus* Status, sapMsg* Message, zabAction Action)
{
    ER_CHECK_NULL(Status, Message);
    SAP_MSG(Message)->Data.Action.Action = (unsigned8)Action;
}

/******************************************************************************
 * Get/Set the type of a notification
 ******************************************************************************/
zabNotificationType sapMsgGetNotifyType(sapMsg* Message)
{
  return SAP_MSG(Message)->Data.Notify.NotificationType;
}
void sapMsgSetNotifyType(erStatus* Status, sapMsg* Message, zabNotificationType NotificationType)
{
  ER_CHECK_NULL(Status, Message);
  SAP_MSG(Message)->Data.Notify.NotificationType = NotificationType;
}

/******************************************************************************
 * Get/Set the data of a notification
 ******************************************************************************/
zabNotificationData sapMsgGetNotifyData(sapMsg* Message)
{
    return SAP_MSG(Message)->Data.Notify.NotificationData;
}
void sapMsgSetNotifyData(erStatus* Status, sapMsg* Message, zabNotificationData NotificationData)
{
  ER_CHECK_NULL(Status, Message);
  SAP_MSG(Message)->Data.Notify.NotificationData = NotificationData;
}


/******************************************************************************
 * Get/Set the app type
 ******************************************************************************/
zabMsgAppType sapMsgGetAppType(sapMsg* Message)
{
  return (zabMsgAppType)SAP_MSG(Message)->Data.App.Type;
}
void sapMsgSetAppType(erStatus* Status, sapMsg* Message, zabMsgAppType App)
{
    ER_CHECK_NULL(Status, Message);
    SAP_MSG(Message)->Data.App.Type = (unsigned8)App;
}

/******************************************************************************
 * Get/Set the max app data length
 ******************************************************************************/
unsigned16 sapMsgGetAppDataMax(sapMsg* Message)
{
  return SAP_MSG(Message)->Data.App.DataMax;
}
void sapMsgSetAppDataMax(erStatus* Status, sapMsg* Message, unsigned16 Max)
{
    ER_CHECK_NULL(Status, Message);
    SAP_MSG(Message)->Data.App.DataMax = Max;
}

/******************************************************************************
 * Get/Set the app data length
 ******************************************************************************/
unsigned16 sapMsgGetAppDataLength(sapMsg* Message)
{
  return SAP_MSG(Message)->Data.App.DataLength;
}
void sapMsgSetAppDataLength(erStatus* Status, sapMsg* Message, unsigned16 Length)
{
    ER_CHECK_NULL(Status, Message);

    if (Length > sapMsgGetAppDataMax(Message))
    {
        erStatusSet(Status, ER_ERROR_BUFFER_OVERFLOW);
    }
    else
    {
        SAP_MSG(Message)->Data.App.DataLength = Length;
    }
}

/******************************************************************************
 * Get/Set the app data length
 ******************************************************************************/
unsigned8* sapMsgGetAppData(sapMsg* Message)
{
  return SAP_MSG(Message)->Data.App.DataPointer;
}
void sapMsgCopyAppDataTo(erStatus* Status, sapMsg* Message, unsigned8* Buffer, unsigned16 Length)
{
    erStatus localStatus;
    erStatusClear(&localStatus, sapMsgService(Message));
    
    ER_CHECK_NULL(Status, Message);

    if (Length > sapMsgGetAppDataMax(Message))
    {
        erStatusSet(Status, ER_ERROR_BUFFER_OVERFLOW);
        return;
    }

    osMemCopy(&localStatus, sapMsgGetAppData(Message), Buffer, Length);

    if (erStatusIsOk(&localStatus))
    {
        sapMsgSetAppDataLength(Status, Message, Length);
    }
    else
    {
        erStatusSet(Status, erStatusGetError(&localStatus));
    }
}

/******************************************************************************
 * Get/Set the app data transaction id
 ******************************************************************************/
unsigned8 sapMsgGetAppTransactionId(sapMsg* Message)
{
  return SAP_MSG(Message)->Data.App.TransactionId;
}
void sapMsgSetAppTransactionId(erStatus* Status, sapMsg* Message, unsigned8 TransactionId)
{
    ER_CHECK_NULL(Status, Message);
    SAP_MSG(Message)->Data.App.TransactionId = TransactionId;
}




/******************************************************************************
 *                      ******************************
 *                 *****      SHORTCUT FUNCTIONS      *****
 *                      ******************************
 ******************************************************************************/ 

/******************************************************************************
 * Put an action across a SAP
 ******************************************************************************/
void sapMsgPutAction(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabAction Action)
{
    sapMsg* Msg;

    Msg = sapMsgAllocate(Status, Sap, SAP_MSG_TYPE_ACTION, Direction);
    if (Msg != NULL)
      {
        sapMsgSetAction(Status, Msg, Action);
        sapMsgPutFree(Status, Sap, Msg);
      }
}

/******************************************************************************
 * Put a NOTIFY across a SAP - Generic Function
 ******************************************************************************/
void sapMsgPutNotify(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabNotificationType Type, zabNotificationData Data)
{
    sapMsg* Msg = NULL;

    Msg = sapMsgAllocate(Status, Sap, SAP_MSG_TYPE_NOTIFY, Direction);
    if (Msg != NULL)
      {
        sapMsgSetNotifyType(Status, Msg, Type);
        sapMsgSetNotifyData(Status, Msg, Data);

        sapMsgPutFree(Status, Sap, Msg);
      }
}

/******************************************************************************
 * Put a NOTIFY(OPEN STATE) across a SAP
 ******************************************************************************/
void sapMsgPutNotifyOpenState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabOpenState OpenState)
{
  zabNotificationData Data;
  Data.openState = OpenState;
  sapMsgPutNotify(Status, Sap, Direction, ZAB_NOTIFICATION_OPEN_STATE, Data);
}

/******************************************************************************
 * Put a NOTIFY(NETWORK STATE) across a SAP
 ******************************************************************************/
void sapMsgPutNotifyNetworkState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabNwkState NwkState)
{
  zabNotificationData Data;
  Data.nwkState = NwkState;
  sapMsgPutNotify(Status, Sap, Direction, ZAB_NOTIFICATION_NETWORK_STATE, Data);
}

/******************************************************************************
 * Put a NOTIFY(FIRMWARE UPDATE STATE) across a SAP
 ******************************************************************************/
void sapMsgPutNotifyFwUpdateState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabFwUpdateState FwUpdateState)
{
  zabNotificationData Data;
  Data.fwUpdateState = FwUpdateState;
  sapMsgPutNotify(Status, Sap, Direction, ZAB_NOTIFICATION_FIRMWARE_UPDATE_STATE, Data);
}

/******************************************************************************
 * Put a NOTIFY(FIRMWARE UPDATE PROGRESS) across a SAP
 ******************************************************************************/
void sapMsgPutNotifyFwUpdateProgress(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, unsigned8 FwUpdateProgress)
{
  zabNotificationData Data;
  Data.progress = FwUpdateProgress;
  sapMsgPutNotify(Status, Sap, Direction, ZAB_NOTIFICATION_FIRMWARE_UPDATE_PROGRESS, Data);
}

/******************************************************************************
 * Put a NOTIFY(CLONE STATE) across a SAP
 ******************************************************************************/
void sapMsgPutNotifyCloneState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabCloneState CloneState)
{
  zabNotificationData Data;
  Data.cloneState = CloneState;
  sapMsgPutNotify(Status, Sap, Direction, ZAB_NOTIFICATION_CLONE_STATE, Data);
}

/******************************************************************************
 * Put a NOTIFY(CLONE PROGRESS) across a SAP
 ******************************************************************************/
void sapMsgPutNotifyCloneProgress(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, unsigned8 Progress)
{
  zabNotificationData Data;
  Data.progress = Progress;
  sapMsgPutNotify(Status, Sap, Direction, ZAB_NOTIFICATION_CLONE_PROGRESS, Data);
}

/******************************************************************************
 * Put a NOTIFY(ERROR) across a SAP
 ******************************************************************************/
void sapMsgPutNotifyError(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabError ErrorNumber)
{
  zabNotificationData Data;
  
  /* Use a local Status as the Status is probably already bad! */
  erStatus localStatus;
  erStatusClear(&localStatus, Status->Service);
  
  Data.errorNumber = ErrorNumber;
  sapMsgPutNotify(&localStatus, Sap, Direction, ZAB_NOTIFICATION_ERROR, Data);
}

/******************************************************************************
 * Put a NOTIFY(NETWORK PERMITJOIN STATE) across a SAP
 ******************************************************************************/
void sapMsgPutNotifyNetworkPermitJoinState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, unsigned16 nwkPermitJoinTime)
{
  zabNotificationData Data;
  Data.nwkPermitJoinTime = nwkPermitJoinTime;
  sapMsgPutNotify(Status, Sap, Direction, ZAB_NOTIFICATION_NETWORK_PERMIT_JOIN, Data);
}

/******************************************************************************
 * Put a NOTIFY(CHANNEL CHANGE STATE) across a SAP
 ******************************************************************************/
void sapMsgPutNotifyChannelChangeState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabChannelChangeState channelChangeState)
{
  zabNotificationData Data;
  Data.channelChangeState = channelChangeState;
  sapMsgPutNotify(Status, Sap, Direction, ZAB_NOTIFICATION_CHANNEL_CHANGE_STATE, Data);
}

/******************************************************************************
 * Put a NOTIFY(NETWORK KEY CHANGE STATE) across a SAP
 ******************************************************************************/
void sapMsgPutNotifyNetworkKeyChangeState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabNetworkKeyChangeState nwkKeyChangeState)
{
  zabNotificationData Data;
  Data.nwkKeyChangeState = nwkKeyChangeState;
  sapMsgPutNotify(Status, Sap, Direction, ZAB_NOTIFICATION_NETWORK_KEY_CHANGE_STATE, Data);
}

/******************************************************************************
 * Put a NOTIFY(NETWORK MAINTENANCE PARAM STATE) across a SAP
 ******************************************************************************/
void sapMsgPutNotifyNetworkMaintParamState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabNetworkMaintenanceParamState networkMaintenanceParamState)
{
  zabNotificationData Data;
  Data.nwkMaintenanceParamState = networkMaintenanceParamState;
  sapMsgPutNotify(Status, Sap, Direction, ZAB_NOTIFICATION_NETWORK_MAINTENANCE_PARAM_STATE, Data);
}

/******************************************************************************
 * Put a NOTIFY(NETWORK INFO STATE) across a SAP
 ******************************************************************************/
void sapMsgPutNotifyNetworkInfoState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabNetworkInfoState networkInfoState)
{
  zabNotificationData Data;
  Data.nwkInfoState = networkInfoState;
  sapMsgPutNotify(Status, Sap, Direction, ZAB_NOTIFICATION_NETWORK_INFO_STATE, Data);
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
