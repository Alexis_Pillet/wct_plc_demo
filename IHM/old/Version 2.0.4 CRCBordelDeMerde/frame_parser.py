# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to parse the frame received from PLC
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from frame import *


def is_ack_success_frame(data):

    # Check frame
    if int(data[0]) == Command.ACK.value:
        if int(data[2]) != Status.ACK_SUCCESS.value:
            return True
        else:
            return False
    else:
        return False


def is_ack_error_frame(data):

        # Check frame
        if int(data[0]) == Command.ACK.value:
            if int(data[2]) != Status.ACK_SUCCESS.value:
                return True, int(data[2])
            else:
                return False, Status.ACK_SUCCESS.value
        else:
            return False, Status.ACK_SUCCESS.value


def is_uninitialized_state_frame(data):

    # Define the desired frame
    master_frame = bytearray([Type.STATE.value, Network.NETWORK.value,
                              NetworkState.UNINITIALIZED.value])

    # Check frame - Command
    if int(data[0]) == Command.CONTROL.value:
        # Check payload
        if data[2:] == master_frame:
            return True
        else:
            return False
    else:
        return False


def is_none_state_frame(data):

    return _check_frame(data, NetworkState.NONE.value)


def is_networking_state_frame(data):

    return _check_frame(data, NetworkState.NETWORKING.value)


def is_networked_state_frame(data):

    # Define the desired frame
    master_frame = bytearray([Type.STATE.value, Network.NETWORK.value,
                              NetworkState.NETWORKED.value])

    # Check frame - Command
    if int(data[0]) == Command.CONTROL.value:
        # Check payload
        if data[3:len(data)-5] == master_frame:
            return True, int(data[4]), bytearray([data[5], data[6]])
        else:
            return False, None, None
    else:
        return False, None, None


def is_data_frame(data):

    # Check frame
    if int(data[0]) == Command.DATA.value:
        return True, data[2:]
    else:
        return False, None


def _check_frame(data, network_status):

    # Define the desired frame
    master_frame = bytearray([Type.STATE.value, Network.NETWORK.value,
                              network_status])

    # Check frame - Command
    if int(data[0]) == Command.CONTROL.value:
        # Check payload
        if data[3:] == master_frame:
            return True
        else:
            return False
    else:
        return False


def compute_crc(data, length):

    # Initialization
    crc = 0

    # XOR of all bytes
    for i in range(length):
        crc ^= int(data[i])

    return crc


def check_crc(data):

    # Save received crc
    received_crc = int(data[len(data) - 1])

    # We don't consider the crc at the end of the frame
    length_frame = len(data) - 1
    # Initialization crc
    local_crc = compute_crc(data, length_frame)

    return received_crc == local_crc
