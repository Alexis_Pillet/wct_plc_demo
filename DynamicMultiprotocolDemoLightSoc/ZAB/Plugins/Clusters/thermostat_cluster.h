/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the Thermostat Cluster.
 *
 *   It supports:
 *    - Multiple, application specified endpoints
 *    - Attributes required for WiserOne testing
 *
 *   WARNING: It does not (currently) support:
 *    - All mandatory attributes
 *    - Optional Attributes
 *    - Commands
 *    - Non-volatile backing of any data.
 *
 * USAGE:
 *   Usage of this plugin is very simple:
 *    1. Initialise it with SzlPlugin_ThermostatClusterInit(), specifying the endpoints and reead/write function pointers
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 * 002.002.038  16-Jan-17   MvdB   Original
 *****************************************************************************/
#ifndef _THERMOSTAT_CLUSTER_H_
#define _THERMOSTAT_CLUSTER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "zabTypes.h"
#include "szl.h"


/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Attributes Identifiers */
typedef enum
{
  ATTRID_THERMOSTAT_LOCAL_TEMP                          = 0x0000,
  ATTRID_THERMOSTAT_OCCUPIED_HEATING_SETPOINT           = 0x0012,
} THERMOSTAT_CLUSTER_ATTRIBUTES;


/* Attribute Data storage
 * An instance of this is may be created per endpoint. */
typedef struct
{
  /* ZigBee Standard Attributes */
  szl_int16 LocalTemperature;
  szl_int16 OccupiedHeatingSetpoint;
}SzlPlugin_Thermostat_Attributes_t;


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Plugin Initialisation
 * This function must be called from the application after the Library has been initialized.
 *
 * Parameters:
 *  Service: Instance of library
 *  numEndpoints: The length of endpointList
 *  endpointList: The endpoints to be initialised with this cluster
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_ThermostatClusterInit(zabService* Service,
                                             szl_uint8 numEndpoints,
                                             szl_uint8 * endpointList,
                                             SZL_CB_DataPointRead_t ReadCallback,
                                             SZL_CB_DataPointWrite_t WriteCallback);

/******************************************************************************
 * Plugin Destruction
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_ThermostatCluster_Destroy(zabService* Service);


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /*_THERMOSTAT_CLUSTER_H_*/
