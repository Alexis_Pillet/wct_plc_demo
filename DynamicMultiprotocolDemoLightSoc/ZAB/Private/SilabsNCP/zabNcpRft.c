/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.012  19-Jul-16   MvdB   Original
 * 002.002.048  06-Feb-17   MvdB   Call response handler for EFR32 RFT receive test start/stop
 *****************************************************************************/

#include "zabNcpPrivate.h"
#include "zabNcpService.h"
#include "zabNcpEzspMfglib.h"
#include "zabNcpEzspUtilities.h"

#include "zabNcpRft.h"

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      *****************************
 *                 *****           MACROS            *****
 *                      *****************************
 ******************************************************************************/

#define ZAB_VND_RFT(_srv)  (ZAB_SERVICE_VENDOR(_srv)->zabNcpRftInfo)

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise Radio Factory Test with a source ID and callbacks
 ******************************************************************************/
void zabNcpRft_Init(erStatus* Status, zabService* Service,
                    unsigned32 SourceID,
                    zabNcpRft_CB_StandardResponse StandardResponseCallback,
                    zabNcpRft_CB_PacketReceivedInd PacketReceivedIndCallback,
                    zabNcpRft_CB_GetCTuneResponse GetCTuneResponseCallback)
{
  ER_CHECK_STATUS(Status);

  ZAB_VND_RFT(Service).RequestType = zabNcpRft_RequestType_None;
  ZAB_VND_RFT(Service).SourceId = SourceID;
  ZAB_VND_RFT(Service).StandardResponseCallback = StandardResponseCallback;
  ZAB_VND_RFT(Service).PacketReceivedIndCallback = PacketReceivedIndCallback;
  ZAB_VND_RFT(Service).GetCTuneResponseCallback = GetCTuneResponseCallback;
}

/******************************************************************************
 * Start the Packet Receive Test
 ******************************************************************************/
void zabNcpRft_StartPacketReceiveTest(erStatus* Status, zabService* Service)
{
  ER_CHECK_STATUS(Status);

  ZAB_VND_RFT(Service).RequestType = zabNcpRft_RequestType_PacketTestRunning;

  zabNcpRft_GeneralResponseHandler(Status, Service, EMBER_SUCCESS);
}

/******************************************************************************
 * Stop the Packet Receive Test
 ******************************************************************************/
void zabNcpRft_StopPacketReceiveTest(erStatus* Status, zabService* Service)
{
  ER_CHECK_STATUS(Status);

  ZAB_VND_RFT(Service).RequestType = zabNcpRft_RequestType_None;

  zabNcpRft_GeneralResponseHandler(Status, Service, EMBER_SUCCESS);
}

/******************************************************************************
 * Send a Packet Received Response - For responses during receive test
 ******************************************************************************/
void zabNcpRft_SendPacketReceivedResponse(erStatus* Status, zabService* Service,
                                          unsigned32 FrameCounter,
                                          unsigned8 Rssi)
{
  unsigned8 index = 0;
  unsigned8 PingResponseFrame[32];
  PingResponseFrame[index++] = 0x01;  // MAC Header - Data Frame
  PingResponseFrame[index++] = 0x08;  // MAC Header - Short Dest Addresses
  PingResponseFrame[index++] = ZAB_VND_RFT(Service).MacSequenceNumber++;
  COPY_OUT_16_BITS(&PingResponseFrame[index], 0xFFFF ); index+=2;   // MAC Header - Dst PAN
  COPY_OUT_16_BITS(&PingResponseFrame[index], 0xFFFF ); index+=2;   // MAC Header - Dst Addr
  PingResponseFrame[index++] = 0x0C;  // NwkFrameControl - Data Frame, GP
  COPY_OUT_32_BITS(&PingResponseFrame[index], ZAB_VND_RFT(Service).SourceId ); index+=4;
  PingResponseFrame[index++] = 0xFF;  // GP Command ID
  PingResponseFrame[index++] = 0x02; // Test Command
  COPY_OUT_32_BITS(&PingResponseFrame[index], FrameCounter); index+=4;
  PingResponseFrame[index++] = Rssi;

  zabNcpEzspMfglib_SendPacket(Status, Service, index, PingResponseFrame);
}

/******************************************************************************
 * Get RAM value of C Tune
 ******************************************************************************/
void zabNcpRft_GetCTuneRequest(erStatus* Status, zabService* Service)
{
  ER_CHECK_STATUS(Status);

  zabNcpEzspMfglib_GetCtune(Status, Service);
}

/******************************************************************************
 * Set RAM value of C Tune
 ******************************************************************************/
void zabNcpRft_SetCTuneRequest(erStatus* Status, zabService* Service,
                               unsigned16 CTune)
{
  ER_CHECK_STATUS(Status);

  zabNcpEzspMfglib_SetCtune(Status, Service, CTune);
}

/******************************************************************************
 * Get Stored (Non-Volatile) value of C Tune
 ******************************************************************************/
void zabNcpRft_GetStoredCTuneRequest(erStatus* Status, zabService* Service)
{
  ER_CHECK_STATUS(Status);
  ZAB_VND_RFT(Service).RequestType = zabNcpRft_RequestType_GetCtuneMfgToken;
  zabNcpEzspUtilities_GetMfgToken(Status, Service, EZSP_MFG_CTUNE);
}

/******************************************************************************
 * Set Stored (Non-Volatile) value of C Tune
 ******************************************************************************/
void zabNcpRft_SetStoredCTuneRequest(erStatus* Status, zabService* Service,
                                     unsigned16 CTune)
{
  unsigned8 dataBuf[2];
  ER_CHECK_STATUS(Status);
  ZAB_VND_RFT(Service).RequestType = zabNcpRft_RequestType_SetCtuneMfgToken;

  COPY_OUT_16_BITS(dataBuf, CTune);

  zabNcpEzspUtilities_SetMfgToken(Status, Service, EZSP_MFG_CTUNE, 2, dataBuf);
}


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                 *****     INTERNAL USE ONLY!!!     *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Create Radio Factory Test service
 ******************************************************************************/
void zabNcpRft_Create(erStatus* Status, zabService* Service)
{
  ZAB_VND_RFT(Service).RequestType = zabNcpRft_RequestType_None;
  ZAB_VND_RFT(Service).SourceId = 0;
  ZAB_VND_RFT(Service).StandardResponseCallback = NULL;
  ZAB_VND_RFT(Service).PacketReceivedIndCallback = NULL;
  ZAB_VND_RFT(Service).GetCTuneResponseCallback = NULL;
}

/******************************************************************************
 * General Response Handler
 * Used for all RFT related commands that have a response with just an EmberStatus:
 *  - zabNcpEzspMfglib_Start
 *  - zabNcpEzspMfglib_End
 *  - zabNcpEzspMfglib_StartTone
 *  - zabNcpEzspMfglib_StopTone
 *  - zabNcpEzspMfglib_StartStream
 *  - zabNcpEzspMfglib_StopStream
 *  - zabNcpEzspMfglib_SetChannel
 *  - zabNcpEzspMfglib_SetPower
 *  - zabNcpEzspMfglib_SetCtune
 *  - zabNcpEzspUtilities_SetMfgToken - replace with rft func
 *  - zabZnpRftUtility_PingResponse
 ******************************************************************************/
void zabNcpRft_GeneralResponseHandler(erStatus* Status, zabService* Service, EmberStatus ResponseStatus)
{
  if (ZAB_VND_RFT(Service).StandardResponseCallback != NULL)
    {
      ZAB_VND_RFT(Service).StandardResponseCallback(Service, (unsigned8)ResponseStatus);
    }
}

/******************************************************************************
 * Get CTune Response Handler
 * Used for all RFT related commands that get a CTune value
 *  - zabNcpRft_GetCTuneRequest
 *  - zabNcpRft_GetStoredCTuneRequest
 ******************************************************************************/
void zabNcpRft_GetFreqTuneResponseHandler(erStatus* Status, zabService* Service, unsigned16 Ctune)
{
  if (ZAB_VND_RFT(Service).GetCTuneResponseCallback != NULL)
    {
      ZAB_VND_RFT(Service).GetCTuneResponseCallback(Service, Ctune);
    }
}


/******************************************************************************
 * Get Manufacturing Token Response Handler
 * Used for zabNcpRft_GetStoredCTuneRequest
 ******************************************************************************/
void zabNcpRft_GetMfgTokenResponseHandler(erStatus* Status, zabService* Service, unsigned8 TokenDataLength, unsigned8* TokenData)
{
  unsigned16 cTune;

  if ( (ZAB_VND_RFT(Service).RequestType == zabNcpRft_RequestType_GetCtuneMfgToken) &&
       (TokenDataLength >= 2) )
    {
      cTune = COPY_IN_16_BITS(TokenData);
      zabNcpRft_GetFreqTuneResponseHandler(Status, Service, cTune);
    }
  ZAB_VND_RFT(Service).RequestType = zabNcpRft_RequestType_None;
}


/******************************************************************************
 * Set Manufacturing Token Response Handler
 ******************************************************************************/
void zabNcpRft_SetMfgTokenResponseHandler(erStatus* Status, zabService* Service, EmberStatus ResponseStatus)
{
  if (ZAB_VND_RFT(Service).RequestType == zabNcpRft_RequestType_SetCtuneMfgToken)
    {
      zabNcpRft_GeneralResponseHandler(Status, Service, ResponseStatus);
    }
  ZAB_VND_RFT(Service).RequestType = zabNcpRft_RequestType_None;
}

/******************************************************************************
 * Get Manufacturing Token Response Handler
 * Used for zabNcpRft_GetStoredCTuneRequest
 ******************************************************************************/
void zabNcpRft_RxHandler(erStatus* Status, zabService* Service, unsigned8 Rssi, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned32 frameCounter;
  if ( (ZAB_VND_RFT(Service).RequestType == zabNcpRft_RequestType_PacketTestRunning) &&
       (ZAB_VND_RFT(Service).PacketReceivedIndCallback != NULL) &&
       (PayloadLength >= 20) &&
       (PayloadData[0] == 0x01) && // MAC Header - Data Frame
       (PayloadData[1] == 0x08) && // MAC Header - Short Dest Addresses
       (COPY_IN_16_BITS(&PayloadData[3]) == 0xFFFF) && // MAC Header - Dst PAN
       (COPY_IN_16_BITS(&PayloadData[5]) == 0xFFFF) && // MAC Header - Dst Addr
       (PayloadData[7] == 0x0C) && // NwkFrameControl - Data Frame, GP
       (COPY_IN_32_BITS(&PayloadData[8]) == ZAB_VND_RFT(Service).SourceId) &&
       (PayloadData[12] == 0xFF) && // GP Command ID
       (PayloadData[13] == 0x01) )
    {
      frameCounter = COPY_IN_32_BITS(&PayloadData[14]);
      ZAB_VND_RFT(Service).PacketReceivedIndCallback(Service, Rssi, frameCounter);
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/