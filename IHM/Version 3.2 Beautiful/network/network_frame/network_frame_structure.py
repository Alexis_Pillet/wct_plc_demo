# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to store the structure of network frames
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from enum import Enum


class NetworkFrameControl(Enum):
    CONTROL = 0x00
    DATA = 0x01
    ACK = 0xFF


class NetworkOptionMask(Enum):
    MASK_OPTION_ACK = 0x01
    MASK_OPTION_ADDRESS_DESTINATION = 0x02
    MASK_OPTION_ADDRESS_SOURCE = 0x04
    MASK_OPTION_FRAGMENTATION = 0x08
    MASK_RESERVED = 0xF0


class NetworkOptionAck(Enum):
    NO_ACK_RESPONSE = 0x00
    ACK_RESPONSE = 0x01


class NetworkOptionAddressDestination(Enum):
    ADDRESS_DESTINATION_NOT_PRESENT = 0x00
    ADDRESS_DESTINATION_PRESENT = 0x01


class NetworkOptionAddressSource(Enum):
    ADDRESS_SOURCE_NOT_PRESENT = 0x00
    ADDRESS_SOURCE_PRESENT = 0x01


class NetworkOptionFragmentation(Enum):
    NO_FRAGMENTATION = 0x00
    FRAGMENTATION = 0x01


class NetworkAckStatusDisplay:

    def __init__(self):

        self.status_string_display = {
            NetworkAckStatus.ACK_SUCCESS.value: 'ACK_SUCCESS',
            NetworkAckStatus.ACK_BAD_NETWORK_SEQUENCE_NUMBER.value: 'ACK_BAD_NETWORK_SEQUENCE_NUMBER',
            NetworkAckStatus.ACK_ALREADY_RX.value: 'ACK_ALREADY_RX',
            NetworkAckStatus.ACK_INVALID_COMMAND.value: 'ACK_INVALID_COMMAND',
            NetworkAckStatus.SEND_NO_ACK.value: 'SEND_NO_ACK'}


class NetworkAckStatus(Enum):
    ACK_SUCCESS = 0x00
    ACK_BAD_NETWORK_SEQUENCE_NUMBER = 0x01
    ACK_ALREADY_RX = 0x02
    ACK_INVALID_COMMAND = 0x03
    SEND_NO_ACK = 0x04
