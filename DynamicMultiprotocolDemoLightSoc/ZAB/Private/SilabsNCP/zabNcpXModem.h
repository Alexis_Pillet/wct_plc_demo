/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/
#ifndef __ZAB_NCP_XMODEM_H__
#define __ZAB_NCP_XMODEM_H__


#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      *****************************
 *                 *****     EXTERNAL VARIABLES      *****
 *                      *****************************
 ******************************************************************************/

/* Some flags for causing errors and confirming they are handled during development testing */
#ifdef ZAB_DEVELOPER_TEST_MODE
zab_bool zabNcpXModem_DropNextIncomingFrame;
zab_bool zabNcpXModem_DropNextOutgoingingFrame;
zab_bool zabNcpXModem_BreakNextOutgoingCrc;
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Reset the service
 ******************************************************************************/
void zabNcpXModem_Reset(erStatus* Status, zabService* Service);

/******************************************************************************
 * Send a carriage return, to get the prompt from the bootloader
 ******************************************************************************/
void zabNcpXModem_SendCarriageReturn(erStatus* Status, zabService* Service);

/******************************************************************************
 * Send a Run command to the bootloader
 ******************************************************************************/
void zabNcpXModem_Run(erStatus* Status, zabService* Service);

/******************************************************************************
 * Send a Get Info command to the bootloader
 ******************************************************************************/
void zabNcpXModem_GetInfo(erStatus* Status, zabService* Service);

/******************************************************************************
 * Send a n Initialise Upload command to the bootloader
 ******************************************************************************/
void zabNcpXModem_InitialiseUpload(erStatus* Status, zabService* Service);

/******************************************************************************
 * Send a 128 byte block of data to the bootloader
 ******************************************************************************/
void zabNcpXModem_SendBlock(erStatus* Status, zabService* Service, unsigned8* BlockData);

/******************************************************************************
 * Send an End Of Transfer command to the bootloader
 ******************************************************************************/
void zabNcpXModem_EndFileTransfer(erStatus* Status, zabService* Service);

/******************************************************************************
 * Process incoming messages from the bootloader and catch info/prompt
 ******************************************************************************/
void zabNcpXModem_ProcessMessageIn(erStatus* Status, zabService* Service, sapMsg* Message);

#ifdef __cplusplus
}
#endif

#endif // __ZAB_NCP_EZSP_H__
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/