/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the profile wide client commands for the ZAB implementation
 *   of the SZL interface
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev           Date     Author  Change Description
 *              23-Aug-13   MvdB   Adapted from SZL
 *              20-Mar-14   MvdB   Fix bug were disable default response bit was not respected
 * 00.00.06.00  12-Jun-14   MvdB   Add Wireless Test Bench functions
 *                                  ARTF69073: Support server side response to Read Reporting Configuration
 * 00.00.06.04  07-Oct-14   MvdB   artf104116: Add RSSI to received packet quality indication
 * 00.00.06_GP  15-Oct-14   MvdB   ARTF105853: Complete basic GP features for Smartlink IPZ
 * 00.00.06.06  17-Oct-14   NJU    artf105931: Disable default response on ZCL responses
 * 01.100.06.00 10-Feb-15   MvdB   Correctly set command id for ZclM_ClusterCmdCfmHandler()
 *                                 ARTF113134: SZL Callbacks not called if error parsing response
 * 01.100.07.00 13-Feb-15   MvdB   artf108759: Szl_DataPointReadNative (and similar) should include maxDataLength
 *                                 artf105440: Review functions like ZclM_ClusterCmdCfmHandler to ensure correct return types
 *                                 artf113641: Do not respond to commands with Invalid Mfg Code (such as Read Attributes)
 * 001.101.001  27-Feb-15   MvdB   ARTF113641: When sending default response to unknown ManufacturerCode, use that ManufacturerCode
 * 002.000.003  06-Mar-15   MvdB   ARTF115854: Validate endpoint on SZL Init and also on ZCL send
 *                                 Remove GpAck. Code is no longer used.
 * 002.000.004  01-Apr-15   MvdB   ARTF116490: ZclM_HandleMsg: For consistency, make RSSI handler use 0xF2 as endpoint when reporting for GPD
 *                                 ARTF116256: Ensure SZL uses szl_memset and szl_memcpy rather than memset and memcpy
 * 002.000.006  07-Apr-15   MvdB   ARTF116255: Review enabling/disabling of GP via SZL_CFG_ENABLE_GREEN_POWER, ZAB_CFG_ENABLE_GREEN_POWER
 * 002.001.000  20-Apr-15   MvdB   ARTF131071: Check for register cluster command when direction in server->client
 * 002.001.001  07-Jul-15   MvdB   ARTF133848: Avoid unaligned struct member warnings on Szl_ReadReportingSendingConfiguration
 *                                 ARTF130228: Add unique address mode for GpSrcId
 *                                 ARTF136585: Add transaction IDs to over the air commands/responses
 * 002.001.003  16-Jul-15   MvdB   ARTF132827: Include source address in cluster command handlers
 *                                 ARTF131073: Differentiate by direction when registering Cluster Command Handlers
 * 002.002.009  07-Oct-15   MvdB   ARTF134853: Add ZclM_ExtractAttributeReportItem to increase code re-use
 * 002.002.010  09-Oct-15   MvdB   ARTF150980: Support Service ID in memory allocation functions for WTB
 * 002.002.014  20-Oct-15   MvdB   ARTF151327: Handle invalid string (length = 0xFF) in SZL
 * 002.002.015  20-Oct-15   MvdB   ARTF132893: Remove API duplication zigbeeDataToNative  ==> ZbToNative
 * 002.002.021  21-Apr-16   MvdB   ARTF166904: ZAB Read Reporting Configuration Response fails if more than one report is read
 *                                 ARTF167807: Move GP read/write attributes response handling to szl_gp
 *                                 ARTF167808: Improve Attribute Read/Write/Configure Responses to always list all attributes requested
 * Silabs:
 * 000.000.009  12-Jul-16   MvdB   ARTF174575: Parametise SZL_CB_ClusterCmd_t *PayloadOutSize with max data length application may provide in ZclM_ClusterCmdIndHandler()
 * 002.002.032  09-Jan-17   MvdB   ARTF172065: Rationalise duplicated private functions into public SZLEXT_ZigBeeDataTypeIsAnalog()
 * 002.002.035  11-Jan-17   MvdB   ARTF170823: Make Read/Write Reporting configuration data (SZL_AttributeReportData_t) consistent with Read/Write data (SZL_AttributeData_t)
 *
 * 002.002.047  31-Jan-17   MvdB   Klockwork Review: Null check error response parameters before use
 *****************************************************************************/

#include "zabCorePrivate.h"

#include "data_converter.h"
#include "endianness.h"
#include "szl.h"
#include "szl_zab.h"

#include "szl_gp.h"
#include "zcl_manager_zab.h"


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS           *****
 *                      *****************************
 ******************************************************************************/
/* Attribute ID + Data Type */
#define M_REPORT_ITEM_HEADER_SIZE 3

/******************************************************************************
 *                      *****************************
 *                 *****         LOCAL TYPES         *****
 *                      *****************************
 ******************************************************************************/



/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

static ZCL_STATUS ZclM_ClusterCmdIndHandler(zabService* Service, zabMsgProDataInd* dp);







/**
 * Copy the common part from an indication into the response message
 */
static void ZclM_CopyCmd2Rsp(ZCL_PAYLOAD_t* i_zcl, ZCL_PAYLOAD_t* r_zcl, szl_uint8 cmd)
{
    ZCL_COMMON_PART_t* i_common = NULL;
    ZCL_COMMON_PART_t* r_common = NULL;

    // copy the frame control and change the direction
    r_zcl->frame_type.frame_control = i_zcl->frame_type.frame_control;
    r_zcl->frame_type.frame_control.direction = !r_zcl->frame_type.frame_control.direction; // change direction
    r_zcl->frame_type.frame_control.disable_default_rsp = szl_true; // disable default response on response

    // copy manufacture code if present
    if (r_zcl->frame_type.frame_control.manufacture_specific)
    {
        r_zcl->frame_type.manufacture_frame.manufacture_code = i_zcl->frame_type.manufacture_frame.manufacture_code;
    }

    // setup pointers to the ZCL messages
    i_common = ZCL_GET_COMMON_PART_PTR(i_zcl);
    r_common = ZCL_GET_COMMON_PART_PTR(r_zcl);

    // copy transaction id and set the proper command for the response
    r_common->tr_id = i_common->tr_id;
    r_common->cmd = cmd;
}


/**
 * Handler for the ZCL IND command:
 *              ZCL_CMD_ID_READ_ATTRIBUTES
 */
ZCL_STATUS ZclM_ReadAttributesInd(zabService* Service, zabMsgProDataInd* dp)
{
    ZCL_PAYLOAD_t* zclInd;
    ZCL_PAYLOAD_t* zclRsp;
    szl_uint8 zclRspBuffer[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
    SZL_Addresses_t destAddr;
    ZCL_COMMON_PART_t* i_common;
    ZCL_COMMON_PART_t* r_common;
    ZCL_READ_ATTRIBUTES_t* i_attrs;
    szl_uint8 max_payload_size;
    szl_uint8 payload_size;
    szl_uint8 i_payload;
    szl_uint8 total_len;
    szl_uint16 numAttributes;
    szl_uint16 i;

    zclInd = (ZCL_PAYLOAD_t*)dp->data;
    if ( (zclInd->frame_type.frame_control.manufacture_specific) &&
         (LE_TO_UINT16(zclInd->frame_type.manufacture_frame.manufacture_code) != ZB_SCHNEIDER_MANUFACTURE_ID) )
      {
        return ZCL_UNSUP_MANUF_GENERAL_COMMAND;
      }

    // copy default part from the indication to the response msg
    zclRsp = (ZCL_PAYLOAD_t*)zclRspBuffer;
    ZclM_CopyCmd2Rsp(zclInd, zclRsp, ZCL_CMD_ID_READ_ATTRIBUTES_RSP);

    // setup helper pointers
    i_common = ZCL_GET_COMMON_PART_PTR(zclInd);
    r_common = ZCL_GET_COMMON_PART_PTR(zclRsp);

    // set data for the attribute data
    i_attrs = (ZCL_READ_ATTRIBUTES_t*)i_common->payload;
    max_payload_size = ZCL_MSG_PAYLOAD_SIZE(SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH, zclInd->frame_type.frame_control.manufacture_specific);
    payload_size = 0;

    i_payload = ZCL_MSG_PAYLOAD_SIZE(dp->dataLength, zclInd->frame_type.frame_control.manufacture_specific);

    numAttributes = i_payload / sizeof(szl_uint16);
    for (i = 0; i < numAttributes; i ++)
      {
        szl_uint8 data[64];
        szl_uint8 len = sizeof(data);
        szl_uint8 data_type;
        SZL_STATUS_t status;
        ZCL_READ_ATTRIBUTE_STATUS_t* r_attrs = (ZCL_READ_ATTRIBUTE_STATUS_t*)(r_common->payload + payload_size);
        szl_uint8 space_needed;

        // read data and update response
        r_attrs->attribute_id = i_attrs->attributes[i].attribute_id;

        // temp copy to these variables, to avoid memory overwrite
        // maybe change DataPointRead to write directly to buffer and have a max length supplied
        status = Szl_DataPointReadZB(Service,
                                     dp->dstAddr.endpoint,
                                     zclInd->frame_type.frame_control.manufacture_specific,
                                     dp->cluster,
                                     r_attrs->attribute_id,
                                     &data_type,
                                     (void*)data,
                                     &len );

        // first check if we have room for the response
        space_needed = ZCL_READ_ATTRIBUTE_STATUS_SIZE(status, len);

        if (max_payload_size - payload_size < (space_needed ))
        {
            // rsp->snp.status = SZL_STATUS_BUFFER_FULL;
            break;
        }

        // we use status directly as it should match the ZCL_ status codes, otherwise add conversion here
        r_attrs->status = status;

        if ( status == SZL_STATUS_SUCCESS)
        {
            // is succeeded update the data type and copy the data
            r_attrs->data_type = data_type;
            szl_memcpy(r_attrs->data, data, len);
        }

        // point to next attribute
        payload_size += space_needed;
      }

    // Setup up addressing and send command
    destAddr.AddressMode = SZL_ADDRESS_MODE_NWK_ADDRESS;
    destAddr.Addresses.Nwk.Address = dp->srcAddr.address.shortAddress;
    destAddr.Addresses.Nwk.Endpoint = dp->srcAddr.endpoint;

    total_len = ZCL_PAYLOAD_SIZE(zclRsp->frame_type.frame_control.manufacture_specific, payload_size);
    if (ZclM_SendCommand(Service,
                         dp->dstAddr.endpoint,
                         &destAddr,
                         dp->cluster,
                         i_common->tr_id,
                         total_len,
                         zclRspBuffer) == SZL_RESULT_SUCCESS)
      {
        return ZCL_HAS_RESPONDED;
      }
    return ZCL_FAILURE;
}


/**
 * Handler for the ZCL IND command:
 *              ZCL_CMD_ID_WRITE_ATTRIBUTES
 */
ZCL_STATUS ZclM_WriteAttributesInd(zabService* Service, zabMsgProDataInd* dp)
{
    ZCL_PAYLOAD_t* zclInd;
    ZCL_PAYLOAD_t* zclRsp;
    szl_uint8 zclRspBuffer[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
    SZL_Addresses_t destAddr;
    SZL_STATUS_t writeStatus = SZL_STATUS_SUCCESS;
    ZCL_COMMON_PART_t* i_common;
    ZCL_COMMON_PART_t* r_common;
    szl_uint8* i_eod;
    szl_uint8* i_attrs_ptr;
    szl_uint8 r_max_payload_size;
    szl_uint8 r_payload;
    szl_uint8 total_len;

    zclInd = (ZCL_PAYLOAD_t*)dp->data;
    if ( (zclInd->frame_type.frame_control.manufacture_specific) &&
         (LE_TO_UINT16(zclInd->frame_type.manufacture_frame.manufacture_code) != ZB_SCHNEIDER_MANUFACTURE_ID) )
      {
        return ZCL_UNSUP_MANUF_GENERAL_COMMAND;
      }

    // copy default part from the indication to the response msg
    zclRsp = (ZCL_PAYLOAD_t*)zclRspBuffer;
    ZclM_CopyCmd2Rsp(zclInd, zclRsp, ZCL_CMD_ID_WRITE_ATTRIBUTES_RSP);

    // setup helper pointers
    i_common = ZCL_GET_COMMON_PART_PTR(zclInd);
    r_common = ZCL_GET_COMMON_PART_PTR(zclRsp);

    // determine the attributes payload size (end of data)
    i_eod = dp->data + dp->dataLength;
    // set data pointer for the first attribute
    i_attrs_ptr = i_common->payload;

    // setup the max data size for the response buffer
    r_max_payload_size = ZCL_MSG_PAYLOAD_SIZE(SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH, zclInd->frame_type.frame_control.manufacture_specific);
    r_payload = 0;

    while (i_eod > i_attrs_ptr)
      {
        // get pointer of attributes type
        ZCL_WRITE_ATTRIBUTE_t* i_attrs = (ZCL_WRITE_ATTRIBUTE_t*)i_attrs_ptr;

        // get the data size for the attribute to write
        szl_uint8 length = ZbDataSize(i_attrs->data_type, i_attrs->data);

        // write data and update response
        szl_uint8 w_status = Szl_DataPointWriteZB(Service,
                                                  dp->dstAddr.endpoint,
                                                  zclInd->frame_type.frame_control.manufacture_specific,
                                                  dp->cluster,
                                                  i_attrs->attribute_id,
                                                  i_attrs->data_type,
                                                  i_attrs->data,
                                                  length );

        // only add status and attribute id for the attributes that failed writing
        if (w_status != ZCL_SUCCESS )
        {
            // we should at least have room for the status
            if (r_max_payload_size - r_payload < ZCL_WRITE_ATTRIBUTE_STATUS_SIZE)
            {
                writeStatus = SZL_STATUS_BUFFER_FULL;
                break;
            }

            ZCL_WRITE_ATTRIBUTE_STATUS_t* r_attrs = (ZCL_WRITE_ATTRIBUTE_STATUS_t*)(r_common->payload + r_payload);
            r_attrs->status = w_status;
            r_attrs->attribute_id = i_attrs->attribute_id;
            r_payload += ZCL_WRITE_ATTRIBUTE_STATUS_SIZE;
        }

        // point to next attributes
        i_attrs_ptr += ZCL_WRITE_ATTRIBUTE_SIZE(length);
      }

    // if payload is zero it means that all attributes was successfully written
    // and we need to add one single status byte with success
    if (r_payload == 0 && writeStatus == SZL_STATUS_SUCCESS )
      {
        ZCL_WRITE_ATTRIBUTE_STATUS_t* r_attrs = (ZCL_WRITE_ATTRIBUTE_STATUS_t*)(r_common->payload);
        r_attrs->status = ZCL_SUCCESS;
        r_payload = 1;
      }


    // Setup up addressing and send command
    destAddr.AddressMode = SZL_ADDRESS_MODE_NWK_ADDRESS;
    destAddr.Addresses.Nwk.Address = dp->srcAddr.address.shortAddress;
    destAddr.Addresses.Nwk.Endpoint = dp->srcAddr.endpoint;

    total_len = ZCL_PAYLOAD_SIZE(zclRsp->frame_type.frame_control.manufacture_specific, r_payload);
    if (ZclM_SendCommand(Service,
                         dp->dstAddr.endpoint,
                         &destAddr,
                         dp->cluster,
                         i_common->tr_id,
                         total_len,
                         zclRspBuffer) == SZL_RESULT_SUCCESS)
      {
        return ZCL_HAS_RESPONDED;
      }
    return ZCL_FAILURE;
}



/**
 * Handler for the ZCL IND command:
 *              ZCL_CMD_ID_DISCOVER_ATTRIBUTES
 */
static ZCL_STATUS ZclM_DiscoverAttributesInd(zabService* Service, zabMsgProDataInd* dp)
{
  ZCL_PAYLOAD_t* zclInd;
  ZCL_PAYLOAD_t* zclRsp;
  szl_uint8 zclRspBuffer[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
  SZL_Addresses_t destAddr;
  ZCL_COMMON_PART_t* i_common;
  ZCL_COMMON_PART_t* r_common;
  szl_uint8 r_max_payload_size;
  ZCL_DISCOVER_ATTRIBUTES_RSP_t* r_payload;
  szl_uint8 total_len;
  szl_uint8 max_attributes;
  int i;
  ZCL_DISCOVER_ATTRIBUTES_t* i_discover;

  zclInd = (ZCL_PAYLOAD_t*)dp->data;
  if ( (zclInd->frame_type.frame_control.manufacture_specific) &&
       (LE_TO_UINT16(zclInd->frame_type.manufacture_frame.manufacture_code) != ZB_SCHNEIDER_MANUFACTURE_ID) )
    {
      return ZCL_UNSUP_MANUF_GENERAL_COMMAND;
    }

  // copy default part from the indication to the response msg
  zclRsp = (ZCL_PAYLOAD_t*)zclRspBuffer;
  ZclM_CopyCmd2Rsp(zclInd, zclRsp, ZCL_CMD_ID_DISCOVER_ATTRIBUTES_RSP);

  // setup helper pointers
  i_common = ZCL_GET_COMMON_PART_PTR(zclInd);
  r_common = ZCL_GET_COMMON_PART_PTR(zclRsp);

  // setup the max data size for the response buffer
  r_max_payload_size = ZCL_MSG_PAYLOAD_SIZE(SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH, zclInd->frame_type.frame_control.manufacture_specific);


  // get max attributes to fit in payload
  i_discover = (ZCL_DISCOVER_ATTRIBUTES_t*)&i_common->payload;
  max_attributes = MIN( ((r_max_payload_size - ZCL_DISCOVER_ATTRIBUTES_RSP_SIZE(0)) / sizeof(ZCL_DISCOVER_ATTRIBUTE_t)), i_discover->maximum_attribute_ids);

  // fill the payload from internal GetAttributes function
  r_payload = (ZCL_DISCOVER_ATTRIBUTES_RSP_t*)(&r_common->payload);
  if (Szl_GetAttributes(Service,
                        (ENUM_SZL_DP_DIRECTION_t)zclInd->frame_type.frame_control.direction,
                        dp->cluster,
                        dp->dstAddr.endpoint,
                        zclInd->frame_type.frame_control.manufacture_specific,
                        LE_TO_UINT16(i_discover->start_attribute_id),
                        max_attributes,
                        &r_payload->discovery_complete,
                        r_payload->attributes,
                        &max_attributes) == SZL_STATUS_SUCCESS)
    {
      // convert ID's from Native to LE
      for(i = 0; i < max_attributes; i++)
        {
          r_payload->attributes[i] = UINT16_TO_LE(r_payload->attributes[i]);
        }

      // Setup up addressing and send command
      destAddr.AddressMode = SZL_ADDRESS_MODE_NWK_ADDRESS;
      destAddr.Addresses.Nwk.Address = dp->srcAddr.address.shortAddress;
      destAddr.Addresses.Nwk.Endpoint = dp->srcAddr.endpoint;

      total_len = ZCL_PAYLOAD_SIZE(zclInd->frame_type.frame_control.manufacture_specific, ZCL_DISCOVER_ATTRIBUTES_RSP_SIZE(max_attributes));
      if (ZclM_SendCommand(Service,
                           dp->dstAddr.endpoint,
                           &destAddr,
                           dp->cluster,
                           i_common->tr_id,
                           total_len,
                           zclRspBuffer) == SZL_RESULT_SUCCESS)
        {
          return ZCL_HAS_RESPONDED;
        }
    }
  return ZCL_FAILURE;
}



/**
 * Handler for the ZCL IND command:
 *              ZCL_CMD_ID_READ_REPORTING_CONFIGURATION
 */
static ZCL_STATUS ZclM_ReadReportingConfigurationInd(zabService* Service, zabMsgProDataInd* dp)
{
  ZCL_PAYLOAD_t* zclInd;
  ZCL_PAYLOAD_t* zclRsp;
  szl_uint8 zclRspBuffer[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
  SZL_Addresses_t destAddr;
  ZCL_COMMON_PART_t* i_common;
  ZCL_COMMON_PART_t* r_common;
  szl_uint8 r_max_payload_size;
  szl_uint8 r_payload;
  szl_uint8 total_len;
  szl_uint8* i_report_ptr;
  szl_uint8* i_eod;

  zclInd = (ZCL_PAYLOAD_t*)dp->data;
  if ( (zclInd->frame_type.frame_control.manufacture_specific) &&
       (LE_TO_UINT16(zclInd->frame_type.manufacture_frame.manufacture_code) != ZB_SCHNEIDER_MANUFACTURE_ID) )
    {
      return ZCL_UNSUP_MANUF_GENERAL_COMMAND;
    }

  // copy default part from the indication to the response msg
  zclRsp = (ZCL_PAYLOAD_t*)zclRspBuffer;
  ZclM_CopyCmd2Rsp(zclInd, zclRsp, ZCL_CMD_ID_READ_REPORTING_CONFIGURATION_RSP);

  // setup helper pointers
  i_common = ZCL_GET_COMMON_PART_PTR(zclInd);
  r_common = ZCL_GET_COMMON_PART_PTR(zclRsp);

  // setup the max data size for the response buffer
  r_max_payload_size = ZCL_MSG_PAYLOAD_SIZE(SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH, zclInd->frame_type.frame_control.manufacture_specific);

  // determine the attributes payload size (end of data)
  i_eod = dp->data + dp->dataLength;

  // set data pointer for the first attribute
  i_report_ptr = i_common->payload;

  r_payload = 0;

  while (i_eod > i_report_ptr)
  {
      SZL_STATUS_t status = SZL_STATUS_SUCCESS;
      szl_uint8 size = 0;
      // get pointer of attributes type
      ZCL_READ_REPORTING_CFG_t* i_configure = (ZCL_READ_REPORTING_CFG_t*)i_report_ptr;
      ZCL_READ_REPORTING_CFG_RSP_t* r_configure = (ZCL_READ_REPORTING_CFG_RSP_t*)(r_common->payload + r_payload);

      if (i_configure->direction == ZCL_CONFIGURE_REPORTING_SENDING)
      {
          szl_uint8 zb_data_length = 0;
          NATIVE_DATA_t reportable_change;
          szl_uint8 reportable_change_length = 0;
          szl_uint16 minInt;
          szl_uint16 maxInt;

          // clear in case no value is returned
          szl_memset(reportable_change.data.raw, 0, sizeof(reportable_change.data.raw));

          // get the reporting configuration for this attribute
          status = Szl_ReadReportingSendingConfiguration(Service,
                                                         dp->dstAddr.endpoint,
                                                         zclInd->frame_type.frame_control.manufacture_specific,
                                                         dp->cluster,
                                                         LE_TO_UINT16(i_configure->attribute_id),
                                                         &r_configure->parameters.sending.data_type,
                                                         &minInt,
                                                         &maxInt,
                                                         &reportable_change_length,
                                                         &reportable_change);
          // We need to use local variables here to avoid warnings with losing the packed info when pointers are passed
          r_configure->parameters.sending.min_reporting_interval = minInt;
          r_configure->parameters.sending.max_reporting_interval = maxInt;

          // if data type is analog - we must report a the reportable change value
          if (SZLEXT_ZigBeeDataTypeIsAnalog(r_configure->parameters.sending.data_type))
          {
              zb_data_length = ZbDataSize(r_configure->parameters.sending.data_type, NULL);
          }

          size = ZCL_READ_REPORTING_CFG_RSP_SIZE(status, i_configure->direction, zb_data_length);

          // we should at least have room for the status
          if (r_max_payload_size - r_payload < size )
          {
              return ZCL_INSUFFICIENT_SPACE;
          }

          // if success - copy the reporting configuration data
          if (status == SZL_STATUS_SUCCESS)
          {
              // convert to LE
              r_configure->parameters.sending.min_reporting_interval = UINT16_TO_LE(r_configure->parameters.sending.min_reporting_interval);
              r_configure->parameters.sending.max_reporting_interval = UINT16_TO_LE(r_configure->parameters.sending.max_reporting_interval);

              // if analog data type - copy reportable change value
              if (zb_data_length)
              {
                  NativeToZb(&reportable_change, NativeDataSize(r_configure->parameters.sending.data_type, NULL), r_configure->parameters.sending.data_type, r_configure->parameters.sending.reportable_change);
              }
          }
      }
      else //if i_configure->direction == ZCL_CONFIGURE_REPORTING_RECEIVING)
      {
          // feature not supported
          status = SZL_STATUS_UNREPORTABLE_ATTRIBUTE;
          size = ZCL_READ_REPORTING_CFG_RSP_SIZE(status, i_configure->direction, 0);
          if (r_max_payload_size - r_payload < size )
          {
              return ZCL_INSUFFICIENT_SPACE;
          }
      }

      // copy the status common data
      r_configure->common.status = status;
      r_configure->common.direction = i_configure->direction;
      r_configure->common.attribute_id = i_configure->attribute_id;
      r_payload += size;

      // advance to next attribute
      i_report_ptr += ZCL_READ_REPORTING_CFG_SIZE;
  }


  // Setup up addressing and send command
  destAddr.AddressMode = SZL_ADDRESS_MODE_NWK_ADDRESS;
  destAddr.Addresses.Nwk.Address = dp->srcAddr.address.shortAddress;
  destAddr.Addresses.Nwk.Endpoint = dp->srcAddr.endpoint;

  total_len = ZCL_PAYLOAD_SIZE(zclInd->frame_type.frame_control.manufacture_specific, r_payload);
  if (ZclM_SendCommand(Service,
                       dp->dstAddr.endpoint,
                       &destAddr,
                       dp->cluster,
                       i_common->tr_id,
                       total_len,
                       zclRspBuffer) == SZL_RESULT_SUCCESS)
    {
      return ZCL_HAS_RESPONDED;
    }
  return ZCL_FAILURE;
}


/**
 * Handler for the ZCL CFM command:
 *              ZCL_CMD_ID_READ_ATTRIBUTES_RSP
 */
static ZCL_STATUS ZclM_ReadAttributesCfm(zabService* Service, zabMsgProDataInd* dp)
{
    syncCallback Callback;
    SZL_AttributeReadRespParams_t* errorResponseParams;
    ZCL_PAYLOAD_t* zcl;
    ZCL_COMMON_PART_t* common;
    szl_uint8* eod;
    szl_uint8* attrs_ptr;
    int i;

    /* Get pointers to ZCL data and the start of the common section */
    zcl = (ZCL_PAYLOAD_t*)dp->data;
    eod = (szl_uint8*)zcl + dp->dataLength;
    common = ZCL_GET_COMMON_PART_PTR(zcl);

    /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
    Callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspThenDestroy(Service,
                                                                            SZL_ZAB_SYNC_TRANS_TYPE_ZCL_READ_ATTRIBUTE_REQ,
                                                                            common->tr_id,
                                                                            (void**)&errorResponseParams);
    /* Check for null pointers or non matching parameters */
    if (Callback.SZL_CB_AttributeReadResp == NULL)
      {
        return ZCL_SUCCESS;
      }
    if (errorResponseParams == NULL)
      {
        Callback.SZL_CB_AttributeReadResp(Service, SZL_STATUS_FAILED, errorResponseParams, common->tr_id);
        return ZCL_SUCCESS;
      }
    if ( (errorResponseParams->ClusterID != dp->cluster) ||
         (errorResponseParams->DestinationEndpoint != dp->dstAddr.endpoint) ||
         (errorResponseParams->ManufacturerSpecific != zcl->frame_type.frame_control.manufacture_specific))
      {
        Callback.SZL_CB_AttributeReadResp(Service, SZL_STATUS_INVALID_FIELD, errorResponseParams, common->tr_id);
        szl_mem_free(errorResponseParams);
        return ZCL_SUCCESS;
      }


    attrs_ptr = common->payload;
    while (eod > attrs_ptr)
      {
        // get pointer of attributes type
        ZCL_READ_ATTRIBUTE_STATUS_t* attrs = (ZCL_READ_ATTRIBUTE_STATUS_t*)(attrs_ptr);
        szl_uint16 attrId;
        SZL_STATUS_t convertStatus;
        NATIVE_DATA_t src;
        void *pSrc = NULL;
        szl_uint8 src_len = 0;
        szl_uint16 src_len_16 = 0;

        attrId = LE_TO_UINT16( attrs->attribute_id );
        convertStatus = attrs->status;

        if (attrs->status == ZCL_SUCCESS)
          {

            // supported data types
            switch (attrs->data_type)
              {
                case SZL_ZB_DATATYPE_BOOLEAN:
                case SZL_ZB_DATATYPE_DATA8:
                case SZL_ZB_DATATYPE_UINT8:
                case SZL_ZB_DATATYPE_INT8:
                case SZL_ZB_DATATYPE_ENUM8:
                case SZL_ZB_DATATYPE_BITMAP8:
                case SZL_ZB_DATATYPE_DATA16:
                case SZL_ZB_DATATYPE_UINT16:
                case SZL_ZB_DATATYPE_INT16:
                case SZL_ZB_DATATYPE_ENUM16:
                case SZL_ZB_DATATYPE_BITMAP16:
                case SZL_ZB_DATATYPE_DATA24:
                case SZL_ZB_DATATYPE_UINT24:
                case SZL_ZB_DATATYPE_INT24:
                case SZL_ZB_DATATYPE_BITMAP24:
                case SZL_ZB_DATATYPE_DATA32:
                case SZL_ZB_DATATYPE_UINT32:
                case SZL_ZB_DATATYPE_INT32:
                case SZL_ZB_DATATYPE_BITMAP32:
                case SZL_ZB_DATATYPE_TOD:
                case SZL_ZB_DATATYPE_DATE:
                case SZL_ZB_DATATYPE_UTC:
                case SZL_ZB_DATATYPE_DATA40:
                case SZL_ZB_DATATYPE_UINT40:
                case SZL_ZB_DATATYPE_INT40:
                case SZL_ZB_DATATYPE_BITMAP40:
                case SZL_ZB_DATATYPE_DATA48:
                case SZL_ZB_DATATYPE_UINT48:
                case SZL_ZB_DATATYPE_INT48:
                case SZL_ZB_DATATYPE_BITMAP48:
                case SZL_ZB_DATATYPE_DATA56:
                case SZL_ZB_DATATYPE_UINT56:
                case SZL_ZB_DATATYPE_INT56:
                case SZL_ZB_DATATYPE_BITMAP56:
                case SZL_ZB_DATATYPE_DATA64:
                case SZL_ZB_DATATYPE_UINT64:
                case SZL_ZB_DATATYPE_INT64:
                case SZL_ZB_DATATYPE_BITMAP64:
                case SZL_ZB_DATATYPE_CLUSTER_ID:
                case SZL_ZB_DATATYPE_ATTR_ID:
                case SZL_ZB_DATATYPE_BAC_OID:
                case SZL_ZB_DATATYPE_IEEE_ADDR:
                case SZL_ZB_DATATYPE_SINGLE_PREC:
                case SZL_ZB_DATATYPE_DOUBLE_PREC:
                    // we converts the stream from LE into native type
                    // the src_len is the native length and the src is the native data ptr
                    if (!ZbToNative(attrs->data_type, attrs->data, ZbDataSize(attrs->data_type, attrs->data), &src, &src_len ) )
                    {
                        convertStatus = SZL_STATUS_INVALID_DATA_TYPE;
                    }
                    pSrc = &src;
                    break;

                case SZL_ZB_DATATYPE_CHAR_STR:
                case SZL_ZB_DATATYPE_OCTET_STR:
                    if (*(szl_uint8*)attrs->data == SZL_ZB_DATATYPE_CHAR_OCTET_STR_INVALID_VALUE)
                      {
                        src_len = 0;
                      }
                    else
                      {
                        src_len = *(szl_uint8*)attrs->data + 1; // the length field is in index[0], plus the null terminator
                      }
                    pSrc = (szl_uint8*)attrs->data + 1;// the real data is in index[1]
                    break;

                case SZL_ZB_DATATYPE_LONG_OCTET_STR:
                    src_len_16 = (LE_TO_UINT16( *(szl_uint16*)attrs->data )); // the length field is in index[0-2]
                    src_len = (szl_uint8)src_len_16;  // DODGY - TODO hande 16 bit length!
                    pSrc = (szl_uint8*)attrs->data + 2;// the real data is in index[2]
                    break;

                case SZL_ZB_DATATYPE_128_BIT_SEC_KEY:
                    src_len = 16;
                    pSrc = attrs->data;
                    break;

                default:
                    printError(Service, "SZL ZCLM ZAB: Error in ZclM_ReadAttributesCfm() - Unsupported attribute data type 0x%02X\n", attrs->data_type);
                    convertStatus = SZL_STATUS_UNSUPPORTED_ATTRIBUTE;
                    break;
              }
          }

        for (i = 0; i < errorResponseParams->NumberOfAttributes; i++)
          {
            if (errorResponseParams->Attributes[i].AttributeID == attrId)
              {
                errorResponseParams->Attributes[i].Status = convertStatus;
                if (errorResponseParams->Attributes[i].Status == ZCL_SUCCESS)
                  {
                    // Over alloc by 1 to ensure we have space to zero terminate strings
                    errorResponseParams->Attributes[i].Data = szl_mem_alloc(src_len+1,
                                                                            MALLOC_ID_SZL_READ_ATTR_RSP_NATIVE_DATA);
                    if ( (errorResponseParams->Attributes[i].Data != NULL) &&
                         (pSrc != NULL) )
                      {
                        errorResponseParams->Attributes[i].DataType = attrs->data_type;
                        errorResponseParams->Attributes[i].DataLength = src_len;
                        szl_memcpy(errorResponseParams->Attributes[i].Data, pSrc, src_len);
                        // Insert zero terminator. Only needed for strings, but doesn't hurt to do every time
                        ((szl_uint8*)errorResponseParams->Attributes[i].Data)[src_len] = 0;
                      }
                    else
                      {
                        errorResponseParams->Attributes[i].Status = ZCL_SOFTWARE_FAILURE;
                        errorResponseParams->Attributes[i].DataType = SZL_ZB_DATATYPE_NO_DATA;
                        errorResponseParams->Attributes[i].DataLength = 0;
                      }
                  }
              }
          }

        // advance to next attribute
        attrs_ptr += ZCL_READ_ATTRIBUTE_STATUS_SIZE(attrs->status, ZbDataSize(attrs->data_type, attrs->data));
      }

    // call callback function
    Callback.SZL_CB_AttributeReadResp(Service, SZL_STATUS_SUCCESS, errorResponseParams, common->tr_id);

    // cleanup
    for (i = 0; i < errorResponseParams->NumberOfAttributes; i ++)
      {
        if (errorResponseParams->Attributes[i].Data != NULL)
          {
            szl_mem_free(errorResponseParams->Attributes[i].Data);
          }
      }

    szl_mem_free(errorResponseParams);

    return ZCL_SUCCESS;
}


/**
 * Handler for the ZCL CFM command:
 *              ZCL_CMD_ID_WRITE_ATTRIBUTES_RSP
 */
static ZCL_STATUS ZclM_WriteAttributesCfm(zabService* Service, zabMsgProDataInd* dp)
{
    syncCallback Callback;
    SZL_AttributeWriteRespParams_t* errorResponseParams;
    ZCL_PAYLOAD_t* zcl;
    ZCL_COMMON_PART_t* common;
    szl_uint8* eod;
    szl_uint8 payload_size;
    szl_uint8 num_attributes;
    szl_uint8* attrs_ptr;
    int i;

    /* Get pointers to ZCL data and the start of the common section */
    zcl = (ZCL_PAYLOAD_t*)dp->data;
    eod = (szl_uint8*)zcl + dp->dataLength;
    common = ZCL_GET_COMMON_PART_PTR(zcl);


    /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
    Callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspThenDestroy(Service,
                                                                            SZL_ZAB_SYNC_TRANS_TYPE_ZCL_WRITE_ATTRIBUTE_REQ,
                                                                            common->tr_id,
                                                                            (void**)&errorResponseParams);
    /* Check for null pointers or non matching parameters */
    if (Callback.SZL_CB_AttributeWriteResp == NULL)
      {
        return ZCL_SUCCESS;
      }
    if (errorResponseParams == NULL)
      {
        Callback.SZL_CB_AttributeWriteResp(Service, SZL_STATUS_FAILED, errorResponseParams, common->tr_id);
        return ZCL_SUCCESS;
      }
    if ( (errorResponseParams->ClusterID != dp->cluster) ||
         (errorResponseParams->DestinationEndpoint != dp->dstAddr.endpoint) ||
         (errorResponseParams->ManufacturerSpecific != zcl->frame_type.frame_control.manufacture_specific))
      {
        Callback.SZL_CB_AttributeWriteResp(Service, SZL_STATUS_INVALID_FIELD, errorResponseParams, common->tr_id);
        szl_mem_free(errorResponseParams);
        return ZCL_SUCCESS;
      }

    // Get the number of attributes - if all succeeded this will be 0 as only status (SUCCESS) is returned
    payload_size = ZCL_MSG_PAYLOAD_SIZE(dp->dataLength, zcl->frame_type.frame_control.manufacture_specific);
    num_attributes = payload_size / ZCL_WRITE_ATTRIBUTE_STATUS_SIZE;

    attrs_ptr = common->payload;

    /* From ZCL:
     * "Note that write attribute status records are not included for successfully written attributes, in order to save bandwidth.
     * In the case of successful writing of all attributes, only a single write attribute status record shall be included in the command,
     * with the status field set to SUCCESS and the attribute identifier field omitted."
     * So, we will now:
     *  If we just have the single status field, set everything to it's value
     *  If we have entries default everything to success and then for any entry we set failure */
    SZL_STATUS_t globalStatus = SZL_STATUS_SUCCESS;
    if (payload_size == 1)
      {
        globalStatus = common->payload[0];
      }
    for (i = 0; i < errorResponseParams->NumberOfAttributes; i++)
      {
        errorResponseParams->Attributes[i].Status = globalStatus;
      }

    /* Now process any real records we have */
    if (num_attributes)
      {
        // point to first attribute status
        num_attributes = 0;
        while (eod > attrs_ptr)
        {
            // get pointer of attributes type
            ZCL_WRITE_ATTRIBUTE_STATUS_t* attrs = (ZCL_WRITE_ATTRIBUTE_STATUS_t*)(attrs_ptr);
            szl_uint16 attrId = LE_TO_UINT16( attrs->attribute_id );
            SZL_STATUS_t convertStatus = attrs->status;

            for (i = 0; i < errorResponseParams->NumberOfAttributes; i++)
              {
                if (errorResponseParams->Attributes[i].AttributeID == attrId)
                  {
                    errorResponseParams->Attributes[i].Status = convertStatus;
                    break;
                  }
              }

            attrs_ptr += ZCL_WRITE_ATTRIBUTE_STATUS_SIZE;
        }
      }

    // call callback function
    Callback.SZL_CB_AttributeWriteResp(Service, SZL_STATUS_SUCCESS, errorResponseParams, common->tr_id);

    // cleanup
    szl_mem_free(errorResponseParams);

    return ZCL_SUCCESS;
}




/**
 * Handler for the ZCL CFM command:
 *              ZCL_CMD_ID_CONFIGURE_REPORTING_RSP
 */
static ZCL_STATUS ZclM_ConfigureReportingCfm(zabService* Service, zabMsgProDataInd* dp)
{
    syncCallback Callback;
    SZL_AttributeReportCfgRespParams_t* errorResponseParams;
    ZCL_PAYLOAD_t* zcl;
    ZCL_COMMON_PART_t* common;
    szl_uint8* eod;
    szl_uint8 payload_size;
    szl_uint8 i;
    szl_uint16 num_attributes;

    /* Get pointers to ZCL data and the start of the common section */
    zcl = (ZCL_PAYLOAD_t*)dp->data;
    eod = (szl_uint8*)zcl + dp->dataLength;
    common = ZCL_GET_COMMON_PART_PTR(zcl);

    /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
    Callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspThenDestroy(Service,
                                                                            SZL_ZAB_SYNC_TRANS_TYPE_ZCL_CONFIGURE_REPORT_REQ,
                                                                            common->tr_id,
                                                                            (void**)&errorResponseParams);
    // if we do not have a callback, or errorResponseParams is invalid then skip the handling for this message
    if ( (Callback.SZL_CB_AttributeReportCfgResp == NULL) ||
         (errorResponseParams == NULL) )
      {
        return ZCL_SUCCESS;
      }

    // Get the number of attributes - if all succeeded this will be 0 as only status (SUCCESS) is returned
    payload_size = ZCL_MSG_PAYLOAD_SIZE(dp->dataLength, zcl->frame_type.frame_control.manufacture_specific);
    num_attributes = payload_size / ZCL_CONFIGURE_REPORTING_RSP_SIZE;

    /* From ZCL:
     * "Note that attribute status records are not included for successfully configured attributes, in order to save bandwidth.
     * In the case of successful configuration of all attributes, only a single attribute status record shall be included in the command,
     * with the status field set to SUCCESS and the direction and attribute identifier fields omitted."
     * So, we will now:
     *  If we just have the single status field, set everything to it's value
     *  If we have entries default everything to success and then for any entry we set failure */
    SZL_STATUS_t globalStatus = SZL_STATUS_SUCCESS;
    if (payload_size == 1)
      {
        globalStatus = common->payload[0];
      }
    for (i = 0; i < errorResponseParams->NumberOfAttributes; i++)
      {
        errorResponseParams->Attributes[i].Status = globalStatus;
      }

    /* Now process an entries we happen to have */
    if ( num_attributes )
    {
        // point to first attribute status
        szl_uint8* status_ptr = common->payload;
        while (eod > status_ptr)
        {
            ZCL_CONFIGURE_REPORTING_RSP_t* status = (ZCL_CONFIGURE_REPORTING_RSP_t*)status_ptr;
            szl_uint16 attrId = LE_TO_UINT16( status->attribute_id );
            SZL_STATUS_t convertStatus = status->status;

            for (i = 0; i < errorResponseParams->NumberOfAttributes; i++)
              {
                if (errorResponseParams->Attributes[i].AttributeID == attrId)
                  {
                    errorResponseParams->Attributes[i].Status = convertStatus;
                    break;
                  }
              }

            status_ptr += ZCL_CONFIGURE_REPORTING_RSP_SIZE;
        }
    }

    // call callback function
    Callback.SZL_CB_AttributeReportCfgResp(Service, SZL_STATUS_SUCCESS, errorResponseParams, common->tr_id);

    // cleanup
    szl_mem_free(errorResponseParams);

    return ZCL_SUCCESS;
}


/**
 * Handler for the ZCL CFM command:
 *              ZCL_CMD_ID_READ_REPORTING_CONFIGURATION_RSP
 */
static ZCL_STATUS ZclM_ReadReportingConfigurationCfm(zabService* Service, zabMsgProDataInd* dp)
{
    syncCallback Callback;
    SZL_AttributeReadReportCfgRespParams_t* errorResponseParams;
    ZCL_PAYLOAD_t* zcl;
    ZCL_COMMON_PART_t* common;
    szl_uint8* eod;
    szl_uint8* i_report_ptr;
    szl_uint8 i;

    /* Get pointers to ZCL data and the start of the common section */
    zcl = (ZCL_PAYLOAD_t*)dp->data;
    eod = (szl_uint8*)zcl + dp->dataLength;
    common = ZCL_GET_COMMON_PART_PTR(zcl);

    /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
    Callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspThenDestroy(Service,
                                                                            SZL_ZAB_SYNC_TRANS_TYPE_ZCL_READ_REPORT_CONFIG_REQ,
                                                                            common->tr_id,
                                                                            (void**)&errorResponseParams);

    // if we do not have a callback, or errorResponseParams is invalid then skip the handling for this message
    if ( (Callback.SZL_CB_AttributeReadReportCfgResp == NULL) ||
         (errorResponseParams == NULL) )
      {
        return ZCL_SUCCESS;
      }

    i_report_ptr = common->payload;
    while (eod > i_report_ptr)
    {
        szl_uint8 zb_data_length = 0;
        ZCL_READ_REPORTING_CFG_RSP_t* payload = (ZCL_READ_REPORTING_CFG_RSP_t*)i_report_ptr;

        // we are only interested in sending report configurations
        if (payload->common.direction == ZCL_CONFIGURE_REPORTING_SENDING)
          {

            szl_uint16 attrId = LE_TO_UINT16( payload->common.attribute_id );
            for (i = 0; i < errorResponseParams->NumberOfAttributes; i++)
              {
                if (attrId == errorResponseParams->Attributes[i].AttributeID)
                  {
                    errorResponseParams->Attributes[i].Status = payload->common.status;
                    if (payload->common.status == ZCL_SUCCESS)
                      {
                        // Load data out of command
                        errorResponseParams->Attributes[i].DataType = payload->parameters.sending.data_type;
                        errorResponseParams->Attributes[i].MinInterval = LE_TO_UINT16(payload->parameters.sending.min_reporting_interval);
                        errorResponseParams->Attributes[i].MaxInterval = LE_TO_UINT16(payload->parameters.sending.max_reporting_interval);
                        errorResponseParams->Attributes[i].LenRC = 0;
                        errorResponseParams->Attributes[i].ReportableChangeData = NULL;

                        /* If data type is analog then there is a reportable change to come */
                        if (SZLEXT_ZigBeeDataTypeIsAnalog(payload->parameters.sending.data_type))
                          {
                            NATIVE_DATA_t nativeFormatData;

                            zb_data_length = ZbDataSize(payload->parameters.sending.data_type, NULL);

                            if (ZbToNative(payload->parameters.sending.data_type,
                                           payload->parameters.sending.reportable_change,
                                           zb_data_length,
                                           &nativeFormatData,
                                           &errorResponseParams->Attributes[i].LenRC) == szl_true)
                              {
                                errorResponseParams->Attributes[i].ReportableChangeData = szl_mem_alloc(errorResponseParams->Attributes[i].LenRC,
                                                                                                        MALLOC_ID_SZL_READ_REP_CFM_NATIVE_DATA);
                                if (errorResponseParams->Attributes[i].ReportableChangeData != NULL)
                                  {
                                    szl_memcpy(errorResponseParams->Attributes[i].ReportableChangeData, &nativeFormatData, errorResponseParams->Attributes[i].LenRC);
                                  }
                                else
                                  {
                                    errorResponseParams->Attributes[i].Status = ZCL_SOFTWARE_FAILURE;
                                    errorResponseParams->Attributes[i].DataType = SZL_ZB_DATATYPE_NO_DATA;
                                    errorResponseParams->Attributes[i].LenRC = 0;
                                  }
                              }
                            else
                              {
                                errorResponseParams->Attributes[i].Status = SZL_STATUS_INVALID_DATA_TYPE;
                              }
                          }
                      }
                    break;
                  }
              }
          }

        i_report_ptr += ZCL_READ_REPORTING_CFG_RSP_SIZE(payload->common.status, payload->common.direction, zb_data_length);
    }

    // call callback function
    Callback.SZL_CB_AttributeReadReportCfgResp(Service, SZL_STATUS_SUCCESS, errorResponseParams, common->tr_id);


    // cleanup
    for (i = 0; i < errorResponseParams->NumberOfAttributes; i ++)
      {
        if (errorResponseParams->Attributes[i].ReportableChangeData != NULL)
          {
            szl_mem_free(errorResponseParams->Attributes[i].ReportableChangeData);
          }
      }

    // cleanup
    szl_mem_free(errorResponseParams);
    return ZCL_SUCCESS;
}


/**
 * Handler for the ZCL CFM command:
 *              ZCL_CMD_ID_DISCOVER_ATTRIBUTES_RSP
 */
static ZCL_STATUS ZclM_DiscoverAttributesCfm(erStatus* Status, zabService* Service, zabMsgProDataInd* dp)
{
  SZL_AttributeDiscoverRespParams_t* Params;
  syncCallback Callback;
  void* errorResponseParams;
  ZCL_PAYLOAD_t* zcl;
  szl_uint16 num_attributes;
  int i;
  ZCL_COMMON_PART_t* common;
  ZCL_DISCOVER_ATTRIBUTES_RSP_t* payload;
  szl_uint16 total_size;

  /* Get pointers to ZCL data and the start of the common section */
  zcl = (ZCL_PAYLOAD_t*)dp->data;
  common = ZCL_GET_COMMON_PART_PTR(zcl);

  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspThenDestroy(Service,
                                                                          SZL_ZAB_SYNC_TRANS_TYPE_ZCL_DISCOVER_ATTRIBUTE_REQ,
                                                                          common->tr_id,
                                                                          &errorResponseParams);

  /* if we do not have a callback, then skip the handling for this message */
  if (Callback.SZL_CB_AttributeReportCfgResp == NULL)
    {
      return ZCL_SUCCESS;
    }

  /* Calculate the size of the response, then malloc enough space for it */
  payload = (ZCL_DISCOVER_ATTRIBUTES_RSP_t*)common->payload;
  num_attributes = ZCL_MSG_PAYLOAD_SIZE(dp->dataLength, zcl_ManufacturerSpecificCmd(dp->data))/sizeof(ZCL_DISCOVER_ATTRIBUTE_t);
  total_size = SZL_AttributeDiscoverRespParams_t_SIZE( num_attributes );

  Params = (SZL_AttributeDiscoverRespParams_t*)szl_mem_alloc(total_size,
                                                             MALLOC_ID_SZL_DISCOVER_ATTR_RSP);
  if (Params == NULL)
    {
      Callback.SZL_CB_AttributeDiscoverResp(Service, SZL_STATUS_MEM_ERROR, (SZL_AttributeDiscoverRespParams_t*)errorResponseParams,common->tr_id);
      szl_mem_free(errorResponseParams);
      return ZCL_SOFTWARE_FAILURE;
    }

  // copy data to the response
  Params->ManufacturerSpecific = zcl->frame_type.frame_control.manufacture_specific;
  Params->DestinationEndpoint = dp->dstAddr.endpoint;
  if (ZclM_ConvertZabToSzlAddress(&dp->srcAddr, &Params->SourceAddress) != szl_true)
    {
      Callback.SZL_CB_AttributeDiscoverResp(Service, SZL_STATUS_UNSUPPORTED_MODE, (SZL_AttributeDiscoverRespParams_t*)errorResponseParams,common->tr_id);
      szl_mem_free(errorResponseParams);
      szl_mem_free(Params);
      return ZCL_FAILURE;
    }
  Params->ClusterID = dp->cluster;
  Params->ListComplete = payload->discovery_complete;
  Params->NumberOfAttributes = num_attributes;

  for(i = 0; i < num_attributes; i++)
  {
      Params->Attributes[i].AttributeID = LE_TO_UINT16( payload->attributes[i].attribute_id );
      Params->Attributes[i].DataType = payload->attributes[i].data_type;
  }

  Callback.SZL_CB_AttributeDiscoverResp(Service, SZL_STATUS_SUCCESS, Params,common->tr_id);

  szl_mem_free(errorResponseParams);
  szl_mem_free(Params);
  return ZCL_SUCCESS;
}


/**
 * Handler for the ZCL CFM command:
 *              ZCL_CMD_ID_DEFAULT_RSP
 */
static ZCL_STATUS ZclM_DefaultResponseCfm(erStatus* Status, zabService* Service, zabMsgProDataInd* dp)
{
  ZCL_PAYLOAD_t* zcl;
  ZCL_COMMON_PART_t* common;
  ZCL_DEFAULT_RESPONSE_t* dr;

  if (dp->dataLength > (sizeof(ZCL_STD_MSG_t)))
    {
      /* Get pointers to ZCL data and the start of the common section */
      zcl = (ZCL_PAYLOAD_t*)dp->data;

      if (dp->dataLength >= ZCL_PAYLOAD_SIZE(zcl->frame_type.frame_control.manufacture_specific, sizeof(ZCL_DEFAULT_RESPONSE_t)))
        {
          common = ZCL_GET_COMMON_PART_PTR(zcl);

          dr = (ZCL_DEFAULT_RESPONSE_t*)common->payload;

          SzlZab_DefaultResponseCallbackAndDestroy(Service, common->tr_id, dr->status);
        }
    }
  return ZCL_SUCCESS;
}


/*******************************************************************************
 * Process an incoming Read Attributes Response
 ******************************************************************************/
static ZCL_STATUS zcl_ProcessReportAttr(erStatus* Status, zabService* Service, zabMsgProDataInd* dp)
{
  SZL_AttributeReportNtfParams_t* ntf;
  szl_uint8* zcl;
  szl_uint8 zclIndex;
  szl_uint8 attrIndex;
  szl_uint8 zbDataLength;
  szl_uint8* attrs_ptr;
  szl_uint8 num_attributes;
  SZL_RESULT_t res;
  ZCL_STATUS zclSts = ZCL_SUCCESS;

  if (dp == NULL)
    {
      return ZCL_SOFTWARE_FAILURE;
    }
  zcl = dp->data;
  if ((zcl == NULL) || (dp->dataLength < SZL_SZL_M_SZL_HEADER_SIZE_STANDARD) )
    {
      return ZCL_SOFTWARE_FAILURE;
    }

  /* Get the number of attributes */
  attrs_ptr = (zcl_ManufacturerSpecificCmd(zcl) ? &zcl[5] : &zcl[3]);
  num_attributes = 0;
  while ( attrs_ptr < (zcl + dp->dataLength))
    {
      num_attributes++;
      attrs_ptr += 3 + ZbDataSize(attrs_ptr[2], &attrs_ptr[3]);
    }

  /* Malloc rsp */
  ntf = (SZL_AttributeReportNtfParams_t*)szl_mem_alloc(SZL_AttributeReportNtfParams_t_SIZE(num_attributes),
                                                       MALLOC_ID_SZL_REP_NTF);
  if (ntf == NULL)
    {
      return ZCL_SOFTWARE_FAILURE;
    }
  szl_memset(ntf, 0, SZL_AttributeReportNtfParams_t_SIZE(num_attributes));
  /* WARNING - MAKE SURE YOU FREE ntf IF YOU ADD ANY RETURNS AFTER THIS POINT!!! */


  /* Parse the ZCL header, leaving index pointing to the start of the data */
  if (zcl_ManufacturerSpecificCmd(zcl))
    {
      /* Exit if the manufacturer code is not ours */
      if (COPY_IN_16_BITS(&(dp->data[1])) != ZB_SCHNEIDER_MANUFACTURE_ID)
        {
          szl_mem_free(ntf);
          return ZCL_UNSUP_MANUF_GENERAL_COMMAND;
        }

      ntf->ManufacturerSpecific = szl_true;
      zclIndex = 3;
    }
  else
    {
      ntf->ManufacturerSpecific = szl_false;
      zclIndex = 1;
    }
  zclIndex++; // skip TID
  zclIndex++; // skip command number

  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */

  ntf->DestinationEndpoint = dp->dstAddr.endpoint;
  ntf->ClusterID = dp->cluster;
  if (ZclM_ConvertZabToSzlAddress(&dp->srcAddr, &ntf->SourceAddress) == szl_true)
    {

      /* Header is processed, so now start parsing attributes */
      ntf->NumberOfAttributes = 0;

      /* Add attributes until we run out of data or available slots */
      attrIndex = 0;
      while ( (dp->dataLength >= (zclIndex+3)) && (attrIndex < num_attributes) )
      {
          res = ZclM_ExtractAttributeReportItem(Service,
                                                &ntf->Attributes[attrIndex],
                                                &(dp->data[zclIndex]),
                                                dp->dataLength - zclIndex,
                                                &zbDataLength);
          if (res != SZL_RESULT_SUCCESS)
            {
              /* There was an error parsing the frame, so we will only indicate whatever we got so far */
              zclSts = ZCL_FAILURE;
              break;
            }

        zclIndex += zbDataLength;
        attrIndex++;
      }
      ntf->NumberOfAttributes = attrIndex;

      if (ntf->NumberOfAttributes > 0)
        {
          Szl_AttributeReportNtfCB(Service, ntf);
        }
      for (attrIndex = 0; attrIndex < ntf->NumberOfAttributes; attrIndex++)
      {
        szl_mem_free(ntf->Attributes[attrIndex].Data);
      }
    }
  else
    {
      printError(Service, "zcl_ProcessReportAttr: Report dropped as address conversion failed\n");
    }
  szl_mem_free(ntf);
  return zclSts;
}



/**
 * Handler for the ZCL CFM Cluster commands
 */
static ZCL_STATUS ZclM_ClusterCmdCfmHandler(zabService* Service, zabMsgProDataInd* dp)
{
    syncCallback Callback;
    void* errorResponseParams;
    ZCL_PAYLOAD_t* zcl;
    ZCL_COMMON_PART_t* common;
    szl_uint8 payload_size;
    szl_uint16 total_size;
    SZL_ClusterCmdRespParams_t* Params;

    /* Get pointers to ZCL data and the start of the common section */
    zcl = (ZCL_PAYLOAD_t*)dp->data;
    common = ZCL_GET_COMMON_PART_PTR(zcl);

    /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
    Callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspThenDestroy(Service,
                                                                            SZL_ZAB_SYNC_TRANS_TYPE_ZCL_CLUSTER_REQ,
                                                                            common->tr_id,
                                                                            &errorResponseParams);

    // If we do not have a callback, then try parsing it is an incoming notification
    if (Callback.SZL_CB_ClusterCmdResp == NULL)
      {
        return ZclM_ClusterCmdIndHandler(Service, dp);
      }

    // Get the number of attributes - if all succeeded this will be 0 as only status (SUCCESS) is returned
    payload_size = ZCL_MSG_PAYLOAD_SIZE(dp->dataLength, zcl->frame_type.frame_control.manufacture_specific);

    // Allocate space for response
    total_size = SZL_ClusterCmdRespParams_t_SIZE(payload_size);
    Params = (SZL_ClusterCmdRespParams_t*)szl_mem_alloc(total_size,
                                                        MALLOC_ID_SZL_CLUSTER_CMD_CFM);
    if (Params == NULL)
      {
        Callback.SZL_CB_ClusterCmdResp(Service, SZL_STATUS_MEM_ERROR, (SZL_ClusterCmdRespParams_t*)errorResponseParams, common->tr_id);
        szl_mem_free(errorResponseParams);
        return ZCL_SOFTWARE_FAILURE;
      }

    // copy data to the response
    Params->ManufacturerSpecific = zcl->frame_type.frame_control.manufacture_specific;
    Params->DestinationEndpoint = dp->dstAddr.endpoint;
    if (ZclM_ConvertZabToSzlAddress(&dp->srcAddr, &Params->SourceAddress) != szl_true)
      {
        Callback.SZL_CB_ClusterCmdResp(Service, SZL_STATUS_UNSUPPORTED_MODE, (SZL_ClusterCmdRespParams_t*)errorResponseParams, common->tr_id);
        szl_mem_free(errorResponseParams);
        szl_mem_free(Params);
        return ZCL_FAILURE;
      }

    Params->ClusterID = dp->cluster;

    Params->Command = common->cmd;
    Params->PayloadLength = payload_size;

    if (payload_size)
    {
        szl_memcpy(Params->Payload, common->payload, payload_size);
    }

    // call callback function
    Callback.SZL_CB_ClusterCmdResp(Service, SZL_STATUS_SUCCESS, Params, common->tr_id);

    // cleanup
    szl_mem_free(errorResponseParams);
    szl_mem_free(Params);

    return ZCL_SUCCESS;
}


/**
 * Handler for the ZCL IND Cluster commands
 */
static ZCL_STATUS ZclM_ClusterCmdIndHandler(zabService* Service, zabMsgProDataInd* dp)
{
  ZCL_STATUS status;
  szl_uint16 totalZclLength;
  szl_uint8 zclRspBuffer[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
  szl_uint8 zclRspBuffer_SzlPayloadLength;
  SZL_Addresses_t srcAddr;
  SZL_Addresses_t dstAddr;
  szl_uint8 zclIndex;
  ZCL_PAYLOAD_t* zclInd;
  ZCL_PAYLOAD_t* zclRsp;
  ZCL_COMMON_PART_t* i_common;
  ZCL_COMMON_PART_t* r_common;

  /* Copy default part from the indication to the response msg.
   * Disable default response in the response header, as any failures can be retried from the client */
  zclInd = (ZCL_PAYLOAD_t*)dp->data;
  if ( (zclInd->frame_type.frame_control.manufacture_specific) &&
       (LE_TO_UINT16(zclInd->frame_type.manufacture_frame.manufacture_code) != ZB_SCHNEIDER_MANUFACTURE_ID) )
    {
      return ZCL_UNSUP_MANUF_CLUSTER_COMMAND;
    }

  zclRsp = (ZCL_PAYLOAD_t*)zclRspBuffer;
  ZclM_CopyCmd2Rsp(zclInd, zclRsp, ZCL_CMD_ID_DEFAULT_RSP);
  zclRsp->frame_type.frame_control.disable_default_rsp = szl_true;

  // setup helper pointers
  i_common = ZCL_GET_COMMON_PART_PTR(zclInd);
  r_common = ZCL_GET_COMMON_PART_PTR(zclRsp);

  printInfo(Service, "SZL ZCL: Cluster Cmd received: DstEP 0x%02X, Cl 0x%04X, Cmd 0x%02X\n", dp->dstAddr.endpoint, dp->cluster, i_common->cmd);

  // Extract the source address
  srcAddr.AddressMode = SZL_ADDRESS_MODE_NWK_ADDRESS;
  srcAddr.Addresses.Nwk.Address = dp->srcAddr.address.shortAddress;
  srcAddr.Addresses.Nwk.Endpoint = dp->srcAddr.endpoint;

  /* Get the destination address mode (in SZL format) of the incoming command*/
  if (ZclM_ConvertZabToSzlAddress(&dp->dstAddr, &dstAddr) != szl_true)
    {
      return ZCL_FAILURE;
    }

  // Callback into app/plugins to allow them to handle the command and provide a response
  r_common->cmd = 0xFF;

  // ARTF174575: Set the maximum paylaod length the handler may provide
  zclRspBuffer_SzlPayloadLength = SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH - ZCL_PAYLOAD_SIZE(zclInd->frame_type.frame_control.manufacture_specific, 0);

  status = (ZCL_STATUS) Szl_ClusterCommandHandler(Service,
                                                  dp->cluster,
                                                  i_common->cmd,
                                                  dp->dstAddr.endpoint,
                                                  zclInd->frame_type.frame_control.manufacture_specific,
                                                  i_common->payload,
                                                  ZCL_MSG_PAYLOAD_SIZE(dp->dataLength, zclInd->frame_type.frame_control.manufacture_specific),
                                                  &r_common->cmd,
                                                  r_common->payload,
                                                  &zclRspBuffer_SzlPayloadLength,
                                                  zclInd->frame_type.frame_control.direction,
                                                  i_common->tr_id,
                                                  srcAddr,
                                                  dstAddr.AddressMode);

  /* If successful and there is a response, send it */
  if ((status == ZCL_SUCCESS) && (zclRspBuffer_SzlPayloadLength))
    {

      /* Calculate total length and confirm it is reasonable */
      totalZclLength = ZCL_FRAME_SIZE(zclInd->frame_type.frame_control.manufacture_specific, zclRspBuffer_SzlPayloadLength);
      if (totalZclLength > SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH)
        {
          printError(Service, "SZL ZCL WARNING: Cluster Command Response Request: Data length 0x%02X exceeds max of 0x%02X\n", totalZclLength, SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH);
          //return SZL_RESULT_DATA_TOO_BIG; // SESA274466
          return ZCL_INSUFFICIENT_SPACE;
        }

      // Setup return address
      dstAddr.AddressMode = SZL_ADDRESS_MODE_NWK_ADDRESS;
      dstAddr.Addresses.Nwk.Address = dp->srcAddr.address.shortAddress;
      dstAddr.Addresses.Nwk.Endpoint = dp->srcAddr.endpoint;

      printInfo(Service, "SZL ZCL: Cluster Command Response Request: SrcEp = 0x%02X, DstAddr = 0x%04X, DstEp = 0x%02X, Cl = 0x%04X, Cmd = 0x%02X, TID = 0x%02X, DataLength = 0x%02X, Data =",
                dp->dstAddr.endpoint,
                dp->srcAddr.address.shortAddress,
                dp->srcAddr.endpoint,
                dp->cluster,
                r_common->cmd,
                r_common->tr_id,
                zclRspBuffer_SzlPayloadLength);
      for (zclIndex = 0; zclIndex < zclRspBuffer_SzlPayloadLength; zclIndex++)
        {
          printInfo(Service, " %02X", r_common->payload[zclIndex]);
        }
      printInfo(Service, "\n");

      /* Send command, with no response tracking. Client will retry if there are failures */
      ZclM_SendCommand(Service,
                       dp->dstAddr.endpoint,
                       &dstAddr,
                       dp->cluster,
                       r_common->tr_id,
                       totalZclLength,
                       zclRspBuffer);

      status = ZCL_HAS_RESPONDED;
    }

  return status;
}

/******************************************************************************
 *                      ******************************
 *             ***** PRIVATE EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Convert an SZL Address into a ZAB Address.
 * Return szl_true if successful, otherwise szl_false
 ******************************************************************************/
static szl_bool convertSzlToZabAddress(SZL_Addresses_t* szlAddress, zabProAddress* zabAddress)
{
  szl_bool result = szl_false;

  if ( (szlAddress == NULL) || (zabAddress == NULL) )
    {
      return szl_false;
    }

  switch(szlAddress->AddressMode)
    {
      case SZL_ADDRESS_MODE_VIA_BIND:
        zabAddress->addressMode = ZAB_ADDRESS_MODE_VIA_BIND;
        result = szl_true;
        break;

      case SZL_ADDRESS_MODE_GROUP:
        zabAddress->addressMode = ZAB_ADDRESS_MODE_GROUP;
        zabAddress->address.shortAddress = szlAddress->Addresses.Group.Address;
        result = szl_true;
        break;

      case SZL_ADDRESS_MODE_NWK_ADDRESS:
        zabAddress->addressMode = ZAB_ADDRESS_MODE_NWK_ADDRESS;
        zabAddress->address.shortAddress = szlAddress->Addresses.Nwk.Address;
        zabAddress->endpoint = szlAddress->Addresses.Nwk.Endpoint;
        result = szl_true;
        break;

      case SZL_ADDRESS_MODE_IEEE_ADDRESS:
        zabAddress->addressMode = ZAB_ADDRESS_MODE_IEEE_ADDRESS;
        zabAddress->address.ieeeAddress = szlAddress->Addresses.IEEE.Address;
        zabAddress->endpoint = szlAddress->Addresses.IEEE.Endpoint;
        result = szl_true;
        break;

      case SZL_ADDRESS_MODE_BROADCAST:
        zabAddress->addressMode = ZAB_ADDRESS_MODE_BROADCAST;
        switch (szlAddress->Addresses.Broadcast.Mode)
          {
            case SZL_BROADCAST_ALL:
              zabAddress->address.shortAddress = ZAB_BROADCAST_ALL;
              result = szl_true;
              break;

            case SZL_BROADCAST_RX_ON_WHEN_IDLE:
              zabAddress->address.shortAddress = ZAB_BROADCAST_RX_ON_WHEN_IDLE;
              result = szl_true;
              break;

            case SZL_BROADCAST_ROUTERS_AND_COORDINATOR:
              zabAddress->address.shortAddress = ZAB_BROADCAST_ROUTERS_AND_COORDINATOR;
              result = szl_true;
              break;

            case SZL_BROADCAST_LOW_POWER_ROUTERS:
              zabAddress->address.shortAddress = ZAB_BROADCAST_LOW_POWER_ROUTERS;
              result = szl_true;
              break;

            default:
              result = szl_false;
              break;

          }
        zabAddress->endpoint = ZAB_BROADCAST_ENDPOINT;
        break;

      case SZL_ADDRESS_MODE_GP_SOURCE_ID:
        zabAddress->addressMode = ZAB_ADDRESS_MODE_GP_SOURCE_ID;
        zabAddress->address.sourceId = szlAddress->Addresses.GpSourceId.SourceId;
        result = szl_true;
        break;

      default:
        result = szl_false;
        break;
    }
  return result;
}


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Extract an attribute from a ZCL payload
 *  *zclPayload should be: Attribute Id, DataType, Data
 *
 * WARNING: If SZL_RESULT_SUCCESS, then this function has malloced space
 *          at Attribute->Data. Freeing this data is the responsibility of the caller!
 ******************************************************************************/
SZL_RESULT_t ZclM_ExtractAttributeReportItem(zabService* Service, SZL_AttributeData_t* Attribute, szl_uint8* zclPayload, szl_uint8 zclDataLengthMax, szl_uint8* zclDataLengthOut)
{
  szl_uint8* zclPayloadCopy = zclPayload;
  szl_uint8 nativeDataLength;
  szl_uint8 zbDataLength;

  /* Validate parameters */
  if ( (Attribute == NULL) || (zclPayload == NULL) || (zclDataLengthOut == NULL) )
  {
    return SZL_RESULT_INTERNAL_LIB_ERROR;
  }
  *zclDataLengthOut = 0;

  /* Must be at least 4 byte: AttributeId(2), DataType(1) Data(1+) */
  if (zclDataLengthMax >= (M_REPORT_ITEM_HEADER_SIZE+1))
    {
      /* Get fixed attribute info */
      Attribute->AttributeID = COPY_IN_16_BITS(zclPayload); zclPayload+=2;
      Attribute->Status = SZL_STATUS_SUCCESS;
      Attribute->DataType = *zclPayload++;

      /* Get data sizes */
      zbDataLength = ZbDataSize(Attribute->DataType, zclPayload);
      if (zclDataLengthMax >= (M_REPORT_ITEM_HEADER_SIZE + zbDataLength) )
        {
          nativeDataLength = NativeDataSize(Attribute->DataType, zclPayload);

          /* Allocate and populate native data */
          Attribute->Data = szl_mem_alloc(nativeDataLength,
                                          MALLOC_ID_SZL_REP_NTF_NATIVE_DATA);
          if (Attribute->Data != NULL)
            {
              szl_memset(Attribute->Data, 0, nativeDataLength);
              Attribute->DataLength = nativeDataLength;

              switch (Attribute->DataType)
                {
                  case SZL_ZB_DATATYPE_OCTET_STR:
                  case SZL_ZB_DATATYPE_CHAR_STR:
                    szl_memcpy(Attribute->Data, zclPayload+1, nativeDataLength);
                    *((szl_uint8*)(Attribute->Data) + (nativeDataLength-1)) = 0;

                    zclPayload += zbDataLength;
                    *zclDataLengthOut = (szl_uint8)(zclPayload-zclPayloadCopy);
                    return SZL_RESULT_SUCCESS;

                  default:
                    if (DataTypeIsPrimitive(Attribute->DataType))
                      {
                        /* To be safe, convert data on stack then safety check to be sure the data is not bigger than we allocated before copying in*/
                        NATIVE_DATA_t alignedNativeData;
                        szl_uint8 nativeLength = 0;
                        if ( ZbToNative( Attribute->DataType, (szl_uint8*)zclPayload, zbDataLength, &alignedNativeData, &nativeLength ) == szl_true)
                          {
                            if (nativeLength <= nativeDataLength)
                              {
                                szl_memcpy(Attribute->Data, alignedNativeData.data.raw, nativeLength);
                                Attribute->DataLength = nativeDataLength;

                                zclPayload += zbDataLength;
                                *zclDataLengthOut = (szl_uint8)(zclPayload-zclPayloadCopy);
                                return SZL_RESULT_SUCCESS;
                              }
                          }
                      }
                    break;
                }

              /* Failed, so cleanup */
              szl_mem_free(Attribute->Data);
              Attribute->Data = NULL;
            }
        }

      Attribute->Status = SZL_STATUS_MEM_ERROR;
      Attribute->DataType= SZL_ZB_DATATYPE_NO_DATA;
      Attribute->DataLength = 0;
    }
  return SZL_RESULT_FAILED;
}

/******************************************************************************
 * Convert a ZAB Address into an SZL Address.
 * Return szl_true if successful, otherwise szl_false
 ******************************************************************************/
szl_bool ZclM_ConvertZabToSzlAddress(zabProAddress* zabAddress, SZL_Addresses_t* szlAddress)
{
  szl_bool result = szl_false;

  if ( (szlAddress == NULL) || (zabAddress == NULL) )
    {
      return szl_false;
    }

  switch (zabAddress->addressMode)
    {
      case ZAB_ADDRESS_MODE_VIA_BIND:
        szlAddress->AddressMode = SZL_ADDRESS_MODE_VIA_BIND;
        result = szl_true;
        break;

      case ZAB_ADDRESS_MODE_GROUP:
        szlAddress->AddressMode = SZL_ADDRESS_MODE_GROUP;
        szlAddress->Addresses.Group.Address = zabAddress->address.shortAddress;
        result = szl_true;
        break;

      case ZAB_ADDRESS_MODE_NWK_ADDRESS:
        szlAddress->AddressMode = SZL_ADDRESS_MODE_NWK_ADDRESS;
        szlAddress->Addresses.Nwk.Address = zabAddress->address.shortAddress;
        szlAddress->Addresses.Nwk.Endpoint = zabAddress->endpoint;
        result = szl_true;
        break;

      case ZAB_ADDRESS_MODE_IEEE_ADDRESS:
        szlAddress->AddressMode = SZL_ADDRESS_MODE_IEEE_ADDRESS;
        szlAddress->Addresses.IEEE.Address = zabAddress->address.ieeeAddress;
        szlAddress->Addresses.IEEE.Endpoint = zabAddress->endpoint;
        result = szl_true;
        break;

      case ZAB_ADDRESS_MODE_GP_SOURCE_ID:
        szlAddress->AddressMode = SZL_ADDRESS_MODE_GP_SOURCE_ID;
        szlAddress->Addresses.GpSourceId.SourceId = zabAddress->address.sourceId;
        result = szl_true;
        break;

      case ZAB_ADDRESS_MODE_BROADCAST:
        szlAddress->AddressMode = SZL_ADDRESS_MODE_BROADCAST;
        result = szl_true;
        switch (zabAddress->address.shortAddress)
          {

            case ZAB_BROADCAST_ALL: szlAddress->Addresses.Broadcast.Mode = SZL_BROADCAST_ALL; break;
            case ZAB_BROADCAST_RX_ON_WHEN_IDLE: szlAddress->Addresses.Broadcast.Mode = SZL_BROADCAST_RX_ON_WHEN_IDLE; break;
            case ZAB_BROADCAST_ROUTERS_AND_COORDINATOR: szlAddress->Addresses.Broadcast.Mode = SZL_BROADCAST_ROUTERS_AND_COORDINATOR; break;
            case ZAB_BROADCAST_LOW_POWER_ROUTERS: szlAddress->Addresses.Broadcast.Mode = SZL_BROADCAST_LOW_POWER_ROUTERS; break;
            default: result = szl_false; break;
          }
        break;

      default:
        result = szl_false;
        break;
    }

  return result;
}


/*******************************************************************************
 * Register a handler for raw ZCL Responses and Events
 ******************************************************************************/
void ZclM_RegisterRawHandler(erStatus* Status,
                            zabService* Service,
                            void (*f)(erStatus*, zabService*, szl_bool*, zabMsgProDataInd*))
{
  ZAB_SRV(Service)->szlService.zcl_RawEventHandler = f;
}



/******************************************************************************
 * Send a raw ZCL command
 *
 * Parameters:
 *   service: ZAB service
 *   srcEndpoint: Local endpoint that command will be sent from
 *   dstAddress: Destination address of the command
 *   cluster: Cluster off the command
 *   cmdLen: Length of cmd field
 *   cmd: Command data
 ******************************************************************************/
SZL_RESULT_t ZclM_SendCommand(zabService* Service,
                              szl_uint8 srcEndpoint,
                              SZL_Addresses_t*  dstAddress,
                              szl_uint16 cluster,
                              szl_uint8 transactionId,
                              szl_uint16 cmdLen, szl_uint8 *cmd)
{
  sapMsg* ApsReqMsg;
  sapMsgAps * apsMsg;
  SZL_RESULT_t result;
  int i;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);

  result = Szl_IsRequestValid(Service,  SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED );
  if (result == SZL_RESULT_SUCCESS)
    {
      if ( Szl_EndpointRegistered(Service, srcEndpoint) == szl_true )
        {
          result = SZL_RESULT_FAILED;

          ApsReqMsg = sapMsgAllocateData(&localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, sapMsgAps_SIZE() + cmdLen);
          if (erStatusIsError(&localStatus) || (ApsReqMsg == NULL) )
            {
              return SZL_RESULT_INTERNAL_LIB_ERROR;
            }

          sapMsgSetAppDataLength(&localStatus, ApsReqMsg, cmdLen);
          sapMsgSetAppType(&localStatus, ApsReqMsg, ZAB_MSG_APP_AF_DATA_OUT);
          sapMsgSetAppTransactionId(&localStatus, ApsReqMsg, transactionId);

          apsMsg = (sapMsgAps *)sapMsgGetAppData(ApsReqMsg);

          // ZAB Msg Settings
          apsMsg->dataReq.profile = SZL_CFG_PROFILE_ID;
          apsMsg->dataReq.cluster = cluster;
          apsMsg->dataReq.srcEndpoint = srcEndpoint;
          apsMsg->dataReq.version  = 0;
          apsMsg->dataReq.radius = 0x0F;
          apsMsg->dataReq.txOptions = 0x00;  // 0x10; //aps ack

          if (convertSzlToZabAddress(dstAddress, &apsMsg->dataReq.dstAddress) == szl_true)
            {
              // ZCL Settings
              apsMsg->dataReq.dataLength = cmdLen;
              osMemCopy(&localStatus, apsMsg->dataReq.data, cmd, cmdLen);

              printInfo(Service, "ZclM_SendCommand: TID = 0x%02X, Len = 0x%X, Data = ", transactionId, cmdLen);
              for (i =0; i < cmdLen; i++)
                {
                  printInfo(Service, "%02X ", cmd[i]);
                }
              printInfo(Service, "\n");

              // Deliver APS message to ZigBee Service by DataSAP
              sapMsgPutFree(&localStatus, ZAB_SRV(Service)->DataSAP, ApsReqMsg );

              if (erStatusIsOk(&localStatus))
                {
                  result = SZL_RESULT_SUCCESS;

    #ifdef ZAB_CFG_ENABLE_WIRELESS_TEST_BENCH
                  if (ZAB_SRV(Service)->szlService.DataRequestNotificationCallback != NULL)
                    {
                      ZAB_SRV(Service)->szlService.DataRequestNotificationCallback(Service, transactionId, &apsMsg->dataReq);
                    }
    #endif
                }
            }
          else
            {
              sapMsgFree(&localStatus, ApsReqMsg);
              result = SZL_RESULT_INVALID_ADDRESS_MODE;
            }
        }
      else
        {
          result = SZL_RESULT_INVALID_DATA;
        }
    }
  return result;
}


/*******************************************************************************
 * Data in handler for the ZCL
 ******************************************************************************/
void ZclM_HandleMsg(erStatus* Status, zabService* Service, sapMsg* Message)
{
  ZCL_STATUS zclStatus;
  szl_bool suppressProcessing;
  zabMsgProDataInd* dp;
  szl_uint8* zcl;
  SZL_DefaultRspReqParams_t defRspParams;
  szl_bool manSpec;
  szl_bool profileWide;
  szl_bool directionServerToClient;
  szl_bool defaultResponseDisabled;
  szl_uint8 commandId;
  szl_uint16 manId = ZB_SCHNEIDER_MANUFACTURE_ID;
  SZL_ReceivedSignalStrengthNtfParams_t rssiParams;

  ER_CHECK_NULL(Status, Message);

  dp = (zabMsgProDataInd*)sapMsgGetAppData(Message);


  /* Callback to indicate LQI of received message (if registered) */
  ZclM_ConvertZabToSzlAddress(&dp->srcAddr, &rssiParams.NwkSourceAddress);
  if (rssiParams.NwkSourceAddress.AddressMode == SZL_ADDRESS_MODE_GP_SOURCE_ID)
    {
      rssiParams.MacSourceAddress = rssiParams.NwkSourceAddress;
    }
  else /* It was a unicast, groupcast or broadcast, but we just take the MAC Nwk Source in any case */
    {
      rssiParams.MacSourceAddress.AddressMode = SZL_ADDRESS_MODE_NWK_ADDRESS;
      rssiParams.MacSourceAddress.Addresses.Nwk.Address = dp->macSourceAddress;
      rssiParams.MacSourceAddress.Addresses.Nwk.Endpoint = SZL_ADDRESS_EP_ALL;
    }
  rssiParams.LastHopLqi = dp->lqi;
  rssiParams.LastHopRssi = dp->rssi;
  Szl_ReceivedRssiNtfCB(Service, &rssiParams);

#ifdef SZL_CFG_ENABLE_GREEN_POWER
  /* If it came from a GPD hand this off to the GP manager */
  if (rssiParams.NwkSourceAddress.AddressMode == SZL_ADDRESS_MODE_GP_SOURCE_ID)
    {
      szlGp_HandleZclMsg(Status, Service, dp);
      return;
    }
#endif


#ifdef ZAB_CFG_ENABLE_WIRELESS_TEST_BENCH
  if ( ZAB_SRV(Service)->szlService.DataIndicationNotificationCallback != NULL)
    {
      ZAB_SRV(Service)->szlService.DataIndicationNotificationCallback(Service, dp);
    }
#endif

  /* If registered, hand the message to the application.
   * It may choose to handle the message itself, in which case it must set *suppressProcessing == true and ZAB will do no more with the message.
   * Otherwise, ZAB will continue further ZCL processing itself. */
  suppressProcessing = szl_false;
  if (ZAB_SRV(Service)->szlService.zcl_RawEventHandler != NULL)
    {
      ZAB_SRV(Service)->szlService.zcl_RawEventHandler(Status, Service, &suppressProcessing, dp);
    }

  /* If raw handler has not taken over, process further and generate responses etc. */
  if (suppressProcessing == szl_false)
    {
      zcl = dp->data;

      /* Setup default status for default response*/
      zclStatus = ZCL_FAILURE;
      manSpec = zcl_ManufacturerSpecificCmd(zcl);
      if (manSpec)
        {
          manId = zcl_GetManufacturerId(zcl);
        }
      profileWide = zcl_ProfileCmd(zcl);
      commandId = zcl_GetCmdId(zcl);
      directionServerToClient = zcl_ServerToClientCmd(zcl);
      defaultResponseDisabled = zcl_DefaultResponseDissabled(zcl);

      printInfo(Service, "SZL ZCLM: Command received: MS=%X, Cl=0x%04X, ProfWide=%X, Dir=%s, CmdId=0x%02X\n",
                 manSpec,
                 dp->cluster,
                 profileWide,
                 directionServerToClient ? "S->C" : "C->S",
                 commandId);

      /* Cut out commands that are MS but without our MSD code*/
      if ( (manSpec) && (manId != ZB_SCHNEIDER_MANUFACTURE_ID) )
        {
          if (profileWide)
            {
              zclStatus = ZCL_UNSUP_MANUF_GENERAL_COMMAND;
            }
          else
            {
              zclStatus = ZCL_UNSUP_MANUF_CLUSTER_COMMAND;
            }
        }
      else
        {
          /*
           * Server to Client Commands
           */
          if (directionServerToClient)
            {


              /*
               * Server to Client: Profile Wide Commands
               */
              if (profileWide)
                {

                  /* Command handlers must handle normal and manufacturer specific commands */
                  switch(commandId)
                    {
                      case ZCL_CMD_ID_READ_ATTRIBUTES_RSP:      zclStatus = ZclM_ReadAttributesCfm(Service, dp); break;
                      case ZCL_CMD_ID_WRITE_ATTRIBUTES_RSP:     zclStatus = ZclM_WriteAttributesCfm(Service, dp); break;
                      case ZCL_CMD_ID_CONFIGURE_REPORTING_RSP:  zclStatus = ZclM_ConfigureReportingCfm(Service, dp); break;
                      case ZCL_CMD_ID_READ_REPORTING_CONFIGURATION_RSP:  zclStatus = ZclM_ReadReportingConfigurationCfm(Service, dp); break;

                      case ZCL_CMD_ID_REPORT_ATTRIBUTES:        zclStatus = zcl_ProcessReportAttr(Status, Service, dp); break;
                      case ZCL_CMD_ID_DEFAULT_RSP:              zclStatus = ZclM_DefaultResponseCfm(Status, Service, dp); break;
                      case ZCL_CMD_ID_DISCOVER_ATTRIBUTES_RSP:  zclStatus = ZclM_DiscoverAttributesCfm(Status, Service, dp); break;
                      default:
                        printError(Service, "ZCL: Warning - Unknown profile wide S->C command received 0x%02X\n", zcl_GetCmdId(zcl));
                        if (manSpec)
                          {
                            zclStatus = ZCL_UNSUP_MANUF_GENERAL_COMMAND;
                          }
                        else
                          {
                            zclStatus = ZCL_UNSUP_GENERAL_COMMAND;
                          }
                        break;
                    }
                }

              /*
               * Server to Client: Cluster Specific Commands
               */
              else
                {
                  zclStatus = ZclM_ClusterCmdCfmHandler(Service, dp);
                }
            }

          /*
           * Client to Server Commands
           */
          else
            {

              /*
               * Client to Server: Profile Wide Commands
               */
              if (profileWide)
                {


                  /* Command handlers must handle normal and manufacturer specific commands */
                  switch(commandId)
                    {
                      case ZCL_CMD_ID_READ_ATTRIBUTES:              zclStatus = ZclM_ReadAttributesInd(Service, dp); break;
                      case ZCL_CMD_ID_WRITE_ATTRIBUTES:             zclStatus = ZclM_WriteAttributesInd(Service, dp); break;
                      case ZCL_CMD_ID_DISCOVER_ATTRIBUTES:          zclStatus = ZclM_DiscoverAttributesInd(Service, dp); break;
                      case ZCL_CMD_ID_READ_REPORTING_CONFIGURATION: zclStatus = ZclM_ReadReportingConfigurationInd(Service, dp); break;

                      /* For now we don't handle the default response:
                       *  - If response failed then client will retry
                       *  - If report failed then we just try again next time */
                      case ZCL_CMD_ID_DEFAULT_RSP: break;

                      default:
                        printError(Service, "ZCL: Warning - Unknown profile wide C->S command received 0x%02X\n", zcl_GetCmdId(zcl));
                        if (manSpec)
                          {
                            zclStatus = ZCL_UNSUP_MANUF_GENERAL_COMMAND;
                          }
                        else
                          {
                            zclStatus = ZCL_UNSUP_GENERAL_COMMAND;
                          }
                        break;
                    }
                }

              /*
               * Client to Server: Cluster Specific Commands
               */
              else
                {
                  zclStatus = ZclM_ClusterCmdIndHandler(Service, dp);
                }
            }
        }

      /*
       * Send default response if:
       *   - A device receives a unicast command that is not a default response command
       *   - No other command is sent in response to the received command
       *   - The Disable default response bit of its Frame control field is set to 0 or when an error results
       */
      if ( (dp->dstAddr.addressMode == ZAB_ADDRESS_MODE_NWK_ADDRESS) &&                 // Unicast
           (commandId != ZCL_CMD_ID_DEFAULT_RSP) &&                                     // Not a Default Response Command
           (zclStatus != ZCL_HAS_RESPONDED) &&                                          // No other response sent
           ( (defaultResponseDisabled == szl_false) || (zclStatus != ZCL_SUCCESS) ) )   // Disable default response bit is 0 or when an error results
        {
          defRspParams.ClusterID = dp->cluster;
          defRspParams.DestinationEndpoint = dp->srcAddr.endpoint;
          defRspParams.DestinationNwkAddress = dp->srcAddr.address.shortAddress;
          defRspParams.ManufacturerSpecific = manSpec;
          defRspParams.ManufacturerCode = manId;
          defRspParams.SourceEndpoint = dp->dstAddr.endpoint;
          defRspParams.Status = zclStatus;
          defRspParams.CommandId = commandId;
          defRspParams.TransactionId = zcl_GetCmdTid(zcl);

          if (directionServerToClient)
            {
              defRspParams.Direction = ZCL_FRAME_DIR_CLIENT_SERVER;
            }
          else
            {
              defRspParams.Direction = ZCL_FRAME_DIR_SERVER_CLIENT;
            }

          SZL_DefaultResponseReq(Service, &defRspParams);
        }
    }
}


/*******************************************************************************
 * Error handler for the ZCL
 * These may be from failures to send, data confirm etc.
 ******************************************************************************/
void ZclM_HandleError(erStatus* Status, zabService* Service, sapMsg* Message)
{
  zabError* errorCode;
  szl_uint8 tid;

  ER_CHECK_NULL(Status, Message);


  errorCode = (zabError*)sapMsgGetAppData(Message);
  if (*errorCode != ZAB_SUCCESS)
    {
      tid = sapMsgGetAppTransactionId(Message);

#ifdef ZAB_CFG_ENABLE_WIRELESS_TEST_BENCH
      if ( ZAB_SRV(Service)->szlService.DataConfirmNotificationCallback != NULL)
        {
          ZAB_SRV(Service)->szlService.DataConfirmNotificationCallback(Service, tid, *errorCode);
        }
#endif

      /* Notify the synchronous transaction table so it can notify failure of the command quickly */
      SzlZab_GenerateSynchronousTransactionFailureByTid(Service, tid);
    }
}
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
