/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the private interface for the ZAB serial service.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev     Date     Author  Change Description
 *  00.00.02.01  10-Dec-13   MvdB   Original
 *****************************************************************************/

#ifndef __ZAB_SERIAL_SERVICE_PRIVATE_H__
#define __ZAB_SERIAL_SERVICE_PRIVATE_H__

#include "zabSerialService.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                 *****         VENDOR PRIVATE       *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise the Serial SAP In/Out Service
 * Clears all handlers and empties all queues
 ******************************************************************************/
extern 
void zabSerialService_Init(erStatus* Status, zabService* Service,
                           zabSerialService_ActionOutCallback_t ActionOutCallback,
                           zabSerialService_SerialOutCallback_t SerialOutCallback);

/******************************************************************************
 * Reset the Serial SAP In/Out Service
 * Clears data but leaves handlers intact
 ******************************************************************************/
extern 
void zabSerialService_Reset(erStatus* Status, zabService* Service);

/******************************************************************************
 * Serial SAP Out Handler
 * Converts the following into nicely formatted function calls for app serial glue:
 *  - Actions: Open/Close
 *  - Application data frames
 ******************************************************************************/
extern 
void zabSerialService_OutHandler (erStatus* Status, sapHandle Sap, sapMsg* Message);

#ifdef __cplusplus
}
#endif

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif //__ZAB_SERIAL_SERVICE_PRIVATE_H__