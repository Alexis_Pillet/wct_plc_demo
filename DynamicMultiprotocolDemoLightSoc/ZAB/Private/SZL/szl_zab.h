/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the core of the ZAB specific foundations for the SZL implementation.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 * Vendor Rev    Date      Author  Change Description
 * 00.00.06.00  12-Jun-14   MvdB   Add Wireless Test Bench functions
 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113134: SZL Callbacks not called if error parsing response
 * 002.000.003  06-Mar-15   MvdB   Remove GpAck. Code is no longer used.
 * 002.001.001  15-Jul-15   MvdB   ARTF130228: Add unique address mode for GpSrcId
 * 002.002.021  21-Apr-16   MvdB   ARTF167807: Support Multi-Cluster Attribute Read/Write for GP
 *                                 ARTF167808: Improve Attribute Read/Write/Configure Responses to always list all attributes requested
 * 002.002.054  20-Jul-17   SMon   ARTF214292 Manage timeout for default response
 *****************************************************************************/
#ifndef _SZL_ZAB_H
#define _SZL_ZAB_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "szl_zab_types.h"  
#include "zcl_manager_zab.h"  
#include "sapMsgUtility.h"
  
#define LIB_DATA( S ) ZAB_SRV( S )->szlService  
  
  
#define SZL_LOCK( Service, id )   while (id) { ZAB_PROTECT_ENTER( (void*)(Service), (szl_uint8)(id) ); break; }    // enters a mutex if id > 0
#define SZL_UNLOCK( Service, id ) while (id) { ZAB_PROTECT_EXIT(  (void*)(Service), (szl_uint8)(id) ); break; }    // exits a mutex if id > 0

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise the low layers of the SZL Service
 * This should be done on ZAB create and befoer app calls SZL_Initialize()
 ******************************************************************************/
extern
void szl_zab_Init(erStatus* Status, zabService* Service);

/******************************************************************************
 * Reset the SZL Service
 ******************************************************************************/
extern 
void szl_zab_Reset(erStatus* Status, zabService* Service);

/******************************************************************************
 * Destroy the SZL Service
 ******************************************************************************/
extern 
void szl_zab_Destroy(erStatus* Status, zabService* Service);


/******************************************************************************
 * Get the Service Id from the Service Pointer
 ******************************************************************************/
extern 
szl_uint8 SzlZab_GetServiceId(zabService* Service);

/******************************************************************************
 * Get a Transaction ID
 * Returns SZL_ZAB_M_TRANSACTION_ID_INVALID if none available.
 ******************************************************************************/
extern
szl_uint8 szl_zab_GetTransactionId(zabService* Service);

/******************************************************************************
 * Convert a destination address of a request into the source address of a (fake) response
 ******************************************************************************/
extern 
void szl_zab_GetResponseAddrFromRequestDestination(SZL_Addresses_t* ReqDstAddr, SZL_Addresses_t* RspSrcAddr);

/******************************************************************************
 * Initialise the Synchronous Transaction Table
 ******************************************************************************/
extern 
void szl_zab_InitSynchronousTransactionTable(zabService* Service);

extern
SZL_RESULT_t SzlZab_ModifySynchronousTransaction(zabService* Service,
                                                szl_uint8 command,
                                                szl_uint8 timout,
                                                szl_uint8 nbRetry,
                                                szl_uint8 tid);
/******************************************************************************
 * Add an entry to the Synchronous Transaction Table
 ******************************************************************************/
extern 
SZL_RESULT_t SzlZab_AddSynchronousTransaction(zabService* Service,
                                              szlZabSyncTransType_t syncTransType,
                                              szl_uint8 tid,
                                              syncCallback* callback,
                                              void* errorResponseParams);
/******************************************************************************
 * Add an entry to the Synchronous Transaction Table
 ******************************************************************************/
extern 
SZL_RESULT_t SzlZab_AddSynchronousTransactionTimeout(zabService* Service,
                                              szlZabSyncTransType_t syncTransType,
                                              szl_uint8 tid,
                                              syncCallback* callback,
                                              void* errorResponseParams,
                                              szl_uint8 timeout);
/******************************************************************************
 * Add a Green Power entry to the Synchronous Transaction Table by IEEE
 ******************************************************************************/
extern 
SZL_RESULT_t SzlZab_AddGreenPowerSynchronousTransaction(zabService* Service,
                                                        szlZabSyncTransType_t syncTransType,
                                                        szl_uint8 tid,
                                                        szl_uint32 srcId,
                                                        syncCallback* callback,
                                                        void* errorResponseParams);

/******************************************************************************
 * Update the timeout of a Green Power  Synchronous Transaction
 ******************************************************************************/
extern 
SZL_RESULT_t SzlZab_UpdateGreenPowerSynchronousTransactiontimeout(zabService* Service,
                                                                  szlZabSyncTransType_t syncTransType,
                                                                  szl_uint32 srcId,
                                                                  szl_uint32 timeoutSeconds);

/******************************************************************************
 * Search the Synchronous Transaction Table for a matching entry by TID
 * Callback will be null if not found.
 ******************************************************************************/
extern
syncCallback SzlZab_GetSynchronousTransactionCallbackAndDestroy(zabService* Service,
                                                                szlZabSyncTransType_t syncTransType,
                                                                szl_uint8 tid);

/******************************************************************************
 * Search the Synchronous Transaction Table for a matching entry by TID
 * Callback will be null if not found.
 * Also return errorResponseParams. Caller is responsible for freeing it!
 ******************************************************************************/
extern
syncCallback SzlZab_GetSynchronousTransactionCallbackAndErrRspThenDestroy(zabService* Service,
                                                                          szlZabSyncTransType_t syncTransType,
                                                                          szl_uint8 tid,
                                                                          void** errorResponseParams);

/******************************************************************************
 * Search the Synchronous Transaction Table for a matching entry by IEEE
 * Callback will be null if not found.
 * Also return errorResponseParams. Caller is responsible for freeing it!
 ******************************************************************************/
extern
syncCallback SzlZab_GetSynchronousTransactionCallbackAndErrRspBySrcIdOptionalDestroy(zabService* Service,
                                                                                     szlZabSyncTransType_t syncTransType,
                                                                                     szl_uint32 gpSrcId,
                                                                                     void** errorResponseParams,
                                                                                     szl_uint8* tid,
                                                                                     szl_bool destroy);

/******************************************************************************
 * An error has occurred with a command (typically a bad data confirm) and the
 * sync transaction failure can be generated now.
 * This is optional, but speeds up timeouts when a failure is detected.
 ******************************************************************************/
extern
void SzlZab_GenerateSynchronousTransactionFailureByTid(zabService* Service,
                                                                     szl_uint8 tid);

/******************************************************************************
 * An error has occurred with a command (typically a GpTxQExpiry) and the
 * sync transaction failure can be generated now.
 * This is optional, but speeds up timeouts when a failure is detected.
 ******************************************************************************/
extern
void SzlZab_GenerateSynchronousTransactionFailureBySrcId(zabService* Service,
                                                         szl_uint32 GpSrcId,
                                                         SZL_STATUS_t szlStatus);

/******************************************************************************
 * Handle default responses:
 *  - Search the Synchronous Transaction Table for a matching ZCL entry by TID
 *  - If found, call the callback with the provided status
 ******************************************************************************/
extern
void SzlZab_DefaultResponseCallbackAndDestroy(zabService* Service,
                                              szl_uint8 tid,
                                              SZL_STATUS_t status);
/**
 * Register endpoint
 *
 * For each endpoint, find each cluster than has a registered attribute in the table. This is the in cluster list.
 * Register endpoints
 * 
 * WARNING: This function requires artf24534 to be fixed before it can work at any time.
 */
extern
SZL_RESULT_t SzlZab_EndpointRegister(zabService* Service);

/******************************************************************************
 * Data in handler for the SZL
 * This must be registered as for Data SAP In for the SZL to run
 * Queues data in for later processing
 ******************************************************************************/
extern 
void szl_zab_DataInHandler(erStatus* Status, sapHandle Sap, sapMsg* Message);

/******************************************************************************
 * SZL Work Function
 * This will call any registered SZL handlers
 ******************************************************************************/
extern 
szl_uint32 szl_zab_Work(erStatus* Status, zabService* Service, szl_uint32 timeNow);



#ifdef ZAB_CFG_ENABLE_WIRELESS_TEST_BENCH
/******************************************************************************
 * WIRELESS TEST BENCH: Register for Data Request Notification from SZL
 ******************************************************************************/
extern 
void szlZab_RegisterDataRequestNotificationHandler(erStatus* Status, zabService* Service, wtbUtility_DataReqNotification_t Callback);

/******************************************************************************
 * WIRELESS TEST BENCH: Register for Data Confirm Notifications from SZL
 ******************************************************************************/
extern 
void szlZab_RegisterDataErrorNotificationHandler(erStatus* Status, zabService* Service, wtbUtility_SzlDataErrorNotification_t Callback);

/******************************************************************************
 * WIRELESS TEST BENCH: Register for Data Indication Notification from SZL
 ******************************************************************************/
extern 
void szlZab_RegisterDataIndicationNotificationHandler(erStatus* Status, zabService* Service, wtbUtility_DataIndNotification_t Callback);
#endif

#ifdef __cplusplus
}
#endif
#endif /* _SZL_ZAB_H */

