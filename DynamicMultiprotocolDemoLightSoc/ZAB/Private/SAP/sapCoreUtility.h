/*
 Name:    Service Access Point Management Utility
 Author:  Cam Williams
 Company: Schneider Electric

 Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

 Description:
   This utility is for creating SAPs and accessing SAPs. A message is sent to a SAP in a direction
   and all the input call back functions that are registered with the direction are sent the message.

   A service creates the SAP and registers its own input call back function and a direction.

   Direction is a 4-bit mask value. Each bit represents a direction, so that a single message may go
   in more than one direction when sent to a SAP.

   Message structure and direction definition are not defined here and may differ from one service to
   another.
*/

#ifndef __SAP_CORE_UTILITY_H__
#define __SAP_CORE_UTILITY_H__

#include "zabCoreService.h"
#include "sapMsgUtility.h"

#ifdef __cplusplus
extern "C" {
#endif


// C A L L   B A C K   F O R   S A P   M E S S A G E S

// define a pointer to a function call back to receive a message from a SAP
typedef void (*sapCoreCallBack)( erStatus* Status, sapHandle Sap, sapMsg* Message );

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// S A P   M A N A G E M E N T

/* This function creates a Service Access Point. The only requirements is a context pointer to the service
   (even null is ok) and the number of access points to put messages to this SAP.
   This function is called by a service and the SAP made available to external code to access.
*/
sapHandle sapCoreCreate( erStatus* Status, sapService Service, unsigned32 AccessCount, unsigned8 MutexId );

/* Add access point for code that transfers to and from the SAP. The service needs to do this as well.
*/
void sapCoreRegisterCallBack( erStatus* Status, sapHandle Sap, sapMsgDirection Direction, sapCoreCallBack CallBack );

/* get service from a SAP
*/
sapService sapCoreGetService( sapHandle Sap );

/* Puts a message to a SAP in a direction defined by the service.
*/
void sapCorePut( erStatus* Status, sapHandle Sap, sapMsgDirection Direction, sapMsg* Message );

////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif
#endif


