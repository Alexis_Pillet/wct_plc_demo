# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to manage management of serialDriver & frame generation/management
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************

from serial_driver import SerialDriver, ask_for_port
from frame_generator import FrameGenerator
from frame_parser import *
from logger import logger
import serial
import sys
import time
from threading import Thread
from random import randint


class SerialProcess:

    def __init__(self):
        self.serial_driver = None
        self.parser = None
        self.engine_reference = None

    def start(self, reference):
        # Save engine reference which started the serial process
        self.engine_reference = reference
        # Start serial driver
        return self._start_serial_driver()

    def stop(self):
        logger().info('Clear program & close serialDriver...')
        # Stop serial driver
        self._stop_serial_driver()
        logger().info('Done!')

    def _stop_serial_driver(self):
        self.serial_driver.stop()
        self.serial_driver.join()
        self.serial_driver.close()

    def _start_serial_driver(self, default_baud_rate=115200, default_rts=None, default_dtr=None, port='COM9'):

        # Suppress non-error messages
        quiet = False
        # Show Python traceback on error
        develop = False

        while True:
            try:
                try:
                    port = ask_for_port()
                except KeyboardInterrupt:
                    sys.stderr.write('\n')
                    # parser.error('user aborted and port is not given')

                serial_instance = serial.serial_for_url(
                    'spy://' + port + '?file=serial_log.log',
                    default_baud_rate,
                    parity='N',
                    rtscts=False,
                    xonxoff=False,
                    do_not_open=True,
                    timeout=0.02)

                if not hasattr(serial_instance, 'cancel_read'):
                    # enable timeout for alive flag polling if cancel_read is not available
                    serial_instance.timeout = 1

                if default_dtr is not None:
                    if not quiet:
                        sys.stderr.write('--- forcing DTR {}\n'.format('active' if default_dtr else 'inactive'))
                    serial_instance.dtr = default_dtr
                if default_rts is not None:
                    if not quiet:
                        sys.stderr.write('--- forcing RTS {}\n'.format('active' if default_rts else 'inactive'))
                    serial_instance.rts = default_rts
                serial_instance.open()
            except serial.SerialException as e:
                sys.stderr.write('could not open port {!r}: {}\n'.format(port, e))
                if develop:
                    raise
                sys.exit(1)
            else:
                break

        self.serial_driver = SerialDriver(
            serial_instance,
            self.engine_reference)
        self.serial_driver.set_rx_encoding('hexlify')
        self.serial_driver.set_tx_encoding('hexlify')

        if not quiet:
            sys.stderr.write('--- Miniterm on {p.name}  {p.baudrate},{p.bytesize},{p.parity},{p.stopbits} ---\n'.format(
                p=self.serial_driver.serial))

        self.serial_driver.start()
        return self.serial_driver


class Engine:

    def __init__(self):
        self.serial_process = SerialProcess()
        self.debug = False
        self.current_state = self.UNINITIALIZED_WAITING_DEVICE_STATE
        self.serial_driver = None
        self.frame_generator = None
        self.device_number = -1
        self.test_thread = None
        self.thread_is_alive = True
        self.automate_function = {  # Association between state and function to execute
            self.UNINITIALIZED_WAITING_DEVICE_STATE: self.process_uninitialized_waiting_device_state,
            self.UNINITIALIZED_WAITING_ACK_CONFIGURATION_STATE:
                self.process_uninitialized_waiting_ack_configuration_state,
            self.UNINITIALIZED_WAITING_STATUS_NONE_STATE: self.process_uninitialized_waiting_status_none_state,
            self.NETWORKING_STATE: self.process_networking_state,
            self.NETWORKED_STATE: self.process_networked_state}

    # State of automate
    UNINITIALIZED_WAITING_DEVICE_STATE = 0
    UNINITIALIZED_WAITING_ACK_CONFIGURATION_STATE = 1
    UNINITIALIZED_WAITING_STATUS_NONE_STATE = 2
    NONE_STATE = 3
    NETWORKING_STATE = 4
    NETWORKED_STATE = 5

    def process_uninitialized_waiting_device_state(self, data):

        # If good frame
        if is_uninitialized_state_frame(data):

            # Update state
            self.current_state = self.UNINITIALIZED_WAITING_ACK_CONFIGURATION_STATE
            # Sleep for 10 ms => little delay in order that PLC Concentrator process the frame
            time.sleep(0.01)
            # Send configuration frame concentrator
            self.frame_generator.send_configuration_concentrator(self.frame_generator.DEFAULT_MAC_ADDRESS,
                                                                 self.frame_generator.DEFAULT_PAN_ID)
            if self.debug:
                logger().info('Status - PLC Network UNINITIALIZED')

    def process_uninitialized_waiting_ack_configuration_state(self, data):

        # if good frame
        if is_ack_success_frame(data):
            # Update state
            self.current_state = self.UNINITIALIZED_WAITING_STATUS_NONE_STATE

            if self.debug:
                logger().info('Status - PLC Network CONFIGURED')
        # Reset of the process realized by PLC concentrator
        elif is_uninitialized_state_frame(data):
            self.process_uninitialized_waiting_device_state(data)
        else:
            logger().error('Error: ERROR ACK Status {{data}}'.format(data[1]))

    def process_uninitialized_waiting_status_none_state(self, data):

        # If good frame
        if is_none_state_frame(data):

            # Update state
            self.current_state = self.NONE_STATE
            # No action to perform or waiting data for state None
            self.process_none_state()

            if self.debug:
                logger().info('Status - PLC Network NONE')
        # Reset of the process realized by PLC concentrator
        elif is_uninitialized_state_frame(data):
            self.process_uninitialized_waiting_device_state(data)

    def process_none_state(self):
        # Update state
        self.current_state = self.NETWORKING_STATE

    def process_networking_state(self, data):

        # If good frame
        if is_networking_state_frame(data):

            # Update state
            self.current_state = self.NETWORKED_STATE

            if self.debug:
                logger().info('Status - PLC Network CREATED')

        # Reset of the process realized by PLC concentrator
        elif is_uninitialized_state_frame(data):
            self.process_uninitialized_waiting_device_state(data)

    def process_networked_state(self, data):

        # If the PLC concentrator resets
        if is_uninitialized_state_frame(data):

            if self.test_thread is not None:
                if self.test_thread.is_alive():
                    self.thread_is_alive = False
                    self.test_thread.join()

            self.process_uninitialized_waiting_device_state(data)

        else:
            # Check if good frame
            # Good_frame / Device number / Mac address
            result = is_networked_state_frame(data)

            # If good frame
            if result[0]:

                if self.debug:
                    logger().info('Status - PLC Network RUNNING')
                    logger().debug('Device number - {}, Mac address - {}'.format(result[1], result[2]))
                    self.device_number = result[1]

                    # Start Test
                    self.thread_is_alive = True
                    self.test_thread = Thread(target=self.test_function_send_data_frame, name='test', args=())
                    self.test_thread.daemon = True
                    self.test_thread.start()
            else:

                # Check if good frame
                # Good_frame / Device number / Mac address
                result = is_data_frame(data)

                # If good frame
                if result[0]:

                    if self.debug:
                        logger().debug('Data received - {}'.format(result[1].hex()))

    def test_function_send_data_frame(self):

        while self.thread_is_alive:

            data = bytearray()
            current_length = randint(1, 25)

            # Add payload of the frame
            for index in range(current_length):
                data.append(index)

            self.frame_generator.send_data_frame(self.device_number, data)

            # Wait 1 second +/- 10ms
            time_random = randint(-1, 1)
            time.sleep(1.0 + (time_random * 0.01))

    # Activate/Deactivate debug
    def set_debug(self, value):
        self.debug = value

    def start(self):
        # Start serial process
        self.serial_driver = self.serial_process.start(self)
        # Create Frame manager
        self.frame_generator = FrameGenerator(self.serial_driver)

    def stop(self):
        # Stop serial process
        self.serial_process.stop()

    # Serial driver received new data from the PLC concentrator
    def new_data(self, data):

        self.automate_function[self.current_state](data)

        # Good_frame
        result = is_ack_error_frame(data)

        # If good frame
        if result[0]:

            if self.debug:
                logger().error('ACK - {}'.format(StatusDisplay().status_string_display[result[1]]))
