# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to model Power Tag Device
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from abc import abstractmethod
from data_model.zigbee_green_power.zigbee_green_power_device import ZigBeeGreenPowerDevice


class PowerTag(ZigBeeGreenPowerDevice):

    def __init__(self, address, id_wireless_bridge, application_frame_generator):
        # call super function
        super(PowerTag, self).__init__(address, id_wireless_bridge, application_frame_generator)
        # Initialization
        self._phase_sequence = None
        self._active_power_pha = None
        self._cumulated_energy = None
        self._partial_energy = None
        self._ac_alarms_mask = None
        self._alarms_mask = None

    def read_attribute(self, cluster_id, attribute_id):
        pass

    def write_attribute(self, cluster_id, attribute_id, value):
        pass

    def discovery(self):
        pass

    @abstractmethod
    def update_data(self, payload):
        pass

    def read_phase_sequence(self):
        pass

    def write_phase_sequence(self):
        pass

    def reset_partial_energy(self):
        pass

    @property
    def phase_sequence(self):
        return self._phase_sequence

    @phase_sequence.setter
    def phase_sequence(self, phase_sequence):
        self._phase_sequence = phase_sequence

    @property
    def active_power_pha(self):
        return self._active_power_pha

    @active_power_pha.setter
    def active_power_pha(self, active_power_pha):
        self._active_power_pha = active_power_pha

    @property
    def cumulated_energy(self):
        return self._cumulated_energy

    @cumulated_energy.setter
    def cumulated_energy(self, cumulated_energy):
        self._cumulated_energy = cumulated_energy

    @property
    def partial_energy(self):
        return self._partial_energy

    @partial_energy.setter
    def partial_energy(self, partial_energy):
        self._partial_energy = partial_energy

    @property
    def ac_alarms_mask(self):
        return self._ac_alarms_mask

    @ac_alarms_mask.setter
    def ac_alarms_mask(self, ac_alarms_mask):
        self._ac_alarms_mask = ac_alarms_mask

    @property
    def alarms_mask(self):
        return self._alarms_mask

    @alarms_mask.setter
    def alarms_mask(self, alarms_mask):
        self._alarms_mask = alarms_mask
