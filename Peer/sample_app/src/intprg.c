/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/******************************************************************************
* File Name     : intprg.c
* Device(s)     : RX631
* Tool-chain    : KPIT GNURX-ELF 15.01
* H/W platform  : G-CPX / EU-CPX2 / G-CPX3
* Description   : Sample software
******************************************************************************/


#include "vect.h"


// Exception(Supervisor Instruction)
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SuperVisorInst(void){/* brk(){  } */}

// Exception(Undefined Instruction)
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_UndefinedInst(void){/* brk(){  } */}

// Exception(Floating Point)
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_FloatingPoint(void){/* brk(){  } */}

// NMI
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) NonMaskableInterrupt(void){/* brk(){  } */}

// Dummy
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Dummy(void){/* brk(){  } */}

// BRK
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_BRK(void){ __builtin_rx_wait (); }

// BUSERR
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_BUSERR(void){ }

// FCU_FCUERR
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_FCU_FCUERR(void){ }

// FCU_FRDYI
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_FCU_FRDYI(void){ }

// ICU_SWINT
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_ICU_SWINT(void){  }

#if 0 //<! r_bsp_api.c
// CMTU0_CMT0
r_bsp_api.c void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Cmt0Cmi0(void){  }
#endif

#if 0 //<! r_bsp_api.c
// CMTU0_CMT1
r_bsp_api.c void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Cmt1Cmi1(void){  }
#endif

// CMTU1_CMT2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_CMTU1_CMT2(void){  }

// CMTU1_CMT3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_CMTU1_CMT3(void){  }

// USB0_D0FIFO0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_USB0_D0FIFO0(void){  }

// USB0_D1FIFO0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_USB0_D1FIFO0(void){  }

// USB0_USBI0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_USB0_USBI0(void){  }

// RSPI0_SPRI0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RSPI0_SPRI0(void){  }

// RSPI0_SPTI0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RSPI0_SPTI0(void){  }

// RSPI0_SPII0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RSPI0_SPII0(void){  }

// RSPI1_SPRI1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RSPI1_SPRI1(void){  }

// RSPI1_SPTI1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RSPI1_SPTI1(void){  }

// RSPI1_SPII1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RSPI1_SPII1(void){  }

// RSPI2_SPRI2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RSPI2_SPRI2(void){  }

// RSPI2_SPTI2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RSPI2_SPTI2(void){  }

// RSPI2_SPII2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RSPI2_SPII2(void){  }

// CAN0_RXF0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_CAN0_RXF0(void){  }

// CAN0_TXF0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_CAN0_TXF0(void){  }

// CAN0_RXM0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_CAN0_RXM0(void){  }

// CAN0_TXM0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_CAN0_TXM0(void){  }

// CAN1_RXF1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_CAN1_RXF1(void){  }

// CAN1_TXF1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_CAN1_TXF1(void){  }

// CAN1_RXM1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_CAN1_RXM1(void){  }

// CAN1_TXM1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_CAN1_TXM1(void){  }

// CAN2_RXF2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_CAN2_RXF2(void){  }

// CAN2_TXF2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_CAN2_TXF2(void){  }

// CAN2_RXM2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_CAN2_RXM2(void){  }

// CAN2_TXM2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_CAN2_TXM2(void){  }

// RTC_COUNTUP
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RTC_COUNTUP(void){  }

// IRQ0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ0(void){  }

// IRQ1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ1(void){  }

// IRQ2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ2(void){  }

// IRQ3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ3(void){  }

// IRQ4
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ4(void){  }

// IRQ5
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ5(void){  }

// IRQ6
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ6(void){  }

// IRQ7
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ7(void){  }

// IRQ8
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ8(void){  }

// IRQ9
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ9(void){  }

// IRQ10
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ10(void){  }

// IRQ11
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ11(void){  }

// IRQ12
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ12(void){  }

// IRQ13
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ13(void){  }

// IRQ14
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ14(void){  }

// IRQ15
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IRQ15(void){  }

// USB_USBR0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_USB_USBR0(void){  }

// RTC_ALARM
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RTC_ALARM(void){  }

// RTC_SLEEP
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RTC_SLEEP(void){  }

// AD0_ADI0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_AD0_ADI0(void){  }

// S12AD0_S12ADI0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_S12AD0_S12ADI0(void){  }

// ICU_GE0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_ICU_GE0(void){  }

// ICU_GE1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_ICU_GE1(void){  }

// ICU_GE2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_ICU_GE2(void){  }

// ICU_GE3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_ICU_GE3(void){  }

// ICU_GE4
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_ICU_GE4(void){  }

// ICU_GE5
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_ICU_GE5(void){  }

// ICU_GE6
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_ICU_GE6(void){  }

// vector 113 reserved

// ICU_GL0
//r_bsp_api.c void Excep_ICU_GL0(void){  }

// SCIX_SCIX0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCIX_SCIX0(void){  }

// SCIX_SCIX1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCIX_SCIX1(void){  }

// SCIX_SCIX2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCIX_SCIX2(void){  }

// SCIX_SCIX3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCIX_SCIX3(void){  }

// TPU0_TGI0A
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU0_TGI0A(void){  }

// TPU0_TGI0B
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU0_TGI0B(void){  }

// TPU0_TGI0C
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU0_TGI0C(void){  }

// TPU0_TGI0D
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU0_TGI0D(void){  }

// TPU1_TGI1A
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU1_TGI1A(void){  }

// TPU1_TGI1B
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU1_TGI1B(void){  }

// TPU2_TGI2A
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU2_TGI2A(void){  }

// TPU2_TGI2B
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU2_TGI2B(void){  }

// TPU3_TGI3A
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU3_TGI3A(void){  }

// TPU3_TGI3B
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU3_TGI3B(void){  }

// TPU3_TGI3C
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU3_TGI3C(void){  }

// TPU3_TGI3D
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU3_TGI3D(void){  }

// TPU4_TGI4A
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU4_TGI4A(void){  }

// TPU4_TGI4B
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU4_TGI4B(void){  }

// TPU5_TGI5A
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU5_TGI5A(void){  }

// TPU5_TGI5B
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU5_TGI5B(void){  }

// TPU6_TGI6A
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU6_TGI6A(void){  }

// TPU6_TGI6B
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU6_TGI6B(void){  }

// TPU6_TGI6C
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU6_TGI6C(void){  }

// TPU6_TGI6D
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU6_TGI6D(void){  }

///#############################################
// MTU0_TGIA0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU0_TGIA0(void){  }

// MTU0_TGIB0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU0_TGIB0(void){  }

// MTU0_TGIC0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU0_TGIC0(void){  }

// MTU0_TGID0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU0_TGID0(void){  }
///##############################################

// MTU0_TGIE0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU0_TGIE0(void){  }

// MTU0_TGIF0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU0_TGIF0(void){  }

// TPU7_TGI7A
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU7_TGI7A(void){  }

// TPU7_TGI7B
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU7_TGI7B(void){  }

///##############################################
// MTU1_TGIA1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU1_TGIA1(void){  }

// MTU1_TGIB1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU1_TGIB1(void){  }
///##############################################

// TPU8_TGI8A
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU8_TGI8A(void){  }

// TPU8_TGI8B
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU8_TGI8B(void){  }

///##############################################
// TPU8_TGIA2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU2_TGIA2(void){  }

// TPU8_TGIB2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU2_TGIB2(void){  }
///##############################################
// TPU8_TGI9A
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU9_TGI9A(void){  }

// TPU9_TGI9B
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU9_TGI9B(void){  }

// TPU9_TGI9C
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU9_TGI9C(void){  }

// TPU9_TGI9D
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU9_TGI9D(void){  }

///##############################################
// MTU3_TGIA3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU3_TGIA3(void){  }

// MTU3_TGIB3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU3_TGIB3(void){  }

// MTU3_TGIC3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU3_TGIC3(void){  }

// MTU3_TGID3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU3_TGID3(void){  }
///##############################################
// TPU10_TGI10A
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU10_TGI10A(void){  }

// TPU10_TGI10B
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU10_TGI10B(void){  }
///##############################################
// MTU4_TGIA4
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU4_TGIA4(void){  }

// MTU4_TGIB4
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU4_TGIB4(void){  }
///##############################################
// MTU4_TGIC4
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU4_TGIC4(void){  }

// MTU4_TGID4
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU4_TGID4(void){  }

// MTU4_TGIV4
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU4_TGIV4(void){  }

// TPU11_TGI11A
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU11_TGI11A(void){  }

// TPU11_TGI11B
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TPU11_TGI11B(void){  }

///##############################################
// MTU5_TGIU5
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU5_TGIU5(void){  }

// MTU5_TGIV5
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU5_TGIV5(void){  }

// MTU5_TGIW5
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_MTU5_TGIW5(void){  }
///##############################################
// POE_OEI1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_POE_OEI1(void){  }

// POE_OEI2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_POE_OEI2(void){  }

#if 0 //<! r_bsp_api.c
// TMR0_CMIA0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Tmr0Cmia0(void){  }
#endif

// TMR0_CMIB0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TMR0_CMIB0(void){  }

// TMR0_OVI0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TMR0_OVI0(void){  }

// TMR1_CMIA1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TMR1_CMIA1(void){  }

// TMR1_CMIB1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TMR1_CMIB1(void){  }

// TMR1_OVI1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TMR1_OVI1(void){  }

// TMR2_CMIA2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TMR2_CMIA2(void){  }

// TMR2_CMIB2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TMR2_CMIB2(void){  }

// TMR2_OVI2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TMR2_OVI2(void){  }

// TMR3_CMIA3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TMR3_CMIA3(void){  }

// TMR3_CMIB3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TMR3_CMIB3(void){  }

// TMR3_OVI3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_TMR3_OVI3(void){  }

// RIIC0_EEI0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC0_EEI0(void){  }

// RIIC0_RXI0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC0_RXI0(void){  }

// RIIC0_TXI0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC0_TXI0(void){  }

// RIIC0_TEI0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC0_TEI0(void){  }

// RIIC1_EEI1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC1_EEI1(void){  }

// RIIC1_RXI1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC1_RXI1(void){  }

// RIIC1_TXI1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC1_TXI1(void){  }

// RIIC1_TEI1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC1_TEI1(void){  }

// RIIC2_EEI2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC2_EEI2(void){  }

// RIIC2_RXI2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC2_RXI2(void){  }

// RIIC2_TXI2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC2_TXI2(void){  }

// RIIC2_TEI2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC2_TEI2(void){  }

// RIIC3_EEI3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC3_EEI3(void){  }

// RIIC3_RXI3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC3_RXI3(void){  }

// RIIC3_TXI3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC3_TXI3(void){  }

// RIIC3_TEI3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_RIIC3_TEI3(void){  }

// DMAC_DMAC0I
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_DMAC_DMAC0I(void){  }

// DMAC_DMAC1I
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_DMAC_DMAC1I(void){  }

// DMAC_DMAC2I
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_DMAC_DMAC2I(void){  }

// DMAC_DMAC3I
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_DMAC_DMAC3I(void){  }

#if 0 //<! r_bsp_api.c
// SCI0_RXI0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci0Rxi0(void){  }
#endif

#if 0 //<! r_bsp_api.c
// SCI0_TXI0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci0Txi0(void){  }
#endif

#if 0 //<! r_bsp_api.c
// SCI0_TEI0
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci0Tei0(void){  }
#endif

// SCI1_RXI1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci1Rxi1(void){  }

// SCI1_TXI1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci1Txi1(void){  }

// SCI1_TEI1
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci1Tei1(void){  }

// SCI2_RXI2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci2Rxi2(void){  }

// SCI2_TXI2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci2Txi2(void){  }

// SCI2_TEI2
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci2Tei2(void){  }

// SCI3_RXI3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI3_RXI3(void){  }

// SCI3_TXI3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI3_TXI3(void){  }

// SCI3_TEI3
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI3_TEI3(void){  }

// SCI4_RXI4
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI4_RXI4(void){  }

// SCI4_TXI4
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI4_TXI4(void){  }

// SCI4_TEI4
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI4_TEI4(void){  }

#if 0 //<! r_bsp_api.c
// SCI5_RXI5
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci5Rxi5(void){  }
#endif

#if 0 //<! r_bsp_api.c
// SCI5_TXI5
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci5Txi5(void){  }
#endif

#if 0 //<! r_bsp_api.c
// SCI5_TEI5
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci5Tei5(void){  }
#endif

// SCI6_RXI6
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI6_RXI6(void){  }

// SCI6_TXI6
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI6_TXI6(void){  }

// SCI6_TEI6
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI6_TEI6(void){  }

// SCI7_RXI7
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI7_RXI7(void){  }

// SCI7_TXI7
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI7_TXI7(void){  }

// SCI7_TEI7
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI7_TEI7(void){  }

// SCI8_RXI8
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI8_RXI8(void){  }

// SCI8_TXI8
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI8_TXI8(void){  }

// SCI8_TEI8
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI8_TEI8(void){  }

#if 0 //<! r_bsp_api.c
// SCI9_RXI9
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci9Rxi9(void){  }
#endif

#if 0 //<! r_bsp_api.c
// SCI9_TXI9
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci9Txi9(void){  }
#endif

#if 0 //<! r_bsp_api.c
// SCI9_TEI9
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci9Tei9(void){  }
#endif

// SCI10_RXI10
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI10_RXI10(void){  }

// SCI10_TXI10
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI10_TXI10(void){  }

// SCI10_TEI10
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI10_TEI10(void){  }

// SCI11_RXI11
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI11_RXI11(void){  }

// SCI11_TXI11
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI11_TXI11(void){  }

// SCI11_TEI11
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_SCI11_TEI11(void){  }
// SCI12_RXI12
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci12Rxi12(void){  }

// SCI12_TXI12
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci12Txi12(void){  }

// SCI12_TEI12
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci12Tei12(void){  }

// IEB_IEBINT
void  __attribute((section("PIntPRG")))  __attribute((interrupt)) Excep_IEB_IEBINT(void){  }
