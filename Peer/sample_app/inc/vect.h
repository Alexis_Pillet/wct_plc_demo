/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/******************************************************************************
* File Name     : vect.h
* Device(s)     : RX631
* Tool-chain    : KPIT GNURX-ELF 15.01
* H/W platform  : G-CPX / EU-CPX2 / G-CPX3
* Description   : Sample software
******************************************************************************/

#ifndef __VECT_H__
#define __VECT_H__
// Exception(Supervisor Instruction)

void  __attribute((interrupt)) Excep_SuperVisorInst(void);

// Exception(Undefined Instruction)

void  __attribute((interrupt)) Excep_UndefinedInst(void);

// Exception(Floating Point)

void  __attribute((interrupt)) Excep_FloatingPoint(void);

// NMI

void  __attribute((interrupt)) NonMaskableInterrupt(void);

// Dummy

void  __attribute((interrupt)) Dummy(void);

// BRK

void  __attribute((interrupt)) Excep_BRK(void);

// vector  1 reserved
// vector  2 reserved
// vector  3 reserved
// vector  4 reserved
// vector  5 reserved
// vector  6 reserved
// vector  7 reserved
// vector  8 reserved
// vector  9 reserved
// vector 10 reserved
// vector 11 reserved
// vector 12 reserved
// vector 13 reserved
// vector 14 reserved
// vector 15 reserved

// BUSERR

void  __attribute((interrupt)) Excep_BUSERR(void);

// vector 17 reserved
// vector 18 reserved
// vector 19 reserved
// vector 20 reserved

// FCU_FCUERR

void  __attribute((interrupt)) Excep_FCU_FCUERR(void);

// vector 22 reserved

// FCU_FRDYI

void  __attribute((interrupt)) Excep_FCU_FRDYI(void);

// vector 24 reserved
// vector 25 reserved
// vector 26 reserved

// ICU_SWINT

void  __attribute((interrupt)) Excep_ICU_SWINT(void);

// CMTU0_CMT0

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Cmt0Cmi0(void);

// CMTU0_CMT1

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Cmt1Cmi1(void);

// CMTU1_CMT2

void  __attribute((interrupt)) Excep_CMTU1_CMT2(void);

// CMTU1_CMT3

void  __attribute((interrupt)) Excep_CMTU1_CMT3(void);

// vector 32 reserved

// USB0_D0FIFO0

void  __attribute((interrupt)) Excep_USB0_D0FIFO0(void);

// USB0_D1FIFO0

void  __attribute((interrupt)) Excep_USB0_D1FIFO0(void);

// USB0_USBI0

void  __attribute((interrupt)) Excep_USB0_USBI0(void);

// vector 36 reserved
// vector 37 reserved
// vector 38 reserved

// RSPI0_SPRI0

void  __attribute((interrupt)) Excep_RSPI0_SPRI0(void);

// RSPI0_SPTI0

void  __attribute((interrupt)) Excep_RSPI0_SPTI0(void);

// RSPI0_SPII0

void  __attribute((interrupt)) Excep_RSPI0_SPII0(void);

// RSPI1_SPRI1

void  __attribute((interrupt)) Excep_RSPI1_SPRI1(void);

// RSPI1_SPTI1

void  __attribute((interrupt)) Excep_RSPI1_SPTI1(void);

// RSPI1_SPII1

void  __attribute((interrupt)) Excep_RSPI1_SPII1(void);

// RSPI2_SPRI2

void  __attribute((interrupt)) Excep_RSPI2_SPRI2(void);

// RSPI2_SPTI2

void  __attribute((interrupt)) Excep_RSPI2_SPTI2(void);

// RSPI2_SPII2

void  __attribute((interrupt)) Excep_RSPI2_SPII2(void);

// CAN0_RXF0

void  __attribute((interrupt)) Excep_CAN0_RXF0(void);

// CAN0_TXF0

void  __attribute((interrupt)) Excep_CAN0_TXF0(void);

// CAN0_RXM0

void  __attribute((interrupt)) Excep_CAN0_RXM0(void);

// CAN0_TXM0

void  __attribute((interrupt)) Excep_CAN0_TXM0(void);

// CAN1_RXF1

void  __attribute((interrupt)) Excep_CAN1_RXF1(void);

// CAN1_TXF1

void  __attribute((interrupt)) Excep_CAN1_TXF1(void);

// CAN1_RXM1

void  __attribute((interrupt)) Excep_CAN1_RXM1(void);

// CAN1_TXM1

void  __attribute((interrupt)) Excep_CAN1_TXM1(void);

// CAN2_RXF2

void  __attribute((interrupt)) Excep_CAN2_RXF2(void);

// CAN2_TXF2

void  __attribute((interrupt)) Excep_CAN2_TXF2(void);

// CAN2_RXM2

void  __attribute((interrupt)) Excep_CAN2_RXM2(void);

// CAN2_TXM2

void  __attribute((interrupt)) Excep_CAN2_TXM2(void);

// vector 60 reserved
// vector 61 reserved

// RTC_COUNTUP

void  __attribute((interrupt)) Excep_RTC_COUNTUP(void);

// vector 63 reserved

// IRQ0

void  __attribute((interrupt)) Excep_IRQ0(void);

// IRQ1

void  __attribute((interrupt)) Excep_IRQ1(void);

// IRQ2

void  __attribute((interrupt)) Excep_IRQ2(void);

// IRQ3

void  __attribute((interrupt)) Excep_IRQ3(void);

// IRQ4

void  __attribute((interrupt)) Excep_IRQ4(void);

// IRQ5

void  __attribute((interrupt)) Excep_IRQ5(void);

// IRQ6

void  __attribute((interrupt)) Excep_IRQ6(void);

// IRQ7

void  __attribute((interrupt)) Excep_IRQ7(void);

// IRQ8

void  __attribute((interrupt)) Excep_IRQ8(void);

// IRQ9

void  __attribute((interrupt)) Excep_IRQ9(void);

// IRQ10

void  __attribute((interrupt)) Excep_IRQ10(void);

// IRQ11

void  __attribute((interrupt)) Excep_IRQ11(void);

// IRQ12

void  __attribute((interrupt)) Excep_IRQ12(void);

// IRQ13

void  __attribute((interrupt)) Excep_IRQ13(void);

// IRQ14

void  __attribute((interrupt)) Excep_IRQ14(void);

// IRQ15

void  __attribute((interrupt)) Excep_IRQ15(void);

// vector 80 reserved
// vector 81 reserved
// vector 82 reserved
// vector 83 reserved
// vector 84 reserved
// vector 85 reserved
// vector 86 reserved
// vector 87 reserved
// vector 88 reserved
// vector 89 reserved

// USB_USBR0

void  __attribute((interrupt)) Excep_USB_USBR0(void);

// vector 91 reserved

// RTC_ALARM

void  __attribute((interrupt)) Excep_RTC_ALARM(void);

// RTC_SLEEP

void  __attribute((interrupt)) Excep_RTC_SLEEP(void);

// vector 94 reserved
// vector 95 reserved
// vector 96 reserved
// vector 97 reserved

// AD0_ADI0

void  __attribute((interrupt)) Excep_AD0_ADI0(void);

// vector 99 reserved
// vector 100 reserved
// vector 101 reserved

// S12AD0_S12ADI0

void  __attribute((interrupt)) Excep_S12AD0_S12ADI0(void);

// vector 103 reserved
// vector 104 reserved
// vector 105 reserved

// ICU_GE0

void  __attribute((interrupt)) Excep_ICU_GE0(void);

// ICU_GE1

void  __attribute((interrupt)) Excep_ICU_GE1(void);

// ICU_GE2

void  __attribute((interrupt)) Excep_ICU_GE2(void);

// ICU_GE3

void  __attribute((interrupt)) Excep_ICU_GE3(void);

// ICU_GE4

void  __attribute((interrupt)) Excep_ICU_GE4(void);

// ICU_GE5

void  __attribute((interrupt)) Excep_ICU_GE5(void);

// ICU_GE6

void  __attribute((interrupt)) Excep_ICU_GE6(void);

// vector 113 reserved

// ICU_GL0

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_IcuGroup12(void);

// vector 115 reserved
// vector 116 reserved
// vector 117 reserved
// vector 118 reserved
// vector 119 reserved
// vector 120 reserved
// vector 121 reserved

// SCIX_SCIX0

void  __attribute((interrupt)) Excep_SCIX_SCIX0(void);

// SCIX_SCIX1

void  __attribute((interrupt)) Excep_SCIX_SCIX1(void);

// SCIX_SCIX2

void  __attribute((interrupt)) Excep_SCIX_SCIX2(void);

// SCIX_SCIX3

void  __attribute((interrupt)) Excep_SCIX_SCIX3(void);

// TPU0_TGI0A

void  __attribute((interrupt)) Excep_TPU0_TGI0A(void);

// TPU0_TGI0B

void  __attribute((interrupt)) Excep_TPU0_TGI0B(void);

// TPU0_TGI0C

void  __attribute((interrupt)) Excep_TPU0_TGI0C(void);

// TPU0_TGI0D

void  __attribute((interrupt)) Excep_TPU0_TGI0D(void);

// TPU1_TGI1A

void  __attribute((interrupt)) Excep_TPU1_TGI1A(void);

// TPU1_TGI1B

void  __attribute((interrupt)) Excep_TPU1_TGI1B(void);

// TPU2_TGI2A

void  __attribute((interrupt)) Excep_TPU2_TGI2A(void);

// TPU2_TGI2B

void  __attribute((interrupt)) Excep_TPU2_TGI2B(void);

// TPU3_TGI3A

void  __attribute((interrupt)) Excep_TPU3_TGI3A(void);

// TPU3_TGI3B

void  __attribute((interrupt)) Excep_TPU3_TGI3B(void);

// TPU3_TGI3C

void  __attribute((interrupt)) Excep_TPU3_TGI3C(void);

// TPU3_TGI3D

void  __attribute((interrupt)) Excep_TPU3_TGI3D(void);

// TPU4_TGI4A

void  __attribute((interrupt)) Excep_TPU4_TGI4A(void);

// TPU4_TGI4B

void  __attribute((interrupt)) Excep_TPU4_TGI4B(void);

// TPU5_TGI5A

void  __attribute((interrupt)) Excep_TPU5_TGI5A(void);

// TPU5_TGI5B

void  __attribute((interrupt)) Excep_TPU5_TGI5B(void);

// TPU6_TGI6A

void  __attribute((interrupt)) Excep_TPU6_TGI6A(void);

// MTU0_TGIA0
//#pragma interrupt (Excep_MTU0_TGIA0(vect=142))
//void Excep_MTU0_TGIA0(void);

// TPU6_TGI6B

void  __attribute((interrupt)) Excep_TPU6_TGI6B(void);

// MTU0_TGIB0
//#pragma interrupt (Excep_MTU0_TGIB0(vect=143))
//void Excep_MTU0_TGIB0(void);

// TPU6_TGI6C

void  __attribute((interrupt)) Excep_TPU6_TGI6C(void);

// MTU0_TGIC0
//#pragma interrupt (Excep_MTU0_TGIC0(vect=144))
//void Excep_MTU0_TGIC0(void);

// TPU6_TGI6D

void  __attribute((interrupt)) Excep_TPU6_TGI6D(void);

// MTU0_TGID0
//#pragma interrupt (Excep_MTU0_TGID0(vect=145))
//void Excep_MTU0_TGID0(void);

// MTU0_TGIE0

void  __attribute((interrupt)) Excep_MTU0_TGIE0(void);

// MTU0_TGIF0

void  __attribute((interrupt)) Excep_MTU0_TGIF0(void);

// TPU7_TGI7A

void  __attribute((interrupt)) Excep_TPU7_TGI7A(void);

// MTU1_TGIA1
//#pragma interrupt (Excep_MTU1_TGIA1(vect=148))
//void Excep_MTU1_TGIA1(void);

// TPU7_TGI7B

void  __attribute((interrupt)) Excep_TPU7_TGI7B(void);

// MTU1_TGIB1
//#pragma interrupt (Excep_MTU1_TGIB1(vect=149))
//void Excep_MTU1_TGIB1(void);

// TPU8_TGI8A

void  __attribute((interrupt)) Excep_TPU8_TGI8A(void);

// TPU8_TGIA2
//#pragma interrupt (Excep_MTU2_TGIA2(vect=150))
//void Excep_MTU2_TGIA2(void);

// TPU8_TGI8B

void  __attribute((interrupt)) Excep_TPU8_TGI8B(void);

// TPU8_TGIB2
//#pragma interrupt (Excep_MTU2_TGIB2(vect=151))
//void Excep_MTU2_TGIB2(void);

// TPU8_TGI9A

void  __attribute((interrupt)) Excep_TPU9_TGI9A(void);

// MTU3_TGIA3
//#pragma interrupt (Excep_MTU3_TGIA3(vect=152))
//void Excep_MTU3_TGIA3(void);

// TPU9_TGI9B

void  __attribute((interrupt)) Excep_TPU9_TGI9B(void);

// MTU3_TGIB3
//#pragma interrupt (Excep_MTU3_TGIB3(vect=153))
//void Excep_MTU3_TGIB3(void);

// TPU9_TGI9C

void  __attribute((interrupt)) Excep_TPU9_TGI9C(void);

// MTU3_TGIC3
//#pragma interrupt (Excep_MTU3_TGIC3(vect=154))
//void Excep_MTU3_TGIC3(void);

// TPU9_TGI9D

void  __attribute((interrupt)) Excep_TPU9_TGI9D(void);

// MTU3_TGID3
//#pragma interrupt (Excep_MTU3_TGID3(vect=155))
//void Excep_MTU3_TGID3(void);

// TPU10_TGI10A

void  __attribute((interrupt)) Excep_TPU10_TGI10A(void);

// MTU4_TGIA4
//#pragma interrupt (Excep_MTU4_TGIA4(vect=156))
//void Excep_MTU4_TGIA4(void);

// TPU10_TGI10B

void  __attribute((interrupt)) Excep_TPU10_TGI10B(void);

// MTU4_TGIB4
//#pragma interrupt (Excep_MTU4_TGIB4(vect=157))
//void Excep_MTU4_TGIB4(void);

// MTU4_TGIC4

void  __attribute((interrupt)) Excep_MTU4_TGIC4(void);

// MTU4_TGID4

void  __attribute((interrupt)) Excep_MTU4_TGID4(void);

// MTU4_TGIV4

void  __attribute((interrupt)) Excep_MTU4_TGIV4(void);

// vector 161 reserved
// vector 162 reserved
// vector 163 reserved

// MTU5_TGIU5

void  __attribute((interrupt)) Excep_MTU5_TGIU5(void);

// MTU5_TGIV5

void  __attribute((interrupt)) Excep_MTU5_TGIV5(void);

// MTU5_TGIW5

void  __attribute((interrupt)) Excep_MTU5_TGIW5(void);

// TPU11_TGI11A

void  __attribute((interrupt)) Excep_TPU11_TGI11A(void);

// TPU11_TGI11B

void  __attribute((interrupt)) Excep_TPU11_TGI11B(void);

// POE_OEI1

void  __attribute((interrupt)) Excep_POE_OEI1(void);

// POE_OEI2

void  __attribute((interrupt)) Excep_POE_OEI2(void);

// vector 168 reserved
// vector 169 reserved

// TMR0_CMIA0

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Tmr0Cmia0(void);

// TMR0_CMIB0

void  __attribute((interrupt)) Excep_TMR0_CMIB0(void);

// TMR0_OVI0

void  __attribute((interrupt)) Excep_TMR0_OVI0(void);

// TMR1_CMIA1

void  __attribute((interrupt)) Excep_TMR1_CMIA1(void);

// TMR1_CMIB1

void  __attribute((interrupt)) Excep_TMR1_CMIB1(void);

// TMR1_OVI1

void  __attribute((interrupt)) Excep_TMR1_OVI1(void);

// TMR2_CMIA2

void  __attribute((interrupt)) Excep_TMR2_CMIA2(void);

// TMR2_CMIB2

void  __attribute((interrupt)) Excep_TMR2_CMIB2(void);

// TMR2_OVI2

void  __attribute((interrupt)) Excep_TMR2_OVI2(void);

// TMR3_CMIA3

void  __attribute((interrupt)) Excep_TMR3_CMIA3(void);

// TMR3_CMIB3

void  __attribute((interrupt)) Excep_TMR3_CMIB3(void);

// TMR3_OVI3

void  __attribute((interrupt)) Excep_TMR3_OVI3(void);

// RIIC0_EEI0

void  __attribute((interrupt)) Excep_RIIC0_EEI0(void);

// RIIC0_RXI0

void  __attribute((interrupt)) Excep_RIIC0_RXI0(void);

// RIIC0_TXI0

void  __attribute((interrupt)) Excep_RIIC0_TXI0(void);

// RIIC0_TEI0

void  __attribute((interrupt)) Excep_RIIC0_TEI0(void);

// RIIC1_EEI1

void  __attribute((interrupt)) Excep_RIIC1_EEI1(void);

// RIIC1_RXI1

void  __attribute((interrupt)) Excep_RIIC1_RXI1(void);

// RIIC1_TXI1

void  __attribute((interrupt)) Excep_RIIC1_TXI1(void);

// RIIC1_TEI1

void  __attribute((interrupt)) Excep_RIIC1_TEI1(void);

// RIIC2_EEI2

void  __attribute((interrupt)) Excep_RIIC2_EEI2(void);

// RIIC2_RXI2

void  __attribute((interrupt)) Excep_RIIC2_RXI2(void);

// RIIC2_TXI2

void  __attribute((interrupt)) Excep_RIIC2_TXI2(void);

// RIIC2_TEI2

void  __attribute((interrupt)) Excep_RIIC2_TEI2(void);

// RIIC3_EEI3

void  __attribute((interrupt)) Excep_RIIC3_EEI3(void);

// RIIC3_RXI3

void  __attribute((interrupt)) Excep_RIIC3_RXI3(void);

// RIIC3_TXI3

void  __attribute((interrupt)) Excep_RIIC3_TXI3(void);

// RIIC3_TEI3

void  __attribute((interrupt)) Excep_RIIC3_TEI3(void);

// DMAC_DMAC0I

void  __attribute((interrupt)) Excep_DMAC_DMAC0I(void);

// DMAC_DMAC1I

void  __attribute((interrupt)) Excep_DMAC_DMAC1I(void);

// DMAC_DMAC2I

void  __attribute((interrupt)) Excep_DMAC_DMAC2I(void);

// DMAC_DMAC3I

void  __attribute((interrupt)) Excep_DMAC_DMAC3I(void);

// vector 202 reserved
// vector 203 reserved
// vector 204 reserved
// vector 205 reserved
// vector 206 reserved
// vector 207 reserved
// vector 208 reserved
// vector 209 reserved
// vector 210 reserved
// vector 211 reserved
// vector 212 reserved
// vector 213 reserved

// SCI0_RXI0

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci0Rxi0(void);

// SCI0_TXI0

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci0Txi0(void);

// SCI0_TEI0

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci0Tei0(void);

// SCI1_RXI1

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci1Rxi1(void);

// SCI1_TXI1

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci1Txi1(void);

// SCI1_TEI1

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci1Tei1(void);

// SCI2_RXI2

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci2Rxi2(void);

// SCI2_TXI2

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci2Txi2(void);

// SCI2_TEI2

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci2Tei2(void);

// SCI3_RXI3

void  __attribute((interrupt)) Excep_SCI3_RXI3(void);

// SCI3_TXI3

void  __attribute((interrupt)) Excep_SCI3_TXI3(void);

// SCI3_TEI3

void  __attribute((interrupt)) Excep_SCI3_TEI3(void);

// SCI4_RXI4

void  __attribute((interrupt)) Excep_SCI4_RXI4(void);

// SCI4_TXI4

void  __attribute((interrupt)) Excep_SCI4_TXI4(void);

// SCI4_TEI4

void  __attribute((interrupt)) Excep_SCI4_TEI4(void);

// SCI5_RXI5

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci5Rxi5(void);

// SCI5_TXI5

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci5Txi5(void);

// SCI5_TEI5

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci5Tei5(void);

// SCI6_RXI6

void  __attribute((interrupt)) Excep_SCI6_RXI6(void);

// SCI6_TXI6

void  __attribute((interrupt)) Excep_SCI6_TXI6(void);

// SCI6_TEI6

void  __attribute((interrupt)) Excep_SCI6_TEI6(void);

// SCI7_RXI7

void  __attribute((interrupt)) Excep_SCI7_RXI7(void);

// SCI7_TXI7

void  __attribute((interrupt)) Excep_SCI7_TXI7(void);

// SCI7_TEI7

void  __attribute((interrupt)) Excep_SCI7_TEI7(void);

// SCI8_RXI8

void  __attribute((interrupt)) Excep_SCI8_RXI8(void);

// SCI8_TXI8

void  __attribute((interrupt)) Excep_SCI8_TXI8(void);

// SCI8_TEI8

void  __attribute((interrupt)) Excep_SCI8_TEI8(void);

// SCI9_RXI9

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci9Rxi9(void);

// SCI9_TXI9

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci9Txi9(void);

// SCI9_TEI9

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci9Tei9(void);

// SCI10_RXI10

void  __attribute((interrupt)) Excep_SCI10_RXI10(void);

// SCI10_TXI10

void  __attribute((interrupt)) Excep_SCI10_TXI10(void);

// SCI10_TEI10

void  __attribute((interrupt)) Excep_SCI10_TEI10(void);

// SCI11_RXI11

void  __attribute((interrupt)) Excep_SCI11_RXI11(void);

// SCI11_TXI11

void  __attribute((interrupt)) Excep_SCI11_TXI11(void);

// SCI11_TEI11

void  __attribute((interrupt)) Excep_SCI11_TEI11(void);

// SCI12_RXI12

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci12Rxi12(void);

// SCI12_TXI12

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci12Txi12(void);

// SCI12_TEI12

void  __attribute((section("PIntPRG")))  __attribute((interrupt)) R_EXCEP_Sci12Tei12(void);

// IEB_IEBINT

void  __attribute((interrupt)) Excep_IEB_IEBINT(void);

// vector 254 reserved
// vector 255 reserved

//;<<VECTOR DATA START (POWER ON RESET)>>
//;Power On Reset PC
void  __attribute((section("PResetPRG"))) PowerON_Reset_PC(void);
//;<<VECTOR DATA END (POWER ON RESET)>>

#endif //__VECT_H__
