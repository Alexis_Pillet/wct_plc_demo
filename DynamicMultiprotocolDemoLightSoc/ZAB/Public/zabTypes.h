/*
  Name:    ZigBee Application Brick Types
  Author:  ZigBee Excellence Center
  Company: Schneider Electric

  Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

  Description:
    This file is included in all ZigBee Application Brick implementations (zabXxxxService.c), NOT interfaces (zabXxxxInterface.h).
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.04.00  21-May-14   MvdB   ARTF58379: Change ask FirmwareImagePointer to FirmwareImageData
 * 00.00.06.01  17-Sep-14   MvdB   ARTF104182/54399: Support channel change API and notification
 * 00.00.06.02  30-Sep-14   MvdB   ARTF55512: Support network key change action
 * 00.00.06.05  08-Oct-14   MvdB   ARTF57973: Mutexes need service pointer. Remove Mutex Asks.
 * 00.00.06_GP  15-Oct-14   MvdB   ARTF105854: Support Smartlink IPZ hardware for Pro+GP ZNP
 * 01.00.00.02  28-Jan-15   MvdB   ARTF111653: Split in/out buffer ask into separate items for short and long
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 * 002.000.002  05-Mar-15   MvdB   ARTF115992: Harmonise zabTypes with JNI
 * 002.000.003  06-Mar-15   MvdB   ARTF116061: Support run time configuration of Green Power endpoint.
 * 002.000.004  01-Apr-15   MvdB   ARTF56504: Support missed channel change recovery, with actions/notifications/asks/gives to get/set parameters.
 * 002.001.001  29-Apr-15   MvdB   ARTF132260: Support firmware upgrade of CC2538 network processors
 * 002.002.005  11-Sep-15   MvdB   ARTF112436: Complete Get Network Info action
 * 002.002.011  12-Oct-15   MvdB   ARTF104108: Add notification of progress for Clone actions
 * 002.002.012  14-Oct-15   MvdB   ARTF104107: Expand enums to support End Device operation
 * 002.002.020  18-Mar-16   MvdB   ARTF165342: Support performing SBL firmware updates of ZAB_NET_PROC_MODEL_CC2538_SBS_SERIAL_GATEWAY
 * 002.002.021  20-Apr-16   MvdB   ARTF167736: Add ZAB_ACTION_INTERNAL_SERIAL_FLUSH
 * 002.002.052  30-Mar-17   MvdB   Support ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE
 * 002.002.053  12-Apr-17   SMon   Support ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE_PA
 */

#ifndef __ZAB_TYPES_H__
#define __ZAB_TYPES_H__

#include "osTypes.h"
#include "erStatusTypes.h"


/******************************************************************************
 *                      *****************************
 *                 *****        DEFAULT VALUES       *****
 *                      *****************************
 ******************************************************************************/

#define ZAB_TYPES_M_SCHNEIDER_MANUFACTURER_CODE 0x105E

#define ZAB_TYPES_M_NETWORK_CHANNEL_UNKNOWN 0
#define ZAB_TYPES_M_NETWORK_ADDRESS_UNKNOWN 0xFFFF
#define ZAB_TYPES_M_NETWORK_PAN_ID_UNKNOWN  0xFFFF
#define ZAB_TYPES_M_NETWORK_EPID_UNKNOWN    0xFFFFFFFFFFFFFFFFULL
#define ZAB_TYPES_M_IEEE_UNKNOWN            0xFFFFFFFFFFFFFFFFULL


/******************************************************************************
 *                      *****************************
 *                 *****           GENERAL           *****
 *                      *****************************
 ******************************************************************************/

/* Service is passed through to other interfaces but is private. It's really a zabServiceStruct */
typedef void zabService;

/******************************************************************************
 *                      *****************************
 *                 *****           ACTIONS           *****
 *                      *****************************
 ******************************************************************************/
typedef enum
{
  ZAB_ACTION_NULL                    = 0, // invalid state
  ZAB_ACTION_ERROR                   = 1, // set error state requires close or init

  /* Open ZAB
   * This establishes a connection to the network processor */
  ZAB_ACTION_OPEN                    = 2,

  /* Open ZAB
   * This closes a connection to the network processor */
  ZAB_ACTION_CLOSE                   = 3,

  /* Initialize network settings
   * Will ask to resume old network settings if any exist. */
  ZAB_ACTION_NWK_INIT                = 4,

  /* Discover ZigBee networks
   * Network must be initialized first.
   * Will return a list of networks that may be used if Join is selected in Network Steer */
  ZAB_ACTION_NWK_DISCOVER            = 5,

  /* Network Steer, will as if you want to Join or Form, then ask for other required parameters */
  ZAB_ACTION_NWK_STEER               = 6,

  /* EZ Mode Network Steering
   * Will discover networks, then form, join or fail based on the results. */
  ZAB_ACTION_EZ_MODE_NWK_STEER       = 7,

  /* Permit Join
   * Permit/deny other devices to join the network */
  ZAB_ACTION_NWK_PERMIT_JOIN_ENABLE  = 8,
  ZAB_ACTION_NWK_PERMIT_JOIN_DISABLE = 9,

  /* Firmware Update
   * Switches to firmware update mode.
   * Only works from ZAB_OPEN_STATE_OPENED or ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE. All other states will fail. */
  ZAB_ACTION_GO_TO_FIRMWARE_UPDATER  = 10,

  /* Exit Firmware Update
   * REturn to normal mode
   * Only works from ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE. All other states will fail. */
  ZAB_ACTION_EXIT_FIRMWARE_UPDATER   = 11,

  /* Firmware Update
   * Starts a firmware update.
   * Only works from ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE. All other states will fail. */
  ZAB_ACTION_UPDATE_FIRMWARE         = 12,

  /* TX Power
   * Request setting of TX Power of the network processor */
  ZAB_ACTION_SET_TX_POWER            = 13,

  /* Change Channel
   * Request the network to change to a new channel */
  ZAB_ACTION_CHANGE_CHANNEL          = 14,

  /* Network Key Channel
   * Request the network key to be updated */
  ZAB_ACTION_CHANGE_NETWORK_KEY      = 15,

  /* Reserved for JNI */
  ZAB_ACTION_RESERVED_19             = 19,

  /* Cloning Actions
   *  - Supported for Texas Instruments network processors only! */
  ZAB_ACTION_CLONE_GET               = 20,
  ZAB_ACTION_CLONE_SET               = 21,

  /* Antenna Selection
   * Will ask for ZAB_ASK_ANTENNA_MODE and set network processor to use this antenna.
   *  - Supported for Texas Instruments network processors only! */
  ZAB_ACTION_SELECT_ANTENNA          = 22,

  /* Get network info
   * Will give: ZAB_GIVE_NWK_ADDRESS, ZAB_GIVE_NWK_CHANNEL_NUMBER, ZAB_GIVE_NWK_PAN_ID, ZAB_GIVE_NWK_EPID.
   * May be called once ZAB_OPEN_STATE_OPENED */
  ZAB_ACTION_NWK_GET_INFO            = 23,

  /* Get/Set Network Maintenance Parameters
   * Set will ask for ZAB_ASK_NETWORK_MAINTENANCE_SLOW_PING_TIME_MS & ZAB_ASK_NETWORK_MAINTENANCE_FAST_PING_TIME_MS.
   * Both will give ZAB_GIVE_NETWORK_MAINTENANCE_SLOW_PING_TIME_MS & ZAB_GIVE_NETWORK_MAINTENANCE_FAST_PING_TIME_MS.
   *  - Supported for Texas Instruments network processors only! */
  ZAB_ACTION_NWK_MAINTENANCE_GET_PARAMS = 24,
  ZAB_ACTION_NWK_MAINTENANCE_SET_PARAMS = 25,

  /* Delimiter for user actions */
  ZAB_ACTION_MAX                     = 26,


  /* INTERNAL ONLY! Not for application usage (will be rejected).
   * This is done as an "internal action" for now as it is easy and low risk for SLIPZ.
   * In the future it should probably be moved somewhere hidden! */
  ZAB_ACTION_INTERNAL_SERIAL_FLUSH   = 0x80,

  /* INTERNAL ONLY! Not for application usage (will be rejected).
   * This is done as an "internal action" for now as it is easy and low risk for EFR32 Development.
   * In the future it should probably be moved somewhere hidden! */
  ZAB_ACTION_INTERNAL_SERIAL_MODE_NORMAL      = 0x81,
  ZAB_ACTION_INTERNAL_SERIAL_MODE_BOOTLOADER  = 0x82,
} zabAction;


/******************************************************************************
 *                      *****************************
 *                 *****        NOTIFICATIONS        *****
 *                      *****************************
 ******************************************************************************/

/* Open/Closed state of the module */
typedef enum
{
  ZAB_OPEN_STATE_NULL      = 0, // ZAB is in error and should be closed
  ZAB_OPEN_STATE_ERROR     = 1, // ZAB is in error and should be closed
  ZAB_OPEN_STATE_CLOSED    = 2, // ZAB is closed and needs to be opened
  ZAB_OPEN_STATE_CLOSING   = 3, // ZAB is in the process of closing
  ZAB_OPEN_STATE_OPENING   = 4, // ZAB is in the processing of opening
  ZAB_OPEN_STATE_OPENED    = 5, // ZAB is open and ready for networking

  ZAB_OPEN_STATE_GOING_TO_FW_UPDATE    = 6, // ZAB is in the process of changing to firmware update mode
  ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE    = 7, // We are connected to a compatible bootloader and ready to update firmware
  ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE = 8, // We are connected to a ZNP, but it is an incompatible version. Firmware update is required.

  ZAB_OPEN_STATE_MAX
} zabOpenState;

/* Network State */
typedef enum
{
  ZAB_NWK_STATE_UNINITIALISED      = 0,
  ZAB_NWK_STATE_NONE               = 1,
  ZAB_NWK_STATE_NETWORKED          = 2,
  ZAB_NWK_STATE_NETWORKING         = 3,
  ZAB_NWK_STATE_DISCOVERING        = 4,
  ZAB_NWK_STATE_DISCOVERY_COMPLETE = 5,
  ZAB_NWK_STATE_LEAVING            = 6,

  // Experimental state for end device support. Indicates that the device is networked, but currently can't communicate as it has no parent */
  ZAB_NWK_STATE_NETWORKED_NO_COMMS = 7,

  // Reserved for JNI
  ZAB_NWK_STATE_RESERVED_7F        = 0x7F,
} zabNwkState;

/* Firmware Update State */
typedef enum
{
  ZAB_FW_STATE_NONE        = 0,
  ZAB_FW_STATE_UPDATING    = 1,
  ZAB_FW_STATE_VERIFYING   = 2,
  ZAB_FW_STATE_FAILED      = 3,
  ZAB_FW_STATE_COMPLETE    = 4,

  // For future
//ZAB_FW_STATE_READING     = 5,
} zabFwUpdateState;

/* Clone State */
typedef enum
{
  ZAB_CLONE_STATE_NONE                   = 0,
  ZAB_CLONE_STATE_READING                = 1,
  ZAB_CLONE_STATE_READ_COMPLETE_SUCCESS  = 2,
  ZAB_CLONE_STATE_READ_FAILED            = 3,
  ZAB_CLONE_STATE_WRITING                = 4,
  ZAB_CLONE_STATE_WRITE_COMPLETE_SUCCESS = 5,
  ZAB_CLONE_STATE_WRITE_FAILED           = 6,
} zabCloneState;

/* Channel Change State */
typedef enum
{
  ZAB_CHANNEL_CHANGE_STATE_NONE        = 0,
  ZAB_CHANNEL_CHANGE_STATE_IN_PROGRESS = 1,
  ZAB_CHANNEL_CHANGE_STATE_FAILED      = 2,
  ZAB_CHANNEL_CHANGE_STATE_SUCCESS     = 3,
} zabChannelChangeState;

/* Network Key Change State */
typedef enum
{
  ZAB_NETWORK_KEY_CHANGE_STATE_NONE        = 0,
  ZAB_NETWORK_KEY_CHANGE_STATE_IN_PROGRESS = 1,
  ZAB_NETWORK_KEY_CHANGE_STATE_FAILED      = 2,
  ZAB_NETWORK_KEY_CHANGE_STATE_SUCCESS     = 3,
} zabNetworkKeyChangeState;

/* Network Maintenance Parameter Get/Set State */
typedef enum
{
  ZAB_NETWORK_MAINTENANCE_PARAM_STATE_NONE          = 0,
  ZAB_NETWORK_MAINTENANCE_PARAM_STATE_GET           = 1,
  ZAB_NETWORK_MAINTENANCE_PARAM_STATE_GET_FAILED    = 2,
  ZAB_NETWORK_MAINTENANCE_PARAM_STATE_GET_SUCCESS   = 3,
  ZAB_NETWORK_MAINTENANCE_PARAM_STATE_SET           = 4,
  ZAB_NETWORK_MAINTENANCE_PARAM_STATE_SET_FAILED    = 5,
  ZAB_NETWORK_MAINTENANCE_PARAM_STATE_SET_SUCCESS   = 6,
} zabNetworkMaintenanceParamState;

/* Network Info State */
typedef enum
{
  ZAB_NETWORK_INFO_STATE_NONE                       = 0,
  ZAB_NETWORK_INFO_STATE_IN_PROGRESS                = 1,
  ZAB_NETWORK_INFO_STATE_FAILED                     = 2,
  ZAB_NETWORK_INFO_STATE_SUCCESS                    = 3,
} zabNetworkInfoState;

/* Notification Data - Union field selected by zabNotificationType */
typedef union
{
  zabOpenState openState;
  zabNwkState nwkState;
  zabFwUpdateState fwUpdateState;
  unsigned8 progress;
  unsigned16 nwkPermitJoinTime;
  zabCloneState cloneState;
  zabError errorNumber;
  zabChannelChangeState channelChangeState;
  zabNetworkKeyChangeState nwkKeyChangeState;
  zabNetworkMaintenanceParamState nwkMaintenanceParamState;
  zabNetworkInfoState nwkInfoState;
} zabNotificationData;

/* Notification Type - Defines field of zabNotificationData to be used */
typedef enum
{                                                /* Data (Field of zabNotificationData) */
  ZAB_NOTIFICATION_UNDEFINED                        = 0,
  ZAB_NOTIFICATION_OPEN_STATE                       = 1, /* zabOpenState */
  ZAB_NOTIFICATION_NETWORK_STATE                    = 2, /* zabNwkState */
  ZAB_NOTIFICATION_FIRMWARE_UPDATE_STATE            = 3, /* zabFwUpdateState */
  ZAB_NOTIFICATION_FIRMWARE_UPDATE_PROGRESS         = 4, /* progress */
  ZAB_NOTIFICATION_NETWORK_PERMIT_JOIN              = 5, /* nwkPermitJoinTime */
  ZAB_NOTIFICATION_CLONE_STATE                      = 6, /* zabCloneState */
  ZAB_NOTIFICATION_ERROR                            = 7, /* errorNumber */
  ZAB_NOTIFICATION_CHANNEL_CHANGE_STATE             = 8, /* zabChannelChangeState */
  ZAB_NOTIFICATION_NETWORK_KEY_CHANGE_STATE         = 9, /* nwkKeyChangeState */
  ZAB_NOTIFICATION_CLONE_PROGRESS                   = 10,/* progress */
  ZAB_NOTIFICATION_NETWORK_MAINTENANCE_PARAM_STATE  = 11, /* nwkMaintenanceParamState */
  ZAB_NOTIFICATION_NETWORK_INFO_STATE               = 12, /* nwkInfoState */
} zabNotificationType;



/******************************************************************************
 *                      *****************************
 *                 *****          ASK/GIVE           *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 * Ask Types
 * These are the values ZAB will ask the App for
 ******************************************************************************/
typedef enum
{
  ZAB_ASK_NULL                              =  0, // invalid

  /* Number of IN (RX) message buffers to allocate.
   * If ZAB_ASK_BUFFER_COUNT_IN_LONG is supported, then ZAB_ASK_BUFFER_COUNT_IN is the number of short buffers.
   * If ZAB_ASK_BUFFER_COUNT_IN_LONG is not supported, then ZAB_ASK_BUFFER_COUNT_IN is divided between short and long buffers. (For backwards compatibility)
   * Asked during zabCoreCreate()
   * Size = unsigned16
   * Format = Number of buffers */
  ZAB_ASK_BUFFER_COUNT_IN                   = 1,

  /* Number of long IN (RX) message buffers to allocate.
   * This item was added in 1.0.1 and is strongly recommended to be supported to give applications
   * clear control over the ratio of short/long buffers.
   * Asked during zabCoreCreate()
   * Size = unsigned16
   * Format = Number of long buffers */
  ZAB_ASK_BUFFER_COUNT_IN_LONG              = 26,

  /* Number of OUT(TX) message buffers to allocate.
   * If ZAB_ASK_BUFFER_COUNT_OUT_LONG is supported, then ZAB_ASK_BUFFER_COUNT_OUT is the number of short buffers.
   * If ZAB_ASK_BUFFER_COUNT_OUT_LONG is not supported, then ZAB_ASK_BUFFER_COUNT_OUT is divided between short and long buffers. (For backwards compatibility)
   * Asked during zabCoreCreate()
   * Size = unsigned16
   * Format = Number of buffers */
  ZAB_ASK_BUFFER_COUNT_OUT                  = 2,

  /* Number of long OUT(TX) message buffers to allocate.
   * This item was added in 1.0.1 and is strongly recommended to be supported to give applications
   * clear control over the ratio of short/long buffers.
   * Asked during zabCoreCreate()
   * Size = unsigned16
   * Format = Number of long buffers */
  ZAB_ASK_BUFFER_COUNT_OUT_LONG             = 27,

  /* Number of event (action/notify) buffers to allocate.
   * Asked during zabCoreCreate()
   * Size = unsigned16
   * Format = Number of event buffers */
  ZAB_ASK_BUFFER_COUNT_EVENT                = 3,

  ZAB_ASK_SAP_MANAGE_MAX                    = 6,    // U REQ 1 byte number of external accesses to SAP
  ZAB_ASK_SAP_DATA_MAX                      = 7,    // U REQ 1 byte number of external accesses to SAP
  ZAB_ASK_SAP_SERIAL_MAX                    = 8,    // U REQ 1 byte number of external accesses to SAP

  // Network Processor Startup
  ZAB_ASK_NETWORK_PROCESSOR_START           = 9,    // Start network processor application. 1 byte: zab_true = yes, false = no.

  // Network Processor Closing
  ZAB_ASK_NETWORK_PROCESSOR_STOP_ON_CLOSE   = 10,   // Stop the network processor during a close action. 1 byte: zab_true = yes, false = no.

  // Network Steering
  ZAB_ASK_NWK_RESUME                        = 11,   // Resume previous network. 1 byte: zab_true = yes, false = no.
  ZAB_ASK_NWK_STEER                         = 12,   // Network Steer option (join/form). 1 byte: See zabNwkSteerOptions
  ZAB_ASK_NWK_CHANNEL_MASK                  = 13,   // Channel Mask for networking operations. 4 byte, native unsigned32. BitX =1 for channel X.
  ZAB_ASK_NWK_SECURITY_LEVEL                = 14,   // Security. 1 byte: zab_true = Network Key, false = None.
  ZAB_ASK_NWK_PAN_ID                        = 15,   // PANID. 2 byte, native unsigned16
  ZAB_ASK_NWK_EPID                          = 16,   // Extended PANID. 8 byte, native unsigned64
  ZAB_ASK_NWK_JOIN_BEACON                   = 17,   // Beacon to be joined. Format/Size = BeaconFormat_t. Return something given with ZAB_GIVE_NWK_BEACON.

  ZAB_ASK_TX_POWER_DBM                      = 18,   // Tx Power in dBm. 1 byte, signed8
  ZAB_ASK_PERMIT_JOIN_TIME                  = 19,   // Permit Join Time. 2 byte, unsigned16

  // Firmware Update
  // Reserved for future upgrade
//ZAB_ASK_FIRMWARE_OPERATION                = 20,   // Bitmap operation Firmware update 1 byte (0x01 = READING ; 0x02 = WRITING ; 0x04 = VERIFING) ; Default = (WRITING | VERIFING)
  ZAB_ASK_FIRMWARE_IMAGE_LENGTH             = 21,   // Length of firmware image. 4 byte unsigned32
  ZAB_ASK_FIRMWARE_IMAGE_DATA               = 22,   // Firmware image data. Param1 = Offset in image

  // Reserved for JNI
  ZAB_ASK_RESERVED_23                       = 23,


  /* Channel to change to during network channel change
   * Asked during ZAB_ACTION_CHANGE_CHANNEL
   * Size = unsigned8
   * Format = 11 <= Channel number <= 26 */
  ZAB_ASK_CHANNEL_TO_CHANGE_TO              = 24,

  // Obsolete, may be reused.
//ZAB_ASK_SPARE_MAY_BE_RESUSED                   = 25,


  /* Antenna operating mode to use.
   * Asked during ZAB_ACTION_SELECT_ANTENNA
   * Size = unsigned8
   * Format = zabAntennaOperatingMode */
  ZAB_ASK_ANTENNA_MODE                      = 28,

  /* Application Endpoint that will be used for GreenPower communications.
   * Stack will forward all incoming GP commands to this endpoint and all outgoing GP
   * commands should be sourced from it.
   * Asked during Network Resume, Form or Join
   * Size = unsigned8
   * Format = Endpoint Number.
   * Valid Range = 1 - 0xEF
   *               ZAB_ENDPOINT_INVALID (and other values or not supported) will disable Green Power */
  ZAB_ASK_GREEN_POWER_ENDPOINT              = 29,


  /* Network Management Slow Ping Time
   * Asked during ZAB_ACTION_NWK_MAINTENANCE_SET_PARAMS
   * Size = unsigned32
   * Format = Slow ping time in milli-seconds. This is the rate at which the device
   *          will ping the Trust Center to confirm it's network settings are correct. */
  ZAB_ASK_NETWORK_MAINTENANCE_SLOW_PING_TIME_MS = 30,


  /* Network Management Fast Ping Time
   * Asked during ZAB_ACTION_NWK_MAINTENANCE_SET_PARAMS
   * Size = unsigned32
   * Format = Fast ping time in milli-seconds. This is the rate at which the device
   *          will ping the Trust Center to confirm it's network settings are incorrect once communication has been lost. */
  ZAB_ASK_NETWORK_MAINTENANCE_FAST_PING_TIME_MS = 31,


  ZAB_ASK_COUNT
} zabAskId;


/******************************************************************************
 * Gives Types
 * These are the values ZAB will give the app
 ******************************************************************************/
typedef enum // Configuration Identifiers
{
  ZAB_GIVE_NULL                                 =  0, // invalid

  // Versioning
  ZAB_GIVE_CORE_VERSION                         = 1,    // 4 byte unsigned8[Major, Minor, Release, Build]
  ZAB_GIVE_VENDOR_VERSION                       = 2,    // 4 byte unsigned8[Major, Minor, Release, Build]
  ZAB_GIVE_VENDOR_TYPE                          = 3,    // 2 bytes, zabVendorType

  /* Hardware model of network processor connected */
  ZAB_GIVE_NETWORK_PROCESSOR_MODEL              = 4,    // 1 byte, zabNetworkProcessorModel

  /* The type of application currently running on the network processor */
  ZAB_GIVE_NETWORK_PROCESSOR_ACTIVE_APP_TYPE    = 5,    // 1 byte, zabNetworkProcessorApplication

  /* If active app is SBL: Type of application loaded in flash that will be started if FW update is not performed */
  ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_TYPE    = 6,    // 1 byte, zabNetworkProcessorApplication

  /* Type of application loaded in flash that will be started if FW update is not performed */
  ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_VERSION = 7,    // 4 byte unsigned8[Major, Minor, Release, Build]

  /* If active app is SBL: Version of the bootloader.*/
  ZAB_GIVE_NETWORK_PROCESSOR_BOOTLOADER_VERSION = 8,    // 4 byte unsigned8[Major, Minor, Release, Build]

  // Network parameters
  ZAB_GIVE_NWK_BEACON                           = 9,    // Beacon from discovered network. Format/Size = BeaconFormat_t. Use to answer ZAB_ASK_NWK_JOIN_BEACON.
  ZAB_GIVE_IEEE                                 = 10,   // IEEE Address of ZAB. 8 byte, native unsigned64
  ZAB_GIVE_NWK_ADDRESS                          = 11,   // Network (short) Address of ZAB. 2 byte, native unsigned16
  ZAB_GIVE_NWK_CHANNEL_NUMBER                   = 12,   // Channel number the ZAB is operating on (11-26). 1 byte.
  ZAB_GIVE_NWK_PAN_ID                           = 13,   // ZigBee PAN ID for the current ZigBee network. 2 byte, native unsigned16
  ZAB_GIVE_NWK_EPID                             = 14,   // ZigBee Extended PID for the current ZigBee network. 2 byte, native unsigned16

  /* Warning: These items are experimental and supported for Texas Instruments network processors only. */
  ZAB_GIVE_DEVICE_TYPE                          = 15,   // Zigbee Device Type, 1 byte, zabDeviceType
  ZAB_GIVE_PARENT_NWK_ADDRESS                   = 16,   // Parent Network (short) Address of ZAB. 2 byte, native unsigned16
  ZAB_GIVE_PARENT_IEEE                          = 17,   // Parent IEEE Address of ZAB. 8 byte, native unsigned64


  ZAB_GIVE_NWK_TX_POWER                         = 18,   // ZigBee Transmit Power in dBm. 1 byte, native signed8

  // Reserved for future Firmware Read
//ZAB_GIVE_FIRMWARE_IMAGE_DATA                  = 19,   // Firmware image data. Param1 = Offset in image

  /* Bitmask of supported antenna operating modes
   * Given during ZAB_ACTION_OPEN and ZAB_ACTION_SELECT_ANTENNA
   * Size = unsigned8
   * Format = zabAntennaSupportedModes */
  ZAB_GIVE_ANTENNA_SUPPORTED_MODES              = 20,

  /* Enumeration of current antenna operating mode
   * Given during ZAB_ACTION_OPEN and ZAB_ACTION_SELECT_ANTENNA
   * Size = unsigned8
   * Format = zabAntennaOperatingMode */
  ZAB_GIVE_ANTENNA_CURRENT_MODE                 = 21,

  /* Network Management Slow Ping Time
   * Given during ZAB_ACTION_NWK_MAINTENANCE_GET_PARAMS, ZAB_ACTION_NWK_MAINTENANCE_SET_PARAMS
   * Size = unsigned32
   * Format = Slow ping time in seconds. This is the rate at which the device
   *          will ping the Trust Center to confirm it's network settings are correct. */
  ZAB_GIVE_NETWORK_MAINTENANCE_SLOW_PING_TIME_MS = 22,

  /* Network Management Fast Ping Time
   * Given during ZAB_ACTION_NWK_MAINTENANCE_GET_PARAMS, ZAB_ACTION_NWK_MAINTENANCE_SET_PARAMS
   * Size = unsigned32
   * Format = Fast ping time in seconds. This is the rate at which the device
   *          will ping the Trust Center to confirm it's network settings are incorrect once communication has been lost. */
  ZAB_GIVE_NETWORK_MAINTENANCE_FAST_PING_TIME_MS = 23,
} zabGiveId;


/* Data format for ZAB_GIVE_NWK_BEACON/ZAB_ASK_NWK_JOIN_BEACON */
typedef struct
{
  unsigned16 src;
  unsigned16 pan;
  unsigned8 channel;
  unsigned8 permitJoin;
  unsigned8 routerCapacity;
  unsigned8 deviceCapacity;
  unsigned8 protocolVersion;
  unsigned8 stackProfile;
  unsigned8 lqi;
  unsigned8 depth;
  unsigned8 updateID;
  unsigned64 epid;
} BeaconFormat_t;

typedef enum
{
  ZAB_VENDOR_TYPE_UNKNOWN                = 0,
  ZAB_VENDOR_TYPE_TEXAS_INSTRUMENTS_ZNP  = 1,
  ZAB_VENDOR_TYPE_SILABS_EZSP            = 2
}zabVendorType;

typedef enum // Hardware Model
{
  ZAB_NET_PROC_MODEL_CC2530_TI_EB               = 0,
  ZAB_NET_PROC_MODEL_CC2531_TI_USB              = 1,
  ZAB_NET_PROC_MODEL_CC2531_SPECTEC_USB_PA      = 2,
  ZAB_NET_PROC_MODEL_CC2530_SE_OUREA_V2         = 3,
  ZAB_NET_PROC_MODEL_CC2531_SE_INVENTEK_PA      = 4,
  ZAB_NET_PROC_MODEL_CC2530_SE_NOVA             = 5,
  ZAB_NET_PROC_MODEL_CC2530_SE_SMARTLINKIPZ     = 6,
  ZAB_NET_PROC_MODEL_CC2538_TI_EB               = 7,
  ZAB_NET_PROC_MODEL_CC2538_SE_OUREA_UART       = 8,
  ZAB_NET_PROC_MODEL_CC2538_SE_OUREA_USB        = 9,
  ZAB_NET_PROC_MODEL_CC2538_SBS_SERIAL_GATEWAY  = 10,
  ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE       = 11,
  ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE_PA    = 12,
  ZAB_NET_PROC_MODEL_SILABS_EFR32_NCP           = 0x80,
  ZAB_NET_PROC_MODEL_UNKNOWN                    = 0xFF,
} zabNetworkProcessorModel;

typedef enum
{
  ZAB_NET_PROC_APP_BOOTLOADER       = 0,
  ZAB_NET_PROC_APP_ZIGBEE_PRO       = 1,
  ZAB_NET_PROC_APP_GP_15_4_BRICK    = 2,
  ZAB_NET_PROC_APP_ZIGBEE_EMC_BRICK = 3,
  ZAB_NET_PROC_APP_GPD_BRICK        = 4,
  ZAB_NET_PROC_APP_ZIGBEE_PRO_GP    = 5,

  ZAB_NET_PROC_APP_UNKNOWN          = 0xFF,
} zabNetworkProcessorApplication;

typedef enum // Network Steering Options
{
  ZAB_NWK_STEER_NONE            = 0,
  ZAB_NWK_STEER_JOIN            = 1,
  ZAB_NWK_STEER_FORM            = 2,

  /* Warning: Experimental feature for Texas Instruments network processors only.
   *          It is not recommended for a normal product to use this. */
  ZAB_NWK_STEER_JOIN_ENDDEVICE  = 3,
} zabNwkSteerOptions;


typedef enum // Device Types
{
  ZAB_DEVICE_TYPE_NONE                = 0,
  ZAB_DEVICE_TYPE_COORDINATOR         = 1,
  ZAB_DEVICE_TYPE_ROUTER              = 2,
  ZAB_DEVICE_TYPE_END_DEVICE          = 3,
  ZAB_DEVICE_TYPE_COUNT
} zabDeviceType;


/* Antenna Operating Mode */
typedef enum
{
  zabAntennaOperatingMode_Unknown               = 0x00,
  zabAntennaOperatingMode_Antenna1              = 0x01,
  zabAntennaOperatingMode_Antenna2              = 0x02,
  zabAntennaOperatingMode_DiversityAuto         = 0x03,
} zabAntennaOperatingMode;

/* Antenna Operating Mode and it's bit masks */
typedef unsigned8 zabAntennaSupportedModes;
#define ZAB_ANTENNA_SUPPORTED_MODE_ANTENNA1       0x01
#define ZAB_ANTENNA_SUPPORTED_MODE_ANTENNA2       0x02
#define ZAB_ANTENNA_SUPPORTED_MODE_DIVERSITY_AUTO 0x04

/* Endpoint limits */
#define ZAB_ENDPOINT_INVALID                      ( 0xFF )
#define ZAB_ENDPOINT_MIN_VALID                    ( 0x01 )
#define ZAB_ENDPOINT_MAX_VALID                    ( 0xEF )

#endif
