#ifndef _SZL_API_H_
#define _SZL_API_H_
/**
 * @ingroup  ZigBee
 * @defgroup szl_api Schneider ZigBee Library API
 * @author   Michael Thorsoe
 * @date     08-12-2014
 * @version  03.03.00
 *     <!--  REMEMBER TO UPDATE the version below -->
 *
 * ZAB Changes:
 * 002.002.021  21-Apr-16   MvdB   ARTF167808: Improve Attribute Read/Write/Configure Responses to always list all attributes requested
 *                                             Add SZL_STATUS_NO_RESPONSE
 *                                             Add Status to SZL_AttributeReportData_t for read response
 *                                             SZL_AttributeWriteRespParams_t: Change NumberOfFailed to NumberOfAttributes
 *                                             SZL_AttributeReportCfgRespParams_t: Change NumberOfFailed to NumberOfAttributes
 * SiLabs
 * 000.000.009  12-Jul-16   MvdB   ARTF174575: Parametise SZL_CB_ClusterCmd_t *PayloadOutSize with max data length application may provide
 *
 * 002.002.035  11-Jan-17   MvdB   ARTF170823: Make Read/Write Reporting configuration data (SZL_AttributeReportData_t) consistent with Read/Write data (SZL_AttributeData_t)
 * 002.002.054  20-Jul-17   SMon   ARTF214292 Manage timeout for default response
 */
#define SZL_API_VERSION_MAJOR "03"
#define SZL_API_VERSION_MINOR "03"
#define SZL_API_VERSION_MICRO "00"
/*
\verbatim
  _________      .__                  .__    .___
 /   _____/ ____ |  |__   ____   ____ |__| __| _/___________
 \_____  \_/ ___\|  |  \ /    \_/ __ \|  |/ __ |/ __ \_  __ \   ______
 /        \  \___|   Y  \   |  \  ___/|  / /_/ \  ___/|  | \/  /_____/
/_______  /\___  >___|  /___|  /\___  >__\____ |\___  >__|
        \/     \/     \/     \/     \/        \/    \/
__________.__      __________
\____    /|__| ____\______   \ ____   ____
  /     / |  |/ ___\|    |  _// __ \_/ __ \    ______
 /     /_ |  / /_/  >    |   \  ___/\  ___/   /_____/
/_______ \|__\___  /|______  /\___  >\___  >
        \/  /_____/        \/     \/     \/
.____    ._____.
|    |   |__\_ |______________ _______ ___.__.
|    |   |  || __ \_  __ \__  \\_  __ <   |  |
|    |___|  || \_\ \  | \// __ \|  | \/\___  |
|_______ \__||___  /__|  (____  /__|   / ____|
        \/       \/           \/       \/
\endverbatim
 * <b>API Documentation</b>
 *
 * Defines the interface for the ZigBee applications towards the ZigBee brick
 *
 * The interface is introduced in order to ease the use of the ZigBee brick
 * The interface consists of both Types, Functions, Macros and Defines
 *
 * @remarks ALL data belongs to the calling party, meaning that the called party needs to take copy of the data to store
 *       This also means that the called party should NOT free data provided as parameter.\n
 * \n
 *
 * <b>Schneider ZigBee Library Dual Chip Overview</b>
 * \msc
 *
 *  APP  [label="Application", linecolour="#0000ff"],
 *  SZL  [label="SZL", linecolour="#00ff60"],
 *  QU1  [label="SNP Queue", linecolour="#55A0C0"],
 *  UAR  [label="UART", linecolour="#000000"],
 *  QU2  [label="SNP Queue", linecolour="#55A0FF"],
 *  ZBB  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"];
 *
 *  APP  box  QU1 [label="Application CHIP", textbgcolour="#7fff7f"],
 *  QU2  box  ZBB [label="ZigBee CHIP", textbgcolour="#ff7f7f"];
 *  APP  =>   SZL [label="SZL API call."];
 *  SZL  :>   ZBB [label="SNP msg."];
 *  ZBB  ->   NWK [label="ZigBee msg."];
 *  ZBB  <-   NWK [label="ZigBee msg."];
 *  SZL  <:   ZBB [label="SNP msg."];
 *  APP  <<=  SZL [label="SZL API callback."];
 *
 * \endmsc
 *
 * <b>Schneider ZigBee Library Single Chip Overview</b>
 * \msc
 *
 *  APP  [label="Application", linecolour="#0000ff"],
 *  SZL  [label="SZL", linecolour="#00ff60"],
 *  QU1  [label="SNP Queue", linecolour="#55A0C0"],
 *  UAR  [label="UART Wrapper", linecolour="#000000"],
 *  QU2  [label="SNP Queue", linecolour="#55A0FF"],
 *  ZBB  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"];
 *
 *  APP  box  ZBB [label="Single CHIP", textbgcolour="#AC8C30"];
 *  APP  =>   SZL [label="SZL API call."];
 *  SZL  :>   ZBB [label="SNP msg."];
 *  ZBB  ->   NWK [label="ZigBee msg."];
 *  ZBB  <-   NWK [label="ZigBee msg."];
 *  SZL  <:   ZBB [label="SNP msg."];
 *  APP  <<=  SZL [label="SZL API callback."];
 *
 * \endmsc
 */
/*
Copyright (c) 2013 - Schneider-Electric R&D

Schneider - ZigBee - Library. (SZL)
*/

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @ingroup szl_api
 * @defgroup szl_api_def Constants and types
 * This section lists all the types in the API
 * @{
 */

/**
  * This is the SZL APIVersion.
  * The API version is a 24bit integer containing EH_SZL_VERSION_MAJOR EH_SZL_VERSIoN_MINOR EH_SZL_VERSION_MICRO
  */
#define SZLAPIVersion   ((szl_uint24)(  ((szl_uint24)(SZL_API_VERSION_MAJOR[0]-48)) << 20 | \
                                        ((szl_uint24)(SZL_API_VERSION_MAJOR[1]-48)) << 16 | \
                                        ((szl_uint24)(SZL_API_VERSION_MINOR[0]-48)) << 12 | \
                                        ((szl_uint24)(SZL_API_VERSION_MINOR[1]-48)) <<  8 | \
                                        ((szl_uint24)(SZL_API_VERSION_MICRO[0]-48)) <<  4 | \
                                        ((szl_uint24)(SZL_API_VERSION_MICRO[1]-48)) ))


/**
  * The maximum number of supported endpoints.
  */
#define SZL_MAX_SUPPORTED_NUM_ENDPOINTS 6


#if (szl_true != 1)
#error "szl_true must be 1"
#endif

/**
 * SZL_VLA_INIT
 *
 * This is the size at which variable length arrays are initialised.
 * Required as C++ compilers do not like arrays of size 0.
 * Leave it as 0 unless you get compile errors/warnings, in which case set it to 1.
 *
 * Default value: 0
 */
#ifndef SZL_VLA_INIT
    #define SZL_VLA_INIT 1
#endif


// Forward declarations
struct _SZL_BrickInitNtfParams_t;
struct _SZL_CoordinatorStateNtfParams_t;
struct _SZL_AttributeChangedNtfParams_t;
struct _SZL_AttributeReportNtfParams_t;
struct _SZL_NwkStateParams_t;
struct _SZL_NwkJoinNtfParams_t;
struct _SZL_NwkLeaveNtfParams_t;

/**
 * Application type
 *
 * This is the overall application type @ref ENUM_SZL_APPLICATION_t
 */
typedef szl_uint8 SZL_APPLICATION_t;

/**
 * Application type values
 *
 * @ref SZL_APPLICATION_t
 */
typedef enum
{
    SZL_APP_INVALID      = 0x00, /**< (value 0x00) Invalid */

    SZL_APP_SED          = 0x01, /**< (value 0x01) Sleeping End Device */
    SZL_APP_ROUTER       = 0x02, /**< (value 0x02) Router */
    SZL_APP_COORDINATOR  = 0x04, /**< (value 0x04) Coordinator (and router) */

    SZL_APP_TEST         = 0x10, // (value 0x10) Test app
} ENUM_SZL_APPLICATION_t;



/**
 * Endpoint
 *
 * This is the endpoint for a device.<br>
 * A device can have from 1 to n endpoints. <br>
 *
 * Reserved values for Endpoint:<br>
 * 0xFF - This is used to specify ALL endpoints
 */
typedef szl_uint8 SZL_EP_t;
#define SZL_ADDRESS_EP_INVALID              0x00    // End point invalid
#define SZL_ADDRESS_EP_ALL                  0xFF    // End point not specified


/**
 * Result type
 *
 * This is the overall result type for the SZL return value @ref ENUM_SZL_RESULT_t
 */
typedef szl_uint8 SZL_RESULT_t;

/**
 * Result type values
 *
 * @ref SZL_RESULT_t
 */
typedef enum
{
    SZL_RESULT_SUCCESS = 0,                   /**< (value 0x00) Success*/
    SZL_RESULT_FAILED,                        /**< (value 0x01) Undefined error*/
    SZL_RESULT_LIBRARY_NOT_INITIALIZED,       /**< (value 0x02) The Library has non been initialized successfully*/
    SZL_RESULT_UART_NOT_INITIALIZED,          /**< (value 0x03) The Library has non been initialized successfully*/
    SZL_RESULT_DATA_MISSING,                  /**< (value 0x04) Data missing*/
    SZL_RESULT_NO_CALLBACK_REGISTERED,        /**< (value 0x05) No Callback function registered*/
    SZL_RESULT_INVALID_DATA,                  /**< (value 0x06) Invalid Data*/
    SZL_RESULT_INVALID_APPLICATION_TYPE,      /**< (value 0x07) Invalid model provided for application type */
    SZL_RESULT_INVALID_ADDRESS_MODE,          /**< (value 0x08) Invalid address mode provided */
    SZL_RESULT_INVALID_NV_GROUP,              /**< (value 0x09) Invalid NV Group provided */
    SZL_RESULT_INVALID_NV_GROUP_ID,           /**< (value 0x0a) Invalid NV Group ID (maybe out of space in group) */
    SZL_RESULT_API_INVALID_VERSION,           /**< (value 0x0b) API not proper version (call is done with a invalid version of the SZL.h */
    SZL_RESULT_MANDATORY_CB_MISSING,          /**< (value 0x0c) Mandatory callback missing */
    SZL_RESULT_FEATURE_INVALID_FOR_TYPE,      /**< (value 0x0d) The Application type can not perform this action */
    SZL_RESULT_NO_FREE_TID,                   /**< (value 0x0e) No free Transaction ID to be used for request */
    SZL_RESULT_CB_QUEUE_FULL,                 /**< (value 0x0f) No more space in the internal callback queeue */
    SZL_RESULT_QUEUE_FULL,                    /**< (value 0x10) No more space in queue for message */
    SZL_RESULT_TABLE_FULL,                    /**< (value 0x11) No more space in table for data */
    SZL_RESULT_OUT_OF_RANGE,                  /**< (value 0x12) Invalid Data */
    SZL_RESULT_DATA_TOO_BIG,                  /**< (value 0x13) The Data is too big */
    SZL_RESULT_OPERATION_NOT_POSSIBLE,        /**< (value 0x14) The operation can not be performed */
    SZL_RESULT_NOT_FOUND,                     /**< (value 0x15) element not found */
    SZL_RESULT_ALREADY_EXISTS,                /**< (value 0x16) element already exists */
    SZL_RESULT_INTERNAL_LIB_ERROR,            /**< (value 0x17) The logic in the library has failed */
    SZL_RESULT_DP_NOT_FOUND,                  /**< (value 0x18) Datapoint not found/registered */
    SZL_RESULT_DP_NOT_LOADED,                 /**< (value 0x19) Datapoint not loaded from NV yet */
} ENUM_SZL_RESULT_t;

/**
 * Status type
 *
 * This is the overall status type for the SZL status value @ref ENUM_SZL_STATUS_t
 */
typedef szl_uint8 SZL_STATUS_t;

/**
 * Status type values
 *
 * @ref SZL_STATUS_t
 *
 * @remarks  These are returned directly into ZCL frames, DO NOT MODIFY the ZCL compatible codes.
 */
typedef enum
{
    SZL_STATUS_SUCCESS                      = 0x00, /**< (value 0x00) Success */
    SZL_STATUS_FAILED                       = 0x01, /**< (value 0x01) Failure */
    SZL_STATUS_UNSUPPORTED_OPERATION        = 0x02, /**< (value 0x02) Operation unsupported */
    SZL_STATUS_NO_RESPONSE                  = 0x03, /**< (value 0x03) No response recieved */
    SZL_STATUS_MULTI_EH_NETWORK_FAILURE     = 0x0f, /**< (value 0x0f) Network failure*/
    SZL_STATUS_MEM_ERROR                    = 0x10, /**< (value 0x10) mem error*/
    SZL_STATUS_BUFFER_FULL                  = 0x11, /**< (value 0x11) buffer full*/
    SZL_STATUS_UNSUPPORTED_MODE             = 0x12, /**< (value 0x12) Unsupported mode*/
    SZL_STATUS_BRICK_RESETTING              = 0x14, /**< (value 0x14) Brick resetting*/

    SZL_STATUS_IN_PROGRESS                  = 0x20, /**< (value 0x20) In progress*/
    SZL_STATUS_TIMEOUT                      = 0x21, /**< (value 0x21) Timeout*/
    SZL_STATUS_INIT                         = 0x22, /**< (value 0x22) Init*/

    SZL_STATUS_ZGP_TXQUEUE_OVERWRITEN       = 0x30, /**< (value 0x30) A pending GreenPower transmission was overwritten */

    SZL_STATUS_TABLE_FULL                   = 0x6C, /**< (value 0x6c) Table full*/
    SZL_STATUS_NOT_AUTHORIZED               = 0x7E, /**< (value 0x7e) Not authorized*/

    SZL_STATUS_MALFORMED_COMMAND            = 0x80, /**< (value 0x80) Malformed command*/
    SZL_STATUS_UNSUP_CLUSTER_COMMAND        = 0x81, /**< (value 0x81) Unsupported cluster command*/
    SZL_STATUS_UNSUP_GENERAL_COMMAND        = 0x82, /**< (value 0x82) Unsupported general command*/
    SZL_STATUS_UNSUP_MANU_CLUSTER_COMMAND   = 0x83, /**< (value 0x83) Unsupported manufacture cluster command*/
    SZL_STATUS_UNSUP_MANU_GENERAL_COMMAND   = 0x84, /**< (value 0x84) Unsupported manufacture general command*/
    SZL_STATUS_INVALID_FIELD                = 0x85, /**< (value 0x85) Invalid field*/
    SZL_STATUS_UNSUPPORTED_ATTRIBUTE        = 0x86, /**< (value 0x86) Unsupported attribute*/
    SZL_STATUS_INVALID_VALUE                = 0x87, /**< (value 0x87) Invalid value*/
    SZL_STATUS_READ_ONLY                    = 0x88, /**< (value 0x88) Read only*/
    SZL_STATUS_INSUFFICIENT_SPACE           = 0x89, /**< (value 0x89) Insufficient space*/
    SZL_STATUS_DUPLICATE_EXISTS             = 0x8a, /**< (value 0x8a) Duplicate exists*/
    SZL_STATUS_NOT_FOUND                    = 0x8b, /**< (value 0x8b) Not found*/
    SZL_STATUS_UNREPORTABLE_ATTRIBUTE       = 0x8c, /**< (value 0x8c) Unreportable attribute*/
    SZL_STATUS_INVALID_DATA_TYPE            = 0x8d, /**< (value 0x8d) Invalid data type*/
    SZL_STATUS_INVALID_SELECTOR             = 0x8e, /**< (value 0x8e) Invalid selector*/
    SZL_STATUS_WRITE_ONLY                   = 0x8f, /**< (value 0x8f) Write only*/

    SZL_STATUS_INCONSISTENT_STARTUP_STATE   = 0x90, /**< (value 0x90) Inconsistent startup state*/
    SZL_STATUS_DEFINED_OUT_OF_BAND          = 0x91, /**< (value 0x91) Defined out of band*/
    SZL_STATUS_INCONSISTENT                 = 0x92, /**< (value 0x92) Inconsisten*/
    SZL_STATUS_ACTION_DENIED                = 0x93, /**< (value 0x93) Action denied*/
    SZL_STATUS_OTA_TIMEOUT                  = 0x94, /**< (value 0x94) OTA timeout*/
    SZL_STATUS_OTA_ABORT                    = 0x95, /**< (value 0x95) OTA abort*/
    SZL_STATUS_OTA_IMAGE_INVALID            = 0x96, /**< (value 0x96) OTA image invalid*/
    SZL_STATUS_OTA_WAIT_FOR_DATA            = 0x97, /**< (value 0x97) OTA wait for data*/
    SZL_STATUS_OTA_NO_IMAGE_AVAILABLE       = 0x98, /**< (value 0x98) OTA no image available*/
    SZL_STATUS_OTA_REQUIRE_MORE_IMAGE       = 0x99, /**< (value 0x99) OTA require more image*/

    SZL_STATUS_SEC_NO_KEY                   = 0xa1, /**< (value 0xa1) Sec no key*/
    SZL_STATUS_SEC_OLD_FRM_COUNT            = 0xa2, /**< (value 0xa2) Sec old frm count*/
    SZL_STATUS_SEC_MAX_FRM_COUNT            = 0xa3, /**< (value 0xx3) Sec max frm count*/
    SZL_STATUS_SEC_CCM_FAIL                 = 0xa4, /**< (value 0xa4) Sec ccm fail*/

    SZL_STATUS_APS_FAIL                     = 0xb1, /**< (value 0xb1) Aps fail*/
    SZL_STATUS_APS_TABLE_FULL               = 0xb2, /**< (value 0xb2) Aps table full*/
    SZL_STATUS_APS_ILLEGAL_REQUEST          = 0xb3, /**< (value 0xb3) Aps illegal request*/
    SZL_STATUS_APS_INVALID_BINDING          = 0xb4, /**< (value 0xb4) Aps invalid binding*/
    SZL_STATUS_APS_UNSUPPORTED_ATTRIBUTE    = 0xb5, /**< (value 0xb5) Aps unsupported attribute*/
    SZL_STATUS_APS_NOT_SUPPORTED            = 0xb6, /**< (value 0xb6) Aps not supported*/
    SZL_STATUS_APS_NO_ACK                   = 0xb7, /**< (value 0xb7) Aps no ack*/
    SZL_STATUS_APS_DUPLICATED_ENTRY         = 0xb8, /**< (value 0xb8) Aps duplicated entry*/
    SZL_STATUS_APS_NO_BOUND_DEVICE          = 0xb9, /**< (value 0xb9) Aps no bound device*/
    SZL_STATUS_APS_NOT_ALLOWED              = 0xba, /**< (value 0xba) Aps not allowed*/
    SZL_STATUS_APS_NOT_AUTHENTICATED        = 0xbb, /**< (value 0xbb) Aps not authenticated*/

    SZL_STATUS_HARDWARE_FAILURE             = 0xc0, /**< (value 0xc0) Hardware failure*/
    SZL_STATUS_NWK_INVALID_PARAM            = 0xc1, /**< (value 0xc1) Nwk invalid param*/
    SZL_STATUS_NWK_INVALID_REQUEST          = 0xc2, /**< (value 0xc2) Nwk invalid request*/
    SZL_STATUS_NWK_NOT_PERMITTET            = 0xc3, /**< (value 0xc3) Nwk not permittet*/
    SZL_STATUS_NWK_STARTUP_FAILURE          = 0xc4, /**< (value 0xc4) Nwk startup failure*/
    SZL_STATUS_NWK_ALREADY_PRESENT          = 0xc5, /**< (value 0xc5) Nwk already present*/
    SZL_STATUS_NWK_SYNC_FAILURE             = 0xc6, /**< (value 0xc6) Nwk sync failure*/
    SZL_STATUS_NWK_TABLE_FULL               = 0xc7, /**< (value 0xc7) Nwk table full*/
    SZL_STATUS_NWK_UNKNOWN_DEVICE           = 0xc8, /**< (value 0xc8) Nwk unknown device*/
    SZL_STATUS_NWK_UNSUPPORTED_ATTRIBUTES   = 0xc9, /**< (value 0xc9) Nwk unsupported attributes*/
    SZL_STATUS_NWK_NO_NETWORKS              = 0xca, /**< (value 0xc1) Nwk no networks*/
    SZL_STATUS_NWK_LEAVE_UNCONFIRMED        = 0xcb, /**< (value 0xcb) Nwk leave unconfirmed*/
    SZL_STATUS_NWK_NO_ACK                   = 0xcc, /**< (value 0xcc) Nwk no ack*/
    SZL_STATUS_NWK_NO_ROUTE                 = 0xcd, /**< (value 0xcd) Nwk no route*/

    SZL_STATUS_MAC_BEACON_LOSS              = 0xe0, /**< (value 0xe0) Mac beacon loss*/
    SZL_STATUS_MAC_CHANNEL_ACCEPT_FAILURE   = 0xe1, /**< (value 0xe1) Mac channel accept failure*/
    SZL_STATUS_MAC_DENIED                   = 0xe2, /**< (value 0xe2) Mac denied*/
    SZL_STATUS_MAC_DISABLE_TRX_FAILURE      = 0xe3, /**< (value 0xe3) Mac disable trx failure*/
    SZL_STATUS_MAC_FAILED_SECURITY_CHECK    = 0xe4, /**< (value 0xe4) Mac failed security check*/
    SZL_STATUS_MAC_FRAME_TOO_LONG           = 0xe5, /**< (value 0xe5) Mac frame too long*/
    SZL_STATUS_MAC_INVALID_GTS              = 0xe6, /**< (value 0xe6) Mac invalid gts*/
    SZL_STATUS_MAC_INVALID_HANDLE           = 0xe7, /**< (value 0xe7) Mac invalid handle*/
    SZL_STATUS_MAC_INVALID_PARAMETER        = 0xe8, /**< (value 0xe8) Mac invalid parameter*/
    SZL_STATUS_MAC_NO_ACK                   = 0xe9, /**< (value 0xe9) Mac no ack*/
    SZL_STATUS_MAC_NO_BEACON                = 0xea, /**< (value 0xea) Mac no beacon*/
    SZL_STATUS_MAC_NO_DATA                  = 0xeb, /**< (value 0xeb) Mac no data*/
    SZL_STATUS_MAC_NO_SHORT_ADDRESS         = 0xec, /**< (value 0xec) Mac no short address*/
    SZL_STATUS_MAC_OUT_OF_CAP               = 0xed, /**< (value 0xed) Mac out of cap*/
    SZL_STATUS_MAC_PANID_CONFLICT           = 0xee, /**< (value 0xee) Mac panid conflict*/
    SZL_STATUS_MAC_REALIGNMENT              = 0xef, /**< (value 0xef) Mac realignment*/
    SZL_STATUS_MAC_TRANSACTION_EXPIRED      = 0xf0, /**< (value 0xf0) Mac transaction expired*/
    SZL_STATUS_MAC_TRANSACTION_OVERFLOW     = 0xf1, /**< (value 0xf1) Mac transaction overflow*/
    SZL_STATUS_MAC_TX_ACTIVE                = 0xf2, /**< (value 0xf2) Mac tx active*/
    SZL_STATUS_MAC_UNAVAILABLE_KEY          = 0xf3, /**< (value 0xf3) Mac unavailable key*/
    SZL_STATUS_MAC_UNSUPPORTED_ATTRIBUTE    = 0xf4, /**< (value 0xf4) Mac unsupported attribute*/
    SZL_STATUS_MAC_UNSUPPORTED              = 0xf5, /**< (value 0xf5) Mac unsupported*/
    SZL_STATUS_MAC_SRC_MATCH_INVALID_INDEX  = 0xff  /**< (value 0xff) Mac src match invalid index*/

} ENUM_SZL_STATUS_t;



/******************************************************************************
 * ZDO Status
 ******************************************************************************/
/**
 * ZDO Status type
 *
 * This is the ZDO over the air status value @ref ENUM_SZL_ZDO_STATUS_t
 */
typedef szl_uint8 SZL_ZDO_STATUS_t;

/**
 * ZDO Status type values
 *
 * @ref SZL_ZDO_STATUS_t
 */
typedef enum
{
    SZL_ZDO_STATUS_SUCCESS                      = 0x00, /**< Success */

    SZL_ZDO_STATUS_INVALID_REQUEST_TYPE         = 0x80,
    SZL_ZDO_STATUS_DEVICE_NOT_FOUND             = 0x81,
    SZL_ZDO_STATUS_INVALID_EP                   = 0x82,
    SZL_ZDO_STATUS_NOT_ACTIVE                   = 0x83,
    SZL_ZDO_STATUS_NOT_SUPPORTED                = 0x84,
    SZL_ZDO_STATUS_TIMEOUT                      = 0x85,
    SZL_ZDO_STATUS_NO_MATCH                     = 0x86,
    SZL_ZDO_STATUS_NO_ENTRY                     = 0x88,
    SZL_ZDO_STATUS_NO_DESCRIPTOR                = 0x89,
    SZL_ZDO_STATUS_INSUFFICIENT_SPACE           = 0x8a,
    SZL_ZDO_STATUS_NOT_PERMITTED                = 0x8b,
    SZL_ZDO_STATUS_TABLE_FULL                   = 0x8c,
    SZL_ZDO_STATUS_NOT_AUTHORIZED               = 0x8d,
    SZL_ZDO_STATUS_DEVICE_BINDING_TABLE_FULL    = 0x8e
} ENUM_SZL_ZDO_STATUS_t;

/**
 * Version type
 *
 * This is the type for the struct version to be used
 */
typedef szl_uint24 SZL_VERSION_t; //We are only comparing EH_SZL_VERSION_MAJOR | EH_SZL_VERSION_MINOR |EH_SZL_VERSION_MICRO


/**
 * ZigBee data type
 *
 * This is the overall type of the ZigBee data types @ref ENUM_SZL_ZIGBEE_DATA_TYPE_t
 */
typedef szl_uint8 SZL_ZIGBEE_DATA_TYPE_t;

/**
 * ZigBee data type values
 * @ref SZL_ZIGBEE_DATA_TYPE_t
 */
typedef enum
{
    SZL_ZB_DATATYPE_NO_DATA         = 0x00, /**<  (value 0x00) Datasize in bytes: App =   0  Brick =   0 */
    SZL_ZB_DATATYPE_DATA8           = 0x08, /**<  (value 0x08) Datasize in bytes: App =   1  Brick =   1 */
    SZL_ZB_DATATYPE_DATA16          = 0x09, /**<  (value 0x09) Datasize in bytes: App =   2  Brick =   2 */
    SZL_ZB_DATATYPE_DATA24          = 0x0a, /**<  (value 0x0a) Datasize in bytes: App =   4  Brick =   3 */
    SZL_ZB_DATATYPE_DATA32          = 0x0b, /**<  (value 0x0b) Datasize in bytes: App =   4  Brick =   4 */
    SZL_ZB_DATATYPE_DATA40          = 0x0c, /**<  (value 0x0c) Datasize in bytes: App =   8  Brick =   5 */
    SZL_ZB_DATATYPE_DATA48          = 0x0d, /**<  (value 0x0d) Datasize in bytes: App =   8  Brick =   6 */
    SZL_ZB_DATATYPE_DATA56          = 0x0e, /**<  (value 0x0e) Datasize in bytes: App =   8  Brick =   7 */
    SZL_ZB_DATATYPE_DATA64          = 0x0f, /**<  (value 0x0f) Datasize in bytes: App =   8  Brick =   8 */
    SZL_ZB_DATATYPE_BOOLEAN         = 0x10, /**<  (value 0x10) Datasize in bytes: App =   1  Brick =   1 */
    SZL_ZB_DATATYPE_BITMAP8         = 0x18, /**<  (value 0x18) Datasize in bytes: App =   1  Brick =   1 */
    SZL_ZB_DATATYPE_BITMAP16        = 0x19, /**<  (value 0x19) Datasize in bytes: App =   2  Brick =   2 */
    SZL_ZB_DATATYPE_BITMAP24        = 0x1a, /**<  (value 0x1a) Datasize in bytes: App =   4  Brick =   3 */
    SZL_ZB_DATATYPE_BITMAP32        = 0x1b, /**<  (value 0x1b) Datasize in bytes: App =   4  Brick =   4 */
    SZL_ZB_DATATYPE_BITMAP40        = 0x1c, /**<  (value 0x1c) Datasize in bytes: App =   8  Brick =   5 */
    SZL_ZB_DATATYPE_BITMAP48        = 0x1d, /**<  (value 0x1d) Datasize in bytes: App =   8  Brick =   6 */
    SZL_ZB_DATATYPE_BITMAP56        = 0x1e, /**<  (value 0x1e) Datasize in bytes: App =   8  Brick =   7 */
    SZL_ZB_DATATYPE_BITMAP64        = 0x1f, /**<  (value 0x1f) Datasize in bytes: App =   8  Brick =   8 */
    SZL_ZB_DATATYPE_UINT8           = 0x20, /**<  (value 0x20) Datasize in bytes: App =   1  Brick =   1 */
    SZL_ZB_DATATYPE_UINT16          = 0x21, /**<  (value 0x21) Datasize in bytes: App =   2  Brick =   2 */
    SZL_ZB_DATATYPE_UINT24          = 0x22, /**<  (value 0x22) Datasize in bytes: App =   4  Brick =   3 */
    SZL_ZB_DATATYPE_UINT32          = 0x23, /**<  (value 0x23) Datasize in bytes: App =   4  Brick =   4 */
    SZL_ZB_DATATYPE_UINT40          = 0x24, /**<  (value 0x24) Datasize in bytes: App =   8  Brick =   5 */
    SZL_ZB_DATATYPE_UINT48          = 0x25, /**<  (value 0x25) Datasize in bytes: App =   8  Brick =   6 */
    SZL_ZB_DATATYPE_UINT56          = 0x26, /**<  (value 0x26) Datasize in bytes: App =   8  Brick =   7 */
    SZL_ZB_DATATYPE_UINT64          = 0x27, /**<  (value 0x27) Datasize in bytes: App =   8  Brick =   8 */
    SZL_ZB_DATATYPE_INT8            = 0x28, /**<  (value 0x28) Datasize in bytes: App =   1  Brick =   1 */
    SZL_ZB_DATATYPE_INT16           = 0x29, /**<  (value 0x29) Datasize in bytes: App =   2  Brick =   2 */
    SZL_ZB_DATATYPE_INT24           = 0x2a, /**<  (value 0x2a) Datasize in bytes: App =   4  Brick =   3 */
    SZL_ZB_DATATYPE_INT32           = 0x2b, /**<  (value 0x2b) Datasize in bytes: App =   4  Brick =   4 */
    SZL_ZB_DATATYPE_INT40           = 0x2c, /**<  (value 0x2c) Datasize in bytes: App =   8  Brick =   5 */
    SZL_ZB_DATATYPE_INT48           = 0x2d, /**<  (value 0x2d) Datasize in bytes: App =   8  Brick =   6 */
    SZL_ZB_DATATYPE_INT56           = 0x2e, /**<  (value 0x2e) Datasize in bytes: App =   8  Brick =   7 */
    SZL_ZB_DATATYPE_INT64           = 0x2f, /**<  (value 0x2f) Datasize in bytes: App =   8  Brick =   8 */
    SZL_ZB_DATATYPE_ENUM8           = 0x30, /**<  (value 0x30) Datasize in bytes: App =   1  Brick =   1 */
    SZL_ZB_DATATYPE_ENUM16          = 0x31, /**<  (value 0x31) Datasize in bytes: App =   2  Brick =   2 */
    SZL_ZB_DATATYPE_SEMI_PREC       = 0x38, /**<  (value 0x38) Datasize in bytes: App =   2  Brick =   2 */
    SZL_ZB_DATATYPE_SINGLE_PREC     = 0x39, /**<  (value 0x39) Datasize in bytes: App =   4  Brick =   4 */
    SZL_ZB_DATATYPE_DOUBLE_PREC     = 0x3a, /**<  (value 0x3a) Datasize in bytes: App =   8  Brick =   8 */
    SZL_ZB_DATATYPE_OCTET_STR       = 0x41, /**<  (value 0x41) Datasize in bytes: App =   -  Brick =   - */
    SZL_ZB_DATATYPE_CHAR_STR        = 0x42, /**<  (value 0x41) Datasize in bytes: App =   n  Brick =   n <br>@remarks The App is zero terminated, the Brick isn't but the Brich has the length in the first szl_byte */
    SZL_ZB_DATATYPE_LONG_OCTET_STR  = 0x43, /**<  (value 0x43) Datasize in bytes: App =   -  Brick =   - */
    SZL_ZB_DATATYPE_LONG_CHAR_STR   = 0x44, /**<  (value 0x44) Datasize in bytes: App =   -  Brick =   - */
    SZL_ZB_DATATYPE_ARRAY           = 0x48, /**<  (value 0x48) Datasize in bytes: App =   -  Brick =   - */
    SZL_ZB_DATATYPE_STRUCT          = 0x4c, /**<  (value 0x4c) Datasize in bytes: App =   -  Brick =   - */
    SZL_ZB_DATATYPE_SET             = 0x50, /**<  (value 0x50) Datasize in bytes: App =   -  Brick =   - */
    SZL_ZB_DATATYPE_BAG             = 0x51, /**<  (value 0x51) Datasize in bytes: App =   -  Brick =   - */
    SZL_ZB_DATATYPE_TOD             = 0xe0, /**<  (value 0xe0) Datasize in bytes: App =   4  Brick =   4 */
    SZL_ZB_DATATYPE_DATE            = 0xe1, /**<  (value 0xe1) Datasize in bytes: App =   4  Brick =   4 */
    SZL_ZB_DATATYPE_UTC             = 0xe2, /**<  (value 0xe2) Datasize in bytes: App =   4  Brick =   4 */
    SZL_ZB_DATATYPE_CLUSTER_ID      = 0xe8, /**<  (value 0xe8) Datasize in bytes: App =   2  Brick =   2 */
    SZL_ZB_DATATYPE_ATTR_ID         = 0xe9, /**<  (value 0xe9) Datasize in bytes: App =   2  Brick =   2 */
    SZL_ZB_DATATYPE_BAC_OID         = 0xea, /**<  (value 0xea) Datasize in bytes: App =   -  Brick =   - */
    SZL_ZB_DATATYPE_IEEE_ADDR       = 0xf0, /**<  (value 0xf0) Datasize in bytes: App =   8  Brick =   8 */
    SZL_ZB_DATATYPE_128_BIT_SEC_KEY = 0xf1, /**<  (value 0xf1) Datasize in bytes: App =  16  Brick =  16 */
    SZL_ZB_DATATYPE_UNKNOWN         = 0xff, /**<  (value 0xff) Datasize in bytes: App =   -  Brick =   - */
} ENUM_SZL_ZIGBEE_DATA_TYPE_t;


/**
 * Invalid values for some specific data types
 */
#define SZL_ZB_DATATYPE_CHAR_OCTET_STR_INVALID_VALUE 0xFF

/**
 * ZigBee Channel type
 *
 * This is the overall type of the ZigBee channels bit mask @ref ENUM_SZL_ZIGBEE_CHANNELS_t
 */
typedef szl_uint16 SZL_ZIGBEE_CHANNELS_t;

/**
 * ZigBee Channel type values
 * @ref SZL_ZIGBEE_CHANNELS_t
 */
typedef enum
{
    CHANNEL_DEFAULT = 0x0000,   /**< (value 0x0000) Using EH_DEFAULT_CHANNELS or HA_DEFAULT_CHANNELS depending of parameters */
    CHANNEL_11      = 0x0001,   /**< (value 0x0001) Channel 11 */
    CHANNEL_12      = 0x0002,   /**< (value 0x0002) Channel 12 */
    CHANNEL_13      = 0x0004,   /**< (value 0x0004) Channel 13 */
    CHANNEL_14      = 0x0008,   /**< (value 0x0008) Channel 14 */
    CHANNEL_15      = 0x0010,   /**< (value 0x0010) Channel 15 */
    CHANNEL_16      = 0x0020,   /**< (value 0x0020) Channel 16 */
    CHANNEL_17      = 0x0040,   /**< (value 0x0040) Channel 17 */
    CHANNEL_18      = 0x0080,   /**< (value 0x0080) Channel 18 */
    CHANNEL_19      = 0x0100,   /**< (value 0x0100) Channel 19 */
    CHANNEL_20      = 0x0200,   /**< (value 0x0200) Channel 20 */
    CHANNEL_21      = 0x0400,   /**< (value 0x0400) Channel 21 */
    CHANNEL_22      = 0x0800,   /**< (value 0x0800) Channel 22 */
    CHANNEL_23      = 0x1000,   /**< (value 0x1000) Channel 23 */
    CHANNEL_24      = 0x2000,   /**< (value 0x2000) Channel 24 */
    CHANNEL_25      = 0x4000,   /**< (value 0x4000) Channel 25 */
    CHANNEL_26      = 0x8000,   /**< (value 0x8000) Channel 26 */

    CHANNEL_ALL     = 0xFFFF,   /**< All channels */
    EH_DEFAULT_CHANNELS = (CHANNEL_11 | CHANNEL_15 | CHANNEL_20 | CHANNEL_25), /**< Channel 11,15,20 and 25 - default if channel is CHANNEL_DEFAULT and useEHOnly */
    HA_DEFAULT_CHANNELS = (CHANNEL_11 | CHANNEL_14 | CHANNEL_15| CHANNEL_19 | CHANNEL_20 | CHANNEL_24 | CHANNEL_25), /**< Channel 11,14,15,19,20,24 and 25 - default if channel is CHANNEL_DEFAULT and not useEHOnly */
} ENUM_SZL_ZIGBEE_CHANNELS_t;


/**
 * Property Type for the Datapoint
 *
 */
typedef struct
{
    szl_uint16 Method:1;                 /**< ---------------x @ref ENUM_SZL_DP_METHOD_t */
    szl_uint16 Access:2;                 /**< -------------xx- @ref ENUM_SZL_DP_ACCESS_t */
    szl_uint16 ManufacturerSpecific:1;   /**< ------------x--- @ref ENUM_SZL_DP_TYPE_t */
    szl_uint16 Direction:1;              /**< -----------x---- @ref ENUM_SZL_DP_DIRECTION_t */
    szl_uint16 Persistent:1;             /**< ----------x----- @ref ENUM_SZL_DP_PERSISTENT_t */
    szl_uint16 Internal:4;               /**< ------xxxx------ reserved for internal usage (set to zero) */
    szl_uint16 Reserved:6;               /**< xxxxxx---------- reserved (set to zero) */
} SZL_DP_PROPERTY_t;

/**
 * Datapoint method
 * @ref SZL_DP_PROPERTY_t
 */
typedef enum
{
    DP_METHOD_DIRECT    = 0, /**< (value 0x00) Direct access method  */
    DP_METHOD_CALLBACK  = 1, /**< (value 0x01) Callbact access method */
} ENUM_SZL_DP_METHOD_t;

/**
 * Datapoint access
 * @ref SZL_DP_PROPERTY_t
 */
typedef enum
{
    DP_ACCESS_NONE   = 0x00, /**< (value 0x00) NO access */
    DP_ACCESS_READ   = 0x01, /**< (value 0x01) Access Read */
    DP_ACCESS_WRITE  = 0x02, /**< (value 0x02) Access Write */
    DP_ACCESS_RW     = (DP_ACCESS_READ | DP_ACCESS_WRITE) /**< (value 0x03) Access Read & Write */
} ENUM_SZL_DP_ACCESS_t;

/**
 * Datapoint type
 * @ref SZL_DP_PROPERTY_t
 */
typedef enum
{
    DP_TYPE_ZB  = 0, /**< (value 0) ZigBee type */
    DP_TYPE_MS  = 1, /**< (value 1) Manufacturer Specific */
} ENUM_SZL_DP_TYPE_t;

/**
 * Datapoint Direction
 * @ref SZL_DP_PROPERTY_t
 */
typedef enum
{
    DP_DIRECTION_SERVER = 0,    /**< (value 0) The attribute is SERVER side */
    DP_DIRECTION_CLIENT = 1,    /**< (value 1) The attribute is CLIENT side */
} ENUM_SZL_DP_DIRECTION_t;

/**
 * Datapoint Persistent
 * @ref SZL_DP_PROPERTY_t
 */
typedef enum
{
    DP_PERSISTENT_NO  = 0,    /**< (value 0) The attribute isn't persistent */
    DP_PERSISTENT_YES = 1,    /**< (value 1) The attribute is persistent and the library will do the load/save to NV when necessary */
} ENUM_SZL_DP_PERSISTENT_t;

/**
 * Operation Type for the Cluster Command
 *
 * This is the overall type of the Operation type @ref ENUM_SZL_CLU_OP_TYPE_t
 */
typedef szl_uint8 SZL_CLU_OP_TYPE_t;

/**
 * Operation Type values
 * @ref SZL_CLU_OP_TYPE_t
 */
typedef enum
{
    SZL_CLU_OP_TYPE_MIN_        = 0x01,
    SZL_CLU_OP_TYPE_INC         = 0x01, /**< The Default INC (value 0x01) operation will be performed on the associated attribute by the library */
    SZL_CLU_OP_TYPE_DEC         = 0x02, /**< The Default DEC (value 0x02) operation will be performed on the associated attribute by the library */
    SZL_CLU_OP_TYPE_SET         = 0x03, /**< The Default SET (value 0x03) operation will be performed on the associated attribute by the library */
    SZL_CLU_OP_TYPE_GET         = 0x04, /**< The Default GET (value 0x04) operation will be performed on the associated attribute by the library */
    SZL_CLU_OP_TYPE_ON          = 0x05, /**< The Default ON  (value 0x05) operation will be performed on the associated attribute by the library */
    SZL_CLU_OP_TYPE_OFF         = 0x06, /**< The Default OFF (value 0x06) operation will be performed on the associated attribute by the library */
    SZL_CLU_OP_TYPE_TOGGLE      = 0x07, /**< The Default TOGGLE (value 0x07) operation will be performed on the associated attribute by the library */
    SZL_CLU_OP_TYPE_MAX_        = 0x07,

    SZL_CLU_OP_TYPE_CUSTOM_CLIENT_TO_SERVER = 0xFE, /**< The CUSTOM_CLIENT_TO_SERVER (value 0xfe) Callback function will be called for this client to server command and be handled by the function */
    SZL_CLU_OP_TYPE_CUSTOM_SERVER_TO_CLIENT = 0xFF, /**< The CUSTOM_SERVER_TO_CLIENT (value 0xff) Callback function will be called for this server to client response and the function will take care of the response */
} ENUM_SZL_CLU_OP_TYPE_t;

/**
 * @ingroup szl_api_def
 * @defgroup szl_api_def_dp Datapoint property pre definition
 * This section lists all the Datapoint property pre definition
 * for the @ref SZL_DP_PROPERTY_t
 * @{
 */

// DIRECT ACCESS of ZIGBEE DP
#define DP_PROP_SE_DI_ZB_RO     {.Method=DP_METHOD_DIRECT,   .Access=DP_ACCESS_READ, .ManufacturerSpecific=DP_TYPE_ZB,   .Persistent=DP_PERSISTENT_NO,   .Direction=DP_DIRECTION_SERVER} /**< Server - ZigBee - Direct access - Read Only - not persistent */
#define DP_PROP_SE_DI_ZB_RO_P   {.Method=DP_METHOD_DIRECT,   .Access=DP_ACCESS_READ, .ManufacturerSpecific=DP_TYPE_ZB,   .Persistent=DP_PERSISTENT_YES,  .Direction=DP_DIRECTION_SERVER} /**< Server - ZigBee - Direct access - Read Only - persistent */
#define DP_PROP_SE_DI_ZB_RW     {.Method=DP_METHOD_DIRECT,   .Access=DP_ACCESS_RW,   .ManufacturerSpecific=DP_TYPE_ZB,   .Persistent=DP_PERSISTENT_NO,   .Direction=DP_DIRECTION_SERVER} /**< Server - ZigBee - Direct access - Read Write - not persistent */
#define DP_PROP_SE_DI_ZB_RW_P   {.Method=DP_METHOD_DIRECT,   .Access=DP_ACCESS_RW,   .ManufacturerSpecific=DP_TYPE_ZB,   .Persistent=DP_PERSISTENT_YES,  .Direction=DP_DIRECTION_SERVER} /**< Server - ZigBee - Direct access - Read Write - persistent */
// DIRECT ACCESS of MANUFACTURE DP
#define DP_PROP_SE_DI_MS_RO     {.Method=DP_METHOD_DIRECT,   .Access=DP_ACCESS_READ, .ManufacturerSpecific=DP_TYPE_MS,   .Persistent=DP_PERSISTENT_NO,   .Direction=DP_DIRECTION_SERVER} /**< Server - Manifacturer Specific - Direct access - Read Only - not persistent */
#define DP_PROP_SE_DI_MS_RO_P   {.Method=DP_METHOD_DIRECT,   .Access=DP_ACCESS_READ, .ManufacturerSpecific=DP_TYPE_MS,   .Persistent=DP_PERSISTENT_YES,  .Direction=DP_DIRECTION_SERVER} /**< Server - Manifacturer Specific - Direct access - Read Only - persistent */
#define DP_PROP_SE_DI_MS_RW     {.Method=DP_METHOD_DIRECT,   .Access=DP_ACCESS_RW,   .ManufacturerSpecific=DP_TYPE_MS,   .Persistent=DP_PERSISTENT_NO,   .Direction=DP_DIRECTION_SERVER} /**< Server - Manifacturer Specific - Direct access - Read Write - not persistent */
#define DP_PROP_SE_DI_MS_RW_P   {.Method=DP_METHOD_DIRECT,   .Access=DP_ACCESS_RW,   .ManufacturerSpecific=DP_TYPE_MS,   .Persistent=DP_PERSISTENT_YES,  .Direction=DP_DIRECTION_SERVER} /**< Server - Manifacturer Specific - Direct access - Read Write - persistent */
// CALLBACK ACCESS of ZIGBEE DP
#define DP_PROP_SE_CB_ZB_RO     {.Method=DP_METHOD_CALLBACK, .Access=DP_ACCESS_READ, .ManufacturerSpecific=DP_TYPE_ZB,   .Persistent=DP_PERSISTENT_NO,   .Direction=DP_DIRECTION_SERVER} /**< Server - ZigBee - Callback access - Read Only - not persistent */
#define DP_PROP_SE_CB_ZB_RW     {.Method=DP_METHOD_CALLBACK, .Access=DP_ACCESS_RW,   .ManufacturerSpecific=DP_TYPE_ZB,   .Persistent=DP_PERSISTENT_NO,   .Direction=DP_DIRECTION_SERVER} /**< Server - ZigBee - Callback access - Read Write - not persistent */
#define DP_PROP_SE_CB_ZB_RW_P   {.Method=DP_METHOD_CALLBACK, .Access=DP_ACCESS_RW,   .ManufacturerSpecific=DP_TYPE_ZB,   .Persistent=DP_PERSISTENT_YES,  .Direction=DP_DIRECTION_SERVER} /**< Server - ZigBee - Callback access - Read Write - persistent */
// CALLBACK ACCESS of MANUFACTURE DP
#define DP_PROP_SE_CB_MS_RO     {.Method=DP_METHOD_CALLBACK, .Access=DP_ACCESS_READ, .ManufacturerSpecific=DP_TYPE_MS,   .Persistent=DP_PERSISTENT_NO,   .Direction=DP_DIRECTION_SERVER} /**< Server - Manifacturer Specific - Callback access - Read Only - not persistent */
#define DP_PROP_SE_CB_MS_RW     {.Method=DP_METHOD_CALLBACK, .Access=DP_ACCESS_RW,   .ManufacturerSpecific=DP_TYPE_MS,   .Persistent=DP_PERSISTENT_NO,   .Direction=DP_DIRECTION_SERVER} /**< Server - Manifacturer Specific - Callback access - Read Write - not persistent */
#define DP_PROP_SE_CB_MS_RW_P   {.Method=DP_METHOD_CALLBACK, .Access=DP_ACCESS_RW,   .ManufacturerSpecific=DP_TYPE_MS,   .Persistent=DP_PERSISTENT_YES,  .Direction=DP_DIRECTION_SERVER} /**< Server - Manifacturer Specific - Callback access - Read Write - persistent */

/**
 * @}
 */





/**
 * @}
 */
// end of defgroup szl_api_def Constants and types

/**
 * Address Mode
 *
 * This is the address mode used.<br>
 */
typedef enum
{
    SZL_ADDRESS_MODE_INVALID = 0,   /**< The address mode has not been set to a valide mode */
    SZL_ADDRESS_MODE_VIA_BIND,      /**< Using binding table. All fields are ignored. */
    SZL_ADDRESS_MODE_GROUP,         /**< Using the Group Address field. All other fields ignored. */
    SZL_ADDRESS_MODE_NWK_ADDRESS,   /**< using the Nwk Address and the Endpoint field, All other fields ignored. */
    SZL_ADDRESS_MODE_IEEE_ADDRESS,  /**< using the IEEE Address and the Endpoint field, All other fields ignored. */
    SZL_ADDRESS_MODE_BROADCAST,     /**< using the Broadcast type, All other fielsd are ignored */
    SZL_ADDRESS_MODE_GP_SOURCE_ID,  /**< Green Power Source Id */
} SZL_ADDRESS_MODE_t;


typedef enum
{
    SZL_BROADCAST_ALL,
    SZL_BROADCAST_RX_ON_WHEN_IDLE,
    SZL_BROADCAST_ROUTERS_AND_COORDINATOR,
    SZL_BROADCAST_LOW_POWER_ROUTERS
} SZL_ADDR_BROADCAST_MODE_t;

typedef struct
{
    SZL_ADDR_BROADCAST_MODE_t Mode;
} SZL_ADDR_BROADCAST_t;


/**
 * Bind Address mode
 */
typedef struct
{
    szl_uint8 Reserved;     /**< Reserved */
} SZL_ADDR_BIND_t;

/**
 * Group Address mode
 */
typedef struct
{
    szl_uint16 Address;     /**< Group address */
} SZL_ADDR_GROUP_t;

/**
 * Network Address mode
 */
typedef struct
{
    szl_uint16 Address;     /**< The short address for a device also called the Network address */
    SZL_EP_t Endpoint;      /**< The Endpoint for the device */
} SZL_ADDR_NWK_t;

/**
 * IEEE Address mode
 */
typedef struct
{
    szl_uint64 Address;     /**< The IEEE address for a device also known as the Extended address */
    SZL_EP_t Endpoint;      /**< The Endpoint for the device */
} SZL_ADDR_IEEE_t;

/**
 * Green Power Device - Source Id
 */
typedef struct
{
    szl_uint32 SourceId;    /**< The Source ID of the Green Power Device */
} SZL_ADDR_GP_SOURCE_ID_t ;

/**
 * Type for DeviceAddresses.
 * This struct holds addresses for a device.
 */
typedef struct
{
    SZL_ADDRESS_MODE_t AddressMode;    /**< The address mode */
    union {
        SZL_ADDR_BIND_t Bind;
        SZL_ADDR_GROUP_t Group;
        SZL_ADDR_NWK_t Nwk;
        SZL_ADDR_IEEE_t IEEE;
        SZL_ADDR_BROADCAST_t Broadcast;
        SZL_ADDR_GP_SOURCE_ID_t GpSourceId; // Green Power Device using Source ID only.
    } Addresses;
} SZL_Addresses_t;


/**
 * @ingroup szl_api
 * @defgroup szl_api_func Common Functions
 * This section defines all the Common API functions implemented in the SZLibrary for the App to call.
 * @{
 */
// First group

//************************************************************************************************
// New common

/**
 *
 * DataPoint read.
 *
 * This is a Callback function used by SZL in order to read value from a datapoint (attribute) @ref SZL_DataPoint_t
 * @remarks For native C strings the data ONLY contains the payload and NOT the zero termination e.g. "Hello" have data length of 5.
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]     Service       (@ref zabService*) Server Instance Pointer
 * @param[in]     ClusterID     (szl_uint16) ClusterID for the datapoint
 * @param[in]     AttributeID   (szl_uint16) AttributeID for the datapoint
 * @param[in]     Endpoint      (szl_uint8)  The endpoint for the datapoint (APP)
 * @param[in,out] Data          (void*)  The buffer to store the read data in
 * @param[in,out] DataLengthInOut (szl_uint16) In: Max number of bytes that may be read to Data
 *                                         Out: The number of bytes read
 *
 * @return @ref SZL_RESULT_t
 */
typedef SZL_RESULT_t (*SZL_CB_DataPointRead_t) (zabService* Service, SZL_EP_t Endpoint, szl_bool ManufacturerSpecific, szl_uint16 ClusterID, szl_uint16 AttributeID, void* Data, szl_uint8* DataLengthInOut);

/**
 *
 * DataPoint Write.
 *
 * This is a OPTIONAL Callback function used by SZL in order to write value to a datapoint (attribute) @ref SZL_DataPoint_t <br>
 * If the callback isn't provided for a datapoint, the datapoint will be read-only.
 * @remarks For native C strings the data ONLY contains the payload and NOT the zero termination e.g. "Hello" have data length of 5.
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]     Service           (@ref zabService*) Server Instance Pointer
 * @param[in]     ClusterID         (szl_uint16) ClusterID for the datapoint
 * @param[in]     AttributeID       (szl_uint16) AttributeID for the datapoint
 * @param[in]     Endpoint          (szl_uint8)  Endpoint for the datapoint (APP)
 * @param[in]     Data              (void*)  The buffer with the data to write
 * @param[in]     DataLength        (szl_uint16) The number of bytes to write
 *
 * @return @ref SZL_RESULT_t
 */
typedef SZL_RESULT_t (*SZL_CB_DataPointWrite_t) (zabService* Service, SZL_EP_t Endpoint, szl_bool ManufacturerSpecific, szl_uint16 ClusterID, szl_uint16 AttributeID, void* Data, szl_uint8 DataLength);

/**
 *
 * Cluster Cmd Handler.
 *
 * This is a Callback function used by SZL to let the application handle Cluster Commands. @ref SZL_ClusterCmd_t <br>
 * @remarks The same callback format is used for both ClientToServer and ServerToClient commands.
 * @remarks All the data is in LITTLE ENDIANNES
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service              (@ref zabService*) Server Instance Pointer
 * @param[in]  ClusterID            The clusterID for the command
 * @param[in]  CmdID                The commandID
 * @param[in]  ZCLPayloadIn         The pointer to the payload from the command (The data is in LITTLE ENDIANNESS)
 * @param[in]  PayloadInSize        The length of the payload for the command
 * @param[out] CmdOutID             The commandID to be send together with the response payload (ignore if no payload to send)
 * @param[out] ZCLPayloadOut        The pointer to the payload buffer to copy the response payload to (The data is in LITTLE ENDIANNESS) (ignore if no payload to send)
 * @param[in/out] PayloadOutSize    The length of the response payload (set to zero if no response payload).
 *                                   - On handler call *PayloadOutSize gives the maximum length response the application may provide.
 *                                     Application MUST respect this to avoid failed commands and memory corruption!
 * @param[in]  TransactionId        The Transaction ID of this command. May be used to match response to request.
 * @param[in]  SourceAddress        The source of the command
 * @param[in]  DestAddressMode      The destination addressing via which the command was delivered
 *
 * @return -   CommandValid         szl_true if command was handled successfully, otherwise szl_false.
 */
typedef szl_bool (*SZL_CB_ClusterCmd_t) (zabService* Service,
                                         SZL_EP_t DestinationEndpoint,
                                         szl_bool ManufacturerSpecific,
                                         szl_uint16 ClusterID,
                                         szl_uint8 CmdID,
                                         szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize,
                                         szl_uint8* CmdOutID,
                                         szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize,
                                         szl_uint8 TransactionId,
                                         SZL_Addresses_t SourceAddress,
                                         SZL_ADDRESS_MODE_t DestAddressMode);
/**
 * @}
 */
// end of defgroup szl_api_func Common Functions













































































































//************************************************************************************************





/**
 * @ingroup szl_api_def
 * @defgroup szl_api_str Structs
 * This section list all the different structures defined for the API
 * @{
 */




/**
 * Type for AttributeData.
 * This struct holds the definition for a attribute data value.
 */
typedef struct
{
    szl_uint16 AttributeID;                 /**< AttributeID for the Attribute */
    SZL_STATUS_t Status;                    /**< The Status for the attribute - If NOT SUCCESS none of the following data is valid */
    SZL_ZIGBEE_DATA_TYPE_t DataType;        /**< The data type for the Attribute - if type is not present then set to SZL_ZB_DATATYPE_UNKNOWN */
    szl_uint8 DataLength;                   /**< The length of the data in bytes - if 0 then no data is available and Data should be null*/
    void* Data;                             /**< pointer to the data - @note all data will be in native endianness and in App data type */
} SZL_AttributeData_t;

/**
 * Type for SZL_AttributeDataType_t.
 * This struct holds the data type and ID for a attribute data.
 */
typedef struct
{
    szl_uint16 AttributeID;                 /**< AttributeID for the Attribute */
    SZL_ZIGBEE_DATA_TYPE_t DataType;        /**< The data type for the attribute */
} SZL_AttributeDataType_t;

/**
 * Type for AttributeStatus.
 * This struct holds the definition for a attribute status.
 */
typedef struct
{
    szl_uint16 AttributeID;                 /**< AttributeID for the Attribute */
    SZL_STATUS_t Status;                    /**< The Status for the attribute */
} SZL_AttributeStatus_t;

/**
 * Type for AttributeReportData.
 * This struct holds the definition for a attribute report data.
 */
typedef struct
{
    szl_uint16 AttributeID;                                         /**< AttributeID for the Attribute */
    SZL_STATUS_t Status;                                            /**< The Status for the attribute - If NOT SUCCESS none of the following data is valid */
    SZL_ZIGBEE_DATA_TYPE_t DataType;                                /**< The data type for the attribute */
    szl_uint16 MinInterval;                                         /**< Min Reporting Interval: 0 means the report can be sent anytime */
    szl_uint16 MaxInterval;                                         /**< Max Reporting Interval: if 0xFFFF, means stop reporting */
    szl_uint8  LenRC;                                               /**< The Native data length of the ReportableChangeData field */
    void* ReportableChangeData;                                     /**< Pointer to the Reportable Change data - @note all data will be in native endianness and in App data type */
} SZL_AttributeReportData_t;

/**
 * Type for AttributeDiscoverReqParams.
 * This struct holds the information for which attributes to discover.
 */
typedef struct
{
    SZL_EP_t SourceEndpoint;            /**< The Endpoint for the source (App) */
    SZL_Addresses_t DestAddrMode;       /**< The Addressing mode to specify how the device communicates with the destination, either via binding table or direct addressing */
    szl_bool ManufacturerSpecific;      /**< This is set to szl_true if manufacturer specific attributes should be discovered, else set to szl_false */
    szl_uint16 ClusterID;               /**< The Cluster ID to discover the attributes from */
    szl_uint16 StartAttributeID;        /**< The Start attribute ID indicates that the attribute discovered must be equal or greater */
    szl_uint8 MaxAttributes;            /**< The maximum number of attributes to return */
} SZL_AttributeDiscoverReqParams_t;

/**
 * Type for AttributeDiscoverRespParams.
 * This struct holds the information for the attributes discovered.
 */
typedef struct
{
    SZL_EP_t DestinationEndpoint;                       /**< The Endpoint for the Destination (App) */
    SZL_Addresses_t SourceAddress;                      /**< The address of the source (remote device). */
    szl_bool ManufacturerSpecific;                      /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint16 ClusterID;                               /**< The Cluster ID for attributes */
    szl_bool ListComplete;                              /**< True if the list is complete, otherwise szl_false */
    szl_uint8 NumberOfAttributes;                       /**< The number of discovered attributes in the following Attributes array */
    SZL_AttributeDataType_t Attributes[SZL_VLA_INIT];   /**< Attribute's - (extends beyond bounds) */
} SZL_AttributeDiscoverRespParams_t;
#define SZL_AttributeDiscoverRespParams_t_SIZE(_num_attributes) (sizeof(SZL_AttributeDiscoverRespParams_t) + (sizeof(SZL_AttributeDataType_t) * (_num_attributes)) - (sizeof(SZL_AttributeDataType_t) * SZL_VLA_INIT))

/**
 * Type for AttributeReadReqParams.
 * This struct holds the information for the attribute to read.
 */
typedef struct
{
    SZL_EP_t SourceEndpoint;                    /**< The Endpoint for the source (App) */
    SZL_Addresses_t DestAddrMode;               /**< The Addressing mode to specify how the device communicates with the destination, either via binding table or direct addressing */
    szl_bool ManufacturerSpecific;              /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint16 ClusterID;                       /**< The Cluster ID that has been bound */
    szl_uint8 NumberOfAttributes;               /**< The number of attribute ID's in the following Attribute ID's array */
    szl_uint16 AttributeIDs[SZL_VLA_INIT];      /**< Attribute ID's - (extends beyond bounds) */
} SZL_AttributeReadReqParams_t;
#define SZL_AttributeReadReqParams_t_SIZE(_num_attributes) (sizeof(SZL_AttributeReadReqParams_t) + (sizeof(szl_uint16) * (_num_attributes)) - (sizeof(szl_uint16) * SZL_VLA_INIT))

/**
 * Type for AttributeReadRespParams.
 * This struct holds the information for the read attributes.
 */
typedef struct
{
    SZL_EP_t DestinationEndpoint;                       /**< The Endpoint for the Destination (App) */
    SZL_Addresses_t SourceAddress;                      /**< The address of the source (remote device). */
    szl_bool ManufacturerSpecific;                      /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint16 ClusterID;                               /**< The Cluster ID that has been bound */
    szl_uint8 NumberOfAttributes;                       /**< The number of attributes in the following Attributes array */
    SZL_AttributeData_t Attributes[SZL_VLA_INIT];       /**< Attributes - (extends beyond bounds) */
} SZL_AttributeReadRespParams_t;
#define SZL_AttributeReadRespParams_t_SIZE(_num_attributes) (sizeof(SZL_AttributeReadRespParams_t) + (sizeof(SZL_AttributeData_t) * (_num_attributes)) - (sizeof(SZL_AttributeData_t) * SZL_VLA_INIT))

/**
 * Type for AttributeWriteReqParams.
 * This struct holds the information for the attribute to write.
 */
typedef struct
{
    SZL_EP_t SourceEndpoint;                            /**< The Endpoint for the source (App) */
    SZL_Addresses_t DestAddrMode;                       /**< The Addressing mode to specify how the device communicates with the destination, either via binding table or direct addressing */
    szl_bool ManufacturerSpecific;                      /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint16 ClusterID;                               /**< The Cluster ID that has been bound */
    szl_uint8 NumberOfAttributes;                       /**< The number of attributes in the following Attributes array */
    SZL_AttributeData_t Attributes[SZL_VLA_INIT];       /**< Attributes - (extends beyond bounds) */
} SZL_AttributeWriteReqParams_t;
#define SZL_AttributeWriteReqParams_t_SIZE(_num_attributes) (sizeof(SZL_AttributeWriteReqParams_t) + (sizeof(SZL_AttributeData_t) * (_num_attributes)) - (sizeof(SZL_AttributeData_t) * SZL_VLA_INIT))

/**
 * Type for AttributeChangedIndParams.
 * This struct holds the information for the attribute changed.
 */
typedef struct
{
    SZL_EP_t Endpoint;              /**< The Endpoint for attribute */
    szl_bool ManufacturerSpecific;  /**< The attribute is Manufacturer Specific */
    szl_uint16 ClusterID;           /**< The Cluster ID for the attribute */
    szl_uint16 AttributeID;         /**< ID for the Attribute */
} SZL_AttributeChangedIndParams_t;

/**
 * Type for AttributeChangedNtfParams.
 * This struct holds the information for the attribute changed.
 */
typedef struct _SZL_AttributeChangedNtfParams_t
{
    SZL_EP_t Endpoint;              /**< The Endpoint for attribute */
    szl_bool ManufacturerSpecific;  /**< The attribute is Manufacturer Specific */
    szl_uint16 ClusterID;           /**< The Cluster ID for the attribute */
    szl_uint16 AttributeID;         /**< ID for the Attribute */
} SZL_AttributeChangedNtfParams_t;

/**
 * Type for AttributeWriteRespParams.
 * This struct holds the result for the attribute write.
 */
typedef struct
{
    SZL_EP_t DestinationEndpoint;                      /**< The Endpoint for the Destination (App) */
    SZL_Addresses_t SourceAddress;                     /**< The address of the source (remote device). */
    szl_bool ManufacturerSpecific;                     /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint16 ClusterID;                              /**< The Cluster ID that has been bound */
    szl_uint8 NumberOfAttributes;                      /**< The number of items in the following Attributes array */
    SZL_AttributeStatus_t Attributes[SZL_VLA_INIT];    /**< Attribute ID's - (extends beyond bounds) */
} SZL_AttributeWriteRespParams_t;
#define SZL_AttributeWriteRespParams_t_SIZE(_num_failed) (sizeof(SZL_AttributeWriteRespParams_t) + (sizeof(SZL_AttributeStatus_t) * (_num_failed)) - (sizeof(SZL_AttributeStatus_t) * SZL_VLA_INIT))

/**
 * Type for Attribute Report Notification parameters.
 * This struct holds the parameters given on a notification for a attribute report  @see SZL_AttributeReportNtf
 */
typedef struct _SZL_AttributeReportNtfParams_t
{
    SZL_EP_t DestinationEndpoint;                   /**< The Endpoint for the Destination (App) */
    SZL_Addresses_t SourceAddress;                  /**< The address of the source (remote device). */
    szl_bool ManufacturerSpecific;                  /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint16 ClusterID;                           /**< The Cluster ID for attributes */
    szl_uint8 NumberOfAttributes;                   /**< The number of attributes in the following Attributes array */
    SZL_AttributeData_t Attributes[SZL_VLA_INIT];   /**< Attribute's - (extends beyond bounds) */
} SZL_AttributeReportNtfParams_t;
#define SZL_AttributeReportNtfParams_t_SIZE(_num_attributes) (sizeof(SZL_AttributeReportNtfParams_t) + (sizeof(SZL_AttributeData_t) * (_num_attributes)) - (sizeof(SZL_AttributeData_t) * SZL_VLA_INIT))

/**
 * Type for AttributeWriteReqParams.
 * This struct holds the information for the attribute to write.
 */
typedef struct
{
    SZL_EP_t SourceEndpoint;                        /**< The Endpoint for the source (App) */
    SZL_Addresses_t DestAddrMode;                   /**< The Addressing mode to specify how the device communicates with the destination, either via binding table or direct addressing */
    szl_bool ManufacturerSpecific;                  /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint16 ClusterID;                           /**< The Cluster ID that has been bound */
    szl_uint8 FrameType;                            /**< The frame type of the tunneled command */
    szl_uint8 Direction;                            /**< The direction of the command (0 = client->server, 1 = server->client) */
    szl_uint8 DisableDefaultResponse;               /**< Disables the default response being sent in cases of successful receipt of command */
    szl_uint8 CommandType;                          /**< Specifies the type of profile/cluster command being used */
    szl_uint8 PayloadSize;                          /**< The size of the following Payload */
    szl_uint8 Payload[SZL_VLA_INIT];                /**< Payload - (extends beyond bounds) */
} SZL_ZdoZclTunnelReqParams_t;
#define SZL_ZdoZclTunnelReqParams_t_SIZE(_size_payload) (sizeof(SZL_ZdoZclTunnelReqParams_t) + (sizeof(szl_uint8) * (_size_payload)) - (sizeof(szl_uint8) * SZL_VLA_INIT))

/**
 * Type for AttributeReportCfgReqParams.
 * This struct holds the information for the report configuration.
 */
typedef struct
{
    SZL_EP_t SourceEndpoint;                            /**< The Endpoint for the source (App) */
    SZL_Addresses_t DestAddrMode;                       /**< The Addressing mode to specify how the device communicates with the destination, either via binding table or direct addressing */
    szl_bool ManufacturerSpecific;                      /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint16 ClusterID;                               /**< The Cluster ID  */
    szl_uint8 NumberOfAttributes;                       /**< The number of attributes in the following Attributes array */
    SZL_AttributeReportData_t Attributes[SZL_VLA_INIT]; /**< Attributes - (extends beyond bounds) */
}SZL_AttributeReportCfgReqParams_t;
#define SZL_AttributeReportCfgReqParams_t_SIZE(_num_attributes) (sizeof(SZL_AttributeReportCfgReqParams_t) + (sizeof(SZL_AttributeReportData_t) * (_num_attributes)) - (sizeof(SZL_AttributeReportData_t) * SZL_VLA_INIT))

/**
 * Type for AttributeReportCfgRespParams.
 * This struct holds the response for the report configuration.
 */
typedef struct
{
    SZL_EP_t DestinationEndpoint;                   /**< The Endpoint for the Destination (App) */
    SZL_Addresses_t SourceAddress;                  /**< The address of the source (remote device). */
    szl_bool ManufacturerSpecific;                  /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint16 ClusterID;                           /**< The Cluster ID that has been bound */
    szl_uint8 NumberOfAttributes;                   /**< The number of attributes in the following Attributes array */
    SZL_AttributeStatus_t Attributes[SZL_VLA_INIT]; /**< Attribute ID's - (extends beyond bounds) */
}SZL_AttributeReportCfgRespParams_t;
#define SZL_AttributeReportCfgRespParams_t_SIZE(_num_attributes) (sizeof(SZL_AttributeReportCfgRespParams_t) + (sizeof(SZL_AttributeStatus_t) * (_num_attributes)) - (sizeof(SZL_AttributeStatus_t) * SZL_VLA_INIT))


/**
 * Type for AttributeReadReportCfgReqParams.
 * This struct holds the data for the report configuration.
 */
typedef struct
{
    SZL_EP_t SourceEndpoint;                        /**< The Endpoint for the source (App) */
    SZL_Addresses_t DestAddrMode;                   /**< The Addressing mode to specify how the device communicates with the destination, either via binding table or direct addressing */
    szl_bool ManufacturerSpecific;                  /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint16 ClusterID;                           /**< The Cluster ID that has been bound */
    szl_uint8 NumberOfAttributes;                   /**< The number of attributes in the following Attributes array */
    szl_uint16 Attributes[SZL_VLA_INIT];            /**< Attribute ID's - (extends beyond bounds) */
}SZL_AttributeReadReportCfgReqParams_t;
#define SZL_AttributeReadReportCfgReqParams_t_SIZE(_num_attributes) (sizeof(SZL_AttributeReadReportCfgReqParams_t) + (sizeof(szl_uint16) * (_num_attributes)) - (sizeof(szl_uint16) * SZL_VLA_INIT))

/**
 * Type for AttributeReadReportCfgRespParams.
 * This struct holds the information for the read report configuration response.
 */
typedef struct
{
    SZL_EP_t DestinationEndpoint;                       /**< The Endpoint for the Destination (App) */
    SZL_Addresses_t SourceAddress;                      /**< The address of the source (remote device). */
    szl_bool ManufacturerSpecific;                      /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint16 ClusterID;                               /**< The Cluster ID  */
    szl_uint8 NumberOfAttributes;                       /**< The number of attributes in the following Attributes array */
    SZL_AttributeReportData_t Attributes[SZL_VLA_INIT]; /**< Attributes - (extends beyond bounds) */
}SZL_AttributeReadReportCfgRespParams_t;
#define SZL_AttributeReadReportCfgRespParams_t_SIZE(_num_attributes) (sizeof(SZL_AttributeReadReportCfgRespParams_t) + (sizeof(SZL_AttributeReportData_t) * (_num_attributes)) - (sizeof(SZL_AttributeReportData_t) * SZL_VLA_INIT))

/**
 * Type for ClusterCmdReqParams.
 * This struct holds the information for the Cluster Command.
 */
typedef struct
{
    SZL_EP_t SourceEndpoint;            /**< The Endpoint for the source (App) */
    SZL_Addresses_t DestAddrMode;       /**< The Addressing mode to specify how the device communicates with the destination, either via binding table or direct addressing */
    szl_bool ManufacturerSpecific;      /**< This is set to szl_true if the command is manufacturer specific, else set to szl_false */
    szl_uint16 ClusterID;               /**< The Cluster ID that has been bound */
    szl_uint8 Command;                  /**< The cluster Command */
    szl_uint8 Direction;                /**< Direction of the command, whether ClientToServer (HC to devices) or ServerToClient (Devices to HC) */
    szl_uint8 PayloadLength;            /**< The length of the payload to follow */
    szl_uint8 Payload[SZL_VLA_INIT];    /**< The ZCL formatted payload in LITTLE ENDIAN - (extends beyond bounds) */
} SZL_ClusterCmdReqParams_t;
#define SZL_ClusterCmdReqParams_t_SIZE(_payload_size) (sizeof(SZL_ClusterCmdReqParams_t) + (sizeof(szl_uint8) * (_payload_size)) - (sizeof(szl_uint8) * SZL_VLA_INIT))

/**
 * Type for ClusterCmdRespParam.
 * This struct holds the result for the Cluster Command.
 */
typedef struct
{
    SZL_EP_t DestinationEndpoint;       /**< The Endpoint for the Destination (App) */
    SZL_Addresses_t SourceAddress;      /**< The address of the source (remote device). */
    szl_bool ManufacturerSpecific;      /**< This is set to szl_true if the command is manufacturer specific, else set to szl_false */
    szl_uint16 ClusterID;               /**< The Cluster ID that has been bound */
    szl_uint8 Command;                  /**< The cluster Command */
    szl_uint8 PayloadLength;            /**< The length of the payload to follow */
    szl_uint8 Payload[SZL_VLA_INIT];    /**< The ZCL formatted payload in LITTLE ENDIAN - (extends beyond bounds) */
} SZL_ClusterCmdRespParams_t;
#define SZL_ClusterCmdRespParams_t_SIZE(_payload_size) (sizeof(SZL_ClusterCmdRespParams_t) + (sizeof(szl_uint8) * (_payload_size)) - (sizeof(szl_uint8) * SZL_VLA_INIT))

/**
 * Type for DataPoint.
 * This struct holds the parameters given by the app for a DataPoint.
 * NOTE: All data will be in native endianness and in App data type (size)<br>
 * For native C strings the Callback functions don't include the zero termination in the Data, but the DirectAccess will maintain the zero termination.
 */
typedef struct
{
    szl_uint16 ClusterID;                   /**< ClusterID for the datapoint */
    szl_uint16 AttributeID;                 /**< AttributeID for the datapoint */
    SZL_EP_t Endpoint;                      /**< The Endpoint for the datapoint */
    SZL_ZIGBEE_DATA_TYPE_t DataType;        /**< The data type for the datapoint */
    szl_uint8 DataSize;                     /**< The size of the data - for strings this is the maximum size including zero termination or length byte*/
    SZL_DP_PROPERTY_t Property;             /**< Property for the datapoint */
    union
    {
        struct
        {
            SZL_CB_DataPointRead_t Read;    /**< [Mandatory] - Callback function to read the datapoint value */
            SZL_CB_DataPointWrite_t Write;  /**< [Optional]  - Callback function to write value to the datapoint, This has to be provided to be able to do WRITE Cluster Cmd's.*/
        } Callback;
        struct
        {
            void* Data;                     /**< [Mandatory] - Pointer to the data.*/
        } DirectAccess;
    }AccessType;
} SZL_DataPoint_t;

/**
 * Type for ClusterCmd.
 * This struct holds the parameters given by the app for a Cluster Cmds.
 */
typedef struct
{
    szl_bool            ManufacturerSpecific;   /**< This is set to 1 if one of the attributes is manufacturer specific, else set to 0 */
    SZL_EP_t            Endpoint;               /**< The Endpoint for the datapoint */
    szl_uint16          ClusterID;              /**< ClusterID for the command */
    szl_uint8           CmdID;                  /**< The ZCL command ID */
    SZL_CLU_OP_TYPE_t   OperationType;          /**< The Operation type */
    union
    {
      struct
      {
        szl_bool                    ManufacturerSpecific;   /**< This is set to 1 if one of the attributes is manufacturer specific, else set to 0 */
        szl_uint16                  AttributeID;            /**< AttributeID for the Operation Type to operate on */
      } Attribute;
      SZL_CB_ClusterCmd_t           Callback;               /**< Callback function if the OperationType is SZL_CLU_OP_TYPE_CUSTOM_CLIENT_TO_SERVER or SZL_CLU_OP_TYPE_CUSTOM_SERVER_TO_CLIENT */
    } OperationAccess;
} SZL_ClusterCmd_t;








/**
 * Type for Initialize.
 * This struct holds the parameters given by the app when it initializes the library @ref SZL_Initialize
 */
typedef struct
{
    SZL_VERSION_t Version;                              /**< The version of the Library szl.h header file */
    szl_uint8 NumberOfEndpoints;                        /**< The number of end points the app supports */
    SZL_EP_t Endpoints[SZL_CFG_ENDPOINT_CNT];
} SZL_Initialize_t;

/**
 * Type for NetworkLeaveParams.
 * This struct holds the parameters given by the app to leave a network.
 */
typedef struct
{
    szl_uint64 DeviceIEEEAddress;        /**< The IEEE address for the device (App) */
    szl_uint16 DeviceNwkAddress;         /**< The network address for the device (App) */
    szl_uint8 Rejoin;                    /**< Set to szl_true if device should re join the network*/
} SZL_NwkLeaveReqParams_t;
#define SZL_NwkLeaveReqParams_t_SIZE (sizeof(SZL_NwkLeaveReqParams_t))

/**
 * Type for NetworkLeaveRspParams.
 */
typedef struct
{
  szl_uint16 DeviceNwkAddress;         /**< The network address for the device (App) */
  SZL_ZDO_STATUS_t Status;         /**< Status return by the leaving device  */
} SZL_NwkLeaveRespParams_t;
#define SZL_NwkLeaveRespParams_t_SIZE (sizeof(SZL_NwkLeaveRespParams_t))

/**
 * Type for Network Leave Notification Params.
 * This struct holds the parameters given by the SZL in the Network Leave notification.
 */
typedef struct _SZL_NwkLeaveNtfParams_t
{
    szl_uint16 NwkAddress;          /**< The short address for the device also called the Network address */
} SZL_NwkLeaveNtfParams_t;

/**
 * @}
 */
// end of defgroup szl_api_str Structs


/**
 *
 * Attribute Report notification.
 *
 * This is a Callback function used by SZL in order to notify that an attribute report has been received
 * This callback has to be registered in the @ref SZL_AttributeReportNtfRegisterCB and @ref SZL_AttributeReportNtfUnregisterCB
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]     Service  (@ref zabService*) Server Instance Pointer
 * @param[in]     Params   (@ref SZL_AttributeReportNtfParams_t*) pointer to the parameters
 *
 * @return -
 */
typedef void (*SZL_CB_AttributeReportNtf_t) (zabService* Service, struct _SZL_AttributeReportNtfParams_t* Params);


/**
 *
 * DataPoint Changed Ntf.
 *
 * This is a Callback function used by SZL in order to inform that a datapoint with direct access mode has been changed<br>
 * This callback has to be registered in the @ref SZL_AttributeChangedNtfRegisterCB and @ref SZL_AttributeChangedNtfUnregisterCB
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]     Service           (@ref zabService*) Server Instance Pointer
 * @param[in]     ClusterID         (szl_uint16) ClusterID for the datapoint
 * @param[in]     AttributeID       (szl_uint16) AttributeID for the datapoint
 * @param[in]     Endpoint          (szl_uint8)  Endpoint for the datapoint (APP)
 *
 * @return -
 */
typedef void (*SZL_CB_AttributeChangedNtf_t) (zabService* Service, struct _SZL_AttributeChangedNtfParams_t* Params);


/**
 *
 * Attributes Deregister
 *
 * This is the function used by the application to deregister it's attributes (datapoints) <br>
 * The attributes are all passed as an array and stored in the SZL.<br>
 * \msc
 *  APP  [label="Application", linecolour="#0000ff"],
 *  SZL  [label="SZL", linecolour="#00ff60"],
 *  ZBB  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"];
 *
 * APP =>   SZL [label="AttributesDeregister()", URL="\ref SZL_AttributesDeregister"];
 * APP <<   SZL [label="Result", URL="\ref SZL_RESULT_t"];
 *
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]     Service      (@ref zabService*) Server Instance Pointer
 * @param[in]     DataPoints   (@ref SZL_DataPoint_t) pointer to an array of datapoints
 * @param[in]     Count        (szl_uint8) number of elements in the DataPoints array
 *
 * @return @ref SZL_RESULT_t
 *
 */
SZL_RESULT_t SZL_AttributesDeregister(zabService* Service, const SZL_DataPoint_t *DataPoints, szl_uint8 Count);

/**
 *
 * Attributes Register
 *
 * This is the function used by the application to register all it's attributes (datapoints) <br>
 * The attributes are all passed as an array and stored in the SZL.<br>
 * The SZL can then read and write the attributes by using the callback functions provided for each attribute.
 * \msc
 *  APP  [label="Application", linecolour="#0000ff"],
 *  SZL  [label="SZL", linecolour="#00ff60"],
 *  ZBB  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"];
 *
 * APP =>   SZL [label="AttributesRegister()", URL="\ref SZL_AttributesRegister"];
 * APP <<   SZL [label="Result", URL="\ref SZL_RESULT_t"];
 *
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]     Service      (@ref zabService*) Server Instance Pointer
 * @param[in]     DataPoints   (@ref SZL_DataPoint_t) pointer to an array of datapoints
 * @param[in]     Count        (szl_uint8) number of elements in the DataPoints array
 *
 * @return @ref SZL_RESULT_t
 *
 */
SZL_RESULT_t SZL_AttributesRegister(zabService* Service, const SZL_DataPoint_t *DataPoints, szl_uint8 Count);


/**
 *
 * Attribute Discover Response.
 *
 * This is the callback function used by SZL in order to reply to the \ref SZL_AttributeDiscoverReq <br>
 * If the request succeeded the Params will hold all the discovered attributes identifier and type <br>
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Status        (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params        (@ref SZL_AttributeDiscoverRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_AttributeDiscoverResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_AttributeDiscoverRespParams_t* Params, szl_uint8 TransactionId);

/**
 *
 * Attribute Discover Request
 *
 * This is the function used by the application to get a list of all attributes supported by a device using the binding table for the cluster<br>
 *
 * \msc
 *  AP1  [label="Application", linecolour="#0000ff"],
 *  SZ1  [label="SZL", linecolour="#00ff60"],
 *  ZB1  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"],
 *  ZB2  [label="ZigBee Brick", linecolour="#ff0000"],
 *  SZ2  [label="SZL", linecolour="#00ff60"],
 *  AP2  [label="Application", linecolour="#0000ff"];
 *
 * AP1 =>   SZ1 [label="AttributeDiscoverReq()", URL="\ref SZL_AttributeDiscoverReq"];
 * AP1 <<   SZ1 [label="Result", URL="\ref SZL_RESULT_t"];
 * SZ1 box  SZ1 [label="If Result=SUCCESS"];
 * SZ1 :>   ZB1;
 * ZB1 ->   ZB2;
 * ZB2 :>   SZ2;
 * SZ2 <<=  AP2;
 * ZB2 <:   SZ2;
 * ZB1 <-   ZB2;
 * SZ1 <:   ZB1;
 * AP1 <<=  SZ1 [label="AttributeDiscoverResp()", URL="\ref SZL_CB_AttributeDiscoverResp_t"];
 * \endmsc
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback      (@ref SZL_CB_AttributeDiscoverResp_t) the callback function for the response
 * @param[in]  Params        (@ref SZL_AttributeDiscoverReqParams_t*) pointer to the parameters
 * @param[out] TransactionId pointer to location where Transaction ID should be returned. Set to NULL if caller does not want the Transaction ID.
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_AttributeDiscoverReq(zabService* Service, SZL_CB_AttributeDiscoverResp_t Callback, SZL_AttributeDiscoverReqParams_t* Params, szl_uint8* TransactionId);

/**
 *
 * Attribute Read response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_AttributeReadReq <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Status        (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params        (@ref SZL_AttributeReadRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_AttributeReadResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_AttributeReadRespParams_t* Params, szl_uint8 TransactionId);

/**
 *
 * Attribute Read request
 *
 * This is the function used by the application to read attribute(s) value from another device using the binding table. <br>
 * Each attribute identifier field shall contain the identifier of the attribute to be read.
 * \msc
 *  AP1  [label="Application", linecolour="#0000ff"],
 *  SZ1  [label="SZL", linecolour="#00ff60"],
 *  ZB1  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"],
 *  ZB2  [label="ZigBee Brick", linecolour="#ff0000"],
 *  SZ2  [label="SZL", linecolour="#00ff60"],
 *  AP2  [label="Application", linecolour="#0000ff"];
 *
 * AP1 =>   SZ1 [label="AttributeReadReq()", URL="\ref SZL_AttributeReadReq"];
 * AP1 <<   SZ1 [label="Result", URL="\ref SZL_RESULT_t"];
 * SZ1 box  SZ1 [label="If Result=SUCCESS"];
 * SZ1 :>   ZB1;
 * ZB1 ->   ZB2;
 * ZB2 :>   SZ2;
 * SZ2 =>   AP2 [label="DataPointReadCB()", URL="\ref SZL_CB_DataPointRead_t"];
 * SZ2 <<=  AP2;
 * ZB2 <:   SZ2;
 * ZB1 <-   ZB2;
 * SZ1 <:   ZB1;
 * AP1 <<=  SZ1 [label="AttributeReadResp()", URL="\ref SZL_CB_AttributeReadResp_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback      (@ref SZL_CB_AttributeReadResp_t) the callback function for the response
 * @param[in]  Params        (@ref SZL_AttributeReadReqParams_t*) pointer to the parameters
 * @param[out] TransactionId pointer to location where Transaction ID should be returned. Set to NULL if caller does not want the Transaction ID.
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_AttributeReadReq(zabService* Service, SZL_CB_AttributeReadResp_t Callback, SZL_AttributeReadReqParams_t* Params, szl_uint8* TransactionId);

/**
 *
 * Attribute Write response.
 *
 * This is a Callback function used by SZL in order to reply to the @ref SZL_AttributeWriteReq
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Status        (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params        (@ref SZL_AttributeWriteRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_AttributeWriteResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_AttributeWriteRespParams_t* Params, szl_uint8 TransactionId);

/**
 *
 * Attribute Write request
 *
 * This is the function used by the application to write attribute(s) value to another device using the binding table
 *
 * \msc
 *  AP1  [label="Application", linecolour="#0000ff"],
 *  SZ1  [label="SZL", linecolour="#00ff60"],
 *  ZB1  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"],
 *  ZB2  [label="ZigBee Brick", linecolour="#ff0000"],
 *  SZ2  [label="SZL", linecolour="#00ff60"],
 *  AP2  [label="Application", linecolour="#0000ff"];
 *
 * AP1 =>   SZ1 [label="AttributeWriteReq()", URL="\ref SZL_AttributeWriteReq"];
 * AP1 <<   SZ1 [label="Result", URL="\ref SZL_RESULT_t"];
 * SZ1 box  SZ1 [label="If Result=SUCCESS"];
 * SZ1 :>   ZB1;
 * ZB1 ->   ZB2;
 * ZB2 :>   SZ2;
 * SZ2 =>   AP2 [label="DataPointWriteCB()", URL="\ref SZL_CB_DataPointWrite_t"];
 * SZ2 <<=  AP2;
 * ZB2 <:   SZ2;
 * ZB1 <-   ZB2;
 * SZ1 <:   ZB1;
 * AP1 <<=  SZ1 [label="AttributeWriteResp()", URL="\ref SZL_CB_AttributeWriteResp_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback      (@ref SZL_CB_AttributeWriteResp_t) the callback function for the response
 * @param[in]  Params        (@ref SZL_AttributeWriteReqParams_t*) pointer to the parameters
 * @param[out] TransactionId pointer to location where Transaction ID should be returned. Set to NULL if caller does not want the Transaction ID.
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_AttributeWriteReq(zabService* Service, SZL_CB_AttributeWriteResp_t Callback, SZL_AttributeWriteReqParams_t* Params, szl_uint8* TransactionId);


/**
 *
 * Attribute Changed Indication
 *
 * This is the function used by the application to indicate to the library that an attribute has been changed
 * NOTE: This should ONLY be called if the App do the change behind the back of the Library
 *
 * \msc
 *  AP1  [label="Application", linecolour="#0000ff"],
 *  SZ1  [label="SZL", linecolour="#00ff60"],
 *  ZB1  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"],
 *  ZB2  [label="ZigBee Brick", linecolour="#ff0000"],
 *  SZ2  [label="SZL", linecolour="#00ff60"],
 *  AP2  [label="Application", linecolour="#0000ff"];
 *
 * AP1 =>   SZ1 [label="AttributeChangedInd()", URL="\ref SZL_AttributeChangedInd"];
 * AP1 <<   SZ1 [label="Result", URL="\ref SZL_RESULT_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService*) Server Instance Pointer
 * @param[in]  Params   (@ref SZL_AttributeChangedIndParams_t*) pointer to the parameters
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_AttributeChangedInd(zabService* Service, SZL_AttributeChangedIndParams_t* Params);


/**
 *
 * Client Cluster Register
 *
 * This is the function used by the application to register all it's client clusters per endpoint <br>
 * The clusters are all passed as an array and stored in the SZL.<br>
 * \msc
 *  APP  [label="Application", linecolour="#0000ff"],
 *  SZL  [label="SZL", linecolour="#00ff60"],
 *  ZBB  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"];
 *
 * APP =>   SZL [label="ClientClusterRegister()", URL="\ref SZL_ClientClusterRegister"];
 * APP <<   SZL [label="Result", URL="\ref SZL_RESULT_t"];
 *
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]     Service           (@ref zabService*) Server Instance Pointer
 * @param[in]     Endpoint          (@ref SZL_EP_t) Endpoint for the clusters
 * @param[in]     ClientClusters    /szl_uint16*) pointer to an array of client cluster
 * @param[in]     Count             (szl_uint8) number of elements in the ClientClusters array
 *
 * @return @ref SZL_RESULT_t
 *
 */
SZL_RESULT_t SZL_ClientClusterRegister(zabService* Service, SZL_EP_t Endpoint, const szl_uint16 *ClientClusters, szl_uint8 Count);

/**
 *
 * Cluster Commands Register
 *
 * This is the function used by the application to register all it's cluster commands to be handled <br>
 * The commands are all passed as an array and stored in the SZL.<br>
 * The SZL can then do the commands on the associated attributes (datapoints) by using the access method provided for each attribute.<br>
 * \msc
 *  APP  [label="Application", linecolour="#0000ff"],
 *  SZL  [label="SZL", linecolour="#00ff60"],
 *  ZBB  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"];
 *
 * APP =>   SZL [label="ClusterCmdRegister()", URL="\ref SZL_ClusterCmdRegister"];
 * APP <<   SZL [label="Result", URL="\ref SZL_RESULT_t"];
 *
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]     Service       (@ref zabService*) Server Instance Pointer
 * @param[in]     ClusterCmds   (@ref SZL_ClusterCmd_t) pointer to an array of cluster commands
 * @param[in]     Count         (szl_uint8) number of elements in the ClusterCmds array
 *
 * @return @ref SZL_RESULT_t
 *
 */
SZL_RESULT_t SZL_ClusterCmdRegister(zabService* Service, const SZL_ClusterCmd_t *ClusterCmds, szl_uint8 Count);

/**
 *
 * Cluster Commands Deregister
 *
 * This is the function used by the application to deregister it's cluster commands <br>
 * The commands are all passed as an array and stored in the SZL.<br>
 * \msc
 *  APP  [label="Application", linecolour="#0000ff"],
 *  SZL  [label="SZL", linecolour="#00ff60"],
 *  ZBB  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"];
 *
 * APP =>   SZL [label="ClusterCmdDeregister()", URL="\ref SZL_ClusterCmdDeregister"];
 * APP <<   SZL [label="Result", URL="\ref SZL_RESULT_t"];
 *
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]     Service       (@ref zabService*) Server Instance Pointer
 * @param[in]     ClusterCmds   (@ref SZL_ClusterCmd_t) pointer to an array of cluster commands
 * @param[in]     Count         (szl_uint8) number of elements in the ClusterCmds array
 *
 * @return @ref SZL_RESULT_t
 *
 */
SZL_RESULT_t SZL_ClusterCmdDeregister(zabService* Service, const SZL_ClusterCmd_t *ClusterCmds, szl_uint8 Count);

/**
 *
 * Cluster Command response.
 *
 * This is a Callback function used by SZL in order to reply to the @ref SZL_ClusterCmdReq
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Status        (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_ClusterCmdRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this confirm. May be used to match confirm to request.
 *
 * @return -
 */
typedef void (*SZL_CB_ClusterCmdResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_ClusterCmdRespParams_t* Params, szl_uint8 TransactionId);


/**
 *
 * Cluster Command request
 *
 * This is the function used by the application to do a cluster command to another device
 *
 * \msc
 *  AP1  [label="Application", linecolour="#0000ff"],
 *  SZ1  [label="SZL", linecolour="#00ff60"],
 *  ZB1  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"],
 *  ZB2  [label="ZigBee Brick", linecolour="#ff0000"],
 *  SZ2  [label="SZL", linecolour="#00ff60"],
 *  AP2  [label="Application", linecolour="#0000ff"];
 *
 * AP1 =>   SZ1 [label="ClusterCmdReq()", URL="\ref SZL_ClusterCmdReq"];
 * AP1 <<   SZ1 [label="Result", URL="\ref SZL_RESULT_t"];
 * SZ1 box  SZ1 [label="If Result=SUCCESS"];
 * SZ1 :>   ZB1;
 * ZB1 ->   ZB2;
 * ZB2 :>   SZ2;
 * SZ2 =>   AP2 [label="ClusterCmdHandler()"];
 * SZ2 <<=  AP2;
 * ZB2 <:   SZ2;
 * ZB1 <-   ZB2;
 * SZ1 <:   ZB1;
 * AP1 <<=  SZ1 [label="ClusterCmdResp()", URL="\ref SZL_CB_ClusterCmdResp_t"];
 * AP1 box  SZ1 [label="Some cluster commands come with a response"];
 * AP1 <<=  SZ1 [label="SZL_CB_ClusterCmdResponse()", URL="\ref SZL_CB_ClusterCmdResp_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback      (@ref SZL_CB_ClusterCmdResp_t) the callback function for the response status
 * @param[in]  Params        (@ref SZL_ClusterCmdReqParams_t*) pointer to the parameters
 * @param[out] TransactionId pointer to location where Transaction ID should be returned. Set to NULL if caller does not want the Transaction ID.
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_ClusterCmdReq(zabService* Service, SZL_CB_ClusterCmdResp_t Callback, SZL_ClusterCmdReqParams_t* Params, szl_uint8* TransactionId);

SZL_RESULT_t SZL_ClusterCmdReqTimeout(zabService* Service,
                               SZL_CB_ClusterCmdResp_t Callback,
                               SZL_ClusterCmdReqParams_t* Params,
                               szl_uint8* TransactionId,
                               szl_uint8 TimeoutForResp,
                               szl_uint8 DefaultRespDisabled);

/**
 *
 * Initialize.
 *
 * This is the function used by the application to initialize the library.<br>
 * The app will have to fill all the mandatory fields, and provide the optional fields that is of interest to the app.
 *
 * \msc
 *  APP  [label="Application", linecolour="#0000ff"],
 *  SZL  [label="SZL", linecolour="#00ff60"],
 *  ZBB  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"];
 *
 * APP =>   SZL [label="Initialize()", URL="\ref SZL_Initialize"];
 * APP <<   SZL [label="Result", URL="\ref SZL_RESULT_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]     Service   (@ref zabService*) Server Instance Pointer
 * @param[in]     InitPtr   (@ref SZL_Initialize_t) pointer to a structure with the initialization parameters
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_Initialize(zabService* Service, SZL_Initialize_t *InitPtr);



/**
 *
 * Destroy.
 *
 * This is the function used by the application to destroy (reset) an instance of the library.<br>
 *
 * \msc
 *  APP  [label="Application", linecolour="#0000ff"],
 *  SZL  [label="SZL", linecolour="#00ff60"],
 *  ZBB  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"];
 *
 * APP =>   SZL [label="Destory()", URL="\ref SZL_Destroy"];
 * APP <<   SZL [label="Result", URL="\ref SZL_RESULT_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]     Service   (@ref zabService*) Server Instance Pointer
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_Destroy(zabService* Service);


/**
 *
 * Attribute Report Configure response.
 *
 * This is a Callback function used by SZL in order to reply to the @ref SZL_AttributeReportCfgReq
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      |   |
  | SZL |   |-------------+---|
  |     .---| SED         |   |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Status        (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params        (@ref SZL_AttributeReportCfgRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_AttributeReportCfgResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_AttributeReportCfgRespParams_t* Params, szl_uint8 TransactionId);

/**
 *
 * Attribute Report Configure request
 *
 * This is the function used by the application to configure a device to automatically
 * report the values of one or more of its attributes. <br>
 * \msc
 *  AP1  [label="Application", linecolour="#0000ff"],
 *  SZ1  [label="SZL", linecolour="#00ff60"],
 *  ZB1  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"],
 *  ZB2  [label="ZigBee Brick", linecolour="#ff0000"],
 *  SZ2  [label="SZL", linecolour="#00ff60"],
 *  AP2  [label="Application", linecolour="#0000ff"];
 *
 * AP1 =>   SZ1 [label="AttributeReportCfgReq()", URL="\ref SZL_AttributeReportCfgReq"];
 * AP1 <<   SZ1 [label="Result", URL="\ref SZL_RESULT_t"];
 * SZ1 box  SZ1 [label="If Result=SUCCESS"];
 * AP1 <<=  SZ1 [label="AttributeReportCfgResp()", URL="\ref SZL_CB_AttributeReportCfgResp_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      |   |
  | SZL | X |-------------+---|
  |     .---| SED         |   |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback      (@ref SZL_CB_AttributeReportCfgResp_t) the callback function for the response
 * @param[in]  Params        (@ref SZL_AttributeReportCfgReqParams_t*) pointer to the parameters
 * @param[out] TransactionId pointer to location where Transaction ID should be returned. Set to NULL if caller does not want the Transaction ID.
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_AttributeReportCfgReq(zabService* Service, SZL_CB_AttributeReportCfgResp_t Callback, SZL_AttributeReportCfgReqParams_t* Params, szl_uint8* TransactionId);


/**
 *
 * Attribute Read Report Configuration response.
 *
 * This is a Callback function used by SZL in order to reply to the @ref SZL_AttributeReadReportCfgReq
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      |   |
  | SZL |   |-------------+---|
  |     .---| SED         |   |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService*) Server Instance Pointer
 * @param[in]  Status   (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_AttributeReadReportCfgRespParams_t*) pointer to the parameters
 *
 * @return -
 */
typedef void (*SZL_CB_AttributeReadReportCfgResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_AttributeReadReportCfgRespParams_t* Params, szl_uint8 TransactionId);

/**
 *
 * Attribute Read Report Configuration request
 *
 * This is the function used by the application to read the report configure from a device. <br>
 * \msc
 *  AP1  [label="Application", linecolour="#0000ff"],
 *  SZ1  [label="SZL", linecolour="#00ff60"],
 *  ZB1  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"],
 *  ZB2  [label="ZigBee Brick", linecolour="#ff0000"],
 *  SZ2  [label="SZL", linecolour="#00ff60"],
 *  AP2  [label="Application", linecolour="#0000ff"];
 *
 * AP1 =>   SZ1 [label="AttributeReadReportCfgReq()", URL="\ref SZL_AttributeReadReportCfgReq"];
 * AP1 <<   SZ1 [label="Result", URL="\ref SZL_RESULT_t"];
 * SZ1 box  SZ1 [label="If Result=SUCCESS"];
 * AP1 <<=  SZ1 [label="AttributeReadReportCfgResp()", URL="\ref SZL_CB_AttributeReadReportCfgResp_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      |   |
  | SZL | X |-------------+---|
  |     .---| SED         |   |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback (@ref SZL_CB_AttributeReadReportCfgResp_t) the callback function for the response
 * @param[in]  Params   (@ref SZL_AttributeReadReportCfgReqParams_t*) pointer to the parameters
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_AttributeReadReportCfgReq(zabService* Service, SZL_CB_AttributeReadReportCfgResp_t Callback, SZL_AttributeReadReportCfgReqParams_t* Params, szl_uint8* TransactionId);

/**
 *
 * Network Leave Notification.
 *
 * This is a Callback function used by SZL to notify to the application, that a device has left a network
 * This callback has to be registered in the @ref SZL_NwkLeaveNtfRegisterCB and @ref SZL_NwkLeaveNtfUnregisterCB
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      |   |
  | SZL |   |-------------+---|
  |     .---| SED         |   |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service    (@ref zabService*) Server Instance Pointer
 * @param[in]  Params     (@ref SZL_NwkLeaveNtfParams_t*) Pointer to the parameters
 *
 * @return -
 */
typedef void (*SZL_CB_NwkLeaveNtf_t) (zabService* Service, struct _SZL_NwkLeaveNtfParams_t* Params);




/**
 *
 * AttributeReportNtf Register Callback
 *
 * This function is used to register a callback for the Attribute Report Notification
 *
 * \msc
 *  AP1  [label="Application", linecolour="#0000ff"],
 *  SZ1  [label="SZL", linecolour="#00ff60"],
 *  ZB1  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"],
 *  ZB2  [label="ZigBee Brick", linecolour="#ff0000"],
 *  SZ2  [label="SZL", linecolour="#00ff60"],
 *  AP2  [label="Application", linecolour="#0000ff"];
 *
 * AP1 =>   SZ1 [label="HandlerAdd()", URL="\ref SZL_CB_AttributeReportNtf_t"];
 * AP1 <<   SZ1 [label="Result", URL="\ref SZL_RESULT_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback (@ref SZL_CB_AttributeReportNtf_t) the callback function for the response
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_AttributeReportNtfRegisterCB(zabService* Service,
                                          SZL_CB_AttributeReportNtf_t Callback);

/**
 *
 * AttributeReportNtf Unregister CB
 *
 * This function is used to unregister a callback for the Attribute Report Notification
 *
 * \msc
 *  AP1  [label="Application", linecolour="#0000ff"],
 *  SZ1  [label="SZL", linecolour="#00ff60"],
 *  ZB1  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"],
 *  ZB2  [label="ZigBee Brick", linecolour="#ff0000"],
 *  SZ2  [label="SZL", linecolour="#00ff60"],
 *  AP2  [label="Application", linecolour="#0000ff"];
 *
 * AP1 =>   SZ1 [label="HandlerRemove()", URL="\ref SZL_CB_AttributeReportNtf_t"];
 * AP1 <<   SZ1 [label="Result", URL="\ref SZL_RESULT_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback (@ref SZL_CB_AttributeReportNtf_t) the callback function for the response
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_AttributeReportNtfUnregisterCB(zabService* Service, SZL_CB_AttributeReportNtf_t Callback);

/**
 *
 * AttributeChangedNtf Register Callback
 *
 * This function is used to register a callback for the Attribute Changed Notification
 *
 * \msc
 *  AP1  [label="Application", linecolour="#0000ff"],
 *  SZ1  [label="SZL", linecolour="#00ff60"],
 *  ZB1  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"],
 *  ZB2  [label="ZigBee Brick", linecolour="#ff0000"],
 *  SZ2  [label="SZL", linecolour="#00ff60"],
 *  AP2  [label="Application", linecolour="#0000ff"];
 *
 * AP1 =>   SZ1 [label="HandlerAdd()", URL="\ref SZL_CB_AttributeChangedNtf_t"];
 * AP1 <<   SZ1 [label="Result", URL="\ref SZL_RESULT_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback (@ref SZL_CB_AttributeChangedNtf_t) the callback function to register
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_AttributeChangedNtfRegisterCB(zabService* Service, SZL_CB_AttributeChangedNtf_t Callback);

/**
 *
 * AttributeChangedNtf Unregister CB
 *
 * This function is used to unregister a callback for the Attribute Changed Notification
 *
 * \msc
 *  AP1  [label="Application", linecolour="#0000ff"],
 *  SZ1  [label="SZL", linecolour="#00ff60"],
 *  ZB1  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"],
 *  ZB2  [label="ZigBee Brick", linecolour="#ff0000"],
 *  SZ2  [label="SZL", linecolour="#00ff60"],
 *  AP2  [label="Application", linecolour="#0000ff"];
 *
 * AP1 =>   SZ1 [label="HandlerRemove()", URL="\ref SZL_CB_AttributeChangedNtf_t"];
 * AP1 <<   SZ1 [label="Result", URL="\ref SZL_RESULT_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback (@ref SZL_CB_AttributeChangedNtf_t) the callback function to unregister
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_AttributeChangedNtfUnregisterCB(zabService* Service, SZL_CB_AttributeChangedNtf_t Callback);


/**
 *
 * NwkLeaveNtf Register CB
 *
 * This function is used to register a callback for the Network Leave Notification
 *
 * \msc
 *  AP1  [label="Application", linecolour="#0000ff"],
 *  SZ1  [label="SZL", linecolour="#00ff60"],
 *  ZB1  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"],
 *  ZB2  [label="ZigBee Brick", linecolour="#ff0000"],
 *  SZ2  [label="SZL", linecolour="#00ff60"],
 *  AP2  [label="Application", linecolour="#0000ff"];
 *
 * AP1 =>   SZ1 [label="HandlerAdd()", URL="\ref SZL_CB_NwkLeaveNtf_t"];
 * AP1 <<   SZ1 [label="Result", URL="\ref SZL_RESULT_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback (@ref SZL_CB_NwkLeaveNtf_t) the callback function for the response
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_NwkLeaveNtfRegisterCB(zabService* Service, SZL_CB_NwkLeaveNtf_t Callback);

/**
 *
 * NwkLeaveNtf Unregister CB
 *
 * This function is used to unregister a callback for the Network Leave Notification
 *
 * \msc
 *  AP1  [label="Application", linecolour="#0000ff"],
 *  SZ1  [label="SZL", linecolour="#00ff60"],
 *  ZB1  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"],
 *  ZB2  [label="ZigBee Brick", linecolour="#ff0000"],
 *  SZ2  [label="SZL", linecolour="#00ff60"],
 *  AP2  [label="Application", linecolour="#0000ff"];
 *
 * AP1 =>   SZ1 [label="HandlerRemove()", URL="\ref SZL_CB_NwkLeaveNtf_t"];
 * AP1 <<   SZ1 [label="Result", URL="\ref SZL_RESULT_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback (@ref SZL_CB_NwkLeaveNtf_t) the callback function for the response
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_NwkLeaveNtfUnregisterCB(zabService* Service, SZL_CB_NwkLeaveNtf_t Callback);


/**
 * @}
 */
// end of defgroup szl_api_func Common Functions



/**
 * @ingroup szl_api
 * @defgroup szl_api_funct Test Functions
 * This section defines all the Test Specific Functions implemented in the SZLibrary for the Test App to call.
 * @{
 */


/**
 *
 * Network Leave response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_NwkLeaveReq
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator |   |
  | App | X |-------------+---|
  |-----+---| Router      |   |
  | SZL |   |-------------+---|
  |     .---| SED         |   |
  .---------------------------.
 \endverbatim
 *
 * @param[in]     Service       (@ref zabService*) Server Instance Pointer
 * @param[in]     Status        (@ref SZL_STATUS_t) The status for the operation
 * @param[in]     TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_NwkLeaveResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_NwkLeaveRespParams_t* Params, szl_uint8 TransactionId);

/**
 *
 * Network Leave Request
 *
 * This is the function used by the application to Leave a network. <br>
 * @remarks This is only to be used for test applications.\n
 * \n
 *      For ZAB, this function may be used by applications. <br>
 *      1)  If the network address matches the network address of the App, the App leaves the network. <br>
 *      2)  If the network address does not match the network address of the App and the IEEE address consists of plain 0's, <br>
 *          the device with the specified network address will be asked to leave the network. <br>
 *      3)  If the network address does not match the network address of the App device, but matches the network address of a router, <br>
 *          and the IEEE address matches the IEEE address of a child of that router, the router will ask the specified child to leave the network. <br>
 *
 * \msc
 *  APP  [label="Application", linecolour="#0000ff"],
 *  SZL  [label="SZL", linecolour="#00ff60"],
 *  ZBB  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"];
 *
 * APP =>   SZL [label="NwkLeaveReq()", URL="\ref SZL_NwkLeaveReq"];
 * APP <<   SZL [label="Result", URL="\ref SZL_RESULT_t"];
 * SZL box  SZL [label="If Result=SUCCESS"];
 * SZL :>   ZBB;
 * ZBB ->   NWK;
 * ...;
 * ZBB <-   NWK;
 * SZL <:   ZBB;
 * APP <<=  SZL [label="NwkLeaveResp()", URL="\ref SZL_CB_NwkLeaveResp_t"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator |   |
  | App |   |-------------+---|
  |-----+---| Router      |   |
  | SZL | X |-------------+---|
  |     .---| SED         |   |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback      (@ref SZL_CB_NwkLeaveResp_t) the callback function for the response
 * @param[in]  Params        (@ref SZL_NwkLeaveReqParams_t*) pointer to the parameters.
 * @param[out] TransactionId pointer to location where Transaction ID should be returned. Set to NULL if caller does not want the Transaction ID.
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_NwkLeaveReq(zabService* Service, SZL_CB_NwkLeaveResp_t Callback, SZL_NwkLeaveReqParams_t* Params, szl_uint8* TransactionId);


/**
 * @}
 */
// end of defgroup szl_api_funct Test Functions


/**
 * @ingroup szl_api
 * @defgroup szl_api_zab ZAB Functions
 * This section defines all the (for now) ZAB specific functions
 * @{
 */


/**
 * Type for Received RSSI Notification parameters.
 * This struct holds the parameters given on a notification for a received message
 */
typedef struct _SZL_ReceivedSignalStrengthNtfParams_t
{
    SZL_Addresses_t NwkSourceAddress;         /**< The address for the Network source. */
    SZL_Addresses_t MacSourceAddress;         /**< The address for the MAC source. */
    szl_uint8 LastHopLqi;                     /**< LQI of the last hop of the received message. */
    szl_int8 LastHopRssi;                     /**< RSSI of the last hop of the received message. */
} SZL_ReceivedSignalStrengthNtfParams_t;
#define SZL_ReceivedSignalStrengthNtfParams_t_SIZE (sizeof(SZL_ReceivedSignalStrengthNtfParams_t))


/**
 *
 * Received RSSI Notification
 *
 * This is a OPTIONAL Callback function used by SZL in order to notify the application
 * of the RSSI of the last hop of a received AF (ZCL) message.
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]     Service  (@ref zabService*) Server Instance Pointer
 * @param[in]     Params   (@ref SZL_ReceivedRssiNtfParams_t*) pointer to the parameters
 *
 * @return -
 */
typedef void (*SZL_CB_ReceivedSignalStrengthNtf_t) (zabService* Service, struct _SZL_ReceivedSignalStrengthNtfParams_t* Params);

/**
 *
 * Received RSSI Notification Register Callback
 *
 * This function is used to register a callback for the Received RSSI Notification
 *
 * @param[in]  Service  (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback (@ref SZL_CB_ReceivedRssiNtf_t) the callback function for the response
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_ReceivedSignalStrengthNtfRegisterCB(zabService* Service, SZL_CB_ReceivedSignalStrengthNtf_t Callback);

/**
 *
 * Received RSSI Notification UnRegister Callback
 *
 * This function is used to unregister a callback for the Received RSSI Notification
 *
 * @param[in]  Service  (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback (@ref SZL_CB_ReceivedRssiNtf_t) the callback function for the response
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_ReceivedSignalStrengthNtfUnregisterCB(zabService* Service, SZL_CB_ReceivedSignalStrengthNtf_t Callback);






/**
 * Plugin Id
 *
 * The ID of each plugin. @ref ENUM_SZL_PLUGIN_ID_t
 * One plugin may be registered with each ID.
 * Plugins that are registered multiple times may be designed, but should use a different
 * ID per registration.
 */
typedef szl_uint8 SZL_PLUGIN_ID_t;

/**
 * Plugin Id values
 *
 * @ref SZL_PLUGIN_ID_t
 */
typedef enum
{
    /* Values defined for plugins delivered with ZAB */
    SZL_PLUGIN_ID_NONE            = 0x00,
    SZL_PLUGIN_ID_BASIC           = 0x01,
    SZL_PLUGIN_ID_IDENTIFY        = 0x02,
    SZL_PLUGIN_ID_ON_OFF          = 0x03,
    SZL_PLUGIN_ID_METERING        = 0x04,
    SZL_PLUGIN_ID_TIME            = 0x05,
    SZL_PLUGIN_ID_ELEC_MEAS       = 0x06,
    SZL_PLUGIN_ID_ECB             = 0x07,
    SZL_PLUGIN_ID_DISCOVERY       = 0x08,
    SZL_PLUGIN_ID_GP_CLIENT       = 0x09,
    SZL_PLUGIN_ID_POLL_CONT_CLIENT  = 0x0A,
    SZL_PLUGIN_ID_OTA_SERVER      = 0x0B,
    SZL_PLUGIN_ID_DEVICE_MANAGER  = 0x0C,
    SZL_PLUGIN_ID_THERMOSTAT      = 0x0D,

    /* Generic values defined for application defined plugins */
    SZL_PLUGIN_ID_APP_0           = 0x80,
    SZL_PLUGIN_ID_APP_1           = 0x81,
    SZL_PLUGIN_ID_APP_2           = 0x82,
    SZL_PLUGIN_ID_APP_3           = 0x83,
    SZL_PLUGIN_ID_APP_4           = 0x84,
    SZL_PLUGIN_ID_APP_5           = 0x85,
    SZL_PLUGIN_ID_APP_6           = 0x86,
    SZL_PLUGIN_ID_APP_7           = 0x87,
    SZL_PLUGIN_ID_APP_8           = 0x88,
    SZL_PLUGIN_ID_APP_9           = 0x89,
    SZL_PLUGIN_ID_APP_A           = 0x8A,
    SZL_PLUGIN_ID_APP_B           = 0x8B,
    SZL_PLUGIN_ID_APP_C           = 0x8C,
    SZL_PLUGIN_ID_APP_D           = 0x8D,
    SZL_PLUGIN_ID_APP_E           = 0x8E,
    SZL_PLUGIN_ID_APP_F           = 0x8F,

    SZL_PLUGIN_ID_RESERVED        = 0xFF,
} ENUM_SZL_PLUGIN_ID_t;

/**
 *
 * Plugin Register
 *
 * Register a plugin with the SZL.
 * Requested data will be allocated for the Service.
 *
 * @param[in]  Service  (@ref zabService*)     Server Instance Pointer
 * @param[in]  PluginId (@ref SZL_PLUGIN_ID_t) The ID of the plugin. Only one of each ID may be registered.
 * @param[in]  PluginDataSize                  The data requested for the plugin
 * @param[in]  PluginDataPtr                   Pointer to location where data pointer will be returned.
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_PluginRegister(zabService* Service, SZL_PLUGIN_ID_t PluginId, szl_uint32 PluginDataSize, void** PluginDataPtr);

/**
 *
 * Plugin Unregister
 *
 * Unregister a plugin from the SZL.
 * Previously allocated data will be free'd
 *
 * @param[in]  Service  (@ref zabService*)     Server Instance Pointer
 * @param[in]  PluginId (@ref SZL_PLUGIN_ID_t) The ID of the plugin. Only one of each ID may be registered.
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_PluginUnregister(zabService* Service, SZL_PLUGIN_ID_t PluginId);

/**
 *
 * Plugin Get Data Pointer
 *
 * Get the data pointer for a registered plugin
 *
 * @param[in]  Service  (@ref zabService*)     Server Instance Pointer
 * @param[in]  PluginId (@ref SZL_PLUGIN_ID_t) The ID of the plugin. Only one of each ID may be registered.
 * @param[in]  PluginDataPtr                   Pointer to location where data pointer will be returned.
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_PluginGetDataPointer(zabService* Service, SZL_PLUGIN_ID_t PluginId, void** PluginDataPtr);









/**
 * @}
 */
// end of defgroup szl_api_zab ZAB Functions


#ifdef __cplusplus
}
#endif

#endif /* _SZL_API_H_ */

