/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the ZNP SAPI interface
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *                           NJ    Original
 *              19-Jul-13   MvdB   Remove unused code
 *****************************************************************************/

#ifndef ZABZNPSAPIUTILITY_H_
#define ZABZNPSAPIUTILITY_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "zabZnpSapiFrame.h"
#include "osTypes.h"
#include "erStatusUtility.h" 
#include "sapTypes.h"
#include "zabTypes.h"

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/  
  
/******************************************************************************
 * Process all messages of subsystem SAPI
 ******************************************************************************/
extern
void zabZnpSapiProcessMsg(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * Get Device Info
 * 
 * Parameters:
 *   deviceInfoParam - The identifier of the Device Info parameter to be read
 ******************************************************************************/
extern
void znpi_sapi_getDeviceInfo( erStatus* Status, zabService* Service, znpSapiDeviceInfoType deviceInfoParam);

/******************************************************************************
 * Get Device Info
 * 
 * Paraneters:
 *   configId - The identifier for the configuration property
 *   length - Specifies the size of the Value buffer in bytes.
 *   pValue -  pointer to the value, in its native format (unsign8/16/32)
 ******************************************************************************/
extern
void znpi_sapi_write_config( erStatus* Status, zabService* Service, znpSapiConfigId configId, unsigned8 length, void *pValue);

/******************************************************************************
 * Read Configuration Data
 * 
 * Parameters:
 *   configId - The identifier for the configuration property
 ******************************************************************************/
extern
void znpi_sapi_read_config( erStatus* Status, zabService* Service, znpSapiConfigId configId);

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#ifdef __cplusplus
}
#endif
#endif /* ZABZNPSAPIUTILITY_H_ */
