# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to manage the application layout
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from queue import Queue, Empty
from threading import Lock, Thread
from apscheduler.schedulers.background import BackgroundScheduler
from .application_frame import *
from datetime import datetime
from logger import get_logger
from .zigbee_device_container import ZigBeeDeviceContainer


class ApplicationProcess(object):

    def __init__(self):
        self._debug = False
        # Thread
        self._receiver_alive = None
        self._receiver_thread = None
        # Read/Write management
        self._local_application_sequence_number = 0
        self._external_application_sequence_number = -1
        # Connection to network process
        self._queue = Queue()
        self._network_frame_generator = None
        # Send command/data management
        self._application_frame_generator = None
        self._mutex = Lock()
        self._mutex_no_multiple_send = Lock()
        self._application_scheduler = BackgroundScheduler()
        self._dict_application_frame_in_progress = {}
        # Data model management
        self._zigbee_device_container = ZigBeeDeviceContainer()

    @property
    def queue(self):
        return self._queue

    @queue.setter
    def queue(self, queue):
        self._queue = queue

    @property
    def network_frame_generator(self):
        return self._network_frame_generator

    @network_frame_generator.setter
    def network_frame_generator(self, frame_generator):
        self._network_frame_generator = frame_generator

    @property
    def application_frame_generator(self):
        return self._application_frame_generator

    @application_frame_generator.setter
    def application_frame_generator(self, frame_generator):
        self._application_frame_generator = frame_generator

    @property
    def zigbee_device_container(self):
        return self._zigbee_device_container

    @zigbee_device_container.setter
    def zigbee_device_container(self, zigbee_device_container):
        self._zigbee_device_container = zigbee_device_container

    # Activate/Deactivate debug
    def set_debug(self, value):
        self._debug = value

    def _start_receiver(self):
        """Start reader thread"""
        self._receiver_alive = True
        self._receiver_thread = Thread(target=self._receiver, name='application_rx', args=())
        self._receiver_thread.daemon = True
        self._receiver_thread.start()

    def _stop_receiver(self):
        """Stop reader thread only, wait for clean exit of thread"""
        self._receiver_alive = False

    def start(self):
        """start worker thread"""
        self._start_receiver()
        # Start network frame generator
        self._application_frame_generator = ApplicationFrameGenerator(self)
        # Start the scheduler
        self._application_scheduler.start()

    def stop(self):
        """set flag to stop worker threads"""
        self._stop_receiver()
        # remove jobs and stop scheduler
        self._application_scheduler.remove_all_jobs()
        self._application_scheduler.shutdown()

    def join(self):
        """wait for worker thread to terminate"""
        self._receiver_thread.join()

    def _receiver(self):
        """loop and copy serial->console"""

        while self._receiver_alive:

            try:
                data = self._queue.get(True, 0.1)
            except Empty:
                continue

            get_logger().debug('APPLICATION PROCESS: DATA - {}'.format(data["frame"]))
            '''
            # We sent a command we are waiting for an acknowledgment
            # Check if this is an acknowledgment
            # result = self._parse_ack_frame(data["frame"])
            # Report to applicative
            if result["is_ack"]:
                # Check if the ack corresponding to a command/data frames sent
                if self._application_scheduler.get_job(str(result["network_sequence_number"])) is not None:
                    # Remove process of sending the command/data frame
                    self._application_scheduler.remove_job(str(result["network_sequence_number"]))
                    # Send data in applicative queue
                    # self._queue.put(data[:len(data) - 1])
            # Check if this is a data frame
            # We can receive a command while we are waiting for an acknowledgment
            # We need still need to treat it
            else:
                result = self._parse_data_frame(data["frame"])
                # if result[0]:
                # Send data in applicative queue
                # self._queue.put(data[:len(data) - 1])
                # Send acknowledgment
                #self._send_ack(data["device_number"], result["ack_status_to_send"])'''

            self._queue.task_done()

    def send_frame(self, frame_control, option, device_number, payload):

        # Increment frame counter
        if self._local_application_sequence_number == 255:
            self._local_application_sequence_number = 0
        else:
            self._local_application_sequence_number += 1

        # Create frame
        frame = bytearray([frame_control, self._local_application_sequence_number, option])

        # Add payload of the frame
        for element in payload:
            frame.append(element)

        # Save number_retry for this transaction
        self._dict_application_frame_in_progress[str(self._local_application_sequence_number)] = 0

        # Schedule the jobs to send the command/data frame
        self._application_scheduler.add_job(self._writer, trigger='interval',
                                            args=(self._local_application_sequence_number, frame, device_number,),
                                            seconds=1.0, id=str(self._local_application_sequence_number),
                                            next_run_time=datetime.now())

    def _writer(self, transaction_id, frame, device_number):

        # We don't send multiple frame at one
        self._mutex_no_multiple_send.acquire()

        try:

            # We are trying to send the frame 3 times
            if self._dict_application_frame_in_progress[str(transaction_id)] < 3:

                # increase retry counter
                self._dict_application_frame_in_progress[str(transaction_id)] += 1
                # Send network frame to mac layout
                self._network_frame_generator.send_data_frame(device_number, frame)

            else:
                # Send error to applicative
                pass

        except Exception:
            self._receiver_alive = False
            raise

        # We finish to send the current frame
        self._mutex_no_multiple_send.release()
