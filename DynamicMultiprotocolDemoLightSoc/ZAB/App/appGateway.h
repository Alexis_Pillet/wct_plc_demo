#ifndef _APP_GATEWAY_H_
#define _APP_GATEWAY_H_

#include "zabCoreService.h"

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/
 extern 
 void appGateway_GetNetworkProcessorIeee(char* readNwkProcAppVersion);
 /******************************************************************************
 * Start the Gateway process
 ******************************************************************************/
extern 
void appGateway_Start(void);
 /******************************************************************************
 * Handle Open State Notifications
 ******************************************************************************/
extern 
void appGateway_OpenStateNtfHandler(zabOpenState openState);

/******************************************************************************
 * Handle Network State Notifications
 ******************************************************************************/
extern 
void appGateway_NwkStateNtfHandler(zabNwkState nwkState);

/******************************************************************************
 * Handle Network Info State Notifications
 ******************************************************************************/
extern 
void appGateway_NetworkInfoStateNtfHandler(zabNetworkInfoState networkInfoState);

/******************************************************************************
 * Handle Asks
 ******************************************************************************/
extern 
zab_bool appGateway_AskCfgHandler(erStatus* Status, zabService* Service, zabAskId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer);


/******************************************************************************
 * Handle Gives
 ******************************************************************************/
extern 
void appGateway_GiveCfgHandler(erStatus* Status, zabService* Service, zabGiveId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer);

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif /* _APP_ZCL_BASIC_H_ */