/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the profile wide client commands for the ZAB implementation
 *   of the SZL ZDO interface - PUBLIC HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *              23-Aug-13   MvdB   Original
 *              27-Jun-14   MvdB   Add User Descriptor support
 *              07-Jul-14   MvdB   ARTF60318: Add Power Descriptor Request
 * 002.000.001  03-Feb-15   MvdB   ARTF115770: Support ZDO Node Descriptor Request
 * 002.001.002  15-Jul-15   MvdB   ARTF136585: Add transaction IDs to over the air ZDO commands/responses
 * 002.002.015  21-Oct-15   MvdB   ARTF104106: Support SZL_ZDO_MatchDescriptorReq()
 *****************************************************************************/

#ifndef _SZL_ZDO_H_
#define _SZL_ZDO_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "szl.h"
#include "szl_zdo_types.h"
  
/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * ZDO IEEE Address Request / Response
 ******************************************************************************/  
extern 
SZL_RESULT_t SZL_ZDO_IeeeAddrReq(zabService* Service,
                                 SZL_CB_ZdoAddrResp_t Callback,
                                 SZL_ZdoIeeeAddrReqParams_t* Params,
                                 szl_uint8* TransactionId);

/******************************************************************************
 * ZDO IEEE Address Request / Response
 ******************************************************************************/  
extern 
SZL_RESULT_t SZL_ZDO_NwkAddrReq(zabService* Service,
                                SZL_CB_ZdoAddrResp_t Callback,
                                SZL_ZdoNwkAddrReqParams_t* Params,
                                szl_uint8* TransactionId);

/******************************************************************************
 * ZDO Power Descriptor Request / Response
 ******************************************************************************/ 
extern 
SZL_RESULT_t SZL_ZDO_PowerDescriptorReq(zabService* Service,
                                        SZL_CB_ZdoPowerDescriptorResp_t Callback,
                                        SZL_ZdoPowerDescriptorReqParams_t* Params,
                                        szl_uint8* TransactionId);


/******************************************************************************
* ZDO Node Descriptor Request / Response
******************************************************************************/
extern
SZL_RESULT_t SZL_ZDO_NodeDescriptorReq(zabService* Service,
                                       SZL_CB_ZdoNodeDescriptorResp_t Callback,
                                       SZL_ZdoNodeDescriptorReqParams_t* Params,
                                       szl_uint8* TransactionId);

/******************************************************************************
 * ZDO Active Endpoint Request / Response
 ******************************************************************************/  
extern 
SZL_RESULT_t SZL_ZDO_ActiveEndpointReq(zabService* Service,
                                       SZL_CB_ZdoActiveEndpointResp_t Callback,
                                       SZL_ZdoActiveEndpointReqParams_t* Params,
                                       szl_uint8* TransactionId);


/******************************************************************************
 * ZDO Simple Descriptor Request / Response
 ******************************************************************************/  
extern 
SZL_RESULT_t SZL_ZDO_SimpleDescriptorReq(zabService* Service,
                                         SZL_CB_ZdoSimpleDescriptorResp_t Callback,
                                         SZL_ZdoSimpleDescriptorReqParams_t* Params,
                                         szl_uint8* TransactionId);

/******************************************************************************
 * ZDO Match Descriptor Request / Response
 ******************************************************************************/ 
extern 
SZL_RESULT_t SZL_ZDO_MatchDescriptorReq(zabService* Service,
                                        SZL_CB_ZdoMatchDescriptorResp_t Callback,
                                        SZL_ZdoMatchDescriptorReqParams_t* Params,
                                        szl_uint8* TransactionId);

/******************************************************************************
 * ZDO MGMT Network Update Request - Energy Scan
 ******************************************************************************/ 
extern
SZL_RESULT_t SZL_ZDO_MgmtNwkUpdateReq_EnergyScan(zabService* Service,
                                                 SZL_CB_ZdoMgmtNwkUpdateResp_EnergyScan_t Callback,
                                                 SZL_ZdoMgmtNwkUpdateReq_EnergyScan_Params_t* Params,
                                                 szl_uint8* TransactionId);

/******************************************************************************
 * ZDO MGMT LQI Request / Response
 ******************************************************************************/ 
extern 
SZL_RESULT_t SZL_ZDO_MgmtLqiReq(zabService* Service,
                                SZL_CB_ZdoMgmtLqiResp_t Callback,
                                SZL_ZdoMgmtLqiReqParams_t* Params,
                                szl_uint8* TransactionId);

/******************************************************************************
 * ZDO MGMT RTG Request / Response
 ******************************************************************************/ 
SZL_RESULT_t SZL_ZDO_MgmtRtgReq(zabService* Service,
                                SZL_CB_ZdoMgmtRtgResp_t Callback,
                                SZL_ZdoMgmtRtgReqParams_t* Params,
                                szl_uint8* TransactionId);

/******************************************************************************
 * ZDO MGMT Bind Request / Response
 ******************************************************************************/ 
SZL_RESULT_t SZL_ZDO_MgmtBindReq(zabService* Service,
                                SZL_CB_ZdoMgmtBindResp_t Callback,
                                SZL_ZdoMgmtBindReqParams_t* Params,
                                 szl_uint8* TransactionId);

/******************************************************************************
 * ZDO Bind Request / Response
 ******************************************************************************/ 
extern 
SZL_RESULT_t SZL_ZDO_BindReq(zabService* Service,
                             SZL_CB_ZdoBindResp_t Callback,
                             SZL_ZdoBindReqParams_t* Params,
                             szl_uint8* TransactionId);

/******************************************************************************
 * ZDO UnBind Request / Response
 ******************************************************************************/ 
extern 
SZL_RESULT_t SZL_ZDO_UnBindReq(zabService* Service,
                               SZL_CB_ZdoBindResp_t Callback,
                               SZL_ZdoBindReqParams_t* Params,
                               szl_uint8* TransactionId);

/******************************************************************************
 * ZDO Device Announce - Register Indication Handler
 ******************************************************************************/ 
extern
SZL_RESULT_t SZL_ZDO_DeviceAnnounceNtfRegisterCB(zabService* Service,
                                                 SZL_CB_ZdoDeviceAnnounceInd_t Callback);

/******************************************************************************
 * ZDO Device Announce - Unregister Indication Handler
 ******************************************************************************/
extern
SZL_RESULT_t SZL_ZDO_DeviceAnnounceNtfUnregisterCB(zabService* Service,
                                                   SZL_CB_ZdoDeviceAnnounceInd_t Callback);

/******************************************************************************
 * ZDO User Descriptor Request / Response
 ******************************************************************************/ 
extern
SZL_RESULT_t SZL_ZDO_UserDescriptorReq(zabService* Service,
                                       SZL_CB_ZdoUserDescriptorResp_t Callback,
                                       SZL_ZdoUserDescriptorReqParams_t* Params,
                                       szl_uint8* TransactionId);

/******************************************************************************
 * ZDO User Descriptor Set Request / Response
 ******************************************************************************/ 
extern 
SZL_RESULT_t SZL_ZDO_UserDescriptorSetReq(zabService* Service,
                                          SZL_CB_ZdoUserDescriptorSetResp_t Callback,
                                          SZL_ZdoUserDescriptorSetReqParams_t* Params,
                                          szl_uint8* TransactionId);

/******************************************************************************
 *                      ******************************
 *                ***** INTERNAL FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/*******************************************************************************
 * Data in handler for the SZL ZDO
 ******************************************************************************/
extern 
void szlZdo_DataInHandler(erStatus* Status, zabService* Service, sapMsg* Message);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* _SZL_ZDO_H_ */

