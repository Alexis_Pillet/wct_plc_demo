/*******************************************************************************
  Filename    : gp_client.c
  $Date       : 2015-02-04                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Mark van den Broeke                                           $:

  Description : This is the Green Power Client plugin for the SZL
*******************************************************************************/
#ifndef _GP_CLIENT_H_
#define _GP_CLIENT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "szl.h"

  
/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/

/* Length of a GP Security KEy = 16 bytes*/
#define SZLPLUGIN_GPC_M_SECURITY_KEY_LENGTH 16  
  
/* Max length of ZIgBee Payload in translation table item */
#define SZLPLUGIN_GPC_M_ZIGBEE_PAYLOAD_LENGTH_MAX 16
#define SZLPLUGIN_GPC_M_ZIGBEE_PAYLOAD_LENGTH_FROM_GPD  0xFF
#define SZLPLUGIN_GPC_M_ZIGBEE_PAYLOAD_LENGTH_PARSED_FROM_GPD  0xFE
  
#define SZLPLUGIN_GPC_M_TT_ITEMS_MAX 6
  
/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

typedef szl_uint8 SZLPLUGIN_GPC_ACCESS_MODE_t;
typedef enum
{
  SZLPLUGIN_GPC_ACCESS_MODE_UNKNOWN     = 0,
  SZLPLUGIN_GPC_ACCESS_MODE_SOURCE_ID   = 1,  
  SZLPLUGIN_GPC_ACCESS_MODE_IEEE        = 2,
  SZLPLUGIN_GPC_ACCESS_MODE_INDEX       = 3,
}ENUM_SZLPLUGIN_GPC_ACCESS_MODE_t;

typedef struct
{
    SZLPLUGIN_GPC_ACCESS_MODE_t AccessMode;   /**< The method by which the GPD will be accessed */
    union {
      szl_uint32 GpSourceId;                  /**< If AccessMode == SZLPLUGIN_GPC_ACCESS_MODE_SOURCE_ID */
      szl_uint64 Ieee;                        /**< If AccessMode == SZLPLUGIN_GPC_ACCESS_MODE_IEEE */
      szl_uint8 Index;                        /**< If AccessMode == SZLPLUGIN_GPC_ACCESS_MODE_INDEX */
    } AccessParameters;
} SzlPlugin_GPC_SinkTableReqItem_t;

typedef struct
{
  SZL_EP_t SourceEndpoint;                    /**< The Endpoint for the source (App) */
  SZL_Addresses_t DestAddress;                /**< The Addressing mode to specify how the device communicates with the destination, either via binding table or direct addressing */
  SzlPlugin_GPC_SinkTableReqItem_t Item;      /**< Sink table item we will request */
} SzlPlugin_GPC_SinkTableReqParams_t;


typedef struct
{
  szl_uint16 ApplicationId:3;                 /**< 0 = 4B SourceId */
                                              /**< 2 = 8B IEEE */
  szl_uint16 CommunicationMode:2;             /**< 0 = Unicast forwarding light and full */
                                              /**< 1 = Groupcast forwarding of the GP Notification command to DGroupID */
                                              /**< 2 = Groupcast forwarding of the GP Notification command to pre-commissioned GroupID */
                                              /**< 3 = Unicast forwarding light */
  szl_uint16 SeqNumCap:1;                     /**< 0 = Random Mac Sequence Number */
                                              /**< 1 = Incremental Mac Sequence Number */
  szl_uint16 RxOnCap:1;                       /**< 0 = GPD has no receiving capabilities in operational mode */
                                              /**< 1 = GPD has receiving capabilities in operational mode */
  szl_uint16 FixedLocation:1;                 /**< 0 = GPD can change its position during its operation in the network */
                                              /**< 1 = GPD is not expected to change its position during its operation in the network*/
  szl_uint16 AssignedAlias:1;                 /**< 0 = Derived alias is used */
                                              /**< 1 = Assigned alias as stored in the GPD Assigned Alias parameter shall be used instead of the 
                                                       alias derived from the GPD ID in case of derived groupcast or unicast communication */
  szl_uint16 SecurityUsed:1;                  /**< 0 = Security-related parameters of the Sink Table entry are present */
                                              /**< 1 = Security-related parameters of the Sink Table entry are not present */
  szl_uint16 Reserved:6;                      /**< Reserved*/
} SZLPLUGIN_GPC_SINK_TABLE_OPTIONS_BITS_t;

/* Union to allow byte-wise access to the options */
typedef union
{
  SZLPLUGIN_GPC_SINK_TABLE_OPTIONS_BITS_t BitOptions;
  szl_uint16 ByteOptions;
} SZLPLUGIN_GPC_SINK_TABLE_OPTIONS_t;

/* Sink Table Item */
typedef struct
{
  SZLPLUGIN_GPC_SINK_TABLE_OPTIONS_t Options;
  szl_uint64 GpdId;
  szl_uint8 DeviceId;
  szl_uint16 GroupList[2];
  szl_uint16 AssignedAlias;
  szl_uint8 GroupcastRadius;
  szl_uint8 SecurityOptions;
  szl_uint32 SecurityFrameCounter;
  szl_uint8 SecurityKey[SZLPLUGIN_GPC_M_SECURITY_KEY_LENGTH];
} SzlPlugin_GPC_SinkTableItem_t;

/* Parameters of Sink Table Response */
typedef struct
{
  SZL_STATUS_t Status;
  szl_uint8 NumberOfSinkTableEntries;
  szl_uint8 StartIndex;
  szl_uint8 EntryCount;
  SzlPlugin_GPC_SinkTableItem_t SinkTableItem;
} SzlPlugin_GPC_SinkTableRspParams_t;









/******************************************************************************
 * Green Power Translation Table Request / Response
 ******************************************************************************/

typedef struct
{
  SZL_EP_t SourceEndpoint;                    /**< The Endpoint for the source (App) */
  SZL_Addresses_t DestAddress;                /**< The Addressing mode to specify how the device communicates with the destination, either via binding table or direct addressing */
  szl_uint8 StartIndex;                       /**< Start Index in the translation table */
} SzlPlugin_GPC_TranslationTableReqParams_t;


typedef struct
{
  szl_uint16 ApplicationId:3;                 /**< 0 = 4B SourceId */
                                              /**< 2 = 8B IEEE */
  szl_uint16 Reserved:5;                      /**< Reserved*/
} SZLPLUGIN_GPC_TRANSLATION_TABLE_OPTIONS_BITS_t;

/* Union to allow byte-wise access to the options */
typedef union
{
  SZLPLUGIN_GPC_TRANSLATION_TABLE_OPTIONS_BITS_t BitOptions;
  szl_uint8 ByteOptions;
} SZLPLUGIN_GPC_TRANSLATION_TABLE_OPTIONS_t;


/* Translation Table Item */
typedef struct
{
  szl_uint32 GpdId;
  szl_uint8 GpdCommandId;
  szl_uint8 Endpoint;
  szl_uint16 Profile;
  szl_uint16 Cluster;
  szl_uint8 ZigBeeCommandId;
  szl_uint8 ZigBeePayloadLength;
  szl_uint8 ZigBeepayload[SZLPLUGIN_GPC_M_ZIGBEE_PAYLOAD_LENGTH_MAX];
} SzlPlugin_GPC_TranslationTableItem_t;

/* Parameters of Translation Table Response */
typedef struct
{
  SZL_STATUS_t Status;
  SZLPLUGIN_GPC_TRANSLATION_TABLE_OPTIONS_t Options;
  szl_uint8 TotalNumberOfEntries;
  szl_uint8 StartIndex;
  szl_uint8 EntryCount;
  SzlPlugin_GPC_TranslationTableItem_t TranslationTableItems[SZLPLUGIN_GPC_M_TT_ITEMS_MAX];
} SzlPlugin_GPC_TranslationTableRspParams_t;



/******************************************************************************
 * Green Power Translation Table Update Request / Response
 ******************************************************************************/


typedef struct
{
  szl_uint16 ApplicationId:3;                 /**< 0 = 4B SourceId */
                                              /**< 2 = 8B IEEE */
  szl_uint16 Action:2;                        /**< 0 = Add*/
                                              /**< 1 = Replace */
                                              /**< 2 = Remove */
  szl_uint16 NumberOfTranslations:3;          /**< how many Translation fields are included in the command*/
  szl_uint16 Reserved:8;                      /**< Reserved*/
} SZLPLUGIN_GPC_TRANSLATION_TABLE_UPDATE_OPTIONS_BITS_t;

/* Union to allow byte-wise access to the options */
typedef union
{
  SZLPLUGIN_GPC_TRANSLATION_TABLE_UPDATE_OPTIONS_BITS_t BitOptions;
  szl_uint16 ByteOptions;
} SZLPLUGIN_GPC_TRANSLATION_TABLE_UPDATE_OPTIONS_t;


/* Translation Table Update Item */
typedef struct
{
  szl_uint8 Index;
  szl_uint8 GpdCommandId;
  szl_uint8 Endpoint;
  szl_uint16 Profile;
  szl_uint16 Cluster;
  szl_uint8 ZigBeeCommandId;
  szl_uint8 ZigBeePayloadLength;
  szl_uint8 ZigBeepayload[SZLPLUGIN_GPC_M_ZIGBEE_PAYLOAD_LENGTH_MAX];
} SzlPlugin_GPC_TranslationTableUpdateItem_t;

typedef struct
{
  SZL_EP_t SourceEndpoint;                    /**< The Endpoint for the source (App) */
  SZL_Addresses_t DestAddress;                /**< The Addressing mode to specify how the device communicates with the destination, either via binding table or direct addressing */
  SZLPLUGIN_GPC_TRANSLATION_TABLE_UPDATE_OPTIONS_t Options;
  szl_uint32 GpdId;
  SzlPlugin_GPC_TranslationTableUpdateItem_t Item[SZLPLUGIN_GPC_M_TT_ITEMS_MAX];
} SzlPlugin_GPC_TranslationTableUpdateReqParams_t;

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/  

/******************************************************************************
 * Green Power Sink Table Request
 ******************************************************************************/
extern   
SZL_RESULT_t SzlPlugin_GPC_SinkTableReq(zabService* Service,
                                        SZL_CB_ClusterCmdResp_t Callback, 
                                        SzlPlugin_GPC_SinkTableReqParams_t* Params);
  
/******************************************************************************
 * Green Power Sink Table Response - Parsing Function
 * 
 * This function may be called to parse parameters returned in SZL_CB_ClusterCmdResp_t Callback
 * in response to SzlPlugin_GPC_SinkTableReq().
 ******************************************************************************/
extern 
SZL_RESULT_t SzlPlugin_GPC_ParseSinkTableRsp(zabService* Service, SZL_STATUS_t Status, 
                                             SZL_ClusterCmdRespParams_t *Params, 
                                             SzlPlugin_GPC_SinkTableRspParams_t* SinkTableRspParams);
  
/******************************************************************************
 * Green Power Remove GPD from Sink Table Request
 ******************************************************************************/
extern   
SZL_RESULT_t SzlPlugin_GPC_SinkTableRemoveGpdReq(zabService* Service,
                                                 SZL_CB_ClusterCmdResp_t Callback, 
                                                 SzlPlugin_GPC_SinkTableReqParams_t* Params);
  
  
  
/******************************************************************************
 * Green Power Translation Table Request
 ******************************************************************************/
extern 
SZL_RESULT_t SzlPlugin_GPC_TranslationTableReq(zabService* Service,
                                               SZL_CB_ClusterCmdResp_t Callback, 
                                               SzlPlugin_GPC_TranslationTableReqParams_t* Params);


/******************************************************************************
 * Green Power Translation Table Response - Parsing Function
 ******************************************************************************/
extern 
SZL_RESULT_t SzlPlugin_GPC_ParseTranslationTableRsp(zabService* Service, SZL_STATUS_t Status, 
                                                    SZL_ClusterCmdRespParams_t *Params, 
                                                    SzlPlugin_GPC_TranslationTableRspParams_t* TranslationTableRspParams);


/******************************************************************************
 * Green Power Translation Table Update Request
 ******************************************************************************/
extern 
SZL_RESULT_t SzlPlugin_GPC_TranslationTableUpdateReq(zabService* Service,
                                                     SZL_CB_ClusterCmdResp_t Callback, 
                                                     SzlPlugin_GPC_TranslationTableUpdateReqParams_t* Params);

#ifdef __cplusplus
}
#endif

#endif /*_DISCOVERY_H_*/
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
