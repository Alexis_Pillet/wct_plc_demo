/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the SiLabs NCP Open State Machine - HEADER TYPES
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 * 002.002.041  18-Jan-17   MvdB   ARTF199128: Fix NCP networking after reception of a leave command
 * 002.002.043  20-Jan-17   MvdB   ARTF190099: Make ping time run time controllable for ZNP & NCP. Private APIs only at this time, for industrial tester.
 *****************************************************************************/
#ifndef __ZAB_NCP_OPEN_TYPES_H__
#define __ZAB_NCP_OPEN_TYPES_H__


#include "zabTypes.h"


/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Value to specify network processor ping is disabled */
#define ZAB_OPEN_M_NET_PROC_PING_DISABLED 0xFFFF

/* Current Item of the state machine */
typedef enum
{
  ZAB_OPEN_CURRENT_ITEM_NONE,
  ZAB_OPEN_CURRENT_ITEM_OPEN_SERIAL_GLUE,
  ZAB_OPEN_CURRENT_ITEM_OPENING_SOFT_RESET,
  ZAB_OPEN_CURRENT_ITEM_OPENING_VERSION,
  ZAB_OPEN_CURRENT_ITEM_OPENING_IEEE,
  ZAB_OPEN_CURRENT_ITEM_OPENING_TEST_FOR_SBL,

  ZAB_OPEN_CURRENT_ITEM_OPENING_SOFT_RESET_SILENT,
  ZAB_OPEN_CURRENT_ITEM_OPENING_VERSION_SILENT,

  ZAB_OPEN_CURRENT_ITEM_CLOSE_SERIAL_GLUE,
  ZAB_OPEN_CURRENT_ITEM_CLOSING_SOFT_RESET,

  ZAB_OPEN_CURRENT_ITEM_PING_SYS_VERSION,

  ZAB_OPEN_CURRENT_ITEM_GO_TO_SBL,
  ZAB_OPEN_CURRENT_ITEM_GO_TO_SBL_AWAIT_PROMPT,

} zabOpenCurrentItem;


/* State information for this state machine */
typedef struct
{
  zabOpenState openState;                     /* Current public state */
  zabOpenCurrentItem currentItem;             /* Current internal state - PRIVATE*/
  unsigned32 timeoutExpiryMs;                 /* Timeout for confirms - PRIVATE */
  unsigned32 lastNetworkProcessorPingTimeMs;  /* Time of last ping of network processor */
  unsigned16 networkProcessorPingTimeMs;      /* Rate of ping of network processor in ms */
  unsigned8 networkProcessorPingFailCount;    /* Number of consecutive times we failed to even send the ping */
} zabOpenInfo_t;


#endif /* __ZAB_NCP_OPEN_TYPES_H__ */
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/