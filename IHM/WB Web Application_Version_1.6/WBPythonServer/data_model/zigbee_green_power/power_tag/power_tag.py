# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to model Power Tag Device
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from abc import abstractmethod
from application import Attribute
from data_model.zigbee_green_power.zigbee_green_power_device import ZigBeeGreenPowerDevice
from json_parser import JsonLogs


class PowerTag(ZigBeeGreenPowerDevice):

    def __init__(self, address, id_wireless_bridge, application_frame_generator):
        # call super function
        super(PowerTag, self).__init__(address, id_wireless_bridge, application_frame_generator)
        # Initialization
        self._ELECTRICAL_MEASUREMENT_CLUSTER = "0b04"
        self._POWER_DIVISOR_ID = "0403"
        self._VOLTAGE_DIVISOR_ID = "0601"
        self._CURRENT_DIVISOR_ID = "0603"
        self._ACTIVE_POWER_PHA_ID = "050b"
        self._PHASE_SEQUENCE_ID = "4e10"
        self._AC_ALARMS_MASK_ID = "4800"
        self._ALARMS_MASK_ID = "0800"
        self._METERING_CLUSTER = "0702"
        self._DIVISOR_ID = "0302"
        self._CUMULATED_ENERGY_ID = "0000"
        self._PARTIAL_ENERGY_ID = "4000"
        self._divisor = 1
        self._power_divisor = 1
        self._voltage_divisor = 1
        self._current_divisor = 1
        self._phase_sequence = 0
        self._active_power_pha = 0
        self._cumulated_energy = 0
        self._partial_energy = 0
        self._ac_alarms_mask = 0
        self._alarms_mask = 0
        self._logs_to_json = JsonLogs("../logs.json")

    @abstractmethod
    def update_data(self, data):
        pass

    @abstractmethod
    def update_rssi(self, lqi, rssi):
        pass

    @abstractmethod
    def get_json_data(self):
        pass

    @abstractmethod
    def get_json_device(self, device_number):
        pass

    def read_phase_sequence(self):
        """Generate a get attribute command to read the phase sequence value of the product"""
        self.get_attribute(self._END_POINT_BASIC_CLUSTER, self._ELECTRICAL_MEASUREMENT_CLUSTER,
                           [self._PHASE_SEQUENCE_ID])

    def write_phase_sequence(self, value):
        """Generate a get attribute command to read the phase sequence value of the product

        Args:
            value: new value of phase sequence
        """
        phase_sequence = self._attributeType(self._PHASE_SEQUENCE_ID, Attribute.Type.ENUM_8, bytes([value]))
        self.set_attribute(self._END_POINT_BASIC_CLUSTER, self._ELECTRICAL_MEASUREMENT_CLUSTER, {phase_sequence})

    def reset_partial_energy(self):
        """Generate a set attribute command to reset partial energy"""
        partial_energy = self._attributeType(self._PARTIAL_ENERGY_ID, Attribute.Type.UINT_48,
                                             bytes([0x00, 0x00, 0x00, 0x00, 0x00, 0x00]))
        self.set_attribute(self._END_POINT_BASIC_CLUSTER, self._METERING_CLUSTER, {partial_energy})

    @property
    def divisor(self):
        return self._divisor

    @divisor.setter
    def divisor(self, divisor):
        self._divisor = divisor

    @property
    def power_divisor(self):
        return self._power_divisor

    @power_divisor.setter
    def power_divisor(self, power_divisor):
        self._power_divisor = power_divisor

    @property
    def voltage_divisor(self):
        return self._voltage_divisor

    @voltage_divisor.setter
    def voltage_divisor(self, voltage_divisor):
        self._voltage_divisor = voltage_divisor

    @property
    def current_divisor(self):
        return self._current_divisor

    @current_divisor.setter
    def current_divisor(self, current_divisor):
        self._current_divisor = current_divisor

    @property
    def phase_sequence(self):
        return self._phase_sequence

    @phase_sequence.setter
    def phase_sequence(self, phase_sequence):
        self._phase_sequence = phase_sequence

    @property
    def active_power_pha(self):
        return self._active_power_pha

    @active_power_pha.setter
    def active_power_pha(self, active_power_pha):
        self._active_power_pha = active_power_pha

    @property
    def cumulated_energy(self):
        return self._cumulated_energy

    @cumulated_energy.setter
    def cumulated_energy(self, cumulated_energy):
        self._cumulated_energy = cumulated_energy

    @property
    def partial_energy(self):
        return self._partial_energy

    @partial_energy.setter
    def partial_energy(self, partial_energy):
        self._partial_energy = partial_energy

    @property
    def ac_alarms_mask(self):
        return self._ac_alarms_mask

    @ac_alarms_mask.setter
    def ac_alarms_mask(self, ac_alarms_mask):
        self._ac_alarms_mask = ac_alarms_mask

    @property
    def alarms_mask(self):
        return self._alarms_mask

    @alarms_mask.setter
    def alarms_mask(self, alarms_mask):
        self._alarms_mask = alarms_mask
