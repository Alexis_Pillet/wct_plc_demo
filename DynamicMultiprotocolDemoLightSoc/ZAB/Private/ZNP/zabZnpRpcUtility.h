/*
 * zabZnpRpcUtility.h
 *
 *  Created on: 5 oct. 2012
 *      Author: SESA236777
 */

#ifndef ZABZNPRPCUTILITY_H_
#define ZABZNPRPCUTILITY_H_





#ifdef __cplusplus
extern "C" {
#endif

#include "osTypes.h"
#include "erStatusUtility.h" 
#include "sapTypes.h"
#include "zabTypes.h"


/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/
typedef enum
{
  ZNPI_CMD_RPC_COMMAND_ERROR          = 0x00,
  ZNPI_CMD_RPC_COMMAND_DEBUG_MSG      = 0x80,
} ZNPI_CMD_RPC_COMMANDCODES;  
  

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Process all messages of subsystem RPC
 ******************************************************************************/
extern 
void zabZnpRpcProcessMsg(erStatus* Status, zabService* Service, sapMsg* Message);



#ifdef __cplusplus
}
#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif /* ZABZNPRPCUTILITY_H_ */
