/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_main.c
*    @version
*        $Rev: 3384 $
*    @last editor
*        $Author: a5089752 $
*    @date
*        $Date:: 2017-05-31 13:49:31 +0900#$
* Description :
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include "r_typedefs.h"
#include "r_stdio_api.h"

/* g3 part */
#include "r_c3sap_api.h"


#include "r_dflash_lib.h"
#include "r_demo_app.h"
#include "r_demo_status2text.h"
#include "r_demo_nvm_process.h"
#include "r_demo_print.h"

/******************************************************************************
Macro definitions
******************************************************************************/
#define APL_PRINTF(x, ...)  (R_STDIO_Printf (__VA_ARGS__))

/******************************************************************************
Typedef definitions
******************************************************************************/
typedef uint32_t (* FP_CONV_IBID2TXT)(uint16_t id, const uint8_t ** txt, uint8_t * len);

/******************************************************************************
Private global variables and functions
******************************************************************************/
/******************************************************************************
Exported global variables
******************************************************************************/
extern const uint8_t g_rom_nvm_sycnword[];

/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/
/******************************************************************************
Functions
******************************************************************************/



/******************************************************************************
* Function Name: r_demo_print_version
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_print_version ()
{
    APL_PRINTF (0xF, "\n\n==========================================================\r\n\r\n");

    APL_PRINTF (0xF, "           RENESAS G3PLC DEMO APPLICATION         \r\n\r\n");

    APL_PRINTF (0xF, "==========================================================\n");
    APL_PRINTF (0xF, "                      (build date: "__DATE__ " "__TIME__ ")\r\n");
}
/******************************************************************************
   End of function  r_demo_print_version
******************************************************************************/


/******************************************************************************
* Function Name: r_demo_print_cert_version
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_print_cert_version (uint8_t hwVer, uint16_t swVer)
{
    APL_PRINTF (0xF, "\n\n==========================================================\r\n\r\n");

    APL_PRINTF (0xF, "        RENESAS G3PLC CERTIFICATION APP (ver %01d.%02d.%02d)         \r\n\r\n", hwVer, swVer >> 8, swVer & 0xFF);

    APL_PRINTF (0xF, "==========================================================\n");
    APL_PRINTF (0xF, "                      (build date: "__DATE__ " "__TIME__ ")\r\n");
}
/******************************************************************************
   End of function  r_demo_print_cert_version
******************************************************************************/

/******************************************************************************
* Function Name: r_demo_print_devinfo_eui64
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void r_demo_print_devinfo_eui64 (uint8_t dev_id, uint8_t * eui64)
{
    uint8_t * pC;

    UNUSED (dev_id);

    pC = eui64;
    APL_PRINTF (dev_id, "  EUI64               :");
    APL_PRINTF (dev_id, "0x%02X%02X_%02X%02X_%02X%02X_%02X%02X\r\n",
                pC[0], pC[1], pC[2], pC[3], pC[4], pC[5], pC[6], pC[7]);

}
/******************************************************************************
   End of function  r_demo_print_devinfo_eui64
******************************************************************************/

/******************************************************************************
* Function Name: r_demo_print_devinfo_psk
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void r_demo_print_devinfo_psk (uint8_t dev_id, uint8_t * psk)
{
    uint8_t * pC;

    UNUSED (dev_id);

    pC = psk;
    APL_PRINTF (dev_id, "  PSK                 :");
    APL_PRINTF (dev_id, "0x%02X%02X_%02X%02X_%02X%02X_%02X%02X_%02X%02X_%02X%02X_%02X%02X_%02X%02X\r\n",
                pC[0], pC[1], pC[2], pC[3], pC[4], pC[5], pC[6], pC[7],
                pC[8], pC[9], pC[10], pC[11], pC[12], pC[13], pC[14], pC[15]);

}
/******************************************************************************
   End of function  r_demo_print_devinfo_psk
******************************************************************************/


/******************************************************************************
* Function Name: r_demo_print_devinfo_coordshort
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void r_demo_print_devinfo_coordshort (uint8_t dev_id, uint8_t * coordAddr)
{
    uint8_t * pC;

    UNUSED (dev_id);

    pC = coordAddr;
    APL_PRINTF (dev_id, "  CoordAddr           :");
    APL_PRINTF (dev_id, "0x%02X%02X\r\n\r\n", pC[0], pC[1]);
}
/******************************************************************************
   End of function  r_demo_print_devinfo_coordshort
******************************************************************************/




/******************************************************************************
* Function Name: r_demo_print_devinfo_panid
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void r_demo_print_devinfo_panid (uint8_t dev_id, uint8_t * panid)
{
    uint8_t * pC;

    UNUSED (dev_id);

    pC = panid;
    APL_PRINTF (dev_id, "  Start PanID         :");
    APL_PRINTF (dev_id, "0x%02X%02X\r\n", pC[0], pC[1]);
}
/******************************************************************************
   End of function  r_demo_print_devinfo_panid
******************************************************************************/



/******************************************************************************
* Function Name: r_demo_print_gmk_1
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void r_demo_print_gmk_1 (uint8_t dev_id, uint8_t * gmk, uint8_t index)
{
    uint8_t * pC;

    UNUSED (dev_id);

    pC = gmk;
    APL_PRINTF (dev_id, "  GMK[index%d]         :", index);
    APL_PRINTF (dev_id, "0x%02X%02X_%02X%02X_%02X%02X_%02X%02X_%02X%02X_%02X%02X_%02X%02X_%02X%02X\r\n",
                pC[0], pC[1], pC[2], pC[3], pC[4], pC[5], pC[6], pC[7],
                pC[8], pC[9], pC[10], pC[11], pC[12], pC[13], pC[14], pC[15]);

}
/******************************************************************************
   End of function  r_demo_print_gmk_1
******************************************************************************/



/******************************************************************************
* Function Name: r_demo_print_devinfo_extid_1
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void r_demo_print_devinfo_extid_1 (uint8_t dev_id, r_demo_extid_t * extid, uint8_t index)
{
    uint8_t * pC;
    uint8_t   i;

    UNUSED (dev_id);

    pC = extid->id;

    if (0 == index)
    {
        APL_PRINTF (dev_id, "  ID_P(%02dByte)      :0x", extid->length);
    }
    else
    {
        APL_PRINTF (dev_id, "  ID_S(%02dByte)      :0x", extid->length);
    }

    for (i = 0; i < extid->length; i++)
    {
        if (((i & 0xF) == 0) && (0 != i))
        {
            APL_PRINTF (dev_id, "\r\n                      ");
        }
        if (((i & 0x1) == 0) && (0 != i))
        {
            APL_PRINTF (dev_id, "_");
        }
        APL_PRINTF (dev_id, "%02X", pC[i]);
    }
    APL_PRINTF (dev_id, "\r\n");

} /* r_demo_print_devinfo_extid_1 */
/******************************************************************************
   End of function  r_demo_print_devinfo_extid_1
******************************************************************************/


/******************************************************************************
* Function Name: r_demo_print_gmk
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void r_demo_print_gmk (uint8_t dev_id, r_cap_dev_cfg_t * config)
{
    r_demo_print_gmk_1 (dev_id, config->gmk[0], 0);
    r_demo_print_gmk_1 (dev_id, config->gmk[1], 1);
}
/******************************************************************************
   End of function  r_demo_print_gmk
******************************************************************************/



/******************************************************************************
* Function Name: r_demo_print_devinfo_extid
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void r_demo_print_devinfo_extid (uint8_t dev_id, r_cap_dev_cfg_t * config)
{
    r_demo_print_devinfo_extid_1 (dev_id, &config->extID[0], 0);
    r_demo_print_devinfo_extid_1 (dev_id, &config->extID[1], 1);
}
/******************************************************************************
   End of function  r_demo_print_devinfo_extid
******************************************************************************/


/******************************************************************************
* Function Name: r_demo_print_devinfo
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_print_devinfo (uint8_t dev_id, r_cap_dev_cfg_t * config)
{
    APL_PRINTF (dev_id, "  ----- Device Config -----\r\n\r\n");

    r_demo_print_devinfo_eui64 (dev_id, config->extendedAddress);

    r_demo_print_devinfo_psk (dev_id, config->psk);

    r_demo_print_devinfo_coordshort (dev_id, config->coordAddr);

}
/******************************************************************************
   End of function  r_demo_print_devinfo
******************************************************************************/


/******************************************************************************
* Function Name: r_demo_print_bakup
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_print_bakup (uint8_t dev_id, uint8_t * param)
{
    r_demo_backup_payload_t * pBkup = (r_demo_backup_payload_t *)param;

    UNUSED (dev_id);

    APL_PRINTF (dev_id, "  ----- Preserved Info -----\r\n\r\n");
    APL_PRINTF (dev_id, "  Prev DeviceType     :0x%02X\r\n", pBkup->DeviceType);
    APL_PRINTF (dev_id, "  Prev RouteType      :0x%02X\r\n", pBkup->RouteType);
    APL_PRINTF (dev_id, "  Prev ActiveKeyIndex :0x%02X\r\n", pBkup->ActiveKeyIndex);
    APL_PRINTF (dev_id, "  Prev FrameCounter   :0x%02X%02X%02X%02X\r\n", pBkup->FrameCounter[0], pBkup->FrameCounter[1], pBkup->FrameCounter[2], pBkup->FrameCounter[3]);
    APL_PRINTF (dev_id, "  Prev PanID          :0x%02X%02X\r\n", pBkup->PanId[0], pBkup->PanId[1]);
    APL_PRINTF (dev_id, "  Prev ShortAddress   :0x%02X%02X\r\n", pBkup->NetworkAddr[0], pBkup->NetworkAddr[1]);
    APL_PRINTF (dev_id, "  Prev LoadSeqNo      :0x%02X%02X\r\n", pBkup->LoadSeqNo[0], pBkup->LoadSeqNo[1]);
}
/******************************************************************************
   End of function  r_demo_print_bakup
******************************************************************************/

/******************************************************************************
* Function Name: r_demo_print_bandplan
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_print_bandplan (r_g3_bandplan_t bandPlan)
{
    APL_PRINTF (dev_id, "\r\n------------------------------------- BandPlan: ");

    if (R_G3_BANDPLAN_CENELEC_A == bandPlan)
    {
        APL_PRINTF (dev_id, "CENELEC-A");
    }
    else if (R_G3_BANDPLAN_CENELEC_B == bandPlan)
    {
        APL_PRINTF (dev_id, "CENELEC-B");
    }
    else if (R_G3_BANDPLAN_ARIB == bandPlan)
    {
        APL_PRINTF (dev_id, "ARIB");
    }
    else if (R_G3_BANDPLAN_FCC == bandPlan)
    {
        APL_PRINTF (dev_id, "FCC");
    }
    else
    {
        /**/
    }
} /* r_demo_print_bandplan */
/******************************************************************************
   End of function  r_demo_print_bandplan
******************************************************************************/

/******************************************************************************
* Function Name: r_demo_print_config
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_print_config (uint8_t dev_id, r_demo_config_t * demo, r_cap_dev_cfg_t * config, r_demo_backup_t * bkup)
{
    uint8_t * toneMask = demo->tonemask;
    APL_PRINTF (dev_id, "\r\n==========================================================\r\n\r\n");

    if (R_G3_BANDPLAN_CENELEC_A == demo->bandPlan)
    {
        APL_PRINTF (dev_id, "  BandPlan            :CENELEC-A\r\n");
        APL_PRINTF (dev_id, "  toneMask            :0x000000000%X%02X%02X%02X%02X\r\n", toneMask[4] & 0xF, toneMask[5], toneMask[6], toneMask[7], toneMask[8]);
    }
    else if (R_G3_BANDPLAN_CENELEC_B == demo->bandPlan)
    {
        APL_PRINTF (dev_id, "  BandPlan            :CENELEC-B\r\n");
        APL_PRINTF (dev_id, "  toneMask            :0x00000000000000%02X%02X\r\n", toneMask[7], toneMask[8]);
    }
    else if (R_G3_BANDPLAN_ARIB == demo->bandPlan)
    {
        APL_PRINTF (dev_id, "  BandPlan            :ARIB\r\n");
        APL_PRINTF (dev_id, "  toneMask            :0x0000%02X%02X%02X%02X%02X%02X%02X\r\n", (toneMask[2] & 0x3), toneMask[3], toneMask[4], toneMask[5], toneMask[6], toneMask[7], toneMask[8]);
    }
    else if (R_G3_BANDPLAN_FCC == demo->bandPlan)
    {
        APL_PRINTF (dev_id, "  BandPlan            :FCC\r\n");
        APL_PRINTF (dev_id, "  toneMask            :0x%02X%02X%02X%02X%02X%02X%02X%02X%02X\r\n", toneMask[0], toneMask[1], toneMask[2], toneMask[3], toneMask[4], toneMask[5], toneMask[6], toneMask[7], toneMask[8]);
    }
    else
    {
        /**/
    }
    if (R_ADP_DEVICE_TYPE_COORDINATOR == demo->devType)
    {
        if (R_G3_ROUTE_TYPE_NORMAL == demo->routeType)
        {
            APL_PRINTF (dev_id, "  Route Type          :A\r\n");
            APL_PRINTF (dev_id, "  Device Type         :Coordinator\r\n\r\n");
            r_demo_print_devinfo (dev_id, config);
            r_demo_print_gmk (dev_id, config);
            r_demo_print_devinfo_panid (dev_id, config->panid);
        }
        else
        {
            APL_PRINTF (config->bootCfg.id, "  Route Type          :B\r\n");
            APL_PRINTF (config->bootCfg.id, "  Device Type         :Coordinator\r\n\r\n");
            r_demo_print_devinfo (dev_id, config);
            r_demo_print_devinfo_extid (dev_id, config);
            r_demo_print_gmk (dev_id, config);
            r_demo_print_devinfo_panid (dev_id, config->panid);
        }
        APL_PRINTF (dev_id, "  activeKeyIndex      :");
        APL_PRINTF (dev_id, "0x%02X\r\n", demo->activeKeyIndex);
    }
    else
    {
        if (R_G3_ROUTE_TYPE_NORMAL == demo->routeType)
        {
            APL_PRINTF (config->bootCfg.id, "  Route Type          :A\r\n");
            APL_PRINTF (config->bootCfg.id, "  Device Type         :Peer\r\n\r\n");
            r_demo_print_devinfo (dev_id, config);
        }
        else
        {
            APL_PRINTF (config->bootCfg.id, "  Route Type          :B\r\n");
            APL_PRINTF (config->bootCfg.id, "  Device Type         :Peer\r\n\r\n");
            r_demo_print_devinfo (dev_id, config);
            r_demo_print_devinfo_extid (dev_id, config);
        }
    }

    if (NULL != bkup)
    {
        if (R_memcmp (g_rom_nvm_sycnword, bkup->SyncWord, 4) == 0)
        {
            r_demo_print_bakup (dev_id, (uint8_t *)&bkup->bkup);
        }
    }

    APL_PRINTF (dev_id, "\r\n==========================================================\r\n\r\n");

} /* r_demo_print_config */
/******************************************************************************
   End of function  r_demo_print_config
******************************************************************************/


/******************************************************************************
* Function Name: r_demo_print_frame_conter
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_print_frame_conter (uint8_t id, uint32_t FrameCounter)
{
    UNUSED (id);
    APL_PRINTF (id, "\r\n#### [ Stored FrameCounter = 0x%08X ] ####\r\n", FrameCounter);
}
/******************************************************************************
   End of function  r_demo_print_frame_conter
******************************************************************************/



/******************************************************************************
* Function Name: r_demo_print_devinfo_all
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_print_devinfo_all (uint8_t dev_id, r_cap_dev_cfg_t * config)
{
    APL_PRINTF (dev_id, "  ----- Device Config -----\r\n\r\n");

    r_demo_print_devinfo_eui64 (dev_id, config->extendedAddress);

    r_demo_print_devinfo_psk (dev_id, config->psk);

    r_demo_print_devinfo_coordshort (dev_id, config->coordAddr);

    r_demo_print_devinfo_panid (dev_id, config->panid);

    r_demo_print_gmk_1 (dev_id, config->gmk[0], 0);
    r_demo_print_gmk_1 (dev_id, config->gmk[1], 1);

    r_demo_print_devinfo_extid_1 (dev_id, &config->extID[0], 0);
    r_demo_print_devinfo_extid_1 (dev_id, &config->extID[1], 1);

} /* r_demo_print_devinfo_all */
/******************************************************************************
   End of function  r_demo_print_devinfo_all
******************************************************************************/


/******************************************************************************
* Function Name: r_demo_print_devinfo_sel
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_print_devinfo_sel (uint8_t dev_id, uint8_t part, uint8_t * param)
{

    switch (part)
    {
        case NVM_CFGPART_ALL:
            r_demo_print_devinfo_all (dev_id, (r_cap_dev_cfg_t *)param);
            break;

        case NVM_CFGPART_EUI64:
            r_demo_print_devinfo_eui64 (dev_id, param);
            break;

        case NVM_CFGPART_PSK:
            r_demo_print_devinfo_psk (dev_id, param);
            break;

        case NVM_CFGPART_COORDADDR:
            r_demo_print_devinfo_coordshort (dev_id, param);
            break;

        case NVM_CFGPART_PANID:
            r_demo_print_devinfo_panid (dev_id, param);
            break;

        case NVM_CFGPART_GMK0:
            r_demo_print_gmk_1 (dev_id, param, 0);
            break;

        case NVM_CFGPART_GMK1:
            r_demo_print_gmk_1 (dev_id, param, 1);
            break;

        case NVM_CFGPART_EXTID_P:
            r_demo_print_devinfo_extid_1 (dev_id, (r_demo_extid_t *)param, 0);
            break;

        case NVM_CFGPART_EXTID_S:
            r_demo_print_devinfo_extid_1 (dev_id, (r_demo_extid_t *)param, 1);
            break;

        default:
            break;
    } /* switch */

    return;
} /* r_demo_print_devinfo_sel */
/******************************************************************************
   End of function  r_demo_print_devinfo_sel
******************************************************************************/




/******************************************************************************
* Function Name: r_demo_disp_ib_info
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_disp_ib_info (uint8_t Layer)
{
    uint16_t     end_cnt = 0;

    uint16_t     i;
    const char * pTxt;
    uint8_t      len     = 0;


    switch (Layer)
    {
        case R_G3_MODE_MAC:
            end_cnt = 0x120;
            break;

        case R_G3_MODE_ADP:
            end_cnt = 0xFF;
            break;

        case R_G3_MODE_EAP:
            end_cnt = 0xFF;
            break;

        default:
            break;
    }

    APL_PRINTF (0xF, "\r\n==========================================================\r\n\r\n");

    APL_PRINTF (0xF, "   ID: (length) names\r\n");

    if (R_G3_MODE_MAC == Layer)
    {
        for (i = 0; i < end_cnt; i++)
        {
            pTxt = ibid_to_text (Layer, i, &len);
            if (0xFF == len)
            {
                continue;
            }
            if (len)
            {
                APL_PRINTF (0xF, "  0x%04X: (%dB) %s\r\n", i, len, pTxt);
            }
            else
            {
                APL_PRINTF (0xF, "  0x%04X: <T>   %s\r\n", i, pTxt);
            }
        }

        for (i = 0x800; i < 0x820; i++)
        {
            pTxt = ibid_to_text (Layer, i, &len);
            if (0xFF == len)
            {
                continue;
            }
            if (len)
            {
                APL_PRINTF (0xF, "  0x%04X: (%dB) %s\r\n", i, len, pTxt);
            }
            else
            {
                APL_PRINTF (0xF, "  0x%04X: <T>   %s\r\n", i, pTxt);
            }
        }
        for (i = 0x8340; i < 0x8350; i++)
        {
            pTxt = ibid_to_text (Layer, i, &len);
            if (0xFF == len)
            {
                continue;
            }
            if (len)
            {
                APL_PRINTF (0xF, "  0x%04X: (%dB) %s\r\n", i, len, pTxt);
            }
            else
            {
                APL_PRINTF (0xF, "  0x%04X: <T>   %s\r\n", i, pTxt);
            }
        }

    }
    else
    {
        for (i = 0; i < end_cnt; i++)
        {
            pTxt = ibid_to_text (Layer, i, &len);
            if (0xFF == len)
            {
                continue;
            }
            if (len)
            {
                APL_PRINTF (0xF, "  0x%02X: (%dB) %s\r\n", i, len, pTxt);
            }
            else
            {
                APL_PRINTF (0xF, "  0x%02X: <T>   %s\r\n", i, pTxt);
            }
        }

    }
} /* r_demo_disp_ib_info */
/******************************************************************************
   End of function  r_demo_disp_ib_info
******************************************************************************/

