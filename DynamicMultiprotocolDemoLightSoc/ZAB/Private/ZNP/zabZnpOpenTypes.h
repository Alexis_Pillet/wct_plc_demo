/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the types for the open state machine for the ZAB Pro ZNP
 *   vendor.
 *   For details and flow charts see XXX.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *  xx.xx.xx.xx 16-Oct-13   MvdB   Original
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 * 002.002.024  28-Jun-16   MvdB   ARTF172101: Implement ZNP ping to handle lost Reset indication when hidden inside a serial command that was not completed before reset
 * 002.002.043  20-Jan-17   MvdB   ARTF190099: Make ping time run time controllable for ZNP & NCP. Private APIs only at this time, for industrial tester.
 *****************************************************************************/
#ifndef __ZAB_ZNP_OPEN_TYPES_H__
#define __ZAB_ZNP_OPEN_TYPES_H__


#include "zabTypes.h"


/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Value to specify network processor ping is disabled */
#define ZAB_OPEN_M_NET_PROC_PING_DISABLED 0xFFFF

/* Current Item of the state machine */
typedef enum
{
  ZAB_OPEN_CURRENT_ITEM_NONE,
  ZAB_OPEN_CURRENT_ITEM_OPEN_SERIAL_GLUE,
  ZAB_OPEN_CURRENT_ITEM_CLOSE_SERIAL_GLUE,
  ZAB_OPEN_CURRENT_ITEM_SBL_VERSION,
  ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_ZNP,
  ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_SBL_WAIT_CLOSE,
  ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_SBL_WAIT_BEFORE_REOPEN,
  ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_SBL_WITHOUT_CLOSE,
  ZAB_OPEN_CURRENT_ITEM_SBL_REOPEN,

  ZAB_OPEN_CURRENT_ITEM_SYS_VERSION,
  ZAB_OPEN_CURRENT_ITEM_SBL_APP_VERSION,
  ZAB_OPEN_CURRENT_ITEM_SBL_START_APP,
  ZAB_OPEN_CURRENT_ITEM_SYS_RESET_FROM_SBL,

  ZAB_OPEN_CURRENT_ITEM_GET_IEEE_BEFORE_OPENED,
  ZAB_OPEN_CURRENT_ITEM_GET_ANTENNA_BEFORE_OPENED,

  ZAB_OPEN_CURRENT_ITEM_STOP_NP_DURING_CLOSE,

  ZAB_OPEN_CURRENT_ITEM_PING_SYS_VERSION,
  ZAB_OPEN_CURRENT_ITEM_SBL_LAUNCH_APP,

} zabOpenCurrentItem;


/* State information for this state machine */
typedef struct
{
  zabOpenState openState;                     /* Current public state */
  zabOpenCurrentItem currentItem;             /* Current internal state */
  unsigned32 timeoutExpiryMs;                 /* Timeout for confirms */
  unsigned32 lastNetworkProcessorPingTimeMs;  /* Time of last ping of network processor */
  unsigned16 networkProcessorPingTimeMs;      /* Rate of ping of network processor in ms */
  unsigned8 networkProcessorSendPingFailCount;/* Number of consecutive times we failed to even send the ping */
} zabOpenInfo_t;



#endif /* __ZAB_ZNP_OPEN_TYPES_H__ */
