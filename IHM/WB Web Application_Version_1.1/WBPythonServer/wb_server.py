# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to start the web server and serial management
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from bottle import route, run, hook, response, static_file, request
from core_engine import CoreEngine

core_engine = CoreEngine()
core_engine.start()


@hook('after_request')  # These lines are needed for avoiding the "Access-Control-Allow-Origin" errors
def enable_header():
    response.headers['Access-Control-Allow-Origin'] = '*'


# Main Page web Application
@route('/WirelessBridge')
def get_file():
    filename = "index.html"
    if not core_engine.check_network_init():
        filename = "error_network_not_init.html"
    return static_file(filename, root="../", mimetype="text/html")


@route('/WirelessBridge', method='POST')
def send_cluster_command():
    id_wireless_bridge = request.forms.get('id_wireless_bridge')
    address = request.forms.get('address')
    type_device = request.forms.get('type_device')
    name_function = request.forms.get('name_function')

    if type_device == 'iact':

        if name_function == 'open':

            if core_engine.zigbee_device_container.is_device_saved(id_wireless_bridge=id_wireless_bridge,
                                                                   address=address):
                iact_devic = core_engine.zigbee_device_container.get_device(id_wireless_bridge=id_wireless_bridge,
                                                                            address=address)
                iact_devic.open()

        elif name_function == 'close':

            if core_engine.zigbee_device_container.is_device_saved(id_wireless_bridge=id_wireless_bridge,
                                                                   address=address):
                iact_devic = core_engine.zigbee_device_container.get_device(id_wireless_bridge=id_wireless_bridge,
                                                                            address=address)
                iact_devic.close()


@route('/<name>.json')
def index(name):
    filename = name + ".json"
    return static_file(filename, root="../", mimetype="text/json")


@route('/css/<name>.css')
def index(name):
    filename = name + ".css"
    return static_file(filename, root="../css/", mimetype="text/css")


@route('/js/jtree/themes/default/<name>.css')
def index(name):
    filename = name + ".css"
    return static_file(filename, root="../js/jtree/themes/default/", mimetype="text/css")


@route('/js/jtree/themes/default/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../js/jtree/themes/default/", mimetype="text/html")


@route('/js/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../js/", mimetype="application/javascript")


@route('/js/jtree/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../js/jtree/", mimetype="application/javascript")


@route('/images/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../images/", mimetype="image/png")


@route('/fonts/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../fonts/", mimetype="font/woff2")


run(host='localhost', port=8080, server='waitress')  # Start web server
# Function run is a blocking call, once the server is stopped => we stop all processes
core_engine.stop()
