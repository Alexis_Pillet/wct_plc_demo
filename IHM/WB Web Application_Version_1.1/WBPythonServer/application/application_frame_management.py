
"""
application.application_frame_management
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to treat the received applicative frames (update data model)

"""


class ApplicationFrameManagement(object):

    def __init__(self):
        pass

    def update_device_data(self, device_number, payload):
        pass

    def update_device_link_indicator(self, device_number, payload):
        pass

    def cluster_command(self, device_number, payload):
        pass
