/*******************************************************************************
  Filename    : gp_client.c
  $Date       : 2015-02-04                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Mark van den Broeke                                           $:

  Description : This is the Green Power Client plugin for the SZL
 * 
 * MODIFICATION HISTORY:
 *  ZAB Rev       Date     Author  Change Description
 * 01.100.06.00 10-Feb-15   MvdB   Original
 * 002.001.001  29-Apr-15   MvdB   artf132252: Error when OS_MEM_UTILITY_USE_MALLOC_ID is not defined
 * 002.001.001  15-Jul-15   MvdB   ARTF136585: Add transaction IDs to over the air commands/responses
 * 002.002.010  09-Oct-15   MvdB   ARTF150980: Support Service ID in memory allocation functions for WTB
 * 002.002.019  24-Nov-15   MvdB   Support ZGP TransTable Req & Update for testing
 * 002.002.022  24-May-16   MvdB   Enable SendPairing on sink table remove
 *******************************************************************************/

#include "zabCoreService.h"
#include "zabCorePrivate.h"
#include "szl_external.h"
#include "szl_plugin.h"

#include "gp_client.h"

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

#define M_GP_CLUSTER_ID 0x0021


#define M_SINK_TABLE_REQ_MAX_PAYLOAD_LENGTH 9

#define M_SINK_TABLE_RSP_MIN_PAYLOAD 4
#define M_DEFAULT_RSP_PAYLOAD_LENGTH 2


#define M_SINK_TABLE_REMOVE_GPD_REQ_MAX_PAYLOAD_LENGTH 13


#define M_TRANSLATION_TABLE_REQ_MAX_PAYLOAD_LENGTH 1

#define M_TRANSLATION_TABLE_RSP_MIN_PAYLOAD 5
#define M_TRANSLATION_TABLE_RSP_MIN_ITEM_SIZE 12


typedef enum
{
  GPC_REQ_COMMAND_ID_TRANSLATION_TABLE_UPDATE = 0x07,
  GPC_REQ_COMMAND_ID_TRANSLATION_TABLE_REQ = 0x08,
  GPC_REQ_COMMAND_ID_PAIRING_CONFIGURATION_REQ = 0x09,
  GPC_REQ_COMMAND_ID_SINK_TABLE_REQ = 0x0A,
}GPC_REQ_COMMAND_ID_t;

typedef enum
{
  GPC_RSP_COMMAND_ID_TRANSLATION_TABLE_RSP = 0x08,
  GPC_RSP_COMMAND_ID_SINK_TABLE_RSP = 0x0A,
}GPC_RSP_COMMAND_ID_t;

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

static szl_uint8 getSinkTableItemLengthFromOptions(SZLPLUGIN_GPC_SINK_TABLE_OPTIONS_BITS_t Options)
{
  // SrcId(4) + DeciveId(1) + GroupCastRad(1)
  szl_uint8 length = 6;
  
  if (Options.ApplicationId == 0x02)
    {
      // IEEE(8), but we already have 4 for SrcId
      length += 4;
    }
  
  if (Options.CommunicationMode == 0x02)
    {
      // Group List
      length += 4;
    }
  
  if (Options.AssignedAlias)
    {
      // Alias
      length += 2;
    }
    
  if (Options.SecurityUsed)
    {
      // Security Options(1) and Key
      length += (1 + SZLPLUGIN_GPC_M_SECURITY_KEY_LENGTH);
    }

  if ( (Options.SecurityUsed) ||
       (Options.SeqNumCap) )
    {
      // SecurityFrameCounter
      length += 2;
    }
  return length;
}


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/ 

/******************************************************************************
 * Green Power Sink Table Request
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_GPC_SinkTableReq(zabService* Service,
                                        SZL_CB_ClusterCmdResp_t Callback, 
                                        SzlPlugin_GPC_SinkTableReqParams_t* Params)
{
  SZL_RESULT_t res = SZL_RESULT_INTERNAL_LIB_ERROR;
  SZL_ClusterCmdReqParams_t* cmdReq;
            
  if ( (Service == NULL) ||
       (Callback == NULL) ||
       (Params == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
          
  cmdReq = (SZL_ClusterCmdReqParams_t*)szl_mem_alloc(SZL_ClusterCmdReqParams_t_SIZE(M_SINK_TABLE_REQ_MAX_PAYLOAD_LENGTH), 
                                                     MALLOC_ID_PLG_GPC_CMD_REQ);
  if (cmdReq != NULL)
    {
      szl_memset(cmdReq, 0, SZL_ClusterCmdReqParams_t_SIZE(M_SINK_TABLE_REQ_MAX_PAYLOAD_LENGTH));  
      
      
      cmdReq->SourceEndpoint = Params->SourceEndpoint;      
      cmdReq->DestAddrMode = Params->DestAddress;
      cmdReq->ManufacturerSpecific = szl_false;
      cmdReq->ClusterID = M_GP_CLUSTER_ID;
      cmdReq->Command = GPC_REQ_COMMAND_ID_SINK_TABLE_REQ;
      cmdReq->Direction = ZCL_FRAME_DIR_CLIENT_SERVER;
      
      switch (Params->Item.AccessMode)
        {
          case SZLPLUGIN_GPC_ACCESS_MODE_SOURCE_ID:
            cmdReq->Payload[0] = 0x00;
            COPY_OUT_32_BITS(&cmdReq->Payload[1], Params->Item.AccessParameters.GpSourceId);
            cmdReq->PayloadLength = 5;
            break;
            
          case SZLPLUGIN_GPC_ACCESS_MODE_IEEE:
            cmdReq->Payload[0] = 0x02;
            COPY_OUT_64_BITS(&cmdReq->Payload[1], Params->Item.AccessParameters.Ieee);
            cmdReq->PayloadLength = 9;
            break;
            
          case SZLPLUGIN_GPC_ACCESS_MODE_INDEX:
            cmdReq->Payload[0] = 0x08;
            cmdReq->Payload[1] = Params->Item.AccessParameters.Index;
            cmdReq->PayloadLength = 2;
            break;
            
          default:
            szl_mem_free(cmdReq);
            return SZL_RESULT_INVALID_ADDRESS_MODE;
        }
      
      res = SZL_ClusterCmdReq(Service, Callback, cmdReq, NULL);
      szl_mem_free(cmdReq);
    }
      
  return res;
}


/******************************************************************************
 * Green Power Sink Table Response - Parsing Function
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_GPC_ParseSinkTableRsp(zabService* Service, SZL_STATUS_t Status, 
                                             SZL_ClusterCmdRespParams_t *Params, 
                                             SzlPlugin_GPC_SinkTableRspParams_t* SinkTableRspParams)
{
  szl_uint8* dp;
  unsigned8 dataIndex;
  
  if ((Params == NULL) || (SinkTableRspParams == NULL))
    {
      return SZL_RESULT_INVALID_DATA;
    }
  szl_memset(SinkTableRspParams, 0, sizeof(SzlPlugin_GPC_SinkTableRspParams_t)); 
  SinkTableRspParams->Status = SZL_RESULT_FAILED;
  
  if (Status == SZL_STATUS_SUCCESS)
    {
      if (Params->ClusterID == M_GP_CLUSTER_ID)
        {
          if (Params->Command == GPC_RSP_COMMAND_ID_SINK_TABLE_RSP)
            {    
              if (Params->PayloadLength >= M_SINK_TABLE_RSP_MIN_PAYLOAD)
                {
                  dp = Params->Payload; 
                  SinkTableRspParams->Status = *dp++;
                  SinkTableRspParams->NumberOfSinkTableEntries = *dp++;
                  SinkTableRspParams->StartIndex = *dp++;
                  SinkTableRspParams->EntryCount = *dp++;

                  if (SinkTableRspParams->EntryCount == 0)
                    {
                      /* There are no entries, so just return what we got! */
                      return SZL_RESULT_SUCCESS;
                    }
                  else if (Params->PayloadLength >= (M_SINK_TABLE_RSP_MIN_PAYLOAD + 2) )
                    {
                      // Limit to one entry for now
                      SinkTableRspParams->EntryCount = 1;
                    
                      SinkTableRspParams->SinkTableItem.Options.ByteOptions = COPY_IN_16_BITS(dp); dp+=2;

                      dataIndex = dp - Params->Payload;
                      dataIndex += getSinkTableItemLengthFromOptions(SinkTableRspParams->SinkTableItem.Options.BitOptions);

                      if (dataIndex <= Params->PayloadLength)
                        {
                          switch (SinkTableRspParams->SinkTableItem.Options.BitOptions.ApplicationId)
                            {
                              case 0x00:
                                SinkTableRspParams->SinkTableItem.GpdId = COPY_IN_32_BITS(dp); dp+=4;
                                break;

                              case 0x02:
                                SinkTableRspParams->SinkTableItem.GpdId = COPY_IN_64_BITS(dp); dp+=8;
                                break;

                              default:
                                // Error
                                return SZL_RESULT_INVALID_DATA;
                            }

                          SinkTableRspParams->SinkTableItem.DeviceId = *dp++;

                          if (SinkTableRspParams->SinkTableItem.Options.BitOptions.CommunicationMode == 0x02)
                            {
                              SinkTableRspParams->SinkTableItem.GroupList[0] = COPY_IN_16_BITS(dp); dp+=2;
                              SinkTableRspParams->SinkTableItem.GroupList[1] = COPY_IN_16_BITS(dp); dp+=2;
                            }

                          if (SinkTableRspParams->SinkTableItem.Options.BitOptions.AssignedAlias)
                            {
                              SinkTableRspParams->SinkTableItem.AssignedAlias = COPY_IN_16_BITS(dp); dp+=2;
                            }

                          SinkTableRspParams->SinkTableItem.GroupcastRadius = *dp++;

                          if (SinkTableRspParams->SinkTableItem.Options.BitOptions.SecurityUsed)
                            {
                              SinkTableRspParams->SinkTableItem.SecurityOptions = *dp++;
                            }

                          if ( (SinkTableRspParams->SinkTableItem.Options.BitOptions.SecurityUsed) ||
                               (SinkTableRspParams->SinkTableItem.Options.BitOptions.SeqNumCap) )
                            {
                              SinkTableRspParams->SinkTableItem.SecurityFrameCounter = COPY_IN_32_BITS(dp); dp+=4;
                            }

                          if (SinkTableRspParams->SinkTableItem.Options.BitOptions.SecurityUsed)
                            {
                              szl_memcpy(SinkTableRspParams->SinkTableItem.SecurityKey, dp, SZLPLUGIN_GPC_M_SECURITY_KEY_LENGTH);
                            }
                          
                          return SZL_RESULT_SUCCESS;
                        }
                    }
                }
            }
        }
      return SZL_RESULT_INVALID_DATA;
    }
  return SZL_RESULT_FAILED;
}


/******************************************************************************
 * Green Power Remove GPD from Sink Table Request
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_GPC_SinkTableRemoveGpdReq(zabService* Service,
                                                 SZL_CB_ClusterCmdResp_t Callback, 
                                                 SzlPlugin_GPC_SinkTableReqParams_t* Params)
{
  SZL_RESULT_t res = SZL_RESULT_INTERNAL_LIB_ERROR;
  SZL_ClusterCmdReqParams_t* cmdReq;            
  SZLPLUGIN_GPC_SINK_TABLE_OPTIONS_t options;
  
  if ( (Service == NULL) ||
       (Callback == NULL) ||
       (Params == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
         
  cmdReq = (SZL_ClusterCmdReqParams_t*)szl_mem_alloc(SZL_ClusterCmdReqParams_t_SIZE(M_SINK_TABLE_REMOVE_GPD_REQ_MAX_PAYLOAD_LENGTH),
                                                     MALLOC_ID_PLG_GPC_CMD_REQ);
  if (cmdReq != NULL)
    {
      szl_memset(cmdReq, 0, SZL_ClusterCmdReqParams_t_SIZE(M_SINK_TABLE_REMOVE_GPD_REQ_MAX_PAYLOAD_LENGTH));  
      
      
      cmdReq->SourceEndpoint = Params->SourceEndpoint;      
      cmdReq->DestAddrMode = Params->DestAddress;
      cmdReq->ManufacturerSpecific = szl_false;
      cmdReq->ClusterID = M_GP_CLUSTER_ID;
      cmdReq->Command = GPC_REQ_COMMAND_ID_PAIRING_CONFIGURATION_REQ;
      cmdReq->Direction = ZCL_FRAME_DIR_CLIENT_SERVER;
      
      // Add Action
      cmdReq->Payload[0] = 0x08 /* Send pairing - gives us a command on the sniffer */ | 0x04 /* Remove Gpd Action */;
      
      // Setup options and GpdId based our access mode
      options.ByteOptions = 0;      
      switch (Params->Item.AccessMode)
        {
          case SZLPLUGIN_GPC_ACCESS_MODE_SOURCE_ID:
            options.BitOptions.ApplicationId = 0; // 4B
            COPY_OUT_16_BITS(&cmdReq->Payload[1], options.ByteOptions);
            COPY_OUT_32_BITS(&cmdReq->Payload[3], Params->Item.AccessParameters.GpSourceId);
            cmdReq->PayloadLength = 8;
            break;
            
          case SZLPLUGIN_GPC_ACCESS_MODE_IEEE:
            options.BitOptions.ApplicationId = 2; // 8B
            COPY_OUT_16_BITS(&cmdReq->Payload[1], options.ByteOptions);
            COPY_OUT_64_BITS(&cmdReq->Payload[3], Params->Item.AccessParameters.Ieee);
            cmdReq->PayloadLength = 12;
            break;
            
          default:
            szl_mem_free(cmdReq);
            return SZL_RESULT_INVALID_ADDRESS_MODE;
        }
      
      cmdReq->Payload[cmdReq->PayloadLength++] = 0xFF; // DeviceId
      cmdReq->Payload[cmdReq->PayloadLength++] = 0xFF; // GroupcastRadius
      cmdReq->Payload[cmdReq->PayloadLength++] = 0x00; // Number Of Paired Endpoints - No paired endpoints
      
      res = SZL_ClusterCmdReq(Service, Callback, cmdReq, NULL);
      szl_mem_free(cmdReq);
    }
      
  return res;
}


/******************************************************************************
 * Green Power Translation Table Request
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_GPC_TranslationTableReq(zabService* Service,
                                               SZL_CB_ClusterCmdResp_t Callback, 
                                               SzlPlugin_GPC_TranslationTableReqParams_t* Params)
{
  SZL_RESULT_t res = SZL_RESULT_INTERNAL_LIB_ERROR;
  SZL_ClusterCmdReqParams_t* cmdReq;
            
  if ( (Service == NULL) ||
       (Callback == NULL) ||
       (Params == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
          
  cmdReq = (SZL_ClusterCmdReqParams_t*)szl_mem_alloc(SZL_ClusterCmdReqParams_t_SIZE(M_TRANSLATION_TABLE_REQ_MAX_PAYLOAD_LENGTH), 
                                                     MALLOC_ID_PLG_GPC_CMD_REQ);
  if (cmdReq != NULL)
    {
      szl_memset(cmdReq, 0, SZL_ClusterCmdReqParams_t_SIZE(M_TRANSLATION_TABLE_REQ_MAX_PAYLOAD_LENGTH));  
            
      cmdReq->SourceEndpoint = Params->SourceEndpoint;      
      cmdReq->DestAddrMode = Params->DestAddress;
      cmdReq->ManufacturerSpecific = szl_false;
      cmdReq->ClusterID = M_GP_CLUSTER_ID;
      cmdReq->Command = GPC_REQ_COMMAND_ID_TRANSLATION_TABLE_REQ;
      cmdReq->Direction = ZCL_FRAME_DIR_CLIENT_SERVER;
      cmdReq->PayloadLength = 1;
      cmdReq->Payload[0] = Params->StartIndex;
      
      res = SZL_ClusterCmdReq(Service, Callback, cmdReq, NULL);
      szl_mem_free(cmdReq);
    }
      
  return res;
}


/******************************************************************************
 * Green Power Translation Table Response - Parsing Function
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_GPC_ParseTranslationTableRsp(zabService* Service, SZL_STATUS_t Status, 
                                                    SZL_ClusterCmdRespParams_t *Params, 
                                                    SzlPlugin_GPC_TranslationTableRspParams_t* TranslationTableRspParams)
{
  szl_uint8* dp;
  unsigned8 itemIndex;
  
  if ((Params == NULL) || (TranslationTableRspParams == NULL))
    {
      return SZL_RESULT_INVALID_DATA;
    }
  szl_memset(TranslationTableRspParams, 0, sizeof(SzlPlugin_GPC_TranslationTableRspParams_t)); 
  TranslationTableRspParams->Status = SZL_RESULT_FAILED;
  
  if (Status == SZL_STATUS_SUCCESS)
    {
      if (Params->ClusterID == M_GP_CLUSTER_ID)
        {
          if (Params->Command == GPC_RSP_COMMAND_ID_TRANSLATION_TABLE_RSP)
            {    
              if (Params->PayloadLength >= M_TRANSLATION_TABLE_RSP_MIN_PAYLOAD)
                {
                  dp = Params->Payload; 
                  TranslationTableRspParams->Status = *dp++;
                  TranslationTableRspParams->Options.ByteOptions = *dp++;
                  
                  /* ONly SrcID supported for now */
                  if (TranslationTableRspParams->Options.BitOptions.ApplicationId != 0)
                    {
                    return SZL_RESULT_OPERATION_NOT_POSSIBLE;
                    }
                  
                  TranslationTableRspParams->TotalNumberOfEntries = *dp++;
                  TranslationTableRspParams->StartIndex = *dp++;
                  TranslationTableRspParams->EntryCount = *dp++;

                  if (TranslationTableRspParams->TotalNumberOfEntries == 0)
                    {
                      /* There are no entries, so just return what we got! */
                      return SZL_RESULT_SUCCESS;
                    }
                  if (TranslationTableRspParams->EntryCount > SZLPLUGIN_GPC_M_TT_ITEMS_MAX)
                    {
                      TranslationTableRspParams->EntryCount = SZLPLUGIN_GPC_M_TT_ITEMS_MAX;
                    }
                  
                  itemIndex = 0;
                  while ( (Params->PayloadLength >= ((dp-Params->Payload)+M_TRANSLATION_TABLE_RSP_MIN_ITEM_SIZE) ) &&
                          (itemIndex < TranslationTableRspParams->EntryCount) )
                    {
                      TranslationTableRspParams->TranslationTableItems[itemIndex].GpdId = COPY_IN_32_BITS(dp); dp+=4;
                      TranslationTableRspParams->TranslationTableItems[itemIndex].GpdCommandId = *dp++;
                      TranslationTableRspParams->TranslationTableItems[itemIndex].Endpoint = *dp++;
                      TranslationTableRspParams->TranslationTableItems[itemIndex].Profile = COPY_IN_16_BITS(dp); dp+=2;
                      TranslationTableRspParams->TranslationTableItems[itemIndex].Cluster = COPY_IN_16_BITS(dp); dp+=2;
                      TranslationTableRspParams->TranslationTableItems[itemIndex].ZigBeeCommandId = *dp++;
                      TranslationTableRspParams->TranslationTableItems[itemIndex].ZigBeePayloadLength = *dp++;
                      
                      if ( (TranslationTableRspParams->TranslationTableItems[itemIndex].ZigBeePayloadLength != SZLPLUGIN_GPC_M_ZIGBEE_PAYLOAD_LENGTH_FROM_GPD)&&
                           (TranslationTableRspParams->TranslationTableItems[itemIndex].ZigBeePayloadLength != SZLPLUGIN_GPC_M_ZIGBEE_PAYLOAD_LENGTH_PARSED_FROM_GPD) )
                        {
                          if (TranslationTableRspParams->TranslationTableItems[itemIndex].ZigBeePayloadLength < SZLPLUGIN_GPC_M_ZIGBEE_PAYLOAD_LENGTH_MAX)
                            {
                              szl_memcpy(TranslationTableRspParams->TranslationTableItems[itemIndex].ZigBeepayload,
                                          dp,
                                          TranslationTableRspParams->TranslationTableItems[itemIndex].ZigBeePayloadLength);
                            }
                          else
                            {
                              return SZL_RESULT_INVALID_DATA;
                            }
                          dp += TranslationTableRspParams->TranslationTableItems[itemIndex].ZigBeePayloadLength;
                        }
                      
                      itemIndex++;
                    }
                  /* If we got the expected number of entries (and didn't run out of data) then success */
                  if (TranslationTableRspParams->EntryCount == itemIndex)
                    {
                      return SZL_RESULT_SUCCESS;
                    }
                }
            }
        }
      return SZL_RESULT_INVALID_DATA;
    }
  return SZL_RESULT_FAILED;
}



/******************************************************************************
 * Green Power Translation Table Update Request
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_GPC_TranslationTableUpdateReq(zabService* Service,
                                                     SZL_CB_ClusterCmdResp_t Callback, 
                                                     SzlPlugin_GPC_TranslationTableUpdateReqParams_t* Params)
{
  SZL_RESULT_t res = SZL_RESULT_INTERNAL_LIB_ERROR;
  SZL_ClusterCmdReqParams_t* cmdReq;
  szl_uint8 payloadLength;
  szl_uint8* dp;
  szl_uint8 itemIndex;
            
  if ( (Service == NULL) ||
       (Callback == NULL) ||
       (Params == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
     
  payloadLength = 6;
  for (itemIndex = 0; itemIndex < Params->Options.BitOptions.NumberOfTranslations; itemIndex++)
    {
      payloadLength += 9;
      
      if ( (Params->Item[itemIndex].ZigBeePayloadLength != SZLPLUGIN_GPC_M_ZIGBEE_PAYLOAD_LENGTH_FROM_GPD)&&
           (Params->Item[itemIndex].ZigBeePayloadLength != SZLPLUGIN_GPC_M_ZIGBEE_PAYLOAD_LENGTH_PARSED_FROM_GPD) )
        {
          payloadLength += Params->Item[itemIndex].ZigBeePayloadLength;
        }
    }
  
  cmdReq = (SZL_ClusterCmdReqParams_t*)szl_mem_alloc(SZL_ClusterCmdReqParams_t_SIZE( payloadLength ), 
                                                     MALLOC_ID_PLG_GPC_CMD_REQ);
  if (cmdReq != NULL)
    {
      szl_memset(cmdReq, 0, SZL_ClusterCmdReqParams_t_SIZE(M_TRANSLATION_TABLE_REQ_MAX_PAYLOAD_LENGTH));  
            
      cmdReq->SourceEndpoint = Params->SourceEndpoint;      
      cmdReq->DestAddrMode = Params->DestAddress;
      cmdReq->ManufacturerSpecific = szl_false;
      cmdReq->ClusterID = M_GP_CLUSTER_ID;
      cmdReq->Command = GPC_REQ_COMMAND_ID_TRANSLATION_TABLE_UPDATE;
      cmdReq->Direction = ZCL_FRAME_DIR_CLIENT_SERVER;
      cmdReq->PayloadLength = payloadLength;
      
      dp = cmdReq->Payload;

      COPY_OUT_16_BITS(dp, Params->Options.ByteOptions); dp += 2;
      COPY_OUT_32_BITS(dp, Params->GpdId); dp += 4;

      for (itemIndex = 0; itemIndex < Params->Options.BitOptions.NumberOfTranslations; itemIndex++)
      {
        *dp++ = Params->Item[itemIndex].Index;
        *dp++ = Params->Item[itemIndex].GpdCommandId;
        *dp++ = Params->Item[itemIndex].Endpoint;
        COPY_OUT_16_BITS(dp, Params->Item[itemIndex].Profile); dp += 2;
        COPY_OUT_16_BITS(dp, Params->Item[itemIndex].Cluster); dp += 2;
        *dp++ = Params->Item[itemIndex].ZigBeeCommandId;
        *dp++ = Params->Item[itemIndex].ZigBeePayloadLength;
        
        if ( (Params->Item[itemIndex].ZigBeePayloadLength != SZLPLUGIN_GPC_M_ZIGBEE_PAYLOAD_LENGTH_FROM_GPD)&&
             (Params->Item[itemIndex].ZigBeePayloadLength != SZLPLUGIN_GPC_M_ZIGBEE_PAYLOAD_LENGTH_PARSED_FROM_GPD) )
          {
            szl_memcpy(dp, Params->Item[itemIndex].ZigBeepayload, Params->Item[itemIndex].ZigBeePayloadLength);
            dp += Params->Item[itemIndex].ZigBeePayloadLength;
          }
                      
      }
      
      res = SZL_ClusterCmdReq(Service, Callback, cmdReq, NULL);
      szl_mem_free(cmdReq);
    }
      
  return res;
}


/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/