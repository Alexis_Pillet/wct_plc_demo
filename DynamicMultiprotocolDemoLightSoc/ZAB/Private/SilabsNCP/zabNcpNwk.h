/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the SiLabs NCP Network State Machine - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.005  30-Jun-16   MvdB   Support energy scan for quietest channel during form
 * 000.000.013  21-Jul-16   MvdB   Support Network Join
 *                                 Upgrade ZDO responses to use correct network address, not just 0
 * 000.000.014  22-Jul-16   MvdB   Support ZAB_ACTION_SET_TX_POWER
 * 000.000.016  25-Jul-16   MvdB   Support ZAB_ACTION_CHANGE_CHANNEL and other a-synchronous network parameter changes
 * 000.000.017  26-Jul-16   MvdB   Support ZAB_ACTION_CHANGE_NETWORK_KEY
 * 000.000.019  26-Jul-16   MvdB   Tidy up permit join notifications
 * 000.000.020  27-Jul-16   MvdB   Add network processor ping: Add zabNcpNwk_GetNwkStateMachineInProgress()
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 * 002.002.039  17-Jan-17   MvdB   ARTF175875: In progress (binding table copy synchronised but not yet used)
 *                                  - Support policy setting in Nwk init and set binding policy to EZSP_ALLOW_BINDING_MODIFICATION
 *                                  - Clear binding table on network join/form. Read binding table on network start.
 * 002.002.040  17-Jan-17   MvdB   ARTF175875: Add zabNcpNwk_GetBindingIndex() to support send via bind
 *****************************************************************************/
#ifndef __ZAB_NCP_NWK_H__
#define __ZAB_NCP_NWK_H__

#include "ember-types.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Create - Initialises networking state / state machine
 ******************************************************************************/
void zabNcpNwk_Create(erStatus* Status, zabService* Service);

/******************************************************************************
 * Handle actions for the networking state machines
 ******************************************************************************/
void zabNcpNwk_Action(erStatus* Status, zabService* Service, unsigned8 Action);

/******************************************************************************
 * Get Network State
 ******************************************************************************/
zabNwkState zabNcpNwk_GetNwkState(zabService* Service);

/******************************************************************************
 * Get if the Network State Machine is currently busy doing something
 ******************************************************************************/
zab_bool zabNcpNwk_GetNwkStateMachineInProgress(zabService* Service);

/******************************************************************************
 * Get Network Address
 ******************************************************************************/
unsigned16 zabNcpNwk_GetNwkAddress(zabService* Service);

/******************************************************************************
 * Get Network Update ID
 ******************************************************************************/
unsigned8 zabNcpNwk_GetNwkUpdateId(zabService* Service);

/******************************************************************************
 * Check for timeouts within the networking state machine
 ******************************************************************************/
unsigned32 zabNcpNwk_UpdateTimeout(erStatus* Status, zabService* Service, unsigned32 Time);

/******************************************************************************
 * 1 second tick handler
 ******************************************************************************/
void zabNcpNwk_OneSecondTickHandler(erStatus* Status, zabService* Service);

/******************************************************************************
 * Get a binding index out of the binding table
 *
 * Returns:
 *   Binding index:
 *     ZAB_NCP_NWK_BINDING_INDEX_NONE if not found
 *     Otherwise index of matching bind in binding table
 *   *BindItem:
 *     Pointer to binding item (if binding index != ZAB_NCP_NWK_BINDING_INDEX_NONE)
 *   *MoreBindsInTable:
 *     zab_true if there are more matching bind items following in the table
 ******************************************************************************/
unsigned8 zabNcpNwk_GetBindingIndex(erStatus* Status, zabService* Service,
                                    unsigned8 SourceEndpoint, unsigned16 Cluster, unsigned8 TransactionId,
                                    const EmberBindingTableEntry** BindItem,
                                    zab_bool* MoreBindsInTable);

/******************************************************************************
 * Various Response Handlers - Self explanatory!
 ******************************************************************************/
void zabNcpNwk_ResetHandler(erStatus* Status, zabService* Service);
void zabNcpNwk_NetworkInitResponseHandler(erStatus* Status, zabService* Service, unsigned8 InitStatus);
void zabNcpNwk_StackStatusHandler(erStatus* Status, zabService* Service, unsigned8 StackStatus);
void zabNcpNwk_GetNetworkAddressResponseHandler(erStatus* Status, zabService* Service, unsigned16 NetworkAddress);
void zabNcpNwk_GetNetworkParametersResponseHandler(erStatus* Status, zabService* Service, unsigned8 GetStatus, EmberNodeType NodeType, EmberNetworkParameters* NetworkParams);
void zabNcpNwk_LeaveResponseHandler(erStatus* Status, zabService* Service, unsigned8 LeaveStatus);
void zabNcpNwk_ClearBindingTableResponseHandler(erStatus* Status, zabService* Service, unsigned8 ClearStatus);
void zabNcpNwk_SecurityInitResponseHandler(erStatus* Status, zabService* Service, unsigned8 SecurityStatus);
void zabNcpNwk_StartScanResponseHandler(erStatus* Status, zabService* Service, unsigned8 ScanStartStatus);
void zabNcpNwk_EnergyScanResultHandler(erStatus* Status, zabService* Service, unsigned8 Channel, signed8 MaxRssiValue);
void zabNcpNwk_NetworkFoundResultHandler(erStatus* Status, zabService* Service, EmberZigbeeNetwork Network, unsigned8 LastHopLqi, signed8 LastHopRssi);
void zabNcpNwk_ScanCompleteHandler(erStatus* Status, zabService* Service, unsigned8 ScanStatus);
void zabNcpNwk_NetworkFormResponseHandler(erStatus* Status, zabService* Service, unsigned8 FormStatus);
void zabNcpNwk_NetworkJoinResponseHandler(erStatus* Status, zabService* Service, unsigned8 JoinStatus);
void zabNcpNwk_SetTxPowerResponseHandler(erStatus* Status, zabService* Service, unsigned8 PowerStatus);
void zabNcpNwk_GetPowerResponseHandler(erStatus* Status, zabService* Service, signed8 TxPower);
void zabNcpNwk_PermitJoinRequestHandler(erStatus* Status, zabService* Service, unsigned16 SourceNetworkAddress, unsigned8 Duration);
void zabNcpNwk_LeaveRequestHandler(erStatus* Status, zabService* Service, unsigned64 Ieee, zab_bool Rejoin);
void zabNcpNwk_PermitJoinResponseHandler(erStatus* Status, zabService* Service, unsigned8 PermitStatus);
void zabNcpNwk_SetConfigurationResponseHandler(erStatus* Status, zabService* Service, unsigned8 ConfigStatus);
void zabNcpNwk_SetPolicyResponseHandler(erStatus* Status, zabService* Service, unsigned8 PolicyStatus);
void zabNcpNwk_GetBindingResponseHandler(erStatus* Status, zabService* Service, unsigned8 BindingStatus, EmberBindingTableEntry* BindingEntry);
void zabNcpNwk_RemoteSetBindingHandler(erStatus* Status, zabService* Service, unsigned8 BindingStatus, unsigned8 BindingIndex, EmberBindingTableEntry* BindingEntry);
void zabNcpNwk_RemoteDeleteBindingHandler(erStatus* Status, zabService* Service, unsigned8 BindingStatus, unsigned8 BindingIndex);
void zabNcpNwk_SetManufacturerCodeResponseHandler(erStatus* Status, zabService* Service);
void zabNcpNwk_BroadcastNextNetworkKeyResponseHandler(erStatus* Status, zabService* Service, unsigned8 KeyStatus);
void zabNcpNwk_BroadcastNextNetworkKeySwitchResponseHandler(erStatus* Status, zabService* Service, unsigned8 KeyStatus);

#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/