#ifndef _ZCL_H_
#define _ZCL_H_
#include "szl_external.h"

// NOTE: All ZCL messages should be in Little Endian format ALWAYS!


// imported from snp.h by mvdb
#define ZB_SCHNEIDER_MANUFACTURE_ID 0x105E
typedef enum
{
    ZB_HA_PROFILE_ID                                = 0x0104,   // Zigbee Home Automation Profile Identification
} ZB_PROFILES_t;

/** returns the pointer to the common part of the zcl message */
#define ZCL_GET_COMMON_PART_PTR(_zcl_msg) ((_zcl_msg)->frame_type.frame_control.manufacture_specific ? &(_zcl_msg)->frame_type.manufacture_frame.msg : &(_zcl_msg)->frame_type.std_frame.msg)






typedef enum
{
    // Generic Device IDs
    ZB_HA_DEVICEID_ON_OFF_SWITCH                    = 0x0000,
    ZB_HA_DEVICEID_LEVEL_CONTROL_SWITCH             = 0x0001,
    ZB_HA_DEVICEID_ON_OFF_OUTPUT                    = 0x0002,
    ZB_HA_DEVICEID_LEVEL_CONTROLLABLE_OUTPUT        = 0x0003,
    ZB_HA_DEVICEID_SCENE_SELECTOR                   = 0x0004,
    ZB_HA_DEVICEID_CONFIGURATION_TOOL               = 0x0005,
    ZB_HA_DEVICEID_REMOTE_CONTROL                   = 0x0006,
    ZB_HA_DEVICEID_COMBINED_INTERFACE               = 0x0007,
    ZB_HA_DEVICEID_RANGE_EXTENDER                   = 0x0008,
    ZB_HA_DEVICEID_MAINS_POWER_OUTLET               = 0x0009,
    ZB_HA_DEVICEID_DOOR_LOCK                        = 0x000A,
    ZB_HA_DEVICEID_DOOR_LOCK_CONTROLLER             = 0x000B,
    ZB_HA_DEVICEID_SIMPLE_SENSOR                    = 0x000C,
    ZB_HA_DEVICEID_CONSUMPTION_AWARENESS_DEVICE     = 0x000D,
    ZB_HA_DEVICEID_HOME_GATEWAY                     = 0x0050,
    ZB_HA_DEVICEID_SMART_PLUG                       = 0x0051,
    ZB_HA_DEVICEID_WHITE_GOODS                      = 0x0052,
    ZB_HA_DEVICEID_METER_INTERFACE                  = 0x0053,
    ZB_HA_DEVICEID_TEST_DEVICE                      = 0x00FF,

    // Lighting Device IDs
    ZB_HA_DEVICEID_ON_OFF_LIGHT                     = 0x0100,
    ZB_HA_DEVICEID_DIMMABLE_LIGHT                   = 0x0101,
    ZB_HA_DEVICEID_COLORED_DIMMABLE_LIGHT           = 0x0102,
    ZB_HA_DEVICEID_ON_OFF_LIGHT_SWITCH              = 0x0103,
    ZB_HA_DEVICEID_DIMMER_SWITCH                    = 0x0104,
    ZB_HA_DEVICEID_COLOR_DIMMER_SWITCH              = 0x0105,
    ZB_HA_DEVICEID_LIGHT_SENSOR                     = 0x0106,
    ZB_HA_DEVICEID_OCCUPANCY_SENSOR                 = 0x0107,

    // Closures Device IDs
    ZB_HA_DEVICEID_SHADE                            = 0x0200,
    ZB_HA_DEVICEID_SHADE_CONTROLLER                 = 0x0201,
    ZB_HA_DEVICEID_WINDOW_COVERING                  = 0x0202,
    ZB_HA_DEVICEID_WINDOW_COVERING_CONTROLLER       = 0x0203,

    // HVAC Device IDs
    ZB_HA_DEVICEID_HEATING_COOLING_UNIT             = 0x0300,
    ZB_HA_DEVICEID_THERMOSTAT                       = 0x0301,
    ZB_HA_DEVICEID_TEMPERATURE_SENSOR               = 0x0302,
    ZB_HA_DEVICEID_PUMP                             = 0x0303,
    ZB_HA_DEVICEID_PUMP_CONTROLLER                  = 0x0304,
    ZB_HA_DEVICEID_PRESSURE_SENSOR                  = 0x0305,
    ZB_HA_DEVICEID_FLOW_SENSOR                      = 0x0306,
    ZB_HA_DEVICEID_MINI_SPLIT_AC                    = 0x0307,

    // Intruder Alarm Systems (IAS) Device IDs
    ZB_HA_DEVICEID_IAS_CONTROL_INDICATING_EQUIPMENT = 0x0400,
    ZB_HA_DEVICEID_IAS_ANCILLARY_CONTROL_EQUIPMENT  = 0x0401,
    ZB_HA_DEVICEID_IAS_ZONE                         = 0x0402,
    ZB_HA_DEVICEID_IAS_WARNING_DEVICE               = 0x0403,
} ZB_DEVICES_t;


// ZCL STUFF
typedef enum
{
    ZCL_CMD_ID_READ_ATTRIBUTES                  = 0x00,
    ZCL_CMD_ID_READ_ATTRIBUTES_RSP              = 0x01,
    ZCL_CMD_ID_WRITE_ATTRIBUTES                 = 0x02,
    ZCL_CMD_ID_WRITE_ATTRIBUTES_UNDIVIDED       = 0x03,
    ZCL_CMD_ID_WRITE_ATTRIBUTES_RSP             = 0x04,
    ZCL_CMD_ID_WRITE_ATTRIBUTES_NO_RESPONSE     = 0x05,
    ZCL_CMD_ID_CONFIGURE_REPORTING              = 0x06,
    ZCL_CMD_ID_CONFIGURE_REPORTING_RSP          = 0x07,
    ZCL_CMD_ID_READ_REPORTING_CONFIGURATION     = 0x08,
    ZCL_CMD_ID_READ_REPORTING_CONFIGURATION_RSP = 0x09,
    ZCL_CMD_ID_REPORT_ATTRIBUTES                = 0x0a,
    ZCL_CMD_ID_DEFAULT_RSP                      = 0x0b,
    ZCL_CMD_ID_DISCOVER_ATTRIBUTES              = 0x0c,
    ZCL_CMD_ID_DISCOVER_ATTRIBUTES_RSP          = 0x0d,
    ZCL_CMD_ID_READ_ATTRIBUTES_STRUCTURED       = 0x0e,
    ZCL_CMD_ID_WRITE_ATTRIBUTES_STRUCTURED      = 0x0f,
    ZCL_CMD_ID_WRITE_ATTRIBUTES_STRUCTURED_RSP  = 0x10,

    // Manufacture specific commands
    _first_manufacture_specific_command_        = 0x50,
    ZCL_CMD_ID_SET_SUMMATION                    = 0x50,
    ZCL_CMD_ID_RESET_SUMMATION                  = 0x51,
    ZCL_CMD_ID_SET_MULTIPLIER                   = 0x53,
    ZCL_CMD_ID_SET_DIVISOR                      = 0x54,
    ZCL_CMD_ID_SET_AUTOADJUST                   = 0xE0,
    ZCL_CMD_ID_SET_MULTIPLIER_AND_DIVISOR       = 0xE1,
} ZCL_CMD_ID_t;

#ifdef PRAGMA_PACK
#pragma pack(1)
#endif

typedef enum
{
    ZCL_FRAME_TYPE_PROFILE_COMMAND = 0x0,
    ZCL_FRAME_TYPE_CLUSTER_COMMAND = 0x1,
    ZCL_FRAME_TYPE_RESERVED        = 0x2,
} ZCL_FRAME_TYPE_t;

typedef enum
{
    ZCL_FRAME_DIR_CLIENT_SERVER     = 0x00,
    ZCL_FRAME_DIR_SERVER_CLIENT     = 0x01,
} ZCL_FRAME_DIR_t;


// Dumped here for now - its zab style so the bit position matters!
typedef enum
{
  ZCL_CFG_REPORT_DIRECTION_SEND_REPORT = 0x00,
  ZCL_CFG_REPORT_DIRECTION_RECEIVE_REPORT = 0x01        
}ZCL_CFG_REPORT_DIRECTION;

typedef enum
{
    ZCL_FRAME_DEFAULT_RESPONSE_ALWAYS   = 0x00,
    ZCL_FRAME_DEFAULT_RESPONSE_ON_ERROR = 0x01,
} ZCL_FRAME_DEF_RESP_t;

typedef struct
{
    szl_uint8  frame_type:2;            // ------xx Frame type specification
                                        //       00 - Command acts across the entire profile
                                        //       01 - Command is specific to a cluster
                                        //       11 - Reserved
    szl_uint8  manufacture_specific:1;  // -----x-- Manufacture specific frame
                                        //      0 - The manufacturer code field shall not be included in the ZCL
                                        //      1 - The manufacturer code field shall be present in the ZCL frame.
    szl_uint8  direction:1;             // ----x--- Direction
                                        //     0 - The command is sent from the client to the server
                                        //     1 - The command is sent from the server to the client
    szl_uint8  disable_default_rsp:1;   // ---x---- Disable default response
                                        //    0 - The Default response command will be returned
                                        //    1 - The Default response command will only be returned if there is an error
    szl_uint8  reserved:3;              // xxx----- Reserved
                                        // 000 - must be set to zero
} ZCL_FRAME_CONTROL_t;


typedef struct
{
    szl_uint8 tr_id;                    // The transaction id field can be used by a controlling device, which may have issued multiple commands, so that it can match the incoming responses
    szl_uint8 cmd;                      // If frame_type is 00, the cmd corresponds to non-reserved values. If frame_type is 01 the cmd corresponds to a cluster specific command.
    szl_uint8 payload[SZL_VLA_INIT];    // first szl_byte of the message
} ZCL_COMMON_PART_t;

typedef struct
{
    ZCL_FRAME_CONTROL_t frame_control;  // contains information defining the command type and other control flags
    szl_uint16 manufacture_code;        // The ZigBee assigned manufacturer code for proprietary extensions to a profile. Only included if the manufacturer_specific field is set to 1
    ZCL_COMMON_PART_t msg;
} ZCL_MANUFACTURE_MSG_t;

typedef struct
{
    ZCL_FRAME_CONTROL_t frame_control;// contains information defining the command type and other control flags
    ZCL_COMMON_PART_t msg;
} ZCL_STD_MSG_t;

typedef struct
{
    szl_uint8 cmd;                      // ZGP Command ID
	szl_uint8 payload[SZL_VLA_INIT];    // first byte of the message
} ZCL_ZGP_MSG_t;

typedef struct
{
    szl_uint8  multi_record:1;          // -------x Multi-Record
                                        //      0 - Frame contains a single Cluster Record
                                        //      1 - Frame contains multiple Cluster Records
    szl_uint8  manufacture_specific:1;  // ------x- Manufacture specific frame
                                        //      0 - The manufacturer code field shall not be included in the ZCL
                                        //      1 - The manufacturer code field shall be present in the ZCL frame.
    szl_uint8  reserved:6;              // xxxxxx-- Reserved
                                        // 000 - must be set to zero
} ZCL_ZGP_OPTIONS_t;

typedef struct
{
    union
    {
        ZCL_FRAME_CONTROL_t frame_control; // to determine if std or manufacture frame
        ZCL_STD_MSG_t std_frame;
        ZCL_MANUFACTURE_MSG_t manufacture_frame;
        ZCL_ZGP_MSG_t zgp_frame;
    }frame_type;
} ZCL_PAYLOAD_t;
#define ZCL_PAYLOAD_SIZE(manufacture, data_len) ((data_len) + ((manufacture) ? (sizeof(ZCL_MANUFACTURE_MSG_t) - SZL_VLA_INIT) : (sizeof(ZCL_STD_MSG_t) - SZL_VLA_INIT)))

// DEFAULT_RSP msg
typedef struct
{
    szl_uint8 cmd;      // The command identifier field is 8-bits in length and specifies the identifier of the received command to which this command is a response
    szl_uint8 status;   // status returned
} ZCL_DEFAULT_RESPONSE_t;


// READ_ATTRIBUTES msg
typedef struct
{
    szl_uint16 attribute_id; // The attribute identifier field is 16-bits in length and shall contain the identifier of the attribute that is to be read.
} ZCL_READ_ATTRIBUTE_ID_t;

typedef struct
{
    ZCL_READ_ATTRIBUTE_ID_t attributes[1]; // n elements of attribute
} ZCL_READ_ATTRIBUTES_t;


// READ_ATTRIBUTES_RSP
typedef struct
{
    szl_uint16 attribute_id;        // The attribute identifier field is 16-bits in length and shall contain the identifier of the attribute that has been read.
    szl_uint8 status;               // specifies the status of the read operation on this attribute. This field shall be set to SUCCESS, if the operation was successful
    szl_uint8 data_type;            // contain the data type of the attribute. 
                                    // For PRO, this field shall only be included if the associated status field contains a value of SUCCESS.
                                    // For GP it is always included regardless of status
    szl_uint8 data[SZL_VLA_INIT];   // contain the current value of this attribute. This field shall only be included if the associated status field contains a value of SUCCESS.
} ZCL_READ_ATTRIBUTE_STATUS_t;
#define ZCL_READ_ATTRIBUTE_STATUS_SIZE(status, data_size) (sizeof(ZCL_READ_ATTRIBUTE_STATUS_t) - (sizeof(szl_uint8) * SZL_VLA_INIT) - 1 + (((status) == SZL_STATUS_SUCCESS) ?  (data_size) + 1 : 0))
#define ZCL_READ_ATTRIBUTE_STATUS_SIZE_GP(status, data_size) (sizeof(ZCL_READ_ATTRIBUTE_STATUS_t) - (sizeof(szl_uint8) * SZL_VLA_INIT) + (((status) == SZL_STATUS_SUCCESS) ?  (data_size) : 0))

// READ_ATTRIBUTES_RSP
typedef struct
{
    ZCL_READ_ATTRIBUTE_STATUS_t attributes[1]; // n elements of attribute
} ZCL_READ_ATTRIBUTES_RSP_t;

// WRITE_ATTRIBUTES msg
typedef struct
{
    szl_uint16 attribute_id;        // The attribute identifier field is 16-bits in length and shall contain the identifier of the attribute that has been read.
    szl_uint8 data_type;            // contain the data type of the attribute. This field shall only be included if the associated status field contains a value of SUCCESS
    szl_uint8 data[SZL_VLA_INIT];   // contain the current value of this attribute. This field shall only be included if the associated status field contains a value of SUCCESS.
} ZCL_WRITE_ATTRIBUTE_t;
#define ZCL_WRITE_ATTRIBUTE_SIZE(data_size) (sizeof(ZCL_WRITE_ATTRIBUTE_t) + (data_size) - (sizeof(szl_uint8) * SZL_VLA_INIT))

// WRITE_ATTRIBUTES_REQ
typedef struct
{
    ZCL_WRITE_ATTRIBUTE_t attributes; // n elements of attribute
} ZCL_WRITE_ATTRIBUTES_REQ_t;

// WRITE_ATTRIBUTES_STATUS msg
typedef struct
{
    szl_uint8 status;        // specifies the status of the write operation on this attribute. This field shall be set to SUCCESS, if the operation was successful
    szl_uint16 attribute_id; // The attribute identifier field is 16-bits in length and shall contain the identifier of the attribute that has been written.
} ZCL_WRITE_ATTRIBUTE_STATUS_t;
#define ZCL_WRITE_ATTRIBUTE_STATUS_SIZE (sizeof(ZCL_WRITE_ATTRIBUTE_STATUS_t))

// WRITE_ATTRIBUTES_RSP
typedef struct
{
    ZCL_WRITE_ATTRIBUTE_STATUS_t attributes; // n elements of attribute
} ZCL_WRITE_ATTRIBUTES_RSP_t;



// CONFIGURE_REPORTING msg
typedef enum
{
    ZCL_CONFIGURE_REPORTING_SENDING     = 0x00,
    ZCL_CONFIGURE_REPORTING_RECEIVING   = 0x01
} ZCL_CONFIGURE_REPORTING_DIRECTION_t;

typedef struct
{
    szl_uint8 data_type;                        // The Attribute data type field contains the data type of the attribute that is to be reported
    szl_uint16 min_reporting_interval;          // The minimum reporting interval field contains the minimum interval, in seconds, between issuing reports of the specified attribute
    szl_uint16 max_reporting_interval;          // The maximum reporting interval field contains the maximum interval, in seconds, between issuing reports of the specified attribute.
    szl_uint8 reportable_change[SZL_VLA_INIT];  // The reportable change field contains the minimum change to the attribute that will result in a report being issued.
} ZCL_CONFIGURE_REPORTING_SENDING_t;
#define ZCL_CONFIGURE_REPORTING_SENDING_SIZE(_reportable_change_size) (sizeof(ZCL_CONFIGURE_REPORTING_SENDING_t) + (_reportable_change_size) - (sizeof(szl_uint8) * SZL_VLA_INIT))

typedef struct
{
    szl_uint16 timeout_period; // The timeout period field contains the maximum expected time, in seconds, between received reports for the attribute specified
} ZCL_CONFIGURE_REPORTING_RECEIVING_t;
#define ZCL_CONFIGURE_REPORTING_RECEIVING_SIZE sizeof(ZCL_CONFIGURE_REPORTING_RECEIVING_t)

typedef struct
{
    szl_uint8 direction;     // The direction field ( can be 0x00 or 0x01)
    szl_uint16 attribute_id; // The attribute id for the reporting item
    union
    {
        ZCL_CONFIGURE_REPORTING_SENDING_t sending;
        ZCL_CONFIGURE_REPORTING_RECEIVING_t receiving;
    }parameters;
} ZCL_CONFIGURE_REPORTING_t;
#define ZCL_CONFIGURE_REPORTING_SIZE(data_size) ( 3 + (data_size))

typedef struct
{
    szl_uint8 status;        // The status field specifies the status of the configure reporting operation attempted on this attribute
    szl_uint8 direction;     // The direction field ( can be 0x00 or 0x01)
    szl_uint16 attribute_id; // The attribute id for the reporting item
} ZCL_CONFIGURE_REPORTING_RSP_t;
#define ZCL_CONFIGURE_REPORTING_RSP_SIZE ( sizeof(ZCL_CONFIGURE_REPORTING_RSP_t))

typedef struct
{
    szl_uint16 attribute_id;        // The attribute id for the reporting item
    szl_uint8 data_type;            // contain the data type of the attribute.
    szl_uint8 data[SZL_VLA_INIT];   // contain the value of this attribute.
} ZCL_REPORT_ATTRIBUTE_t;
#define ZCL_REPORT_ATTRIBUTE_SIZE(data_size) ( sizeof(ZCL_REPORT_ATTRIBUTE_t) + (data_size) - (sizeof(szl_uint8) * SZL_VLA_INIT))

typedef struct
{
    szl_uint8 direction;     // The direction field ( can be 0x00 or 0x01)
    szl_uint16 attribute_id; // The attribute id for the reporting item
} ZCL_READ_REPORTING_CFG_t;
#define ZCL_READ_REPORTING_CFG_SIZE sizeof(ZCL_READ_REPORTING_CFG_t)

typedef struct
{
    ZCL_CONFIGURE_REPORTING_RSP_t common;
    union
    {
        ZCL_CONFIGURE_REPORTING_SENDING_t sending;
        ZCL_CONFIGURE_REPORTING_RECEIVING_t receiving;
    }parameters;
} ZCL_READ_REPORTING_CFG_RSP_t;
#define ZCL_READ_REPORTING_CFG_RSP_SIZE(_s, _d, _rc) ( sizeof(ZCL_CONFIGURE_REPORTING_RSP_t) + ((_s) != SZL_STATUS_SUCCESS ?  0 : ((_d) == ZCL_CONFIGURE_REPORTING_SENDING) ? ZCL_CONFIGURE_REPORTING_SENDING_SIZE(_rc) : ZCL_CONFIGURE_REPORTING_RECEIVING_SIZE ))

typedef struct
{
    szl_uint16 start_attribute_id;      // The start attribute identifier field specifies the value of the identifier at which to begin the attribute discovery
    szl_uint8 maximum_attribute_ids;    // The maximum attribute identifiers field specifies the maximum number of attribute identifiers that are to be returned in the resulting discover attributes response command
} ZCL_DISCOVER_ATTRIBUTES_t;
#define ZCL_DISCOVER_ATTRIBUTES_SIZE ( sizeof(ZCL_DISCOVER_ATTRIBUTES_t))

typedef struct
{
    szl_uint16 attribute_id; // The attribute id
    szl_uint8 data_type;     // contain the data type of the attribute.
} ZCL_DISCOVER_ATTRIBUTE_t;

typedef struct
{
  szl_bool discovery_complete;
  ZCL_DISCOVER_ATTRIBUTE_t attributes[SZL_VLA_INIT];
} ZCL_DISCOVER_ATTRIBUTES_RSP_t;
#define ZCL_DISCOVER_ATTRIBUTES_RSP_SIZE(num_attributes) ( sizeof(ZCL_DISCOVER_ATTRIBUTES_RSP_t) + ((num_attributes) * sizeof(ZCL_DISCOVER_ATTRIBUTE_t)) - (SZL_VLA_INIT * sizeof(ZCL_DISCOVER_ATTRIBUTE_t)))

#ifdef PRAGMA_PACK
#pragma pack()
#endif



// Status codes
typedef enum
{
    ZCL_SUCCESS                     = 0x00, // Operation was successful.
    ZCL_FAILURE                     = 0x01, // Operation was not successful.
    ZCL_NOT_AUTHORIZED              = 0x7e, // The sender of the command does not have authorization to carry out this command.
    ZCL_RESERVED_FIELD_NOT_ZERO     = 0x7f, // A reserved field/subfield/bit contains a non-zero value.
    ZCL_MALFORMED_COMMAND           = 0x80, // The command appears to contain the wrong fields, as detected either by the presence of one or more invalid field entries or by there being missing fields.
    ZCL_UNSUP_CLUSTER_COMMAND       = 0x81, // The specified cluster command is not supported on the device.
    ZCL_UNSUP_GENERAL_COMMAND       = 0x82, // The specified general ZCL command is not supported on the device.
    ZCL_UNSUP_MANUF_CLUSTER_COMMAND = 0x83, // A manufacturer specific unicast, cluster specific command was received with an unknown manufacturer code, or the manufacturer code was recognized but the command is not supported.
    ZCL_UNSUP_MANUF_GENERAL_COMMAND = 0x84, // A manufacturer specific unicast, ZCL specific command was received with an unknown manufacturer code, or the manufacturer code was recognized but the command is not supported.
    ZCL_INVALID_FIELD               = 0x85, // At least one field of the command contains an incorrect value, according to the specification the device is implemented to.
    ZCL_UNSUPPORTED_ATTRIBUTE       = 0x86, // The specified attribute does not exist on the device.
    ZCL_INVALID_VALUE               = 0x87, // Out of range error, or set to a reserved value. Attribute keeps its old value.
    ZCL_READ_ONLY                   = 0x88, // Attempt to write a read only attribute.
    ZCL_INSUFFICIENT_SPACE          = 0x89, // An operation (e.g. an attempt to create an entry in a table) failed due to an insufficient amount of free space available.
    ZCL_DUPLICATE_EXISTS            = 0x8a, // An attempt to create an entry in a table failed due to a duplicate entry already being present in the table.
    ZCL_NOT_FOUND                   = 0x8b, // The requested information (e.g. table entry) could not befound.
    ZCL_UNREPORTABLE_ATTRIBUTE      = 0x8c, // Periodic reports cannot be issued for this attribute.
    ZCL_INVALID_DATA_TYPE           = 0x8d, // The data type given for an attribute is incorrect.
    ZCL_INVALID_SELECTOR            = 0x8e, // The selector for an attribute is incorrect.
    ZCL_WRITE_ONLY                  = 0x8f, // A request has been made to read an attribute that the requestor is not authorized to read. No action taken.
    ZCL_INCONSISTENT_STARTUP_STATE  = 0x90, // Setting the requested values would put the device in an inconsistent state on startup.
    ZCL_DEFINED_OUT_OF_BAND         = 0x91, // An attempt has been made to write an attribute that is present but is defined using an out-of-band method and not over the air.
    ZCL_INCONSISTENT                = 0x92, // The supplied values (e.g. contents of table cells) are inconsistent.
    ZCL_ACTION_DENIED               = 0x93, // The credentials presented by the device sending the command are not sufficient to perform this action.
    ZCL_TIMEOUT                     = 0x94, // The exchange was aborted due to excessive response time.
//    ZCL_ASDU_TOO_LONG               = 0xa0, // A transmit request failed since the ASDU is too large and fragmentation is not supported.
//    ZCL_DEFRAG_DEFERRED             = 0xa1, // A received fragmented frame could not be defragmented at the current time.
//    ZCL_DEFRAG_UNSUPPORTED          = 0xa2, // A received fragmented frame could not be defragmented since the device does not support fragmentation.
//    ZCL_ILLEGAL_REQUEST             = 0xa3, // A parameter value was out of range.
//    ZCL_INVALID_BINDING             = 0xa4, // An APSME-UNBIND.request failed due to the requested binding link not existing in the binding table.
//    ZCL_INVALID_GROUP               = 0xa5, // An APSME-REMOVE-GROUP.request has been issued with a group identifier that does not appear in the group table.
//    ZCL_INVALID_PARAMETER           = 0xa6, // A parameter value was invalid or out of range.
//    ZCL_NO_ACK                      = 0xa7, // An APSDE-DATA.request requesting acknowledged transmission failed due to no acknowledgement being received.
//    ZCL_NO_BOUND_DEVICE             = 0xa8, // An APSDE-DATA.request with a destination addressing mode set to 0x00 failed due to there being no devices bound to this device.
//    ZCL_NO_SHORT_ADDRESS            = 0xa9, // An APSDE-DATA.request with a destinationaddressing mode set to 0x03 failed due to no corresponding short address found in the address map table.
//    ZCL_NOT_SUPPORTED               = 0xaa, // An APSDE-DATA.request with a destination addressing mode set to 0x00 failed due to a binding table not being supported on the device.
//    ZCL_SECURED_LINK_KEY            = 0xab, // An ASDU was received that was secured using a link key.
//    ZCL_SECURED_NWK_KEY             = 0xac, // An ASDU was received that was secured using a network key.
//    ZCL_SECURITY_FAIL               = 0xad, // An APSDE-DATA.request requesting security has resulted in an error during the corresponding security processing.
//    ZCL_TABLE_FULL                  = 0xae, // An APSME-BIND.request or APSME.ADDGROUP. request issued when the binding or group tables, respectively, were full.
//    ZCL_UNSECURED                   = 0xaf, // An ASDU was received without any security.
//    ZCL_UNSUPPORTED_ATTRIBUTE_APSME = 0xb0, // An APSME-GET.request or APSMESET.request has been issued with an unknown attribute identifier.
    ZCL_HARDWARE_FAILURE            = 0xc0, // An operation was unsuccessful due to a hardware failure.
    ZCL_SOFTWARE_FAILURE            = 0xc1, // An operation was unsuccessful due to a software failure.
    ZCL_CALIBRATION_ERROR           = 0xc2, // An error occurred during calibration.
            
    ZCL_HAS_RESPONDED = 0xfe, // added by mvdb
} ZCL_STATUS;

#endif /* _ZCL_H_ */



