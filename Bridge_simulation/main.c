/***********************************************************************************************//**
 * \file   main.c
 * \brief  Silicon Labs Empty Example Project
 *
 * This example demonstrates the bare minimum needed for a Blue Gecko C application
 * that allows Over-the-Air Device Firmware Upgrading (OTA DFU). The application
 * starts advertising after boot and restarts advertising after a connection is closed.
 ***************************************************************************************************
 * <b> (C) Copyright 2016 Silicon Labs, http://www.silabs.com</b>
 ***************************************************************************************************
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 **************************************************************************************************/


#ifndef GENERATION_DONE
#error You must run generate first!
#endif

/* Board headers */
#include "boards.h"
#include "ble-configuration.h"
#include "board_features.h"
#include "platform.h"

/* Bluetooth stack headers */
#include "bg_types.h"
#include "native_gecko.h"
#include "gatt_db.h"
#include "aat.h"

/* Libraries containing default Gecko configuration values */
#include "em_emu.h"
#include "em_cmu.h"
#include "em_rtcc.h"
#ifdef FEATURE_BOARD_DETECTED
#include "bspconfig.h"
#include "pti.h"
#endif

/* Device initialization header */
#include "InitDevice.h"

#ifdef FEATURE_SPI_FLASH
#include "em_usart.h"
#include "mx25flash_spi.h"
#endif /* FEATURE_SPI_FLASH */


/* application specific headers */
#include "app_ui.h"
#include "app_timer.h"
#include "uart_echo.h"


#include <stdio.h>
#include "stdio.h"
#include "retargetserial.h"
#include "gpiointerrupt.h"


/***********************************************************************************************//**
 * @addtogroup Application
 * @{
 **************************************************************************************************/

/***********************************************************************************************//**
 * @addtogroup app
 * @{
 **************************************************************************************************/

/* connection parameters */
#define CONN_INTERVAL_MIN     40      //50ms
#define CONN_INTERVAL_MAX     40      //50ms
#define CONN_SLAVE_LATENCY    0       //no latency
#define CONN_TIMEOUT          100     //1000ms

/* scan parameters */
#define SCAN_INTERVAL  		    32      //20ms  
#define SCAN_WINDOW  		      32      //20ms
#define SCAN_PASSIVE          0

/* advertiser parameters */
#define ADV_INTERVAL_MIN      160     //100ms
#define ADV_INTERVAL_MAX      160     //100ms

#define SIZE_BUFF_ADV_DATA    16
#define SIZE_BUFF_ADV         ( SIZE_BUFF_ADV_DATA + 5 )

/* Number of slaves available for tests */
#define NB_DEVICES_AVAILABLE  11

#ifndef MAX_CONNECTIONS
#define MAX_CONNECTIONS       5
#endif

#define THREAD_PEER_BRIDGE      0
#define THREAD_ZIGBEE_BRIDGE    1

uint8_t bluetooth_stack_heap[DEFAULT_BLUETOOTH_HEAP(MAX_CONNECTIONS)];

#ifdef FEATURE_PTI_SUPPORT
static const RADIO_PTIInit_t ptiInit = RADIO_PTI_INIT;
#endif

  
/* Gecko configuration parameters (see gecko_configuration.h) */
static const gecko_configuration_t config = {
  .config_flags=0,
  .sleep.flags=SLEEP_FLAGS_DEEP_SLEEP_ENABLE,
  .bluetooth.max_connections=MAX_CONNECTIONS,
  .bluetooth.heap=bluetooth_stack_heap,
  .bluetooth.heap_size=sizeof(bluetooth_stack_heap),
  .bluetooth.sleep_clock_accuracy = 100, // ppm
  .gattdb=&bg_gattdb_data,
  .ota.flags=0,
  .ota.device_name_len=3,
  .ota.device_name_ptr="OTA",
  #ifdef FEATURE_PTI_SUPPORT
  .pti = &ptiInit,
  #endif
};

/* the UUID of the ms service */
const char test_service_uuid[16] =
  {
    0xF0, 0x19, 0x21, 0xB4, 0x47, 0x8F, 0xA4, 0xBF,
    0xA1, 0x4F, 0x63, 0xFD, 0xEE, 0xD6, 0x14, 0x1D
  };
/* and its little endian version */
char test_service_uuid_le[16];

/* the UUID of the ms control characteristic */
const char test_control_characteristic_uuid[16] =
  {
    0x63, 0x60, 0x32, 0xe0, 0x37, 0x5e, 0xa4, 0x88, 
    0x53, 0x4e, 0x6d, 0xfb, 0x64, 0x35, 0xbf, 0xf7
  };
//and its little endian version
char test_control_characteristic_uuid_le[16];

/* the UUID of the ms control characteristic */
const char test_data_characteristic_uuid[16] =
  {
    0x53, 0xA1, 0x81, 0x1F, 0x58, 0x2C, 0xD0, 0xA5, 
    0x45, 0x40, 0xFC, 0x34, 0xF3, 0x27, 0x42, 0x98
  };
//and its little endian version
char test_data_characteristic_uuid_le[16];


/* Found MS descriptors */
uint32_t test_gatt_service_handle = 13;
uint16_t test_control_characteristic = 15;
uint16_t test_data_characteristic = 18;


/* Flag for indicating DFU Reset must be performed */
uint8_t boot_to_dfu = 0;


/* connection handling variables for multiple slaves */
uint8  connection_handle_table[MAX_CONNECTIONS];
uint32 service_handle_table[MAX_CONNECTIONS][50];
uint8  active_connections_num;

/******************************************************************************
Typedef definitions
******************************************************************************/
typedef enum
{
	ACK,
	NACK,
}e_ack_value;

/* Index of command and associated value in the UART frame between bridge and PLC */
typedef enum
{
	IDX_START_CHAR			= 0x00,
	IDX_FRAME_SIZE			= 0x01,
	IDX_CMD					    = 0x02,
	IDX_COUNTER				  = 0x03,
	IDX_CONTROL				  = 0x04,
	IDX_DATA				    = 0x04,
	IDX_ACK					    = 0x04,
	IDX_STATE				    = 0x05,
	IDX_DEVICE_TYPE			= 0x05,
	IDX_ID_NETWORK			= 0x05,
	IDX_NETWORK_STATE		= 0x06,
	IDX_DISCOVERY_STATE	= 0x06,
	IDX_ADV_STATE			  = 0x06,
	IDX_MAC_ADDRESS			= 0x06,
}e_frame_index;

/* Command type */
typedef enum
{
	COMMAND_TYPE_CONTROL  = 0x00,
	COMMAND_TYPE_DATA     = 0x01,
	COMMAND_TYPE_ACK      = 0x02,
}e_command_type;

/* Techno type */
typedef enum
{
	TECHNO_PLC,
	TECHNO_LORA,
}e_techno_type;

/* Control command type */
typedef enum
{
	CONTROL_CMD_SATE,
	CONTROL_CMD_DEVICE_TYPE,
	CONTROL_CMD_MAC_ADDRESS,
}e_control_command_type;

/* Network state */
typedef enum
{
	NWK_STATE_UNINITIALISED,
	NWK_STATE_WAIT_CONFIG,
	NWK_STATE_NONE,
	NWK_STATE_NETWORKING,
	NWK_STATE_NETWORKED,
	NWK_STATE_NETWORK_LOSS,
}e_network_state;

/* Discovery state, only in when sate = NWK_STATE_NONE */
typedef enum
{
	NWK_STATE_NO_DISCOVERY,
	NWK_STATE_DISCOVERING,
	NWK_STATE_DISCOVERY_COMPLETE,
}e_network_state_discovery;

/* PLC device type */
typedef enum
{
	PLC_DEVICE_PEER,
	PLC_DEVICE_CONCENTRATOR,
}e_plc_device_type;

/* Network state */
typedef struct
{
	uint8_t							type;			/* 0-Network, 1-Discovery, 2-Advertising */

	union
	{
		e_network_state				network;		/* Network state: 0-Uninitialized, 1-None, 2-Networking, 3-Networked, 4-Network loss */
		e_network_state_discovery	discovey;		/* Discovery state: 0-No discovery, 1-Discovering, 2-Discovery complete */
	};
}t_network_state;

/* Device type */
typedef struct
{
	e_plc_device_type				type;			/* 0-Peer, 1-Concentrator */
	uint16_t						mac_address;	/* Peer mac address */
}t_plc_device_type;

/* Id network */
typedef struct
{
	uint8_t							nb_netwok;		/* Nb of network plc available */
	uint16_t						id[5];			/* Id of each network */
}t_id_network;

/* Id network */
typedef struct
{
	uint8_t							ack;			/* 0-Ack, 1-Nack */
}t_ack_frame;

/* Struct of control frame received from the bridge */
typedef struct
{
	e_control_command_type			control;		/* Control cmd type: 0-State network, 1-Deviceplc type, 2-ID network plc */

	/* List of different control frame type */
	union
	{
		t_network_state				network_state;	/* Network state (network, discovery, advertising */
		t_plc_device_type			device_plc;		/* Device plc plugged */
		t_id_network				id_network;		/* Id device plc */
	};
}t_control_frame;

/* Struct of frame received from the bridge */
typedef struct
{
	uint8_t   						size;			/* Frame size */
	e_techno_type					techno;			/* Techno plugged to the bridge: 0-PLC, 1-LorA...*/
	e_control_command_type			command;		/* Command type: 0-Control, 1-Data, 2-Ack */

	/* List of different frame type */
	union
	{
		t_control_frame				control_frame;	/* Network state (network, discovery, advertising */
		uint8_t data[128];							/* Data to transmit */
		t_ack_frame					ack_frame;		/* Ack frame response */
	};
}bridge_packet;


/* LCD line to display */
char network_state_l1[20]     = "Network state:    \n";  /* Network state line 1 */
char network_state_l2[20]     = "RESET             \n";  /* Network state line 2 */
char line2[20]                = "                  \n";
char line3[20]                = "                  \n";
char line4[20]                = "                  \n";
char line5[20]                = "                  \n";
char line6[20]                = "                  \n";
char line7[20]                = "                  \n";
char line8[20]                = "                  \n";
char line9[20]                = "                  \n";
char line10[20]               = "                  \n";

/* find the index of a given connection in the connection handle table */
uint8 find_connection_handle(uint8 connection)
{
	uint8 c;
	for (c = 0; c < active_connections_num; c++)
	{
		if (connection_handle_table[c] == connection)
		{
			return c;
		}
	}

	return 0xFF;
}

/* Push button generates external signal event for the stacks */  
void handle_button_push(uint8_t pin)
{
	gecko_external_signal((uint32)pin);
}

/* Return elapsed time in miliseconds */
uint32_t Timeout( uint32_t start_time )
{
  uint32_t time, elapsedtime;
  
  time = RTCC_CounterGet();     
  elapsedtime = ( time - start_time );
  
  return elapsedtime;
}

/* Print 10 lines of the lcd */
void PrintLCD( void )
{
  char text[200];  
  strcpy( text, network_state_l1 );
  strcat( text, network_state_l2 );
  appUiWriteString( text );
}

   #ifndef FEATURE_IOEXPANDER
/* Periodically called Display Polarity Inverter Function for the LCD.
   Toggles the the EXTCOMIN signal of the Sharp memory LCD panel, which prevents building up a DC
   bias according to the LCD's datasheet */
static void (*dispPolarityInvert)(void *);
  #endif /* FEATURE_IOEXPANDER */

/**************************************************************************//**
 * @brief   Register a callback function at the given frequency.
 *
 * @param[in] pFunction  Pointer to function that should be called at the
 *                       given frequency.
 * @param[in] argument   Argument to be given to the function.
 * @param[in] frequency  Frequency at which to call function at.
 *
 * @return  0 for successful or
 *         -1 if the requested frequency does not match the RTC frequency.
 *****************************************************************************/
int rtcIntCallbackRegister(void (*pFunction)(void*),
                           void* argument,
                           unsigned int frequency)
{
  #ifndef FEATURE_IOEXPANDER

  dispPolarityInvert =  pFunction;
  /* Start timer with required frequency */
  gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(1000 / frequency), DISP_POL_INV_TIMER, false);

  #endif /* FEATURE_IOEXPANDER */

  return 0;
}


/******************************************************************************
* Function: calcFCS
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint8_t	calcFCS( uint8_t *pMsg, uint8_t len )
{
	uint8_t result = 0;

	while( len-- )
	{
		result ^= *pMsg++;
	}

	return result;
}
/******************************************************************************
   End of function  calcFCS
******************************************************************************/



/**
 * @brief  Main function  
 */
void main(void)
{
	e_network_state 				    nwk_state 			    = NWK_STATE_UNINITIALISED;
	e_network_state_discovery 	nwk_state_discovery = NWK_STATE_NO_DISCOVERY;
	uint8_t buff_tx[20] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                          0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                          0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                          0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
  
  uint8_t buf[13] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
  uint8_t buf_rx[10]; 
  uint8_t frame_counter = 0;
  
  /* Unique device ID */
  uint16_t devId;
  struct gecko_msg_system_get_bt_address_rsp_t* btAddr;
  char devName[APP_DEVNAME_LEN + 1];
  
  struct gecko_cmd_packet* evt;  
  
  /* Initialize peripherals */
  enter_DefaultMode_from_RESET();

  /* Init UART */
//  RETARGET_SerialInit(); // OU PAS
  UART_Init();

  /* Initialize stack */
  gecko_init(&config);  

  /* Test HW init */  
  GPIOINT_Init();
  
  /* Create the device name based on the 16-bit device ID */
  btAddr = gecko_cmd_system_get_bt_address();
  devId = *((uint16*)(btAddr->address.addr));
  
  /* Initialize graphics. */
  appUiInit(devId);
  PrintLCD();
  
  GPIO_PinOutClear( gpioPortF, 4 );        
  
  while (1) 
  {
    /* Event pointer for handling events */
    struct gecko_cmd_packet* evt;

    /* Check for stack event. */
    evt = gecko_wait_event();
    
    /* Check stack event */
    switch( BGLIB_MSG_ID( evt->header ) )
    {       
      /* This boot event is generated when the system boots up after reset. */
      case gecko_evt_system_boot_id:        
        /* Start the repeating timer. The 1st parameter '32768'
         * tells the timer to run for 2 second (32.768 kHz oscillator), the 2nd parameter is
         * the timer handle and the 3rd parameter '0' tells the timer to repeat continuously until
         * stopped manually.*/
        gecko_cmd_hardware_set_soft_timer( 65536, THREAD_PEER_BRIDGE, 0 );
        break;
        
      /* this event is triggered by gecko_external_signal() *
      * in our case when a button is pushed     */
      case gecko_evt_system_external_signal_id:
        /* BP0 - Change Phy*/
        if( ( evt->data.evt_system_external_signal.extsignals == 8 ) || ( evt->data.evt_system_external_signal.extsignals == 4 ) )
        {              
        }
        break;
        
      /* This event is generated when the software timer has ticked. In this example the temperature
      * is read after every 1 second and then the indication of that is sent to the listening client. */
      case gecko_evt_hardware_soft_timer_id:   
        switch( evt->data.evt_hardware_soft_timer.handle )
        {
          case THREAD_PEER_BRIDGE:            
            /* Blinking according to soft timer period*/
            GPIO_PinOutToggle( gpioPortF, 4 );                 
            
            buf[IDX_START_CHAR] = 0xFE;
            buf[IDX_FRAME_SIZE] = 14;
            
            buf[IDX_CMD]        = COMMAND_TYPE_DATA;
            buf[IDX_COUNTER]    = frame_counter++;
            buf[IDX_DATA]       = 1;
            buf[IDX_DATA+1]     = frame_counter;
            buf[IDX_DATA+2]     = '2';
            buf[IDX_DATA+3]     = '3';
            buf[IDX_DATA+4]     = '4';
            buf[IDX_DATA+5]     = '5';
            buf[IDX_DATA+6]     = '6';
            buf[IDX_DATA+7]     = '7';
            buf[IDX_DATA+8]     = '8';
            buf[IDX_DATA+9]     = '9';     
            
            buf[IDX_DATA+10]    = calcFCS( &buf[2], buf[IDX_FRAME_SIZE]-2 );
            
            UART_Send( buf, buf[IDX_FRAME_SIZE]+1 );
            /* Here are processed all the actions linked to the states of the bridge/peer thread */
            switch( nwk_state )
            {  
              case NWK_STATE_UNINITIALISED:
                /* Display thread state */
                snprintf( network_state_l2, sizeof(network_state_l2), "UNINITIALISED     \n" );
                PrintLCD();
                
                /* Send network state to the peer PLC */
                break;
              
              case NWK_STATE_NONE:
                /* Display thread state */
                snprintf( network_state_l2, sizeof(network_state_l2), "NONE              \n" );
                PrintLCD();
                                
                switch( nwk_state_discovery )
                {
                  case NWK_STATE_NO_DISCOVERY:
                    /* Display thread state */
                    snprintf( network_state_l2, sizeof(network_state_l2), "NO DISCOVER       \n" );
                    PrintLCD();
                    
                    /* Send network state to the peer PLC */
                    break;
                  
                  case NWK_STATE_DISCOVERING:
                    /* Display thread state */
                    snprintf( network_state_l2, sizeof(network_state_l2), "DISCOVERING       \n" );
                    PrintLCD();
                    
                    /* Send network state to the peer PLC */
                    break;
                  
                  case NWK_STATE_DISCOVERY_COMPLETE:
                    /* Display thread state */
                    snprintf( network_state_l2, sizeof(network_state_l2), "DISCOVERY COMPLETE\n" );
                    PrintLCD();
                    
                    /* Send network state to the peer PLC */
                    break;
                    
                  default:
                    break;
                }                
                break;
              
              case NWK_STATE_NETWORKED:
                /* Display thread state */
                snprintf( network_state_l2, sizeof(network_state_l2), "NETWORKED         \n" );
                PrintLCD();
                
                /* Send network state to the peer PLC */
                break;
              
              case NWK_STATE_NETWORKING:
                /* Display thread state */
                snprintf( network_state_l2, sizeof(network_state_l2), "NETWORKING        \n" );
                PrintLCD();
                break;
              
//              case NWK_STATE_LEAVING:
//                /* Display thread state */
//                snprintf( network_state_l2, sizeof(network_state_l2), "LEAVING           \n" );
//                PrintLCD();
//                break;
//              
//              case NWK_STATE_ADVERTISING:
//                /* Display thread state */
//                snprintf( network_state_l2, sizeof(network_state_l2), "ADVERTISING       \n" );
//                PrintLCD();
//                break;
//              
//              case NWK_STATE_ADVERTISING_COMPLETE:
//                /* Display thread state */
//                snprintf( network_state_l2, sizeof(network_state_l2), "ADVERTISING CPLT  \n" );
//                PrintLCD();
//                break;
              
              default:
                break;      
            }
            break;
            
          case THREAD_ZIGBEE_BRIDGE:
            break;
            
          default:
            break;            
        }
        break;
        
      default:
        break;
    }
  } /* End while */
} 

