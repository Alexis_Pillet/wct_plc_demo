#include "appConfig.h"
#include "appMain.h"
//#include "appZabCoreGlue.h"
#include "zabCoreService.h"

#include "appZcl.h"
#include "appZclBasic.h"
#include "appZdo.h"
#include "appGp.h"
#include "appGateway.h"
#include "appZclPollControlClient.h"
#include "appDevMan.h"

#include "basic_cluster.h"
#include "identify_cluster.h"
#include "discovery.h"
#include "time_cluster.h"
#include "poll_control_client.h"

/******************************************************************************
 *                      *****************************
 *                 *****        CONFIGURATION        *****
 *                      *****************************
 ******************************************************************************/

/* Endpoint that will be used by the application */
#define APP_ENDPOINT 0x01

#define ZAB_TYPES_M_NETWORK_PAN_ID_UNINITIALISED 0xFFFE
/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/
/* States of the Gateway */
typedef enum
{
  gateway_closed,
  gateway_opening,
  gateway_getting_nwk_info,
  gateway_updating_firmware,
  gateway_nwk_resuming,
  gateway_nwk_form_init,
  gateway_nwk_forming,
  gateway_nwk_done,
} gateway_state;

/******************************************************************************
 *                      ******************************
 *                 *****       LOCAL VARIABLES        *****
 *                      ******************************
 ******************************************************************************/
/* State of gateway state machine */
static gateway_state gatewayState = gateway_closed;

/* Track is SZL is already initialised (hence no need ot re-init it on gateway restart */
static szl_bool szlInitialised = szl_false;

/* Stored copies of notified ZAB states */
static zabOpenState gatewayOpenState = ZAB_OPEN_STATE_CLOSED;
static zabNwkState gatewayNwkState = ZAB_NWK_STATE_NONE;

/* Stored copies of given ZAB data */
static unsigned64 networkProcessorIeee;
static unsigned64 networkEpid;
static unsigned16 networkAddress;
static unsigned16 networkPanId;
static zabNetworkProcessorModel nwkProcHardwareType;
static unsigned8 nwkProcAppVersion[4];

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/
 /******************************************************************************
 * Check for error and cleanup if one is found
 ******************************************************************************/
void checkStatusAndKillIfError(erStatus* Status)
{
  erStatus localStatus;
  erStatusClear(&localStatus, appService);

  if (erStatusIsError(Status))
    {
      printApp(appService, "AppGateway: Error 0x%04 detected (%s)\n",
               erStatusGetError(Status),
               erStatusUtility_GetErrorString((zabError)erStatusGetError(Status)));

     /* gatewayState = gateway_closed;
      zabCoreAction(&localStatus, appService, ZAB_ACTION_CLOSE);*/
    }
}

void appGateway_InitValue(void)
{
  networkAddress = ZAB_TYPES_M_NETWORK_PAN_ID_UNINITIALISED;
  networkPanId = ZAB_TYPES_M_NETWORK_PAN_ID_UNINITIALISED;
  networkEpid = 0;
  networkProcessorIeee = 0;  
}
/******************************************************************************
 * Init the SZL for the App
 ******************************************************************************/

void initialiseSzlApplication(void)
{
  SZL_Initialize_t init;
  SZL_RESULT_t res;

  /* Init the SZL is it has not been previously initialised */
  if (szlInitialised == szl_false)
    {

      printApp(appService, "AppGateway: Initialising SZL for endpoint 0x%02X...\n", APP_ENDPOINT);

      /* Init SZL */
      init.Version = SZLAPIVersion;
      init.NumberOfEndpoints = 1;
      init.Endpoints[0] = APP_ENDPOINT;
      res = SZL_Initialize(appService, &init);

      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_Initialize Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
          printApp(appService, "AppGateway: Critical Failure. Aborted.\n");
          return;
        }
      szlInitialised = szl_true;

      /* Register useful handlers */
      res = SZL_AttributeChangedNtfRegisterCB(appService, appZcl_AttributeChangedNotificationHandler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_AttributeChangedNtfRegisterCB Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SZL_AttributeReportNtfRegisterCB(appService, appZcl_ReportAttrHandler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_AttributeReportNtfRegisterCB Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SZL_ReceivedSignalStrengthNtfRegisterCB(appService, appZcl_ReceivedSignalStrengthHandler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_ReceivedSignalStrengthNtfRegisterCB Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SZL_NwkLeaveNtfRegisterCB(appService, appZdo_NetworkLeaveIndicationHandler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_NwkLeaveNtfRegisterCB Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

    #ifdef APP_CONFIG_ENABLE_GREEN_POWER
      res = SZL_GP_GpdCommissioningNtfRegisterCB(appService, appGp_GpdCommissioningNtf_Handler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_GP_GpdCommissioningNtfRegisterCB Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SZL_GP_GpdCommissionedNtfRegisterCB(appService, appGp_GpdCommissionedNtf_Handler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_GP_GpdCommissionedNtfRegisterCB Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }
    #endif

      /* Init Plugins */
      res = appZcl_SzlBasicPlugin_InitEndpoints(init.NumberOfEndpoints, init.Endpoints);
      if (res == SZL_RESULT_SUCCESS)
        {
          res = SzlPlugin_BasicClusterInit(appService, init.NumberOfEndpoints, init.Endpoints,
                                            appZcl_SzlBasicPlugin_ReadAttrHandler,
                                            appZcl_SzlBasicPlugin_WriteAttrHandler);
        }
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SzlPlugin_BasicClusterInit Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SzlPlugin_IdentifyClusterInit(appService, init.NumberOfEndpoints, init.Endpoints,
                                          appZcl_SzlIdentifyPluginHandler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SzlPlugin_IdentifyClusterInit Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SzlPlugin_TimeCluster_Init(appService, init.Endpoints[0], appZcl_SzlTimePluginHandler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SzlPlugin_TimeCluster_Init Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SzlPlugin_Discovery_Init(appService, init.Endpoints[0], appZdo_Discovery_NodeDetected_Handler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SzlPlugin_Discovery_Init Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SzlPlugin_PollControlClient_Init(appService, 1, &init.Endpoints[0],
                                             appZclPollControlClient_CheckinNotificationHandler,
                                             appZclPollControlClient_FastPollStopRespHandler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SzlPlugin_PollControlClient_Init Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = appDevMan_Init(appService, 20, 600);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: appDevMan_Init Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }


      /* Init Client Clusters */
      const szl_uint16 clientClusterList[] = {0x0000, 0x0003, 0x0006, 0x0702, 0x0B04, 0xFF14};
      res = SZL_ClientClusterRegister(appService, init.Endpoints[0], clientClusterList, sizeof(clientClusterList)/sizeof(szl_uint16));
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_ClientClusterRegister Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }
     
    }

  printApp(appService, "AppGateway: Process complete!!!\n");
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/
void appGateway_GetNetworkProcessorIeee(char* readNwkProcAppVersion)
{

            memcpy(readNwkProcAppVersion, nwkProcAppVersion, 4);

}
/******************************************************************************
 * Start the Gateway process
 ******************************************************************************/
void appGateway_Start(void)
{
  erStatus localStatus;
  erStatusClear(&localStatus, appService);

  if ( (gatewayState == gateway_closed) &&
       (gatewayOpenState == ZAB_OPEN_STATE_CLOSED) )
    {
      printApp(appService, "AppGateway: Opening...\n");
      gatewayState = gateway_opening;
      appGateway_InitValue();
      zabCoreAction(&localStatus, appService, ZAB_ACTION_OPEN);
      checkStatusAndKillIfError(&localStatus);
    }
  else
    {
      printApp(appService, "AppGateway: Invalid state\n");
    }
}

/******************************************************************************
 * Handle Open State Notifications
 ******************************************************************************/
void appGateway_OpenStateNtfHandler(zabOpenState openState)
{
  erStatus localStatus;
  erStatusClear(&localStatus, appService);

  gatewayOpenState = openState;

  if ( (gatewayState == gateway_opening) ||
       /* Also handle reset when gateway is running by re-starting network */
       ( (gatewayOpenState == ZAB_OPEN_STATE_OPENED) &&
         (gatewayNwkState == ZAB_NWK_STATE_UNINITIALISED) &&
         (gatewayState == gateway_nwk_done) ) )
    {
      switch(openState)
        {
          case ZAB_OPEN_STATE_OPENING:
            /* Do nothing */
            break;
          case ZAB_OPEN_STATE_OPENED:
            /* Successfully opened.
             * If FW is up to date then get network info.
             * If not, go to FW updater and update it */
              printApp(appService, "AppGateway: Getting network info...\n");
              gatewayState = gateway_getting_nwk_info;
              zabCoreAction(&localStatus, appService, ZAB_ACTION_NWK_GET_INFO);
              checkStatusAndKillIfError(&localStatus);
            break;
         default:
            /* Error failed */
            gatewayState = gateway_closed;
            printApp(appService, "AppGateway: Failed to open.\n");
            break;
        }
      return;
    }

  /* If there is a close while the gateway is active then reset states. */
  if (openState == ZAB_OPEN_STATE_CLOSED)
    {
      if (gatewayState != gateway_closed)
        {
          gatewayState = gateway_closed;
          printApp(appService, "AppGateway: Closed\n");
        }
    }
}    

/******************************************************************************
 * Handle Network State Notifications
 ******************************************************************************/
void appGateway_NwkStateNtfHandler(zabNwkState nwkState)
{

  erStatus localStatus;
  erStatusClear(&localStatus, appService);

  gatewayNwkState = nwkState;
  if (gatewayState == gateway_nwk_resuming)
    {
      switch(nwkState)
        {
          case ZAB_NWK_STATE_NETWORKING:
            /* Do nothing */
            break;

          case ZAB_NWK_STATE_NETWORKED:
          case ZAB_NWK_STATE_NETWORKED_NO_COMMS:
            /* Successfully resumed */
            printApp(appService, "AppGateway: Network resume complete\n");
            gatewayState = gateway_nwk_done;
            initialiseSzlApplication();
            break;

          default:
            gatewayState = gateway_closed;
            printApp(appService, "AppGateway: Network resume failed.\n");
            break;
        }
      return;
    }
  
  if (gatewayState == gateway_nwk_form_init)
    {
      switch(nwkState)
        {
          case ZAB_NWK_STATE_LEAVING:
          case ZAB_NWK_STATE_NETWORKING:
            /* Do nothing */
            break;

          case ZAB_NWK_STATE_NONE:
            /* Successfully re-initialised */
            printApp(appService, "AppGateway: Network init before form complete. Forming...\n");
            gatewayState = gateway_nwk_forming;
            appConfig_Config.nwkSteerOptions = ZAB_NWK_STEER_FORM;
            zabCoreAction(&localStatus, appService, ZAB_ACTION_NWK_STEER);
            checkStatusAndKillIfError(&localStatus);
           break;

          default:
            gatewayState = gateway_closed;
            printApp(appService, "AppGateway: Network init before form failed.\n");
            break;
        }
      return;
    }
 
    if (gatewayState == gateway_nwk_forming)
    {
      switch(nwkState)
        {
          case ZAB_NWK_STATE_NETWORKING:
            /* Do nothing */
            break;

          case ZAB_NWK_STATE_NETWORKED:
            /* Successfully formed */
            printApp(appService, "AppGateway: Network form complete\n");
            gatewayState = gateway_nwk_done;
            //commitNetworkInfoToFile(networkProcessorIeee, networkEpid, networkAddress);
            initialiseSzlApplication();
            break;

          default:
            gatewayState = gateway_closed;
            printApp(appService, "AppGateway: Network form failed.\n");
            zabCoreAction(&localStatus, appService, ZAB_ACTION_CLOSE);
            break;
        }
      return;
    }

}

/******************************************************************************
 * Handle Network Info State Notifications
 ******************************************************************************/
void appGateway_NetworkInfoStateNtfHandler(zabNetworkInfoState networkInfoState)
{
  erStatus localStatus;
  erStatusClear(&localStatus, appService);

  if (gatewayState == gateway_getting_nwk_info)
    {
      switch(networkInfoState)
        {
          case ZAB_NETWORK_INFO_STATE_IN_PROGRESS:
            /* Do nothing */
            break;
          case ZAB_NETWORK_INFO_STATE_SUCCESS:
            /* We got all the network info, so compare it with uninitialised value either resume network or wait before form a new one*/

                
               if( networkPanId == ZAB_TYPES_M_NETWORK_PAN_ID_UNINITIALISED)
               {
                 printApp(appService, "AppGateway: Network info does not match - FORMING new network\n");

                gatewayState = gateway_nwk_form_init;
               }
               else
                {
                printApp(appService, "AppGateway: Network info matched. Resuming.\n");

                gatewayState = gateway_nwk_resuming;
                zabCoreAction(&localStatus, appService, ZAB_ACTION_NWK_INIT);
            checkStatusAndKillIfError(&localStatus);
              } 

            
            break;
          default:
            gatewayState = gateway_closed;
            printApp(appService, "AppGateway: Firmware update failed.\n");
            zabCoreAction(&localStatus, appService, ZAB_ACTION_CLOSE);
            break;
        }
    }
}

 /******************************************************************************
 * Handle Asks
 ******************************************************************************/
zab_bool appGateway_AskCfgHandler(erStatus* Status, zabService* Service, zabAskId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer)
{
  unsigned8 Out8;
  printApp(appService,"appGateway_AskCfgHandler\n");
    /* Resuming Previous Network:
   *  - Resume network = TRUE
   *  - Set GP Endpoint to application endpoint */
  if (gatewayState == gateway_nwk_resuming)
    {
      if (What == ZAB_ASK_NWK_RESUME)
        {
          Out8 = (unsigned8)zab_true;
          printApp(Service, "AppGateway: ZAB_ASK_NWK_RESUME = %s\n", Out8 ? "TRUE" : "FALSE");
          osMemCopy( Status, Buffer, &Out8, sizeof(Out8) );
          return zab_true;
        }

#ifdef APP_CONFIG_ENABLE_GREEN_POWER
      else if (What == ZAB_ASK_GREEN_POWER_ENDPOINT)
        {
          Out8 = APP_ENDPOINT;
          printApp(Service, "AppGateway: ZAB_ASK_GREEN_POWER_ENDPOINT = 0x%02X (%d)\n", Out8, Out8);
          osMemCopy( Status, Buffer, &Out8, sizeof(Out8) );
          return zab_true;
        }
#endif
    }

  /* Init Before Forming New Network:
   *  - Resume network = FALSE */
  if (gatewayState == gateway_nwk_form_init)
    {
      if (What == ZAB_ASK_NWK_RESUME)
        {
          Out8 = (unsigned8)zab_false;
          printApp(Service, "AppGateway: ZAB_ASK_NWK_RESUME = %s\n", Out8 ? "TRUE" : "FALSE");
          osMemCopy( Status, Buffer, &Out8, sizeof(Out8) );
          return zab_true;
        }
    }
  
    /* Forming New Network:
   *  - Network Steer = Form
   *  - Channel Mask = All
   *  - EPID = Schneider Wiser EH
   *  - Set GP Endpoint to application endpoint */
  if (gatewayState == gateway_nwk_forming)
    {
      if (What == ZAB_ASK_NWK_STEER)
        {
          Out8 = (unsigned8)ZAB_NWK_STEER_FORM;
          printApp(Service, "AppGateway: ZAB_ASK_NWK_STEER = Form\n");
          osMemCopy( Status, Buffer, &Out8, sizeof(Out8) );
          return zab_true;
        }
#ifdef APP_CONFIG_ENABLE_GREEN_POWER
      else if (What == ZAB_ASK_GREEN_POWER_ENDPOINT)
        {
          Out8 = APP_ENDPOINT;
          printApp(Service, "AppGateway: ZAB_ASK_GREEN_POWER_ENDPOINT = 0x%02X (%d)\n", Out8, Out8);
          osMemCopy( Status, Buffer, &Out8, sizeof(Out8) );
          return zab_true;
        }
#endif
    }
return zab_false;
}
/******************************************************************************
 * Handle Gives
 ******************************************************************************/
void appGateway_GiveCfgHandler(erStatus* Status, zabService* Service, zabGiveId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer)
{
  printApp(appService,"appGateway_GiveCfgHandler\n");
  unsigned32 len;
  erStatus localStatus;
  erStatusClear(&localStatus, appService);

  len = *Size;

  switch (What)
    {
      case ZAB_GIVE_IEEE:
        if (len == sizeof(unsigned64))
          {
            networkProcessorIeee = *(unsigned64*)Buffer;
          }
        break;

      case ZAB_GIVE_NWK_ADDRESS:
        if (len == sizeof(unsigned16))
          {
            networkAddress = *(unsigned16*)Buffer;
          }
        break;

      case ZAB_GIVE_NWK_EPID:
        if (len == sizeof(unsigned64))
          {
            networkEpid = *(unsigned64*)Buffer;
          }
        break;

      case ZAB_GIVE_NETWORK_PROCESSOR_MODEL:
        if (len == sizeof(unsigned8))
          {
            nwkProcHardwareType = (zabNetworkProcessorModel)(*Buffer);
          }
        break;

      case ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_VERSION:
        if (len == 4)
          {
            memcpy(nwkProcAppVersion, Buffer, 4);
          }
        break;
      case ZAB_GIVE_NWK_PAN_ID:
        if (len == sizeof(unsigned16))
          {
            networkPanId =*(unsigned16*)Buffer;
          }
      break;
      default:
        break;
    }
}

 
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/