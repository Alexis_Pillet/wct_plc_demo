
"""
application.application_frame.application_frame_generator
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to parse the applicative frame

"""
from .application_frame_structure import Application
from collections import namedtuple, defaultdict
from enum import IntEnum
import struct


def get_address(frame, index_address):
    """Extract the address source of the frame

    Take into account if this is a IEEE address or Source ID address

    Args:
        frame: data of the frame
        index_address: index of the address in the frame

    Returns:
        A dict with the result
        "address": Address of the device
        "data": payload of the frame
        example:
        >>> {"address": '01020304', "data": {[0x00, 0x00]}}
    """
    result = {"address": None, "data": None}

    if (int(frame[0]) & Application.OptionUpMask.SOURCE_ADDRESS_TYPE) == Application.OptionUpAddressType.IEEE_ADDRESS:
        ieee_address = bytearray(frame[index_address:index_address + 8])
        # Big endian
        ieee_address.reverse()
        result["address"] = ''.join('{:02x}'.format(x) for x in ieee_address)
        if (int(frame[0]) & Application.OptionUpMask.SOURCE_EP_PRESENT) == Application.OptionUpEP.PRESENT:
            # We remove the source End Point
            result["data"] = frame[index_address + 8 + 1:]
        else:
            result["data"] = frame[index_address + 8:]
    elif (int(frame[0]) &
          Application.OptionUpMask.SOURCE_ADDRESS_TYPE) == Application.OptionUpAddressType.SOURCE_ID_ADDRESS:
        source_id = bytearray(frame[index_address:index_address + 4])
        # Big endian
        source_id.reverse()
        result["address"] = ''.join('{:02x}'.format(x) for x in source_id)
        if (int(frame[0]) & Application.OptionUpMask.SOURCE_EP_PRESENT) == Application.OptionUpEP.PRESENT:
            # We remove the source End Point
            result["data"] = frame[index_address + 4 + 1:]
        else:
            result["data"] = frame[index_address + 4:]

    return result


class Attribute:
    """Attribute class"""

    @staticmethod
    def parse():
        """ Association between Attribute type and function to parse it"""
        return {Attribute.Type.BOOLEAN: _parse_boolean,
                Attribute.Type.BITMAP_16: _parse_uint_16,
                Attribute.Type.UINT_16: _parse_uint_16,
                Attribute.Type.UINT_24: _parse_uint_24,
                Attribute.Type.UINT_32: _parse_uint_32,
                Attribute.Type.UINT_48: _parse_uint_48,
                Attribute.Type.INT_16: _parse_int_16,
                Attribute.Type.INT_32: _parse_int_32,
                Attribute.Type.SINGLE_PRECISION: _parse_single_precision,
                Attribute.Type.ENUM_16: _parse_uint_16,
                Attribute.Type.STRING: _parse_string}

    class Type(IntEnum):
        """Type of the attribute"""
        BOOLEAN = 0x10
        BITMAP_16 = 0x19
        UINT_16 = 0x21
        UINT_24 = 0x22
        UINT_32 = 0x23
        UINT_48 = 0x25
        INT_16 = 0x29
        INT_32 = 0x2b
        ENUM_16 = 0x31
        SINGLE_PRECISION = 0x39
        STRING = 0x42


def _parse_boolean(data):
    """Extract the value of a boolean attribute

    Args:
        data: data to parse

    Returns:
        A dict with the result
        "data": Value of the attribute
        "length": length of the data
        example:
        >>> {"data": 1, "length": 2}
    """
    length_data = int(data[0])
    return {"data": int(data[1]), "length": length_data}


def _parse_uint_16(data):
    """Extract the value of an unsigned int 16 attribute

    Args:
        data: data to parse

    Returns:
        A dict with the result
        "data": Value of the attribute
        "length": length of the data
        example:
        >>> {"data": 16, "length": 2}
    """
    length_data = int(data[0])
    return {"data": int.from_bytes(data[1:3], byteorder='little', signed=False), "length": length_data}


def _parse_uint_24(data):
    """Extract the value of an unsigned int 24 attribute

    Args:
        data: data to parse

    Returns:
        A dict with the result
        "data": Value of the attribute
        "length": length of the data
        example:
        >>> {"data": 16, "length": 3}
    """
    length_data = int(data[0])
    return {"data": int.from_bytes(data[1:4], byteorder='little', signed=False), "length": length_data}


def _parse_uint_32(data):
    """Extract the value of an unsigned int 32 attribute

    Args:
        data: data to parse

    Returns:
        A dict with the result
        "data": Value of the attribute
        "length": length of the data
        example:
        >>> {"data": 16, "length": 4}
    """
    length_data = int(data[0])
    return {"data": int.from_bytes(data[1:5], byteorder='little', signed=False), "length": length_data}


def _parse_uint_48(data):
    """Extract the value of an unsigned int 48 attribute

    Args:
        data: data to parse

    Returns:
        A dict with the result
        "data": Value of the attribute
        "length": length of the data
        example:
        >>> {"data": 16, "length": 6}
    """
    length_data = int(data[0])
    return {"data": int.from_bytes(data[1:7], byteorder='little', signed=False), "length": length_data}


def _parse_int_16(data):
    """Extract the value of an int 16 attribute

    Args:
        data: data to parse

    Returns:
        A dict with the result
        "data": Value of the attribute
        "length": length of the data
        example:
        >>> {"data": 16, "length": 2}
    """
    length_data = int(data[0])
    return {"data": int.from_bytes(data[1:3], byteorder='little', signed=True), "length": length_data}


def _parse_int_32(data):
    """Extract the value of an int 32 attribute

    Args:
        data: data to parse

    Returns:
        A dict with the result
        "data": Value of the attribute
        "length": length of the data
        example:
        >>> {"data": 1024, "length": 2}
    """
    length_data = int(data[0])
    return {"data": int.from_bytes(data[1:5], byteorder='little', signed=True), "length": length_data}


def _parse_single_precision(data):
    """Extract the value of an single precision attribute

    Args:
        data: data to parse

    Returns:
        A dict with the result
        "data": Value of the attribute
        "length": length of the data
        example:
        >>> {"data": 16.0, "length": 4}
    """
    length_data = int(data[0])
    return {"data": struct.unpack('<f', data[1:5]), "length": length_data}


def _parse_string(data):
    """Extract the value of a string attribute

    Args:
        data: data to parse

    Returns:
        A dict with the result
        "data": Value of the attribute
        "length": length of the data
        example:
        >>> {"data": "zigbee", "length": 6}
    """
    length_data = int(data[0])
    if length_data == 0:
        return {"data": "", "length": 0}
    else:
        return {"data": data[1:length_data+1].decode("utf-8"), "length": length_data}


def _parse_default(data):
    """Extract the value of a string attribute

    Args:
        data: data to parse

    Returns:
        A dict with the result
        "data": Value of the attribute
        "length": length of the data
        example:
        >>> {"data": "zigbee", "length": 6}
    """
    length_data = int(data[0])
    return {"data": data[1:length_data + 1], "length": length_data}


def get_attributes(data):
    """Extract the attributes of the specified data

    Args:
        data: data to parse

    Returns:
        A defaultdict of named tuple of the parsed attributes
        Named tuple
        >>> AttributeType = namedtuple('Attribute', ['attribute_id', 'attribute_type', 'attribute_value'])
        Key of the defaultdict will be the cluster_id
    """
    AttributeType = namedtuple('AttributeType', ['attribute_id', 'attribute_type', 'attribute_value'])
    result = defaultdict(list)

    success = 0x00
    # Start at the beginning of the cluster id
    index = 0
    cluster_id = bytearray(data[index:index+2])
    # Big endian
    cluster_id.reverse()
    cluster_id = cluster_id.hex()
    # Place index at the start of the attribute list
    index += 2
    while index < len(data):
        attribute_id = bytearray(data[index:index+2])
        # Big endian
        attribute_id.reverse()
        attribute_id = attribute_id.hex()
        index += 2
        if int(data[index]) != success:
            # Pass to the next attribute and start a new treatment
            index += 1
            continue
        # Pass to the next byte and parse the current attribute
        index += 1
        attribute_type = data[index]
        # Parse attribute value
        index += 1
        # Get the function to parse the current attribute of type attribute_type

        value = Attribute.parse().get(attribute_type, _parse_default)(data[index:],)
        attribute = AttributeType(attribute_id, attribute_type, value["data"])
        result[cluster_id].append(attribute)
        # Pass to the next attribute - we are adding 1 to take into account the length of the value
        index += value["length"] + 1

    return result
