/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_sysp.c
*    @version
*        $Rev: 3384 $
*    @last editor
*        $Author: a5089752 $
*    @date
*        $Date:: 2017-05-31 13:49:31 +0900#$
* Description :
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include "r_typedefs.h"
#include "r_stdio_api.h"
#include "r_byte_swap.h"

/* g3 part */
#include "r_c3sap_api.h"


/* app part */
#include "r_demo_api.h"
#include "r_demo_tools.h"
#include "r_demo_app.h"
#include "r_demo_sys.h"
#include "r_demo_status2text.h"


/******************************************************************************
Macro definitions
******************************************************************************/
#define R_DEMPO_APP_EAP_ENTRIES_PER_CYCLE  (16)

    #define R_VOLATILE  volatile

/******************************************************************************
Typedef definitions
******************************************************************************/
/******************************************************************************
Exported global variables
******************************************************************************/
extern r_demo_config_t g_demo_config;
extern r_demo_buff_t   g_demo_buff;


/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/

/******************************************************************************
Private global variables and functions
******************************************************************************/
volatile r_demo_sys_cb_str_t g_syscb;

/******************************************************************************
Functions
******************************************************************************/



/*===========================================================================*/
/*    SYS APIs                                                               */
/*===========================================================================*/


/******************************************************************************
* Function Name: R_DEMO_SysPing
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_SysPing (r_sys_ping_cnf_t ** cnf)
{
    r_result_t                    status;
    R_VOLATILE r_sys_ping_cnf_t * pingCfm = (R_VOLATILE r_sys_ping_cnf_t *)&g_syscb.pingCnf;
    *cnf = (r_sys_ping_cnf_t *)pingCfm;

    pingCfm->status = R_DEMO_SYS_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Sys-Ping requesting... ");
    }

    status = R_SYS_PingReq ();
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {
        /* Wait for completion of R_ADP_AdpmSet */
        while (R_DEMO_SYS_STATUS_NOT_SET == pingCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_SYS_STATUS_SUCCESS == pingCfm->status)
            {
                R_STDIO_Printf ("success. \n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: 0x%.2X\n", pingCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_SysPing */
/******************************************************************************
   End of function  R_DEMO_SysPing
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_SysVersion
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_SysVersion (r_sys_version_cnf_t ** cnf)
{
    r_result_t                       status;
    R_VOLATILE r_sys_version_cnf_t * versionCfm = (R_VOLATILE r_sys_version_cnf_t *)&g_syscb.versionCnf;
    *cnf = (r_sys_version_cnf_t *)versionCfm;

    versionCfm->pfVer = 0;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Sys-Version requesting... ");
    }

    status = R_SYS_VersionReq ();
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {
        /* Wait for completion of R_ADP_AdpmSet */
        while (0 == versionCfm->pfVer)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (3u == versionCfm->pfVer)
            {
                R_STDIO_Printf ("success. Sys version:0x%04X HW version:0x%08X \n", versionCfm->sysVer, versionCfm->hwVer);
            }
            else
            {
                R_STDIO_Printf ("failed. pfVer: 0x%.2X\n", versionCfm->pfVer);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_SysVersion */
/******************************************************************************
   End of function  R_DEMO_SysVersion
******************************************************************************/

/******************************************************************************
* Function Name: R_DEMO_SysGetInfo
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_SysGetInfo (r_sys_get_info_req_t *  req,
                              r_sys_get_info_cnf_t ** cnf)
{
    r_result_t                        status;
    R_VOLATILE r_sys_get_info_cnf_t * getInfoCfm = (R_VOLATILE r_sys_get_info_cnf_t *)&g_syscb.getInfoCnf;
    *cnf = (r_sys_get_info_cnf_t *)getInfoCfm;

    getInfoCfm->status = R_DEMO_SYS_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Sys-GetInfo requesting... ");
    }

    status = R_SYS_GetInfoReq (req);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {
        /* Wait for completion of R_ADP_AdpmSet */
        while (R_DEMO_SYS_STATUS_NOT_SET == getInfoCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_SYS_STATUS_SUCCESS == getInfoCfm->status)
            {
                R_STDIO_Printf ("success. \n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: 0x%.2X\n", getInfoCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_SysGetInfo */
/******************************************************************************
   End of function  R_DEMO_SysGetInfo
******************************************************************************/

/******************************************************************************
* Function Name: R_DEMO_SysClearInfo
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_SysClearInfo (r_sys_clear_info_req_t *  req,
                                r_sys_clear_info_cnf_t ** cnf)
{
    r_result_t                          status;
    R_VOLATILE r_sys_clear_info_cnf_t * clearInfoCfm = (R_VOLATILE r_sys_clear_info_cnf_t *)&g_syscb.clearInfoCnf;
    *cnf = (r_sys_clear_info_cnf_t *)clearInfoCfm;

    clearInfoCfm->status = R_DEMO_SYS_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Sys-ClearInfo requesting... ");
    }

    status = R_SYS_ClearInfoReq (req);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {
        /* Wait for completion of R_ADP_AdpmSet */
        while (R_DEMO_SYS_STATUS_NOT_SET == clearInfoCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_SYS_STATUS_SUCCESS == clearInfoCfm->status)
            {
                R_STDIO_Printf ("success. \n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: 0x%.2X\n", clearInfoCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_SysClearInfo */
/******************************************************************************
   End of function  R_DEMO_SysClearInfo
******************************************************************************/






/*===========================================================================*/
/*    APP for Statistics                                                     */
/*===========================================================================*/
/******************************************************************************
* Function Name: R_DEMO_AppSysGetStatistics
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AppSysGetStatistics (r_sys_info_block_t block)
{
    uint32_t               i;
    const char *           pTxt;

    r_sys_get_info_req_t   req;
    r_sys_get_info_cnf_t * pCnf;

    req.infoType = R_SYS_INFO_TYPE_STATS;
    req.infoBlock = block;

    if (
        (R_DEMO_SysGetInfo (&req, &pCnf) != R_RESULT_SUCCESS) ||
        (R_SYS_STATUS_SUCCESS != pCnf->status)
        )
    {
        return R_RESULT_FAILED;
    }

    switch (block)
    {
        case R_SYS_INFO_BLOCK_UARTIF_0:
            R_STDIO_Printf ("\n   UIF Statistics\n");
            break;

        default:
            block = R_SYS_INFO_BLOCK_MAX;
            break;
    }

    if (R_SYS_INFO_BLOCK_MAX != block)
    {
        for (i = 0; i < (pCnf->length >> 2); i++)
        {
            pTxt = R_LOG_Stats2TextSys ((uint8_t)block, i);
            if (NULL == pTxt)
            {
                break;
            }
            R_STDIO_Printf ("%41s(%2d):%10lu\n", pTxt, i, pCnf->pinfo[i]);
        }
    }

    return R_RESULT_SUCCESS;
} /* R_DEMO_AppSysGetStatistics */
/******************************************************************************
   End of function  R_DEMO_AppSysGetStatistics
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppSysGetLog
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AppSysGetLog (r_sys_info_block_t block)
{
    uint32_t               i;
    r_sys_get_info_req_t   req;
    r_sys_get_info_cnf_t * pCnf;

    req.infoType  = R_SYS_INFO_TYPE_LOGS;
    req.infoBlock = block;

    if (
        (uint32_t)(R_DEMO_SysGetInfo (&req, &pCnf) != R_RESULT_SUCCESS) ||
        (uint32_t)(R_SYS_STATUS_SUCCESS != pCnf->status)
        )
    {
        return R_RESULT_FAILED;
    }

    switch (block)
    {
        case R_SYS_INFO_BLOCK_UARTIF_0:
            R_STDIO_Printf ("\n   UIF Log\n");
            break;

        default:
            break;
    }

    for (i = 0; i < (pCnf->length >> 2); i += 2)
    {
        R_STDIO_Printf ("      %4d: 0x%08X(%10lu[ms])    0x%08X\n", i, pCnf->pinfo[i], pCnf->pinfo[i], pCnf->pinfo[i + 1]);
    }

    return R_RESULT_SUCCESS;
} /* R_DEMO_AppSysGetLog */
/******************************************************************************
   End of function  R_DEMO_AppSysGetLog
******************************************************************************/

/******************************************************************************
* Function Name: R_DEMO_AppSysClearInfo
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AppSysClearInfo (uint8_t type)
{
    r_sys_clear_info_req_t   req = {0}; // all zero(or all 1) mean all clear
    r_sys_clear_info_cnf_t * pCnf;

    req.infoTypeBit = type;

    if (
        (uint32_t)(R_DEMO_SysClearInfo (&req, &pCnf) != R_RESULT_SUCCESS) ||
        (uint32_t)(R_SYS_STATUS_SUCCESS != pCnf->status)
        )
    {
        return R_RESULT_FAILED;
    }
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_DEMO_AppSysClearInfo
******************************************************************************/








/******************************************************************************
* Function Name: menu_sys_statistics
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t menu_sys_statistics (void)
{
    /* Configuration Menu */
    while (1)
    {
        R_STDIO_Printf ("\f-------------------Configuration Menu-----------------------");
        R_STDIO_Printf ("\n 0 - Clear Statistics");
        R_STDIO_Printf ("\n 1 - Clear Log");
        R_STDIO_Printf ("\n 2 - Get UIF Statistics");
        R_STDIO_Printf ("\n 3 - Get UIF Log");
        R_STDIO_Printf ("\n z - Return");

        R_STDIO_Gets ((char *)g_demo_buff.getStringBuffer);

        if (strlen ((char *)g_demo_buff.getStringBuffer) == 1)
        {
            switch (g_demo_buff.getStringBuffer[0])
            {
                case '0':
                    R_DEMO_AppSysClearInfo (R_SYS_INFO_TYPE_BIT_STATS);
                    break;

                case '1':
                    R_DEMO_AppSysClearInfo (R_SYS_INFO_TYPE_BIT_LOGS);
                    break;

                case '2':
                    R_DEMO_AppSysGetStatistics (R_SYS_INFO_BLOCK_UARTIF_0);
                    break;

                case '3':
                    R_DEMO_AppSysGetLog (R_SYS_INFO_BLOCK_UARTIF_0);
                    break;

                case 'z':
                    return R_RESULT_SUCCESS;

                default:
                    R_STDIO_Printf ("\n\n Invalid option! \n");
                    break;
            } /* switch */

        }
    }
} /* menu_sys_statistics */
/******************************************************************************
   End of function  menu_sys_statistics
******************************************************************************/




/******************************************************************************
* Function Name: R_DEMO_AppSysMenu
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AppSysMenu (void)
{
    /* Configuration Menu */
    while (1)
    {
        R_STDIO_Printf ("\f-------------------Configuration Menu-----------------------");
        R_STDIO_Printf ("\n 0 - ping");
        R_STDIO_Printf ("\n 1 - version");
        R_STDIO_Printf ("\n 7 - Statistics/Log");
        R_STDIO_Printf ("\n 8 - cpx reboot");
        R_STDIO_Printf ("\n z - Return");

        R_STDIO_Gets ((char *)g_demo_buff.getStringBuffer);

        if (strlen ((char *)g_demo_buff.getStringBuffer) == 1)
        {
            switch (g_demo_buff.getStringBuffer[0])
            {
                case '0':
                {
                    r_sys_ping_cnf_t * pCnf;
                    R_DEMO_SysPing (&pCnf);
                    break;
                }

                case '1':
                {
                    r_sys_version_cnf_t * pCnf;
                    R_DEMO_SysVersion (&pCnf);
                    break;
                }

                case '7':
                    menu_sys_statistics ();
                    break;

                case '8':
                {
                    R_DEMO_ModemReboot ();
                    break;
                }

                case 'z':
                    return R_RESULT_SUCCESS;

                default:
                    R_STDIO_Printf ("\n\n Invalid option! \n");
                    break;
            } /* switch */

        }
    }
} /* R_DEMO_AppSysMenu */
/******************************************************************************
   End of function  R_DEMO_AppSysMenu
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppHandleSysRebootReqInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppHandleSysRebootReqInd (const r_sys_rebootreq_ind_t * ind)
{
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Received SYS-ReBootRequest indication with status value: 0x%.2X", ind->status);
    }
    R_DEMO_LED (R_DEMO_G3_USE_PRIMARY_CH, R_DEMO_LED_ALERT);

    /* Call Reboot Routine. */

    /* TODO it should be soft reset but current keep for testing
     * R_DEMO_ModemReboot();
     */

}
/******************************************************************************
   End of function  R_DEMO_AppHandleSysRebootReqInd
******************************************************************************/

