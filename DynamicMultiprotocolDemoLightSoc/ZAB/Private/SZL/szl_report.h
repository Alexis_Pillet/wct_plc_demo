#ifndef _SZL_REPORT_H_
#define _SZL_REPORT_H_

#include "data_converter.h"

/* Value of Max Time used by Szl_ReportSendingConfigure() to disable report */
#define SZL_REPORT_M_REPORT_DISABLED_TIME 0xFFFF

typedef struct
{
    szl_uint8 Version;                  // This is the version for this structure - used for sanity check when reading from NV
    szl_uint16 ClusterID;               // The Cluster ID
    szl_uint16 AttributeID;             // the attribute ID
    SZL_EP_t Endpoint;              // The endpoint for the attribute
    szl_bool ManufacturerSpecific;       // is Manufacture specific
    SZL_ZIGBEE_DATA_TYPE_t DataType;// the data type - used for sanity check
    szl_uint16 Min;                     // The polling interval for the periodic reports
    szl_uint16 Max;                     // The report interval for periodic reports
    szl_bool ReportOnChange;            // true if reportable change is present
    NATIVE_DATA_t ReportableChange; // the reportable change value (related to the datatype of the attribute)
} SZL_REPORT_CFG_t;

typedef struct
{
    szl_uint32 Postponed;               // The report has been postponed to a given time
    szl_uint32 LastReportedTime;        // The time the report was last send - if not send yet it will be TIME_INVALID
    NATIVE_DATA_t LastReportedValue;// The value at the last report time
} SZL_REPORT_INFO_t;

typedef enum
{
    REPORT_EMPTY           = 0x00,
    REPORT_CONFIGURED      = 0x01,
    REPORT_SEND_NOW        = 0x08,
    
    REPORT_SEND_IN_PROGRESS = 0x10,
} REPORT_STATUS_t;

typedef struct
{
    szl_uint8 Status;                    // Status for the report
    /*
    struct
    {
        szl_uint8 Task;                   // NV tasks to be performed
        szl_uint16 Key;                   // The Key for the in the NV record
    } NV;*/
    struct
    {
        SZL_REPORT_INFO_t Info;   // Information for last report
        SZL_REPORT_CFG_t Config;  // The configuration for the report
    } Data;
} SZL_REPORT_ENTRY_t;


/**
 * Initialize
 *
 * This function has to be called before the report functionality will be available
 */
extern
void Szl_ReportInitialize(zabService* Service);

//void Szl_ReportNotifyAttributeChanged(zabService* Service, SZL_EP_t Endpoint, szl_bool ManufacturerSpecific, szl_uint16 ClusterID, szl_uint16 AttributeID);

/**
 * Sending Configure
 *
 * This function adds/updates or deletes a Report Configuration send to it via ZigBee.
 * Setting Max = 0xFFFF causes report to be deleted.
 */
extern
SZL_STATUS_t Szl_ReportSendingConfigure(zabService* Service, 
                                        SZL_EP_t Endpoint, 
                                        szl_bool ManufacturerSpecific, 
                                        szl_uint16 ClusterID,
                                        szl_uint16 AttributeID,
                                        szl_uint8 DataType,
                                        szl_uint16 Min,
                                        szl_uint16 Max,
                                        NATIVE_DATA_t* ReportableChangeValue);

/**
 * Read Reporting Sending Configuration
 *
 * This function reads a configuration for sending a report
 */
extern
SZL_STATUS_t Szl_ReadReportingSendingConfiguration(zabService* Service, SZL_EP_t Endpoint, szl_bool ManufacturerSpecific, szl_uint16 ClusterID, szl_uint16 AttributeID, szl_uint8* DataType, szl_uint16* Min, szl_uint16* Max, szl_uint8* RCVLength, void* ReportableChangeValue);


#endif /*_SZL_REPORT_H_*/
