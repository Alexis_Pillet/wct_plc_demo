# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to start the web server and serial management
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from bottle import route, run, hook, response, static_file, request
from core_engine import CoreEngine

core_engine = CoreEngine()
core_engine.start()


@hook('after_request')  # These lines are needed for avoiding the "Access-Control-Allow-Origin" errors
def enable_header():
    response.headers['Access-Control-Allow-Origin'] = '*'


# Main Page web Application
@route('/WirelessBridge')
def get_file():
    filename = "index.html"
    return static_file(filename, root="../", mimetype="text/html")


@route('/WirelessBridge', method='POST')
def send_cluster_command():
    id_wireless_bridge = int(request.forms.get('id_wireless_bridge'))
    address = request.forms.get('address')
    type_device = request.forms.get('type_device')
    name_function = request.forms.get('name_function')

    if not core_engine.zigbee_device_container.is_device_saved(id_wireless_bridge=id_wireless_bridge, address=address):
        return

    device = core_engine.zigbee_device_container.get_device(id_wireless_bridge=id_wireless_bridge, address=address)

    if type_device == 'iact':

        if name_function == 'discovery':
            device.discovery()
        elif name_function == 'open':
            device.open()
        elif name_function == 'close':
            device.close()

    elif type_device == 'co2_multisensor':

        if name_function == 'discovery':
            device.discovery()

    elif type_device == 'power_tag_1p':

        if name_function == 'discovery':
            device.discovery()
        elif name_function == 'reset_partial_energy':
            device.reset_partial_energy()

    elif type_device == 'power_tag_3p':

        if name_function == 'discovery':
            device.discovery()
        elif name_function == 'reset_partial_energy':
            device.reset_partial_energy()

    elif type_device == 'power_tag_3pn':

        if name_function == 'discovery':
            device.discovery()
        elif name_function == 'reset_partial_energy':
            device.reset_partial_energy()


@route('/<name>.json')
def index(name):
    filename = name + ".json"
    return static_file(filename, root="../", mimetype="text/json")


@route('/css/<name>.css')
def index(name):
    filename = name + ".css"
    return static_file(filename, root="../css/", mimetype="text/css")


@route('/js/jtree/themes/default/<name>.css')
def index(name):
    filename = name + ".css"
    return static_file(filename, root="../js/jtree/themes/default/", mimetype="text/css")


@route('/js/jtree/themes/default/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../js/jtree/themes/default/", mimetype="text/html")


@route('/js/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../js/", mimetype="application/javascript")


@route('/js/jtree/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../js/jtree/", mimetype="application/javascript")


@route('/images/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../images/", mimetype="image/png")


@route('/images/<name>.ico')
def index(name):
    filename = name + ".ico"
    return static_file(filename, root="../images/", mimetype="image/x-icon")


@route('/fonts/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../fonts/", mimetype="font/woff2")


run(host='localhost', port=8080, server='waitress')  # Start web server
# Function run is a blocking call, once the server is stopped => we stop all processes
core_engine.stop()
