/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the configuration of Mutexes for ZAB.
 *
 *   This file is to be copied and modified for a particular implementation of the
 *   ZigBee Application Brick.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Original.
 * 002.001.005  22-Jul-15   MvdB   Correct comments and default values
 *****************************************************************************/

#ifndef _ZAB_MUTEX_CONFIGURE_H_
#define _ZAB_MUTEX_CONFIGURE_H_

/******************************************************************************
 *                      ******************************
 *                 *****       MUTEX MANAGEMENT       *****
 *                      ******************************
 ******************************************************************************/
/*
 * The macros below must be defined for to protect data if there are more than one user thread.
 *
 * Mutexes and critical sections are created by ZAB_PROTECT_CREATE which asks the
 * user to create the item and return an identifier that is passed to the macros below.
 *
 * The user decides if none, one, or many mutexes/sections are needed.
 * The Service pointer is provided to allow applications to manage Mutexes per service.
 *
 * If a valid mutex is created a MutexId between 1 and 255 shall be returned.
 * If a valid mutex was not created a MutexId of 0 shall be returned.
 *
 * This means each item protected has an id from 1..N and the user can use it to identify a lock,
 * mutex or critical sections.
 *
 */

/* Bring in header file with prototypes for MutexCreate(), MutexRelease(), MutexEnter(), MutexExit() */
#include "mutex.h"

/******************************************************************************
 * Create a Mutex for a Service and return an unsigned8 MutexId to reference
 * it within the service.
 * Return 0 if mutexes not required.
 *
 * Examples:
 *  Mutexes Not Used: #define ZAB_PROTECT_CREATE( Service, MutexId )   (0)
 *  Mutexes Used:     #define ZAB_PROTECT_CREATE( Service, MutexId )   MutexCreate( Service )
 ******************************************************************************/
#define ZAB_PROTECT_CREATE( Service )             MutexCreate( Service )


/******************************************************************************
 * Release (Destroy) a Mutex from a Service.
 *
 * Examples:
 *  Mutexes Not Used: #define ZAB_PROTECT_RELEASE( Service, MutexId )
 *  Mutexes Used:     #define ZAB_PROTECT_RELEASE( Service, MutexId )   MutexRelease(Service, MutexId);
 ******************************************************************************/
#define ZAB_PROTECT_RELEASE( Service, MutexId )   MutexRelease( (Service), (MutexId) )


/******************************************************************************
 * Enter (Lock) a Mutex from a Service.
 *
 * Examples:
 *  Mutexes Not Used: #define ZAB_PROTECT_ENTER( Service, MutexId )
 *  Mutexes Used:     #define ZAB_PROTECT_ENTER( Service, MutexId )   MutexEnter(Service, MutexId);
 ******************************************************************************/
#define ZAB_PROTECT_ENTER( Service, MutexId )     MutexEnter(Service, MutexId)


/******************************************************************************
 * Exit (Unlock) a Mutex from a Service.
 *
 * Examples:
 *  Mutexes Not Used: #define ZAB_PROTECT_EXIT( Service, MutexId )
 *  Mutexes Used:     #define ZAB_PROTECT_EXIT( Service, MutexId )   MutexExit(Service, MutexId);
 ******************************************************************************/
#define ZAB_PROTECT_EXIT( Service, MutexId )      MutexExit(Service, MutexId)

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif