/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the Poll Control Cluster Client.
 *   This plugin is originally designed for MiGenie.
 * 
 *   It supports:
 *    - Operation on multiple endpoints
 *    - Mandatory commands only
 *
 * USAGE:
 *   Usage of this plugin is very simple:
 *    1. Initialise it with handler for Checkin and FastPollResposne callbacks
 *    2. Plugin will call Checkin handler when any SED checks in.
 *       App can return true to have SED remain in fast poll, or false to stop.
 *    3. If app requested fast polling it can then send pending commands to the
 *       SED and then a FastPollStop once all pending data is sent.
 * 
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         04-Jul-16   MvdB   Original
 *****************************************************************************/

#include "poll_control_client.h"
#include "szl_api.h"

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

#define ZCL_CLUSTER_ID_POLL_CONTROL     0x0020


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/* Params required for plugin operation */
typedef struct
{
  SzlPlugin_PollControlClient_CheckinNotification_CB_t CheckinCallback;
  SzlPlugin_PollControlClient_FastPollStopResp_CB_t FastPollStopRespCallback;
  szl_uint8 NumEndpoints;
  szl_uint8 Endpoints[VLA_INIT]; // Variable length array
}SzlPlugin_PollControlClient_Params_t;
#define SzlPlugin_PollControlClient_Params_t_SIZE(numEndpoints) (sizeof(SzlPlugin_PollControlClient_Params_t) - (VLA_INIT*sizeof(szl_uint8))+ (sizeof(szl_uint8) * (numEndpoints)))


/**
 * Poll Control cluster commands
 */
typedef enum
{
  COMMAND_POLL_CONTROL_SERVER_TO_CLIENT_CHECK_IN_REQ          = 0x00, // Mandatory, Rx Supported
} PollControl_Cmd_ServerToClient_t;
typedef enum
{
  COMMAND_POLL_CONTROL_CLIENT_TO_SERVER_CHECK_IN_RSP          = 0x00, // Mandatory, Tx Supported
  COMMAND_POLL_CONTROL_CLIENT_TO_SERVER_FAST_POLL_STOP        = 0x01, // Mandatory, Tx Supported
  COMMAND_POLL_CONTROL_CLIENT_TO_SERVER_SET_LONG_POLL_INT     = 0x02, // Optional, Tx Not Supported
  COMMAND_POLL_CONTROL_CLIENT_TO_SERVER_SET_SHORT_POLL_INT    = 0x03, // Optional, Tx Not Supported
} PollControl_Cmd_ClientToServer_t;

/* Prototype so we can reference the function in constClusterCommands[] */
static szl_bool SzlPlugin_PollControlClient_ClusterCommandHandler(zabService* Service, 
                                                                  szl_uint8 DestinationEndpoint, 
                                                                  szl_bool manufacturerSpecific, 
                                                                  szl_uint16 ClusterID, 
                                                                  szl_uint8 CmdID, 
                                                                  szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize, 
                                                                  szl_uint8* CmdOutID, 
                                                                  szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize, 
                                                                  szl_uint8 TransactionId,
                                                                  SZL_Addresses_t SourceAddress,
                                                                  SZL_ADDRESS_MODE_t DestAddressMode);

/* Cluster Commands */
static const SZL_ClusterCmd_t constClusterCommands[] ={
  {szl_false, SZL_ADDRESS_EP_ALL, ZCL_CLUSTER_ID_POLL_CONTROL, COMMAND_POLL_CONTROL_SERVER_TO_CLIENT_CHECK_IN_REQ, SZL_CLU_OP_TYPE_CUSTOM_SERVER_TO_CLIENT, {.Callback = SzlPlugin_PollControlClient_ClusterCommandHandler}},
};
#define CLUSTER_COMMANDS_COUNT    ( sizeof(constClusterCommands) / sizeof(SZL_ClusterCmd_t))



/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/* Cluster command handler */
static szl_bool SzlPlugin_PollControlClient_ClusterCommandHandler(zabService* Service, 
                                                                  szl_uint8 DestinationEndpoint, 
                                                                  szl_bool manufacturerSpecific, 
                                                                  szl_uint16 ClusterID, 
                                                                  szl_uint8 CmdID, 
                                                                  szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize, 
                                                                  szl_uint8* CmdOutID, 
                                                                  szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize, 
                                                                  szl_uint8 TransactionId,
                                                                  SZL_Addresses_t SourceAddress,
                                                                  SZL_ADDRESS_MODE_t DestAddressMode)
{
  szl_uint8 index;
  SzlPlugin_PollControlClient_Params_t * pluginParams = NULL;
  szl_bool startFastpolling;
  szl_uint16 fastPollTimeout;
  szl_bool cmdHandled = szl_false;
  
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_POLL_CONT_CLIENT, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return szl_false;
    }
  
  if (CmdID == COMMAND_POLL_CONTROL_SERVER_TO_CLIENT_CHECK_IN_REQ)
    {
      if ( (pluginParams->CheckinCallback != NULL) &&
           (SourceAddress.AddressMode == SZL_ADDRESS_MODE_NWK_ADDRESS) )
        {
          fastPollTimeout = 0;
          startFastpolling = pluginParams->CheckinCallback(Service, DestinationEndpoint, SourceAddress.Addresses.Nwk.Address, &fastPollTimeout);
          
          /* Build response */
          *CmdOutID = COMMAND_POLL_CONTROL_CLIENT_TO_SERVER_CHECK_IN_RSP;
          index = 0;
          ZCLPayloadOut[index++] = (szl_uint8)startFastpolling;
          COPY_OUT_16_BITS(&ZCLPayloadOut[index], fastPollTimeout); index+=2;
          *PayloadOutSize = index;
          
          cmdHandled = szl_true;
        }
    }
  
  return cmdHandled;
}

/* Fast Poll Stop Response Handler*/
void SzlPlugin_PollControlClient_FastPollStopRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ClusterCmdRespParams_t *Params, szl_uint8 TransactionId)
{
  SzlPlugin_PollControlClient_Params_t * pluginParams = NULL;
  
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_POLL_CONT_CLIENT, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return;
    }
  
  if ( (pluginParams->FastPollStopRespCallback != NULL) &&
       (Params->SourceAddress.AddressMode == SZL_ADDRESS_MODE_NWK_ADDRESS) )
    {
      pluginParams->FastPollStopRespCallback(Service, Status, Params->SourceAddress.Addresses.Nwk.Address, TransactionId);
    }
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/  

/**
 * Poll Control Cluster Client - Initialization
 *
 * This function must be called from the application after the Library has been initialized.
 * 
 * @param[in]  Service       (zabService*)  Service Instance Pointer
 * @param[in]  NumEndpoints  (szl_uint8)    Length of EndpointList
 * @param[in]  EndpointList  (szl_uint8*)   List of endpoints the cluster should be registered for
 * @param[in]  CheckinCallback  (SzlPlugin_PollControlClient_CheckinNotification_CB_t) the callback function for received Checking commands
 * @param[in]  FastPollStopRespCallback  (SzlPlugin_PollControlClient_FastPollStopResp_CB_t) the callback function for FastPollStop responses
 */
SZL_RESULT_t SzlPlugin_PollControlClient_Init(zabService* Service, 
                                              szl_uint8 NumEndpoints, 
                                              szl_uint8* EndpointList, 
                                              SzlPlugin_PollControlClient_CheckinNotification_CB_t CheckinCallback,
                                              SzlPlugin_PollControlClient_FastPollStopResp_CB_t FastPollStopRespCallback)
{
  SZL_RESULT_t result = SZL_RESULT_SUCCESS;
  szl_uint8 endpointIndex;
  szl_uint8 cmdIndex;
  SzlPlugin_PollControlClient_Params_t * pluginParams = NULL;
  SZL_ClusterCmd_t clusterCommands[CLUSTER_COMMANDS_COUNT];
  unsigned16 clientCluster = ZCL_CLUSTER_ID_POLL_CONTROL;

  /* Validate inputs */
  if ( (Service == NULL) || (CheckinCallback == NULL) || (FastPollStopRespCallback == NULL) || (EndpointList == NULL) || (NumEndpoints > SZL_CFG_ENDPOINT_CNT) || (NumEndpoints == 0) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
    
  /* Malloc then initialise parameters the plugin needs to store and link from the SZL */
  result = SZL_PluginRegister(Service, SZL_PLUGIN_ID_POLL_CONT_CLIENT, SzlPlugin_PollControlClient_Params_t_SIZE(NumEndpoints), (void**)&pluginParams);
  if (result == SZL_RESULT_SUCCESS)
    {
      if (pluginParams != NULL)
        {
          pluginParams->CheckinCallback = CheckinCallback;
          pluginParams->FastPollStopRespCallback = FastPollStopRespCallback;
          pluginParams->NumEndpoints = NumEndpoints;

          /* Register commands for each endpoint (setting correct endpoint in to the registration parameters */
          szl_memcpy(clusterCommands, constClusterCommands, sizeof(constClusterCommands));
          for (endpointIndex = 0; endpointIndex < NumEndpoints; endpointIndex++)
            {
              pluginParams->Endpoints[endpointIndex] = EndpointList[endpointIndex];

              /* Initialise endpoint for each cluster command */
              for (cmdIndex = 0; cmdIndex < CLUSTER_COMMANDS_COUNT; cmdIndex++)
                {
                  clusterCommands[cmdIndex].Endpoint = EndpointList[endpointIndex];
                }

              /* Register the clusters commands */
              result = SZL_ClusterCmdRegister(Service, clusterCommands, CLUSTER_COMMANDS_COUNT);
              if( (result == SZL_RESULT_SUCCESS) || (result == SZL_RESULT_ALREADY_EXISTS) )
                {
                  result = SZL_RESULT_SUCCESS;
                }
              else
                {
                  break;
                }

              /* Register the cluster as a client of the endpoint */
              result = SZL_ClientClusterRegister(Service, EndpointList[endpointIndex], &clientCluster, 1);
              if( (result == SZL_RESULT_SUCCESS) || (result == SZL_RESULT_ALREADY_EXISTS) )
                {
                  result = SZL_RESULT_SUCCESS;
                }
              else
                {
                  break;
                }
            }
        }
      else
        {
          result = SZL_RESULT_INTERNAL_LIB_ERROR;
        }
    }

  if (result != SZL_RESULT_SUCCESS)
    {
      /* Failed. Cleanup */
      printError(Service, "SzlPlugin_PollControlClient_Init: Failed 0x%02X\n", result);
      SZL_PluginUnregister(Service, SZL_PLUGIN_ID_POLL_CONT_CLIENT);
    }

  return result;
}


/**
 * Poll Control Cluster Client - Destroy
 * 
 * @param[in]  Service       (zabService*) Service Instance Pointer
 */
SZL_RESULT_t SzlPlugin_PollControlClient_Destroy(zabService* Service)
{
  SZL_RESULT_t result;
  szl_uint8 endpointIndex;
  szl_uint8 cmdIndex;
  szl_uint8 endpoint;
  SzlPlugin_PollControlClient_Params_t* pluginParams = NULL;
  SZL_ClusterCmd_t clusterCommands[CLUSTER_COMMANDS_COUNT];
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_POLL_CONT_CLIENT, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
  
  /* De-Register commands for each endpoint (setting correct endpoint in to the registration parameters */
  szl_memcpy(clusterCommands, constClusterCommands, sizeof(constClusterCommands));
  for (endpointIndex = 0; endpointIndex < pluginParams->NumEndpoints; endpointIndex++)
    {
      endpoint = pluginParams->Endpoints[endpointIndex];

      /* Initialise endpoint for each cluster command */
      for (cmdIndex = 0; cmdIndex < CLUSTER_COMMANDS_COUNT; cmdIndex++)
        {
          clusterCommands[cmdIndex].Endpoint = endpoint;
        }

      // De-Register the clusters commands
      result = SZL_ClusterCmdDeregister(Service, clusterCommands, CLUSTER_COMMANDS_COUNT);
      if(result != SZL_RESULT_SUCCESS)
        {
          /* Print error but continue to make best effort to clean everything up! */
          printError(Service, "SzlPlugin_PollControlClient_Destroy: WARNING: Deregister commands failed with Result 0x%02X\n",
                     result);
        }
    }  

  /* Now unregister the plugin */
  return SZL_PluginUnregister(Service, SZL_PLUGIN_ID_POLL_CONT_CLIENT);
}


/**
 * Poll Control Cluster Client - Fast Poll Stop command
 * 
 * @param[in]  Service       (zabService*) Service Instance Pointer
 * @param[in]  Params        (SzlPlugin_PollControlClient_FastPollStopReqParams_t*) pointer to the parameters
 * @param[out] TransactionId pointer to location where Transaction ID should be returned. Set to NULL if caller does not want the Transaction ID.
 */
SZL_RESULT_t SzlPlugin_PollControlClient_FastPollStopReq(zabService* Service,
                                                         SzlPlugin_PollControlClient_FastPollStopReqParams_t* Params,
                                                         szl_uint8* TransactionId)
{
  SZL_ClusterCmdReqParams_t cmdReq;
  cmdReq.ManufacturerSpecific = szl_false;
  cmdReq.SourceEndpoint = Params->SourceEndpoint;
  cmdReq.DestAddrMode = Params->DestAddrMode;
  cmdReq.ClusterID = ZCL_CLUSTER_ID_POLL_CONTROL;
  cmdReq.Direction = ZCL_FRAME_DIR_CLIENT_SERVER;
  cmdReq.Command = COMMAND_POLL_CONTROL_CLIENT_TO_SERVER_FAST_POLL_STOP;
  cmdReq.PayloadLength = 0;
  
  return SZL_ClusterCmdReq(Service, SzlPlugin_PollControlClient_FastPollStopRspHandler, &cmdReq, TransactionId);
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/