/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Binding Commands/Responses
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 002.002.039  17-Jan-17   MvdB   ARTF175875: Original
 *****************************************************************************/

#include "zabNcpService.h"
#include "zabNcpNwk.h"
#include "zabNcpEzsp.h"
#include "zabNcpEzspBinding.h"

#include "ezsp-enum.h"
#include "ezsp-enum-decode.h"
#include "ezsp-protocol.h"
#include "ember-types.h"


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* Size of serialised EmberBindingTableEntry */
#define EMBER_BINDING_TABLE_ENTRY_SERIALISED_SIZE (1 + 1 + 2 + 1 + 8 + 1)

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 * Clear Binding Request + Response Handler
 ******************************************************************************/
void zabNcpEzspBinding_ClearAll(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_CLEAR_BINDING_TABLE};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspBinding_ClearAllResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 index = 0;
  EmberStatus responseStatus;

  if (PayloadLength >= 1)
    {
      responseStatus = PayloadData[index++];

      printVendor(Service, "EZSP_CLEAR_BINDING_TABLE: Status: %s (0x%02X)\n",
                  decodeEmberStatus(responseStatus), responseStatus);

      /* Notify network state machine */
      zabNcpNwk_ClearBindingTableResponseHandler(Status, Service, responseStatus);
    }
}

/******************************************************************************
 * Get Binding Request + Response Handler
 ******************************************************************************/
void zabNcpEzspBinding_Get(erStatus* Status, zabService* Service, unsigned8 Index)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_GET_BINDING, Index};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspBinding_GetResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 index = 0;
  EmberStatus responseStatus;
  EmberBindingTableEntry bindEntry = {0};

  if (PayloadLength >= 1)
    {
      responseStatus = PayloadData[index++];

      if ( (responseStatus == EMBER_SUCCESS) && (PayloadLength >= (index + EMBER_BINDING_TABLE_ENTRY_SERIALISED_SIZE)) )
        {
          bindEntry.type = PayloadData[index++];
          bindEntry.local = PayloadData[index++];
          bindEntry.clusterId = COPY_IN_16_BITS(&PayloadData[index]); index += 2;
          bindEntry.remote = PayloadData[index++];
          osMemCopy(Status, bindEntry.identifier, &PayloadData[index], 8); index += 8;
          bindEntry.networkIndex = PayloadData[index++];
        }

      printVendor(Service, "EZSP_GET_BINDING: Status: %s (0x%02X), Type: %d, LocalEp = 0x%02X, Cluster = 0x%04X, RemoteEp = 0x%02X, Identifier = 0x%016llX, NwkIndex = 0x%02X\n",
                  decodeEmberStatus(responseStatus), responseStatus,
                  bindEntry.type,
                  bindEntry.local,
                  bindEntry.clusterId,
                  bindEntry.remote,
                  COPY_IN_64_BITS(bindEntry.identifier),
                  bindEntry.networkIndex);

      /* Notify network state machine so it can build the host binding table copy */
      zabNcpNwk_GetBindingResponseHandler(Status, Service, responseStatus, &bindEntry);
    }
}


/******************************************************************************
 * Remote Set Binding Table Handler
 ******************************************************************************/
void zabNcpEzspBinding_RemoteSetBindingHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 index = 0;
  EmberBindingTableEntry bindEntry = {0};
  unsigned8 bindingTableIndex;
  EmberStatus policyDecision;

  // SiLabs Case 128372: This command is one byte short in 5.8.0!
  if (PayloadLength >= (EMBER_BINDING_TABLE_ENTRY_SERIALISED_SIZE + 1)) //2)) TODO WARNING MVDB THIS SHOULD BE 2!!!
    {
      bindEntry.type = PayloadData[index++];
      bindEntry.local = PayloadData[index++];
      bindEntry.clusterId = COPY_IN_16_BITS(&PayloadData[index]); index += 2;
      bindEntry.remote = PayloadData[index++];
      osMemCopy(Status, bindEntry.identifier, &PayloadData[index], 8); index += 8;

      // SiLabs Case 128372: This byte is missing in 5.8.0!
      if (PayloadLength >= (EMBER_BINDING_TABLE_ENTRY_SERIALISED_SIZE + 2))
        {
          bindEntry.networkIndex = PayloadData[index++];
        }

      bindingTableIndex = PayloadData[index++];
      policyDecision = PayloadData[index++];

      printVendor(Service, "EZSP_REMOTE_SET_BINDING_HANDLER: PolicyDecision: %s (0x%02X), Index = %d, Type: %d, LocalEp = 0x%02X, Cluster = 0x%04X, RemoteEp = 0x%02X, Identifier = 0x%016llX, NwkIndex = 0x%02X\n",
                  decodeEmberStatus(policyDecision), policyDecision,
                  bindingTableIndex,
                  bindEntry.type,
                  bindEntry.local,
                  bindEntry.clusterId,
                  bindEntry.remote,
                  COPY_IN_64_BITS(bindEntry.identifier),
                  bindEntry.networkIndex);

      /* Notify network state machine so it can maintain the host binding table copy */
      zabNcpNwk_RemoteSetBindingHandler( Status, Service, policyDecision, bindingTableIndex, &bindEntry);
    }
}


/******************************************************************************
 * Remote Delete Binding Table Handler
 ******************************************************************************/
void zabNcpEzspBinding_RemoteDeleteBindingHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 index = 0;
  unsigned8 bindingTableIndex;
  EmberStatus policyDecision;

  if (PayloadLength >= 2)
    {
      bindingTableIndex = PayloadData[index++];
      policyDecision = PayloadData[index++];

      printVendor(Service, "EZSP_REMOTE_DELETE_BINDING_HANDLER: PolicyDecision: %s (0x%02X), Index = %d\n",
                  decodeEmberStatus(policyDecision), policyDecision,
                  bindingTableIndex);

      /* Notify network state machine so it can maintain the host binding table copy */
      zabNcpNwk_RemoteDeleteBindingHandler(Status, Service, policyDecision, bindingTableIndex);
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/