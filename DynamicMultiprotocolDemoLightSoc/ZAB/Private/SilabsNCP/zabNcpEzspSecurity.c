/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Security Commands/Responses
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.017  26-Jul-16   MvdB   Support GetKey
 * 000.000.019  26-Jul-16   MvdB   Replace memcpy with osMemCopy, memset with osMemSet
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/

#include "zabNcpService.h"
#include "zabNcpEzsp.h"
#include "zabNcpEzspSecurity.h"

#include "ezsp-enum.h"
#include "ezsp-enum-decode.h"
#include "ezsp-protocol.h"
#include "ember-types.h"

#include "zabNcpNwk.h"

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* Size of serialised EmberKeyStruct */
#define EMBER_KEY_STRUCT_SIZE ( 36 ) // EmberKeyStructBitmask(2), EmberKeyType(1), EmberKeyData(16), outgoingFrameCounter(4), incomingFrameCounter(4), sequenceNumber(1), partnerEUI64(8)

/* Size of serialised EmberInitialSecurityState */
#define EMBER_INITIAL_SECURITY_STATE_SIZE ( 2 + EMBER_ENCRYPTION_KEY_SIZE + EMBER_ENCRYPTION_KEY_SIZE + 1 + EUI64_SIZE) // bitmask(2), preconfiguredKey(EMBER_ENCRYPTION_KEY_SIZE), networkKey(EMBER_ENCRYPTION_KEY_SIZE), networkKeySequenceNumber(1), preconfiguredTrustCenterEui64(EUI64_SIZE)

/* Default Trust Center Link Key: ZigBeeAlliance09 */
static const unsigned8 defaultTclk[EMBER_ENCRYPTION_KEY_SIZE] = {0x5A, 0x69, 0x67, 0x42, 0x65, 0x65, 0x41, 0x6C, 0x6C, 0x69, 0x61, 0x6E, 0x63, 0x65, 0x30, 0x39};

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Set Initial Security State Request + Response Handler
 ******************************************************************************/
void zabNcpEzspSecurity_SetInitialState(erStatus* Status, zabService* Service)
{
  unsigned8 index;
  unsigned8 keyIndex;
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH + EMBER_INITIAL_SECURITY_STATE_SIZE] = {0/*seq*/, 0/*FrameControl*/, EZSP_SET_INITIAL_SECURITY_STATE};

  index = EZSP_PARAMETERS_INDEX;
  COPY_OUT_16_BITS(&cmd[index], 0x0100/*EMBER_HAVE_PRECONFIGURED_KEY*/ | 0x0200/*EMBER_HAVE_NETWORK_KEY*/ | 0x0004/*EMBER_TRUST_CENTER_GLOBAL_LINK_KEY*/); index+=2; // bitmask
  osMemCopy(Status, &cmd[index], (unsigned8*)defaultTclk, EMBER_ENCRYPTION_KEY_SIZE); index+=EMBER_ENCRYPTION_KEY_SIZE; // preconfiguredKey

  printVendor(Service, "zabNcpEzspSecurity_SetInitialState: NwkKey = ");
  for (keyIndex = 0; keyIndex < EMBER_ENCRYPTION_KEY_SIZE; keyIndex++)
    {
      cmd[index] = (unsigned8)ZAB_RAND_16();
      printVendor(Service, "%02X ", cmd[index]);
      index++;
    }
  printVendor(Service, "\n");

  cmd[index++] = 0; //networkKeySequenceNumber
  osMemSet(Status, &cmd[index], 0, EUI64_SIZE); index+=EUI64_SIZE; // preconfiguredTrustCenterEui64

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, index, cmd);
}
void zabNcpEzspSecurity_SetInitialStateResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_SET_INITIAL_SECURITY_STATE: Status: %s (0x%02X)\n",
                  decodeEmberStatus(PayloadData[0]),
                  PayloadData[0]);

      zabNcpNwk_SecurityInitResponseHandler(Status, Service, PayloadData[0]);
    }
}

/******************************************************************************
 * Get Current Security State Request + Response Handler
 ******************************************************************************/
void zabNcpEzspSecurity_GetCurrentSecurityState(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_GET_CURRENT_SECURITY_STATE};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspSecurity_GetCurrentSecurityStateResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberCurrentSecurityBitmask currentSecurityBitmask;
  if (PayloadLength >= 3)
    {
      currentSecurityBitmask = COPY_IN_16_BITS(&PayloadData[1]);
      printApp(Service, "EZSP_GET_CURRENT_SECURITY_STATE: Status: %s (0x%02X), SecBitMask: 0x%04X\n",
               decodeEmberStatus(PayloadData[0]),
               PayloadData[0],
               currentSecurityBitmask);
    }
}

/******************************************************************************
 * Get Key Request + Response Handler
 ******************************************************************************/
void zabNcpEzspSecurity_GetKey(erStatus* Status, zabService* Service, EmberKeyType KeyType)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_GET_KEY, KeyType};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspSecurity_GetKeyResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus rspStatus;
  EmberKeyStruct keyStruct;
  unsigned8 index = 0;

  if (PayloadLength >= (1 + EMBER_KEY_STRUCT_SIZE) ) // Status + keyStruct
    {
      rspStatus = PayloadData[index++];
      keyStruct.bitmask = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
      keyStruct.type = PayloadData[index++];
      osMemCopy(Status, keyStruct.key.contents, &PayloadData[index], EMBER_ENCRYPTION_KEY_SIZE); index+=EMBER_ENCRYPTION_KEY_SIZE;
      keyStruct.outgoingFrameCounter = COPY_IN_32_BITS(&PayloadData[index]); index+=4;
      keyStruct.incomingFrameCounter = COPY_IN_32_BITS(&PayloadData[index]); index+=4;
      keyStruct.sequenceNumber = PayloadData[index++];
      osMemCopy(Status, keyStruct.partnerEUI64, &PayloadData[index], EUI64_SIZE); index+=EUI64_SIZE;

      printApp(Service, "EZSP_GET_KEY: Status = %s (0x%02X), KSN = 0x%02X, Key =",
               decodeEmberStatus(rspStatus),
               rspStatus,
               keyStruct.sequenceNumber);
      for (index = 0; index < EMBER_ENCRYPTION_KEY_SIZE; index++)
        {
          printApp(Service, " %02X", keyStruct.key.contents[index]);
        }
      printApp(Service, "\n", keyStruct.key.contents[index]);
    }
}

/******************************************************************************
 * Add Transient Link Key Request + Response Handler
 ******************************************************************************/
void zabNcpEzspSecurity_AddTransientLinkKey(erStatus* Status, zabService* Service)
{
  unsigned8 index;
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH + EUI64_SIZE + EMBER_ENCRYPTION_KEY_SIZE] = {0/*seq*/, 0/*FrameControl*/, EZSP_ADD_TRANSIENT_LINK_KEY};

  index = EZSP_PARAMETERS_INDEX;
  osMemSet(Status, &cmd[index], 0xFF, EUI64_SIZE); index+=EUI64_SIZE;// Wild Card EUI64
  osMemCopy(Status, &cmd[index], (unsigned8*)defaultTclk, EMBER_ENCRYPTION_KEY_SIZE); index+=EMBER_ENCRYPTION_KEY_SIZE; // preconfiguredKey ZigBeeAlliance09

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, index, cmd);
}
void zabNcpEzspSecurity_AddTransientLinkKeyResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_ADD_TRANSIENT_LINK_KEY: Status: %s (0x%02X)\n",
                  decodeEmberStatus(PayloadData[0]),
                  PayloadData[0]);
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/