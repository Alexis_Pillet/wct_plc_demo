/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the message formatting for ZigBee Pro Application messages.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.00.04  25-Feb-14   MvdB   Cleanup
 * 00.00.03.01  19-Mar-14   MvdB   artf53864: Support mgmt leave request/response
 * 00.00.06.03  01-Oct-14   MvdB   artf104879: Support energy scans via Mgmt Nwk Update Request
 * 00.00.06.04  07-Oct-14   MvdB   artf104116: Add RSSI to received packet quality indication
 * 01.00.00.02  29-Jan-15   MvdB   Support GPD De-commissioning
 * 002.001.001  15-Jul-15   MvdB   ARTF130228: Add unique address mode for GpSrcId
 *                                 Move remaining contents of zabProFrame.h here and delete file
 * 002.001.003  16-Jul-15   MvdB   ARTF133822/132827: Include source and destination addressing on AfIncoming frames
 * 002.002.015  21-Oct-15   MvdB   ARTF104106: Support SZL_ZDO_MatchDescriptorReq()
 *****************************************************************************/

#ifndef __ZAB_PARSE_PRO_PRIVATE_H__
#define __ZAB_PARSE_PRO_PRIVATE_H__

#include "zabCoreConfigure.h"

#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/


/* Types of messages in ZAB */
typedef enum
{                                     /* Data Format */
  ZAB_MSG_APP_NULL = 0,               /* None */
  ZAB_MSG_APP_CMD_ERROR,              /* zabError */

  /* AF (ZCL Data) */
  ZAB_MSG_APP_AF_DATA_OUT,            /* zabMsgProDataReq */
  ZAB_MSG_APP_AF_DATA_IN,             /* zabMsgProDataInd */

  /* ZDO requests/responses/indications */
  ZAB_MSG_APP_ZDO_IEEE_REQ,           /* SZL_ZdoIeeeAddrReqParams_t */
  ZAB_MSG_APP_ZDO_IEEE_RSP,           /* SZL_ZdoAddrRespParams_t */
  ZAB_MSG_APP_ZDO_NWK_REQ,            /* SZL_ZdoNwkAddrReqParams_t */
  ZAB_MSG_APP_ZDO_NWK_RSP,            /* SZL_ZdoAddrRespParams_t */
  ZAB_MSG_APP_ZDO_ACTIVE_EP_REQ,      /* SZL_ZdoActiveEndpointReqParams_t */
  ZAB_MSG_APP_ZDO_ACTIVE_EP_RSP,      /* SZL_ZdoActiveEndpointRespParams_t */
  ZAB_MSG_APP_ZDO_SIMPLE_DESC_REQ,    /* SZL_ZdoSimpleDescriptorReqParams_t */
  ZAB_MSG_APP_ZDO_SIMPLE_DESC_RSP,    /* SZL_ZdoSimpleDescriptorRespParams_t */
  ZAB_MSG_APP_ZDO_MATCH_DESC_REQ,     /* SZL_ZdoMatchDescriptorReqParams_t */
  ZAB_MSG_APP_ZDO_MATCH_DESC_RSP,     /* SZL_ZdoMatchDescriptorRespParams_t */
  ZAB_MSG_APP_ZDO_POWER_DESC_REQ,     /* SZL_ZdoPowerDescriptorReqParams_t */
  ZAB_MSG_APP_ZDO_POWER_DESC_RSP,     /* SZL_ZdoPowerDescriptorRespParams_t */
  ZAB_MSG_APP_ZDO_NODE_DESC_REQ,      /* SZL_ZdoNodeDescriptorReqParams_t */
  ZAB_MSG_APP_ZDO_NODE_DESC_RSP,      /* SZL_ZdoNodeDescriptorRespParams_t */
  ZAB_MSG_APP_ZDO_BIND_REQ,           /* SZL_ZdoBindReqParams_t */
  ZAB_MSG_APP_ZDO_BIND_RSP,           /* SZL_ZdoBindRespParams_t */
  ZAB_MSG_APP_ZDO_UNBIND_REQ,         /* SZL_ZdoBindReqParams_t */
  ZAB_MSG_APP_ZDO_UNBIND_RSP,         /* SZL_ZdoBindRespParams_t */
  ZAB_MSG_APP_ZDO_MGMT_LQI_REQ,       /* SZL_ZdoMgmtLqiReqParams_t */
  ZAB_MSG_APP_ZDO_MGMT_LQI_RSP,       /* SZL_ZdoMgmtLqiRespParams_t */
  ZAB_MSG_APP_ZDO_MGMT_RTG_REQ,       /* SZL_ZdoMgmtRtgReqParams_t */
  ZAB_MSG_APP_ZDO_MGMT_RTG_RSP,       /* SZL_ZdoMgmtRtgRespParams_t */
  ZAB_MSG_APP_ZDO_MGMT_BIND_REQ,      /* SZL_ZdoMgmtBindReqParams_t */
  ZAB_MSG_APP_ZDO_MGMT_BIND_RSP,      /* SZL_ZdoMgmtBindRespParams_t */
  ZAB_MSG_APP_ZDO_MGMT_LEAVE_REQ,     /* SZL_NwkLeaveReqParams_t */
  ZAB_MSG_APP_ZDO_MGMT_LEAVE_RSP,     /* zabMsgProMgmtLeaveRsp */
  ZAB_MSG_APP_ZDO_MGMT_NWK_UPDATE_REQ,/* zabMsgProMgmtNwkUpdateReq */
  ZAB_MSG_APP_ZDO_MGMT_NWK_UPDATE_RSP,/* SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t */
  ZAB_MSG_APP_ZDO_USER_DESC_REQ,      /* SZL_ZdoUserDescriptorReqParams_t */
  ZAB_MSG_APP_ZDO_USER_DESC_RSP,      /* SZL_ZdoUserDescriptorRespParams_t */
  ZAB_MSG_APP_ZDO_USER_DESC_SET_REQ,  /* SZL_ZdoUserDescriptorSetReqParams_t */
  ZAB_MSG_APP_ZDO_USER_DESC_SET_RSP,  /* SZL_ZdoUserDescriptorSetRespParams_t */
  ZAB_MSG_APP_ZDO_DEVICE_ANNOUNCE,    /* SZL_ZdoDeviceAnnounceIndParams_t */
  ZAB_MSG_APP_ZDO_NWK_LEAVE_IND,      /* zabMsgPro_NwkLeaveInd */


  ZAB_MSG_APP_GP_COM_IND,             /* SZL_GpComIndParams_t */
  ZAB_MSG_APP_GP_COM_REPLY_REQ,       /* SZL_GpComReplyReqParams_t */
  ZAB_MSG_APP_GP_COM_NTF_IND,         /* SZL_GP_GpdCommissionedNtfParams_t */
  ZAB_MSG_APP_GP_TX_QUEUE_EXPIRY,     /* unsigned64 GpdId */


  /* Configuration */
  ZAB_MSG_APP_REGISTER_REQ,           /* zabMsgProSimpleDescriptor */

  /* Serialised Data */
  ZAB_MSG_APP_ZNP,                    /* ZNP formatted serial messages */
  ZAB_MSG_APP_EZSP_ASH,               /* EZSP ASH formatted messages*/
  ZAB_MSG_APP_EZSP_ASH_CRC,           /* EZSP ASH formatted messages with CRC*/
  ZAB_MSG_APP_EZSP_XMODEM,            /* XMODEM message - used by NCP bootloader */
  ZAB_MSG_APP_RAW,                    /* Raw serial data as output across the serial sap */

  ZAB_MSG_APP_COUNT,
} zabMsgAppType;


/* Define addressing mode used */
typedef enum
{
  ZAB_ADDRESS_MODE_VIA_BIND = 0x00,
  ZAB_ADDRESS_MODE_GROUP = 0x01,
  ZAB_ADDRESS_MODE_NWK_ADDRESS = 0x02,
  ZAB_ADDRESS_MODE_IEEE_ADDRESS = 0x03,
  ZAB_ADDRESS_MODE_GP_SOURCE_ID = 0x04,
  ZAB_ADDRESS_MODE_BROADCAST = 0x0F,
  ZAB_ADDRESS_MODE_UNDEFINED,
} zabAddressMode;

/* Broadcast addresses */
#define ZAB_BROADCAST_ALL                       0xFFFF
#define ZAB_BROADCAST_RX_ON_WHEN_IDLE           0xFFFD
#define ZAB_BROADCAST_ROUTERS_AND_COORDINATOR   0xFFFC
#define ZAB_BROADCAST_LOW_POWER_ROUTERS         0xFFFB

/* Endpoint IDs */
#define ZAB_BROADCAST_ENDPOINT                  0xFF
#define ZAB_GP_ENDPOINT                         0xF2

/*
 * AF Address - Used for all data requests
 *   addressMode:
 *     ZAB_AF_ADDRESS_MODE_VIA_BIND: Using binding table. All other fields ignored.
 *     ZAB_AF_ADDRESS_MODE_GROUP: shortAddress = Group ID. All other fields ignored.
 *     ZAB_AF_ADDRESS_MODE_NWK_ADDRESS: shortAddress = Network Address of node,  destEndpoint = Destination Endpoint in node. All other fields ignored.
 *     ZAB_AF_ADDRESS_MODE_IEEEE_ADDRESS: ieeeAddress = IEEE Address of node,  destEndpoint = Destination Endpoint in node. All other fields ignored.
 *     ZAB_AF_ADDRESS_MODE_BROADCAST: shortAddress = ZAB_BROADCAST_ALL|ZAB_BROADCAST_RX_ON_WHEN_IDLE|ZAB_BROADCAST_ROUTERS_AND_COORDINATOR|ZAB_BROADCAST_LOW_POWER_ROUTERS
 *                                    destEndpoint = ZAB_BROADCAST_ENDPOINT or Specific endpoint
 *                                    All other fields ignored
 */
typedef struct
{
  zabAddressMode addressMode;
  union {
    unsigned16 shortAddress;
    unsigned32 sourceId;
    unsigned64 ieeeAddress;
  } address;
  unsigned8 endpoint;
} zabProAddress;

/* Data Request - ZAB INTERNAL */
typedef struct
{
  unsigned8 version;        /* Version of this struct, to handle changes in future. Set to 0 */
  zabProAddress dstAddress; /* Destination of frame */
  unsigned16 profile;       /* Profile of frame, so we can also do ZDO messages.
                             * WARNING: Used for NCP only, not ZNP! */
  unsigned16 cluster;       /* Cluster of frame */
  unsigned8 srcEndpoint;    /* Source endpoint the request will be transmitted from */
  unsigned8 txOptions;      /* 0x10 = APS Ack Requested, 0x20 = Force Route Discovery, 0x40 = Enable APS Security */
  unsigned8 radius;         /* The distance, in hops, that a transmitted frame will be allowed to travel through the network. */
  unsigned16 dataLength;    /* Length of data to follow */
  unsigned8 data[VLA_INIT]; /* Data (ZCL), Length defined by dataLength */
}zabMsgProDataReq;

/* Data Indication - ZAB INTERNAL */
typedef struct
{
  unsigned8 version;            /* Version of this struct, to handle changes in future. Set to 0 */
  zabProAddress srcAddr;        /* Address of node that sent the command */
  zabProAddress dstAddr;        /* Addressing of how the command was received on the node */
  unsigned16 cluster;           /* Cluster of frame */
  unsigned16 macSourceAddress;  /* MAC Source Address of the frame (gives real originator for RSSI/LQI) */
  unsigned8 lqi;                /* Link quality measured during reception */
  signed8 rssi;                 /* RSSI measured during reception */
  unsigned8 securityStatus;     /* Info about security used for the command */
  unsigned32 timeStamp;         /* Reception time - units?? */
  unsigned16 dataLength;        /* Length of data to follow */
  unsigned8 data[VLA_INIT];     /* Data (ZCL), Length defined by dataLength */
}zabMsgProDataInd;
#define zabMsgProDataInd_SIZE() (sizeof(zabMsgProDataInd) - (VLA_INIT*sizeof(unsigned8)))

typedef union {
    zabMsgProDataInd dataInd;
    zabMsgProDataReq dataReq;
} sapMsgAps;
#define sapMsgAps_SIZE() (sizeof(sapMsgAps) - (VLA_INIT*sizeof(unsigned8)))


/* The simple descriptor of an endpoint - Used to register an endpoint with the vendor */
typedef struct
{
  unsigned8 endpoint;
  unsigned16 profileId;
  unsigned16 deviceId;
  unsigned8 deviceVersion;
  unsigned8 numInClusters;
  unsigned8 numOutClusters;
  /* The first numInClusters are the in clusters, the next numOutClusters are the out clusters*/
  unsigned16 clusterList[VLA_INIT]; // Variable length
} zabMsgProSimpleDescriptor;
#define zabMsgProSimpleDescriptor_SIZE(_num_clusters) (sizeof(zabMsgProSimpleDescriptor) - (VLA_INIT*sizeof(unsigned16)) + (sizeof(unsigned16) * (_num_clusters)))

/* MGMT Leave Rsp */
typedef struct
{
  unsigned16 srcNwkAddress;
  unsigned8 status;
} zabMsgProMgmtLeaveRsp;
#define zabMsgProMgmtLeaveRsp_SIZE (sizeof(zabMsgProMgmtLeaveRsp))



/* Network Leave Indication */
typedef struct
{
  unsigned16 srcNwkAddress;
  unsigned64 srcIeeeAddress;
  zab_bool request;
  zab_bool removeChildren;
  zab_bool rejoin;
} zabMsgPro_NwkLeaveInd;
#define zabMsgPro_NwkLeaveInd_SIZE (sizeof(zabMsgPro_NwkLeaveInd))

/* MGMT Network Update Request */
typedef struct
{
  zabAddressMode DestinationAddressMode;
  unsigned16 DestinationAddress;
  unsigned32 ChannelMask;
  unsigned8 ScanDuration;
  unsigned8 ScanCount;
  unsigned16 NetworkManagerAddress;
} zabMsgProMgmtNwkUpdateReq;
#define zabMsgProMgmtNwkUpdateReq_SIZE (sizeof(zabMsgProMgmtNwkUpdateReq))


#ifdef __cplusplus
}
#endif

#endif // __ZAB_PARSE_PRO_PRIVATE_H__


