
#ifndef _SZL_H_
#define _SZL_H_
#include "szl_config.h"
#include "szl_external.h"
#include "szl_types.h"
#include "szl_api.h"
#include "szl_extended_api.h"
#include "zcl_def.h"










/**
 * Type for Default Response - ADDED BY MVDB
 */
typedef struct
{
    SZL_EP_t SourceEndpoint;              /**< The Endpoint for the source (App) */
    SZL_EP_t DestinationEndpoint;         /**< The endpoint for the destination (remote device). */
    szl_uint16 DestinationNwkAddress;     /**< The network address for the destination (remote device). */
    szl_bool ManufacturerSpecific;        /**< This is set to szl_true if the command is manufacture specific, else set to szl_false */
    szl_uint16 ManufacturerCode;          /**< Manufacturer Code. Used if ManufacturerSpecific = szl_true. Required for responses to unknown manufacturer codes */
    szl_uint16 ClusterID;                 /**< The Cluster ID that has been bound */
    szl_uint8 TransactionId;              /**< Transaction ID */
    szl_uint8 CommandId;                  /**< Command  ID */
    SZL_STATUS_t Status;                  /**< Status to be returned in the default response  */
    ZCL_FRAME_DIR_t Direction;            /**< Direction for default response */
} SZL_DefaultRspReqParams_t;
#define SZL_DefaultRspReqParams_t_SIZE() (sizeof(SZL_DefaultRspReqParams_t))


/******************************************************************************
 * Default Response Request - ADDED BY MVDB
 ******************************************************************************/
SZL_RESULT_t SZL_DefaultResponseReq(zabService* Service,
                                    SZL_DefaultRspReqParams_t* Params);

#endif /* _SZL_H_ */


