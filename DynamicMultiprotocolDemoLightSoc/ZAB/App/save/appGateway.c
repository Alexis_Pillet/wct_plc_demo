/******************************************************************************
 *                          ZAB TEST APPLICATION
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains an example of ZAB being used in a typical gateway application.
 *   It will:
 *    - Open ZAB
 *    - Update the network processors firmware if it has a newer image available
 *    - Check GATEWAY_FILENAME for previous network info:
 *      - if file found and details match network settings in the dongle, then resume the network.
 *      - else form a new network.
 *
 * NOTES:
 * Config file contents are:
 *   IEEE=0x124B000433BA41    - Must match to ensure we have the correct dongle
 *   EPID=0x7CDE02AB04015E10  - Must match to ensure we have the correct network
 *   NWKADDR=0x0000           - Must match to ensure we have the correct device in the network.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 002.002.005  11-Sep-15   MvdB   Original
 * 002.002.012  14-Oct-15   MvdB   ARTF104107: Support End Device operation
 * 002.002.027  02-Aug-16   MvdB   Relax network config check to not include network address for SiLabs compatibility
 * 002.002.028  12-Dec-16   MvdB   ConsoleTest: Enable restart (network initialise) of appGateway on ZNP reset
 * 002.002.029  06-Jan-17   MvdB   ConsoleTest: Do not re-init SZL on restarts of appGateway
 * 002.002.047  31-Jan-17   MvdB   Add poll control client and device manager to gateway
 *****************************************************************************/

#include "appConfig.h"
//#include "appConsoleMain.h"
#include "appZabCoreGlue.h"
#include "zabCoreService.h"


#include "appZcl.h"
#include "appZclBasic.h"
#include "appZdo.h"
#include "appGp.h"
//#include "platUtility.h"
#include "appGateway.h"
#include "appZclPollControlClient.h"
#include "appDevMan.h"


#include "basic_cluster.h"
#include "identify_cluster.h"
#include "discovery.h"
#include "time_cluster.h"
#include "poll_control_client.h"


#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>


/******************************************************************************
 *                      *****************************
 *                 *****        CONFIGURATION        *****
 *                      *****************************
 ******************************************************************************/

/* Endpoint that will be used by the application */
#define APP_ENDPOINT 0x01

/* Max space allocated for a firmware filename */
#define FIRMWARE_FILENAME_MAX_LENGTH 100

/* Name of config file where network info will be stored */
#define GATEWAY_FILENAME "gatewayNetwork.cfg"


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/* States of the Gateway */
typedef enum
{
  gateway_closed,
  gateway_opening,
  gateway_getting_nwk_info,
  gateway_updating_firmware,
  gateway_nwk_resuming,
  gateway_nwk_form_init,
  gateway_nwk_forming,
  gateway_nwk_done,
} gateway_state;

/******************************************************************************
 *                      ******************************
 *                 *****       LOCAL VARIABLES        *****
 *                      ******************************
 ******************************************************************************/

/* State of gateway state machine */
static gateway_state gatewayState = gateway_closed;

/* Track is SZL is already initialised (hence no need ot re-init it on gateway restart */
static szl_bool szlInitialised = szl_false;

/* Stored copies of notified ZAB states */
static zabOpenState gatewayOpenState = ZAB_OPEN_STATE_CLOSED;
static zabNwkState gatewayNwkState = ZAB_NWK_STATE_NONE;

/* Stored copies of given ZAB data */
static unsigned64 networkProcessorIeee;
static unsigned64 networkEpid;
static unsigned16 networkAddress;
static zabNetworkProcessorModel nwkProcHardwareType;
static unsigned8 nwkProcAppVersion[4];


/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Commit network info to configuration file
 ******************************************************************************/
void commitNetworkInfoToFile(unsigned64 ieee, unsigned64 epid, unsigned16 nwkAddress)
{
  FILE * pFile;

  pFile = fopen (GATEWAY_FILENAME, "w");

  if (pFile != NULL)
  {
    fprintf(pFile, "IEEE=0x%llX\nEPID=0x%llX\nNWKADDR=0x%04X\n",
            (long long unsigned int)ieee,
            (long long unsigned int)epid,
            (unsigned int)nwkAddress);

    fclose (pFile);
  }
}

/******************************************************************************
 * Get network info from configuration file
 ******************************************************************************/
void getNetworkInfoFromFile(unsigned64* ieee, unsigned64* epid, unsigned16* nwkAddress)
{
  FILE* pFile;
  long lSize;
  char * buffer;
  size_t result;
  char * pch;

  *ieee = 0xFFFFFFFFFFFFFFFF;
  *epid = 0xFFFFFFFFFFFFFFFF;
  *nwkAddress = 0xFFFF;

  pFile=fopen(GATEWAY_FILENAME, "r");
  if (pFile != NULL)
    {
      // obtain file size:
      fseek (pFile , 0 , SEEK_END);
      lSize = ftell (pFile);
      rewind (pFile);

      // allocate memory to contain the whole file:
      buffer = (char*) malloc (sizeof(char)*(lSize+1));
      if (buffer != NULL)
        {
          result = fread (buffer,1,lSize,pFile);
          if (result == lSize)
            {
              buffer[lSize] = 0;
              pch = strtok (buffer,",=\n\r");
              while (pch != NULL)
                {
                  if (strncmp (pch,"IEEE",4) == 0)
                    {
                      pch = strtok (NULL, ",=\n\r");
                      if (pch != NULL)
                        {
                          *ieee = strtoull(pch,0,16);
                        }
                    }
                  else if (strncmp (pch,"EPID",4) == 0)
                    {
                      pch = strtok (NULL, ",=\n\r");
                      if (pch != NULL)
                        {
                          *epid = strtoull(pch,0,16);
                        }
                    }
                  else if (strncmp (pch,"NWKADDR",7) == 0)
                    {
                      pch = strtok (NULL, ",=\n\r");
                      if (pch != NULL)
                        {
                          *nwkAddress = strtoull(pch,0,16);
                        }
                    }
                  pch = strtok (NULL, ",=\n\r");
                }

            }
          else
            {
              printApp(appService, "AppGateway: Failed to read %s with result %d\n", GATEWAY_FILENAME, result);
            }
          free (buffer);
        }
      else
        {
          printApp(appService, "AppGateway: Failed to malloc space (%d bytes) to read %s\n", lSize, GATEWAY_FILENAME);
        }
      fclose (pFile);
    }
  else
    {
      printApp(appService, "AppGateway: Failed to open %s\n", GATEWAY_FILENAME);
    }

  printApp(appService, "AppGateway: Restored Network Parameters:\n\t\tIEEE = 0x%llX\n\t\tEPID = 0x%llX\n\t\tNwkAddr = 0x%04X\n",
                       *ieee,
                       *epid,
                       *nwkAddress);
}

/******************************************************************************
 * Check for error and cleanup if one is found
 ******************************************************************************/
void checkStatusAndKillIfError(erStatus* Status)
{
  erStatus localStatus;
  erStatusClear(&localStatus, appService);

  if (erStatusIsError(Status))
    {
      printApp(appService, "AppGateway: Error 0x%04 detected (%s)\n",
               erStatusGetError(Status),
               erStatusUtility_GetErrorString((zabError)erStatusGetError(Status)));

      gatewayState = gateway_closed;
      zabCoreAction(&localStatus, appService, ZAB_ACTION_CLOSE);
    }
}

/******************************************************************************
 * Init the SZL for the App
 ******************************************************************************/
void initialiseSzlApplication(void)
{
  SZL_Initialize_t init;
  SZL_RESULT_t res;

  /* Init the SZL is it has not been previously initialised */
  if (szlInitialised == szl_false)
    {
      printApp(appService, "AppGateway: Initialising SZL for endpoint 0x%02X...\n", APP_ENDPOINT);

      /* Init SZL */
      init.Version = SZLAPIVersion;
      init.NumberOfEndpoints = 1;
      init.Endpoints[0] = APP_ENDPOINT;
      res = SZL_Initialize(appService, &init);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_Initialize Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
          printApp(appService, "AppGateway: Critical Failure. Aborted.\n");
          return;
        }
      szlInitialised = szl_true;

      /* Register useful handlers */
      res = SZL_AttributeChangedNtfRegisterCB(appService, appZcl_AttributeChangedNotificationHandler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_AttributeChangedNtfRegisterCB Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SZL_AttributeReportNtfRegisterCB(appService, appZcl_ReportAttrHandler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_AttributeReportNtfRegisterCB Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SZL_ReceivedSignalStrengthNtfRegisterCB(appService, appZcl_ReceivedSignalStrengthHandler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_ReceivedSignalStrengthNtfRegisterCB Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SZL_NwkLeaveNtfRegisterCB(appService, appZdo_NetworkLeaveIndicationHandler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_NwkLeaveNtfRegisterCB Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

    #ifdef APP_CONFIG_ENABLE_GREEN_POWER
      res = SZL_GP_GpdCommissioningNtfRegisterCB(appService, appGp_GpdCommissioningNtf_Handler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_GP_GpdCommissioningNtfRegisterCB Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SZL_GP_GpdCommissionedNtfRegisterCB(appService, appGp_GpdCommissionedNtf_Handler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_GP_GpdCommissionedNtfRegisterCB Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }
    #endif

      /* Init Plugins */
      res = appZcl_SzlBasicPlugin_InitEndpoints(init.NumberOfEndpoints, init.Endpoints);
      if (res == SZL_RESULT_SUCCESS)
        {
          res = SzlPlugin_BasicClusterInit(appService, init.NumberOfEndpoints, init.Endpoints,
                                            appZcl_SzlBasicPlugin_ReadAttrHandler,
                                            appZcl_SzlBasicPlugin_WriteAttrHandler);
        }
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SzlPlugin_BasicClusterInit Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SzlPlugin_IdentifyClusterInit(appService, init.NumberOfEndpoints, init.Endpoints,
                                          appZcl_SzlIdentifyPluginHandler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SzlPlugin_IdentifyClusterInit Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SzlPlugin_TimeCluster_Init(appService, init.Endpoints[0], appZcl_SzlTimePluginHandler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SzlPlugin_TimeCluster_Init Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SzlPlugin_Discovery_Init(appService, init.Endpoints[0], appZdo_Discovery_NodeDetected_Handler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SzlPlugin_Discovery_Init Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = SzlPlugin_PollControlClient_Init(appService, 1, &init.Endpoints[0],
                                             appZclPollControlClient_CheckinNotificationHandler,
                                             appZclPollControlClient_FastPollStopRespHandler);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SzlPlugin_PollControlClient_Init Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }

      res = appDevMan_Init(appService, 20, 600);
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: appDevMan_Init Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }


      /* Init Client Clusters */
      const szl_uint16 clientClusterList[] = {0x0000, 0x0003, 0x0006, 0x0702, 0x0B04, 0xFF14};
      res = SZL_ClientClusterRegister(appService, init.Endpoints[0], clientClusterList, sizeof(clientClusterList)/sizeof(szl_uint16));
      if (res != SZL_RESULT_SUCCESS)
        {
          printApp(appService, "AppGateway: SZL_ClientClusterRegister Failed: Result = 0x%02X (%s)\n", res, appZcl_GetSzlResultString(res));
        }
    }

  printApp(appService, "AppGateway: Process complete!!!\n");
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Start the Gateway process
 ******************************************************************************/
void appGateway_Start(void)
{
  erStatus localStatus;
  erStatusClear(&localStatus, appService);

  if ( (gatewayState == gateway_closed) &&
       (gatewayOpenState == ZAB_OPEN_STATE_CLOSED) )
    {
      printApp(appService, "AppGateway: Opening...\n");
      gatewayState = gateway_opening;
      zabCoreAction(&localStatus, appService, ZAB_ACTION_OPEN);
      checkStatusAndKillIfError(&localStatus);
    }
  else
    {
      printApp(appService, "AppGateway: Invalid state\n");
    }
}

/******************************************************************************
 * Handle Open State Notifications
 ******************************************************************************/
void appGateway_OpenStateNtfHandler(zabOpenState openState)
{
  char fwFilename[APP_CONFIG_MAX_FIRMWARE_FILENAME_LENGTH];
  erStatus localStatus;
  erStatusClear(&localStatus, appService);

  gatewayOpenState = openState;

  if ( (gatewayState == gateway_opening) ||
       /* Also handle reset when gateway is running by re-starting network */
       ( (gatewayOpenState == ZAB_OPEN_STATE_OPENED) &&
         (gatewayNwkState == ZAB_NWK_STATE_UNINITIALISED) &&
         (gatewayState == gateway_nwk_done) ) )
    {
      switch(openState)
        {
          case ZAB_OPEN_STATE_OPENING:
          case ZAB_OPEN_STATE_GOING_TO_FW_UPDATE:
            /* Do nothing */
            break;

          case ZAB_OPEN_STATE_OPENED:
            /* Successfully opened.
             * If FW is up to date then get network info.
             * If not, go to FW updater and update it */
            if (platUtility_IsAppImageUpToDate(nwkProcHardwareType, nwkProcAppVersion) == zab_true)
              {
                printApp(appService, "AppGateway: Getting network info...\n");
                gatewayState = gateway_getting_nwk_info;
                zabCoreAction(&localStatus, appService, ZAB_ACTION_NWK_GET_INFO);
                checkStatusAndKillIfError(&localStatus);
              }
            else
              {
                zabCoreAction(&localStatus, appService, ZAB_ACTION_GO_TO_FIRMWARE_UPDATER);
                checkStatusAndKillIfError(&localStatus);
              }
            break;

          case ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE:
            /* Need to update firmware */
            printApp(appService, "AppGateway: Firmware update required\n");

            platUtility_GetLatestBinFileName(nwkProcHardwareType, fwFilename, APP_CONFIG_MAX_FIRMWARE_FILENAME_LENGTH);

            if ( (fwFilename[0] != 0) &&
                 (appZabCoreGlue_OpenFirmwareImage(appService, fwFilename) == zab_true) )
              {
                gatewayState = gateway_updating_firmware;
                zabCoreAction(&localStatus, appService, ZAB_ACTION_UPDATE_FIRMWARE);
                checkStatusAndKillIfError(&localStatus);
              }
            else
              {
                printApp(appService, "AppGateway: Firmware update failed with file: %s\n", fwFilename);
                gatewayState = gateway_closed;
                zabCoreAction(&localStatus, appService, ZAB_ACTION_CLOSE);
              }
            break;

          case ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE:
            /* Firmware Update Required */
            printApp(appService, "AppGateway: Firmware incompatible. Going to firmware updater.\n");
            zabCoreAction(&localStatus, appService, ZAB_ACTION_GO_TO_FIRMWARE_UPDATER);
            checkStatusAndKillIfError(&localStatus);
            break;

          default:
            /* Error failed */
            gatewayState = gateway_closed;
            printApp(appService, "AppGateway: Failed to open.\n");
            break;
        }
      return;
    }

  /* If there is a close while the gateway is active then reset states. */
  if (openState == ZAB_OPEN_STATE_CLOSED)
    {
      if (gatewayState != gateway_closed)
        {
          gatewayState = gateway_closed;
          printApp(appService, "AppGateway: Closed\n");
        }
    }
}

/******************************************************************************
 * Handle Network State Notifications
 ******************************************************************************/
void appGateway_NwkStateNtfHandler(zabNwkState nwkState)
{
  erStatus localStatus;
  erStatusClear(&localStatus, appService);

  gatewayNwkState = nwkState;

  if (gatewayState == gateway_nwk_resuming)
    {
      switch(nwkState)
        {
          case ZAB_NWK_STATE_NETWORKING:
            /* Do nothing */
            break;

          case ZAB_NWK_STATE_NETWORKED:
          case ZAB_NWK_STATE_NETWORKED_NO_COMMS:
            /* Successfully resumed */
            printApp(appService, "AppGateway: Network resume complete\n");
            gatewayState = gateway_nwk_done;
            initialiseSzlApplication();
            break;

          default:
            gatewayState = gateway_closed;
            printApp(appService, "AppGateway: Network resume failed.\n");
            break;
        }
      return;
    }


  if (gatewayState == gateway_nwk_form_init)
    {
      switch(nwkState)
        {
          case ZAB_NWK_STATE_LEAVING:
          case ZAB_NWK_STATE_NETWORKING:
            /* Do nothing */
            break;

          case ZAB_NWK_STATE_NONE:
            /* Successfully re-initialised */
            printApp(appService, "AppGateway: Network init before form complete. Forming...\n");

            gatewayState = gateway_nwk_forming;
            zabCoreAction(&localStatus, appService, ZAB_ACTION_NWK_STEER);
            checkStatusAndKillIfError(&localStatus);
            break;

          default:
            gatewayState = gateway_closed;
            printApp(appService, "AppGateway: Network init before form failed.\n");
            break;
        }
      return;
    }


  if (gatewayState == gateway_nwk_forming)
    {
      switch(nwkState)
        {
          case ZAB_NWK_STATE_NETWORKING:
            /* Do nothing */
            break;

          case ZAB_NWK_STATE_NETWORKED:
            /* Successfully formed */
            printApp(appService, "AppGateway: Network form complete\n");
            gatewayState = gateway_nwk_done;
            commitNetworkInfoToFile(networkProcessorIeee, networkEpid, networkAddress);
            initialiseSzlApplication();
            break;

          default:
            gatewayState = gateway_closed;
            printApp(appService, "AppGateway: Network form failed.\n");
            zabCoreAction(&localStatus, appService, ZAB_ACTION_CLOSE);
            break;
        }
      return;
    }
}

/******************************************************************************
 * Handle Firmware Update State Notifications
 ******************************************************************************/
void appGateway_FwUpdateStateNtfHandler(zabFwUpdateState fwUpdateState)
{
  erStatus localStatus;
  erStatusClear(&localStatus, appService);

  if (gatewayState == gateway_updating_firmware)
    {
      switch(fwUpdateState)
        {
          case ZAB_FW_STATE_UPDATING:
          case ZAB_FW_STATE_VERIFYING:
            /* Do nothing */
            break;

          case ZAB_FW_STATE_COMPLETE:
            /* Successfully updated. Set to opening and await OPENED notification */
            gatewayState = gateway_opening;
            break;

          default:
            gatewayState = gateway_closed;
            printApp(appService, "AppGateway: Firmware update failed.\n");
            zabCoreAction(&localStatus, appService, ZAB_ACTION_CLOSE);
            break;
        }
    }
}

/******************************************************************************
 * Handle Network Info State Notifications
 ******************************************************************************/
void appGateway_NetworkInfoStateNtfHandler(zabNetworkInfoState networkInfoState)
{
  unsigned64 savedNetworkProcessorIeee;
  unsigned64 savedNetworkEpid;
  unsigned16 savedNetworkAddress;
  erStatus localStatus;
  erStatusClear(&localStatus, appService);

  if (gatewayState == gateway_getting_nwk_info)
    {
      switch(networkInfoState)
        {
          case ZAB_NETWORK_INFO_STATE_IN_PROGRESS:
            /* Do nothing */
            break;

          case ZAB_NETWORK_INFO_STATE_SUCCESS:
            /* We got all the network info, so compare it with the file and either resume network or form a new one*/
            getNetworkInfoFromFile(&savedNetworkProcessorIeee, &savedNetworkEpid, &savedNetworkAddress);
            if ( (networkProcessorIeee == savedNetworkProcessorIeee) &&
                 //(networkAddress == savedNetworkAddress) && TODO Removed as SILABS cannot get the network address for now
                 (networkEpid == savedNetworkEpid) )
              {
                printApp(appService, "AppGateway: Network info matched. Resuming.\n");

                gatewayState = gateway_nwk_resuming;
              }
            else
              {
                printApp(appService, "AppGateway: Network info does not match - FORMING new network\n");

                gatewayState = gateway_nwk_form_init;
              }
            zabCoreAction(&localStatus, appService, ZAB_ACTION_NWK_INIT);
            checkStatusAndKillIfError(&localStatus);
            break;

          default:
            gatewayState = gateway_closed;
            printApp(appService, "AppGateway: Firmware update failed.\n");
            zabCoreAction(&localStatus, appService, ZAB_ACTION_CLOSE);
            break;
        }
    }
}

/******************************************************************************
 * Handle Asks
 ******************************************************************************/
zab_bool appGateway_AskCfgHandler(erStatus* Status, zabService* Service, zabAskId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer)
{
  unsigned8 Out8;

  /* Opening:
   *  - Start network process if image is up to date. If not then remain in bootloader to load new image */
  if (gatewayState == gateway_opening)
    {
      if (What == ZAB_ASK_NETWORK_PROCESSOR_START)
        {
          Out8 = (unsigned8)platUtility_IsAppImageUpToDate(nwkProcHardwareType, nwkProcAppVersion);
          osMemCopy( Status, Buffer, &Out8, sizeof(Out8) );

          printApp(Service, "AppGateway: ZAB_ASK_NETWORK_PROCESSOR_START = %s\n", Out8? "TRUE" : "FALSE");
          return zab_true;
        }
    }

  /* Resuming Previous Network:
   *  - Resume network = TRUE
   *  - Set GP Endpoint to application endpoint */
  if (gatewayState == gateway_nwk_resuming)
    {
      if (What == ZAB_ASK_NWK_RESUME)
        {
          Out8 = (unsigned8)zab_true;
          printApp(Service, "AppGateway: ZAB_ASK_NWK_RESUME = %s\n", Out8 ? "TRUE" : "FALSE");
          osMemCopy( Status, Buffer, &Out8, sizeof(Out8) );
          return zab_true;
        }

#ifdef APP_CONFIG_ENABLE_GREEN_POWER
      else if (What == ZAB_ASK_GREEN_POWER_ENDPOINT)
        {
          Out8 = APP_ENDPOINT;
          printApp(Service, "AppGateway: ZAB_ASK_GREEN_POWER_ENDPOINT = 0x%02X (%d)\n", Out8, Out8);
          osMemCopy( Status, Buffer, &Out8, sizeof(Out8) );
          return zab_true;
        }
#endif
    }

  /* Init Before Forming New Network:
   *  - Resume network = FALSE */
  if (gatewayState == gateway_nwk_form_init)
    {
      if (What == ZAB_ASK_NWK_RESUME)
        {
          Out8 = (unsigned8)zab_false;
          printApp(Service, "AppGateway: ZAB_ASK_NWK_RESUME = %s\n", Out8 ? "TRUE" : "FALSE");
          osMemCopy( Status, Buffer, &Out8, sizeof(Out8) );
          return zab_true;
        }
    }

  /* Forming New Network:
   *  - Network Steer = Form
   *  - Channel Mask = All
   *  - EPID = Schneider Wiser EH
   *  - Set GP Endpoint to application endpoint */
  if (gatewayState == gateway_nwk_forming)
    {
      if (What == ZAB_ASK_NWK_STEER)
        {
          Out8 = (unsigned8)ZAB_NWK_STEER_FORM;
          printApp(Service, "AppGateway: ZAB_ASK_NWK_STEER = Form\n");
          osMemCopy( Status, Buffer, &Out8, sizeof(Out8) );
          return zab_true;
        }
      /* Leave these ones out to allow the usual appConfig_Config to make the decisions as normal for ConsoleTest*/
/*
      else if (What == ZAB_ASK_NWK_CHANNEL_MASK)
        {
          unsigned32 Out32 = 0x07FFF800;
          printApp(Service, "AppGateway: ZAB_ASK_NWK_CHANNEL_MASK = All Channels\n");
          osMemCopy( Status, Buffer, (unsigned8*)&Out32, sizeof(Out32) );
          return zab_true;
        }

      else if (What == ZAB_ASK_NWK_EPID)
        {
          unsigned64 Out64 = 0x105E010445480000 + (unsigned16)rand();
          printApp(Service, "AppGateway: ZAB_ASK_NWK_EPID = 0x%llX\n", Out64);
          osMemCopy( Status, Buffer, (unsigned8*)&Out64, sizeof(Out64) );
          return zab_true;
        }
*/

#ifdef APP_CONFIG_ENABLE_GREEN_POWER
      else if (What == ZAB_ASK_GREEN_POWER_ENDPOINT)
        {
          Out8 = APP_ENDPOINT;
          printApp(Service, "AppGateway: ZAB_ASK_GREEN_POWER_ENDPOINT = 0x%02X (%d)\n", Out8, Out8);
          osMemCopy( Status, Buffer, &Out8, sizeof(Out8) );
          return zab_true;
        }
#endif
    }

  return zab_false;
}

/******************************************************************************
 * Handle Gives
 ******************************************************************************/
void appGateway_GiveCfgHandler(erStatus* Status, zabService* Service, zabGiveId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer)
{
  unsigned32 len;
  erStatus localStatus;
  erStatusClear(&localStatus, appService);

  len = *Size;

  switch (What)
    {
      case ZAB_GIVE_IEEE:
        if (len == sizeof(unsigned64))
          {
            networkProcessorIeee = *(unsigned64*)Buffer;
          }
        break;

      case ZAB_GIVE_NWK_ADDRESS:
        if (len == sizeof(unsigned16))
          {
            networkAddress = *(unsigned16*)Buffer;
          }
        break;

      case ZAB_GIVE_NWK_EPID:
        if (len == sizeof(unsigned64))
          {
            networkEpid = *(unsigned64*)Buffer;
          }
        break;

      case ZAB_GIVE_NETWORK_PROCESSOR_MODEL:
        if (len == sizeof(unsigned8))
          {
            nwkProcHardwareType = (zabNetworkProcessorModel)(*Buffer);
          }
        break;

      case ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_VERSION:
        if (len == 4)
          {
            memcpy(nwkProcAppVersion, Buffer, 4);
          }
        break;

      default:
        break;
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/