# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to store the ZigBeeDevice Objects defining the topology of the network
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from collections import namedtuple
from data_model import ZigBeeDeviceFactory


class ZigBeeDeviceContainer:

    def __init__(self):
        self._list_zigbee_device = {}
        self._key = namedtuple("Key", ["id_wireless_bridge", "address"])

    def add_device(self, model_id, id_wireless_bridge, address):

        # Get the factory of the device
        device_factory = ZigBeeDeviceFactory.get_factory(model_id)
        # Create the device
        device = device_factory.create(address, id_wireless_bridge)
        # Store the device
        self._list_zigbee_device[self._key(id_wireless_bridge=id_wireless_bridge, address=address)] = device

    def get_device(self, address, id_wireless_bridge):
        return self._list_zigbee_device[self._key(id_wireless_bridge=id_wireless_bridge, address=address)]

    def is_device_saved(self, address, id_wireless_bridge):
        return self._list_zigbee_device.get(self._key(id_wireless_bridge=id_wireless_bridge, address=address), None) \
               is not None
