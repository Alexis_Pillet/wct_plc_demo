/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_stdio_api.c
*    @version
*        $Rev: 3081 $
*    @last editor
*        $Author: a5089763 $
*    @date  
*        $Date:: 2017-04-13 20:49:04 +0900#$
* Description : 
******************************************************************************/
/******************************************************************************
Includes <System Includes> , "Project Includes"
******************************************************************************/
#include <stdio.h>
#include <stdarg.h>
#if defined(__RENESAS__)
/* Intrinsic functions provided by compiler. */
#include <machine.h>
#else
#include "CCRXmachine.h"
#endif
#include "iorx631.h"
#include "r_config.h"
#include "r_typedefs.h"
#include "r_stdio_api.h"
#include "r_bsp_api.h"
#include "r_timer_api.h"
#include "cpx3_api\cpx3_sap\uif\src\core\r_uif_base.h"


/******************************************************************************
Macro definitions
******************************************************************************/
#define R_STDIO_MAX_BUF_LEN (256u)   /* Max bytes to be printfed */

#define WITH_PRINTF

/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/

/******************************************************************************
Private global variables and functions
******************************************************************************/
/******************************************************************************
* Static variables
******************************************************************************/
static          uint8_t          uart_tx_buffer[R_STDIO_MAX_BUF_LEN];
static 			uint8_t 		 uart_tx_debug_buffer[R_STDIO_MAX_BUF_LEN];
static volatile r_bsp_uart_rx_t  uart_rx_params 			= {0, NULL, 0, 0, 0, R_BSP_UART_RX_OFF, 	  R_FALSE, R_FALSE, RX_UNINITIALIZED};
static volatile r_bsp_uart_debug_rx_t  uart_rx_debug_params = {0, NULL, 0, 0, 0, R_BSP_UART_DEBUG_RX_OFF, R_FALSE, R_FALSE, RX_UNINITIALIZED};
static volatile r_boolean_t      tx_busy_flag = R_FALSE;
static volatile r_boolean_t      tx_debug_flag = R_FALSE;

/******************************************************************************
* Local function headers
******************************************************************************/
static void stdio_rx_byte_handle(uint8_t rx_byte);
static void stdio_tx_debug_msg_handle(void);
static void stdio_rx_debug_msg_handle(uint8_t rx_byte);

/******************************************************************************
* Extern variables
******************************************************************************/

/******************************************************************************
* Global variables
******************************************************************************/

/******************************************************************************
Function implementations
******************************************************************************/



/******************************************************************************
* Function Name:R_STDIO_Init
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_STDIO_Init(void)
{
    r_result_t result;

    result = R_BSP_ConfigureUart(R_UART_DBG_CLOCK,
                        115200,
                        R_BSP_RX_DEBUG_UART,
                        R_UART_TX_IPR,
                        R_UART_RX_IPR,
                        R_BSP_CLK_OUT_OFF,
                        &stdio_tx_debug_msg_handle,
                        &stdio_rx_debug_msg_handle);

    uart_rx_debug_params.pdata          = NULL;
    uart_rx_debug_params.size			= 0xFF;
    uart_rx_debug_params.max_buf_size   = R_STDIO_MAX_BUF_LEN;
    uart_rx_debug_params.on             = R_BSP_UART_DEBUG_RX_ON;
    uart_rx_debug_params.cnt            = 0u;
    uart_rx_debug_params.time_stamp_set = R_FALSE;
    uart_rx_debug_params.rx_state		= RX_READY;

    if( result == R_RESULT_SUCCESS)
    {
		/* Configure UART for Demo Application */
		result = R_BSP_ConfigureUart(R_UART_HOST_CLOCK,
									 115200,
									 R_BSP_RX_HOST_UART,
									 R_UART_TX_IPR,
									 R_UART_RX_IPR,
									 R_BSP_CLK_OUT_OFF,
									 &R_STDIO_TxFinishedHandle,
									 &stdio_rx_byte_handle);

	    uart_rx_params.pdata          = NULL;
	    uart_rx_params.size	  		  = 0xFF;
	    uart_rx_params.max_buf_size   = R_STDIO_MAX_BUF_LEN;
	    uart_rx_params.on             = R_BSP_UART_RX_ON;
	    uart_rx_params.cnt            = 0u;
	    uart_rx_params.time_stamp_set = R_FALSE;
	    uart_rx_params.rx_state       = RX_READY;
    }

    return result;
}
/******************************************************************************
   End of function  R_STDIO_Init
******************************************************************************/

/******************************************************************************
* Function Name:R_STDIO_Printf
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
int32_t R_STDIO_Printf(const char* pformat, ...)
{
    va_list argument_list;
    int32_t string_length = 0;
#ifdef WITH_PRINTF
    /* Wait for previous transmission to finish */
    while (R_TRUE == tx_busy_flag)
    {
        nop();
    }

    tx_busy_flag = R_TRUE;

    /* Init argument list */
    va_start(argument_list, pformat);

    /* Convert string to buffer */
    string_length = vsprintf((char*)uart_tx_buffer, pformat, argument_list);
    if (string_length > (int32_t)R_STDIO_MAX_BUF_LEN)
    {
        string_length = -1; /* error */
    }
    else
    {
        /* Send buffer */
        R_BSP_SendUart(uart_tx_buffer, string_length, R_BSP_RX_HOST_UART);
    }

    /* Perform any cleanup necessary so that the function can return */
    va_end(argument_list);
#endif
    return string_length;
}
/******************************************************************************
   End of function  R_STDIO_Printf
******************************************************************************/

/******************************************************************************
* Function Name:R_STDIO_Gets
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
char R_STDIO_Gets(char* pstr)
{
    volatile r_bsp_uart_rx_status_t on_flag;

    /* Reset counter */
    uart_rx_params.cnt = 0u;

    /* Set RX to ON */
    uart_rx_params.on = R_BSP_UART_RX_ON;
    on_flag           = R_BSP_UART_RX_ON;

    /* Set buffer */
    uart_rx_params.pdata = (uint8_t*) pstr;

    /* Wait for transmission to finish */
    while (R_BSP_UART_RX_ON == on_flag)
    {
        on_flag = uart_rx_params.on;
        nop();
    }

    /* Terminate string. */
    *(pstr + uart_rx_params.cnt) = '\0';

    /* Return length */
    return uart_rx_params.cnt;
}
/******************************************************************************
   End of function  R_STDIO_Gets
******************************************************************************/

/******************************************************************************
* Function Name:R_STDIO_GetsWithTimer
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
char R_STDIO_GetsWithTimer(char* pstr,
                           uint32_t time)
{
    volatile r_bsp_uart_rx_status_t on_flag;
    uint32_t expire_time;

    /* Reset counter */
    uart_rx_params.cnt = 0u;

    /* Set RX to ON */
    uart_rx_params.on = R_BSP_UART_RX_ON;
    on_flag           = R_BSP_UART_RX_ON;

    /* Set buffer */
    uart_rx_params.pdata = (uint8_t*) pstr;

    R_TIMER_GetExpireTickCount(&expire_time, time);

    /* Wait for transmission to finish */
    while ((R_BSP_UART_RX_ON == on_flag) && (R_TIMER_TickCountExpired(expire_time) == R_FALSE))
    {
        on_flag = uart_rx_params.on;
        nop();
    }

    /* Terminate string. */
    *(pstr + uart_rx_params.cnt) = '\0';

    /* Return length */
    return uart_rx_params.cnt;
}
/******************************************************************************
   End of function  R_STDIO_GetsWithTimer
******************************************************************************/

/******************************************************************************
* Function R_STDIO_Get_Busy_Tx
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint8_t R_STDIO_Get_Busy_Tx( void )
{
	return tx_busy_flag;
}
/******************************************************************************
   End of function  R_STDIO_Get_Busy_Tx
******************************************************************************/

/******************************************************************************
* Function R_STDIO_Set_Busy
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_STDIO_Set_Busy( r_boolean_t flag )
{
	tx_busy_flag = flag;
}
/******************************************************************************
   End of function  R_STDIO_Set_Busy
******************************************************************************/

/******************************************************************************
* Function R_STDIO_Get_Busy_Rx
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint8_t R_STDIO_Get_Busy_Rx( void )
{
	if( uart_rx_params.rx_state == RX_READY )
	{
		return 0;
	}
	else
	{
		return 1;
	}
}
/******************************************************************************
   End of function  R_STDIO_Get_Busy_Rx
******************************************************************************/


/******************************************************************************
* Function Name:R_STDIO_TxFinishedHandle
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void R_STDIO_TxFinishedHandle(void)
{
    tx_busy_flag = R_FALSE;
}
/******************************************************************************
   End of function  R_STDIO_TxFinishedHandle
******************************************************************************/

/******************************************************************************
* Function Name:stdio_rx_byte_handle
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
static void stdio_rx_byte_handle(uint8_t rx_byte)
{
	static uint32_t start_uart_timeout 	= 0;
	uint8_t	FCSresult = 0;

	if ((R_BSP_UART_DEBUG_RX_ON == uart_rx_params.on) /*&& (NULL != uart_rx_debug_params.pdata)*/)
	{
		/* Timeout reception frame ? */
		if( ( R_UIF_ChkTimeoutMsec( start_uart_timeout, 5 ) ) && ( uart_rx_params.rx_state != RX_COMPLETE ) )
		{
			/* Error, stop reception, reset process */
			uart_rx_params.rx_state = RX_READY;
			uart_rx_params.cnt = 0;
		}

		/* First byte, uart ready and start of frame character */
		if( ( uart_rx_params.cnt == 0) && ( uart_rx_params.rx_state == RX_READY ) && ( rx_byte == 0xFE ) )
		{
			uart_rx_params.rx_state = RX_RECEIVING;

			/* Get time to start timeout */
			start_uart_timeout = R_UIF_GetCurrentTimeMsec( );
		}

		/* UART ok for reception? & timeout 100ms not reached */
		if( uart_rx_params.rx_state == RX_RECEIVING )
		{
			/* Save byte */
			uart_rx_params.pdata[uart_rx_params.cnt] = rx_byte;

			if( uart_rx_params.cnt == 1 )
			{
				/* Save frame size if the size is correct */
				if( rx_byte <= BUFFER_FRAME_SIZE )
				{
					uart_rx_params.size = rx_byte;
				}
				else
				{
					/* Error, stop reception, reset process */
					uart_rx_params.rx_state = RX_READY;
					uart_rx_params.cnt = 0;
				}
			}

			/* Wait for next character */
			uart_rx_params.cnt++;

			/* All bytes received? */
			if( uart_rx_params.cnt == ( uart_rx_params.size + 1 ) )
			{
				/* Check CRC */
				FCSresult = calcFCS( &uart_rx_params.pdata[2], ( uart_rx_params.size - 2 ) );

				/* CRC ok? */
				if( FCSresult == uart_rx_params.pdata[uart_rx_params.size] )
				{
					/* Change state for treatment */
					uart_rx_params.rx_state = RX_COMPLETE;

					/* Do not copy the CRC */
					uart_rx_params.size--;

					/* Transmission finished */
					uart_rx_params.on = R_BSP_UART_RX_OFF;
				}
				else
				{
					/* Change state for treatment */
					uart_rx_params.rx_state = RX_ERROR;

					/* Transmission finished */
					uart_rx_params.on = R_BSP_UART_RX_ON;
				}

				/* Reset counter for next reception */
				uart_rx_params.cnt = 0;
			}

			/* New byte -> restart timeout */
			start_uart_timeout = R_UIF_GetCurrentTimeMsec( );
		}
	}
	else
	{
		/* do nothing */
	}


//    if ((R_BSP_UART_RX_ON == uart_rx_params.on) && (NULL != uart_rx_params.pdata))
//    {
//        uart_rx_params.pdata[uart_rx_params.cnt] = rx_byte;
//
//        if (('\r' == rx_byte) || ('\n' == rx_byte))
//        {
//            /* Transmission finished */
//            uart_rx_params.on = R_BSP_UART_RX_OFF;
//        }
//        else if (ESC_CHARACTER == rx_byte) /* ESC */
//        {
//            /* If ESC, set counter to one */
//            uart_rx_params.cnt = 1;
//
//            /* Set zero element to ESC */
//            uart_rx_params.pdata[0] = rx_byte;
//
//            /* Transmission finished */
//            uart_rx_params.on = R_BSP_UART_RX_OFF;
//        }
//        else if (0x08 == rx_byte) /* Backspace */
//        {
//            if (uart_rx_params.cnt < 1)
//            {
//                uart_rx_params.cnt  = 0u;
//            }
//            else
//            {
//                uart_rx_params.cnt -= 1;
//            }
//        }
//        else
//        {
//            /* Wait for next character */
//            uart_rx_params.cnt++;
//        }
//    }
//    else
//    {
//        /* do nothing */
//    }
}
/******************************************************************************
   End of function  stdio_rx_byte_handle
******************************************************************************/

/******************************************************************************
* Function R_STDIO_Send_Uart5
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_STDIO_Send_Uart5( const uint8_t* data, const uint8_t size )
{
	r_result_t result = R_RESULT_FAILED;

    /* Wait for previous transmission to finish */
    while (R_TRUE == tx_debug_flag)
    {
        nop();
    }

    tx_debug_flag = R_TRUE;

    /* Check lenght */
    if (size > (int32_t)R_STDIO_MAX_BUF_LEN)
    {
    	result = R_RESULT_BAD_INPUT_ARGUMENTS; /* error */
    }
    else
    {
        /* Send buffer */
       result = R_BSP_SendUart(data, size, R_BSP_RX_DEBUG_UART);
    }

    return result;
}
/******************************************************************************
   End of function  R_STDIO_Send_Uart5
******************************************************************************/


/******************************************************************************
* Function R_STDIO_Send_Uart5_String
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_STDIO_Send_Uart5_String(const char* pformat, ...)
{
    va_list argument_list;
    int32_t string_length = 0;

    /* Wait for previous transmission to finish */
    while (R_TRUE == tx_debug_flag)
    {
        nop();
    }

    tx_debug_flag = R_TRUE;

    /* Init argument list */
    va_start(argument_list, pformat);

    /* Convert string to buffer */
    string_length = vsprintf((char*)uart_tx_debug_buffer, pformat, argument_list);
    if (string_length > (int32_t)R_STDIO_MAX_BUF_LEN)
    {
        string_length = -1; /* error */
    }
    else
    {
        /* Send buffer */
        R_BSP_SendUart(uart_tx_debug_buffer, string_length, R_BSP_RX_DEBUG_UART);
    }

    /* Perform any cleanup necessary so that the function can return */
    va_end(argument_list);
}
/******************************************************************************
   End of function  R_STDIO_Send_String_Uart5
******************************************************************************/

/******************************************************************************
* Function Name:R_STDIO_Debug_Gets
* Description :
* Arguments :
* Return Value :
******************************************************************************/
char R_STDIO_Debug_Gets(char* pstr)
{
    volatile r_bsp_uart_rx_status_t on_debug_flag;

    /* Reset counter */
    uart_rx_debug_params.cnt = 0u;

    /* Set RX to ON */
    uart_rx_debug_params.on = R_BSP_UART_DEBUG_RX_ON;
    on_debug_flag         	= R_BSP_UART_DEBUG_RX_ON;

    /* Set buffer */
    uart_rx_debug_params.pdata = (uint8_t*) pstr;

    /* Wait for transmission to finish */
    while (R_BSP_UART_DEBUG_RX_ON == on_debug_flag)
    {
    	on_debug_flag = uart_rx_debug_params.on;
        nop();
    }

    /* Terminate string. */
    *(pstr + uart_rx_debug_params.cnt) = '\0';

    /* Return length */
    return uart_rx_debug_params.cnt;
}
/******************************************************************************
   End of function  R_STDIO_Debug_Gets
******************************************************************************/


/******************************************************************************
* Function R_STDIO_Debug_Get_Busy_Tx
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint8_t R_STDIO_Debug_Get_Busy_Tx( void )
{
	return tx_debug_flag;
}
/******************************************************************************
   End of function  R_STDIO_Debug_Get_Busy
******************************************************************************/

/******************************************************************************
* Function R_STDIO_Debug_Set_Busy
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_STDIO_Debug_Set_Busy( r_boolean_t flag )
{
	tx_debug_flag = flag;
}
/******************************************************************************
   End of function  R_STDIO_Debug_Set_Busy
******************************************************************************/



/******************************************************************************
* Function R_STDIO_Debug_Get_Busy_Rx
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint8_t R_STDIO_Debug_Get_Busy_Rx( void )
{
	if( uart_rx_debug_params.rx_state == RX_READY )
	{
		return 0;
	}
	else
	{
		return 1;
	}
}
/******************************************************************************
   End of function  R_STDIO_Debug_Get_Busy
******************************************************************************/

/******************************************************************************
* Function Name:stdio_tx_debug_msg_handle
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
static void stdio_tx_debug_msg_handle(void)
{
    tx_debug_flag = R_FALSE;
}
/******************************************************************************
   End of function  stdio_tx_debug_msg_handle
******************************************************************************/

/******************************************************************************
* Function Name:stdio_rx_debug_msg_handle
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
static void stdio_rx_debug_msg_handle(uint8_t rx_byte)
{
	static uint32_t start_uart_timeout 	= 0;
	uint8_t	FCSresult = 0;

    if ((R_BSP_UART_DEBUG_RX_ON == uart_rx_debug_params.on) /*&& (NULL != uart_rx_debug_params.pdata)*/)
    {
		/* Timeout reception frame ? */
		if( ( R_UIF_ChkTimeoutMsec( start_uart_timeout, 5 ) ) && ( uart_rx_debug_params.rx_state != RX_COMPLETE ) )
		{
			/* Error, stop reception, reset process */
			uart_rx_debug_params.rx_state = RX_READY;
			uart_rx_debug_params.cnt = 0;
		}

		/* First byte, uart ready and start of frame character */
		if( ( uart_rx_debug_params.cnt == 0) && ( uart_rx_debug_params.rx_state == RX_READY ) && ( rx_byte == 0xFE ) )
		{
			uart_rx_debug_params.rx_state = RX_RECEIVING;

			/* Get time to start timeout */
			start_uart_timeout = R_UIF_GetCurrentTimeMsec( );
		}

		/* UART ok for reception? & timeout 100ms not reached */
		if( uart_rx_debug_params.rx_state == RX_RECEIVING )
		{
			/* Save byte */
			uart_rx_debug_params.pdata[uart_rx_debug_params.cnt] = rx_byte;

			if( uart_rx_debug_params.cnt == 1 )
			{
				/* Save frame size if the size is correct */
				if( rx_byte <= BUFFER_FRAME_SIZE )
				{
					uart_rx_debug_params.size = rx_byte;
				}
				else
				{
					/* Error, stop reception, reset process */
					uart_rx_debug_params.rx_state = RX_READY;
					uart_rx_debug_params.cnt = 0;
				}
			}

			/* Wait for next character */
			uart_rx_debug_params.cnt++;

			/* All bytes received? */
			if( uart_rx_debug_params.cnt == uart_rx_debug_params.size + 1 )
			{
				/* Check CRC */
				FCSresult = calcFCS( &uart_rx_debug_params.pdata[2], ( uart_rx_debug_params.size - 2 ) );

				/* CRC ok? */
				if( FCSresult == uart_rx_debug_params.pdata[uart_rx_debug_params.size] )
				{
					/* Change state for treatment */
					uart_rx_debug_params.rx_state = RX_COMPLETE;

					/* Do not copy the CRC */
					uart_rx_debug_params.size--;

					/* Transmission finished */
					uart_rx_debug_params.on = R_BSP_UART_DEBUG_RX_OFF;
				}
				else
				{
					/* Change state for treatment */
					uart_rx_debug_params.rx_state = RX_ERROR;

					/* Transmission finished */
					uart_rx_debug_params.on = R_BSP_UART_DEBUG_RX_ON;
				}

				/* Reset counter for next reception */
				uart_rx_debug_params.cnt = 0;
			}

			/* New byte -> restart timeout */
			start_uart_timeout = R_UIF_GetCurrentTimeMsec( );
		}
    }
    else
    {
        /* do nothing */
    }
}
/******************************************************************************
   End of function  stdio_rx_debug_msg_handle
******************************************************************************/



/******************************************************************************
* Function Name: Uart_IHM_Check_Frame
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint8_t Uart_IHM_Check_Frame( uint8_t* rx_data )
{
	uint8_t i = 0;

	/* Check if new reception complete */
	if( uart_rx_params.rx_state == RX_COMPLETE )
	{
		/* Save payload */
		for( i=0; i<uart_rx_params.size+1 ; i++ )
		{
			rx_data[i] = uart_rx_params.pdata[i];
		}
		uart_rx_params.rx_state = RX_READY;
		uart_rx_params.on 		= R_BSP_UART_DEBUG_RX_ON;

		return uart_rx_params.size;
	}
	else
	{
		/* To be sure that the frame size is null on the main process */
		return 0;
	}
}
/******************************************************************************
   End of function  Uart_IHM_Check_Frame
******************************************************************************/

/******************************************************************************
* Function Name: Uart_Check_Frame
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint8_t Uart_Check_Frame( uint8_t* rx_data )
{
	uint8_t i = 0;

	/* Check if new reception complete */
	if( uart_rx_debug_params.rx_state == RX_COMPLETE )
	{
		/* Save payload */
		for( i=0; i<uart_rx_debug_params.size+1 ; i++ )
		{
			rx_data[i] = uart_rx_debug_params.pdata[i];
		}
		uart_rx_debug_params.rx_state 	= RX_READY;
		uart_rx_debug_params.on 		= R_BSP_UART_DEBUG_RX_ON;

		return uart_rx_debug_params.size;
	}
	else
	{
		/* To be sure that the frame size is null on the main process */
		return 0;
	}
}
/******************************************************************************
   End of function  Uart_Check_Frame
******************************************************************************/



/******************************************************************************
* Function: calcFCS
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint8_t	calcFCS( uint8_t *pMsg, uint8_t len )
{
	uint8_t result = 0;

	while( len-- )
	{
		result ^= *pMsg++;
	}

	return result;
}
/******************************************************************************
   End of function  calcFCS
******************************************************************************/

