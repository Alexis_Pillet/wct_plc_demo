/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the Serial Boot Loader state machine for the ZAB Pro ZNP
 *   vendor.
 *   For details and flow charts see XXX.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *  00.00.02.00 18-Oct-13   MvdB   Upgraded from old file
 *                                 Support notification of errors
 * 00.00.03.01  19-Mar-14   MvdB   artf53851: Implement compatibility check for firmware image
 * 00.00.04.00  21-May-14   MvdB   artf58379: Change to block based image data asks
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
 * 002.001.001  29-Apr-15   MvdB   ARTF132260: Support firmware upgrade of CC2538 network processors
 * 002.001.006  28-Jul-15   MvdB   ARTF132260: Add handshake at start of firmware upgrade for CC2538
 * 002.002.001  19-Aug-15   MvdB   ARTF147664: Support 120 GPDs on CC2538 - required changing NV size
 * 002.002.013  14-Oct-15   MvdB   ARTF151072: Add service pointer to osTimeGetMilliseconds() and osTimeGetSeconds()
 * 002.002.020  18-Mar-16   MvdB   ARTF165342: Support performing SBL firmware updates of ZAB_NET_PROC_MODEL_CC2538_SBS_SERIAL_GATEWAY
 * 002.002.024  28-Jun-16   MvdB   ARTF172101: Implement ZNP ping to handle lost Reset indication when hidden inside a serial command that was not completed before reset
 * 002.002.031  09-Jan-17   MvdB   ARTF172881: Avoid work() returning zero due to errors like szl not initialised
 * 002.002.043  20-Jan-17   MvdB   ARTF190099: Make ping time run time controllable for ZNP & NCP. Private APIs only at this time, for industrial tester.
 * 002.002.052  30-Mar-17   MvdB   Support ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE
 * 002.002.053  12-Apr-17   SMon   Support ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE_PA
 *****************************************************************************/

#include "zabZnpService.h"



/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* Default value for Asks */
#define M_DEFAULT_UPDATE_LENGTH     0
#define M_DEFAULT_UPDATE_POINTER    NULL

/* These defined values all the management of the serial flush depending on max data length */
#define M_FLUSH_LENGTH                  254
#define M_FLUSH_BLOCK_SIZE              128
#define M_FLUSH_BLOCK_COUNT             2
#if ( (M_FLUSH_BLOCK_COUNT * M_FLUSH_BLOCK_SIZE) < M_FLUSH_LENGTH)
#error zabZnpSblUtility: (M_FLUSH_BLOCK_COUNT * M_FLUSH_BLOCK_SIZE) < M_FLUSH_LENGTH
#endif

/* Request / Response length constants */
#define HANDSHAKE_REQUEST_DATA_LENGTH   0
#define HANDSHAKE_RESPONSE_DATA_LENGTH  1

#define VERSION_REQUEST_DATA_LENGTH     0
#define VERSION_RESPONSE_DATA_LENGTH    6

/* Flash Write Request
 *  - CC2530/1: Address = 2 bytes (Addr/4). Data = 64 bytes
 *  - CC2538:   Address = 4 bytes. Data = 128 bytes */
#define CC253X_WRITE_LENGTH                    (64)
#define CC253X_WRITE_REQUEST_DATA_LENGTH       (CC253X_WRITE_LENGTH + 2)
#define CC2538_WRITE_LENGTH                    (128)
#define CC2538_WRITE_REQUEST_DATA_LENGTH       (CC2538_WRITE_LENGTH + 4)
#define WRITE_LENTH_MAX                         CC2538_WRITE_LENGTH

#define WRITE_RESPONSE_DATA_LENGTH             (1)


/* Flash Read Request
 *  - CC2530/1: Address = 2 bytes (Addr/4)
 *  - CC2538:   Address = 4 bytes */
#define CC253X_READ_REQUEST_DATA_LENGTH        2
#define CC2538_READ_REQUEST_DATA_LENGTH        4

/* Flash Read Response
 *  - Common: Status = 1 byte
 *  - CC2530/1: Address = 2 bytes (Addr/4). Data = 64 bytes
 *  - CC2538:   Address = 4 bytes. Data = 128 bytes */
#define CC253X_READ_LENGTH                     64
#define CC253X_READ_RESPONSE_DATA_OFFSET       (3)
#define CC253X_READ_RESPONSE_DATA_LENGTH       (CC253X_READ_LENGTH + CC253X_READ_RESPONSE_DATA_OFFSET)
#define CC2538_READ_LENGTH                     128
#define CC2538_READ_RESPONSE_DATA_OFFSET       (5)
#define CC2538_READ_RESPONSE_DATA_LENGTH       (CC2538_READ_LENGTH + CC2538_READ_RESPONSE_DATA_OFFSET)
#define READ_LENTH_MAX                         CC2538_READ_LENGTH

#define ENABLE_REQUEST_DATA_LENGTH      0
#define ENABLE_RESPONSE_DATA_LENGTH     1

#define RESET_REQUEST_DATA_LENGTH       0

#define START_APP_REQUEST_DATA_LENGTH     0
#define START_APP_RESPONSE_DATA_LENGTH    1

/* Timeout constants */
#define M_TIMEOUT_DISABLED 0xFFFFFFFF

/* Offset of version numbers in an update binary.
 * These are used to confirm the image supplied by the app is correct and compatible */
#define CC253X_M_TRANSPORT_REVISION_OFFSET     0x3A7E4
#define CC253X_M_PRODUCT_ID_OFFSET             0x3A7E5
#define CC253X_M_FIRMWARE_VERSION_OFFSET       0x3A7E6
#define CC253X_M_HARDWARE_TYPE_OFFSET          0x3A7FF
#define CC253X_M_MINIMUM_IMAGE_LENGTH          (CC253X_M_HARDWARE_TYPE_OFFSET+1)

#define CC2538_M_TRANSPORT_REVISION_OFFSET     0x757E4
#define CC2538_M_HARDWARE_TYPE_OFFSET          0x757FF
#define CC2538_M_MINIMUM_IMAGE_LENGTH          (CC2538_M_HARDWARE_TYPE_OFFSET+1)

/* Size of the version block */
#define M_VERSION_SECTION_SIZE ((CC253X_M_HARDWARE_TYPE_OFFSET-CC253X_M_TRANSPORT_REVISION_OFFSET)+1)

/* Offsets of various parameters within the version block */
#define M_PRODUCT_ID_OFFSET_FROM_TRANSPORT_REV              (CC253X_M_PRODUCT_ID_OFFSET - CC253X_M_TRANSPORT_REVISION_OFFSET)
#define M_FIRMWARE_VERSION_MAJOR_OFFSET_FROM_TRANSPORT_REV  (CC253X_M_FIRMWARE_VERSION_OFFSET - CC253X_M_TRANSPORT_REVISION_OFFSET)
#define M_FIRMWARE_VERSION_MINOR_OFFSET_FROM_TRANSPORT_REV  ((CC253X_M_FIRMWARE_VERSION_OFFSET - CC253X_M_TRANSPORT_REVISION_OFFSET)+1)
#define M_FIRMWARE_VERSION_MAINT_OFFSET_FROM_TRANSPORT_REV  ((CC253X_M_FIRMWARE_VERSION_OFFSET - CC253X_M_TRANSPORT_REVISION_OFFSET)+2)
#define M_HARDWARE_TYPE_OFFSET_FROM_TRANSPORT_REV           (CC253X_M_HARDWARE_TYPE_OFFSET - CC253X_M_TRANSPORT_REVISION_OFFSET)

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/* ZNP command codes for SBL */
typedef enum
{
  ZNPI_CMD_SBL_RESET_REQ            = 0x00,
  ZNPI_CMD_SBL_WRITE_FLASH_REQ      = 0x01,
  ZNPI_CMD_SBL_READ_FLASH_REQ       = 0x02,
  ZNPI_CMD_SBL_ENABLE_FLASH_REQ     = 0x03,
  ZNPI_CMD_SBL_HANDSHAKE_REQ        = 0x04,
  ZNPI_CMD_SBL_VERSION_SBL_REQ      = 0x05,
  ZNPI_CMD_SBL_VERSION_APP_REQ      = 0x06,
  ZNPI_CMD_SBL_DESCRIPTOR_APP_REQ   = 0x07,
  ZNPI_CMD_SBL_START_APP_REQ        = 0x08,

  ZNPI_CMD_SBL_RESET_RESP            = 0x80,
  ZNPI_CMD_SBL_WRITE_FLASH_RESP      = 0x81,
  ZNPI_CMD_SBL_READ_FLASH_RESP       = 0x82,
  ZNPI_CMD_SBL_ENABLE_FLASH_RESP     = 0x83,
  ZNPI_CMD_SBL_HANDSHAKE_RESP        = 0x84,
  ZNPI_CMD_SBL_VERSION_SBL_RESP      = 0x85,
  ZNPI_CMD_SBL_VERSION_APP_RESP      = 0x86,
  ZNPI_CMD_SBL_DESCRIPTOR_APP_RESP   = 0x87,
  ZNPI_CMD_SBL_START_APP_RSP         = 0x88,
} SBL_COMMAND_CODES;


/******************************************************************************
 *                      *****************************
 *                 *****           MACROS            *****
 *                      *****************************
 ******************************************************************************/

/* Macros for quick access to SBL info */
#define ZAB_SERVICE_ZNP_SBL( _srv )  (ZAB_SERVICE_VENDOR( (_srv) )->zabSblInfo)
#define SBL_CURRENT_ITEM( _srv )     (ZAB_SERVICE_ZNP_SBL( (_srv) ).currentItem)
#define SBL_TIMEOUT( _srv )          (ZAB_SERVICE_ZNP_SBL( (_srv) ).timeoutExpiryMs)
#define SBL_IMAGE_LENGTH( _srv )     (ZAB_SERVICE_ZNP_SBL( (_srv) ).imageLength)
#define SBL_ADDRESS( _srv )          (ZAB_SERVICE_ZNP_SBL( (_srv) ).address)
#define SBL_CLASS( _srv )            (ZAB_SERVICE_ZNP_SBL( (_srv) ).sblClass)

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/




/******************************************************************************
 * Utility functions to get addresses for various classes of device
 ******************************************************************************/
static unsigned32 getMinImageLength(zabSblClass sblClass)
{
  switch(sblClass)
    {
      case ZAB_SBL_CLASS_CC2530_CC2531:   return CC253X_M_MINIMUM_IMAGE_LENGTH;
      case ZAB_SBL_CLASS_CC2538:          return CC2538_M_MINIMUM_IMAGE_LENGTH;
      default:                            return 0xFFFFFFFF;
    }
}
static unsigned32 getTransportRevisionOffset(zabSblClass sblClass)
{
  switch(sblClass)
    {
      case ZAB_SBL_CLASS_CC2530_CC2531:   return CC253X_M_TRANSPORT_REVISION_OFFSET;
      case ZAB_SBL_CLASS_CC2538:          return CC2538_M_TRANSPORT_REVISION_OFFSET;
      default:                            return 0;
    }
}
static unsigned32 getHardwareTypeOffset(zabSblClass sblClass)
{
  switch(sblClass)
    {
      case ZAB_SBL_CLASS_CC2530_CC2531:   return CC253X_M_HARDWARE_TYPE_OFFSET;
      case ZAB_SBL_CLASS_CC2538:          return CC2538_M_HARDWARE_TYPE_OFFSET;
      default:                            return 0;
    }
}
static unsigned32 getWriteLength(zabSblClass sblClass)
{
  switch(sblClass)
    {
      case ZAB_SBL_CLASS_CC2530_CC2531:   return CC253X_WRITE_LENGTH;
      case ZAB_SBL_CLASS_CC2538:          return CC2538_WRITE_LENGTH;
      default:                            return 0;
    }
}
static unsigned32 getWriteRequestDataLength(zabSblClass sblClass)
{
  switch(sblClass)
    {
      case ZAB_SBL_CLASS_CC2530_CC2531:   return CC253X_WRITE_REQUEST_DATA_LENGTH;
      case ZAB_SBL_CLASS_CC2538:          return CC2538_WRITE_REQUEST_DATA_LENGTH;
      default:                            return 0;
    }
}
static unsigned32 getReadLength(zabSblClass sblClass)
{
  switch(sblClass)
    {
      case ZAB_SBL_CLASS_CC2530_CC2531:   return CC253X_READ_LENGTH;
      case ZAB_SBL_CLASS_CC2538:          return CC2538_READ_LENGTH;
      default:                            return 0;
    }
}
static unsigned32 getReadRequestDataLength(zabSblClass sblClass)
{
  switch(sblClass)
    {
      case ZAB_SBL_CLASS_CC2530_CC2531:   return CC253X_READ_REQUEST_DATA_LENGTH;
      case ZAB_SBL_CLASS_CC2538:          return CC2538_READ_REQUEST_DATA_LENGTH;
      default:                            return 0;
    }
}
static unsigned32 getReadResponseDataOffset(zabSblClass sblClass)
{
  switch(sblClass)
    {
      case ZAB_SBL_CLASS_CC2530_CC2531:   return CC253X_READ_RESPONSE_DATA_OFFSET;
      case ZAB_SBL_CLASS_CC2538:          return CC2538_READ_RESPONSE_DATA_OFFSET;
      default:                            return 0;
    }
}
static unsigned32 getReadResponseDataLength(zabSblClass sblClass)
{
  switch(sblClass)
    {
      case ZAB_SBL_CLASS_CC2530_CC2531:   return CC253X_READ_RESPONSE_DATA_LENGTH;
      case ZAB_SBL_CLASS_CC2538:          return CC2538_READ_RESPONSE_DATA_LENGTH;
      default:                            return 0;
    }
}


/******************************************************************************
 * Set the state machine inactive
 ******************************************************************************/
static void updateInactive(erStatus* Status, zabService* Service)
{
  SBL_CURRENT_ITEM(Service) = ZAB_SBL_CURRENT_ITEM_NONE;
  SBL_TIMEOUT(Service) = M_TIMEOUT_DISABLED;
  SBL_IMAGE_LENGTH(Service) = M_DEFAULT_UPDATE_LENGTH;
  SBL_ADDRESS(Service) = 0;
  SBL_CLASS(Service) = ZAB_SBL_CLASS_NONE;
}

/******************************************************************************
 * Update completed successfully
 ******************************************************************************/
static void updateComplete(erStatus* Status, zabService* Service)
{
  updateInactive(Status, Service);
  sapMsgPutNotifyFwUpdateState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_FW_STATE_COMPLETE);
}

/******************************************************************************
 * Update failed
 ******************************************************************************/
static void updateFailed(erStatus* Status, zabService* Service)
{
  updateInactive(Status, Service);
  sapMsgPutNotifyFwUpdateState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_FW_STATE_FAILED);
  zabZnpOpen_InitNetworkProcessorPing(Status, Service, ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS);
}

/******************************************************************************
 * Set item if status is good
 * Otherwise gracefull harakiri
 ******************************************************************************/
static void checkStatusSetItem(erStatus* Status, zabService* Service, zabSblCurrentItem item)
{
  if (erStatusIsOk(Status))
    {
      SBL_CURRENT_ITEM(Service) = item;
    }
  else
    {
      sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
      updateFailed(Status, Service);
    }
}

/******************************************************************************
 * Set a timeout
 ******************************************************************************/
static void setTimeout(erStatus* Status, zabService* Service, unsigned32 Timeout)
{
  if (erStatusIsOk(Status))
    {
      osTimeGetMilliseconds(Service, &SBL_TIMEOUT(Service));
      SBL_TIMEOUT(Service) += Timeout;

      /* Handle the (very unlikely) case our timeout works out to the disabled value */
      if (SBL_TIMEOUT(Service) == M_TIMEOUT_DISABLED)
        {
          SBL_TIMEOUT(Service)++;
        }
    }
}


/******************************************************************************
 * SBL Handshake
 ******************************************************************************/
static
void sblHandshake(erStatus* Status, zabService* Service)
{
  sapMsg* Msg = zabParseZNPCreateRequest(Status, Service, HANDSHAKE_REQUEST_DATA_LENGTH);

  ER_CHECK_STATUS_NULL(Status, Msg);
  printVendor(Service, "zabZnpSblUtility: SBL Handshake Req\n");
  zabParseZNPSetLength(Status, Msg, HANDSHAKE_REQUEST_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_AREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SBL);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SBL_HANDSHAKE_REQ);

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Write Flash
 *
 * Parameters:
 *  Address = Offset within image
 *  DataPointer = Pointer to bytes of data to write
 ******************************************************************************/
static
void sblWriteFlash(erStatus* Status,
                    zabService* Service,
                    unsigned32 Address,
                    unsigned8* DataPointer)
{
  unsigned8* znp;
  sapMsg* Msg = zabParseZNPCreateRequest(Status, Service, getWriteRequestDataLength(SBL_CLASS(Service)));

  ER_CHECK_STATUS_NULL(Status, Msg);

  printVendor(Service, "zabZnpSblUtility: Write Flash Req\n");

  zabParseZNPSetLength(Status, Msg, getWriteRequestDataLength(SBL_CLASS(Service)));
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_AREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SBL);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SBL_WRITE_FLASH_REQ);

  znp = (ZAB_MSG_ZNP(Msg)->Data);

  switch(SBL_CLASS(Service))
    {
      // CC2530 has a two byte address (address/4)
      case ZAB_SBL_CLASS_CC2530_CC2531:
        COPY_OUT_16_BITS(znp, (Address >> 2) ); znp+=2;
        break;

      // CC2538 has a four byte address
      case ZAB_SBL_CLASS_CC2538:
        COPY_OUT_32_BITS(znp, Address); znp+=4;
        break;

      default:
        sapMsgFree(Status, Msg);
        return;
    }

  osMemCopy(Status, znp, DataPointer, getWriteLength(SBL_CLASS(Service)));

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Read Flash
 *
 * Parameters:
 *  Address = Offset within image
 ******************************************************************************/
static
void sblReadFlash(erStatus* Status,
                  zabService* Service,
                  unsigned32 Address)
{
  unsigned8* znp;
  sapMsg* Msg = zabParseZNPCreateRequest(Status, Service, getReadRequestDataLength(SBL_CLASS(Service)));

  ER_CHECK_STATUS_NULL(Status, Msg);
  printVendor(Service, "zabZnpSblUtility: Read Flash Req\n");

  zabParseZNPSetLength(Status, Msg, getReadRequestDataLength(SBL_CLASS(Service)));
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_AREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SBL);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SBL_READ_FLASH_REQ);

  znp = (ZAB_MSG_ZNP(Msg)->Data);

  switch(SBL_CLASS(Service))
    {
      // CC2530 has a two byte address (address/4)
      case ZAB_SBL_CLASS_CC2530_CC2531:
        COPY_OUT_16_BITS(znp, (Address >> 2) ); znp+=2;
        break;

      // CC2538 has a four byte address
      case ZAB_SBL_CLASS_CC2538:
        COPY_OUT_32_BITS(znp, Address); znp+=4;
        break;

      default:
        sapMsgFree(Status, Msg);
        return;
    }

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Enable Flash
 * This command checks the DEvice type of the image, then copies the CRC into the CRC shadow.
 * It is to be used after this loader has completed a successful verify.
 ******************************************************************************/
static void enableFlash(erStatus* Status, zabService* Service)
{
  sapMsg* Msg = zabParseZNPCreateRequest(Status, Service, ENABLE_REQUEST_DATA_LENGTH);

  ER_CHECK_STATUS_NULL(Status, Msg);
  printVendor(Service, "zabZnpSblUtility: Enable Flash Req\n");
  zabParseZNPSetLength(Status, Msg, ENABLE_REQUEST_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_AREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SBL);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SBL_ENABLE_FLASH_REQ);

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Perform the next chunk of the verify
 ******************************************************************************/
static void verifyNextChunk(erStatus* Status, zabService* Service)
{
  if (SBL_ADDRESS(Service) < SBL_IMAGE_LENGTH(Service))
    {
      sblReadFlash(Status, Service,
                   SBL_ADDRESS(Service));
      checkStatusSetItem(Status, Service, ZAB_SBL_CURRENT_ITEM_READING);
      setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_SBL_TIMEOUT_MS);
    }
  else
    {
      // Complete
      enableFlash(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_SBL_CURRENT_ITEM_ENABING);
      setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_SBL_TIMEOUT_MS);
    }
}

/******************************************************************************
 * Perform the next chunk of the write
 ******************************************************************************/
static void updateNextChunk(erStatus* Status, zabService* Service)
{
  unsigned8 imageBuffer[WRITE_LENTH_MAX];
  unsigned32 bufferSize;

  if (SBL_ADDRESS(Service) < SBL_IMAGE_LENGTH(Service))
    {
      /* Get the next chunk of data to be written at offset = SBL_ADDRESS */
      bufferSize = srvCoreAskBackBuffer(Status, Service, ZAB_ASK_FIRMWARE_IMAGE_DATA,
                                        SBL_ADDRESS(Service),
                                        getWriteLength(SBL_CLASS(Service)),
                                        imageBuffer);

      if (bufferSize < getWriteLength(SBL_CLASS(Service)))
        {
          erStatusSet(Status, ZAB_ERROR_ASKED_VALUE_INVALID);
        }

      sblWriteFlash(Status, Service,
                    SBL_ADDRESS(Service),
                    imageBuffer);
      checkStatusSetItem(Status, Service, ZAB_SBL_CURRENT_ITEM_WRITING);
      setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_SBL_TIMEOUT_MS);
    }
  else
    {
      // Start Verify
      SBL_ADDRESS(Service) = 0;
      sapMsgPutNotifyFwUpdateState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_FW_STATE_VERIFYING);
      sapMsgPutNotifyFwUpdateProgress(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, 0);
      verifyNextChunk(Status, Service);
    }
}

/******************************************************************************
 * Start the firmware update process
 ******************************************************************************/
static void updateFirmware(erStatus* Status, zabService* Service)
{
  unsigned32 imageLength;
  unsigned8 imageBuffer[M_VERSION_SECTION_SIZE];
  unsigned32 bufferSize;
  ER_CHECK_STATUS(Status);

  // Check the network processor type we are working with and store the SBL class it belongs too.
  switch (ZAB_SERVICE_VENDOR(Service)->networkProcessorHardwareType)
    {
      case znpHardwareType_CC2530_TI_EB:
      case znpHardwareType_CC2531_TI_USB_Dongle:
      case znpHardwareType_CC2531_Spectec_USB_Dongle_PA:
      case znpHardwareType_CC2530_OUREA_V2:
      case znpHardwareType_CC2531_SE_Inventek_USB_Dongle_PA:
      case znpHardwareType_CC2530_Nova:
      case znpHardwareType_CC2530_SmartlinkIpz:
        SBL_CLASS(Service) = ZAB_SBL_CLASS_CC2530_CC2531;
        break;

      case znpHardwareType_CC2538_TI_EB:
      case znpHardwareType_CC2538_OUREA_UART:
      case znpHardwareType_CC2538_OUREA_USB:
      case znpHardwareType_CC2538_SBS_SerialGateway:
      case znpHardwareType_CC2538_SZCYIT_Dongle:
      case znpHardwareType_CC2538_SZCYIT_Dongle_PA:
        SBL_CLASS(Service) = ZAB_SBL_CLASS_CC2538;
        break;

      default:
        sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_UNKNOWN_NETWORK_PROCESSOR);
        updateFailed(Status, Service);
        return;
    }

  imageLength = srvCoreAskBack32(Status, Service, ZAB_ASK_FIRMWARE_IMAGE_LENGTH, M_DEFAULT_UPDATE_LENGTH);

  if (imageLength >= getMinImageLength(SBL_CLASS(Service)))
    {
      bufferSize = srvCoreAskBackBuffer(Status, Service, ZAB_ASK_FIRMWARE_IMAGE_DATA,
                                        getTransportRevisionOffset(SBL_CLASS(Service)),
                                        M_VERSION_SECTION_SIZE,
                                        imageBuffer);

      if (bufferSize >= (getHardwareTypeOffset(SBL_CLASS(Service))-getTransportRevisionOffset(SBL_CLASS(Service)))+1)
        {
          if (zabZnpSysUtility_CompatibilityCheckNewFirmwareImage(Service,
                                                                  imageBuffer[0],
                                                                  (znpProductId)imageBuffer[M_PRODUCT_ID_OFFSET_FROM_TRANSPORT_REV],
                                                                  imageBuffer[M_FIRMWARE_VERSION_MAJOR_OFFSET_FROM_TRANSPORT_REV],
                                                                  imageBuffer[M_FIRMWARE_VERSION_MINOR_OFFSET_FROM_TRANSPORT_REV],
                                                                  imageBuffer[M_FIRMWARE_VERSION_MAINT_OFFSET_FROM_TRANSPORT_REV],
                                                                  (znpHardwareType)imageBuffer[M_HARDWARE_TYPE_OFFSET_FROM_TRANSPORT_REV]) == zab_true)
            {
              SBL_IMAGE_LENGTH(Service) = imageLength;
              SBL_ADDRESS(Service) = 0;

              sapMsgPutNotifyFwUpdateProgress(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, 0);
              sapMsgPutNotifyFwUpdateState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_FW_STATE_UPDATING);

              //updateNextChunk(Status, Service);

              checkStatusSetItem(Status, Service, ZAB_SBL_CURRENT_ITEM_HANDSHAKING);
              setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_SBL_TIMEOUT_MS);
              sblHandshake(Status, Service);
            }
          else
            {
              sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_BOOTLOADER_IMAGE_INVALID);
              updateFailed(Status, Service);
            }
        }
      else
        {
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ASKED_VALUE_INVALID);
          updateFailed(Status, Service);
        }
    }
  else
    {
      sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ASKED_VALUE_INVALID);
      updateFailed(Status, Service);
    }
}

/******************************************************************************
 * SBL Reset
 ******************************************************************************/
void sblReset(erStatus* Status, zabService* Service)
{
  sapMsg* Msg = zabParseZNPCreateRequest(Status, Service, RESET_REQUEST_DATA_LENGTH);

  ER_CHECK_STATUS_NULL(Status, Msg);
  printVendor(Service, "zabZnpSblUtility: SBL Reset Req\n");
  zabParseZNPSetLength(Status, Msg, RESET_REQUEST_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_AREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SBL);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SBL_RESET_REQ);

  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Create
 * Initialises SBL state machine
 ******************************************************************************/
void zabZnpSblUtility_Create(erStatus* Status, zabService* Service)
{
  updateInactive(Status, Service);
  sapMsgPutNotifyFwUpdateProgress(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, 0);
  sapMsgPutNotifyFwUpdateState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_FW_STATE_NONE);
}

/******************************************************************************
 * Update timeout
 * Check for timeouts within the state machine
 ******************************************************************************/
unsigned32 zabZnpSblUtility_UpdateTimeout(erStatus* Status, zabService* Service, unsigned32 Time)
{
  unsigned32 timeoutRemaining = ZAB_WORK_DELAY_MAX_MS;
  ER_CHECK_STATUS_VALUE(Status, timeoutRemaining);
  ER_CHECK_NULL_VALUE(Status, Service, timeoutRemaining);

  if (SBL_TIMEOUT(Service) != M_TIMEOUT_DISABLED)
    {
      /* Check timeout while handling wrapped time*/
      timeoutRemaining = SBL_TIMEOUT(Service) - Time;
      if ( (timeoutRemaining == 0) || (timeoutRemaining > ZAB_ZNP_MS_TIMEOUT_MAX) )
        {
          timeoutRemaining = 0;
          updateFailed(Status, Service);
        }
    }
  return timeoutRemaining;
}

/******************************************************************************
 * Action Handler
 * Handles Update action for the SBL state machines
 ******************************************************************************/
void zabZnpSblUtility_Action(erStatus* Status, zabService* Service, unsigned8 Action)
{
  ER_CHECK_STATUS(Status);
  ER_CHECK_NULL(Status, Service);

  switch(Action)
    {
      /*
      ** Firmware Update
      */
      case ZAB_ACTION_UPDATE_FIRMWARE:
        if (zabZnpSblUtility_UpdateActive(Service) == zab_false)
          {
            if (zabZnpOpen_GetOpenState(Service) == ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE)
              {
                updateFirmware(Status, Service);
              }
            else
              {
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
                sapMsgPutNotifyFwUpdateState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_FW_STATE_NONE);
              }
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_BOOTLOADER_ACTION_IN_PROGRESS);
          }
        break;

      /*
      ** Exit Firmware Updater
      */
      case ZAB_ACTION_EXIT_FIRMWARE_UPDATER:
        if (zabZnpSblUtility_UpdateActive(Service) == zab_false)
          {
            if (zabZnpOpen_GetOpenState(Service) == ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE)
              {
                //sblReset(Status, Service);
                /* Start the app.
                 * This is just an API for the Open State machine, it does not run within the SBL state machine.
                 * We just send the req and pass any response to the Open state machine, which is responsible for timeouts etc. */
                zabZnpSblUtility_AppStartReq(Status, Service);
                zabZnpOpen_SblAppStartNotification(Status, Service);
              }
            else
              {
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
                sapMsgPutNotifyFwUpdateState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_FW_STATE_NONE);
              }
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_BOOTLOADER_ACTION_IN_PROGRESS);
          }
        break;
    }
}

/******************************************************************************
 * Process all incoming messages of subsystem SBL
 ******************************************************************************/
void zabZnpSBLUtility_ProcessMsg(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8 cmd;
  unsigned16 progress;
  unsigned16 index;
  unsigned8 dataLength;
  unsigned8* dataPointer = NULL;
  unsigned8 imageBuffer[READ_LENTH_MAX];
  unsigned32 bufferSize;
  unsigned32 readLength;

  zabParseZNPGetCommandID(Status, Message, &cmd);

  dataLength = ZAB_MSG_ZNP(Message)->Length;
  dataPointer = ZAB_MSG_ZNP(Message)->Data;


  switch (cmd)
    {

      case ZNPI_CMD_SBL_READ_FLASH_RESP:
        printVendor(Service, "zabZnpSblUtility: Read Flash Rsp\n");
        if( (SBL_CURRENT_ITEM(Service) == ZAB_SBL_CURRENT_ITEM_READING) &&
            (dataLength >= getReadResponseDataLength(SBL_CLASS(Service))) &&
            ((ZNPI_API_ERROR_CODES)dataPointer[0] == ZNPI_API_ERR_SUCCESS) )
          {
            readLength = getReadLength(SBL_CLASS(Service));

            /* Get the next chunk of data to be verified at offset = SBL_ADDRESS */
            bufferSize = srvCoreAskBackBuffer(Status, Service, ZAB_ASK_FIRMWARE_IMAGE_DATA,
                                              SBL_ADDRESS(Service),
                                              readLength,
                                              imageBuffer);

            if ( (bufferSize >= readLength) &&
                 (osMemCmp(Status, &dataPointer[getReadResponseDataOffset(SBL_CLASS(Service))], imageBuffer, readLength) == 0) )
              {
                SBL_ADDRESS(Service) += readLength;
                progress = ((unsigned64)SBL_ADDRESS(Service) << 8) / SBL_IMAGE_LENGTH(Service);
                if (progress > 255)
                  {
                    progress = 255;
                  }
                sapMsgPutNotifyFwUpdateProgress(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, (unsigned8)progress);
                verifyNextChunk(Status, Service);
              }
            else
              {
                printError(Service, "zabZnpSblUtility: ERROR: Verify Failed at 0x%04X\n", SBL_ADDRESS(Service));

                printError(Service, "zabZnpSblUtility: ERROR: Expected:");
                for (index = 0; index < readLength; index++)
                  {
                    printError(Service, " %02X", imageBuffer[index]);
                  }
                printError(Service, "\n");

                printError(Service, "zabZnpSblUtility: ERROR: Received:");
                for (index = 0; index < readLength; index++)
                  {
                    printError(Service, " %02X", dataPointer[getReadResponseDataOffset(SBL_CLASS(Service))+index]);
                  }
                printError(Service, "\n");

                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_BOOTLOADER_VERIFY_FAILED);
                updateFailed(Status, Service);
              }
          }
        break;

      case ZNPI_CMD_SBL_WRITE_FLASH_RESP:
        printVendor(Service, "zabZnpSblUtility: Write Flash Rsp\n");
        if( (SBL_CURRENT_ITEM(Service) == ZAB_SBL_CURRENT_ITEM_WRITING) &&
            (dataLength >= WRITE_RESPONSE_DATA_LENGTH) )
          {
            if ((ZNPI_API_ERROR_CODES)dataPointer[0] == ZNPI_API_ERR_SUCCESS)
              {
                SBL_ADDRESS(Service) += getWriteLength(SBL_CLASS(Service));
                progress = ((unsigned64)SBL_ADDRESS(Service) << 8) / SBL_IMAGE_LENGTH(Service);
                sapMsgPutNotifyFwUpdateProgress(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, progress);
                updateNextChunk(Status, Service);
              }
            else
              {
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_BOOTLOADER_WRITE_FAILED);
                updateFailed(Status, Service);
              }
          }
        break;

      case ZNPI_CMD_SBL_ENABLE_FLASH_RESP:
        printVendor(Service, "zabZnpSblUtility: Enable Flash Rsp\n");
        if( (SBL_CURRENT_ITEM(Service) == ZAB_SBL_CURRENT_ITEM_ENABING) &&
            (dataLength >= ENABLE_RESPONSE_DATA_LENGTH) )
          {
            if ((ZNPI_API_ERROR_CODES)dataPointer[0] == ZNPI_API_ERR_SUCCESS)
              {
                // Complete!
                zabZnpOpen_SblAppStartNotification(Status, Service);
                updateComplete(Status, Service);
              }
            else
              {
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_BOOTLOADER_VERIFY_FAILED);
                updateFailed(Status, Service);
              }
          }
        break;


      case ZNPI_CMD_SBL_VERSION_APP_RESP:
        if(dataLength >= VERSION_RESPONSE_DATA_LENGTH)
          {
            printVendor(Service, "zabZnpSblUtility: SBL Version Rsp\n");
            zabZnpOpen_SblAppVersionRspHandler(Status,
                                                Service,
                                                (sblStatus)dataPointer[0], //Result
                                                dataPointer[1], // TransportRev,
                                                (znpProductId)dataPointer[2], // ProductId,
                                                dataPointer[3], // MajorVersion,
                                                dataPointer[4], // MinorVersion
                                                dataPointer[5]); // ReleaseVersion
          }
        break;


      case ZNPI_CMD_SBL_START_APP_RSP:
        if(dataLength >= START_APP_RESPONSE_DATA_LENGTH)
          {
            printVendor(Service, "zabZnpSblUtility: Start App Rsp\n");
            zabZnpOpen_SblAppStartRspHandler(Status,
                                              Service,
                                              (sblStatus)dataPointer[0]);
          }
        break;


      case ZNPI_CMD_SBL_HANDSHAKE_RESP:
        if(dataLength >= HANDSHAKE_RESPONSE_DATA_LENGTH)
          {
            printVendor(Service, "zabZnpSblUtility: Handshake Rsp\n");
            updateNextChunk(Status, Service);
          }
        break;

      default:
          printError(Service, "zabZnpSblUtility: WARNING: unknown  ZNP message ID=%#02x.\n", cmd);
          break;
    }
}

/******************************************************************************
 * Send an SBL Application Version Request
 ******************************************************************************/
void zabZnpSblUtility_AppVersionReq(erStatus* Status, zabService* Service)
{
  sapMsg* Msg = zabParseZNPCreateRequest(Status, Service, VERSION_REQUEST_DATA_LENGTH);

  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetLength(Status, Msg, VERSION_REQUEST_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_AREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SBL);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SBL_VERSION_APP_REQ);

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Send an SBL Application Start Request
 ******************************************************************************/
void zabZnpSblUtility_AppStartReq(erStatus* Status, zabService* Service)
{
  sapMsg* Msg = zabParseZNPCreateRequest(Status, Service, VERSION_REQUEST_DATA_LENGTH);

  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetLength(Status, Msg, START_APP_REQUEST_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_AREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SBL);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SBL_START_APP_REQ);

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Confirm if a version of bootloader is supported by this utility
 * Note: 0.0.0 indicates no response, which is a TI standard bootloader if it has
 *       previously responded to a handshake
 ******************************************************************************/
zab_bool zabZnpSblUtility_CompatibilityCheck(unsigned8 MajorVersion,
                                            unsigned8 MinorVersion,
                                            unsigned8 ReleaseVersion)
{
  /* For now, all versions are considered valid */
  return zab_true;
}

/******************************************************************************
 * Confirm if an update is in progress
 ******************************************************************************/
zab_bool zabZnpSblUtility_UpdateActive(zabService* Service)
{
  zab_bool updateActive;

  if (SBL_CURRENT_ITEM(Service) == ZAB_SBL_CURRENT_ITEM_NONE)
    {
      updateActive = zab_false;
    }
  else
    {
      updateActive = zab_true;
    }
  return updateActive;
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
