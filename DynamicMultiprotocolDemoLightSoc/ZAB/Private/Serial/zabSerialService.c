/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the main interface for the ZAB serial service
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.02.01  10-Dec-13   MvdB   Original
 * 00.00.04.00  25-Apr-14   MvdB   Fix issue with data in with no FCS
 * 002.000.007  xx-Apr-15   MvdB   ARTF110329: Review handling of serial transmit failures and close when detected.
 * 002.002.021  20-Apr-16   MvdB   ARTF167734: Improve receive parser to drop invalid commands and recover faster
 *                                 ARTF167736: Add ZAB_ACTION_INTERNAL_SERIAL_FLUSH
 * 002.002.037  11-Jan-17   MvdB   ARTF165759: Upgrade zabSerialService_SerialReceiveHandler() to return zabSerialService_ReceiveStatus_t to indicate when work is required
 *****************************************************************************/
#include "zabZnpService.h"
#include "zabSerialServicePrivate.h"

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

#define SERIAL_ACTION_OUT_CALLBACK( S ) (ZAB_SERVICE_ZNP( S )->ActionOutCallback)
#define SERIAL_DATA_OUT_CALLBACK( S )   (ZAB_SERVICE_ZNP( S )->SerialOutCallback)


/******************************************************************************
 * Confirm Cmd0 is incoming command is valid
 ******************************************************************************/
static zab_bool validateIncomingCommandZero(unsigned8 Cmd0)
{
  ZNPI_TYPE cmdType;
  ZNPI_SUBSYSTEM cmdSubSys;
  zab_bool cmdValid = zab_false;

  cmdType = (ZNPI_TYPE)(Cmd0 & ZNPI_MSG_CMD_0_TYPE_MASK);
  cmdSubSys = (ZNPI_SUBSYSTEM)(Cmd0 & ZNPI_MSG_CMD_0_SUBSYSTEM_MASK);

  if ( ( (cmdType == ZNPI_TYPE_SRSP) || (cmdType == ZNPI_TYPE_AREQ) ) &&
       ( (cmdSubSys <= ZNPI_SUBSYSTEM_SBL) || (cmdSubSys == ZNPI_SUBSYSTEM_GP) || (cmdSubSys == ZNPI_SUBSYSTEM_RFT)) )
    {
      cmdValid = zab_true;
    }

  return cmdValid;
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                 *****         VENDOR PUBLIC        *****
 *                      ******************************
 ******************************************************************************/

#define SERIAL_RX_BUFFER( S ) (ZAB_SERVICE_ZNP( S )->SerialReceiveBuffer)

/* Index of the data length field in a ZNP command */
#ifdef ZAB_VND_CFG_ZNP_M_SOF_AND_FCS_ENABLED
#define ZNP_M_LENGTH_INDEX 1
#define ZNP_M_CMD0_INDEX   2
#else
#define ZNP_M_LENGTH_INDEX 0
#define ZNP_M_CMD0_INDEX   1
#endif

/* Number of bytes before the data field in a ZNP command */
#ifdef ZAB_VND_CFG_ZNP_M_SOF_AND_FCS_ENABLED
#define ZNP_M_NUM_BYTES_BEFORE_DATA 4
#else
#define ZNP_M_NUM_BYTES_BEFORE_DATA 3
#endif


/******************************************************************************
 * Serial Receive Handler
 * Application serial glue must feed ZAB serial bytes received from the network
 * processor via this function.
 * There are no restriction on how bytes are fed to ZAB (one at a time, or in blocks):
 * ZAB will handle all queues, checksums, start of frame etc.
 *
 * Returns zabSerialService_ReceiveStatus_t:
 *  - ZAB_SERIAL_RECEIVE_WORK_NOT_REQUIRED:
 *    - Serial data did not complete a command that requires processing.
 *      No further action required.
 *  - ZAB_SERIAL_RECEIVE_WORK_READY
 *    - Serial data completed a command that requires processing.
 *      zabCoreWork() should be called as soon as possible.
 ******************************************************************************/
zabSerialService_ReceiveStatus_t zabSerialService_SerialReceiveHandler(erStatus* Status,
                                                                       zabService* Service,
                                                                       unsigned8 DataLength,
                                                                       unsigned8* DataPointer)
{
  unsigned16 index;
  sapMsg* Msg;
  zabSerialService_ReceiveStatus_t rxStatus = ZAB_SERIAL_RECEIVE_WORK_NOT_REQUIRED;

#ifdef ZAB_VND_CFG_ZNP_M_SOF_AND_FCS_ENABLED
  unsigned8 fcs;
#endif
  ER_CHECK_STATUS_VALUE(Status, rxStatus);

  printVendor(Service, "ZNP Serial Rx Handler: Received 0x%02X bytes: ", DataLength);
  for (index = 0; index < DataLength; index++)
    {
      printVendor(Service, " %02X", DataPointer[index]);
    }
  printVendor(Service, "\n");


  index = 0;
  while (index < DataLength)
    {

#ifdef ZAB_VND_CFG_ZNP_M_SOF_AND_FCS_ENABLED
      /* If we have not started a frame, drop bytes until we hit the SOF */
      if (SERIAL_RX_BUFFER(Service).Length == 0)
        {
          if (DataPointer[index] == ZAB_VND_CFG_ZNP_M_START_OF_FRAME)
            {
              SERIAL_RX_BUFFER(Service).Data[SERIAL_RX_BUFFER(Service).Length] = DataPointer[index];
              SERIAL_RX_BUFFER(Service).Length++;
            }
          else
            {
          	  // Drop bytes until we hit SOF
              printError(Service, "ZNP Serial Rx Handler: Warning - byte dropped\n");
            }
        }

      /* Once we have received the SOF, start processing [length][cmd0][cmd1][data[length]][fcs] */
      else
#endif // ZAB_VND_CFG_ZNP_M_SOF_AND_FCS_ENABLED
        {
          /* Pick out the length byte for range checking */
          if (SERIAL_RX_BUFFER(Service).Length == ZNP_M_LENGTH_INDEX)
            {
              if (DataPointer[index] <= ZAB_VND_CFG_ZNP_M_MAX_ZNP_LENGTH_FIELD_VALUE)
                {
                  SERIAL_RX_BUFFER(Service).Data[SERIAL_RX_BUFFER(Service).Length] = DataPointer[index];
                  SERIAL_RX_BUFFER(Service).Length++;
                }
              else if (DataPointer[index] == ZAB_VND_CFG_ZNP_M_START_OF_FRAME)
                {
                  /* Do Nothing
                   * This is to handle a specific error we are seeing most commonly in the SLIPZ serial glue */
                  printError(Service, "ZNP Serial Rx Handler: Warning - double SOF\n");
                }
              else
                {
                  printError(Service, "ZNP Serial Rx Handler: Length byte exceeds max\n");
                  SERIAL_RX_BUFFER(Service).Length = 0;
                }
            }

          /* We will do some tight validation on Cmd0 to try to filter out rubbish data */
          else if (SERIAL_RX_BUFFER(Service).Length == ZNP_M_CMD0_INDEX )
            {
              if ( validateIncomingCommandZero(DataPointer[index]) == zab_true )
                {
                  SERIAL_RX_BUFFER(Service).Data[SERIAL_RX_BUFFER(Service).Length] = DataPointer[index];
                  SERIAL_RX_BUFFER(Service).Length++;
                }
              else
                {
                  printError(Service, "ZNP Serial Rx Handler: Cmd0 invalid: 0x%02X\n", DataPointer[index]);
                  SERIAL_RX_BUFFER(Service).Length = 0;
                }
            }

          /* Accept Cmd1 and Command Data as defined by data_length field at Data[1] */
          else if (SERIAL_RX_BUFFER(Service).Length < (SERIAL_RX_BUFFER(Service).Data[ZNP_M_LENGTH_INDEX]+ZNP_M_NUM_BYTES_BEFORE_DATA))
            {
              SERIAL_RX_BUFFER(Service).Data[SERIAL_RX_BUFFER(Service).Length] = DataPointer[index];
              SERIAL_RX_BUFFER(Service).Length++;
            }

#ifdef ZAB_VND_CFG_ZNP_M_SOF_AND_FCS_ENABLED
          /*
           * We have received SOF, Cmd0, Cmd1 and the define number of data bytes, so now it is the CRC .
           * If the CRC matches, create and send SAP message.
           * If it fails, discard what we have and wait for the next command.
           */
          else
            {
              fcs = zabUtility_ComputeFCS(&SERIAL_RX_BUFFER(Service).Data[ZNP_M_LENGTH_INDEX], SERIAL_RX_BUFFER(Service).Length-ZNP_M_LENGTH_INDEX);

              if (fcs == DataPointer[index])
#else
          /* For Non FCS systems, we need to check if this is the last byte and if so push the frame upwards */
          if (SERIAL_RX_BUFFER(Service).Length >= (SERIAL_RX_BUFFER(Service).Data[ZNP_M_LENGTH_INDEX]+ZNP_M_NUM_BYTES_BEFORE_DATA))
            {
#endif


                {
                  Msg = zabParseZNPCreateResponse(Status, Service, (unsigned16) SERIAL_RX_BUFFER(Service).Length/*-ZNP_M_LENGTH_INDEX ANAS*/);
                  if ( erStatusIsOk(Status) && (Msg != NULL) )
                    {
                      sapMsgCopyAppDataTo(Status, Msg, SERIAL_RX_BUFFER(Service).Data, (unsigned16) SERIAL_RX_BUFFER(Service).Length/*-ZNP_M_LENGTH_INDEX*/);
                      sapMsgPutFree(Status, zabCoreSapSerial(Service), Msg);
                    }

                  /* Whether we were successful or not, ask for work.
                   * The only normal reason for failing would be if all buffers were full, in which case work is a good thing to clear them anyway! */
                  rxStatus = ZAB_SERIAL_RECEIVE_WORK_READY;
                }
#ifdef ZAB_VND_CFG_ZNP_M_SOF_AND_FCS_ENABLED
              else
                {
                  printError(Service, "ZNP Serial Rx Handler: FCS error\n");
                }
#endif // ZAB_VND_CFG_ZNP_M_SOF_AND_FCS_ENABLED

              /* Clear everything for next frame */
              SERIAL_RX_BUFFER(Service).Length = 0;
            }
        }
      index++;
    }

  return rxStatus;
}

/******************************************************************************
 * Open State Notification
 * Application serial glue must feed ZAB notifications via this function.
 ******************************************************************************/
void zabSerialService_NotifyOpenState(erStatus* Status,
                                      zabService* Service,
                                      zabOpenState OpenState)
{
  ER_CHECK_STATUS(Status);
  sapMsgPutNotifyOpenState(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_IN, OpenState);
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                 *****         VENDOR PRIVATE       *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise the Serial SAP In/Out Service
 * Clears all handlers and empties all queues
 ******************************************************************************/
void zabSerialService_Init(erStatus* Status, zabService* Service,
                           zabSerialService_ActionOutCallback_t ActionOutCallback,
                           zabSerialService_SerialOutCallback_t SerialOutCallback)
{
  ER_CHECK_NULL(Status, ActionOutCallback);
  ER_CHECK_NULL(Status, SerialOutCallback);
  SERIAL_ACTION_OUT_CALLBACK(Service) = ActionOutCallback;
  SERIAL_DATA_OUT_CALLBACK(Service) = SerialOutCallback;
  zabSerialService_Reset(Status, Service);
}

/******************************************************************************
 * Reset the Serial SAP In/Out Service
 * Clears data but leaves handlers intact
 ******************************************************************************/
void zabSerialService_Reset(erStatus* Status, zabService* Service)
{
  SERIAL_RX_BUFFER(Service).Length = 0;
}

/******************************************************************************
 * Serial SAP Out Handler
 * Converts the following into nicely formatted function calls for app serial glue:
 *  - Actions: Open/Close
 *  - Application data frames
 ******************************************************************************/
void zabSerialService_OutHandler (erStatus* Status, sapHandle Sap, sapMsg* Message)
{
  zabService* Service;
  sapMsgType Type;
  zabAction Action;
  erStatus glueStatus;
  ER_CHECK_NULL(Status, Message);
  Service = sapCoreGetService(Sap);
  erStatusClear(&glueStatus, Service);

  /* First see if it is an action or application data */
  Type = sapMsgGetType(Message);
  switch (Type)
    {
      case SAP_MSG_TYPE_ACTION:
        /* Perform open or close actions and notify result */
        Action = sapMsgGetAction(Message);
        switch (Action)
          {
            case ZAB_ACTION_OPEN:
            case ZAB_ACTION_CLOSE:
              /* Send the message to the serial glue. Catch any errors and return them as ZAB_ERROR_VENDOR_SENDING */
              if (SERIAL_ACTION_OUT_CALLBACK(Service) != NULL)
                {
                  SERIAL_ACTION_OUT_CALLBACK(Service)(&glueStatus, Service, Action);
                  if (erStatusIsError(&glueStatus))
                    {
                      erStatusSet(Status, ZAB_ERROR_VENDOR_SENDING);
                    }
                }
              else
                {
                  erStatusSet(Status, ZAB_ERROR_VENDOR_SENDING);
                }
              break;

            /* Flush the serial queue on request from vendor.
             * This is done after a command failed to get a response.
             * It may result in another frames getting dropped, but if we do not do it we can have cases where the serial parser is expecting
             * a long frame, so multiple serial SRSP's get consumed by it, and even several retries fail */
            case ZAB_ACTION_INTERNAL_SERIAL_FLUSH:
              printError(Service, "ZNP Serial: Rx Flush!!!\n");
              SERIAL_RX_BUFFER(Service).Length = 0;
              break;


            default:
              printError(Service, "zabSerialService_SerialOutHandler: Unknown Action = 0x%02X\n", (unsigned8)Action);
              break;
          }
        break;

      case SAP_MSG_TYPE_APP:
        /* Send serial data */
        if (sapMsgGetAppType(Message) == ZAB_MSG_APP_RAW)
          {
            /* Send the message to the serial glue. Catch any errors and return them as ZAB_ERROR_VENDOR_SENDING */
            if (SERIAL_DATA_OUT_CALLBACK(Service) != NULL)
              {
                SERIAL_DATA_OUT_CALLBACK(Service)(&glueStatus, Service, sapMsgGetAppDataLength(Message), sapMsgGetAppData(Message));
                if (erStatusIsError(&glueStatus))
                  {
                    erStatusSet(Status, ZAB_ERROR_VENDOR_SENDING);
                  }
              }
            else
              {
                erStatusSet(Status, ZAB_ERROR_VENDOR_SENDING);
              }
          }
        else
          {
            printError(Service, "appSerialCallBack: Unknown App Msg Type = 0x%02X\n", sapMsgGetAppType(Message));
          }
        break;

      default:
        printError(Service, "appSerialCallBack: Unknown Msg Type = 0x%02X\n", (unsigned8)Type);
        break;
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/