/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the Electrical Circuit Breaker Cluster.
 *   This plugin is originally designed for Nova (partner).
 * 
 *   It supports:
 *    - Multiple, application specified endpoints
 *    - Standard attributes
 *    - =S= MS attributes defined in ZigBee&GreenPower_Invariants_Partner_Products_V05.pdf
 *    - Default report configuration for time based reports (fast or slow)
 *    - Callback notification to app when a writable attribute is changed
 * 
 *   It does not (currently) support:
 *    - Non-volatile backing of any data.
 *    - Report on change. These are not required for Nova, but would be required for certification.
 *
 * CONFIGURATION:
 *   ECB_M_USE_DIRECT_ACCESS:
 *    - Define to have plugin malloc data storage for attributes and access the data directly.
 *    - Undefine to have plugin use callbacks to request attribute read/write via callbacks
 *
 * USAGE:
 *   Usage of this plugin is very simple:
 *   If using ECB_M_USE_DIRECT_ACCESS:
 *    1. Initialise it with SzlPlugin_EcbClusterInit(), specify the endpoints and a write notification callback
 *    2. Use SzlPlugin_EcbCluster_GetAttributePointer() to get access to the data for each endpoint.
 *    3. Populate the data
 *    4. Call SzlPlugin_EcbCluster_ConfigureReporting() to start attribute reporting
 *    5. Update data as it changes, the plugin will do the rest.
 *   If not using METERING_M_USE_DIRECT_ACCESS:
 *    1. Initialise it with SzlPlugin_EcbClusterInit(), specify the endpoints and read/write attribute callbacks
 *    2. Call SzlPlugin_EcbCluster_ConfigureReporting() to start attribute reporting
 * 
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         18-Apr-13   MvdB   Original
 *    2         19-May-14   MvdB   Remove endpoint number from public attribute data
 *    3         29-Oct-14   MvdB   artf74202: Support use of callbacks instead of direct access
 *    4         19-Feb-15   MvdB   Add attributes ATTRID_ECB_TU_SERVICE_LIFE, ATTRID_ECB_TU_ADAPTATION_INFO
 *    6         01-Apr-15   MvdB   ARTF116257: ECB Plugin: TuAdaptationInfo length should have +1 for zero terminator
 * 002.001.006  27-Jul-15   MvdB   ARTF134686: Standardise on SZL_RESULT_t for plugin init return value
 *                                 ARTF138318: Improve plugin destroy, add disable reporting function
 *****************************************************************************/
#ifndef _ELECTRICAL_CIRCUIT_BREAKER_CLUSTER_H_
#define _ELECTRICAL_CIRCUIT_BREAKER_CLUSTER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "zabTypes.h"
#include "szl.h"

/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/

/* If defined, the plugin will use Direct Access to attribute data. This means:
 *  - Plugin will malloc space to store all attribute data for the cluster.
 *  - Plugin will directly access this data via pointers.
 *  - Application can get a pointer to the structure, then set values in it.
 *  - If the applicaiton is not single threaded it must handle mutexing before accessing the data.
 * If not defined, the plugin will use callbacks to access attribute data. This means:
 *  - The plugin will read/write data by calling an application supplied callback.
 *  - The application can then manage all data access itself.
 */
//#define ECB_M_USE_DIRECT_ACCESS
  
/* String lengths */
#define ECB_M_REFERENCE_LENGTH              32
#define ECB_M_RANGE_LENGTH                  12
#define ECB_M_STANDARD_LENGTH               4
#define ECB_M_SERIAL_NUMBER_LENGTH          12
#define ECB_M_PERFORMANCE_LENGTH            8
#define ECB_M_HARDWARE_VERSION_LENGTH       12
#define ECB_M_USAGE_LENGTH                  12
#define ECB_M_TU_ADAPTATION_INFO_LENGTH     12  
  
/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Attributes Identifiers */
typedef enum
{
  ATTRID_ECB_BR_STATUS                = 0x0000,
  ATTRID_ECB_BR_STATUS_ALARM          = 0x0001,
  ATTRID_ECB_BR_STATUS_ALARM_MASK     = 0x0002,
  ATTRID_ECB_BR_TRIP_BASIC_ALARM      = 0x0100,
  ATTRID_ECB_BR_TRIP_BASIC_ALARM_MASK = 0x0101,
  ATTRID_ECB_BR_SETPOINT_ALARM        = 0x0200,
  ATTRID_ECB_BR_SETPOINT_ALARM_MASK   = 0x0201,
  ATTRID_ECB_NOMINAL_CURRENT          = 0x0300,
  ATTRID_ECB_BR_SETTINGS_ALARM        = 0x0301,
  ATTRID_ECB_BR_SETTINGS_ALARM_MASK   = 0x0302,
  ATTRID_ECB_BR_IDENTIFIER            = 0x0400,
  ATTRID_ECB_BR_TYPE                  = 0x0401,
  ATTRID_ECB_BR_REFERENCE             = 0x0402,
  ATTRID_ECB_BR_FAMILY                = 0x0403,
  ATTRID_ECB_BR_RANGE                 = 0x0404,
  ATTRID_ECB_BR_STANDARD              = 0x0405,
  ATTRID_ECB_BR_SERIAL_NUMBER         = 0x0406,
  ATTRID_ECB_BR_PERFORMANCE           = 0x0407,
  ATTRID_ECB_BR_NB_POLES              = 0x0408,
  ATTRID_ECB_BR_INSTALLATION          = 0x0409,
  ATTRID_ECB_BR_HW_VERSION            = 0x040A,
  ATTRID_ECB_BR_USAGE                 = 0x040B,
  ATTRID_ECB_BR_RATING                = 0x040C,
  ATTRID_ECB_NB_OPERATION_ELECT       = 0x0500,
  ATTRID_ECB_NB_OPERATION_MECHA       = 0x0501,
  ATTRID_ECB_BR_HEALTH_STATUS         = 0x0502,
  ATTRID_ECB_TU_SERVICE_LIFE          = 0x0503,
  ATTRID_ECB_PROTECTION_TYPE          = 0x0600,
  ATTRID_ECB_APPLICATION_TYPE         = 0x0601,
  ATTRID_ECB_TU_ADAPTATION_INFO       = 0x0602,
} ELECTRICAL_CIRCUIT_BREAKER_CLUSTER_ATTRIBUTES;



typedef struct
{
  szl_uint8  of:1;            // OF (Indication contacts)
                            // 0 = Breaker is opened
                            // 1 = Breaker is closed
  szl_uint8  sd:1;            // SD (Trip indication contact)
                            // 0 = No trip
                            // 1 = Breaker has tripped due to electrical fault or Shunt trip
  szl_uint8  sde:1;           // SDE (Fault trip indication contact)
                            // 0 = No trip
                            // 1 = Breaker has tripped due to electrical fault
  szl_uint8  ch:1;            // CH (Charged)
                            // 0 = Spring discharged
                            // 1 = Spring loaded
  szl_uint8  ch_supported:1;  // CH Supported
                            // 0 = False
                            // 1 = True
  szl_uint8  pf:1;            // PF (Ready to close)
                            // 0 = Not ready to close
                            // 1 = Ready to close
  szl_uint8  pf_supported:1;  // PF Supported
                            // 0 = False
                            // 1 = True
  szl_uint8  reserved:1;      // Reserved - must be set to zero
} ECB_BR_STATUS_t;

typedef struct
{
  szl_uint8 breaker_opened:1;
  szl_uint8 breaker_closed:1;
  szl_uint8 breaker_read_to_close:1;
  szl_uint8 breaker_spring_loaded:1;
  szl_uint8 breaker_tripped_sd:1;
  szl_uint8 breaker_tripped_sde:1;
  szl_uint8 reserved:2;      // Reserved - must be set to zero
} ECB_BR_STATUS_ALARM_t;

typedef struct
{
  szl_uint16 long_time_protection:1;
  szl_uint16 short_time_protection:1;
  szl_uint16 instantaneous_protection:1;
  szl_uint16 ground_fault_protection:1;
  szl_uint16 earth_leakage_protection:1;
  szl_uint16 din_protection:1;
  szl_uint16 aaf_protection:1;
  szl_uint16 reserved:8;
  szl_uint16 not_available:1;
} ECB_BR_TRIP_BASIC_ALARM_t;

typedef struct
{
  szl_uint16 overcurrent_90_ir:1;
  szl_uint16 overcurrent_105_ir:1;
  szl_uint16 reserved:13;
  szl_uint16 not_available:1;
} ECB_BR_SETPOINT_ALARM_t;


typedef struct
{
  szl_uint16 settings_change:1;
  szl_uint16 reserved:14;
  szl_uint16 not_available:1;
} ECB_BR_SETTINGS_ALARM_t;

typedef szl_uint8 ECB_BR_INSTALLATION_t;
typedef enum
{
  ECB_BR_INSTALLATION_DRAW_OUT  = 0x00,
  ECB_BR_INSTALLATION_FIXED     = 0x01,
} ENUM_ECB_BR_INSTALLATION_t;

typedef szl_uint8 ECB_BR_HEALTH_STATUS_t;
typedef enum
{
  ECB_BR_HEALTH_STATUS_GREEN    = 0x00,
  ECB_BR_HEALTH_STATUS_ORANGE   = 0x01,
  ECB_BR_HEALTH_STATUS_RED      = 0x02,
} ENUM_ECB_BR_HEALTH_STATUS_t;

typedef szl_uint16 ECB_PROTECTION_TYPE_t;
typedef enum
{
  ECB_PROTECTION_TYPE_DEFAULT     = 0x00,
} ENUM_ECB_PROTECTION_TYPE_t;

typedef szl_uint16 ECB_APPLICATION_TYPE_t;
typedef enum
{
  ECB_APPLCIATION_TYPE_DEFAULT    = 0x00,
} ENUM_ECB_APPLICATION_TYPE_t;

/* Attribute Data storage
 * An instance of this is created per endpoint.
 * Applications may access it via SzlPlugin_ElecMeasCluster_GetAttributePointer() */
typedef struct
{
  ECB_BR_STATUS_t BrStatus;
  ECB_BR_STATUS_ALARM_t BrStatusAlarm;
  ECB_BR_STATUS_ALARM_t BrStatusAlarmMask;
  ECB_BR_TRIP_BASIC_ALARM_t BrTripBasicAlarm;
  ECB_BR_TRIP_BASIC_ALARM_t BrTripBasicAlarmMask;
  ECB_BR_SETPOINT_ALARM_t BrSetpointAlarm;
  ECB_BR_SETPOINT_ALARM_t BrSetpointAlarmMask;
  szl_uint16 NominalCurrent;
  ECB_BR_SETTINGS_ALARM_t BrSettingsAlarm;
  ECB_BR_SETTINGS_ALARM_t BrSettingsAlarmMask;
  szl_uint16 BrIdentifier;
  szl_uint8 BrType;
  char BrReference[ECB_M_REFERENCE_LENGTH+1];
  szl_uint8 BrFamily;
  char BrRange[ECB_M_RANGE_LENGTH+1];
  char BrStandard[ECB_M_STANDARD_LENGTH+1];
  char BrSerialNumber[ECB_M_SERIAL_NUMBER_LENGTH+1];
  char BrPerformance[ECB_M_PERFORMANCE_LENGTH+1];
  szl_uint8 BrNbPoles;
  ECB_BR_INSTALLATION_t BrInstallation;
  char BrHWVersion[ECB_M_HARDWARE_VERSION_LENGTH+1];
  char BrUsage[ECB_M_USAGE_LENGTH+1];
  szl_uint8 BrRating;  // Type TBC
  szl_uint16 NbOperationElect;
  szl_uint16 NbOperationMecha;
  ECB_BR_HEALTH_STATUS_t BrHealthStatus;
  szl_uint8 TuServiceLife;
  ECB_PROTECTION_TYPE_t ProtectionType;
  ECB_APPLICATION_TYPE_t EcbApplicationType;
  char TuAdaptationInfo[ECB_M_TU_ADAPTATION_INFO_LENGTH+1];
}SzlPlugin_ECB_Attributes_t;


/* Attribute Write Notification Handler format */
#ifdef ECB_M_USE_DIRECT_ACCESS
typedef void (*SzlPlugin_ECB_AttributeWriteNotification_CB_t) (zabService * Service, 
                                                               szl_uint8 Endpoint, 
                                                               szl_uint16 AttributeID);
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Plugin Initialisation
 * This function must be called from the application after the Library has been initialized.
 * 
 * Parameters:
 *  Service: Instance of library
 *  numEndpoints: The length of endpointList
 *  endpointList: The endpoints to be initialised with this cluster
 *  WriteAttributesCallback: Callback will be called to notify application that a writable attribute has been written from ZigBee.
 *                           It is optional. If the app does not want to be notified this may be set to NULL.
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_EcbCluster_Init(zabService* Service, 
                                       szl_uint8 numEndpoints, 
                                       szl_uint8 * endpointList, 
#ifdef ECB_M_USE_DIRECT_ACCESS
                                       SzlPlugin_ECB_AttributeWriteNotification_CB_t WriteAttributesCallback
#else                                           
                                       SZL_CB_DataPointRead_t ReadCallback,
                                       SZL_CB_DataPointWrite_t WriteCallback
#endif
                                           );

/******************************************************************************
 * Plugin Destruction
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_EcbCluster_Destroy(zabService* Service);

/******************************************************************************
 * Get pointer to Electrical Measurement Cluster Attributes for an endpoint
 * This can be used to get access to the endpoints parameters for the local 
 *  application to read or write them
 * Return NULL if not found
 ******************************************************************************/
#ifdef ECB_M_USE_DIRECT_ACCESS
extern
SzlPlugin_ECB_Attributes_t* SzlPlugin_EcbCluster_GetAttributePointer(zabService* Service, szl_uint8 endpoint);
#endif

/******************************************************************************
 * Configure reporting for the cluster.
 * Data must be initialised before this function is called to avoid reporting uninitialised values
 * 
 * This function currently only configures a time based default report.
 * If report on change is required the plugin must be extended.
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_EcbCluster_ConfigureReporting(zabService* Service);

/******************************************************************************
 * Disable reporting for the cluster
 ******************************************************************************/
extern 
SZL_RESULT_t SzlPlugin_EcbCluster_DisableReporting(zabService* Service);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /*_ELECTRICAL_CIRCUIT_BREAKER_CLUSTER_H_*/
