/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the ZNP AF Functions
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev     Date     Author  Change Description
 *****************************************************************************/

#ifndef _ZAB_ZNP_AF_UTILITY_H_
#define _ZAB_ZNP_AF_UTILITY_H_




#ifdef __cplusplus
extern "C" {
#endif

  
typedef enum
{
     /****************/
     /* AF Interface */
     /****************/

     ZNPI_CMD_AF_REGISTER                = 0x00, /* AF_REGISTER */
     ZNPI_CMD_AF_DATA_REQUEST            = 0x01, /* AF_DATA_REQUEST */
     ZNPI_CMD_AF_DATA_REQUEST_EXT        = 0x02, /* AF_DATA_REQUEST_EXT */
     ZNPI_CMD_AF_DATA_REQUEST_SRC_RTG    = 0x03, /* AF_DATA_REQUEST_SRC_RTG */
     ZNPI_CMD_AF_DELETE                  = 0x04,

     ZNPI_CMD_AF_INTER_PAN_CTL           = 0x10, /* AF_INTER_PAN_CTL */
     ZNPI_CMD_AF_DATA_STORE              = 0x11, /* AF_DATA_STORE */
     ZNPI_CMD_AF_DATA_RETRIEVE           = 0x12, /* AF_DATA_RETRIEVE */

     ZNPI_CMD_AF_DATA_CONFIRM            = 0x80, /* AF_DATA_CONFIRM */
     ZNPI_CMD_AF_INCOMING_MSG            = 0x81, /* AF_INCOMING_MSG */
     ZNPI_CMD_AF_INCOMING_MSG_EXT        = 0x82, /* AF_INCOMING_MSG */

} ZNPI_CMD_AF_COMMANDCODES;  
  

/**
 * process All Message of subsystem AF
 */
extern void zabZnpAfUtility_ProcessMsg(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * Data Request - Extended
 * This sends a ZigBee data request, as used for ZCL commands.
 * Extedned function is used to support unicast, groupcast, broadcast, ieee and bind addressing
 ******************************************************************************/
extern void zabZnpAfUtility_DataRequestExt(erStatus* Status, zabService* Service, sapMsg *appMsg);

/******************************************************************************
 * Register an endpoint in ZNP
 ******************************************************************************/
extern 
void zabZnpAfUtility_RegisterEndpoint(erStatus* Status, zabService* Service, sapMsg *appMsg);

/******************************************************************************
 * Delete an endpoint in ZNP
 ******************************************************************************/
#ifdef M_ENABLE_AF_DELETE // This function is not (yet) used
extern 
void zabZnpAfUtility_DeleteEndpoint(erStatus* Status, zabService* Service, sapMsg *appMsg);
#endif

#ifdef __cplusplus
}
#endif



#endif /* _ZAB_ZNP_AF_UTILITY_H_ */
