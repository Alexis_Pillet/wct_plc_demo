const detail_iact = ({
	name,
	status,
	id_wireless_bridge,
	address,
	type_device,
	manufacturer_name,
	model_identifier,
	application_firmware_version,
	application_hardware_version,
	product_serial_number,
	product_identifier,
	product_model,
	rssi,
	lqi
}) => `
    <div>
    <p id="title_device">Wireless Bridge ${id_wireless_bridge + 1} - ${name}</p>
    <section class="box grid">
        <div class="row_1 column_1">
          <p id="title_data">Status: <span class="text-green">${status}</<span></p>
        </div>
        <div class="row_1 column_2">
          <p id="title_data">Manufacturer Name: <span class="text-green">${manufacturer_name}</<span></p>
          <p id="title_data">Model Identifier: <span class="text-green">${model_identifier}</<span></p>
          <p id="title_data">Application Firmware Version: <span class="text-green">${application_firmware_version}</<span></p>
          <p id="title_data">Application Hardware Version: <span class="text-green">${application_hardware_version}</<span></p>
        </div>
        <div class="row_1 column_3">
          <p id="title_data">Product Serial Number: <span class="text-green">${product_serial_number}</<span></p>
          <p id="title_data">Product Identifier: <span class="text-green">${product_identifier}</<span></p>
          <p id="title_data">Product Model: <span class="text-green">${product_model}</<span></p>
        </div>
        <div class="row_2 column_3 margin_top_1">
          <p id="title_detail">Address: <span class="text-green">${address}</<span></p>
          <p id="title_detail">RSSI: <span class="text-green">${rssi} dBm</<span></p>
          <p id="title_detail">LQI: <span class="text-green">${lqi}</<span></p>
        </div>
        <div class="row_3 column_3">
          <iframe name="votar" style="display:none;"></iframe>
          <div class="div_multiple_button">
            <form id="iact_discovery" action="/WirelessBridge" method="post" target="votar">
                <input id='button_discovery_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="DISCOVERY">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="discovery">
            </form>
          </div>
          <div class="div_multiple_button">
            <form id="iact_action_open" action="/WirelessBridge" method="post" target="votar">
                <input id='button_open_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="OPEN">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="open">
            </form>
          </div>
          <div class="div_multiple_button">
            <form id="iact_action_close" action="/WirelessBridge" method="post" target="votar">
                <input id='button_close_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="CLOSE">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="close">
            </form>
          </div>
        </div>
    </section>
    </div>`;

const detail_temp = ({
	name,
	id_wireless_bridge,
	address,
	type_device,
	co2,
	temperature,
	humidity,
	manufacturer_name,
	model_identifier,
	application_firmware_version,
	application_hardware_version,
	product_serial_number,
	product_identifier,
	product_model,
	rssi,
	lqi
}) => `
    <div>
    <p id="title_device">Wireless Bridge ${id_wireless_bridge + 1} - ${name}</p>
    <section class="box grid">
        <div class="row_1 column_1">
          <p id="title_data">CO2: <span class="text-green">${co2} PPM</<span></p>
          <p id="title_data">Temperature: <span class="text-green">${temperature} °C</<span></p>
          <p id="title_data">Humidity: <span class="text-green">${humidity} %RH</<span></p>
        </div>
        <div class="row_1 column_2">
          <p id="title_data">Manufacturer Name: <span class="text-green">${manufacturer_name}</<span></p>
          <p id="title_data">Model Identifier: <span class="text-green">${model_identifier}</<span></p>
          <p id="title_data">Application Firmware Version: <span class="text-green">${application_firmware_version}</<span></p>
          <p id="title_data">Application Hardware Version: <span class="text-green">${application_hardware_version}</<span></p>
        </div>
        <div class="row_1 column_3">
          <p id="title_data">Product Serial Number: <span class="text-green">${product_serial_number}</<span></p>
          <p id="title_data">Product Identifier: <span class="text-green">${product_identifier}</<span></p>
          <p id="title_data">Product Model: <span class="text-green">${product_model}</<span></p>
        </div>
        <div class="row_2 column_3 margin_top_1">
          <p id="title_detail">Address: <span class="text-green">${address}</<span></p>
          <p id="title_detail">RSSI: <span class="text-green">${rssi} dBm</<span></p>
          <p id="title_detail">LQI: <span class="text-green">${lqi}</<span></p>
        </div>
        <div class="row_3 column_3_no_size">
          <iframe name="votar" style="display:none;"></iframe>
          <form id="co2_discovery" action="/WirelessBridge" method="post" target="votar">
              <input id='button_discovery_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="DISCOVERY">
              <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
              <input type="hidden" name="address" value="${address}">
              <input type="hidden" name="type_device" value="${type_device}">
              <input type="hidden" name="name_function" value="discovery">
          </form>
        </div>
    </section>
    </div>`;

const detail_power_tag_1p = ({
	name,
	status,
	id_wireless_bridge,
	address,
	type_device,
	manufacturer_name,
	model_identifier,
	application_firmware_version,
	application_hardware_version,
	product_serial_number,
	product_identifier,
	product_model,
	rssi,
	lqi,
	voltage_pha,
	current_pha,
	cumulated_energy,
	partial_energy,
	phase_sequence,
	ac_alarms_mask,
	alarms_mask
}) => `
    <div>
    <p id="title_device">Wireless Bridge ${id_wireless_bridge + 1} - ${name}</p>
    <section class="box grid">
        <div class="row_1 column_1">
          <p id="title_data">Voltage Pha: <span class="text-green">${voltage_pha} V</<span></p>
        </div>

        <div class="row_1 column_2">
          <p id="title_data">Current Pha: <span class="text-green">${current_pha} A</<span></p>
        </div>

        <div class="row_2 column_1 margin_top_1">
          <p id="title_data">Cumulated Energy: <span class="text-green">${cumulated_energy} kWh</<span></p>
          <p id="title_data">Partial Energy: <span class="text-green">${partial_energy} kWh</<span></p>
          <p id="title_data">Phase Sequence: <span class="text-green">${phase_sequence}</<span></p>
          <p id="title_data">AC Alarms Mask: <span class="text-green">${ac_alarms_mask}</<span></p>
          <p id="title_data">Alarms Mask: <span class="text-green">${alarms_mask}</<span></p>
        </div>

        <div class="row_2 column_2 margin_top_1">
          <p id="title_data">Manufacturer Name: <span class="text-green">${manufacturer_name}</<span></p>
          <p id="title_data">Model Identifier: <span class="text-green">${model_identifier}</<span></p>
          <p id="title_data">Application Firmware Version: <span class="text-green">${application_firmware_version}</<span></p>
          <p id="title_data">Application Hardware Version: <span class="text-green">${application_hardware_version}</<span></p>
        </div>

        <div class="row_2 column_3 margin_top_1">
          <p id="title_data">Product Serial Number: <span class="text-green">${product_serial_number}</<span></p>
          <p id="title_data">Product Identifier: <span class="text-green">${product_identifier}</<span></p>
          <p id="title_data">Product Model: <span class="text-green">${product_model}</<span></p>
        </div>

        <div class="row_3 column_3">
          <p id="title_detail">Address: <span class="text-green">${address}</<span></p>
          <p id="title_detail">RSSI: <span class="text-green">${rssi} dBm</<span></p>
          <p id="title_detail">LQI: <span class="text-green">${lqi}</<span></p>
        </div>

				<div class="row_4 column_3">
          <iframe name="votar" style="display:none;"></iframe>
          <div class="div_multiple_button">
            <form id="power_tag_discovery" action="/WirelessBridge" method="post" target="votar">
                <input id='button_discovery_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="DISCOVERY">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="discovery">
            </form>
          </div>
          <div class="div_multiple_button">
            <form id="power_tag_reset" action="/WirelessBridge" method="post" target="votar">
                <label for="power_tag_reset">Partial Energy:</label>
                <input id='button_reset_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="RESET">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="reset_partial_energy">
            </form>
          </div>
        </div>

        <div class="row_5 column_3">
          <iframe name="votar_2" style="display:none;"></iframe>
          <div class="div_multiple_button">
            <form id="get_phase_sequence" action="/WirelessBridge" method="post" target="votar_2">
                <label for="phase_sequence">Phase sequence:</label>
                <input id='button_get_phase_sequence_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="GET">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="get_phase_sequence">
            </form>
          </div>
    		</div>

				<div class="row_6 column_3">
					<iframe name="votar_2" style="display:none;"></iframe>
					<div class="div_multiple_button">
					<label for="phase_sequence">Phase sequence:</label>
					<select name="phase_sequence_value" onchange="save_value_phase_sequence(this)" style="margin-top: 2.0rem;margin-right: 1.0rem;" id="phase_sequence_${id_wireless_bridge}_${address}" form="set_phase_sequence">
							<option value="0">PhA, PhB, PhC</option>
							<option value="1">PhB, PhA, PhC</option>
							<option value="2">PhC, PhA, PhB</option>
							<option value="3">PhA, PhC, PhB</option>
							<option value="4">PhB, PhC, PhA</option>
							<option value="5">PhC, PhB, PhA</option>
					</select>
						<form id="set_phase_sequence" action="/WirelessBridge" method="post" target="votar_2">
								<input id='button_set_phase_sequence_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="SET">
								<input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
								<input type="hidden" name="address" value="${address}">
								<input type="hidden" name="type_device" value="${type_device}">
								<input type="hidden" name="name_function" value="set_phase_sequence">
						</form>
					</div>
				</div>
    </section>
    </div>`;

const detail_power_tag_3p = ({
	name,
	status,
	id_wireless_bridge,
	address,
	type_device,
	manufacturer_name,
	model_identifier,
	application_firmware_version,
	application_hardware_version,
	product_serial_number,
	product_identifier,
	product_model,
	rssi,
	lqi,
	voltage_pha_phb,
	voltage_phb_phc,
	voltage_phc_pha,
	current_pha,
	current_phb,
	current_phc,
	active_power_pha,
	active_power_phb,
	active_power_phc,
	total_active_power,
	cumulated_energy,
	partial_energy,
	phase_sequence,
	ac_alarms_mask,
	alarms_mask
}) => `
    <div>
    <p id="title_device">Wireless Bridge ${id_wireless_bridge + 1} - ${name}</p>
    <section class="box grid">
        <div class="row_1 column_1">
          <p id="title_data">Voltage Pha-Phb: <span class="text-green">${voltage_pha_phb} V</<span></p>
          <p id="title_data">Voltage Phb-Phc: <span class="text-green">${voltage_phb_phc} V</<span></p>
          <p id="title_data">Voltage Phc-Pha: <span class="text-green">${voltage_phc_pha} V</<span></p>
        </div>

        <div class="row_1 column_2">
          <p id="title_data">Current Pha: <span class="text-green">${current_pha} A</<span></p>
          <p id="title_data">Current Phb: <span class="text-green">${current_phb} A</<span></p>
          <p id="title_data">Current Phc: <span class="text-green">${current_phc} A</<span></p>
        </div>

        <div class="row_1 column_3">
          <p id="title_data">Active Power Pha: <span class="text-green">${active_power_pha} W</<span></p>
          <p id="title_data">Active Power Phb: <span class="text-green">${active_power_phb} W</<span></p>
          <p id="title_data">Active Power Phc: <span class="text-green">${active_power_phc} W</<span></p>
          <p id="title_data">Total Active Power: <span class="text-green">${total_active_power} kW</<span></p>
        </div>

        <div class="row_2 column_1 margin_top_1">
          <p id="title_data">Cumulated Energy: <span class="text-green">${cumulated_energy} kWh</<span></p>
          <p id="title_data">Partial Energy: <span class="text-green">${partial_energy} kWh</<span></p>
          <p id="title_data">Phase Sequence: <span class="text-green">${phase_sequence}</<span></p>
          <p id="title_data">AC Alarms Mask: <span class="text-green">${ac_alarms_mask}</<span></p>
          <p id="title_data">Alarms Mask: <span class="text-green">${alarms_mask}</<span></p>
        </div>

        <div class="row_2 column_2 margin_top_1">
          <p id="title_data">Manufacturer Name: <span class="text-green">${manufacturer_name}</<span></p>
          <p id="title_data">Model Identifier: <span class="text-green">${model_identifier}</<span></p>
          <p id="title_data">Application Firmware Version: <span class="text-green">${application_firmware_version}</<span></p>
          <p id="title_data">Application Hardware Version: <span class="text-green">${application_hardware_version}</<span></p>
        </div>

        <div class="row_2 column_3 margin_top_1">
          <p id="title_data">Product Serial Number: <span class="text-green">${product_serial_number}</<span></p>
          <p id="title_data">Product Identifier: <span class="text-green">${product_identifier}</<span></p>
          <p id="title_data">Product Model: <span class="text-green">${product_model}</<span></p>
        </div>

        <div class="row_3 column_3">
          <p id="title_detail">Address: <span class="text-green">${address}</<span></p>
          <p id="title_detail">RSSI: <span class="text-green">${rssi} dBm</<span></p>
          <p id="title_detail">LQI: <span class="text-green">${lqi}</<span></p>
        </div>

				<div class="row_4 column_3">
          <iframe name="votar" style="display:none;"></iframe>
          <div class="div_multiple_button">
            <form id="power_tag_discovery" action="/WirelessBridge" method="post" target="votar">
                <input id='button_discovery_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="DISCOVERY">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="discovery">
            </form>
          </div>
          <div class="div_multiple_button">
            <form id="power_tag_reset" action="/WirelessBridge" method="post" target="votar">
                <label for="power_tag_reset">Partial Energy:</label>
                <input id='button_reset_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="RESET">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="reset_partial_energy">
            </form>
          </div>
        </div>

        <div class="row_5 column_3">
          <iframe name="votar_2" style="display:none;"></iframe>
          <div class="div_multiple_button">
            <form id="get_phase_sequence" action="/WirelessBridge" method="post" target="votar_2">
                <label for="phase_sequence">Phase sequence:</label>
                <input id='button_get_phase_sequence_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="GET">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="get_phase_sequence">
            </form>
          </div>
    		</div>

				<div class="row_6 column_3">
					<iframe name="votar_2" style="display:none;"></iframe>
					<div class="div_multiple_button">
					<label for="phase_sequence">Phase sequence:</label>
					<select name="phase_sequence_value" onchange="save_value_phase_sequence(this)" style="margin-top: 2.0rem;margin-right: 1.0rem;" id="phase_sequence_${id_wireless_bridge}_${address}" form="set_phase_sequence">
							<option value="0">PhA, PhB, PhC</option>
							<option value="1">PhB, PhA, PhC</option>
							<option value="2">PhC, PhA, PhB</option>
							<option value="3">PhA, PhC, PhB</option>
							<option value="4">PhB, PhC, PhA</option>
							<option value="5">PhC, PhB, PhA</option>
					</select>
						<form id="set_phase_sequence" action="/WirelessBridge" method="post" target="votar_2">
								<input id='button_set_phase_sequence_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="SET">
								<input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
								<input type="hidden" name="address" value="${address}">
								<input type="hidden" name="type_device" value="${type_device}">
								<input type="hidden" name="name_function" value="set_phase_sequence">
						</form>
					</div>
				</div>
    </section>
    </div>`;

const detail_power_tag_3pn = ({
	name,
	status,
	id_wireless_bridge,
	address,
	type_device,
	manufacturer_name,
	model_identifier,
	application_firmware_version,
	application_hardware_version,
	product_serial_number,
	product_identifier,
	product_model,
	rssi,
	lqi,
	voltage_pha,
	voltage_phb,
	voltage_phc,
	current_pha,
	current_phb,
	current_phc,
	active_power_pha,
	active_power_phb,
	active_power_phc,
	total_active_power,
	cumulated_energy,
	partial_energy,
	phase_sequence,
	ac_alarms_mask,
	alarms_mask
}) => `
    <div>
    <p id="title_device">Wireless Bridge ${id_wireless_bridge + 1} - ${name}</p>
    <section class="box grid">
        <div class="row_1 column_1">
          <p id="title_data">Voltage Pha: <span class="text-green">${voltage_pha} V</<span></p>
          <p id="title_data">Voltage Phb: <span class="text-green">${voltage_phb} V</<span></p>
          <p id="title_data">Voltage Phc: <span class="text-green">${voltage_phc} V</<span></p>
        </div>

        <div class="row_1 column_2">
          <p id="title_data">Current Pha: <span class="text-green">${current_pha} A</<span></p>
          <p id="title_data">Current Phb: <span class="text-green">${current_phb} A</<span></p>
          <p id="title_data">Current Phc: <span class="text-green">${current_phc} A</<span></p>
        </div>

        <div class="row_1 column_3">
          <p id="title_data">Active Power Pha: <span class="text-green">${active_power_pha} W</<span></p>
          <p id="title_data">Active Power Phb: <span class="text-green">${active_power_phb} W</<span></p>
          <p id="title_data">Active Power Phc: <span class="text-green">${active_power_phc} W</<span></p>
          <p id="title_data">Total Active Power: <span class="text-green">${total_active_power} kW</<span></p>
        </div>

        <div class="row_2 column_1 margin_top_1">
          <p id="title_data">Cumulated Energy: <span class="text-green">${cumulated_energy} kWh</<span></p>
          <p id="title_data">Partial Energy: <span class="text-green">${partial_energy} kWh</<span></p>
          <p id="title_data">Phase Sequence: <span class="text-green">${phase_sequence}</<span></p>
          <p id="title_data">AC Alarms Mask: <span class="text-green">${ac_alarms_mask}</<span></p>
          <p id="title_data">Alarms Mask: <span class="text-green">${alarms_mask}</<span></p>
        </div>

        <div class="row_2 column_2 margin_top_1">
          <p id="title_data">Manufacturer Name: <span class="text-green">${manufacturer_name}</<span></p>
          <p id="title_data">Model Identifier: <span class="text-green">${model_identifier}</<span></p>
          <p id="title_data">Application Firmware Version: <span class="text-green">${application_firmware_version}</<span></p>
          <p id="title_data">Application Hardware Version: <span class="text-green">${application_hardware_version}</<span></p>
        </div>

        <div class="row_2 column_3 margin_top_1">
          <p id="title_data">Product Serial Number: <span class="text-green">${product_serial_number}</<span></p>
          <p id="title_data">Product Identifier: <span class="text-green">${product_identifier}</<span></p>
          <p id="title_data">Product Model: <span class="text-green">${product_model}</<span></p>
        </div>

        <div class="row_3 column_3">
          <p id="title_detail">Address: <span class="text-green">${address}</<span></p>
          <p id="title_detail">RSSI: <span class="text-green">${rssi} dBm</<span></p>
          <p id="title_detail">LQI: <span class="text-green">${lqi}</<span></p>
        </div>

        <div class="row_4 column_3">
          <iframe name="votar" style="display:none;"></iframe>
          <div class="div_multiple_button">
            <form id="power_tag_discovery" action="/WirelessBridge" method="post" target="votar">
                <input id='button_discovery_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="DISCOVERY">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="discovery">
            </form>
          </div>
          <div class="div_multiple_button">
            <form id="power_tag_reset" action="/WirelessBridge" method="post" target="votar">
                <label for="power_tag_reset">Partial Energy:</label>
                <input id='button_reset_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="RESET">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="reset_partial_energy">
            </form>
          </div>
        </div>

        <div class="row_5 column_3">
          <iframe name="votar_2" style="display:none;"></iframe>
          <div class="div_multiple_button">
            <form id="get_phase_sequence" action="/WirelessBridge" method="post" target="votar_2">
                <label for="phase_sequence">Phase sequence:</label>
                <input id='button_get_phase_sequence_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="GET">
                <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
                <input type="hidden" name="address" value="${address}">
                <input type="hidden" name="type_device" value="${type_device}">
                <input type="hidden" name="name_function" value="get_phase_sequence">
            </form>
          </div>
    		</div>

				<div class="row_6 column_3">
					<iframe name="votar_2" style="display:none;"></iframe>
					<div class="div_multiple_button">
					<label for="phase_sequence">Phase sequence:</label>
					<select name="phase_sequence_value" onchange="save_value_phase_sequence(this)" style="margin-top: 2.0rem;margin-right: 1.0rem;" id="phase_sequence_${id_wireless_bridge}_${address}" form="set_phase_sequence">
							<option value="0">PhA, PhB, PhC</option>
							<option value="1">PhB, PhA, PhC</option>
							<option value="2">PhC, PhA, PhB</option>
							<option value="3">PhA, PhC, PhB</option>
							<option value="4">PhB, PhC, PhA</option>
							<option value="5">PhC, PhB, PhA</option>
					</select>
						<form id="set_phase_sequence" action="/WirelessBridge" method="post" target="votar_2">
								<input id='button_set_phase_sequence_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="SET">
								<input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
								<input type="hidden" name="address" value="${address}">
								<input type="hidden" name="type_device" value="${type_device}">
								<input type="hidden" name="name_function" value="set_phase_sequence">
						</form>
					</div>
				</div>

    </section>
    </div>`;

data_product_iact = [];
data_product_temp = [];
data_product_power_tag_1p = [];
data_product_power_tag_3p = [];
data_product_power_tag_3pn = [];

function save_value_phase_sequence(selectObject) {
			// Save new data
			localStorage.setItem(selectObject.id, selectObject.selectedIndex);
}


var refresh_data = function (json, reference_node) {

	selected_device = null;
	selected_wireless_bridge = null;
	// Looking for the selected device
	json.forEach(function (wireless_bridge) {
		wireless_bridge.children.forEach(function (device) {

			if (device.id == reference_node) {
				selected_device = device;
				selected_wireless_bridge = wireless_bridge;
			}

		});
	});

	if ((selected_device != null) && (selected_wireless_bridge != null)) {

		// Update data according to the type of the device
		if (selected_device.type == 'iact') {

			data_product_iact.push({
				name: selected_device.text,
				type_device: selected_device.type,

				manufacturer_name: selected_device.data.product.manufacturer_name,
				model_identifier: selected_device.data.product.model_identifier,
				application_firmware_version: selected_device.data.product.application_firmware_version,
				application_hardware_version: selected_device.data.product.application_hardware_version,
				product_serial_number: selected_device.data.product.product_serial_number,
				product_identifier: selected_device.data.product.product_identifier,
				product_model: selected_device.data.product.product_model,

				rssi: selected_device.data.product.rssi,
				lqi: selected_device.data.product.lqi,

				status: selected_device.data.status,

				id_wireless_bridge: selected_wireless_bridge.data.device_number,
				address: selected_device.data.product.address,
			})
		}

		if (selected_device.type == 'co2_multisensor') {
			data_product_temp.push({
				name: selected_device.text,
				type_device: selected_device.type,

				manufacturer_name: selected_device.data.product.manufacturer_name,
				model_identifier: selected_device.data.product.model_identifier,
				application_firmware_version: selected_device.data.product.application_firmware_version,
				application_hardware_version: selected_device.data.product.application_hardware_version,
				product_serial_number: selected_device.data.product.product_serial_number,
				product_identifier: selected_device.data.product.product_identifier,
				product_model: selected_device.data.product.product_model,

				rssi: selected_device.data.product.rssi,
				lqi: selected_device.data.product.lqi,

				co2: selected_device.data.co2,
				temperature: selected_device.data.temperature,
				humidity: selected_device.data.relative_humidity,

				id_wireless_bridge: selected_wireless_bridge.data.device_number,
				address: selected_device.data.product.address,
			})
		}

		if (selected_device.type == 'power_tag_1p') {
			data_product_power_tag_1p.push({
				name: selected_device.text,
				type_device: selected_device.type,

				manufacturer_name: selected_device.data.product.manufacturer_name,
				model_identifier: selected_device.data.product.model_identifier,
				application_firmware_version: selected_device.data.product.application_firmware_version,
				application_hardware_version: selected_device.data.product.application_hardware_version,
				product_serial_number: selected_device.data.product.product_serial_number,
				product_identifier: selected_device.data.product.product_identifier,
				product_model: selected_device.data.product.product_model,

				rssi: selected_device.data.product.rssi,
				lqi: selected_device.data.product.lqi,

				voltage_pha: selected_device.data.electrical_measurement.voltage_pha,
				current_pha: selected_device.data.electrical_measurement.current_pha,

				cumulated_energy: selected_device.data.metering.cumulated_energy,
				partial_energy: selected_device.data.metering.partial_energy,
				phase_sequence: selected_device.data.electrical_measurement.phase_sequence,
				ac_alarms_mask: selected_device.data.electrical_measurement.ac_alarms_mask,
				alarms_mask: selected_device.data.electrical_measurement.alarms_mask,

				id_wireless_bridge: selected_wireless_bridge.data.device_number,
				address: selected_device.data.product.address,
			})
		}

		if (selected_device.type == 'power_tag_3p') {
			data_product_power_tag_3p.push({
				name: selected_device.text,
				type_device: selected_device.type,

				manufacturer_name: selected_device.data.product.manufacturer_name,
				model_identifier: selected_device.data.product.model_identifier,
				application_firmware_version: selected_device.data.product.application_firmware_version,
				application_hardware_version: selected_device.data.product.application_hardware_version,
				product_serial_number: selected_device.data.product.product_serial_number,
				product_identifier: selected_device.data.product.product_identifier,
				product_model: selected_device.data.product.product_model,

				rssi: selected_device.data.product.rssi,
				lqi: selected_device.data.product.lqi,

				voltage_pha_phb: selected_device.data.electrical_measurement.voltage_pha_phb,
				voltage_phb_phc: selected_device.data.electrical_measurement.voltage_phb_phc,
				voltage_phc_pha: selected_device.data.electrical_measurement.voltage_phc_pha,

				current_pha: selected_device.data.electrical_measurement.current_pha,
				current_phb: selected_device.data.electrical_measurement.current_phb,
				current_phc: selected_device.data.electrical_measurement.current_phc,

				active_power_pha: selected_device.data.electrical_measurement.active_power_pha,
				active_power_phb: selected_device.data.electrical_measurement.active_power_phb,
				active_power_phc: selected_device.data.electrical_measurement.active_power_phc,
				total_active_power: selected_device.data.electrical_measurement.total_active_power,

				cumulated_energy: selected_device.data.metering.cumulated_energy,
				partial_energy: selected_device.data.metering.partial_energy,
				phase_sequence: selected_device.data.electrical_measurement.phase_sequence,
				ac_alarms_mask: selected_device.data.electrical_measurement.ac_alarms_mask,
				alarms_mask: selected_device.data.electrical_measurement.alarms_mask,

				id_wireless_bridge: selected_wireless_bridge.data.device_number,
				address: selected_device.data.product.address,
			})
		}

		if (selected_device.type == 'power_tag_3pn') {
			data_product_power_tag_3pn.push({
				name: selected_device.text,
				type_device: selected_device.type,

				manufacturer_name: selected_device.data.product.manufacturer_name,
				model_identifier: selected_device.data.product.model_identifier,
				application_firmware_version: selected_device.data.product.application_firmware_version,
				application_hardware_version: selected_device.data.product.application_hardware_version,
				product_serial_number: selected_device.data.product.product_serial_number,
				product_identifier: selected_device.data.product.product_identifier,
				product_model: selected_device.data.product.product_model,

				rssi: selected_device.data.product.rssi,
				lqi: selected_device.data.product.lqi,

				voltage_pha: selected_device.data.electrical_measurement.voltage_pha,
				voltage_phb: selected_device.data.electrical_measurement.voltage_phb,
				voltage_phc: selected_device.data.electrical_measurement.voltage_phc,

				current_pha: selected_device.data.electrical_measurement.current_pha,
				current_phb: selected_device.data.electrical_measurement.current_phb,
				current_phc: selected_device.data.electrical_measurement.current_phc,

				active_power_pha: selected_device.data.electrical_measurement.active_power_pha,
				active_power_phb: selected_device.data.electrical_measurement.active_power_phb,
				active_power_phc: selected_device.data.electrical_measurement.active_power_phc,
				total_active_power: selected_device.data.electrical_measurement.total_active_power,

				cumulated_energy: selected_device.data.metering.cumulated_energy,
				partial_energy: selected_device.data.metering.partial_energy,
				phase_sequence: selected_device.data.electrical_measurement.phase_sequence,
				ac_alarms_mask: selected_device.data.electrical_measurement.ac_alarms_mask,
				alarms_mask: selected_device.data.electrical_measurement.alarms_mask,

				id_wireless_bridge: selected_wireless_bridge.data.device_number,
				address: selected_device.data.product.address,
			})
		}
	}
}

var set_on_changed_action = function () {

	$('#jstree_demo_div').on('ready.jstree', function (e, data) {
		if (data.instance.get_node('WB_1')) {
			$('#date').html(data.instance.get_node('WB_1').data.date);
		} else {
			$('#date').html('');
		}
	});

	$('#jstree_demo_div')
	// listen for event
	.on('changed.jstree', function (e, data) {

		// Reset
		data_product_iact = [];
		data_product_temp = [];
		data_product_power_tag_1p = [];
		data_product_power_tag_3p = [];
		data_product_power_tag_3pn = [];

		// Update data for each selected device
		for (i = 0, j = data.selected.length; i < j; i++) {
			refresh_data(JSON.parse(localStorage.getItem("topology-data")), data.selected[i]);
		}

		// Create interface
		$('#detail-product').html(data_product_iact.map(detail_iact).join(''));
		$('#detail-product').append(data_product_temp.map(detail_temp).join(''));
		$('#detail-product').append(data_product_power_tag_1p.map(detail_power_tag_1p).join(''));
		$('#detail-product').append(data_product_power_tag_3p.map(detail_power_tag_3p).join(''));
		$('#detail-product').append(data_product_power_tag_3pn.map(detail_power_tag_3pn).join(''));
	})

}

var load_tree = function () {
	// Load tree for the first time
	var $treeview = $("#jstree_demo_div");
	$.getJSON('./topology.json?nocache=' + (new Date()).getTime(), function (data) {
		$treeview.jstree({
			"state": {
				"key": "123456"
			},
			"checkbox": {
				"keep_selected_style": true
			},
			"state": {
				"key": "zigbeeView"
			},
			"types": {
				"default": {
					"icon": "./images/logo_wb.png"
				},
				"iact": {
					"icon": "./images/logo_iact.png"
				},
				"co2_multisensor": {
					"icon": "./images/logo_temp.png"
				},
				"power_tag_1p": {
					"icon": "./images/logo_power_tag.png"
				},
				"power_tag_3p": {
					"icon": "./images/logo_power_tag.png"
				},
				"power_tag_3pn": {
					"icon": "./images/logo_power_tag.png"
				}

			},
			"plugins": ["checkbox", "sort", "state", "types"],
			"core": {
				"themes": {
					"name": "default",
					"dots": true,
					"icons": true
				},
				"data": data
			}
		});

		// Save topology data
		localStorage.setItem("topology-data", JSON.stringify(data));
	});
};

var reload_data = function ($id) {

	$.getJSON('./topology.json?nocache=' + (new Date()).getTime(), function (data) {

		// Do nothing if the data didn't change
		if (localStorage.getItem("topology-data") != JSON.stringify(data)) {

			// Save new data
			localStorage.setItem("topology-data", JSON.stringify(data));

			// Get Tree
			var jsTree = $('#jstree_demo_div').jstree(true);
			jsTree.save_state();

			// Reset
			data_product_iact = [];
			data_product_temp = [];
			data_product_power_tag_1p = [];
			data_product_power_tag_3p = [];
			data_product_power_tag_3pn = [];

			// Get selected device to update the associated data
			node_checked = jsTree.get_checked();
			node_checked.forEach(function (entry) {
				refresh_data(data, entry);
			});

			if (data[0] != null) {
				$('#date').html(data[0].data.date);
			} else {
				$('#date').html('');
			}

			// Create interface
			$('#detail-product').html(data_product_iact.map(detail_iact).join(''));
			$('#detail-product').append(data_product_temp.map(detail_temp).join(''));
			$('#detail-product').append(data_product_power_tag_1p.map(detail_power_tag_1p).join(''));
			$('#detail-product').append(data_product_power_tag_3p.map(detail_power_tag_3p).join(''));
			$('#detail-product').append(data_product_power_tag_3pn.map(detail_power_tag_3pn).join(''));

			$('select').each(function (select_element) {
					default_option = localStorage.getItem($(this)[0].id)
					if(default_option != null){
					$(this).val(default_option)
					}
			});

		}
	});
};

function load_logs() {
	$.getJSON('./logs.json?nocache=' + (new Date()).getTime(), function (data) {
		display_logs(data);
		// Save logs
		localStorage.setItem("logs-data", JSON.stringify(data));
	});
}

function reload_logs() {
	$.getJSON('./logs.json?nocache=' + (new Date()).getTime(), function (data) {
		// Do nothing if the data didn't change
		if (localStorage.getItem("logs-data") != JSON.stringify(data)) {
			// Save new data
			localStorage.setItem("logs-data", JSON.stringify(data));
			display_logs(data);
		}
	});
}

function display_logs(json_data) {

	// Get div of the logs to display them
	logData = $('#log-data');
	// Clear logs
	logData.empty();

	if (json_data.length != 0) {
		json_data.forEach(function (o, i) {
			logData.append("<span class='text'>" + o.date + " -</span>");
			if (o.type == "INFO") {
				logData.append("<span class='text text-info'>INFO: </span>");
				logData.append("<span class='text text-info'>" + o.message + "<br/></span>");
			} else if (o.type == "ACTION") {
				logData.append("<span class='text text-info'>ACTION: </span>");
				logData.append("<span class='text text-info'>" + o.message + "<br/></span>");
			} else if (o.type == "ERROR") {
				logData.append("<span class='text text-error'>ERROR: </span>");
				logData.append("<span class='text text-error'>" + o.message + "<br/></span>");
			} else if (o.type == "DEBUG") {
				logData.append("<span class='text text-debug'>DEBUG: </span>");
				logData.append("<span class='text text-debug'>" + o.message + "<br/></span>");
			}
		});
	}
}

// Reload data every 2 seconds
setInterval(function () {
	reload_data();
	reload_logs();
}, 2000);

// Load data for the first time
$(function () {
	load_logs();
	load_tree();
	set_on_changed_action();
});
