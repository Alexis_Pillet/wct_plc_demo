/******************************************************************************
 *                          ZAB Test Console
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the application glue for the test console:
 *    - Serial Out ACtion and Data Handlers
 *    - Management In Handler (For notifications)
 *    - Ask and Give Handlers
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.03.00  16-Jan-14   MvdB   Major change to serial action and data handling
 * 00.00.04.00  21-May-14   MvdB   artf58379: Change ask/give Instance from 8 to 32 bits and rename Param1
 * 002.000.007  14-Apr-15   MvdB   ConsoleTest: Add command line function to simulate serial glue errors
 *****************************************************************************/

#ifndef APPZABCOREGLUE_H_
#define APPZABCOREGLUE_H_

#include "zabCoreService.h"


/******************************************************************************
 * Make the next Open action timeout.
 * Used for testing effect of glue failure.
 ******************************************************************************/
extern
void appZabCoreGlue_NextOpenTimeout(void);

/******************************************************************************
 * Make the next Close action timeout.
 * Used for testing effect of glue failure.
 ******************************************************************************/
extern
void appZabCoreGlue_NextCloseTimeout(void);

/******************************************************************************
 * Ask Handler
 * Returns configuration values ASKED from the APP by ZAB
 ******************************************************************************/
extern
void appAskCfg(erStatus* Status, zabService* Service, zabAskId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer);

/******************************************************************************
 * Give Handler
 * Accepts configuration values GIVEN by ZAB to the app
 ******************************************************************************/
extern 
void appGiveCfg(erStatus* Status, zabService* Service, zabGiveId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer);

/******************************************************************************
 * Management SAP Callback.
 * This function is registered to handle notification coming IN from ZAB.
 ******************************************************************************/
extern 
void appZabManageCallBack(erStatus* Status, sapHandle Sap, sapMsg* Message);

/******************************************************************************
 * Serial Out Action Handler
 * This function is registered to handle actions coming out of ZAB to the serial glue.
 ******************************************************************************/
extern 
void appZabCoreGlue_SerialOutActionHandler(erStatus* Status, zabService* Service, zabAction Action);

/******************************************************************************
 * Serial Out Data Handler
 * This function is registered to handle serial data coming out of ZAB to the serial glue.
 ******************************************************************************/
extern 
void appZabCoreGlue_SerialOutDataHandler(erStatus* Status, zabService* Service, unsigned16 DataLength, unsigned8* Data);


/******************************************************************************
 * Open a firmware image so it is ready when pointers and length are asked during the update.
 ******************************************************************************/
extern
zab_bool appZabCoreGlue_OpenFirmwareImage(zabService* Service, char* filename);


#endif /* APPZABCOREGLUE_H_ */
