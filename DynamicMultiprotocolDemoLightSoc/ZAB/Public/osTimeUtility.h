/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the protoypes for time mangement functions in ZAB.
 *   Functions must be implemented by the user in the application.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.02.00  22-Oct-13   MvdB   Tidy up
 * 002.002.013  14-Oct-15   MvdB   ARTF151072: Add service pointer to osTimeGetMilliseconds() and osTimeGetSeconds()
 *****************************************************************************/

#ifndef __OS_TIME_UTILITY_H__
#define __OS_TIME_UTILITY_H__

#include "osTypes.h"

#ifdef __cplusplus
extern "C" {
#endif

  
/******************************************************************************
 *                      ******************************
 *                 *****  EXTERN FUNCTION PROTOTYPES  *****
 *                 ***** TO BE IMPLEMENTED BY THE APP *****
 *                      ******************************
 ******************************************************************************/ 
  
/******************************************************************************
 * Get the time in Milli-Seconds
 * 
 * The start point for this time is not important (it may be system start), it
 * must just increment every milli-second and may roll over.
 ******************************************************************************/
void osTimeGetMilliseconds( zabService* Service, unsigned32* MilliSeconds );

/******************************************************************************
 * Get the time in Seconds
 * 
 * The start point for this time is not important (it may be system start), it
 * must just increment every second and may roll over.
 ******************************************************************************/
void osTimeGetSeconds( zabService* Service, unsigned32* Seconds );


#ifdef __cplusplus
}
#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif

