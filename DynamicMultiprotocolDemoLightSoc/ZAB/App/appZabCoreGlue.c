#include "appZabCoreGlue.h"
#include "appMain.h"
#include "appConfig.h"
#include "appGateway.h"
#include "zabCoreService.h"

#include "app/framework/include/af.h"
/******************************************************************************
 *                      ******************************
 *                 *****       LOCAL VARIABLES        *****
 *                      ******************************
 ******************************************************************************/
 /******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/
 /******************************************************************************
 * Print error info
 ******************************************************************************/
static void UserZabErrorDisplay(erStatus* Status, zabService* Service )
{
  if (erStatusIsOk(Status))
      return;

  printApp(Service, "<<ZAB Error Warning!!!>> <0x%04X> <%s>\n",
            erStatusGetError(Status),
            erStatusUtility_GetErrorString(erStatusGetError(Status)));

  erStatusClear(Status, Service);
}
 /******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/
 /******************************************************************************
 * Management SAP Callback.
 * This function is registered to handle notification coming IN from ZAB.
 ******************************************************************************/
void appZabManageCallBack (erStatus* Status, sapHandle Sap, sapMsg* Message)
{
  printApp(appService,"appZabManageCallBack\n");
    sapMsgType Type = (sapMsgType)0;
  zabNotificationData notifyData;
  zabService* Service = (zabService *)sapCoreGetService( Sap );

  ER_CHECK_NULL(Status, Message);

  Type = sapMsgGetType(Message);

  UserZabErrorDisplay(Status, Service);

  if (Type == SAP_MSG_TYPE_NOTIFY)
    {

      notifyData = sapMsgGetNotifyData(Message);
      switch(sapMsgGetNotifyType(Message))
        {
          case ZAB_NOTIFICATION_OPEN_STATE:
            printApp(Service, "AppNotify: Open State = %s\n",
                      zabUtility_GetOpenStateString(notifyData.openState));

            appGateway_OpenStateNtfHandler(notifyData.openState);
            break;
          case ZAB_NOTIFICATION_NETWORK_STATE:
            printApp(Service, "AppNotify: Network State = %s\n",
                      zabUtility_GetNetworkStateString(notifyData.nwkState));

            appGateway_NwkStateNtfHandler(notifyData.nwkState);
            break;
         case ZAB_NOTIFICATION_NETWORK_PERMIT_JOIN:
            printApp(Service, "AppNotify: Permit Join Time = %d seconds\n",
                      notifyData.nwkPermitJoinTime);
            break;
          case ZAB_NOTIFICATION_ERROR:
            printError(Service, "AppNotify: <<<ERROR 0x%04X>>> %s\n",
                       (unsigned16)notifyData.errorNumber,
                       erStatusUtility_GetErrorString(notifyData.errorNumber));
            break;
          case ZAB_NOTIFICATION_CHANNEL_CHANGE_STATE:
            printApp(Service, "AppNotify: Channel Change State = %s\n",
                      zabUtility_GetChannelChangeStateString(notifyData.channelChangeState));
            break;

          case ZAB_NOTIFICATION_NETWORK_KEY_CHANGE_STATE:
            printApp(Service, "AppNotify: Network Key Change State = %s\n",
                      zabUtility_GetNetworkKeyChangeStateString(notifyData.nwkKeyChangeState));
            break;   
          case ZAB_NOTIFICATION_NETWORK_INFO_STATE:
            printApp(Service, "AppNotify: Network Info State = %s\n",
                      zabUtility_GetNetworkInfoStateString(notifyData.nwkInfoState));

            appGateway_NetworkInfoStateNtfHandler(notifyData.nwkInfoState);
            break;
          default:
            printApp(Service, "AppNotify: WARNING: Unknown notification type 0x%02X\n",
                      sapMsgGetNotifyType(Message));
        }
    }
  else
    {
      printApp(Service, "APP Manage SAP Handler: WARNING: Other Sap Type receive :%s\n", zabUtility_GetSapMsgTypeString(Type) );
    }
}

/******************************************************************************
 * Serial Out Action Handler
 * This function is registered to handle actions coming out of ZAB to the serial glue.
 ******************************************************************************/
void appZabCoreGlue_SerialOutActionHandler(erStatus* Status, zabService* Service, zabAction Action)
{
  printApp(appService,"appZabCoreGlue_SerialOutActionHandler\n");
  switch (Action)
    {
      case ZAB_ACTION_OPEN:

            printApp(Service, "Serial Glue Action = ZAB_ACTION_OPEN\n");

                printApp(Service, "Serial Glue Notify = ZAB_OPEN_STATE_OPENED\n");
                zabSerialService_NotifyOpenState(Status, Service, ZAB_OPEN_STATE_OPENED);

          break;

      case ZAB_ACTION_CLOSE:

            printApp(Service, "Serial Glue Action = ZAB_ACTION_CLOSE\n");
            printApp(Service, "Serial Glue Notify = ZAB_OPEN_STATE_CLOSED\n");
            zabSerialService_NotifyOpenState(Status, Service, ZAB_OPEN_STATE_CLOSED);
          
        break;

      default:
        printApp(Service, "appZabCoreGlue_SerialOutActionHandler: Unknown Action = 0x%02X\n", (unsigned8)Action);
        break;
    }
  appMain_WorkRequired();

}

/******************************************************************************
 * Serial Out Data Handler
 * This function is registered to handle serial data coming out of ZAB to the serial glue.
 ******************************************************************************/
void appZabCoreGlue_SerialOutDataHandler(erStatus* Status, zabService* Service, unsigned16 DataLength, unsigned8* Data)
{
  //printApp(appService,"appZabCoreGlue_SerialOutDataHandler\n");
  #ifdef INCLUDE_SERIAL_DEBUG
    int i;
    char* myString = NULL;
    char* myStringEnd;
    myString = (char*)malloc(50 + (DataLength * 3));
    if (myString != NULL)
      {
        myStringEnd = myString;
        myStringEnd += sprintf(myStringEnd, "WRITE --> ");
        for (i = 0; i < DataLength; i++)
        {
            myStringEnd += sprintf(myStringEnd, "%02x ", (unsigned int)Data[i]);
        }
        myStringEnd += sprintf(myStringEnd, "\n");
        platPrintSerial(Service, myString);
        free(myString);
      }
#endif
  COM_WriteData(COM_USART1,(uint8_t*) Data, DataLength);
}

/******************************************************************************
 * Ask Handler
 * Returns configuration values ASKED from the APP by ZAB
 ******************************************************************************/
void appAskCfg (erStatus* Status, zabService* Service, zabAskId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer)
{
                     
  unsigned8  Out8 = 0;
  unsigned16 Out16 = 0;
  unsigned32 Out32 = 0;
  unsigned64 Out64 = 0;
  unsigned32 OutSize = 0;
  //unsigned8* OutPointer = NULL;

  ER_CHECK_STATUS_NULL(Status, Service);
  ER_CHECK_NULL(Status, Buffer);

  if (srvCoreGetServiceId(Status, Service) != APP_CONFIG_M_SERVICE_ID)
    {
      return;
    }

  // See if App Gateway wishes to hadnle the item. If it does the nwe jsut return.
  if (appGateway_AskCfgHandler(Status, Service, What, Param1, Size, Buffer) == zab_true)
  {
    return;
  }
  switch (What)
    {

      case ZAB_ASK_BUFFER_COUNT_IN:
        OutSize = 2;
        Out16 = 0x0020;
        printApp(Service, "AppAsk: ZAB_ASK_BUFFER_COUNT_IN = 0x%04X\n", Out16);
        osMemCopy(Status, Buffer, (unsigned8*)&Out16, OutSize);
        break;

      case ZAB_ASK_BUFFER_COUNT_IN_LONG:
        OutSize = 2;
        Out16 = 0x0020;
        printApp(Service, "AppAsk: ZAB_ASK_BUFFER_COUNT_IN_LONG = 0x%04X\n", Out16);
        osMemCopy(Status, Buffer, (unsigned8*)&Out16, OutSize);
        break;

      case ZAB_ASK_BUFFER_COUNT_OUT:
        OutSize = 2;
        Out16 = 0x0020;
        printApp(Service, "AppAsk: ZAB_ASK_BUFFER_COUNT_OUT = 0x%04X\n", Out16);
        osMemCopy(Status, Buffer, (unsigned8*)&Out16, OutSize);
        break;

      case ZAB_ASK_BUFFER_COUNT_OUT_LONG:
        OutSize = 2;
        Out16 = 0x0020;
        printApp(Service, "AppAsk: ZAB_ASK_BUFFER_COUNT_OUT_LONG = 0x%04X\n", Out16);
        osMemCopy(Status, Buffer, (unsigned8*)&Out16, OutSize);
        break;

      case ZAB_ASK_BUFFER_COUNT_EVENT:
        OutSize = 2;
        Out16 = 0x0004;
        printApp(Service, "AppAsk: ZAB_ASK_BUFFER_COUNT_EVENT = 0x%04X\n", Out16);
        osMemCopy(Status, Buffer, (unsigned8*)&Out16, OutSize);
        break;

      case ZAB_ASK_SAP_MANAGE_MAX:
        OutSize = 1;
        Out8 = 1; // In notification
        printApp(Service, "AppAsk: ZAB_ASK_SAP_MANAGE_MAX = 0x%02X\n", Out8);
        osMemCopy(Status, Buffer, &Out8, OutSize);
        break;

      case ZAB_ASK_SAP_DATA_MAX:
        OutSize = 1;
        Out8 = 0; // No application requirements for data sap access
        printApp(Service, "AppAsk: ZAB_ASK_SAP_DATA_MAX = 0x%02X\n", Out8);
        osMemCopy(Status, Buffer, &Out8, OutSize);
        break;

      case ZAB_ASK_SAP_SERIAL_MAX:
        OutSize = 1;
        Out8 = 0; // No additional access required
        printApp(Service, "AppAsk: ZAB_ASK_SAP_SERIAL_MAX = 0x%02X\n", Out8);
        osMemCopy(Status, Buffer, &Out8, OutSize);
        break;

      case ZAB_ASK_NETWORK_PROCESSOR_START:

        printApp(Service, "AppAsk: ZAB_ASK_NETWORK_PROCESSOR_START \n");

        break;

      case ZAB_ASK_NETWORK_PROCESSOR_STOP_ON_CLOSE:
        /* Leave as default */
        printApp(Service, "AppAsk: ZAB_ASK_NETWORK_PROCESSOR_STOP_ON_CLOSE = Default \n");
        break;

      case ZAB_ASK_NWK_RESUME:
        OutSize = 1;
        printApp(Service, "AppAsk: ZAB_ASK_NWK_RESUME = %s\n", appConfig_Config.nwkResume ? "TRUE" : "FALSE");
        osMemCopy( Status, Buffer, &appConfig_Config.nwkResume, OutSize );
        break;

      case ZAB_ASK_NWK_CHANNEL_MASK:
        OutSize = 4;
        // Use a channel if one is specified, otherwise all
        if ( (appConfig_Config.channelNumber > 10) && (appConfig_Config.channelNumber < 27) )
          {
            printApp(Service, "AppAsk: ZAB_ASK_NWK_CHANNEL_MASK = Channel 0x%02X (%d)\n", appConfig_Config.channelNumber, appConfig_Config.channelNumber);
            Out32 = 1 << appConfig_Config.channelNumber;
          }
        else
          {
            printApp(Service, "AppAsk: ZAB_ASK_NWK_CHANNEL_MASK = All Channels\n");
            Out32 = 0x07FFF800;
          }
        osMemCopy( Status, Buffer, (unsigned8*)&Out32, OutSize );
        break;

      case ZAB_ASK_NWK_STEER:
        OutSize = 1;
        Out8 = appConfig_Config.nwkSteerOptions;
        printApp(Service, "AppAsk: ZAB_ASK_NWK_STEER = %s\n", zabUtility_GetNwkSteerString(appConfig_Config.nwkSteerOptions));
        osMemCopy( Status, Buffer, &Out8, OutSize );
        break;

      case ZAB_ASK_NWK_SECURITY_LEVEL:
        OutSize = 1;
        Out8 = 1;
        printApp(Service, "AppAsk: ZAB_ASK_NWK_SECURITY_LEVEL = 0x%02X\n", Out8);
        osMemCopy( Status, Buffer, &Out8, OutSize );
        break;

      case ZAB_ASK_NWK_PAN_ID:
        OutSize = 2;
        if (appConfig_Config.panId != APP_CONFIG_M_PAN_ID_UNDEFINED)
          {
            printApp(Service, "AppAsk: ZAB_ASK_NWK_PAN_ID = 0x%04X\n", appConfig_Config.panId);
            osMemCopy(Status, Buffer, (unsigned8*)&appConfig_Config.panId, OutSize);
          }
        else
          {
            osMemCopy(Status, (unsigned8*)&Out16, Buffer, sizeof(unsigned16));
            printApp(Service, "AppAsk: ZAB_ASK_NWK_PAN_ID = Not set. Using ZAB default: 0x%04X\n", Out16);
          }
        break;

      case ZAB_ASK_NWK_EPID:
        if (appConfig_Config.extPanId != APP_CONFIG_M_EPID_UNDEFINED)
          {
            OutSize = 8;
            printApp(Service, "AppAsk: ZAB_ASK_NWK_EPID = 0x%016llX\n", appConfig_Config.extPanId);
            osMemCopy(Status, Buffer, (unsigned8*)&appConfig_Config.extPanId, OutSize);
          }
        else
          {
            osMemCopy(Status, (unsigned8*)&Out64, Buffer, sizeof(unsigned64));
            printApp(Service, "AppAsk: ZAB_ASK_NWK_EPID = Not set. Using ZAB default: 0x%016llX\n", Out64);
          }
        break;

      case ZAB_ASK_NWK_JOIN_BEACON:

        printApp(Service, "AppAsk: ZAB_ASK_NWK_JOIN_BEACON = Index \n");

        break;

      case ZAB_ASK_FIRMWARE_IMAGE_LENGTH:

        printApp(Service, "AppAsk: ZAB_ASK_FIRMWARE_IMAGE_LENGTH = \n");
    
        break;

      case ZAB_ASK_FIRMWARE_IMAGE_DATA:

        printApp(Service, "AppAsk: ZAB_ASK_FIRMWARE_IMAGE_DATA: Offset = \n");
 
        break;

      case ZAB_ASK_TX_POWER_DBM:
        OutSize = 1;
        Out8 = (unsigned8)appConfig_Config.txPower;
        printApp(Service, "AppAsk: ZAB_ASK_TX_POWER_DBM = %d\n", appConfig_Config.txPower);
        osMemCopy( Status, Buffer, &Out8, OutSize );

        break;

      case ZAB_ASK_PERMIT_JOIN_TIME:
        OutSize = 2;
        printApp(Service, "AppAsk: ZAB_ASK_PERMIT_JOIN_TIME = %d\n", appConfig_Config.permitJoinTime);
        osMemCopy(Status, Buffer, (unsigned8*)&appConfig_Config.permitJoinTime, OutSize);
        break;

      case ZAB_ASK_CHANNEL_TO_CHANGE_TO:

        printApp(Service, "AppAsk: ZAB_ASK_CHANNEL_TO_CHANGE_TO = \n");

        break;

      case ZAB_ASK_ANTENNA_MODE:
   
        printApp(Service, "AppAsk: ZAB_ASK_ANTENNA_MODE = \n");
    
        break;

#ifdef APP_CONFIG_ENABLE_GREEN_POWER
      case ZAB_ASK_GREEN_POWER_ENDPOINT:
        OutSize = 1;
        Out8 = appConfig_Config.greenPowerEndpoint;
        printApp(Service, "AppAsk: ZAB_ASK_GREEN_POWER_ENDPOINT = 0x%02X (%d)\n", Out8, Out8);
        osMemCopy( Status, Buffer, &Out8, OutSize );
        break;
#endif

      case ZAB_ASK_NETWORK_MAINTENANCE_SLOW_PING_TIME_MS:
     
        printApp(Service, "AppAsk: ZAB_ASK_NETWORK_MAINTENANCE_SLOW_PING_TIME_MS = \n");
    
        break;

      case ZAB_ASK_NETWORK_MAINTENANCE_FAST_PING_TIME_MS:
       
        printApp(Service, "AppAsk: ZAB_ASK_NETWORK_MAINTENANCE_FAST_PING_TIME_MS = \n");
       
        break;

      default:
        /* For unsupported items, just leave them as defaults */
        printApp(Service, "WARNING: Unhandled cfg item asked, What = 0x%04X\n", (unsigned16)What);
        break;
    }
}

/******************************************************************************
 * Give Handler
 * Accepts configuration values GIVEN by ZAB to the app
 ******************************************************************************/
 void appGiveCfg(erStatus* Status, zabService* Service, zabGiveId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer)
{
    unsigned32 OutSize = 0;
  unsigned32 len;

  ER_CHECK_STATUS_NULL(Status, Service);
  ER_CHECK_NULL(Status, Buffer);
  len = *Size;

  if (srvCoreGetServiceId(Status, Service) != APP_CONFIG_M_SERVICE_ID)
    {
      return;
    }

  printApp(Service, "AppGive: ");
  switch (What)
    {
      case ZAB_GIVE_CORE_VERSION:
        if (len == 4)
          {
            osMemCopy(Status, (unsigned8*)&appConfig_Info.coreVersion, Buffer, len);
            printApp(Service, "Core Version: %03d.%03d.%03d.%03d",
                   appConfig_Info.coreVersion[0],
                   appConfig_Info.coreVersion[1],
                   appConfig_Info.coreVersion[2] ,
                   appConfig_Info.coreVersion[3]);
          }
        else
          {
              erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;

      case ZAB_GIVE_VENDOR_VERSION:
        if (len == 4)
          {
            osMemCopy(Status, (unsigned8*)&appConfig_Info.vendorVersion, Buffer, len);
            printApp(Service, "Vendor Version: %03d.%03d.%03d.%03d",
                   appConfig_Info.vendorVersion[0],
                   appConfig_Info.vendorVersion[1],
                   appConfig_Info.vendorVersion[2],
                   appConfig_Info.vendorVersion[3]);
          }
        else
          {
            erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;

      case ZAB_GIVE_VENDOR_TYPE:
        if (len == 2) 
          {
            appConfig_Info.vendorType = *(zabVendorType*)Buffer;
            printApp(Service, "Vendor Type: %s", zabUtility_GetVendorTypeString(appConfig_Info.vendorType));
          }
        else
          {
            erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;

      case ZAB_GIVE_NETWORK_PROCESSOR_MODEL:
        if (len == 1)
          {
            appConfig_Info.hardwareType = (zabNetworkProcessorModel)(*Buffer);
            printApp(Service, "Nwk Proc Model = %s", zabUtility_GetNetworkProcessorModelString(appConfig_Info.hardwareType));
          }
        else
          {
            erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;


      case ZAB_GIVE_NETWORK_PROCESSOR_ACTIVE_APP_TYPE: // 1 byte,
        if (len == 1)
          {
            printApp(Service, "Nwk Proc Active Application Type = %s", zabUtility_GetNetworkProcessorApplicationString((zabNetworkProcessorApplication)(*Buffer)));
          }
        else
          {
            erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;


      case ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_TYPE: // 1 byte,
        if (len == 1)
          {
            printApp(Service, "Nwk Proc ZigBee Application Type = %s", zabUtility_GetNetworkProcessorApplicationString((zabNetworkProcessorApplication)(*Buffer)));
          }
        else
          {
            erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;

      case ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_VERSION:
        if (len == 4)
          {
            printApp(Service, "Nwk Proc ZigBee Application Version = %03d.%03d.%03d.%03d",
                   Buffer[0],
                   Buffer[1],
                   Buffer[2],
                   Buffer[3]);
          }
        else
          {
              erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;

      case ZAB_GIVE_NETWORK_PROCESSOR_BOOTLOADER_VERSION:
        if (len == 4)
          {
            printApp(Service, "Nwk Proc Bootloader Version = %03d.%03d.%03d.%03d",
                   Buffer[0],
                   Buffer[1],
                   Buffer[2],
                   Buffer[3]);
          }
        else
          {
              erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;


    /* Beacons received during network discovery
     * Store in table so one can be selected and returned for Join*/
    case ZAB_GIVE_NWK_BEACON:
      if (appConfig_Config.beaconCount < APP_CONFIG_M_MAX_BEACONS)
        {
          OutSize = sizeof(BeaconFormat_t);
          osMemCopy( Status, (unsigned8*)&appConfig_Config.beaconStorage[appConfig_Config.beaconCount], Buffer, OutSize);

          printApp(Service, "Beacon 0x%02X: Src 0x%04X, PAN 0x%04X, Ch 0x%02X, PJ 0x%02X, EPID 0x%016llX, PD 0x%02X, SP 0x%02X",
                                  appConfig_Config.beaconCount,
                                  appConfig_Config.beaconStorage[appConfig_Config.beaconCount].src,
                                  appConfig_Config.beaconStorage[appConfig_Config.beaconCount].pan,
                                  appConfig_Config.beaconStorage[appConfig_Config.beaconCount].channel,
                                  appConfig_Config.beaconStorage[appConfig_Config.beaconCount].permitJoin,
                                  appConfig_Config.beaconStorage[appConfig_Config.beaconCount].epid,
                                  appConfig_Config.beaconStorage[appConfig_Config.beaconCount].depth,
                                  appConfig_Config.beaconStorage[appConfig_Config.beaconCount].stackProfile);

          appConfig_Config.beaconCount++;
        }
      else
        {
          printApp(Service, "WARNING: Beacon table full");
        }
      break;


    case ZAB_GIVE_IEEE:
      if (len == 8)
        {
          appConfig_Info.ieee = *(unsigned64*)Buffer;
          printApp(Service, "IEEE = 0x%016llX", *(unsigned64*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_NWK_ADDRESS:
      if (len == 2)
        {
          appConfig_Info.nwkAddress = *(unsigned16*)Buffer;
          printApp(Service, "Network Address = 0x%04X", *(unsigned16*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_NWK_CHANNEL_NUMBER:
      if (len == 1)
        {
          appConfig_Info.channel = *Buffer;
          printApp(Service, "Channel = 0x%02X", *Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_NWK_PAN_ID:
      if (len == 2)
        {
          appConfig_Info.panID = *(unsigned16*)Buffer;
          printApp(Service, "PAN ID = 0x%04X", *(unsigned16*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_NWK_EPID:
      if (len == 8)
        {
          appConfig_Info.epid = *(unsigned64*)Buffer;
          printApp(Service, "EPID = 0x%016llX,", *(unsigned64*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_DEVICE_TYPE:
      if (len == sizeof(unsigned8))
        {
          printApp(Service, "ZAB_GIVE_DEVICE_TYPE = %s", zabUtility_GetDeviceTypeString(*(zabDeviceType*)Buffer));
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_PARENT_IEEE:
      if (len == sizeof(unsigned64))
        {
          printApp(Service, "ZAB_GIVE_PARENT_IEEE = 0x%016llX,", *(unsigned64*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_PARENT_NWK_ADDRESS:
      if (len == sizeof(unsigned16))
        {
          printApp(Service, "ZAB_GIVE_PARENT_NWK_ADDRESS = 0x%04X", *(unsigned16*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_NWK_TX_POWER:
      if (len == sizeof(signed8))
        {
          appConfig_Info.actualTxPower = *(signed8*)Buffer;
          printApp(Service, "Actual Tx Power = %ddBm", *(signed8*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_ANTENNA_SUPPORTED_MODES:
      if (len == sizeof(signed8))
        {
          appConfig_Info.antennaSupportedModes = (zabAntennaSupportedModes)*Buffer;
          printApp(Service, "Supported Antenna Modes = 0x%02X: %s %s %s",
                   appConfig_Info.antennaSupportedModes,
                   (appConfig_Info.antennaSupportedModes & ZAB_ANTENNA_SUPPORTED_MODE_ANTENNA1) ? "Antenna1," : "",
                   (appConfig_Info.antennaSupportedModes & ZAB_ANTENNA_SUPPORTED_MODE_ANTENNA2) ? "Antenna2," : "",
                   (appConfig_Info.antennaSupportedModes & ZAB_ANTENNA_SUPPORTED_MODE_DIVERSITY_AUTO) ? "DiveristyAuto" : ""
                  );
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_ANTENNA_CURRENT_MODE:
      if (len == sizeof(signed8))
        {
          appConfig_Info.antennaOperatingMode = (zabAntennaOperatingMode)*Buffer;
          printApp(Service, "Current Antenna Mode = 0x%02X: %s",
                   appConfig_Info.antennaOperatingMode,
                   zabUtility_GetAntennaOperatingModeString(appConfig_Info.antennaOperatingMode));
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_NETWORK_MAINTENANCE_SLOW_PING_TIME_MS:
      if (len == sizeof(unsigned32))
        {
          printApp(Service, "ZAB_GIVE_NETWORK_MAINTENANCE_SLOW_PING_TIME_MS = %d", *(unsigned32*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_NETWORK_MAINTENANCE_FAST_PING_TIME_MS:
      if (len == sizeof(unsigned32))
        {
          printApp(Service, "ZAB_GIVE_NETWORK_MAINTENANCE_FAST_PING_TIME_MS = %d", *(unsigned32*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    default:
      printApp(Service, "WARNING: Unhandled cfg item given, What = 0x%04X", (unsigned16)What);
  }

  printApp(Service, "\n");

  // Pass to appGateway which may wish to catch the item too
  appGateway_GiveCfgHandler(Status, Service, What, Param1, Size, Buffer);
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/