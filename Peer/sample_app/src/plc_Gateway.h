/*
 * plc_Gateway.h
 *
 *  Created on: 26 sept. 2017
 *      Author: SESA260450
 */

#ifndef SAMPLE_APP_SRC_PLC_GATEWAY_H_
#define SAMPLE_APP_SRC_PLC_GATEWAY_H_

/******************************************************************************
Macro definitions
******************************************************************************/


/******************************************************************************
Typedef definitions
******************************************************************************/
/* Command type */
typedef enum
{
	COMMAND_TYPE_CONTROL,
	COMMAND_TYPE_DATA,
	COMMAND_TYPE_ACK=0xFF,
}e_command_type;

/* Control command type */
typedef enum
{
	CONTROL_CMD_SATE,
	CONTROL_CMD_DEVICE_TYPE,
	CONTROL_CMD_ID_NETWORK,
	CONTROL_CMD_CONFIG,
	CONTROL_CMD_PLC_OK,
	CONTROL_CMD_PLC_KO
}e_control_command_type;

/* State command type */
typedef enum
{
	CONTROL_STATE_NETWORK,
	CONTROL_STATE_DISCOVERY,
	CONTROL_STATE_ADVERTISING,
}e_control_state_type;

/* Network state */
typedef enum
{
	NWK_STATE_UNINITIALISED,
	NWK_STATE_NONE,
	NWK_STATE_NETWORKING,
	NWK_STATE_NETWORKED,
	NWK_STATE_NETWORK_LOSS,
	NWK_STATE_WAIT_CONFIG,
}e_network_state;

/* Discovery state, only in when sate = NWK_STATE_NONE */
typedef enum
{
	NWK_STATE_NO_DISCOVERY,
	NWK_STATE_DISCOVERING,
	NWK_STATE_DISCOVERY_COMPLETE,
}e_network_state_discovery;

/* Config state */
typedef enum
{
	RESET_CONFIG,
	GET_CONFIG,
}e_config_cmd;

/* Error code */
typedef enum
{
	FRAME_SUCCESS,
	FRAME_FAILED,
	FRAME_BAD_CRC,
	FRAME_BAD_COUNTER,
	FRAME_INVALID_CMD,
	FRAME_UNKNOW_ERROR,
}e_ack_value;

/* Network state */
typedef struct
{
	e_network_state 				state;
	uint8_t							peer_device_idx;
	uint8_t							peer_address[2];
}t_network;

/* Network state */
typedef struct
{
	e_control_state_type			type;			/* 0-Network, 1-Discovery, 2-Advertising */

	union
	{
		t_network					network;		/* Network state: 0-Uninitialized, 1-None, 2-Networking, 3-Networked, 4-Network loss */
		e_network_state_discovery	discovey;		/* Discovery state: 0-No discovery, 1-Discovering, 2-Discovery complete */
	};
}t_network_state;

/* Device type */
typedef struct
{
	r_adp_device_type_t				type;			/* 0-Peer, 1-Concentrator */
	uint8_t							mac_address[2];	/* Mac address */
	uint8_t							panID[2];		/* Network PanID */
}t_plc_device_type;

/* Id network */
typedef struct
{
	uint8_t							nb_netwok;		/* Nb of network plc available */
	uint8_t							id[2][5];			/* Id of each network */
}t_id_network;

/* Device config */
typedef struct
{
	e_config_cmd					cmd;			/* 0- reset config, 1- get config */
}t_config;

/* Id network */
typedef struct
{
	e_ack_value						value;			/* 0-Ack, 1-Nack */
}t_ack_frame;

/* Struct of control frame received */
typedef struct
{
	e_control_command_type			type;		/* Control cmd type: 0-State network, 1-Deviceplc type, 2-ID network plc */

	/* List of different control frame type */
	union
	{
		t_network_state				state;			/* Network state (network, discovery, advertising */
		t_plc_device_type			device_plc;		/* Device plc plugged */
		t_id_network				id_network;		/* Id device plc */
		t_config					config;			/* Device config command */
	};
}t_control_frame;

/* Struct of data frame received */
typedef struct
{
	uint8_t							peer_device_idx;
	uint8_t 						data[BUFFER_FRAME_SIZE-3];
}t_data_frame;

/* Struct of frames */
typedef struct
{
	uint8_t							start_of_frame;	/* 0xFE as start of frame char */
	uint8_t   						size;			/* Frame size */
	e_command_type					command;		/* Command type: 0-Control, 1-Data, 2-Ack */
	uint8_t							frame_counter;	/* Frame n° */

	/* List of different frame type */
	union
	{
		t_control_frame				control;		/* Network state (network, discovery, advertising */
		t_data_frame				data;			/* Data to transmit */
		t_ack_frame					ack;			/* Ack frame response */
	};
}t_uart_packet;

typedef struct
{
	union
	{
		t_uart_packet				packet;
		uint8_t						data[BUFFER_FRAME_SIZE];
	};
}t_uart_data;


/******************************************************************************
* Function Name: Peer_Display_Error
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void Peer_Display_Error ( r_result_t result );

/******************************************************************************
* Function Name: Uart_Send_Data_Frame
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t Uart_Send_Control_Frame ( t_uart_data* tx_frame );

/******************************************************************************
* Function Name: Uart_Send_Data_Frame
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t Uart_Send_Data_Frame ( uint8_t* data, uint8_t size_data );

/******************************************************************************
* Function Name: Gateway_Transmit_Data
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t Gateway_Transmit_Frame ( uint8_t type, uint8_t* data, uint8_t size_data, uint16_t exp );

/******************************************************************************
* Function Name: Gateway_Set_Peer_Address
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void Gateway_Set_Peer_Address( uint8_t* network_address );

/******************************************************************************
* Function Name: PLC_Gateway
* Description :
* Arguments :
* Return Value :
******************************************************************************/
extern r_result_t PLC_Gateway (void);


#endif /* SAMPLE_APP_SRC_PLC_GATEWAY_H_ */
