/*******************************************************************************
  Filename    : data_converter.h
  $Date       : 2013-05-13                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : data converter functions for the SZL

 * 002.002.032  09-Jan-17   MvdB   ARTF172065: Rationalise duplicated private functions into public SZLEXT_ZigBeeDataTypeIsAnalog()
*******************************************************************************/
#ifndef _DATA_CONVERTER_H_
#define _DATA_CONVERTER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "szl.h"

/**************************************************************************************************
* INCLUDES
**************************************************************************************************/

/**************************************************************************************************
* DEFINES
**************************************************************************************************/
/*
 * Global defines
 */

/*
 * Application data defines
 */

/**************************************************************************************************
* ENUMs, STRUCTs & UNIONs
**************************************************************************************************/
// Struct for easy handling of native type conversion to/from szl_byte
typedef struct
{
    union
    {
        szl_uint8  raw[8];
        szl_uint8  primitive8;
        szl_uint16 primitive16;
        szl_uint24 primitive24;
        szl_uint32 primitive32;
        szl_uint40 primitive40;
        szl_uint48 primitive48;
        szl_uint56 primitive56;
        szl_uint64 primitive64;
        szl_int64  primitive64_signed;
    }data;
} NATIVE_DATA_t;

// Struct for easy handling of le type conversion to/from szl_byte
typedef struct
{
    union
    {
        szl_uint8   raw[8];
        szl_uint8   primitive8;
        szl_uint16  primitive16;
        le_uint24 primitive24;
        szl_uint32  primitive32;
        le_uint40 primitive40;
        le_uint48 primitive48;
        le_uint56 primitive56;
        le_uint64 primitive64;
    }data;
} LE_DATA_t;

/**************************************************************************************************
* PROTOS
**************************************************************************************************/
void DCDataSetToZero(void *data);
szl_uint8 NativePadSize(SZL_ZIGBEE_DATA_TYPE_t zbDataType);
szl_bool DataTypeIsPrimitive(SZL_ZIGBEE_DATA_TYPE_t DataType);
szl_bool DataTypeIsSigned(SZL_ZIGBEE_DATA_TYPE_t DataType);
szl_uint8 ZbDataSize(SZL_ZIGBEE_DATA_TYPE_t DataType, void* data);
szl_uint8 NativeDataSize(SZL_ZIGBEE_DATA_TYPE_t DataType, void* data);
szl_bool ZbToNative(SZL_ZIGBEE_DATA_TYPE_t dataType, szl_uint8* zb, szl_uint8 zbLength, NATIVE_DATA_t* pNativeData, szl_uint8* lengthNativeData );
szl_uint8 NativeToZb(void* nativeData, szl_uint8 nativeDataSize, SZL_ZIGBEE_DATA_TYPE_t DataType, void* zb );

#ifdef __cplusplus
}
#endif

#endif /* _DATA_CONVERTER_H_ */
