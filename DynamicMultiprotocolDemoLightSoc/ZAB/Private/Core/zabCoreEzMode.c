/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZ Mode networking state machine
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 02.00.00.01  29-Jul-13   MvdB   Original
 * 00.00.02.00  18-Oct-13   MvdB   Support notification of errors
 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 * 002.000.009  16-Apr-15   MvdB   Return timeout from zabCoreEzMode_Work()
 * 002.002.007  28-Sep-15   MvdB   EZ-Mode join: Schneider EPID endian-ness was backwards
 * 002.002.012  14-Oct-15   MvdB   ARTF104107: Support End Device operation
 * 002.002.031  09-Jan-17   MvdB   ARTF172881: Avoid work() returning zero due to errors like szl not initialised
 ******************************************************************************/


#include "zabCorePrivate.h"

#ifdef ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Get the current EZ Mode State
 ******************************************************************************/
static zabCoreEzModeState getEzModeState(zabService* Service)
{
  zabCoreEzModeState ezModeState;

  /* Be super careful and use the mutex on reads as well as writes */
  ZAB_MUTEX_ENTER(Service, ZAB_SRV(Service)->MutexId);
  ezModeState = ZAB_SRV(Service)->ezModeInfo.state;
  ZAB_MUTEX_EXIT(Service, ZAB_SRV(Service)->MutexId);

  return ezModeState;
}

/******************************************************************************
 * Set the current EZ Mdoe State
 ******************************************************************************/
static void setEzModeState(zabService* Service, zabCoreEzModeState ezModeState)
{
  ZAB_MUTEX_ENTER(Service, ZAB_SRV(Service)->MutexId);
  ZAB_SRV(Service)->ezModeInfo.state = ezModeState;
  ZAB_MUTEX_EXIT(Service, ZAB_SRV(Service)->MutexId);
}


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Out Action Handler
 * This handles EZ Mode actions from the application
 *
 * Note: For now this is just when actions are put into ZAB
 *       It could be run off the sap is necessary
 ******************************************************************************/
void zabCoreEzMode_ActionOutHandler(erStatus* Status, zabService* Service, zabAction Action)
{
  ER_CHECK_STATUS(Status);

  /* Handle EZ-Mode actions. Ignore others. */
  if (Action == ZAB_ACTION_EZ_MODE_NWK_STEER)
    {
      switch(zabCoreGetNetworkState(Service))
        {
          /* If None, start EZMode form/join */
          case ZAB_NWK_STATE_NONE:
            setEzModeState(Service, ZAB_CORE_EZ_MODE_STATE_FORM_JOIN);
            break;

          /* If already networked, set permit join */
          case ZAB_NWK_STATE_NETWORKED:
          case ZAB_NWK_STATE_NETWORKED_NO_COMMS:
            zabCoreAction(Status, Service, ZAB_ACTION_NWK_PERMIT_JOIN_ENABLE);
            break;

          /* Invalid network state - error */
          case ZAB_NWK_STATE_UNINITIALISED:
          case ZAB_NWK_STATE_NETWORKING:
          case ZAB_NWK_STATE_DISCOVERING:
          case ZAB_NWK_STATE_DISCOVERY_COMPLETE:
          case ZAB_NWK_STATE_LEAVING:
          default:
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_INVALID_NETWORK_STATE);
            erStatusSet(Status, ZAB_ERROR_INVALID_NETWORK_STATE);
            break;
        }
    }
}


/******************************************************************************
 * Ask Handler
 * Sets configuration values ASKED by the ZAB, if they are for an EZ-Mode process
 * that is running.
 *
 * Return Value:
 *  zab_true: Ask has been handled by EzMode. Application should not be asked.
 *  false: Ask was not answered by EzMode. Application should not be asked.
 ******************************************************************************/
zab_bool zabCoreEzMode_AskCfgHandler(erStatus* Status, zabService* Service, zabAskId CfgId, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer)
{
  zabCoreEzModeState ezModeState;
  zab_bool returnValue = szl_false;

  ER_CHECK_STATUS_FALSE(Status);

  ezModeState = getEzModeState(Service);

  switch (ezModeState)
    {
      case ZAB_CORE_EZ_MODE_STATE_NETWORK_DISCOVERY:
        if (CfgId == ZAB_ASK_NWK_CHANNEL_MASK)
          {
            /* Keep the default channel mask of all channels */
            returnValue = zab_true;
          }
        break;

      case ZAB_CORE_EZ_MODE_STATE_NETWORK_STEER:
        switch (CfgId)
          {
            case ZAB_ASK_NWK_STEER:                      // zabNwkSteerOptions
              returnValue = zab_true;
              if (ZAB_SRV(Service)->ezModeInfo.discoveryResult == ZAB_CORE_EZ_MODE_DISCOVERY_NO_BEACONS)
                {
                  printInfo(Service, "EZMODE: Form\n");
                  *Buffer = (unsigned8)ZAB_NWK_STEER_FORM;
                }

              else if ( (ZAB_SRV(Service)->ezModeInfo.discoveryResult == ZAB_CORE_EZ_MODE_DISCOVERY_ONE_NETWORK) ||
                        (ZAB_SRV(Service)->ezModeInfo.discoveryResult == ZAB_CORE_EZ_MODE_DISCOVERY_ONE_SCHNEIDER_NETWORK) )
                {
                  printInfo(Service, "EZMODE: Join\n");
                  *Buffer = (unsigned8)ZAB_NWK_STEER_JOIN;
                }
              break;

            case ZAB_ASK_NWK_JOIN_BEACON:
              returnValue = zab_true;
              osMemCopy( Status, Buffer, (unsigned8*)&ZAB_SRV(Service)->ezModeInfo.beacon, sizeof(BeaconFormat_t));
              break;

            /* Just use the defaults for these form parameters */
            case ZAB_ASK_NWK_PAN_ID:
            case ZAB_ASK_NWK_SECURITY_LEVEL:
            case ZAB_ASK_NWK_CHANNEL_MASK:
              returnValue = zab_true;
              break;

            default:
              break;
          }
        break;

      default:
        break;
    }
  return returnValue;
}


/******************************************************************************
 * Give Handler
 * Gets configuration values GIVEN by the ZAB, if they are for an EZ-Mode process
 * that is running.
 *
 * Return Value:
 *  zab_true: Give has been handled by EzMode. Application should not be given.
 *  false: Give was not answered by EzMode. Application should be given.
 ******************************************************************************/
zab_bool zabCoreEzMode_GiveCfgHandler(erStatus* Status, zabService* Service, zabGiveId CfgId, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer)
{
  zabCoreEzModeState ezModeState;
  BeaconFormat_t* beacon;
  zab_bool schneiderBeacon = szl_false;
  zab_bool returnValue = szl_false;

  ER_CHECK_STATUS_FALSE(Status);

  ezModeState = getEzModeState(Service);

  /*
   * If we are running network discovery, handle beacons
   */
  if (ezModeState == ZAB_CORE_EZ_MODE_STATE_NETWORK_DISCOVERY)
    {
      if (CfgId == ZAB_GIVE_NWK_BEACON)
        {
          /* Keep the default channel mask of all channels */
          returnValue = zab_true;

          beacon = (BeaconFormat_t*)Buffer;

          printInfo(Service, "EZMODE Beacon: Src 0x%04X, PAN 0x%04X, Ch 0x%02X, PJ 0x%02X, EPID 0x%016llX, PD 0x%02X, SP 0x%02X\n",
                                beacon->src,
                                beacon->pan,
                                beacon->channel,
                                beacon->permitJoin,
                                beacon->epid,
                                beacon->depth,
                                beacon->stackProfile);

          /* We only care about beacons that are permitting join. Ignore the rest. */
          if (beacon->permitJoin)
            {
              unsigned16 manCode;
              manCode = ((unsigned16)beacon->epid << 8) | (((unsigned16)beacon->epid >> 8) & 0x00FF);
              if (manCode == ZAB_TYPES_M_SCHNEIDER_MANUFACTURER_CODE)
                {
                  schneiderBeacon = zab_true;
                  printInfo(Service, "EZMODE Beacon: Schneider beacon\n");
                }

              switch(ZAB_SRV(Service)->ezModeInfo.discoveryResult)
                {
                  /* Store the first beacon, noting if it is schneider or non schneider */
                  case ZAB_CORE_EZ_MODE_DISCOVERY_NO_BEACONS:
                    if (schneiderBeacon == zab_true)
                      {
                        printInfo(Service, "EZMODE Beacon: One Schneider network\n");
                        ZAB_SRV(Service)->ezModeInfo.discoveryResult = ZAB_CORE_EZ_MODE_DISCOVERY_ONE_SCHNEIDER_NETWORK;
                      }
                    else
                      {
                        printInfo(Service, "EZMODE Beacon: One non Schneider network\n");
                        ZAB_SRV(Service)->ezModeInfo.discoveryResult = ZAB_CORE_EZ_MODE_DISCOVERY_ONE_NETWORK;
                      }
                    osMemCopy(Status, (unsigned8*)&ZAB_SRV(Service)->ezModeInfo.beacon, (unsigned8*)beacon, sizeof(BeaconFormat_t));
                    break;

                  /*
                   * We have see one non Schneider network already.
                   * If this is a Schneider network, then prefer it and go to ZAB_CORE_EZ_MODE_DISCOVERY_ONE_SCHNEIDER_NETWORK.
                   * If it is non Schneider:
                   *   - If it is from the same network as the previous beacon, keep it if it is "better", otherwise discard.
                   *   - If it from a different network, go to ZAB_CORE_EZ_MODE_DISCOVERY_MANY_NETWORKS state
                   */
                  case ZAB_CORE_EZ_MODE_DISCOVERY_ONE_NETWORK:
                    if (schneiderBeacon == zab_true)
                      {
                        printInfo(Service, "EZMODE Beacon: One Schneider network\n");
                        ZAB_SRV(Service)->ezModeInfo.discoveryResult = ZAB_CORE_EZ_MODE_DISCOVERY_ONE_SCHNEIDER_NETWORK;
                        osMemCopy(Status, (unsigned8*)&ZAB_SRV(Service)->ezModeInfo.beacon, (unsigned8*)beacon, sizeof(BeaconFormat_t));
                      }
                    else
                      {

                        if (beacon->epid == ZAB_SRV(Service)->ezModeInfo.beacon.epid)
                          {
                            /* Its the same network */
                            if (beacon->lqi > ZAB_SRV(Service)->ezModeInfo.beacon.lqi)
                              {
                                printInfo(Service, "EZMODE Beacon: Beacon preferred based on lqi\n");
                                osMemCopy(Status, (unsigned8*)&ZAB_SRV(Service)->ezModeInfo.beacon, (unsigned8*)beacon, sizeof(BeaconFormat_t));
                              }
                          }
                        else
                          {
                            printInfo(Service, "EZMODE Beacon: Many non schneider networks\n");
                            /* Its a different network */
                            ZAB_SRV(Service)->ezModeInfo.discoveryResult = ZAB_CORE_EZ_MODE_DISCOVERY_MANY_NETWORKS;
                          }
                      }
                    break;

                  /* We have seen >1 non Schneider networks. Prefer a Schneider one, otherwise ignore */
                  case ZAB_CORE_EZ_MODE_DISCOVERY_MANY_NETWORKS:
                    if (schneiderBeacon == zab_true)
                      {
                        printInfo(Service, "EZMODE Beacon: One Schneider network\n");
                        ZAB_SRV(Service)->ezModeInfo.discoveryResult = ZAB_CORE_EZ_MODE_DISCOVERY_ONE_SCHNEIDER_NETWORK;
                        osMemCopy(Status, (unsigned8*)&ZAB_SRV(Service)->ezModeInfo.beacon, (unsigned8*)beacon, sizeof(BeaconFormat_t));
                      }
                    break;

                  /*
                   * We have see one Schneider network already.
                   * If it is Schneider:
                   *   - If it is from the same network as the previous beacon, keep it if it is "better", otherwise discard.
                   *   - If it from a different network, go to ZAB_CORE_EZ_MODE_DISCOVERY_MANY_SCHNEIDER_NETWORK state
                   * If it is not Schneider, ignore.
                   */
                  case ZAB_CORE_EZ_MODE_DISCOVERY_ONE_SCHNEIDER_NETWORK:
                    if (schneiderBeacon == zab_true)
                      {
                        if (beacon->epid == ZAB_SRV(Service)->ezModeInfo.beacon.epid)
                          {
                            printInfo(Service, "EZMODE Beacon: Beacon preferred based on lqi\n");
                            /* Its the same network */
                            if (beacon->lqi > ZAB_SRV(Service)->ezModeInfo.beacon.lqi)
                              {
                                osMemCopy(Status, (unsigned8*)&ZAB_SRV(Service)->ezModeInfo.beacon, (unsigned8*)beacon, sizeof(BeaconFormat_t));
                              }
                          }
                        else
                          {
                            printInfo(Service, "EZMODE Beacon: Many Schneider networks\n");
                            /* Its a different network */
                            ZAB_SRV(Service)->ezModeInfo.discoveryResult = ZAB_CORE_EZ_MODE_DISCOVERY_MANY_SCHNEIDER_NETWORK;
                          }
                      }
                    break;

                  /* There are many open Schneider networks. Ignore everything and fail. */
                  //case ZAB_CORE_EZ_MODE_DISCOVERY_MANY_SCHNEIDER_NETWORK:
                  default:
                    break;
                }


            }
        }
    }
  return returnValue;
}

/******************************************************************************
 * EZ Mode Work
 * Runs in the core work context
 ******************************************************************************/
void zabCoreEzMode_NwkStateNotificationHandler(erStatus* Status, zabService* Service, zabNwkState nwkState)
{
  zabCoreEzModeState ezModeState;

  ER_CHECK_STATUS(Status);

  ezModeState = getEzModeState(Service);

  /* Start with a network discovery */
  if (ezModeState == ZAB_CORE_EZ_MODE_STATE_NETWORK_DISCOVERY)
    {
      if (nwkState == ZAB_NWK_STATE_DISCOVERY_COMPLETE)
        {
          /* Discovery is complete, so now we want to continue if there were:
           *  No networks -> form (if ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING_FORM defined)
           *  1 network -> join
           *  1 schneider network -> join */
          if (
#ifdef ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING_FORM
               (ZAB_SRV(Service)->ezModeInfo.discoveryResult == ZAB_CORE_EZ_MODE_DISCOVERY_NO_BEACONS) ||
#endif
               (ZAB_SRV(Service)->ezModeInfo.discoveryResult == ZAB_CORE_EZ_MODE_DISCOVERY_ONE_NETWORK) ||
               (ZAB_SRV(Service)->ezModeInfo.discoveryResult == ZAB_CORE_EZ_MODE_DISCOVERY_ONE_SCHNEIDER_NETWORK) )
            {
              setEzModeState(Service, ZAB_CORE_EZ_MODE_STATE_NETWORK_STEER);
              zabCoreAction(Status, Service, ZAB_ACTION_NWK_STEER);
            }
          else
            {
#ifdef ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING_FORM
              printError(Service, "ZabCoreEzMode: WARNING: Network steering failed as there are too many open networks\n");
#else
              if (ZAB_SRV(Service)->ezModeInfo.discoveryResult == ZAB_CORE_EZ_MODE_DISCOVERY_NO_BEACONS)
                {
                  printError(Service, "ZabCoreEzMode: WARNING: Network steering failed as there are no open networks\n");
                }
              else
                {
                  printError(Service, "ZabCoreEzMode: WARNING: Network steering failed as there are too many open networks\n");
                }
#endif
              sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
              setEzModeState(Service, ZAB_CORE_EZ_MODE_STATE_IDLE);
            }
        }
      else if (nwkState == ZAB_NWK_STATE_NONE)
        {
          printError(Service, "ZabCoreEzMode: Error at end of discovery\n");
        }
    }
}


/******************************************************************************
 * EZ Mode Work
 * Runs in the core work context
 ******************************************************************************/
unsigned32 zabCoreEzMode_Work(erStatus* Status, zabService* Service)
{
  unsigned32 nextWorkRequiredDelayMs = ZAB_WORK_DELAY_MAX_MS;
  zabCoreEzModeState ezModeState;

  ER_CHECK_STATUS_VALUE(Status, nextWorkRequiredDelayMs);

  ezModeState = getEzModeState(Service);

  /* Start with a network discovery */
  if (ezModeState == ZAB_CORE_EZ_MODE_STATE_FORM_JOIN)
    {
      nextWorkRequiredDelayMs = 0;
      ZAB_SRV(Service)->ezModeInfo.discoveryResult = ZAB_CORE_EZ_MODE_DISCOVERY_NO_BEACONS;
      zabCoreAction(Status, Service, ZAB_ACTION_NWK_DISCOVER);
      if (erStatusIsOk(Status))
        {
          setEzModeState(Service, ZAB_CORE_EZ_MODE_STATE_NETWORK_DISCOVERY);
        }
      else
        {
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
          setEzModeState(Service, ZAB_CORE_EZ_MODE_STATE_IDLE);
        }
    }
  return nextWorkRequiredDelayMs;
}

/******************************************************************************
 * Initialise EZ Mode
 ******************************************************************************/
void zabCoreEzMode_Init(erStatus* Status, zabService* Service)
{
  ZAB_SRV(Service)->ezModeInfo.state = ZAB_CORE_EZ_MODE_STATE_IDLE;
}


#endif // ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/