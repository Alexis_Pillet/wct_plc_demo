/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the configuration values for the ZNP Vendor.
 *
 *   Defines in "ENABLE / DISABLE MODULES" should be set based on what
 *   features the application requires.
 *
 *   Other values in this file may be tuned for an application, but should only
 *   be modified if you understand what you are doing and test the effects.
 *
 *   For details and flow charts see ZEC007.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *  00.00.00.02 30-Oct-13   MvdB   Original
 *                                 Add ZAB_VENDOR_SREQ_TIMEOUT
 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 00.00.06.02  30-Sep-14   MvdB   artf55512: Support network key change action
 * 00.00.06.04  07-Oct-14   MvdB   artf56243: Review handling of received Leave commands
 * 00.00.06.06  17-Oct-14   MvdB   artf105017: Update ZAB_VND_CFG_ZNP_M_NWK_CONFIRM_TIMEOUT_MS
 * 00.00.08.01  27-Nov-14   MvdB   Update default for ZAB_VND_CFG_ZNP_M_OPEN_SOFT_RST_TIMEOUT_MS to 10s
 * 01.100.06.00 10-Feb-15   MvdB   Split ZAB_VENDOR_SREQ_TIMEOUT into ZAB_VENDOR_DATA_CONFIRM_TIMEOUT and ZAB_VENDOR_SRSP_TIMEOUT
 * 002.002.012  14-Oct-15   MvdB   ARTF104107: Add ZAB_VND_CFG_ZNP_M_END_DEVICE_JOIN_TIMEOUT_MS for End Device operation
 * 002.002.021  20-Apr-16   MvdB   ARTF167736: Add serial retries to improve robustness against lossy serial link
 * 002.002.024  28-Jun-16   MvdB   ARTF172101: Add ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS
 * 002.002.028  12-Dec-16   MvdB   Increase default ZAB_VND_CFG_ZNP_M_NWK_FORM_TIMEOUT_MS from 20 to 30 seconds after seeing some form timeouts.
 *****************************************************************************/

#ifndef __ZAB_VENDOR_CONFIGURE_ZNP_H__
#define __ZAB_VENDOR_CONFIGURE_ZNP_H__


/******************************************************************************
 *                      *****************************
 *                 ***** NETWORK PROCESSOR SELECTION *****
 *                      *****************************
 ******************************************************************************/

/* Select network processor type */
#define ZAB_VND_CFG_ZNP_M_NP_CC2531USB_DONGLE
//#define ZAB_VND_CFG_ZNP_M_NP_CC2530_UART
//#define ZAB_VND_CFG_ZNP_M_NP_CC2530_SPI

/******************************************************************************
 *                      *****************************
 *                 *****  ENABLE / DISABLE MODULES   *****
 *                      *****************************
 ******************************************************************************/

/* Green Power - Inherit from the Core Configuration */
#ifdef ZAB_CFG_ENABLE_GREEN_POWER
#define ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER
#endif

/* Define to enable the Radio Factory Test module for the vendor.
 * This is typically only used by test applications during development.
 * Default = Undefined */
#define ZAB_VND_CFG_ZNP_ENABLE_RFT

/* Wireless Test Bench - Inherit from the Core Configuration */
#ifdef ZAB_CFG_ENABLE_WIRELESS_TEST_BENCH
#define ZAB_VND_CFG_ENABLE_WIRELESS_TEST_BENCH
#endif
/******************************************************************************
 *                      *****************************
 *                 *****          OPTIONS            *****
 *                      *****************************
 ******************************************************************************/

/* Enabling this option increases the validation of messages in ZAB.
 * It should be used during development and *may* be turned off for production code
 * if performance is a major concern.
 * Default = Defined. */
#define ZAB_CHECK_MSG_ALL

/* Allow ZAB to run with the standard ZNP.
 * This is not recommended. It is only for debugging with standard TI images. */
//#define ZAB_VND_CFG_ALLOW_STANDARD_TI_ZNP


/******************************************************************************
 *                      *****************************
 *                 *****     OPEN STATE MACHINE      *****
 *                      *****************************
 ******************************************************************************/

/* Time in ms to wait for serial glue to open or close before timing out
 * Default = 2000 for CC2531 */
#define ZAB_VND_CFG_ZNP_M_OPEN_SERIAL_GLUE_TIMEOUT_MS 2000

/* Time in ms to wait for a soft reset to complete before timing out
 * Default = 10000 for CC2531 */
#define ZAB_VND_CFG_ZNP_M_OPEN_SOFT_RST_TIMEOUT_MS 10000

/* Time in ms to wait after a Reset to SBL before re-establishing comms
 * Default = 5000 for CC2531 */
#define ZAB_VND_CFG_ZNP_M_OPEN_SBL_RST_TIMEOUT_MS 5000

/* Time in ms to wait for SRSP's before timing out
 * Default = 2000 for CC2531 */
#define ZAB_VND_CFG_ZNP_M_OPEN_GENERAL_TIMEOUT_MS 2000

/* Time in ms at which to ping network processor to check it is alive and in the correct mode.
 * Recommended minimum = 5000
 * Default = 10000
 * Disabled = 0xFFFF */
#define ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS             (0XFFFF) //(10000)

/******************************************************************************
 *                      *****************************
 *                 *****      SERIAL BOOT LOADER     *****
 *                      *****************************
 ******************************************************************************/

/* Time in ms to wait for SBL SRSP's before timing out
 * Default = 2000 for CC2531 */
#define ZAB_VND_CFG_ZNP_M_SBL_TIMEOUT_MS 2000


/******************************************************************************
 *                      *****************************
 *                 *****    NETWORK STATE MACHINE    *****
 *                      *****************************
 ******************************************************************************/

/* Time in ms to wait for confirms before timing out
 * Default = 10000 for CC2531 */
#define ZAB_VND_CFG_ZNP_M_NWK_CONFIRM_TIMEOUT_MS 10000

/* Time in ms to wait for Form to complete before timing out
 * Default = 30000 for CC2531
 * Typically takes around 12-15 seconds with a full channel mask.
 * Can take longer with long cahnnel mask and noisy channels */
#define ZAB_VND_CFG_ZNP_M_NWK_FORM_TIMEOUT_MS 60000

/* Time in ms to wait for an End Device Join to complete before canceling.
 * Default = 20000 for CC2531 (Usually takes around 10 seconds) */
#define ZAB_VND_CFG_ZNP_M_END_DEVICE_JOIN_TIMEOUT_MS 20000

/* Time in ms to wait for Leave to complete before timing out
 * Default = 20000 for CC2531 (Stack will take 5 seconds + reset time) */
#define ZAB_VND_CFG_ZNP_M_NWK_LEAVE_TIMEOUT_MS 20000

/* Time in ms to wait for channel change to be notified as complete
 * Default = 5000 for CC253x */
#define ZAB_VND_CFG_ZNP_M_CHANNEL_CHANGE_TIMEOUT_MS 5000

/* Time in ms to wait between Key Transport and Key Switch commands when changing network key
 * Default = 5000 */
#define ZAB_VND_CFG_ZNP_M_NETWORK_KEY_SWITCH_DELAY_MS 5000

/* When we send the command to switch network key it is possible the serial connection will
 * be in use by a data request or some other command. In this case we want to wait and retry later.
 * Default Retries = 10
 * Default Delay Between Retries = 500ms */
#define ZAB_VND_CFG_ZNP_M_NETWORK_KEY_SWITCH_RETRIES              10
#define ZAB_VND_CFG_ZNP_M_NETWORK_KEY_SWITCH_RETRY_BACKOFF_MS     500


/******************************************************************************
 *                      *****************************
 *                 *****    ZNP FRAME MANAGEMENT     *****
 *                      *****************************
 ******************************************************************************/

/* If defined, serial link will not be unlocked until a data confirm is received for an AF Data Request
 * If undefined, it will unlock on SRSP */
//#define ZAB_VENDOR_UNLOCK_SERIAL_ON_AF_DATA_CONFIRM

/* Maximum time to wait for a DataConfirm to DataRequest before timing out */
#ifdef ZAB_VENDOR_UNLOCK_SERIAL_ON_AF_DATA_CONFIRM
  #define ZAB_VENDOR_DATA_CONFIRM_TIMEOUT 9000
#endif

/* Maximum time to wait for an SRSP to an SREQ before timing out */
#define ZAB_VENDOR_SRSP_TIMEOUT 2000


/* Number of retries to attempt when serial transmissions do not recevie their serial ack
 * within ZAB_VENDOR_SRSP_TIMEOUT. Total number of attempts will be (ZAB_VENDOR_SERIAL_RETRIES  + 1).
 * This is implemented to improve robustness when serial link is not reliable (SLIPZ).
 * Default = 1
 * Disabled = 0 */
#define ZAB_VENDOR_SERIAL_RETRIES 1

/* Time to backoff (delay) before sending a retry if the transmit failed (error returned by serial glue).
 * This should be fairly short, but long enough to give the glue time to hopefully get over
 * whatever caused the error.
 * Default = 100ms */
#define ZAB_VENDOR_SERIAL_BACKOFF_MS 100



// this is the min and max put into the raw vendor frame buffer in each message
#define ZAB_VENDOR_MAX_DATA  255                // max ZNP message size  = 253 bytes + SOF + FCS
#define ZAB_VENDOR_MIN_DATA   48                // min ZNP message size (most common)

#define ZAB_VENDOR_MAX_DATA_RX ZAB_VENDOR_MAX_DATA   // max ZNP message size

/* Enable SOF and FCS for USB and UART network processors */
#if ( defined(ZAB_VND_CFG_ZNP_M_NP_CC2531USB_DONGLE) | \
      defined(ZAB_VND_CFG_ZNP_M_NP_CC2530_UART) )
#define ZAB_VND_CFG_ZNP_M_SOF_AND_FCS_ENABLED
#endif

/* Define Start of Frame value */
#define ZAB_VND_CFG_ZNP_M_START_OF_FRAME 0xFE

/* Max value of the Length field of a ZNP frame */
#define ZAB_VND_CFG_ZNP_M_MAX_ZNP_LENGTH_FIELD_VALUE 250

/******************************************************************************
 *                      *****************************
 *                 *****             ZDO             *****
 *                      *****************************
 ******************************************************************************/

/* MAC Capabilities Flag, as used for a device announce
 * Bit 0 = Alternate PAN Coordinator. Default 0.
 * Bit 1 = Device Type (1=FFD, 0= RFD). Default = 1.
 * Bit 2 = Power Source (1=mains). Default = 1.
 * Bit 3 = RxOnWhenIdle. Default = 1.
 * Bits 4-5 = Reserved. Default 0.
 * Bit 6 = High Security Capability. Default = 0.
 * Bit 7 = Allocate Address. Default = 0. */
#define ZAB_VND_CFG_ZNP_M_MAC_CAPABILITIES 0x0E


/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif // __ZAB_VENDOR_CONFIGURE_ZNP_H__
