
"""
data_model.zigbee_green_power.power_tag.power_tag_3p
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to model Power Tag 3P Device

"""
from data_model.zigbee_green_power.power_tag.power_tag import PowerTag


class PowerTag3P(PowerTag):
    """Model Power Tag 3P Device"""

    def __init__(self, address, id_wireless_bridge, application_frame_generator):
        """Initialization of the Power Tag 3P Device"""
        super(PowerTag3P, self).__init__(address, id_wireless_bridge, application_frame_generator)
        self._VOLTAGE_PHA_PHB_ID = "4b00"
        self._VOLTAGE_PHB_PHC_ID = "4c00"
        self._VOLTAGE_PHC_PHA_ID = "4d00"
        self._CURRENT_PHA_ID = "0508"
        self._CURRENT_PHB_ID = "0908"
        self._CURRENT_PHC_ID = "0a08"
        self._ACTIVE_POWER_PHB_ID = "090b"
        self._ACTIVE_POWER_PHC_ID = "0a0b"
        self._TOTAL_ACTIVE_POWER_ID = "0304"
        self._voltage_pha_phb = 0
        self._voltage_phb_phc = 0
        self._voltage_phc_pha = 0
        self._current_pha = 0
        self._current_phb = 0
        self._current_phc = 0
        self._active_power_phb = 0
        self._active_power_phc = 0
        self._total_active_power = 0

    def update_data(self, data):
        """Update the attributes of the device according to the value specified

        Args:
            data: dict of the value received
        """
        list_attribute = data[self._BASIC_CLUSTER]
        for attribute in list_attribute:
            if attribute.attribute_id == self._MANUFACTURER_NAME:
                self.manufacturer_name = attribute.attribute_value
            elif attribute.attribute_id == self._MODEL_IDENTIFIER:
                self.model_identifier = attribute.attribute_value
            elif attribute.attribute_id == self._APPLICATION_FIRMWARE_VERSION_ID:
                self.application_firmware_version = attribute.attribute_value
            elif attribute.attribute_id == self._APPLICATION_HARDWARE_VERSION_ID:
                self.application_hardware_version = attribute.attribute_value
            elif attribute.attribute_id == self._PRODUCT_SERIAL_NUMBER_ID:
                self.product_serial_number = attribute.attribute_value
            elif attribute.attribute_id == self._PRODUCT_IDENTIFIER_ID:
                self.product_identifier = attribute.attribute_value
            elif attribute.attribute_id == self._PRODUCT_MODEL:
                self.product_model = attribute.attribute_value

        list_attribute = data[self._ELECTRICAL_MEASUREMENT_CLUSTER]
        for attribute in list_attribute:
            if attribute.attribute_id == self._VOLTAGE_PHA_PHB_ID:
                self._voltage_pha_phb = attribute.attribute_value
            elif attribute.attribute_id == self._VOLTAGE_PHB_PHC_ID:
                self._voltage_phb_phc = attribute.attribute_value
            elif attribute.attribute_id == self._VOLTAGE_PHC_PHA_ID:
                self._voltage_phc_pha = attribute.attribute_value
            elif attribute.attribute_id == self._CURRENT_PHA_ID:
                self._current_pha = attribute.attribute_value
            elif attribute.attribute_id == self._CURRENT_PHB_ID:
                self._current_phb = attribute.attribute_value
            elif attribute.attribute_id == self._CURRENT_PHC_ID:
                self._current_phc = attribute.attribute_value
            elif attribute.attribute_id == self._ACTIVE_POWER_PHA_ID:
                self._active_power_pha = attribute.attribute_value
            elif attribute.attribute_id == self._ACTIVE_POWER_PHB_ID:
                self._active_power_phb = attribute.attribute_value
            elif attribute.attribute_id == self._ACTIVE_POWER_PHC_ID:
                self._active_power_phc = attribute.attribute_value
            elif attribute.attribute_id == self._TOTAL_ACTIVE_POWER_ID:
                self._total_active_power = attribute.attribute_value
            elif attribute.attribute_id == self._PHASE_SEQUENCE_ID:
                self._phase_sequence = attribute.attribute_value
            elif attribute.attribute_id == self._AC_ALARMS_MASK_ID:
                self._ac_alarms_mask = attribute.attribute_value
            elif attribute.attribute_id == self._ALARMS_MASK_ID:
                self._alarms_mask = attribute.attribute_value
            elif attribute.attribute_id == self._POWER_DIVISOR_ID:
                self._power_divisor = attribute.attribute_value
            elif attribute.attribute_id == self._VOLTAGE_DIVISOR_ID:
                self._voltage_divisor = attribute.attribute_value
            elif attribute.attribute_id == self._CURRENT_DIVISOR_ID:
                self._current_divisor = attribute.attribute_value

        list_attribute = data[self._METERING_CLUSTER]
        for attribute in list_attribute:
            if attribute.attribute_id == self._CUMULATED_ENERGY_ID:
                self._cumulated_energy = attribute.attribute_value
            elif attribute.attribute_id == self._PARTIAL_ENERGY_ID:
                self._partial_energy = attribute.attribute_value
            elif attribute.attribute_id == self._DIVISOR_ID:
                self._divisor = attribute.attribute_value

    def update_rssi(self, lqi, rssi):
        """Update the lqi and rssi of the product

        Args:
            lqi: lqi value
            rssi: rssi value
        """
        self.rssi = rssi
        self.lqi = lqi

    def get_json_data(self):
        """Get the json representation of the data of the Power Tag 3P product"""
        return {"product": {"address": self.address.upper(),
                            "lqi": self.lqi,
                            "rssi": self.rssi,
                            "model_identifier": self.model_identifier,
                            "application_firmware_version": self.application_firmware_version,
                            "application_hardware_version": self.application_hardware_version,
                            "product_serial_number": self.product_serial_number,
                            "product_identifier": self.product_identifier,
                            "product_model": self.product_model,
                            "manufacturer_name": self.manufacturer_name},
                "electrical_measurement": {"voltage_pha_phb": self.voltage_pha_phb / self.voltage_divisor,
                                           "voltage_phb_phc": self.voltage_phb_phc / self.voltage_divisor,
                                           "voltage_phc_pha": self.voltage_phc_pha / self.voltage_divisor,
                                           "current_pha": self.current_pha / self.current_divisor,
                                           "current_phb": self.current_phb / self.current_divisor,
                                           "current_phc": self.current_phc / self.current_divisor,
                                           "active_power_pha": self.active_power_pha,
                                           "active_power_phb": self.active_power_phb,
                                           "active_power_phc": self.active_power_phc,
                                           "total_active_power": self.total_active_power / self.power_divisor,
                                           "phase_sequence": self.phase_sequence,
                                           "ac_alarms_mask": self.ac_alarms_mask,
                                           "alarms_mask": self.alarms_mask},
                "metering": {"cumulated_energy": self.cumulated_energy / self.divisor,
                             "partial_energy": self.partial_energy / self.divisor}
                }

    def get_json_device(self, device_number):
        """Get the json representation of the Power Tag 3P product

        Args:
            device_number: number of the device
        """
        return {"text": self.address.upper() + ' - Power Tag 3P',
                'id': "WB_" + str(self.id_wireless_bridge + 1) + "_DEVICE_" + self.address,
                'type': 'power_tag_3p',
                'data': self.get_json_data()
                }

    @property
    def voltage_pha_phb(self):
        return self._voltage_pha_phb

    @voltage_pha_phb.setter
    def voltage_pha_phb(self, voltage_pha_phb):
        self._voltage_pha_phb = voltage_pha_phb

    @property
    def voltage_phb_phc(self):
        return self._voltage_phb_phc

    @voltage_phb_phc.setter
    def voltage_phb_phc(self, voltage_phb_phc):
        self._voltage_phb_phc = voltage_phb_phc

    @property
    def voltage_phc_pha(self):
        return self._voltage_phc_pha

    @voltage_phc_pha.setter
    def voltage_phc_pha(self, voltage_phc_pha):
        self._voltage_phc_pha = voltage_phc_pha

    @property
    def current_pha(self):
        return self._current_pha

    @current_pha.setter
    def current_pha(self, current_pha):
        self._current_pha = current_pha

    @property
    def current_phb(self):
        return self._current_phb

    @current_phb.setter
    def current_phb(self, current_phb):
        self._current_phb = current_phb

    @property
    def current_phc(self):
        return self._current_phc

    @current_phc.setter
    def current_phc(self, current_phc):
        self._current_phc = current_phc

    @property
    def active_power_phb(self):
        return self._active_power_phb

    @active_power_phb.setter
    def active_power_phb(self, active_power_phb):
        self._active_power_phb = active_power_phb

    @property
    def active_power_phc(self):
        return self._active_power_phc

    @active_power_phc.setter
    def active_power_phc(self, active_power_phc):
        self._active_power_phc = active_power_phc

    @property
    def total_active_power(self):
        return self._total_active_power

    @total_active_power.setter
    def total_active_power(self, total_active_power):
        self._total_active_power = total_active_power
