/******************************************************************************
 *                          ZAB TEST APPLICATION
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the application configuration settings for the ZAB
 *   Test Application.
 *   This is where we:
 *    - Define compile time settings (Header)
 *    - Create storage for values that may be asked by ZAB, or given to us from ZAB.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.03.00  16-Jan-14   MvdB   Use common header block
 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 00.00.06.05  09-Oct-14   MvdB   Tidy up messy allocation of appConfig_Config.port
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 * 002.000.003  06-Mar-15   MvdB   ARTF116061: Support run time configuration of Green Power endpoint.
 * 002.000.004  01-Apr-15   MvdB   ARTF56504: Support missed channel change recovery, with actions to get/set parameters.
 *                                 ConsoleTest - add boolean to enable/disable acceptance of GPDs
 * 002.002.002  01-Sep-15   MvdB   ARTF147441: Add appConfig_Config.greenPowerKeyMode
 * 002.002.004  04-Sep-15   MvdB   ARTF147424: Add appConfig_GetDeviceId()
 * 002.002.022  10-May-16   MvdB   Change EP1 Device Id to 7 (Combined interface) for gateway certification
 * 002.002.028  12-Dec-16   MvdB   ConsoleTest: Add runtime configurable options for report and RSSI printing
 * 002.002.029  06-Jan-17   MvdB   ConsoleTest: Support logging reports to reports.csv
 * 002.002.044  23-Jan-17   MvdB   Add greenPowerFilterByModelId
 * 002.002.051  16-Mar-17   MvdB   ConsoleTest: Add selective GP commissioning by source ID
 * 002.002.052  12-Apr-17   SMon   ConsoleTest: Add a decommissioning all command
 *****************************************************************************/



#include "zabCoreService.h"
#include "szl_gp.h"

#include "appConfig.h"
#include "appOsMemoryGlue.h"

#include <string.h>


/******************************************************************************
 *                      ******************************
 *                 *****       EXTERN VARIABLES       *****
 *                      ******************************
 ******************************************************************************/

appInfo_t appConfig_Info;
appConfig_t appConfig_Config;

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise the run time configuration database
 ******************************************************************************/
void appConfig_Init(void)
{
  appConfig_Info.channel = ZAB_TYPES_M_NETWORK_CHANNEL_UNKNOWN;
  appConfig_Info.epid = ZAB_TYPES_M_NETWORK_EPID_UNKNOWN;
  appConfig_Info.ieee = ZAB_TYPES_M_IEEE_UNKNOWN;
  appConfig_Info.nwkAddress = ZAB_TYPES_M_NETWORK_ADDRESS_UNKNOWN;
  appConfig_Info.panID = ZAB_TYPES_M_NETWORK_PAN_ID_UNKNOWN;
  memset(appConfig_Info.coreVersion, sizeof(appConfig_Info.coreVersion), 0xFF);
  memset(appConfig_Info.vendorVersion, sizeof(appConfig_Info.vendorVersion), 0xFF);
  appConfig_Info.vendorType = ZAB_VENDOR_TYPE_UNKNOWN;
  appConfig_Info.actualTxPower = -127;
  appConfig_Info.antennaSupportedModes = 0;
  appConfig_Info.antennaOperatingMode = zabAntennaOperatingMode_Unknown;

  appConfig_Config.port = NULL;

  appConfig_Config.startNetworkProcessorApplication = zab_true;
  appConfig_Config.nwkResume = zab_true;
  appConfig_Config.nwkSteerOptions = ZAB_NWK_STEER_JOIN;
  appConfig_Config.channelNumber = APP_CONFIG_M_CHANNEL_NUM_DEFAULT;
  appConfig_Config.panId = APP_CONFIG_M_PAN_ID_UNDEFINED;
  appConfig_Config.extPanId = APP_CONFIG_M_EPID_UNDEFINED;
  appConfig_Config.txPower = APP_CONFIG_M_DEFAULT_TX_POWER;
  appConfig_Config.permitJoinTime = APP_CONFIG_M_DEFAULT_PJ_TIME;
  appConfig_Config.channelToChangeTo = APP_CONFIG_M_DEFAULT_CHANNEL_TO_CHANGE_TO;
  appConfig_Config.newAntennaOperatingMode = zabAntennaOperatingMode_Antenna1;
  appConfig_Config.autoDiscoverEndpoints = zab_true;
  appConfig_Config.printRssiInfo = zab_true;
  appConfig_Config.printReportInfo = zab_true;
  appConfig_Config.printFullReportInfo = zab_true;
  appConfig_Config.generateReportCsvFile = zab_false;
  appConfig_Config.greenPowerEndpoint = APP_CONFIG_M_DEFAULT_GREEN_POWER_ENDPOINT;
  appConfig_Config.greenPowerAcceptGpdCommissioning = zab_true;
  appConfig_Config.greenPowerKeyMode = (unsigned8)SZL_GP_COMMISSIONING_KEY_MODE_DEFAULT;
  appConfig_Config.greenPowerFilterByModelId = 0xFFFF;
  appConfig_Config.greenPowerFilterBySourceId = 0xFFFFFFFF;
  appConfig_Config.greenPowerFilterByWhiteList = 0; 
  appConfig_Config.beaconCount = 0;
  appConfig_Config.beaconSelected = 0;
  appConfig_Config.nwkMaintSlowPingTimeMs = 0;
  appConfig_Config.nwkMaintFastPingTimeMs = 0;
  appConfig_Config.pollControlDataPending = zab_false;
  appConfig_Config.pollControlFastPollTime = 0;
  appConfig_Config.greenPowerDecommissioning = zab_false;
}


/******************************************************************************
 * Get Device ID for Endpoint
 ******************************************************************************/
unsigned16 appConfig_GetDeviceId(unsigned8 Endpoint)
{
  unsigned16 deviceId;

  switch(Endpoint)
    {
      case 1:
        deviceId = 0x0007; // ZB_HA_DEVICEID_COMBINED_INTERFACE
        break;

      case 2:
        deviceId = 0x000D; // ZB_HA_DEVICEID_CONSUMPTION_AWARENESS_DEVICE
        break;

      default:
        deviceId = 0x0007; // ZB_HA_DEVICEID_COMBINED_INTERFACE
    }

  return deviceId;
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/