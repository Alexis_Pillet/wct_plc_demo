/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL Device Manager plugin.
 *   This plugin is originally designed for MiGenie.
 *
 * USAGE:
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         07-Jul-16   MvdB   Original
 * 002.002.047  31-Jan-17   MvdB   Support optional parameters on DeviceManager gets()
 *                                 Make a recursive call of SzlPlugin_DevMan_UpdateAddresses() in case app may have added previously untracked node
 *****************************************************************************/

#include "szl.h"
#include "szl_zdo.h"
#include "szl_timer.h"

#include "device_manager.h"

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* Timed task will run every 1 second */
#define TASK_PERIOD_S                     ( 1 )

/* Maximum number of commands we allow per TASK_PERIOD_S */
#define MAX_COMMANDS_PER_TICK             ( 1 )

/* Minimum number of TASK_PERIOD_S ticks between broadcasts.
 * Should not be faster than 2 seconds to avoid using up the broadcast bandwidth */
#define MIN_TICKS_BETWEEN_BROADCASTS      ( 2 )

/* Timer value when disabled */
#define DEV_MAN_TIMER_DISABLED            ( 0xFFFFFFFF )

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

typedef enum
{
  NodeDiscoveryState_None       = 0x00,
  NodeDiscoveryState_New        = 0x01,
  NodeDiscoveryState_InProgress = 0x02,
}SzlPlugin_DevMan_NodeDiscoveryState_t;

typedef enum
{
  NodeDiscoveryType_None                    = 0x00,
  NodeDiscoveryType_IeeeAddressRequired     = 0x01,
  NodeDiscoveryType_NetworkAddressRequired  = 0x02,
  NodeDiscoveryType_Refresh                 = 0x03,
}SzlPlugin_DevMan_NodeDiscoveryType_t;

/* Information stored per node*/
typedef struct
{
  SzlPlugin_DevMan_NodeDiscoveryState_t DiscoveryState;
  szl_uint16 NetworkAddress;
  szl_uint32 TimeSinceLastComms;
  szl_uint32 TimeSinceLastDiscovery;
  szl_uint64 IeeeAddress;
} SzlPlugin_DevMan_NodeItem_t;

/* Information stored per service */
typedef struct
{
  szl_uint16 MaxSupportedNodes;
  szl_uint16 MaxRequestsOnAir;
  szl_uint16 CurrentRequestsOnAir;
  szl_uint8 TimerId;
  szl_uint32 IncompleteDiscoveryRetryTime;
  szl_uint32 DiscoveryRefreshTime;
  szl_uint32 CommsTimeThreshold;
  szl_uint32 TimeSinceLastBroadcast;
  SzlPlugin_DevMan_AddressChanged_CB_t AddressChangedCallback;
  SzlPlugin_DevMan_UntrackedNodeComms_CB_t UntrackedNodeCommsCallback;
  SzlPlugin_DevMan_DeviceLeft_CB_t DeviceLeftCallback;
  SzlPlugin_DevMan_TimeSinceLastCommsExceedsThreshold_CB_t TimeExceedsThresholdCallback;
  SzlPlugin_DevMan_NodeItem_t NodeItems[1]; // Variable Length Array - Must remain last item in struct
} SzlPlugin_DevMan_Params_t;
#define SzlPlugin_DevMan_Params_t_SIZE( numItems ) ( (sizeof(SzlPlugin_DevMan_Params_t) - sizeof(SzlPlugin_DevMan_NodeItem_t)) + (numItems * sizeof(SzlPlugin_DevMan_NodeItem_t)) )


/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 * Reset a node item
 ******************************************************************************/
static void resetItem(SzlPlugin_DevMan_NodeItem_t* nodeItem)
{
  szl_memset(nodeItem, 0xFF, sizeof(SzlPlugin_DevMan_NodeItem_t));
}

/******************************************************************************
 * Set a node item to default with addresses
 ******************************************************************************/
static void setItem(SzlPlugin_DevMan_NodeItem_t* nodeItem, szl_uint16 NetworkAddress, szl_uint64 IeeeAddress, szl_bool NewItem)
{
  if (NewItem == szl_true)
    {
      nodeItem->DiscoveryState = NodeDiscoveryState_New;
    }
  else
    {
      nodeItem->DiscoveryState = NodeDiscoveryState_None;
    }

  nodeItem->NetworkAddress = NetworkAddress;
  nodeItem->TimeSinceLastComms = 0;
  nodeItem->TimeSinceLastDiscovery = 0;
  nodeItem->IeeeAddress = IeeeAddress;
}

/******************************************************************************
 * Check if a node item requires discovery
 ******************************************************************************/
static SzlPlugin_DevMan_NodeDiscoveryType_t itemReadyForDiscovery(SzlPlugin_DevMan_NodeItem_t* nodeItem, szl_uint32 IncompleteDiscoveryRetryTime, szl_uint32 DiscoveryRefreshTime)
{
  SzlPlugin_DevMan_NodeDiscoveryType_t discoveryType = NodeDiscoveryType_None;

  /* One of the addresses is unknown */
  if ( ( (nodeItem->NetworkAddress == SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN) && (nodeItem->IeeeAddress != SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN) ) ||
       ( (nodeItem->NetworkAddress != SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN) && (nodeItem->IeeeAddress == SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN) ) )
    {
      /* Discovery has not been tried for at least IncompleteDiscoveryRetryTime, or item is new */
      if ( (nodeItem->TimeSinceLastDiscovery >= IncompleteDiscoveryRetryTime) ||
           (nodeItem->DiscoveryState == NodeDiscoveryState_New) )
        {
          if (nodeItem->NetworkAddress == SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN)
            {
              discoveryType = NodeDiscoveryType_NetworkAddressRequired;
            }
          else
            {
              discoveryType = NodeDiscoveryType_IeeeAddressRequired;
            }
        }
    }

  /* Both addresses are known, but it has been a long time since we confirmed the addresses */
  else if ( ( (nodeItem->IeeeAddress != SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN) && (nodeItem->NetworkAddress != SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN) ) &&
            (nodeItem->TimeSinceLastDiscovery >= DiscoveryRefreshTime) )
    {
      discoveryType = NodeDiscoveryType_Refresh;
    }

  return discoveryType;
}

/******************************************************************************
 * Update a pair of addresses
 * Requires valid NetworkAddress and IeeeAddress
 ******************************************************************************/
static void SzlPlugin_DevMan_UpdateAddresses(zabService* Service, SzlPlugin_DevMan_Params_t* pluginParams, szl_uint16 NetworkAddress, szl_uint64 IeeeAddress, szl_bool RecursiveCall)
{
  szl_uint16 index;
  SzlPlugin_DevMan_NodeItem_t* nodeItem;
  szl_bool itemUpdated = szl_false;
  szl_bool notifyItemAddress;

  if ( (NetworkAddress != SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN) && (IeeeAddress != SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN) )
    {
      for (index = 0; index < pluginParams->MaxSupportedNodes; index++)
        {
          notifyItemAddress = szl_false;
          nodeItem = &pluginParams->NodeItems[index];

          /* If the IEEE matches then we want to update the Network address to match.
           * If the network address matches and:
           *  - IEEE is invalid, then update IEEE.
           *  - IEEE does not match, then invalidate the network address of this entry.
           * Once we have updated the entry once any other updates become deletes.
           *  - This handles the case where we had an entry with just the network address and another with just the IEEE  */
          if ( (nodeItem->IeeeAddress == IeeeAddress) ||
               ( (nodeItem->NetworkAddress == NetworkAddress) && (nodeItem->IeeeAddress == SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN) ) )
            {
              /* First item found we update it and keep it */
              if (itemUpdated == szl_false)
                {
                  itemUpdated = szl_true;

                  /* Notify the item if something is changing */
                  if ( (nodeItem->NetworkAddress != NetworkAddress) || (nodeItem->IeeeAddress != IeeeAddress) )
                    {
                      notifyItemAddress = szl_true;
                    }

                  setItem(nodeItem, NetworkAddress, IeeeAddress, szl_false);
                }
              else
                {
                  resetItem(nodeItem);
                }
            }
          else if (nodeItem->NetworkAddress == NetworkAddress)
            {
              nodeItem->NetworkAddress = SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN;
              notifyItemAddress = szl_true;
            }

          /* The item has change so notify app */
          if ( (notifyItemAddress == szl_true) && (pluginParams->AddressChangedCallback != NULL) )
            {
              pluginParams->AddressChangedCallback(Service, nodeItem->NetworkAddress, nodeItem->IeeeAddress);
            }
        }
    }

  /* If item was not found probably a new device announce, then notify app */
  if ( (itemUpdated == szl_false) && (pluginParams->UntrackedNodeCommsCallback != NULL) && (RecursiveCall == szl_false) )
    {
      pluginParams->UntrackedNodeCommsCallback(Service, NetworkAddress, IeeeAddress);

      /* Make a recursive call to ourselves to update the address pair in the case that the app chose to add the new node.*/
      SzlPlugin_DevMan_UpdateAddresses(Service, pluginParams, NetworkAddress, IeeeAddress, szl_true);
    }
}

/******************************************************************************
 * Network & IEEE Address Response handler
 ******************************************************************************/
static void SzlPlugin_DevMan_ZdoAddrRespHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoAddrRespParams_t* Params, szl_uint8 TransactionId)
{
  szl_uint16 index;
  szl_uint16 assocDevIndex;
  SzlPlugin_DevMan_Params_t * pluginParams = NULL;
  SzlPlugin_DevMan_NodeItem_t* nodeItem;

  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DEVICE_MANAGER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return;
    }

  /* Decrement requests on air. If not sensible then clean it up! */
  if ( (pluginParams->CurrentRequestsOnAir > 0) && (pluginParams->CurrentRequestsOnAir <= pluginParams->MaxRequestsOnAir) )
    {
      pluginParams->CurrentRequestsOnAir--;
    }
  else
    {
      printError(Service, "SzlPlugin_DevMan_ZdoAddrRespHandler: CurrentRequestsOnAir Invalid = %d\n", pluginParams->CurrentRequestsOnAir);
      pluginParams->CurrentRequestsOnAir = 0;
    }

  if ( (Status == SZL_STATUS_SUCCESS) && (Params->Status == SZL_ZDO_STATUS_SUCCESS) )
    {
      SzlPlugin_DevMan_UpdateAddresses(Service, pluginParams, Params->NetworkAddress, Params->IeeeAddr, szl_false);

      /* If we have a callback for untracked items process device list for new addresses and notify app */
      if (pluginParams->UntrackedNodeCommsCallback != NULL)
        {
          for (assocDevIndex = 0; assocDevIndex < Params->NumAssocDev; assocDevIndex++)
            {
              for (index = 0; index < pluginParams->MaxSupportedNodes; index++)
                {
                  nodeItem = &pluginParams->NodeItems[index];
                  if (nodeItem->NetworkAddress == Params->AssocDevNetworkAddresses[assocDevIndex])
                    {
                      break;
                    }
                }
              if (index >= pluginParams->MaxSupportedNodes)
                {
                  pluginParams->UntrackedNodeCommsCallback(Service, Params->AssocDevNetworkAddresses[assocDevIndex], SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN);
                }
            }
        }
    }
}

/******************************************************************************
 * Request the IEEE Address of a node from the network
 ******************************************************************************/
static void SzlPlugin_DevMan_GetIeee(zabService* Service, SzlPlugin_DevMan_NodeItem_t* nodeItem)
{
  SZL_RESULT_t szlResult;
  SZL_ZdoIeeeAddrReqParams_t ieeeAddrReqParams;

  ieeeAddrReqParams.NetworkAddress = nodeItem->NetworkAddress;
  ieeeAddrReqParams.RequestType = SZL_ZDO_ADDR_REQ_EXTENDED;
  ieeeAddrReqParams.StartIndex = 0;
  szlResult = SZL_ZDO_IeeeAddrReq(Service,
                                  SzlPlugin_DevMan_ZdoAddrRespHandler,
                                  &ieeeAddrReqParams,
                                  NULL);
  /* Reset time since last discovery as we have tried. Set state to in progress if command send successfully */
  nodeItem->TimeSinceLastDiscovery = 0;
  if (szlResult == SZL_RESULT_SUCCESS)
    {
      nodeItem->DiscoveryState = NodeDiscoveryState_InProgress;
    }
}

/******************************************************************************
 * RSSI Handler - Used as the simplest method to know when a node has communicated
 ******************************************************************************/
static void SzlPlugin_DevMan_ReceivedSignalStrengthHandler(zabService* Service, struct _SZL_ReceivedSignalStrengthNtfParams_t* Params)
{
  szl_uint16 index;
  SzlPlugin_DevMan_Params_t * pluginParams = NULL;
  SzlPlugin_DevMan_NodeItem_t* nodeItem;
  szl_uint16 nwkSourceAddress = SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN;
  szl_uint16 macSourceAddress = SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN;
  szl_bool nwkSourceMatched = szl_false;
  szl_bool macSourceMatched = szl_false;

  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DEVICE_MANAGER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return;
    }

  /* Validate addresses and only process if we have a usable network address */
  if (Params->NwkSourceAddress.AddressMode == SZL_ADDRESS_MODE_NWK_ADDRESS)
    {
      nwkSourceAddress = Params->NwkSourceAddress.Addresses.Nwk.Address;
    }
  if (Params->MacSourceAddress.AddressMode == SZL_ADDRESS_MODE_NWK_ADDRESS)
    {
      macSourceAddress = Params->MacSourceAddress.Addresses.Nwk.Address;
    }
  if ( (nwkSourceAddress != SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN) || (macSourceAddress != SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN) )
    {
      /* Run through the list and reset time for either network address*/
      for (index = 0; index < pluginParams->MaxSupportedNodes; index++)
        {
          nodeItem = &pluginParams->NodeItems[index];

          if ( (nwkSourceAddress != SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN) && (nodeItem->NetworkAddress == nwkSourceAddress) )
            {
              nwkSourceMatched = szl_true;
              nodeItem->TimeSinceLastComms = 0;
            }
          if ( (macSourceAddress != SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN) && (nodeItem->NetworkAddress == macSourceAddress) )
            {
              macSourceMatched = szl_true;
              nodeItem->TimeSinceLastComms = 0;
            }
        }

      /* If either address did not match then notify the app  */
      if ( (index >= pluginParams->MaxSupportedNodes) && (pluginParams->UntrackedNodeCommsCallback != NULL) )
        {
          if ( (nwkSourceAddress != SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN) &&
               (nwkSourceMatched == szl_false) )
            {
              pluginParams->UntrackedNodeCommsCallback(Service, nwkSourceAddress, SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN);
            }
          if ( (macSourceAddress != SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN)  &&
               (macSourceMatched == szl_false) &&
               (macSourceAddress != nwkSourceAddress) )
            {
              pluginParams->UntrackedNodeCommsCallback(Service, macSourceAddress, SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN);
            }
        }
    }
}

/******************************************************************************
 * Device Announce Handler
 ******************************************************************************/
static void SzlPlugin_DevMan_ZdoDeviceAnnounceHandler (zabService* Service, SZL_ZdoDeviceAnnounceIndParams_t* Params)
{
  SzlPlugin_DevMan_Params_t * pluginParams = NULL;

  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DEVICE_MANAGER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return;
    }

  SzlPlugin_DevMan_UpdateAddresses(Service, pluginParams, Params->NwkAddress, Params->IeeeAddress, szl_false);
}

/******************************************************************************
 * Leave Handler
 ******************************************************************************/
static void SzlPlugin_DevMan_NetworkLeaveIndicationHandler(zabService* Service, SZL_NwkLeaveNtfParams_t* Params)
{
  szl_uint16 index;
  SzlPlugin_DevMan_Params_t * pluginParams = NULL;
  SzlPlugin_DevMan_NodeItem_t* nodeItem;
  szl_bool callbackCalled = szl_false;

  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DEVICE_MANAGER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) ||
       (Params->NwkAddress == SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN) )
    {
      return;
    }

  /* Remove any node matching the address */
  for (index = 0; index < pluginParams->MaxSupportedNodes; index++)
    {
      nodeItem = &pluginParams->NodeItems[index];

      if (nodeItem->NetworkAddress == Params->NwkAddress)
        {
          if (pluginParams->DeviceLeftCallback != NULL)
            {
              callbackCalled = szl_true;
              pluginParams->DeviceLeftCallback(Service, nodeItem->NetworkAddress, nodeItem->IeeeAddress);
            }
          resetItem(nodeItem);
        }
    }

  /* If it wasn't in the table then notify of leave anyway but with unknown IEEE */
  if ( (pluginParams->DeviceLeftCallback != NULL) && (callbackCalled == szl_false) )
    {
      pluginParams->DeviceLeftCallback(Service, Params->NwkAddress, SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN);
    }
}

/******************************************************************************
 * Timed Task Function
 ******************************************************************************/
static void SzlPlugin_DevMan_TimerExpired(zabService* Service, szl_uint8 ID, szl_uint32 duration)
{
  szl_uint16 index;
  szl_uint8 commandsSentThisTick;
  SzlPlugin_DevMan_Params_t * pluginParams = NULL;
  SzlPlugin_DevMan_NodeDiscoveryType_t discoveryType;
  SzlPlugin_DevMan_NodeItem_t* nodeItem;
  SZL_ZdoNwkAddrReqParams_t nwkAddrReqParams;
  SZL_RESULT_t szlResult;

  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DEVICE_MANAGER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return;
    }

  /* Update common timers */
  if (pluginParams->TimeSinceLastBroadcast != DEV_MAN_TIMER_DISABLED)
    {
      pluginParams->TimeSinceLastBroadcast++;
    }

  /* Process the table:
   *  - Maximum of MAX_COMMANDS_PER_TICK commands should be generated per tick to control network traffic and ZAB buffer usage.
   *  - Broadcasting will be limited by MIN_TICKS_BETWEEN_BROADCASTS. Should not exceed one every 2 seconds (half the broadcast bandwidth) */
  commandsSentThisTick = 0;
  for (index = 0; index < pluginParams->MaxSupportedNodes; index++)
    {
      nodeItem = &pluginParams->NodeItems[index];

      /* Update item timers */
      if (nodeItem->TimeSinceLastComms != DEV_MAN_TIMER_DISABLED)
        {
          nodeItem->TimeSinceLastComms++;

          if ( (pluginParams->TimeExceedsThresholdCallback != NULL) &&
               (pluginParams->CommsTimeThreshold != SZL_PLUGIN_DEV_MAN_COMMS_TIME_THRESHOLD_DISABLED) &&
               (nodeItem->TimeSinceLastComms == pluginParams->CommsTimeThreshold) )
            {
              pluginParams->TimeExceedsThresholdCallback(Service, nodeItem->NetworkAddress, nodeItem->IeeeAddress, nodeItem->TimeSinceLastComms);
            }
        }
      if (nodeItem->TimeSinceLastDiscovery != DEV_MAN_TIMER_DISABLED)
        {
          nodeItem->TimeSinceLastDiscovery++;
        }

      /* Run discovery, but limit the number of commands generated per tick to MAX_COMMANDS_PER_TICK */
      if ( (commandsSentThisTick < MAX_COMMANDS_PER_TICK) && (pluginParams->CurrentRequestsOnAir < pluginParams->MaxRequestsOnAir) )
        {
          discoveryType = itemReadyForDiscovery(nodeItem, pluginParams->IncompleteDiscoveryRetryTime, pluginParams->DiscoveryRefreshTime);
          switch (discoveryType)
            {
              /* Treat Refresh like an IEEE Addr Req, as this is unicast and more efficient than a broadcast */
              case NodeDiscoveryType_IeeeAddressRequired:
              case NodeDiscoveryType_Refresh:
                commandsSentThisTick++;
                pluginParams->CurrentRequestsOnAir++;
                SzlPlugin_DevMan_GetIeee(Service, nodeItem);
                break;

              case NodeDiscoveryType_NetworkAddressRequired:
                if (pluginParams->TimeSinceLastBroadcast >= MIN_TICKS_BETWEEN_BROADCASTS)
                  {
                    nwkAddrReqParams.IeeeAddress = nodeItem->IeeeAddress;
                    nwkAddrReqParams.RequestType = SZL_ZDO_ADDR_REQ_EXTENDED;
                    nwkAddrReqParams.StartIndex = 0;
                    szlResult = SZL_ZDO_NwkAddrReq(Service,
                                                   SzlPlugin_DevMan_ZdoAddrRespHandler,
                                                   &nwkAddrReqParams,
                                                   NULL);
                    /* Reset time since last discovery as we have tried.
                     * If command send successfully:
                     *  - Set state to in progress
                     *  - Reset time since last broadcast */
                    commandsSentThisTick++;
                    pluginParams->CurrentRequestsOnAir++;
                    nodeItem->TimeSinceLastDiscovery = 0;
                    if (szlResult == SZL_RESULT_SUCCESS)
                      {
                        nodeItem->DiscoveryState = NodeDiscoveryState_InProgress;
                        pluginParams->TimeSinceLastBroadcast = 0;
                      }
                  }
                break;

              case NodeDiscoveryType_None:
              default:
                ; /* Do nothing */
            }
        }
    }
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/**
 * Device Manager - Initialization
 *
 * This function must be called from the application after the Library has been initialized.
 *
 * @param[in]  Service                        Service Instance Pointer
 * @param[in]  MaxSupportedNodes              Number of nodes supported by the manager
 *                                             - RAM required will scale linearly
 * @param[in]  MaxRequestsOnAir               Number of discovery requests the manager may have in process at a time
 *                                             - Controls how many transaction buffers are used by the manager
 *                                             - Recommend a fraction of SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS
 * @param[in]  IncompleteDiscoveryRetryTime   Time after which addresses of a node with either address unknown will be retried
 *                                             - Must not be less than SZL_PLUGIN_DEV_MAN_INCOMPLETE_DISCOVERY_RETRY_TIME_MIN
 *                                             - Recommend SZL_PLUGIN_DEV_MAN_INCOMPLETE_DISCOVERY_RETRY_TIME_DEFAULT
 * @param[in]  DiscoveryRefreshTime           Time after which addresses of a node with known addresses will be refreshed
 *                                             - Must not be less than SZL_PLUGIN_DEV_MAN_DISCOVERY_REFRESH_TIME_MIN
 *                                             - Recommend SZL_PLUGIN_DEV_MAN_DISCOVERY_REFRESH_TIME_DEFAULT
 * @param[in]  CommsTimeThreshold             Time in seconds of no communication after which application will be notified via TimeExceedsThresholdCallback
 *                                             - SZL_PLUGIN_DEV_MAN_COMMS_TIME_THRESHOLD_DISABLED if not required
 * @param[in]  AddressChangedCallback         OPTIONAL - Called on change of any Network Address to IEEE Address mapping
 * @param[in]  UntrackedNodeCommsCallback     OPTIONAL - Called if a message is received from a node that is not being tracked
 * @param[in]  DeviceLeftCallback             OPTIONAL - Called if a node is detected leaving the network
 * @param[in]  TimeExceedsThresholdCallback   OPTIONAL - Called if a node has failed to communicate for the time threshold
 *
 * @return     SZL_RESULT_t                   SZL_RESULT_SUCCESS if successful.
 */
extern
SZL_RESULT_t SzlPlugin_DevMan_Init(zabService* Service,
                                   szl_uint16 MaxSupportedNodes,
                                   szl_uint16 MaxRequestsOnAir,
                                   szl_uint32 IncompleteDiscoveryRetryTime,
                                   szl_uint32 DiscoveryRefreshTime,
                                   szl_uint32 CommsTimeThreshold,
                                   SzlPlugin_DevMan_AddressChanged_CB_t AddressChangedCallback,
                                   SzlPlugin_DevMan_UntrackedNodeComms_CB_t UntrackedNodeCommsCallback,
                                   SzlPlugin_DevMan_DeviceLeft_CB_t DeviceLeftCallback,
                                   SzlPlugin_DevMan_TimeSinceLastCommsExceedsThreshold_CB_t TimeExceedsThresholdCallback)
{
  SZL_RESULT_t result = SZL_RESULT_SUCCESS;
  SzlPlugin_DevMan_Params_t * pluginParams = NULL;
  szl_bool pluginRegistered = szl_false;
  szl_bool rssiCallbackRegistered = szl_false;
  szl_bool deviceAnnounceCallbackRegistered = szl_false;
  szl_bool leaveNtfCallbackRegistered = szl_false;

  /* Validate inputs */
  if ( (Service == NULL) || (MaxSupportedNodes == 0) ||
       (IncompleteDiscoveryRetryTime < SZL_PLUGIN_DEV_MAN_INCOMPLETE_DISCOVERY_RETRY_TIME_MIN) ||
       (DiscoveryRefreshTime < SZL_PLUGIN_DEV_MAN_DISCOVERY_REFRESH_TIME_MIN) )
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /* Malloc then initialise parameters the plugin needs to store and link from the SZL */
  result = SZL_PluginRegister(Service, SZL_PLUGIN_ID_DEVICE_MANAGER, SzlPlugin_DevMan_Params_t_SIZE(MaxSupportedNodes), (void**)&pluginParams);
  if (result == SZL_RESULT_SUCCESS)
    {
      pluginRegistered = szl_true;

      if (pluginParams != NULL)
        {
          pluginParams->MaxSupportedNodes = MaxSupportedNodes;
          pluginParams->MaxRequestsOnAir = MaxRequestsOnAir;
          pluginParams->CurrentRequestsOnAir = 0;
          pluginParams->IncompleteDiscoveryRetryTime = IncompleteDiscoveryRetryTime;
          pluginParams->DiscoveryRefreshTime = DiscoveryRefreshTime;
          pluginParams->CommsTimeThreshold = CommsTimeThreshold;
          pluginParams->AddressChangedCallback = AddressChangedCallback;
          pluginParams->UntrackedNodeCommsCallback = UntrackedNodeCommsCallback;
          pluginParams->DeviceLeftCallback = DeviceLeftCallback;
          pluginParams->TimeExceedsThresholdCallback = TimeExceedsThresholdCallback;
          /* Memset to defaults - careful!*/
          szl_memset(pluginParams->NodeItems, 0xFF, sizeof(SzlPlugin_DevMan_NodeItem_t)*MaxSupportedNodes);

          /* Register callbacks and timer */
          result = SZL_ReceivedSignalStrengthNtfRegisterCB(Service, SzlPlugin_DevMan_ReceivedSignalStrengthHandler);
          if (result == SZL_RESULT_SUCCESS)
            {
              rssiCallbackRegistered = szl_true;

              result = SZL_ZDO_DeviceAnnounceNtfRegisterCB(Service, SzlPlugin_DevMan_ZdoDeviceAnnounceHandler);
              if (result == SZL_RESULT_SUCCESS)
                {
                  deviceAnnounceCallbackRegistered = szl_true;

                  result = SZL_NwkLeaveNtfRegisterCB(Service, SzlPlugin_DevMan_NetworkLeaveIndicationHandler);
                  if (result == SZL_RESULT_SUCCESS)
                    {
                      leaveNtfCallbackRegistered = szl_true;

                      /* Start our 1 second timer to process the table and state machines */
                      pluginParams->TimerId = Szl_ExtTimerRegister(Service, szl_true, TASK_PERIOD_S, SzlPlugin_DevMan_TimerExpired);
                      Szl_ExtTimerStart(Service, pluginParams->TimerId);
                      if(pluginParams->TimerId > 0)
                        {
                          result = SZL_RESULT_SUCCESS;
                        }
                      else
                        {
                          result = SZL_RESULT_TABLE_FULL;
                        }
                    }
                }
            }
        }
      else
        {
          result = SZL_RESULT_INTERNAL_LIB_ERROR;
        }
    }

  /* If we failed then de-register anything we managed to get done */
  if (result != SZL_RESULT_SUCCESS)
    {
      printError(Service, "SzlPlugin_DevMan_Init: Failed 0x%02X\n", result);

      if (leaveNtfCallbackRegistered == szl_true)
        {
          SZL_NwkLeaveNtfUnregisterCB(Service, SzlPlugin_DevMan_NetworkLeaveIndicationHandler);
        }
      if (deviceAnnounceCallbackRegistered == szl_true)
        {
          SZL_ReceivedSignalStrengthNtfUnregisterCB(Service, SzlPlugin_DevMan_ReceivedSignalStrengthHandler);
        }
      if (rssiCallbackRegistered == szl_true)
        {
          SZL_ReceivedSignalStrengthNtfUnregisterCB(Service, SzlPlugin_DevMan_ReceivedSignalStrengthHandler);
        }
      if (pluginRegistered == szl_true)
        {
          SZL_PluginUnregister(Service, SZL_PLUGIN_ID_DEVICE_MANAGER);
        }
    }

  return result;


}

/**
 * Device Manager - Destroy
 *
 * @param[in]  Service                Service Instance Pointer
 *
 * @return     SZL_RESULT_t           SZL_RESULT_SUCCESS if successful.
 */
SZL_RESULT_t SzlPlugin_DevMan_Destroy(zabService* Service)
{
  SzlPlugin_DevMan_Params_t * pluginParams = NULL;

  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DEVICE_MANAGER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_OPERATION_NOT_POSSIBLE;
    }

  /* Make best effort to destroy callbacks and timer */
  Szl_ExtTimerUnregister(Service, pluginParams->TimerId);
  SZL_ZDO_DeviceAnnounceNtfUnregisterCB(Service, SzlPlugin_DevMan_ZdoDeviceAnnounceHandler);
  SZL_ReceivedSignalStrengthNtfUnregisterCB(Service, SzlPlugin_DevMan_ReceivedSignalStrengthHandler);
  SZL_NwkLeaveNtfUnregisterCB(Service, SzlPlugin_DevMan_NetworkLeaveIndicationHandler);

  return SZL_PluginUnregister(Service, SZL_PLUGIN_ID_DEVICE_MANAGER);
}

/**
 * Device Manager - Load by Network Address
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  NetworkAddress     Network address of the node
 *
 * @return     SZL_RESULT_t       SZL_RESULT_SUCCESS if successful
 *                                SZL_RESULT_ALREADY_EXISTS if already in the device manager
 *                                Other on error
 */
SZL_RESULT_t SzlPlugin_DevMan_LoadByNetworkAddress(zabService* Service, szl_uint16 NetworkAddress)
{
  szl_uint16 index;
  szl_uint16 freeIndex;
  SzlPlugin_DevMan_Params_t * pluginParams = NULL;
  SzlPlugin_DevMan_NodeItem_t* nodeItem;
  SZL_RESULT_t szlResult = SZL_RESULT_SUCCESS;

  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DEVICE_MANAGER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_OPERATION_NOT_POSSIBLE;
    }
  if (NetworkAddress == SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN)
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /* Check address does not already exist. Save first free index to add at if it doesn't already exist */
  freeIndex = pluginParams->MaxSupportedNodes;
  for (index = 0; index < pluginParams->MaxSupportedNodes; index++)
    {
      nodeItem = &pluginParams->NodeItems[index];

      if (nodeItem->NetworkAddress == NetworkAddress)
        {
          szlResult = SZL_RESULT_ALREADY_EXISTS;
          break;
        }

      if ( (freeIndex >= pluginParams->MaxSupportedNodes) &&
           (nodeItem->NetworkAddress == SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN) &&
           (nodeItem->IeeeAddress == SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN) )
        {
          freeIndex = index;
        }
    }

  /* If no errors and valid index, then add item */
  if (szlResult == SZL_RESULT_SUCCESS)
    {
      if (freeIndex < pluginParams->MaxSupportedNodes)
        {
          setItem(&pluginParams->NodeItems[freeIndex], NetworkAddress, SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN, szl_true);
        }
      else
        {
          szlResult = SZL_RESULT_TABLE_FULL;
        }
    }
  return szlResult;
}

/**
 * Device Manager - Load by IEEE Address
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  IeeeAddress        IEEE Address of the node
 *
 * @return     SZL_RESULT_t       SZL_RESULT_SUCCESS if successful.
 *                                SZL_RESULT_ALREADY_EXISTS if already in the device manager.
 *                                Other on error.
 */
SZL_RESULT_t SzlPlugin_DevMan_LoadByIeeeAddress(zabService* Service, szl_uint64 IeeeAddress)
{
  szl_uint16 index;
  szl_uint16 freeIndex;
  SzlPlugin_DevMan_Params_t * pluginParams = NULL;
  SzlPlugin_DevMan_NodeItem_t* nodeItem;
  SZL_RESULT_t szlResult = SZL_RESULT_SUCCESS;

  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DEVICE_MANAGER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_OPERATION_NOT_POSSIBLE;
    }
  if (IeeeAddress == SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN)
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /* Check address does not already exist. Save first free index to add at if it doesn't already exist */
  freeIndex = pluginParams->MaxSupportedNodes;
  for (index = 0; index < pluginParams->MaxSupportedNodes; index++)
    {
      nodeItem = &pluginParams->NodeItems[index];

      if (nodeItem->IeeeAddress == IeeeAddress)
        {
          szlResult = SZL_RESULT_ALREADY_EXISTS;
          break;
        }

      if ( (freeIndex >= pluginParams->MaxSupportedNodes) &&
           (nodeItem->NetworkAddress == SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN) &&
           (nodeItem->IeeeAddress == SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN) )
        {
          freeIndex = index;
        }
    }

  /* If no errors and valid index, then add item */
  if (szlResult == SZL_RESULT_SUCCESS)
    {
      if (freeIndex < pluginParams->MaxSupportedNodes)
        {
          setItem(&pluginParams->NodeItems[freeIndex], SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN, IeeeAddress, szl_true);
        }
      else
        {
          szlResult = SZL_RESULT_TABLE_FULL;
        }
    }
  return szlResult;
}

/**
 * Device Manager - Remove by Network Address
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  NetworkAddress     Network address of the node
 *
 * @return     SZL_RESULT_t       SZL_RESULT_SUCCESS if successful
 *                                Other on error.
 */
SZL_RESULT_t SzlPlugin_DevMan_RemoveByNetworkAddress(zabService* Service, szl_uint16 NetworkAddress)
{
  szl_uint16 index;
  SzlPlugin_DevMan_Params_t * pluginParams = NULL;
  SzlPlugin_DevMan_NodeItem_t* nodeItem;
  SZL_RESULT_t szlResult = SZL_RESULT_NOT_FOUND;

  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DEVICE_MANAGER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_OPERATION_NOT_POSSIBLE;
    }
  if (NetworkAddress == SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN)
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /* Remove any node matching the address */
  for (index = 0; index < pluginParams->MaxSupportedNodes; index++)
    {
      nodeItem = &pluginParams->NodeItems[index];

      if (nodeItem->NetworkAddress == NetworkAddress)
        {
          szlResult = SZL_RESULT_SUCCESS;
          resetItem(nodeItem);
        }
    }

  return szlResult;
}

/**
 * Device Manager - Remove by IEEE Address
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  IeeeAddress        IEEE Address of the node
 *
 * @return     SZL_RESULT_t       SZL_RESULT_SUCCESS if successful
 *                                Other on error.
 */
SZL_RESULT_t SzlPlugin_DevMan_RemoveByIeeeAddress(zabService* Service, szl_uint64 IeeeAddress)
{
  szl_uint16 index;
  SzlPlugin_DevMan_Params_t * pluginParams = NULL;
  SzlPlugin_DevMan_NodeItem_t* nodeItem;
  SZL_RESULT_t szlResult = SZL_RESULT_NOT_FOUND;

  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DEVICE_MANAGER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_OPERATION_NOT_POSSIBLE;
    }
  if (IeeeAddress == SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN)
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /* Remove any node matching the address */
  for (index = 0; index < pluginParams->MaxSupportedNodes; index++)
    {
      nodeItem = &pluginParams->NodeItems[index];

      if (nodeItem->IeeeAddress == IeeeAddress)
        {
          szlResult = SZL_RESULT_SUCCESS;
          resetItem(nodeItem);
        }
    }

  return szlResult;
}

/**
 * Device Manager - Get IEEE Address and Time Since Last Comms from Network Address
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  NetworkAddress     Network address of the node
 * @param[out] IeeeAddress        IEEE Address of the node
 *                                 - Set NULL if not required
 *                                 - SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN if not found
 * @param[out] TimeSinceLastComms Time in seconds since last message was received from the device
 *                                 - Set NULL if not required
 *                                 - SZL_PLUGIN_DEV_MAN_TIME_SINCE_LAST_COMMS_UNKNOWN if not found no never communicated
 *
 * @return     SZL_RESULT_t       SZL_RESULT_SUCCESS if successful
 *                                Other on error.
 */
SZL_RESULT_t SzlPlugin_DevMan_GetByNetworkAddress(zabService* Service, szl_uint16 NetworkAddress, szl_uint64* IeeeAddress, szl_uint32* TimeSinceLastComms)
{
  szl_uint16 index;
  SzlPlugin_DevMan_Params_t * pluginParams = NULL;
  SzlPlugin_DevMan_NodeItem_t* nodeItem;
  SZL_RESULT_t szlResult = SZL_RESULT_NOT_FOUND;

  /* Set defaults */
  if (IeeeAddress != NULL)
    {
      *IeeeAddress = SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN;
    }
  if (TimeSinceLastComms != NULL)
    {
      *TimeSinceLastComms = SZL_PLUGIN_DEV_MAN_TIME_SINCE_LAST_COMMS_UNKNOWN;
    }

  /* Validate parameters and get data pointer */
  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DEVICE_MANAGER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_OPERATION_NOT_POSSIBLE;
    }
  if (NetworkAddress == SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN)
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /* Search for matching item */
  for (index = 0; index < pluginParams->MaxSupportedNodes; index++)
    {
      nodeItem = &pluginParams->NodeItems[index];

      if (nodeItem->NetworkAddress == NetworkAddress)
        {
          szlResult = SZL_RESULT_SUCCESS;
          if (IeeeAddress != NULL)
            {
              *IeeeAddress = nodeItem->IeeeAddress;
            }
          if (TimeSinceLastComms != NULL)
            {
              *TimeSinceLastComms = nodeItem->TimeSinceLastComms;
            }
          break;
        }
    }

  return szlResult;
}

/**
 * Device Manager - Get Network Address and Time Since Last Comms from IEEE Address
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  IeeeAddress        IEEE Address of the node
 * @param[out] NetworkAddress     Network address of the node
 *                                 - Set NULL if not required
 *                                 - SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN if not found
 * @param[out] TimeSinceLastComms Time in seconds since last message was received from the device
 *                                 - Set NULL if not required
 *                                 - SZL_PLUGIN_DEV_MAN_TIME_SINCE_LAST_COMMS_UNKNOWN if not found no never communicated
 *
 * @return     SZL_RESULT_t       SZL_RESULT_SUCCESS if successful
 *                                Other on error.
 */
SZL_RESULT_t SzlPlugin_DevMan_GetByIeeeAddress(zabService* Service, szl_uint64 IeeeAddress, szl_uint16* NetworkAddress, szl_uint32* TimeSinceLastComms)
{
  szl_uint16 index;
  SzlPlugin_DevMan_Params_t * pluginParams = NULL;
  SzlPlugin_DevMan_NodeItem_t* nodeItem;
  SZL_RESULT_t szlResult = SZL_RESULT_NOT_FOUND;

  /* Set defaults */
  if (NetworkAddress != NULL)
    {
      *NetworkAddress = SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN;
    }
  if (TimeSinceLastComms != NULL)
    {
      *TimeSinceLastComms = SZL_PLUGIN_DEV_MAN_TIME_SINCE_LAST_COMMS_UNKNOWN;
    }

  /* Validate parameters and get data pointer */
  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DEVICE_MANAGER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_OPERATION_NOT_POSSIBLE;
    }
  if (IeeeAddress == SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN)
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /* Search for matching item */
  for (index = 0; index < pluginParams->MaxSupportedNodes; index++)
    {
      nodeItem = &pluginParams->NodeItems[index];

      if (nodeItem->IeeeAddress == IeeeAddress)
        {
          szlResult = SZL_RESULT_SUCCESS;

          if (NetworkAddress != NULL)
            {
              *NetworkAddress = nodeItem->NetworkAddress;
            }
          if (TimeSinceLastComms != NULL)
            {
              *TimeSinceLastComms = nodeItem->TimeSinceLastComms;
            }
          break;
        }
    }

  return szlResult;
}

/**
 * Device Manager - Get Network Address and Time Since Last Comms from IEEE Address
 *
 * @param[in]  Service            Service Instance Pointer
 * @param[in]  Index              Table index to read from
 * @param[out] NetworkAddress     Network address of the node
 *                                 - Set NULL if not required
 *                                 - SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN if not found
 * @param[out] IeeeAddress        IEEE Address of the node
 *                                 - Set NULL if not required
 *                                 - SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN if not found
 * @param[out] TimeSinceLastComms Time in seconds since last message was received from the device
 *                                 - Set NULL if not required
 *                                 - SZL_PLUGIN_DEV_MAN_TIME_SINCE_LAST_COMMS_UNKNOWN if not found no never communicated
 *
 * @return     SZL_RESULT_t       SZL_RESULT_SUCCESS if successful
 *                                SZL_RESULT_OUT_OF_RANGE is beyond the limits of the table
 *                                Other on error.
 */
SZL_RESULT_t SzlPlugin_DevMan_GetByIndex(zabService* Service, szl_uint16 Index, szl_uint16* NetworkAddress, szl_uint64* IeeeAddress, szl_uint32* TimeSinceLastComms)
{
  SzlPlugin_DevMan_Params_t * pluginParams = NULL;
  SzlPlugin_DevMan_NodeItem_t* nodeItem;
  SZL_RESULT_t szlResult = SZL_RESULT_OUT_OF_RANGE;

  /* Set defaults */
  if (NetworkAddress != NULL)
    {
      *NetworkAddress = SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN;
    }
  if (IeeeAddress != NULL)
    {
      *IeeeAddress = SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN;
    }
  if (TimeSinceLastComms != NULL)
    {
      *TimeSinceLastComms = SZL_PLUGIN_DEV_MAN_TIME_SINCE_LAST_COMMS_UNKNOWN;
    }

  /* Validate parameters and get data pointer */
  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DEVICE_MANAGER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_OPERATION_NOT_POSSIBLE;
    }

  /* Access data by index */
  if (Index < pluginParams->MaxSupportedNodes)
    {
      nodeItem = &pluginParams->NodeItems[Index];

      szlResult = SZL_RESULT_SUCCESS;

      if (NetworkAddress != NULL)
        {
          *NetworkAddress = nodeItem->NetworkAddress;
        }
      if (IeeeAddress != NULL)
        {
          *IeeeAddress = nodeItem->IeeeAddress;
        }
      if (TimeSinceLastComms != NULL)
        {
          *TimeSinceLastComms = nodeItem->TimeSinceLastComms;
        }
    }

  return szlResult;
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/