# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to store the structure of frames
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from enum import Enum


class Command(Enum):
    CONTROL = 0x00
    DATA = 0x01
    ACK = 0x02


class Type(Enum):
    STATE = 0x00
    DEVICE_TYPE = 0x01
    ID_NETWORK = 0x02
    CONFIG = 0x03


class StatusDisplay:

    def __init__(self):

        self.status_string_display = {
            Status.ACK_SUCCESS.value: 'ACK_SUCCESS',
            Status.ACK_FAILED.value: 'ACK_FAILED',
            Status.ACK_BAD_CRC.value: 'ACK_BAD_CRC',
            Status.ACK_BAD_FRAME_COUNTER.value: 'ACK_BAD_FRAME_COUNTER',
            Status.ACK_INVALID_COMMAND.value: 'ACK_INVALID_COMMAND',
            Status.ACK_UNKNOWN_ERROR.value: 'ACK_UNKNOWN_ERROR',
            Status.ACK_RETRY_EXPIRED.value: 'ACK_RETRY_EXPIRED',
            Status.SEND_NO_ACK.value: 'SEND_NO_ACK'}


class Status(Enum):
    ACK_SUCCESS = 0x00
    ACK_FAILED = 0x01
    ACK_BAD_CRC = 0x02
    ACK_BAD_FRAME_COUNTER = 0x03
    ACK_INVALID_COMMAND = 0x04
    ACK_UNKNOWN_ERROR = 0x05
    ACK_RETRY_EXPIRED = 0x06
    SEND_NO_ACK = 0x07


class Network(Enum):
    NETWORK = 0x00
    DISCOVERY = 0x01
    ADVERTISING = 0x02


class DeviceType(Enum):
    PEER = 0x00
    CONCENTRATOR = 0x01


class NetworkState(Enum):
    UNINITIALIZED = 0x00
    NONE = 0x01
    NETWORKING = 0x02
    NETWORKED = 0x03
    NETWORK_LOSS = 0x04


class DiscoveryState(Enum):
    NO_DISCOVERY = 0x00
    DISCOVERING = 0x01
    DISCOVERY_COMPLETE = 0x02


class AdvertisingState(Enum):
    ADVERTISING = 0x00
    ADVERTISING_COMPLETE = 0x01
