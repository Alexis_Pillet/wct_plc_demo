# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to manage management of serialDriver & frame generation/management
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************

from mac import *
from logger import get_logger
import serial
import sys
import time
from queue import Queue, Empty
from threading import Thread
from random import randint


class SerialProcess:

    def __init__(self):
        self._serial_driver = None
        self._mac_queue = None

    def start(self, queue):
        # Save mac engine's queue
        self._mac_queue = queue
        # Start serial driver
        return self._start_serial_driver()

    def stop(self):
        get_logger().info('Clear program & close serialDriver...')
        # Stop serial driver
        self._stop_serial_driver()
        get_logger().info('Done!')

    def _stop_serial_driver(self):
        self._serial_driver.stop()
        self._serial_driver.join()
        self._serial_driver.close()

    def _start_serial_driver(self, default_baud_rate=115200, default_rts=None, default_dtr=None, port='COM9'):

        # Suppress non-error messages
        quiet = False
        # Show Python traceback on error
        develop = False

        while True:
            try:
                try:
                    port = ask_for_port()
                except KeyboardInterrupt:
                    sys.stderr.write('\n')

                serial_instance = serial.serial_for_url(
                    'spy://' + port + '?file=serial_log.log',
                    default_baud_rate,
                    parity='N',
                    rtscts=False,
                    xonxoff=False,
                    do_not_open=True,
                    timeout=0.05)

                if not hasattr(serial_instance, 'cancel_read'):
                    # enable timeout for alive flag polling if cancel_read is not available
                    serial_instance.timeout = 1

                if default_dtr is not None:
                    if not quiet:
                        sys.stderr.write('--- forcing DTR {}\n'.format('active' if default_dtr else 'inactive'))
                    serial_instance.dtr = default_dtr
                if default_rts is not None:
                    if not quiet:
                        sys.stderr.write('--- forcing RTS {}\n'.format('active' if default_rts else 'inactive'))
                    serial_instance.rts = default_rts
                serial_instance.open()
            except serial.SerialException as e:
                sys.stderr.write('could not open port {!r}: {}\n'.format(port, e))
                if develop:
                    raise
                sys.exit(1)
            else:
                break

        self._serial_driver = SerialDriver(
            serial_instance,
            self._mac_queue)
        self._serial_driver.set_rx_encoding('hexlify')
        self._serial_driver.set_tx_encoding('hexlify')

        if not quiet:
            sys.stderr.write('--- Miniterm on {p.name}  {p.baudrate},{p.bytesize},{p.parity},{p.stopbits} ---\n'.format(
                p=self._serial_driver.serial_instance))

        self._serial_driver.start()
        return self._serial_driver


class MacProcess:

    def __init__(self):
        self._serial_process = SerialProcess()
        self._debug = False
        self._current_state = self.UNINITIALIZED_WAITING_DEVICE_STATE
        self._serial_driver = None
        self._queue = Queue()
        self._network_queue = None
        self._mac_frame_generator = None
        self._device_number = []
        self._mac_process_thread = None
        self._tread_alive = False
        self._automate_function = {  # Association between state and function to execute
            self.UNINITIALIZED_WAITING_DEVICE_STATE: self.process_uninitialized_waiting_device_state,
            self.UNINITIALIZED_WAITING_ACK_CONFIGURATION_STATE:
                self.process_uninitialized_waiting_ack_configuration_state,
            self.UNINITIALIZED_WAITING_STATUS_NONE_STATE: self.process_uninitialized_waiting_status_none_state,
            self.NETWORKING_STATE: self.process_networking_state,
            self.NETWORKED_STATE: self.process_networked_state}

    # State of automate
    UNINITIALIZED_WAITING_DEVICE_STATE = 0
    UNINITIALIZED_WAITING_ACK_CONFIGURATION_STATE = 1
    UNINITIALIZED_WAITING_STATUS_NONE_STATE = 2
    NONE_STATE = 3
    NETWORKING_STATE = 4
    NETWORKED_STATE = 5

    @property
    def network_queue(self):
        return self._network_queue

    @network_queue.setter
    def network_queue(self, network_queue):
        self._network_queue = network_queue

    @property
    def mac_frame_generator(self):
        return self._mac_frame_generator

    @mac_frame_generator.setter
    def mac_frame_generator(self, frame_generator):
        self._mac_frame_generator = frame_generator

    def process_uninitialized_waiting_device_state(self, data):

        # If good frame
        if is_uninitialized_state_frame(data):

            # Update state
            self._current_state = self.UNINITIALIZED_WAITING_ACK_CONFIGURATION_STATE
            # Sleep for 10 ms => little delay in order that PLC Concentrator process the frame
            time.sleep(0.01)
            # Send configuration frame concentrator
            self._mac_frame_generator.send_configuration_concentrator(self._mac_frame_generator.DEFAULT_MAC_ADDRESS,
                                                                      self._mac_frame_generator.DEFAULT_PAN_ID)
            if self._debug:
                get_logger().info('Status - PLC Network UNINITIALIZED')
        else:
            get_logger().error('BAD FRAME - PLC Network UNINITIALIZED')

    def process_uninitialized_waiting_ack_configuration_state(self, data):

        # if good frame
        if is_ack_success_frame(data):
            # Update state
            self._current_state = self.UNINITIALIZED_WAITING_STATUS_NONE_STATE

            if self._debug:
                get_logger().info('Status - PLC Network CONFIGURED')
        # Reset of the process realized by PLC concentrator
        elif is_uninitialized_state_frame(data):
            self.process_uninitialized_waiting_device_state(data)
        else:
            get_logger().error('BAD FRAME - PLC Network CONFIGURED')

    def process_uninitialized_waiting_status_none_state(self, data):

        # If good frame
        if is_none_state_frame(data):

            # Update state
            self._current_state = self.NONE_STATE
            # No action to perform or waiting data for state None
            self.process_none_state()

            if self._debug:
                get_logger().info('Status - PLC Network NONE')
        # Reset of the process realized by PLC concentrator
        elif is_uninitialized_state_frame(data):
            self.process_uninitialized_waiting_device_state(data)
        else:
            get_logger().error('BAD FRAME - PLC Network NONE')

    def process_none_state(self):
        # Update state
        self._current_state = self.NETWORKING_STATE

    def process_networking_state(self, data):

        # If good frame
        if is_networking_state_frame(data):

            # Update state
            self._current_state = self.NETWORKED_STATE

            if self._debug:
                get_logger().info('Status - PLC Network CREATED')

        # Reset of the process realized by PLC concentrator
        elif is_uninitialized_state_frame(data):
            self.process_uninitialized_waiting_device_state(data)
        else:
            get_logger().error('BAD FRAME - PLC Network CREATED')

    def process_networked_state(self, data):

        # If the PLC concentrator resets
        if is_uninitialized_state_frame(data):

            self.process_uninitialized_waiting_device_state(data)

        else:
            # Check if good frame
            # Good_frame / Device number / Mac address
            result = is_networked_state_frame(data)

            # If good frame
            if result["result"]:

                if self._debug:
                    get_logger().info('Status - PLC Network RUNNING')
                    get_logger().debug('Device number - {}, Mac address - {}'.format(result["device_number"],
                                                                                     result["mac_address"]))

                self._device_number.append(result["device_number"])

            else:

                # Check if good frame
                # Good_frame Data / Device number / Payload
                result = is_data_frame(data)

                # If good frame
                if result["result"]:

                    if self._debug:
                        get_logger().debug('MAC PROCESS: Data - {} - {}'.format(result["payload"][0],
                                                                                result["payload"][1:].hex()))

                    # Send Data frame to network layout
                    self._network_queue.put({"device_number": result["payload"][0], "frame": result["payload"][1:]})

    # Activate/Deactivate debug
    def set_debug(self, value):
        self._debug = value

    def start(self):
        # Start serial process
        self._serial_driver = self._serial_process.start(self._queue)
        # Create Frame manager
        self._mac_frame_generator = MacFrameGenerator(self._serial_driver)
        # start thread to treat frame from serial driver
        self._tread_alive = True
        self._mac_process_thread = Thread(target=self._treat_data, name='treat', args=())
        self._mac_process_thread.daemon = True
        self._mac_process_thread.start()

    def stop(self):
        # Stop serial process
        self._serial_process.stop()
        # Stop thread
        self._tread_alive = False
        self._mac_process_thread.join()

    def _treat_data(self):

        while self._tread_alive:

            try:
                data = self._queue.get(True, 0.1)
            except Empty:
                continue

            # Pass new data to mac_engine to treat it
            self._automate_function[self._current_state](data)

            # Good_frame
            result = is_ack_error_frame(data)

            # If good frame
            if result["result"]:

                if self._debug:
                    get_logger().error('ACK - {}'.format(StatusDisplay().status_string_display[result["ack_status"]]))

            self._queue.task_done()
