/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the main interface for the ZAB ZNP Vendor - HEADER
 *
 * Versioning:
 * Versions are Major.Minor.Release.Build (each number is a byte and displayed as decimal
 * in this dot notation). A major number change is not backward compatible with other major
 * releases, unless specified. Minor number change is backward compatible and have new
 * features. Releases are bug fixes or enhancements. Builds are given to QA and beta customers.
 * Build number is not shown for product release.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Core Rev      Date     Author  Change Description
 * 00.00.01.00  27-Aug-13   MvdB   Rework. Rebase version numbers to 0.0.1.0
 * 00.00.03.00  27-Jan-14   MvdB   0.0.3 Release
 * 00.00.04.00  14-Apr-14   MvdB   0.0.4 Release
 * 00.00.05.00  10-Jun-14   MvdB   0.0.5 Release
 * 00.00.06.00  12-Sep-14   MvdB   0.0.6 Release
 * 00.00.06.02  30-Sep-14   MvdB   artf55512: Support network key change action
 * 00.00.06.03  01-Oct-14   MvdB   artf104879: Support energy scans via Mgmt Nwk Update Request
 * 00.00.06.04  07-Oct-14   MvdB   artf104116: Add RSSI to received packet quality indication
 *                                 artf56243: Review handling of received Leave commands
 *                                 artf105222: Improve ReportGeneratorSend() handling of failures
 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 *                                 ARTF104098: Move mutex config to zabMutexConfigure.h
 * 00.00.06.100 15-oct-14   MvdB   0.0.6.100 - GP Release for SLIPZ. Referred to as 0.0.6.0.GP
 * 00.00.06.101 15-oct-14   MvdB   0.0.6.101 - GP Release for SLIPZ. Merge in ZAB 00.00.06.00B EPID rollback
 * 00.00.06.06  30-Oct-14   MvdB   artf105931: Disable default response on ZCL responses
 *                                 artf105017: Update ZAB_VND_CFG_ZNP_M_NWK_CONFIRM_TIMEOUT_MS
 *                                 artf105443: Make sure all NV items are cleared on network leave, including network key
 *                                 Minor tweaks to fix warnings for Cedric
 *                                 artf106135: Handle network leave indications
 *                                 artf104105: Fix padding for negative values of signed non native datatypes (int48 etc)
 *                                 artf106648: Permit join should only be broadcast to routers and coordinator, not all devices
 *                                 artf74202: Support callbacks in Nova plugins
 *                                 artf106710: Fix freeing of invalid pointers during discovery plugin failure
 * 00.00.07.00  31-Oct-14   MvdB   Release
 * 00.00.07.01  05-Nov-14   MvdB   ARTF107075: Correct usage of pthread_mutex_t, change Mutex array to pointers
 *                                 ARTF107076: Replication Get Fails
 * 00.00.07.02  12-Nov-14   MvdB   artf107459: Add retries during endpoint discovery
 *                                 ECB: Fix missing memcpy in init when using callbacks
 * 00.00.08.00  14-Nov-14   MvdB   ARTF107632: Improve notifications when there are failures
 *                                 ARTF57119: Implement Schneider LED handling
 *                                 ARTF107735: Use Schneider LQI calculations
 * 00.00.08.01  27-Nov-14   MvdB   Update default for ZAB_VND_CFG_ZNP_M_OPEN_SOFT_RST_TIMEOUT_MS to 10s
 *                                 General minor updates to ConsoleTest application
 * 01.00.00.00  05-Dec-14   MvdB   Check serial link is available before trying to set PJ from timer
 * 01.00.00.01  20-Jan-15   MvdB   Merge Green Power.
 *                                 Move VLA_INIT and ZAB_RAND_16 from zabCoreConfigure.h to osTypes.h
 *                                 Upgrade SZL_ReceivedSignalStrengthNtfParams_t to support GPDs
 *                                 Support 0xA0 and 0xA2 GP reports
 * 01.00.00.02  03-Feb-15   MvdB   GP Read/Write Attributes
 *                                 ARTF111653: Split in/out buffer ask into separate items for short and long
 *                                 Support GPD De-commissioning
 *                                 ARTF111291: Check short address will not be converted to a broadcast by TI stack. Reject broadcast leaves.
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 *                                 ARTF112481: ZAB Discovery sometimes returns a corrupted cluster list
 *                                 Correctly set command id for ZclM_ClusterCmdCfmHandler()
 *                                 Set ListComplete in errorRspParams for SZL_AttributeDiscoverReq()
 *                                 Support GP Client Cluster plugin
 *                                 ARTF113134: SZL Callbacks not called if error parsing response
 *                                 Split ZAB_VENDOR_SREQ_TIMEOUT into ZAB_VENDOR_DATA_CONFIRM_TIMEOUT and ZAB_VENDOR_SRSP_TIMEOUT
 *                                 ARTF113139: Upgrade ZNP Clone to cover Green Power NV Items
 * 001.101.000 20-Feb-15   MvdB    When opening, give App Type/Version from SBL even if not compatible
 *                                 Replace nullptr with NULL as it was a duplication and caused Klocwork issues
 *                                 artf108759: Szl_DataPointReadNative (and similar) should include maxDataLength
 *                                 ARTF70280: Fix zabZnp_ProcessMgmtBindRsp() MT processing for binds to groups
 *                                 artf105440: Review functions like ZclM_ClusterCmdCfmHandler to ensure correct return types
 *                                 ARTF109084: Validate PAN and EPID during form
 *                                 artf113641: Do not respond to commands with Invalid Mfg Code (such as Read Attributes)
 *                                 artf113674: Direction parameter not used by SZL_ClusterCmdReq
 *                                 Basic Cluster: Remove obsoleted attributes: ATTRID_BASIC_DEVICE_NAME_STRING
 *                                 ECB Cluster: Add attributes ATTRID_ECB_TU_SERVICE_LIFE, ATTRID_ECB_TU_ADAPTATION_INFO
 *                                 Metering Cluster: Add Attributes: 4014-4017, 4100-4105, 4200-4205, 4300-4305. Correct name and R/W as of SZCL 1.0.2
 *                                 Improve handling of future ZNP ProductIds
 *                                 WARNING: Known critical issue artf109790 to be fixed!!!
 * 001.101.001  27-Feb-15   MvdB   ARTF113925: Double leave indication was causing double initialisation to be required during NwkInit with resume=false
 *                                 Tidy up unnecessary gives of product/version etc during closing
 *                                 Correct function name SZL_ReceivedRssiNtfUnregisterCB() to SZL_ReceivedSignalStrengthNtfUnregisterCB()
 *                                 Remove unused values from szl_config.h and add validation
 *                                 ARTF113641: When sending default response to unknown ManufacturerCode, use that ManufacturerCode
 *                                 ARTF113870: Don't reset cluster count in Szl_BuildClustersListFromDataPoints
 *                                 ARTF113870: Szl_BuildClustersListFromDataPoints use better counter data size so it doesn't wrap
 *                                 ARTF115372: Basic Cluster: Correct ATTRID_BASIC_POWER_SOURCE DataType from SZL_ZB_DATATYPE_UINT8 to SZL_ZB_DATATYPE_ENUM8
 * 002.000.000  27-Feb-15   MvdB   Roll version number for release
 * 002.000.001  04-Mar-15   MvdB   ARTF104099: Add service pointer to print function for multi instance apps.
 *                                             Add Service to SzlPlugin_IdentifyClusterNotification_CB_t
 *                                 ARTF115770: Support ZDO Node Descriptor Request
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
 *                                 ARTF115992: Harmonise zabTypes with JNI
 * 002.000.003  06-Mar-15   MvdB   ARTF115854: Validate endpoint on SZL Init and also on ZCL send
 *                                 ARTF116061: Support run time configuration of Green Power endpoint.
 *                                             Remove SZL_CONFIG_GP_MANAGEMENT_EP, Add ZAB_ASK_GREEN_POWER_ENDPOINT.
 *                                 ARTF113874: Print list of available bin files from OpenForFirmwareUpdate in ConsoleTest
 * 002.000.004  01-Apr-15   MvdB   ARTF113871: IeeeAddrRsp/NwkAddrRsp: Clear start address if no associated device list.
 *                                 ARTF116256: Ensure SZL uses szl_memset and szl_memcpy rather than memset and memcpy
 *                                 ARTF116490: For consistency, make RSSI handler use 0xF2 as endpoint when reporting for GPD
 *                                 ARTF56504: Support missed channel change recovery, with actions/notifications/asks/gives to get/set parameters.
 *                                 ARTF116257: ECB Plugin: TuAdaptationInfo length should have +1 for zero terminator
 * 002.000.005  07-Apr-15   MvdB   ARTF115365: Add new API for sending GP commands - SZL_GP_CmdReq()
 * 002.000.006  08-Apr-15   MvdB   ARTF116255: Review enabling/disabling of GP via SZL_CFG_ENABLE_GREEN_POWER, ZAB_CFG_ENABLE_GREEN_POWER
 *                                 ARTF130273: Have LocalWorkIn() check status is ok before dequeueing messages
 *                                 ARTF113873: Set M_DEFAULT_CHANNEL_TO_CHANGE_TO to an invalid value so action fails if ask not correctly supported.
 *                                 ARTF113873: Tidy up action error handling in zanZnpNwk.c - issue not closed.
 * 002.000.007  14-Apr-15   MvdB   ARTF130627: Handle SRSP errors in Open state machine for better error management.
 *                                             Ensure glue is closed on error before indicating CLOSED to the app.
 *                                             Tidy up any active state machines upon CLOSE from glue/vendor.
 *                                 ARTF130628: Replace Clone Timeout with SRSP timeout.
 *                                 ARTF110329: Review handling of serial transmit failures and close when detected.
 * 002.000.008  16-Apr-15   MvdB   ARTF130755: Fix handling of SimpleDescRsp from invalid endpoint. Requires SZNP 2.0.5.
 *                                 ARTF130931: Fix message buffer corruption and Assert when AfDataRequest creates message of size ZAB_VENDOR_MIN_DATA if VLA_INIT != 0
 *                                             Add ZAB_ASSERT macro in zabCoreConfigure.h
 * 002.000.009  17-Apr-15   MvdB   Return timeout from zabCoreEzMode_Work()
 *                                 ARTF130995: Typo - Rename from SZL_ZDO_DeciveAnnounceNtfRegisterCB() to SZL_ZDO_DeviceAnnounceNtfRegisterCB()
 *                                 ARTF131022: Remove SZL_GP_Initialize() as it is now obsolete
 * 002.001.000  20-Apr-15   MvdB   Version for release
 *                                 ARTF131071: Fix SZL_ClusterCmdRegister() for incoming Server->Client notifications
 * 002.001.001  15-Jul-15   MvdB   ARTF130228: Add unique address mode for GpSrcId
 *                                 ARTF136585: Add transaction IDs to over the air commands/responses
 *                                 ARTF134068: cmd_tty - make string length variable
 *                                 ARTF135944: Rename SZL_CFG_MAX_CLIENT_ATTRIBUTES to SZL_CFG_MAX_CLIENT_CLUSTERS
 *                                 ARTF133853: Remove 32bit assumption in appMemGlue_Free()
 *                                 ARTF133845: Remove duplicate definition of SZL_CB_ExtTimer_t
 *                                 ARTF133848: Avoid unaligned struct member warnings on Szl_ReadReportingSendingConfiguration
 *                                 ARTF133846: Remove unused arguments in printApp calls
 *                                 ARTF134376: Fix incorrect comment on SZL_ZB_DATATYPE_UINT32 data type size
 *                                 ARTF132256: Typo in SZL_ZDO_MGMT_LQI_DEVICE_COORINDATOR corrected to SZL_ZDO_MGMT_LQI_DEVICE_COORDINATOR
 *                                 ARTF134857: Use SZL_AttributeReportNtfParams_t_SIZE() when mallocing notifications
 *                                 ARTF132263: Move szl_external.h into config file directory
 *                                 ARTF132252: GP_Client: Error when OS_MEM_UTILITY_USE_MALLOC_ID is not defined
 *                                 ARTF132254: Clean up type casts in vendor for IAR compiler
 *                                 ARTF133844: Remove second definition of szl_bool
 *                                 ARTF132259: szl_report/ReportCheckExpired(): Suspend report generation if zab is not opened & networked
 *                                 ARTF131778: zabZnpOpen.c uses erStatusSet(Service,..) instead of erStatusSet(Status, ...)
 *                                 ARTF132260: Support firmware upgrade of CC2538 network processors
 *                                 Move remaining contents of zabProFrame.h to zabParseProPrivate.h
 *                                 ConsoleTest: Add appZcl_GetZclStatusString() and appZcl_GetSzlAddressString()
 *                                 ConsoleTest: Add groups, identify and scenes client commands
 *                                 ConsoleTest: Support groups addressing mode
 *                                 ConsoleTest: Add CYGWIN_NT-6.1-WOW to makefile
 * 002.001.002  15-Jul-15   MvdB   ARTF136585: Add transaction IDs to over the air ZDO commands/responses
 * 002.001.003  16-Jul-15   MvdB   ARTF132827: Include source address in cluster command handlers
 *                                 ARTF131073: Differentiate by direction when registering Cluster Command Handlers
 *                                 ARTF133822/132827: Include source and destination addressing on AfIncoming frames
 * 002.001.004  21-Jul-15   MvdB   ARTF134686: Update plugin allocation method to be generic and protect against multiple initialisation
 *                                             Add SZL_CFG_MAX_PLUGINS to config files
 *                                             Rework zabCoreDestroy to detect and return as early as possible if there is an error.
 *                                             Add Szl_InternalIsLibInitialized() and use before allowing registration.
 *                                             Protect against SZL_Initialize() with null pointer
 *                                             Call zabCoreFreeQueue() in Szl_InternalInitialize() to avoid losing messages on multiple SZL init
 *                                             Only accept frames in szl_zab_DataInHandler() if lib is initialised
 *                                             Add SZL_ERROR_PLUGIN_NOT_UNREGISTERED
 *                                 ARTF136586: Enable GPEP as a valid SZL endpoint if GreenPower is enabled
 *                                 ARTF132296: Do not close on ZAB_ERROR_VENDOR_SENDING. Just notify app.
 *                                             Add zabZnpOpen_ReadyForMessagesIn()
 *                                             Drop messages coming in from serial glue if we are not open/opening
 *                                 ConsoleTest: Remove console colours. They were cool but clog up the log files.
 * 002.001.005  22-Jul-15   MvdB   zabMutexConfigure.h: Correct comments and default values
 *                                 zabDebug.h: Use generic printf in reference file
 *                                 szl_external.h: Use generic malloc/free in reference file
 * 002.001.006  28-Jul-15   MvdB   Fix TID returned by SZL_NwkLeaveReq()
 *                                 ARTF138318: Add SZL_AttributesDeregister() and SZL_ClusterCmdDeregister()
 *                                             Improve plugin destroy, add disable reporting function
 *                                             Improve Szl_ClusterCmdFind() and Szl_DataPointFind() to get de-registration working
 *                                 ARTF134686: Standardise on SZL_RESULT_t for plugin init return value
 *                                 ARTF132260: Add handshake at start of firmware upgrade for CC2538
 * 002.002.000  29-Jul-15   MvdB   Release
 * 002.002.001  27-Aug-15   MvdB   ARTF146858: Fix crash in print in appGp_ClientPlugin_SinkTableRemoveGpdRsp_Handler()
 *                                 ARTF147439: Increase binding table size for CC2538 (SZNP 2.2.5)
 *                                 ARTF147664: Support 120 GPDs on CC2538 (SZNP 2.2.5)
 *                                 ARTF147630: Support stored FREQTUNE for industrialisation (SZNP 2.2.5)
 *                                 ARTF132260: Finalise support of FW upgrade for CC2538 (SNP 2.2.5 and SBL 2.6.4)
 * 002.002.002  01-Sep-15   MvdB   ARTF147441: Allow application control of GP key mode
 *                                             Add details types for SZL_GP_GpdCommissioningNtfParams_t.ExtendedOptions
 *                                             Add SZL_GP_COMMISSIONING_KEY_MODE_t to SZL_CB_GpdCommissioningNtf_t
 *                                             Support Key Mode in zabZnpGp_CommissioningReplyReq() and extended options in processGpCommissGpdfInd()
 *                                             ConsoleTest: Add appConfig_Config.greenPowerKeyMode, use in appGp_GpdCommissioningNtf_Handler(), allow setting from command line.
 *                                 ARTF148845: Fix issue with status in Szl_InternalInitialize()
 * 002.002.003  01-Sep-15   MvdB   ARTF147441: ConsoleTest: Fix issue with SZL_GP_COMMISSIONING_KEY_MODE_DEFAULT
 * 002.002.004  04-Sep-15   MvdB   ARTF146123: Wait for close notification from glue when going to SBL
 *                                             Only perform the close for the network processors that need it.
 *                                 ARTF147424: Upgrade Szl_GetDeviceID() macro to include endpoint parameter.
 *                                             Move macro from szl_internal.h to szl_config.h
 *                                 ARTF146124: Tidy up return of Szl_ReportSendingConfigure() when removing non-existing items
 *                                 ARTF146663: Remove error set in processZnpStatus() as we already push it to the app as a notification
 *                                 ARTF148423: Upgrade szl_zab_GetTransactionId() to check TID is not in use already
 *                                 ConsoleTest: Correctly malloc parameters using VLA_INIT in cmdline.c
 *                                 Make sure app handler cannot modify SrcId or Options in commissioningIndHandler()
 * 002.002.005  11-Sep-15   MvdB   ConsoleTest: Add appGateway
 *                                 ConsoleTest: Major rework of build process, restructure of target/platform folders.
 *                                 Config_REFERENCE: Simplify to a single folder. Remove sub folders for target and platform.
 *                                 ARTF148813: zcl_GP_ProcessReportAttrMs_MultiCluster: Incorrect cluster ID on handler call if clusters are mixed
 *                                 ARTF148744: zcl_GP_ProcessReportAttrMs_MultiCluster: Overlap in memcpy causes crash
 *                                 ARTF112436: Complete Get Network Info action
 *                                 ARTF146124: Change return to success when removing non-existing items
 *                                 ARTF146845: Increase block size to suit 64 bit builds
 * 002.002.006  18-Sep-15   MvdB   ARTF148943: Limit max read length to local convert buffer size in Szl_DataPointReadPrimitive()
 *                                 Fix various minor issues found by klocwork
 *                                  - Tidy up null pointer checks in Discovery.c: Callback_ZdoMgmtLqiResp()
 * 002.002.007  28-Sep-15   MvdB   ARTF148934: Clone Get: Support reading of selected items
 *                                 EZ-Mode join: Schneider EPID endian-ness was backwards
 * 002.002.008  30-Sep-15   MvdB   Add stub disable command for GP Basic Proxy testing
 *                                 Add ZGP unicast commissioning modes
 * 002.002.009  07-Oct-15   MvdB   ARTF150769: szl_zab: Tweak processDataIn() to request fast work after any command
 *                                 ARTF134853: Handle MS ranges of Green Power Attribute Ids
 *                                             Add ZclM_ExtractAttributeReportItem to increase code re-use
 *                                 Rename SZL_GP_COMMISSIONING_KEY_MODE_NONE to SZL_GP_COMMISSIONING_KEY_MODE_NO_SECURITY for clarity
 *                                 ARTF150763: Optimise SZL sort for Nova by replacing bubble sort with comb sort
 * 002.002.010  09-Oct-15   MvdB   Improve freeing in szl_zab_InitSynchronousTransactionTable to avoid linux double free errors
 *                                 Clone: Check SELECTIVE_READ_ID_CALLBACK is not null before using in nextReadItem()
 *                                 ARTF150980: Support Service ID in memory allocation functions for WTB
 * 002.002.011  12-Oct-15   MvdB   ARTF148934: Upgrade clone get to allow app to read only changed items
 *                                 ARTF104108: Add notification of progress for Clone actions
 *                                 zrvCoreUtility: Improve error handling in ask shortcut functions
 * 002.002.012  14-Oct-15   MvdB   ARTF104107: Support End Device operation
 *                                             szl_zdo: Check network status before sending ZDO commands (important for NoComms end device)
 *                                 ARTF151335: Identify Plugin: Endpoint parameter in callback is wrong on timer expiry
 * 002.002.013  20-Oct-15   MvdB   ARTF151072: Add service pointer to osTimeGetMilliseconds() and osTimeGetSeconds()
 * 002.002.014  20-Oct-15   MvdB   ARTF151327: Handle invalid string (length = 0xFF) in SZL
 * 002.002.015  21-Oct-15   MvdB   ARTF132893: Remove API duplication zigbeeDataToNative  ==> ZbToNative
 *                                 ARTF104106: Support SZL_ZDO_MatchDescriptorReq()
 * 002.002.016  12-Nov-15   MvdB   Add zab_test_cluster plugin
 *                                 Tidy up handling of various data type issues found with zab_test_cluster
 *                                 Update comment for when zabCoreWork() returns zero
 * 002.002.017  13-Nov-15   MvdB   Support single and double precision floating point data types
 * 002.002.018  16-Nov-15   MvdB   Merge some more simple changes in from JNI_DEV branch: comments and brackets
 *                                 ARTF153404: Handle osTimeGetMilliseconds() wrapping
 * 002.002.019  24-Nov-15   MvdB   ARTF153404: Handle osTimeGetSeconds() wrapping
 *                                             Tidy up error cases
 *                                 Support ZGP TransTable Req & Update for testing
 * 002.002.020  18-Mar-16   MvdB   ARTF164900: Handle channel change to self gracefully
 *                                 ARTF165341: Delay Networked ntf during join until key received (DEV_ROUTER)
 *                                 ARTF165342: Support performing SBL firmware updates of ZAB_NET_PROC_MODEL_CC2538_SBS_SERIAL_GATEWAY
 *                                 Use a localStatus in sapMsgPutNotifyError() as status is probably already bad
 *                                 ARTF166827: Fix szl_zab_GetResponseAddrFromRequestDestination() for GP
 * 002.002.021  21-Apr-16   MvdB   ARTF166904: ZAB Read Reporting Configuration Response fails if more than one report is read
 *                                 ARTF167736: Add serial retries to improve robustness against lossy serial link
 *                                             Add ZAB_ACTION_INTERNAL_SERIAL_FLUSH, only allow app actions < ZAB_ACTION_MAX
 *                                 ARTF167734: Improve receive parser to drop invalid commands and recover faster
 *                                 ARTF167807: Support Multi-Cluster Attribute Read/Write for GP
 *                                             Add SZL_ZAB_M_GP_MULTI_FRAME_RESPONSE_DELAY_S config value
 *                                 ARTF167808: Improve Attribute Read/Write/Configure Responses to always list all attributes requested
 * 002.002.022  24-May-16   MvdB   ARTF169100: Fix SZL_AttributeReadReportCfgReqParams_t_SIZE calculation
 *                                 ARTF154247: Error Response for GP Read Attribute which has invalid Address Mode on timeout
 *                                 Change EP1 Device Id to 7 (Combined interface) for gateway certification
 *                                 gp_client plugin: Enable SendPairing on sink table remove
 *                                 Fix casts of error response param ** in calls to SzlZab_GetSynchronousTransactionCallbackAndErrRspBySrcIdOptionalDestroy()
 *                                 Fix issue in SZL_GP_MultiCluster_ReadAttributesCfm() with fail response handling
 * 002.002.023  28-Jun-16   MvdB   ARTF172483: Modify reset to bootloader for CC2538USB to handle re-enumeration required to disable watchdog
 *                                 Add prototype OTA Upgrade plugin
 * 002.002.024  28-Jun-16   MvdB   ARTF172101: Implement ZNP ping to handle lost Reset indication when hidden inside a serial command that was not completed before reset
 * 002.002.025  29-Jul-16   MvdB   Add Poll Control Client Plugin
 * 002.002.026  29-Jul-16   MvdB   Support Silabs NCP network processor
 * 002.002.027  02-Aug-16   MvdB   Run test plan on Silabs NCP network processor
 *                                  - Support source endpoint in appZclIdentifyClient function
 *                                 SiLabs Vendor 000.000.023
 * 002.002.028  12-Dec-16   MvdB   ARTF186964: ZNP Ping should check network action not in process before starting ping
 *                                 ARTF186981: SLIPZ stuck with CLOSED but NETWORKED when ZNP ping fails first send
 *                                 Increase default ZAB_VND_CFG_ZNP_M_NWK_FORM_TIMEOUT_MS from 20 to 30 seconds after seeing some form timeouts.
 *                                 ConsoleTest: Add runtime configurable options for report and RSSI printing
 *                                 ConsoleTest: Enable restart (network initialise) of appGateway on ZNP reset
 *                                 ConsoleTest: Add date to timestamp
 * 002.002.029  06-Jan-17   MvdB   ConsoleTest: Support logging reports to reports.csv
 *                                 ConsoleTest: Do not re-init SZL on restarts of appGateway
 * 002.002.030  09-Jan-17   MvdB   ARTF198434: Upgrade SZL_GP_GpdCommissioningNtfParams_t to include command and cluster lists
 * 002.002.031  09-Jan-17   MvdB   ARTF152098: SZL: Improve update of simple descriptors when attributes/commands are registered/deregistered
 *                                 ARTF172881: Avoid work() returning zero due to errors like szl not initialised
 * 002.002.032  09-Jan-17   MvdB   ARTF172065: Rationalise duplicated private functions into public SZLEXT_ZigBeeDataTypeIsAnalog()
 * 002.002.033  10-Jan-17   MvdB   ARTF170315: Fix incorrect data types for some MS attributes in the Metering Plugin
 * 002.002.034  10-Jan-17   MvdB   ARTF171185: ConsoleTest: MGMT Leave Request does not set IEEE as specified
 * 002.002.035  11-Jan-17   MvdB   ARTF170823: Make Read/Write Reporting configuration data (SZL_AttributeReportData_t) consistent with Read/Write data (SZL_AttributeData_t)
 * 002.002.036  11-Jan-17   MvdB   ARTF167732: Upgrade discovery plugin to handle unknown IEEE/Nwk Addresses better
 * 002.002.037  11-Jan-17   MvdB   ARTF165759: Upgrade zabSerialService_SerialReceiveHandler() to return zabSerialService_ReceiveStatus_t to indicate when work is required
 * 002.002.038  12-Jan-17   MvdB   ARTF113872: Deny key change unless node is Trust Center (it fails if done from other devices)
 * 002.002.037  12-Jan-17   MvdB   ConsoleTest: Support Level Control Client
 * 002.002.038  16-Jan-17   MvdB   Add Thermostat Cluster plugin
 *                                 ConsoleTest: Fix silabs builds - replace some left over SILABS_EZSP with APP_CONFIG_M_VENDOR_SILABS_NCP
 *                                 ConsoleTest: Big cmdline upgrade - add cmdlineZclClient.c/h
 * 002.002.039  17-Jan-17   MvdB   Support bootloading of SiLabs NCP 5.8.x (Changed to EMBER_PHY_RAIL)
 *                                 ARTF175875: NCP send via bind in progress (binding table copy synchronised but not yet used)
 *                                  - Add ZAB_VENDOR_BINDING_TABLE_SIZE
 *                                  - Support policy setting in Nwk init and set binding policy to EZSP_ALLOW_BINDING_MODIFICATION
 *                                  - Clear binding table on network join/form. Read binding table on network start.
 * 002.002.040  17-Jan-17   MvdB   ARTF175875: NCP send via bind completed
 * 002.002.041  18-Jan-17   MvdB   ARTF199128: Fix NCP networking after reception of a leave command
 * 002.002.042  20-Jan-17   MvdB   ConsoleTest: Add GP Write/Read test to appTestplan
 *                                 ARTF199306: Release transaction before calling callback for GP Read/Write responses
 * 002.002.043  20-Jan-17   MvdB   ARTF190099: Make ping time run time controllable for ZNP & NCP. Private APIs only at this time, for industrial tester.
 * 002.002.044  23-Jan-17   MvdB   ARTF67562: ZNP: Speed up ZDO failure callbacks by reporting failure codes in ZDO SRSPs to SZL
 *                                 ARTF146855: ConsoleTest: Fix header file issue for scandir
 * 002.002.045  26-Jan-17   MvdB   Error tidy up: Remove unused errors, update descriptions, merge silabs and ti errors into common vendor codes
 * 002.002.046  26-Jan-17   MvdB   SVN/Project Tidy up: Add thermostat cluster plugin, correct some file paths that were still coming from the silabs dev branch
 * 002.002.047  31-Jan-17   MvdB   Klockwork Review: Correct some small errors identified by klockwork
 *                                 Support optional parameters on DeviceManager gets()
 *                                 Make a recursive call of SzlPlugin_DevMan_UpdateAddresses() in case app may have added previously untracked node
 *                                 Add and enforce SZL_ZDO_USER_DESCRIPTOR_LENGTH_MAX
 * 002.002.048  06-Feb-17   MvdB   Test Plan:
 *                                  - Fix EFR32 give ZAB_NET_PROC_MODEL_SILABS_EFR32_NCP for boot loader
 *                                  - Speed up OPENING notification on EFR32 ZAB_ACTION_EXIT_FIRMWARE_UPDATER
 *                                  - Call response handler for EFR32 RFT receive test start/stop
 * 002.002.049  15-Feb-17   MvdB   ConsoleTest: Ensure *PayloadOutSize is set correctly by cluster command handlers
 *                                 Correct some comments in header files
 * 002.002.050  13-Mar-17   SM     Fix issues on compilation with Linux or IAR compiler
 * 002.002.051  16-Mar-17   MvdB   Remove unused SZL_ADDRESS_MODE_COMBINED
 *                                 Add Documentation folder to repository
 * 002.002.052  30-Mar-17   MvdB   Support ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE
 * 002.002.053  12-Apr-17   SMon   Support ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE_PA
 * 002.002.054  20-Jul-17   SMon   ARTF214292 Manage timeout for default response
 *                                 ARTF214298 Manage write secured key attribut 
 *                                 Add script management in console test
 *****************************************************************************/

#ifndef __ZAB_CORE_INTERFACE_H__
#define __ZAB_CORE_INTERFACE_H__

#include "osTypes.h"
#include "zabTypes.h"
#include "zabCoreConfigure.h"
#include "zabMutexConfigure.h"
#include "zabDebug.h"
#include "osMemUtility.h"
#include "osTimeUtility.h"
#include "sapCoreUtility.h"
#include "sapMsgUtility.h"
#include "srvCoreUtility.h"
#include "erStatusUtility.h"
#include "zabUtility.h"
#include "zabSerialService.h"

#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************
 *                      ******************************
 *                 *****          VERSIONING          *****
 *                      ******************************
 ******************************************************************************/

// This is our version for the entire ZigBee Application Brick
#define ZAB_VERSION_MAJOR   2   // non-backward compatible changes
#define ZAB_VERSION_MINOR   2   // backward compatible changes/enhancements
#define ZAB_VERSION_RELEASE 54  // given to QA for testing new version or fixes
#define ZAB_VERSION_BUILD   0   // internal developer build, not shown




/******************************************************************************
 *                      ******************************
 *                 *****          CONSTANTS           *****
 *                      ******************************
 ******************************************************************************/

/* Invalid value of Service Id */
#define ZAB_CORE_SERVICE_ID_INVALID 0xFF

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Get the version of ZAB Core
 ******************************************************************************/
extern
void zabCoreGetVersion (erStatus* Status, unsigned char* Version);

/******************************************************************************
 * Create ZAB
 * At creation all memory is allocated and configuration data is asked back to the app. If configuration
 * data is needed my the app, it is also given back to the app depending on compile time options.
 *
 * Parameters:
 *  Status: Pointer to ZAB Status. Error must be ZAB_SUCCESS for function to execute.
 *  Id: Service ID of ZAB. Used for multiple instances.
 *  ConfigureBack: Function pointer by which ZAB instance will ASK for configuration values
 *  GiveBack: Function pointer by which ZAB instance will GIVE data
 *  ActionOutCallback: Callback to forward actions to the serial glue
 *  SerialOutCallback: Callback to forward data to the serial glue
 *
 * Returns:
 *  Status: Pointer to ZAB Status. Error indicates success/failure of the function
 *  zabService*: Pointer to instance of ZAB.
 *               Application should store this is the Service pointer for accessing this instance of ZAB.
 ******************************************************************************/
extern
zabService* zabCoreCreate(erStatus* Status,
                          unsigned8 Id,
                          srvConfigureBack ConfigureBack,
                          srvGiveBack GiveBack,
                          zabSerialService_ActionOutCallback_t ActionOutCallback,
                          zabSerialService_SerialOutCallback_t SerialOutCallback);

/******************************************************************************
 * Reset an instance of ZAB
 * Re-initialises data but keeps resources allocated
 ******************************************************************************/
extern
void zabCoreReset(erStatus* Status, zabService * Service);

/******************************************************************************
 * Destroy an instance of ZAB
 * Deallocates all resources
 ******************************************************************************/
extern
void zabCoreDestroy(erStatus* Status, zabService* Service);

/******************************************************************************
 * Perform an Action on the ZAB
 *
 * Parameters:
 *  Status: Pointer to ZAB Status. Error must be ZAB_SUCCESS for function to execute.
 *  Service: Pointer to instance of ZAB.
 *  Action: Action to be performed. See zabAction for supported actions.
 ******************************************************************************/
extern
void zabCoreAction(erStatus* Status, zabService* Service, zabAction Action);

/******************************************************************************
 * Get the Open State of ZAB, as returned in last notification
 ******************************************************************************/
extern
zabOpenState zabCoreGetOpenState(zabService* Service);

/******************************************************************************
 * Get the Network State of ZAB, as returned in last notification
 ******************************************************************************/
extern
zabNwkState zabCoreGetNetworkState(zabService* Service);

/******************************************************************************
 * Get if ZAB if ready to send command across the network (Open and Networked)
 ******************************************************************************/
extern
zab_bool zabCoreService_GetZabNetworkReady(zabService* Service);

/******************************************************************************
 * Get SAPs (Manage/Data/Serial)from an instance of ZAB
 ******************************************************************************/
extern
sapHandle zabCoreSapManage(zabService* Service);
extern
sapHandle zabCoreSapData(zabService* Service);
extern
sapHandle zabCoreSapSerial(zabService* Service);


/******************************************************************************
 * This does the work for the service, processing queues, state machines and timeouts.
 *
 * Return Value:
 * The longest delay in ms before work SHOULD be called again.
 *  - Zero is returned to request work be re-called immediately.
 *  - Calling work faster than the returned value is good.
 *  - Calling work slower than the returned value is ok but not recommended, as timeouts
 *    we be slow to occur.
 *  - It is ok to ignore this value and just call work regularly on a timed tick or in a super loop.
 *  - Work MUST be called after calling an API function or feeding serial bytes into ZAB to update the returned value.
 ******************************************************************************/
unsigned32 zabCoreWork(erStatus* Status, zabService* Service);

#ifdef __cplusplus
}
#endif

#endif


