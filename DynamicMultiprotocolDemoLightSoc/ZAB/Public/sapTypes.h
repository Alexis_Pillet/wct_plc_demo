/* 
  Name:    Utility Types
  Author:  ZigBee Excellence Center
  Company: Schneider Electric

  Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

  Description:
    This file is included in all Utility module implementations (utXxxxService.c), NOT interfaces (utXxxxInterface.h).
*/
#ifndef __SAP_TYPES_H__
#define __SAP_TYPES_H__

#ifdef __cplusplus
extern "C" {
#endif

    typedef void  sapMsg;       // user can not know how message is structured for maintenance

    typedef void* sapHandle;   // SAP instance

    typedef void* sapService;   // Service

#ifdef __cplusplus
}
#endif


#endif /* __SAP_TYPES_H__ */

