#ifndef _SZL_TIMER_TYPES_H_
#define _SZL_TIMER_TYPES_H_


#define SZL_RESERVED_EXTERNAL_TIMER_COUNT 0
#define SZL_EXTERNAL_TIMER_COUNT (SZL_CFG_EXTERNAL_TIMER_COUNT + SZL_RESERVED_EXTERNAL_TIMER_COUNT)

// TimerID's
#define EXT_TIMER_ID_BYTES (SZL_EXTERNAL_TIMER_COUNT + 7) / 8
//szl_uint8 BitsTimerID[EXT_TIMER_ID_BYTES]; //The timer ID space, 0-xxx, bitfield (must not use 0)

#define IS_VALIDEXT_ID(ID) ( (ID) > 0 && (ID) <= SZL_EXTERNAL_TIMER_COUNT)

typedef void (*SZL_CB_ExtTimer_t) (zabService* Service, szl_uint8 ID, szl_uint32 duration);



// Timer functions
// (resolution in seconds)
typedef struct
{
    szl_bool active;     // timer is active/deactive
    szl_uint32 start;    // the time the timer were started
    szl_uint32 interval; // timer interval in seconds
} SZL_Timer_t;



typedef struct
{
    szl_bool inUse;         /**< true if timer is in use */
    szl_bool repeatable;    /**< true if the timer is repeatable */
    szl_uint16 interval;    /**< default interval for the timer */
    SZL_Timer_t timer;  /**< Internal timer data */
    SZL_CB_ExtTimer_t callback; /**< callback function for the timer - called when timer expires or stops */
} SZL_EXT_Timer_t;


typedef struct
{
  szl_uint32 gLastTimeTick;
  szl_uint32 gSecondsTimer;
  SZL_EXT_Timer_t ExtTimers[SZL_EXTERNAL_TIMER_COUNT];
  szl_uint8 BitsTimerID[EXT_TIMER_ID_BYTES]; //The timer ID space, 0-xxx, bitfield (must not use 0)
  szl_uint32 gMillisecondsCounter;
}SZL_TIMER_t;

#endif /*_SZL_TIMER_TYPES_H_*/
