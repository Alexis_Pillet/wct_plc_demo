/*
 Name:    Service Utility Implementation
 Author:  Cam Williams
 Company: Schneider Electric

 Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

 Description:
 * 002.002.011  12-Oct-15   MvdB   Improve error handling in ask shortcut functions

*/

#include "zabCoreService.h"

#include "srvCoreUtility.h"
#include "srvCorePrivate.h"
#include "zabCorePrivate.h"

/******************************************************************************
 * Get the service ID
 ******************************************************************************/
unsigned8 srvCoreGetServiceId( erStatus* Status, zabService* Service)
{
  ER_CHECK_NULL_VALUE(Status, Service, ZAB_CORE_SERVICE_ID_INVALID);
  return SRV_STRUCT( Service )->Id;
}

/******************************************************************************
 * Set the service ID
 ******************************************************************************/
void srvCoreSetServiceId( erStatus* Status, zabService* Service, unsigned8 Id )
{
    ER_CHECK_NULL(Status, Service);
   SRV_STRUCT( Service )->Id = Id;
}

/******************************************************************************
 * Set the ASK handler
 ******************************************************************************/
void srvCoreSetConfigureBack( erStatus* Status, zabService* Service, srvConfigureBack ConfigureBack )
{
    ER_CHECK_NULL(Status, Service);
   SRV_STRUCT( Service )->ConfigureBack = ConfigureBack;
}

/******************************************************************************
 * Set the GIVE handler
 ******************************************************************************/
void srvCoreSetGiveBack( erStatus* Status, zabService* Service, srvGiveBack GiveBack )
{
    ER_CHECK_NULL(Status, Service);
    SRV_STRUCT(Service)->GiveBack = GiveBack;
}

/******************************************************************************
 * ASK back config data from app
 * This functions may be called into the core from the vendor.
 * The core may answer, or forward the call onto the app to answer
 ******************************************************************************/
void srvCoreAskBack( erStatus* Status, zabService* Service, zabAskId What, unsigned32 Param1, unsigned32* Size, void* Buffer )
{
    ER_CHECK_NULL(Status, Service);
    ER_CHECK_NULL(Status, Size);
    ER_CHECK_NULL(Status, Buffer);
  /* If the Core wants to answer, e.g. during EZ mode, then it should do so here*/
#ifdef ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING
  if (zabCoreEzMode_AskCfgHandler(Status, Service, What, Param1, Size, (unsigned8*)Buffer) != zab_true)
#endif
    {

      if (SRV_STRUCT( Service )->ConfigureBack == NULL)
        {
          erStatusSet( Status, SRV_ERROR_ASK_NO_ITEM );
          return;
        }

      SRV_STRUCT( Service )->ConfigureBack(Status, Service, What, Param1, Size, (unsigned8*)Buffer);

      /* If app set size to zero then data is invalid. Set error. */
      if (*Size == 0)
        {
          erStatusSet(Status, SRV_ERROR_ASK_NO_ITEM);
          return;
        }
    }
}

/******************************************************************************
 * ASK back config data from app - 8 bit short cut function
 ******************************************************************************/
unsigned8 srvCoreAskBack8( erStatus* Status, zabService* Service, zabAskId What, unsigned8 Default )
{
  unsigned32 Size  = 1;
  unsigned8 Value = Default;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);

  srvCoreAskBack( &localStatus, Service, What, 0, &Size, (unsigned8*)&Value );

  if (erStatusIsError(&localStatus))
    {
      Value = Default;
      if (erStatusGetError(&localStatus) != SRV_ERROR_ASK_NO_ITEM)
        {
          erStatusSet(Status, erStatusGetError(&localStatus));
        }
    }
  else if (Size == 0)
    {
      Value = Default;
    }

  return Value;
}

/******************************************************************************
 * ASK back config data from app - 16 bit short cut function
 ******************************************************************************/
unsigned16 srvCoreAskBack16( erStatus* Status, zabService* Service, zabAskId What, unsigned16 Default )
{
  unsigned32 Size  = 2;
  unsigned16 Value = Default;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);

  srvCoreAskBack( &localStatus, Service, What, 0, &Size, (unsigned8*)&Value );

  if (erStatusIsError(&localStatus))
    {
      Value = Default;
      if (erStatusGetError(&localStatus) != SRV_ERROR_ASK_NO_ITEM)
        {
          erStatusSet(Status, erStatusGetError(&localStatus));
        }
    }
  else if (Size == 0)
    {
      Value = Default;
    }

  return Value;
}

/******************************************************************************
 * ASK back config data from app - 32 bit short cut function
 ******************************************************************************/
unsigned32 srvCoreAskBack32( erStatus* Status, zabService* Service, zabAskId What, unsigned32 Default )
{
  unsigned32 Size  = 4;
  unsigned32   Value = Default;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);

  srvCoreAskBack( &localStatus, Service, What, 0, &Size, (unsigned8*)&Value );

  if (erStatusIsError(&localStatus))
    {
      Value = Default;
      if (erStatusGetError(&localStatus) != SRV_ERROR_ASK_NO_ITEM)
        {
          erStatusSet(Status, erStatusGetError(&localStatus));
        }
    }
  else if (Size == 0)
    {
      Value = Default;
    }

  return Value;
}

/******************************************************************************
 * ASK back config data from app - 64 bit short cut function
 ******************************************************************************/
unsigned64 srvCoreAskBack64(erStatus* Status, zabService* Service, zabAskId What, unsigned64 Default)
{
  unsigned32 Size  = 8;
  unsigned64   Value = Default;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);

  srvCoreAskBack( &localStatus, Service, What, 0, &Size, (unsigned8*)&Value );

  if (erStatusIsError(&localStatus))
    {
      Value = Default;
      if (erStatusGetError(&localStatus) != SRV_ERROR_ASK_NO_ITEM)
        {
          erStatusSet(Status, erStatusGetError(&localStatus));
        }
    }
  else if (Size == 0)
    {
      Value = Default;
    }

  return Value;
}

/******************************************************************************
 * ASK back config data from app - 8bit buffer short cut function
 ******************************************************************************/
unsigned32 srvCoreAskBackBuffer( erStatus* Status, zabService* Service, zabAskId What, unsigned32 Param1, unsigned32 Size, unsigned8* Buffer )
{
  unsigned32 Length = Size;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);

  srvCoreAskBack(&localStatus, Service, What, Param1, &Length, Buffer);
  if ( erStatusIsError(&localStatus)  &&
       (erStatusGetError(&localStatus) != SRV_ERROR_ASK_NO_ITEM) )
    {
      erStatusSet(Status, erStatusGetError(&localStatus));
    }

  return Length;
}

/******************************************************************************
 * Give back information to the app
 * Information is passed to the app via a pre-register function pointer
 ******************************************************************************/
void srvCoreGiveBack( erStatus* Status,  zabService* Service,
                      zabGiveId What, unsigned32 Param1,
                      unsigned32 Size, unsigned8* Buffer )
{
  /* If the core wants to intercept any Given data it should do so here */
  switch (What)
    {
      case ZAB_GIVE_NWK_ADDRESS: ZAB_SRV(Service)->nwkAddress = *(unsigned16*)Buffer; break;
      case ZAB_GIVE_NWK_CHANNEL_NUMBER: ZAB_SRV(Service)->channel = *Buffer; break;
      case ZAB_GIVE_NWK_PAN_ID: ZAB_SRV(Service)->panID = *(unsigned16*)Buffer; break;
      case ZAB_GIVE_NWK_EPID: ZAB_SRV(Service)->epid = *(unsigned64*)Buffer; break;
      default: break;
    }

  /* Give EZ Mode the chance to intercept the give if it caused it to happen */
#ifdef ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING
  if (zabCoreEzMode_GiveCfgHandler(Status, Service, What, Param1, &Size, Buffer) != zab_true)
#endif
    {
      /* Call the give back if one has been registered */
      if (SRV_STRUCT( Service )->GiveBack != NULL)
        {
          SRV_STRUCT( Service )->GiveBack( Status, Service, What, Param1, &Size, Buffer );
        }
    }
}

/******************************************************************************
 * GIVE back information to the app - 8 bit short cut function
 ******************************************************************************/
void srvCoreGiveBack8(  erStatus* Status, zabService* Service, zabGiveId What, unsigned8 Value )
{
  unsigned8 U8_Data = Value;
  srvCoreGiveBack( Status, Service, What, 0, 1, (unsigned8*)&U8_Data );
}

/******************************************************************************
 * GIVE back information to the app - 16 bit short cut function
 ******************************************************************************/
void srvCoreGiveBack16(  erStatus* Status, zabService* Service, zabGiveId What, unsigned16 Value )
{
  unsigned16 U16_Data = Value;
  srvCoreGiveBack( Status, Service, What, 0, 2, (unsigned8*)&U16_Data );
}

/******************************************************************************
 * GIVE back information to the app - 32 bit short cut function
 ******************************************************************************/
void srvCoreGiveBack32(  erStatus* Status, zabService* Service, zabGiveId What, unsigned32 Value )
{
  unsigned32 U32_Data = Value;
  srvCoreGiveBack( Status, Service, What, 0, 4, (unsigned8*)&U32_Data );
}

/******************************************************************************
 * GIVE back information to the app - 8 bit array short cut function
 ******************************************************************************/
void srvCoreGiveBackBuffer ( erStatus* Status,  zabService* Service, zabGiveId What, unsigned32 Size, unsigned8* Buffer )
{
  srvCoreGiveBack( Status, Service, What, 0, Size, Buffer );
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/