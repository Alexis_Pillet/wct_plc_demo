/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_sys_cb.c
*    @version
*        $Rev: 3384 $
*    @last editor
*        $Author: a5089752 $
*    @date
*        $Date:: 2017-05-31 13:49:31 +0900#$
* Description :
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/

/* common */
#include "r_typedefs.h"

/* synergy */

/* sys part */
#include "r_c3sap_api.h"

/* app part */
#include "r_demo_api.h"
#include "r_demo_app.h"
#include "r_demo_sys.h"
#include "r_demo_app_thread.h"

/******************************************************************************
Macro definitions
******************************************************************************/
/******************************************************************************
Typedef definitions
******************************************************************************/
/******************************************************************************
Exported global variables
******************************************************************************/
extern volatile r_demo_sys_cb_str_t g_syscb;

/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/
/******************************************************************************
Private global variables and functions
******************************************************************************/


/******************************************************************************
Functions
******************************************************************************/

/******************************************************************************
* Function Name: R_SYS_PingConfirm
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void R_SYS_PingConfirm (const r_sys_ping_cnf_t * confirm)
{
    g_syscb.pingCnf = *confirm;
}
/******************************************************************************
   End of function  R_SYS_PingConfirm
******************************************************************************/

/******************************************************************************
* Function Name: R_SYS_VersionConfirm
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void R_SYS_VersionConfirm (const r_sys_version_cnf_t * confirm)
{
    g_syscb.versionCnf = *confirm;
}
/******************************************************************************
   End of function  R_SYS_VersionConfirm
******************************************************************************/


/******************************************************************************
* Function Name: R_SYS_GetInfoConfirm
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void R_SYS_GetInfoConfirm (const r_sys_get_info_cnf_t * confirm)
{
    g_syscb.getInfoCnf = *confirm;
    g_syscb.getInfoCnf.pinfo = (uint32_t *)g_syscb.cbBuffU32;

    if (
        (confirm->length <= R_DEMO_APP_STATS_BUFF_MAXLEN) &&
        (NULL != confirm->pinfo)
        )
    {
        R_memcpy ((uint8_t *)g_syscb.getInfoCnf.pinfo, (const uint8_t *)confirm->pinfo, confirm->length);
    }
    else
    {
        g_syscb.getInfoCnf.status = R_DEMO_SYS_STATUS_FAILED;
    }
}
/******************************************************************************
   End of function  R_SYS_GetInfoConfirm
******************************************************************************/


/******************************************************************************
* Function Name: R_SYS_ClearInfoConfirm
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void R_SYS_ClearInfoConfirm (const r_sys_clear_info_cnf_t * confirm)
{
    g_syscb.clearInfoCnf = *confirm;
}
/******************************************************************************
   End of function  R_SYS_ClearInfoConfirm
******************************************************************************/


/******************************************************************************
* Function Name: R_SYS_EventIndication
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void R_SYS_EventIndication (const r_sys_event_ind_t * indication)
{
    g_syscb.eventInd = *indication;
}
/******************************************************************************
   End of function  R_SYS_EventIndication
******************************************************************************/


/******************************************************************************
* Function Name: R_SYS_ReBootReqIndication
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void R_SYS_ReBootReqIndication (const r_sys_rebootreq_ind_t * indication)
{
    /* Enqueue indication. */
    R_DEMO_AppThreadEnqueInd ((const uint8_t *)indication,
                              R_DEMO_APP_HANDLE_REBOOT_REQUEST_IND,
                              sizeof (r_sys_rebootreq_ind_t));

}
/******************************************************************************
   End of function  R_SYS_ReBootReqIndication
******************************************************************************/

/******************************************************************************
* Functions
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_InitSysCallBack
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_InitSysCallBack (r_sys_callback_t * pCallBack)
{

    if (NULL == pCallBack)
    {
        return R_RESULT_ILLEGAL_NULL_POINTER;
    }

    R_memset ((uint8_t *)pCallBack, 0, sizeof (r_sys_callback_t));

    pCallBack->R_SYS_PingCnf                                       =   R_SYS_PingConfirm;
    pCallBack->R_SYS_VersionCnf                                    =   R_SYS_VersionConfirm;
    pCallBack->R_SYS_GetInfoCnf                                    =   R_SYS_GetInfoConfirm;
    pCallBack->R_SYS_ClearInfoCnf                                  =   R_SYS_ClearInfoConfirm;
    pCallBack->R_SYS_EventInd                                      =   R_SYS_EventIndication;
    pCallBack->R_SYS_ReBootReqInd                                  =   R_SYS_ReBootReqIndication;

    return R_RESULT_SUCCESS;
} /* R_DEMO_InitSysCallBack */
/******************************************************************************
   End of function  R_DEMO_InitSysCallBack
******************************************************************************/

