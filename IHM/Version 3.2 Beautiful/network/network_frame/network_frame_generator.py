# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to generate mac frames
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from .network_frame_structure import *


class NetworkFrameGenerator:

    DEFAULT_ACK_OPTION = NetworkOptionAck.ACK_RESPONSE.value
    DEFAULT_SOURCE_ADDRESS_OPTION = NetworkOptionAddressSource.ADDRESS_SOURCE_NOT_PRESENT.value
    DEFAULT_DESTINATION_ADDRESS_OPTION = NetworkOptionAddressDestination.ADDRESS_DESTINATION_NOT_PRESENT.value
    DEFAULT_FRAGMENTATION_OPTION = NetworkOptionFragmentation.NO_FRAGMENTATION.value

    def __init__(self, network_process):
        self._network_process = network_process
        self._default_option = (self.DEFAULT_ACK_OPTION & NetworkOptionMask.MASK_OPTION_ACK.value) \
            + (self.DEFAULT_SOURCE_ADDRESS_OPTION & NetworkOptionMask.MASK_OPTION_ADDRESS_SOURCE.value) \
            + (self.DEFAULT_DESTINATION_ADDRESS_OPTION & NetworkOptionMask.MASK_OPTION_ADDRESS_DESTINATION.value) \
            + (self.DEFAULT_FRAGMENTATION_OPTION & NetworkOptionMask.MASK_OPTION_FRAGMENTATION.value)

    def send_data_frame(self, device_number, payload):

        # Send frame
        self._network_process.send_frame(NetworkFrameControl.DATA.value, self._default_option, device_number, payload)
