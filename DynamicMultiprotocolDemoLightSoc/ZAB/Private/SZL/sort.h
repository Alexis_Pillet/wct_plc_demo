/*******************************************************************************
  Filename    : sort.h
  $Date       : 2013-05-01                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : sort functions for the SZL
*******************************************************************************/
#ifndef _SORT_H_
#define _SORT_H_

typedef  int (*bubble_sort_compare_fn_t)(const void *, const void *);

void bubble_sort(void* listElem, const int numElem, const int sizeElem, bubble_sort_compare_fn_t);

#endif /* _SORT_H_ */
