/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the Radio Factory Test interface for ZNP.
 *   This is a Schneider specific extension to ZNP intended for use in NOVA.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *  00.00.02.00 03-Oct-13   MvdB   Original
 *  00.00.02.00 22-Oct-13   MvdB   Add parameters to zabZnpRftUtility_ConfigSendTest
 *  00.00.06.00 25-Jun-14   MvdB   Re-purpose API for server side Nova Factory Test
 * 002.002.001  19-Aug-15   MvdB   ARTF147630: Support stored FREQTUNE for industrialisation (SZNP 2.2.5)
 *****************************************************************************/

#ifndef _ZAB_ZNP_RFT_UTILITY_H_
#define _ZAB_ZNP_RFT_UTILITY_H_


#include "zabVendorConfigureZnp.h"

#ifdef __cplusplus
extern "C" {
#endif
#ifdef ZAB_VND_CFG_ZNP_ENABLE_RFT

/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Types of  Standard Response
 ******************************************************************************/
typedef enum
{
  /* Note - its critical these enum values match the ZNP command values as we are taking a shortcut on command processing
   *        DO NOT CHANGE THE ENUM VALUES!*/
  zabZnpRftUtility_ResponseType_EnterTest           = 0x00,
  zabZnpRftUtility_ResponseType_ConfigCarrierTest   = 0x01,
  zabZnpRftUtility_ResponseType_ConfigReceiveTest   = 0x03,
  zabZnpRftUtility_ResponseType_StartTest           = 0x10,
  zabZnpRftUtility_ResponseType_StopTest            = 0x11,
  zabZnpRftUtility_ResponseType_PingResponse        = 0x18,
  zabZnpRftUtility_ResponseType_SetFreqTune         = 0x21,
  zabZnpRftUtility_ResponseType_SetStoredFreqTune   = 0x23,
}zabZnpRftUtility_ResponseType;

/******************************************************************************
 * Prototype for Standard Response Callback
 ******************************************************************************/
typedef void (*zabZnpRftUtility_CB_StandardResponse) (zabService* Service, zabZnpRftUtility_ResponseType ResponseType, unsigned8 status);

/******************************************************************************
 * Prototype for Receive Indication Callback
 ******************************************************************************/
typedef void (*zabZnpRftUtility_CB_PingInd) (zabService* Service, unsigned8 status, unsigned32 RxTimeStamp, unsigned8 RSSI, unsigned32 FrameCounter);

/******************************************************************************
 * Prototype for Get FreqTune Callback
 ******************************************************************************/
typedef void (*zabZnpRftUtility_CB_GetFreqTuneResponse) (zabService* Service, unsigned8 Status, unsigned8 FreqTune);



/******************************************************************************
 * ZAB internal parameter storage
 ******************************************************************************/
typedef struct
{
  zabZnpRftUtility_CB_StandardResponse StandardResponseCallback;
  zabZnpRftUtility_CB_PingInd ReceiveIndCallback;
  zabZnpRftUtility_CB_GetFreqTuneResponse GetFreqTuneResponseCallback;
  zabZnpRftUtility_CB_GetFreqTuneResponse GetStoredFreqTuneResponseCallback;
}zabZnpRftUtility_Params;


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise Radio Factory Test with a source ID.
 ******************************************************************************/
extern
void zabZnpRftUtility_EnterTest(erStatus* Status, zabService* Service, unsigned32 SourceID);

/******************************************************************************
 * Configure the Carrier Test
 *
 * Modulated = true/false
 * RfChannel = 11-26
 * Power = Value of CC2520 TXPOWER register. This is not a DBM value!
 ******************************************************************************/
extern
void zabZnpRftUtility_ConfigCarrierTest(erStatus* Status,
                                        zabService* Service,
                                        zab_bool Modulated,
                                        unsigned8 RfChannel,
                                        unsigned8 Power);

/******************************************************************************
 * Configure the Receive Test
 *
 * RfChannel = 11-26
 ******************************************************************************/
extern
void zabZnpRftUtility_ConfigReceiveTest(erStatus* Status,
                                        zabService* Service,
                                        unsigned8 RfChannel);

/******************************************************************************
 * Start the previously configured test
 ******************************************************************************/
extern
void zabZnpRftUtility_StartTest(erStatus* Status,
                                zabService* Service);

/******************************************************************************
 * Stop the current test
 ******************************************************************************/
extern
void zabZnpRftUtility_StopTest(erStatus* Status,
                               zabService* Service);

/******************************************************************************
 * Ping Response - For responses to Ping Indications during receive test
 ******************************************************************************/
extern
void zabZnpRftUtility_PingResponse(erStatus* Status,
                                   zabService* Service,
                                   unsigned32 FrameCounter,
                                   unsigned8 Rssi);

/******************************************************************************
 * Get Freq Tune Request
 ******************************************************************************/
extern
void zabZnpRftUtility_GetFreqTuneRequest(erStatus* Status,
                                         zabService* Service);


/******************************************************************************
 * Set Freq Tune Request
 ******************************************************************************/
extern
void zabZnpRftUtility_SetFreqTuneRequest(erStatus* Status,
                                         zabService* Service,
                                         unsigned8 FreqTune);

/******************************************************************************
 * Get Stored Freq Tune Request
 ******************************************************************************/
extern
void zabZnpRftUtility_GetStoredFreqTuneRequest(erStatus* Status,
                                               zabService* Service);

/******************************************************************************
 * Set Stored Freq Tune Request
 ******************************************************************************/
extern
void zabZnpRftUtility_SetStoredFreqTuneRequest(erStatus* Status,
                                               zabService* Service,
                                               unsigned8 FreqTune);

/******************************************************************************
 * Register a callback for Standard (status only) Responses
 * The zabZnpRftUtility_ResponseType enumeration shows which command the response is for.
 ******************************************************************************/
extern
void zabZnpRftUtility_RegisterStandardResponseHandler(erStatus* Status,
                                                      zabService* Service,
                                                      zabZnpRftUtility_CB_StandardResponse Callback);

/******************************************************************************
 * Register a callback for Receive Indications
 ******************************************************************************/
extern
void zabZnpRftUtility_RegisterPingIndicationHandler(erStatus* Status,
                                                       zabService* Service,
                                                       zabZnpRftUtility_CB_PingInd Callback);

/******************************************************************************
 * Register a callback for Get Freq Tune Response
 ******************************************************************************/
extern
void zabZnpRftUtility_RegisterGetFreqTuneResponseHandler(erStatus* Status,
                                                         zabService* Service,
                                                         zabZnpRftUtility_CB_GetFreqTuneResponse Callback);

/******************************************************************************
 * Register a callback for Get Stored Freq Tune Response
 ******************************************************************************/
extern
void zabZnpRftUtility_RegisterGetStoredFreqTuneResponseHandler(erStatus* Status,
                                                               zabService* Service,
                                                               zabZnpRftUtility_CB_GetFreqTuneResponse Callback);


/******************************************************************************
 *                      ******************************
 *               ***** ZAB PRIVATE FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Process all messages in the RFT subsystem
 ******************************************************************************/
extern
void zabZnpRftUtility_ProcessMsg(erStatus* Status, zabService* Service, sapMsg* Message);

#endif // ZAB_VND_CFG_ZNP_ENABLE_RFT
#ifdef __cplusplus
}
#endif



#endif /* _ZAB_ZNP_RFT_UTILITY_H_ */
