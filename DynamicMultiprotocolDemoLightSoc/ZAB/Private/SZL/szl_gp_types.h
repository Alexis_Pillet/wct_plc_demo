/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the profile wide client commands for the ZAB implementation
 *   of the SZL GP interface - TYPES
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *              22-Jul-14   MvdB   Original
 * 00.00.06_GP  15-Oct-14   MvdB   ARTF105853: Complete basic GP features for Smartlink IPZ
 * 01.00.00.02  29-Jan-15   MvdB   Support GPD De-commissioning
 * 002.000.005  07-Apr-15   MvdB   ARTF115365: Add new API for sending GP commands - SZL_GP_CmdReq()
 * 002.001.001  15-Jul-15   MvdB   ARTF130228: Add unique address mode for GpSrcId
 * 002.002.002  01-Sep-15   MvdB   ARTF147441: Add details types for SZL_GP_GpdCommissioningNtfParams_t.ExtendedOptions
 *                                             Add SZL_GP_COMMISSIONING_KEY_MODE_t to SZL_CB_GpdCommissioningNtf_t
 * 002.002.009  07-Oct-15   MvdB   Rename SZL_GP_COMMISSIONING_KEY_MODE_NONE to SZL_GP_COMMISSIONING_KEY_MODE_NO_SECURITY for clarity
 * 002.002.030  09-Jan-17   MvdB   ARTF198434: Upgrade SZL_GP_GpdCommissioningNtfParams_t to include command and cluster lists
 *****************************************************************************/

#ifndef _SZL_GP_TYPES_H_
#define _SZL_GP_TYPES_H_

#ifdef __cplusplus
extern "C"
{
#endif

/* The Green Power Endpoint */
#define SZL_GP_ENDPOINT 0xF2

/* Value used when ManufacturerId or ManufacturerModelId is not defined */
#define SZL_GP_COM_IND_ID_NOT_DEFINED   ( 0xFFFF )

/* Maximum length of Command ID and Cluster lists in GPD Commissioning Indication */
#define SZL_GP_COM_IND_MAX_LIST_LENGTH  ( 16 )

/**
 * Commissioning Indication Options
 */
typedef struct
{
  szl_uint8 MacSeqNumCap:1;                           /**< Ignore. For future expansion */
  szl_uint8 RxOnCap:1;                                /**< GPD is capable of receiving data */
  szl_uint8 AppInfoPresent:1;                         /**< Ignore. For future expansion */
  szl_uint8 Reserved1:1;                              /**< Ignore. Reserved. */
  szl_uint8 PanIdRequest:1;                           /**< Ignore. For future expansion */
  szl_uint8 GpSecurityKeyReq:1;                       /**< GPD is able to receive a key */
  szl_uint8 FixedLocation:1;                          /**< True if GPD is in a fixed location. False if it is mobile */
  szl_uint8 ExtOptionsPresent:1;                      /**< The contents of the ExtendedOptions is valid */
} SZL_GP_COM_IND_OPTIONS_t;


/**
 * Commissioning Indication Extended Options - Security Level Capabilities
 * SZL_GP_COM_IND_EXT_OPTIONS_t.SecurityLevelCapabilities
 */
typedef enum
{
  SZL_GP_SECURITY_LEVEL_CAP_NONE          = 0x00,     /**< No security */
  SZL_GP_SECURITY_LEVEL_CAP_AUTH          = 0x02,     /**< Authentication with 4B frame counter and 4B MIC. Not encrypted. */
  SZL_GP_SECURITY_LEVEL_CAP_AUTH_ENCRYPT  = 0x03,     /**< Encryption and Authentication with 4B frame counter and 4B MIC. */
} SZL_GP_SECURITY_LEVEL_CAP_t;

/**
 * Commissioning Indication Extended Options - Security Key Type
 * SZL_GP_COM_IND_EXT_OPTIONS_t.KeyType
 */
typedef enum
{
  SZL_GP_SECURITY_KEY_TYPE_NONE               = 0x00, /**< No security */
  SZL_GP_SECURITY_KEY_TYPE_ZB_NWK             = 0x01, /**< ZigBee Pro Nwk Key */
  SZL_GP_SECURITY_KEY_TYPE_GP_GRP             = 0x02, /**< Green Power Group Key */
  SZL_GP_SECURITY_KEY_TYPE_NWK_DERIVED_GRP    = 0x03, /**< Green Power Group Key derived from ZigBee Pro Network Key */
  SZL_GP_SECURITY_KEY_TYPE_OUT_OF_BOX         = 0x04, /**< GPD is pre-configured with a security key */
  SZL_GP_SECURITY_KEY_TYPE_DERIVED_INDIVIDUAL = 0x07, /**< Individual key is derived from the GP Group key */
}SZL_GP_SECURITY_KEY_TYPE_t;

/**
 * Commissioning Indication Extended Options
 */
typedef struct
{
  SZL_GP_SECURITY_LEVEL_CAP_t SecurityLevelCapabilities:2;  /**< The GPD's security capabilities during normal operation */
  SZL_GP_SECURITY_KEY_TYPE_t KeyType:3;                     /**< The type of security key the GPD is configured with */
  szl_uint8 GpdKeyPresent:1;                                /**< Out Of Box key was present in the commissioning frame */
  szl_uint8 GpdKeyEncryption:1;                             /**< Key was encrypted and MIC present in the commissionign frame. */
  szl_uint8 GpdOutgoingCounterPresent:1;                    /**< Outgoing frame counter was present in the commissioning frame */
} SZL_GP_COM_IND_EXT_OPTIONS_t;


/**
 * Commissioning Indication Application Information
 */
typedef struct
{
  szl_uint8 ManufacturerIdPresent:1;                  /**< Indicates ManufacturerId field is valid */
  szl_uint8 ModelIdPresent:1;                         /**< Indicates ManufacturerModelId field is valid */
  szl_uint8 GpdCommandsPresent:1;                     /**< Indicates GPD Command List is present */
  szl_uint8 ClusterListPresent:1;                     /**< Indicates Cluster List is present */
  szl_uint8 Reserved:4;                               /**< Reserved bits */
} SZL_GP_COM_IND_APP_INFO_t;

/**
 * Type for GpCommissioningInd.
 */
typedef struct _SZL_GP_GpdCommissioningNtfParams_t
{
  SZL_Addresses_t Address;                                      /**< Address of the GPD */
  szl_uint8 DeviceId;                                           /**< GP Device Id. If 0xFE then ManufacturerId & ManufacturerModelId are valid*/
  SZL_GP_COM_IND_OPTIONS_t Options;                             /**< Options related to the command/device */
  SZL_GP_COM_IND_EXT_OPTIONS_t ExtendedOptions;                 /**< Extended security options */
  SZL_GP_COM_IND_APP_INFO_t AppInfo;                            /**< Application Info */
  szl_uint16 ManufacturerId;                                    /**< Manufacturer ID. SZL_GP_COM_IND_ID_NOT_DEFINED = Invalid. */
  szl_uint16 ManufacturerModelId;                               /**< Manufacturer Model ID. SZL_GP_COM_IND_ID_NOT_DEFINED = Invalid */
  szl_uint8 GpdCommandIdListLength;                             /**< Number of valid items in GpdCommandIdList */
  szl_uint8 GpdCommandIdList[SZL_GP_COM_IND_MAX_LIST_LENGTH];   /**< List of commands supported by the GPD */
  szl_uint8 ServerClusterListLength;                            /**< Number of valid items in ServerClusterList */
  szl_uint16 ServerClusterList[SZL_GP_COM_IND_MAX_LIST_LENGTH]; /**< List of server clusters supported by the GPD */
  szl_uint8 ClientClusterListLength;                            /**< Number of valid items in ClientClusterList */
  szl_uint16 ClientClusterList[SZL_GP_COM_IND_MAX_LIST_LENGTH]; /**< List of client clusters supported by the GPD */
} SZL_GP_GpdCommissioningNtfParams_t;
#define SZL_GP_GpdCommissioningNtfParams_t_SIZE (sizeof(SZL_GP_GpdCommissioningNtfParams_t))


/**
 * Green Power Key Mode to be used in SZL_CB_GpdCommissioningNtf_t
 *
 * SZL_GP_COMMISSIONING_KEY_MODE_NO_SECURITY
 *  - Communications with this GPD will be in the clear. No security used.
 *  - May be set if:
 *   - SZL_GP_COM_IND_OPTIONS_t.GpSecurityKeyReq == True
 * SZL_GP_COMMISSIONING_KEY_MODE_SHARED
 *  - Communications with this GPD will be secured with a key. Key type will be as defined by the SharedKeyType attribute in the sink.
 *  - May be set if:
 *   - SZL_GP_COM_IND_OPTIONS_t.GpSecurityKeyReq == True
 *   - SZL_GP_COM_IND_OPTIONS_t.ExtOptionsPresent == True
 *   - SZL_GP_COM_IND_EXT_OPTIONS_t.SecurityLevelCapabilities > SZL_GP_SECURITY_LEVEL_CAP_NONE
 * SZL_GP_COMMISSIONING_KEY_MODE_OOB
 *  - Communications with this GPD will be secured with the Out Of Box key provided by the device.
 *  - May be set if:
 *   - SZL_GP_COM_IND_OPTIONS_t.ExtOptionsPresent == True
 *   - SZL_GP_COM_IND_EXT_OPTIONS_t.GpdKeyPresent == True
 *   - SZL_GP_COM_IND_EXT_OPTIONS_t.KeyType == SZL_GP_SECURITY_KEY_TYPE_OUT_OF_BOX
 * SZL_GP_COMMISSIONING_KEY_MODE_DEFAULT
 *  - ZAB will manage the security and apply the highest level supported by the device:
 *   - Priority 1 = Shared Key
 *   - Priority 2 = Out Of Box Key
 *   - Priority 3 = No security
 *  - May be set without conditions.
 */
typedef enum
{
  SZL_GP_COMMISSIONING_KEY_MODE_NO_SECURITY   = 0x00, /* No security */
  SZL_GP_COMMISSIONING_KEY_MODE_SHARED        = 0x01, /* Use the Shared key as defined by ATTRID_GP_SHARED_SECURITY_KEYTYPE */
  SZL_GP_COMMISSIONING_KEY_MODE_OOB           = 0x02, /* Use the Out Of Box key */
  SZL_GP_COMMISSIONING_KEY_MODE_DEFAULT       = 0x03  /* Use the info from the sink table to generate a default key */
} SZL_GP_COMMISSIONING_KEY_MODE_t;

/**
 * GPD Commissioning Notification Callback
 */
typedef szl_bool (*SZL_CB_GpdCommissioningNtf_t) (zabService* Service, struct _SZL_GP_GpdCommissioningNtfParams_t* Params, SZL_GP_COMMISSIONING_KEY_MODE_t* KeyMode);


/**
 * Type for GpCommissioningReplyReq.
 */
typedef struct
{
  SZL_Addresses_t Address;                            /**< Address of the GPD */
  SZL_GP_COM_IND_OPTIONS_t Options;                   /**<  */
  SZL_GP_COMMISSIONING_KEY_MODE_t KeyMode;
} SZL_GpComReplyReqParams_t;
#define SZL_GpComReplyReqParams_t_SIZE (sizeof(SZL_GpComReplyReqParams_t))


/**
 * Type for GpCommissioned Ntf
 */
typedef enum
{
  ZGP_COMMISSIONED_EVENT_COMMISSIONED,
  ZGP_COMMISSIONED_EVENT_DECOMMISSIONED,
} ENUM_ZGP_COMMISSIONED_EVENT_t;

typedef struct _SZL_GP_GpdCommissionedNtfParams_t
{
  ENUM_ZGP_COMMISSIONED_EVENT_t Event;                /**< Commissioned Event Type */
  SZL_Addresses_t Address;                            /**< Address of the GPD */
  szl_uint8  DeviceId;                                /**< Device ID of the GPD */
} SZL_GP_GpdCommissionedNtfParams_t;
#define SZL_GP_GpdCommissionedNtfParams_t_SIZE (sizeof(SZL_GP_GpdCommissionedNtfParams_t))

typedef void (*SZL_CB_GpdCommissionedNtf_t) ( zabService* Service,
                                              struct _SZL_GP_GpdCommissionedNtfParams_t* Params );



/**
 * Type for GpCmdReqParams.
 * This struct holds the information for the GP Command.
 */
typedef struct
{
    SZL_EP_t SourceEndpoint;            /**< The Endpoint for the source (App) */
    SZL_Addresses_t Address;            /**< The Addressing mode to specify how the device communicates with the destination. Must be a GPD address. */
    szl_uint8 GpCommand;                /**< The GP Command Id */
    szl_uint8 PayloadLength;            /**< The length of the payload to follow */
    szl_uint8 Payload[SZL_VLA_INIT];    /**< The ZGP command formatted payload in LITTLE ENDIAN - (extends beyond bounds) */
} SZL_GP_CmdReqParams_t;
#define SZL_GP_CmdReqParams_t_SIZE(_payload_size) (sizeof(SZL_GP_CmdReqParams_t) + (sizeof(szl_uint8) * (_payload_size)) - (sizeof(szl_uint8) * SZL_VLA_INIT))


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* _SZL_GP_TYPES_H_ */

