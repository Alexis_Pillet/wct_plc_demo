/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the profile wide client commands for the ZAB implementation
 *   of the SZL GP interface
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 * 00.00.06     22-Jul-14   MvdB   Original
 * 00.00.06_GP  15-Oct-14   MvdB   ARTF105853: Complete basic GP features for Smartlink IPZ
 * 01.00.00.02  29-Jan-15   MvdB   Support GPD De-commissioning
 * 002.000.003  06-Mar-15   MvdB   Remove GpAck. Code is no longer used.
 * 002.000.005  07-Apr-15   MvdB   ARTF115365: Add new API for sending GP commands - SZL_GP_CmdReq()
 * 002.000.009  17-Apr-15   MvdB   ARTF131022: Remove SZL_GP_Initialize() as it is now obsolete
 * 002.001.001  15-Jul-15   MvdB   ARTF130228: Add unique address mode for GpSrcId
 *                                 ARTF134857: Use SZL_AttributeReportNtfParams_t_SIZE() when mallocing notifications
 * 002.001.003  16-Jul-15   MvdB   ARTF132827: Include source address in cluster command handlers
 * 002.002.002  01-Sep-15   MvdB   ARTF147441: Support Key Mode in commissioningIndHandler()
 * 002.002.004  02-Sep-15   MvdB   Make sure app handler cannot modify SrcId or Options in commissioningIndHandler()
 * 002.002.005  10-Sep-15   MvdB   ARTF148813: zcl_GP_ProcessReportAttrMs_MultiCluster: Incorrect cluster ID on handler call if clusters are mixed
 *                                 ARTF148744: zcl_GP_ProcessReportAttrMs_MultiCluster: Overlap in memcpy causes crash
 * 002.002.009  07-Oct-15   MvdB   ARTF134853: Handle MS ranges of Green Power Attribute Ids
 * 002.002.010  09-Oct-15   MvdB   ARTF150980: Support Service ID in memory allocation functions for WTB
 * 002.002.021  21-Apr-16   MvdB   ARTF167807: Support Multi-Cluster Attribute Read/Write for GP
 * 002.002.022  24-May-16   MvdB   Fix casts of error response param ** in calls to SzlZab_GetSynchronousTransactionCallbackAndErrRspBySrcIdOptionalDestroy()
 *                                 Fix issue in SZL_GP_MultiCluster_ReadAttributesCfm() with fail response handling
 * 002.002.042  20-Jan-17   MvdB   ARTF199306: Release transaction before calling callback for GP Read/Write responses
 * 002.002.047  31-Jan-17   MvdB   Klockwork Review: Null check error response parameters before use
 *****************************************************************************/

/******************************************************************************
 * INCLUDES
 ******************************************************************************/
#include "szl_zab.h"
#include "zabCorePrivate.h"
#include "endianness.h"

#include "szl.h"

#ifdef SZL_CFG_ENABLE_GREEN_POWER

#include "zcl_manager_zab.h"
#include "szl_gp.h"


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS           *****
 *                      *****************************
 ******************************************************************************/

/* Green Power Cluster Id */
#define M_GP_CLUSTER_ID 0x0021

/* Max payload length for a GPD command. This has not been accurately calculated, just guestimated!*/
#define SZL_GP_M_MAX_COMMAND_LENGTH 75

/* Minimum value of a Manufacturer Specific Attribute Id.
 * Any value lower than this is a standard attribute.
 * WARNING: 0x4000 is now a Schneider specific value, as the ZCL has moved the range to 0x5000 to accommodate ZLL, but it's too late for us! */
#define M_MS_ATTRIBUTE_RANGE_START 0x4000

/* Minimum value of a Manufacturer Specific Cluster Id.
 * Any value lower than this is a standard cluster */
#define M_MS_CLUSTER_RANGE_START 0xFC00

/******************************************************************************
 * GP Report Common
 ******************************************************************************/
#define M_GP_REPORT_MS_CODE_SIZE 2

/* If present, MS Code always follows command ID */
#define M_GP_REPORT_MS_CODE_OFFSET 1

/******************************************************************************
 * GP Report and MS Report Structure
 ******************************************************************************/
/* Cmd ID + Cluster ID = 3   Note: Add 2 if MS */
#define M_GP_REPORT_HEADER_LENGTH 3

/* Cmd ID = 1   Note: Add 2 if MS */
#define M_GP_REPORT_CLUSTER_OFFSET 1

/* Attribute ID + Data Type */
#define M_GP_REPORT_ITEM_HEADER_SIZE 3

/* M_GP_REPORT_ITEM_HEADER_SIZE + one byte of data */
#define M_GP_MIN_REPORT_ITEM_SIZE (M_GP_REPORT_ITEM_HEADER_SIZE + 1)

/******************************************************************************
 * GP Multi-Cluster Report and MS Report Structure
 ******************************************************************************/
/* Cmd ID = 1   Note: Add 2 if MS */
#define M_GP_MULTI_CLUSTER_REPORT_HEADER_LENGTH 1

/* Cluster ID + Attribute ID + Data Type */
#define M_GP_MULTI_CLUSTER_REPORT_ITEM_HEADER_SIZE 5

/* M_GP_MULTI_CLUSTER_REPORT_ITEM_HEADER_SIZE + one byte of data */
#define M_GP_MIN_MULTI_CLUSTER_REPORT_ITEM_SIZE (M_GP_MULTI_CLUSTER_REPORT_ITEM_HEADER_SIZE + 1)


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/* Green Power Command IDs */
typedef enum
{
  GP_ZCL_CMD_ID_ATTRIB_REPORT                     = 0xA0,
  GP_ZCL_CMD_ID_ATTRIB_REPORT_MS                  = 0xA1,
  GP_ZCL_CMD_ID_ATTRIB_REPORT_MULTI_CLUSTER       = 0xA2,
  GP_ZCL_CMD_ID_ATTRIB_REPORT_MULTI_CLUSTER_MS    = 0xA3,
  GP_ZCL_CMD_ID_ATTRIB_READ_RESPONSE              = 0xA5,
  GP_ZCL_CMD_ID_ATTRIB_WRITE_RESPONSE             = 0xA7,
} GP_ZCL_CMD_ID;


/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Send a ZGP Commissioning Reply
 ******************************************************************************/
static void commissioningReplyReq(zabService* Service,
                                  SZL_GpComReplyReqParams_t* Params)
{
  szl_uint8 length;
  szl_uint8 tid = 0;
  sapMsg* Msg;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);

  /* Validate inputs */
  if (Params == NULL)
    {
      return;
    }

  printInfo(Service, "SZL GP: Sending Commissioning Reply for GPD 0x%08X\n", Params->Address.Addresses.GpSourceId.SourceId);

  /* Allocate SAP message, with enough data for the Params */
  length = SZL_GpComReplyReqParams_t_SIZE;
  Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
  if (erStatusIsError(&localStatus) || (Msg == NULL))
    {
      return;
    }

  /* Set message parameters */
  sapMsgSetAppDataMax (&localStatus, Msg, length);
  sapMsgSetAppDataLength(&localStatus, Msg, length);
  sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_GP_COM_REPLY_REQ);
  sapMsgSetAppTransactionId(&localStatus, Msg, tid);

  osMemCopy(&localStatus, sapMsgGetAppData(Msg), (szl_uint8*)Params, length);
  sapMsgPutFree(&localStatus, ZAB_SRV( Service )->DataSAP, Msg );
}

/******************************************************************************
 * ZGP Commissioning Indication Handler
 ******************************************************************************/
static void commissioningIndHandler(zabService* Service, sapMsg* Message)
{
  SZL_GP_GpdCommissioningNtfParams_t* szlNtf;
  SZL_GpComReplyReqParams_t szlReply;

  szlNtf = (SZL_GP_GpdCommissioningNtfParams_t*)sapMsgGetAppData(Message);

  if (szlNtf->Address.AddressMode == SZL_ADDRESS_MODE_GP_SOURCE_ID)
    {
      printInfo(Service, "SZL GP: Commissioning Request received for GPD 0x%08X\n", szlNtf->Address.Addresses.GpSourceId.SourceId);

      /* Build a Reply  */
      szlReply.Address = szlNtf->Address;

      /* We just give back the request options and GPZNP will sort it out */
      szlReply.Options = szlNtf->Options;

      szlReply.KeyMode = SZL_GP_COMMISSIONING_KEY_MODE_DEFAULT;
      if ( Szl_GpdCommissioningNtfCB(Service, szlNtf, &szlReply.KeyMode) == szl_true)
        {
          printInfo(Service, "SZL GP: Commissioning accepted for GPD 0x%08X\n", szlNtf->Address.Addresses.GpSourceId.SourceId);

          commissioningReplyReq(Service, &szlReply);
        }
      else
        {
          printInfo(Service, "SZL GP: Commissioning rejected for GPD 0x%018X\n", szlNtf->Address.Addresses.GpSourceId.SourceId);
        }
    }
}

/******************************************************************************
 * ZGP Commissioning Success Indication Handler
 ******************************************************************************/
static void commissioningNtfIndHandler(zabService* Service, sapMsg* Message)
{
  SZL_GP_GpdCommissionedNtfParams_t* szlNtf;

  szlNtf = (SZL_GP_GpdCommissionedNtfParams_t*)sapMsgGetAppData(Message);

  Szl_GpdCommissionedNtfCB(Service, szlNtf);
}

/******************************************************************************
 * ZGP Commissioning Indication Handler
 ******************************************************************************/
static void txQueueExpiryIndHandler(zabService* Service, sapMsg* Message)
{
  szl_uint32* srcIdPointer;

  srcIdPointer = (szl_uint32*)sapMsgGetAppData(Message);

  printInfo(Service, "SZL GP: Tx Queue Expiry for GPD 0x%08X\n", *srcIdPointer);

  SzlZab_GenerateSynchronousTransactionFailureBySrcId(Service, *srcIdPointer, SZL_STATUS_FAILED);
}


/******************************************************************************
 * Process a GP Attribute Report.
 * Support Single/Multi Cluster and Standard/MS
 ******************************************************************************/
static ZCL_STATUS zcl_GP_ProcessReportAttr(erStatus* Status, zabService* Service, zabMsgProDataInd* dp, szl_bool ManufacturerSpecific, szl_bool MultiCluster)
{
  SZL_AttributeReportNtfParams_t* ntf;
  szl_uint16* clusters;
  szl_uint8* attrs_ptr;
  szl_uint8 num_attributes;
  szl_uint8 attrIndex;
  szl_uint8* zcl;
  szl_uint8 zclIndex;
  szl_uint8 zbDataLength;
  ZCL_STATUS zclSts = ZCL_SUCCESS;
  SZL_RESULT_t res;

  /* Setup info about size/position within the ZCL payload */
  szl_uint8 minLength;                // Minimum valid payload length
  szl_uint8 payloadOffset;            // Offset of the payload, after the header
  szl_uint8 itemHeaderSize;           // Size of attribute item header (before the data). Including attribute Id, Datatype, maybe cluster
  szl_uint8 singleClusterOffset = 0;  // Offset of the cluster Id for non-multi cluster commands. Don't care for multi cluster.
  if (MultiCluster)
    {
      minLength = M_GP_MULTI_CLUSTER_REPORT_HEADER_LENGTH + M_GP_MIN_MULTI_CLUSTER_REPORT_ITEM_SIZE;
      payloadOffset = M_GP_MULTI_CLUSTER_REPORT_HEADER_LENGTH;
      itemHeaderSize = M_GP_MULTI_CLUSTER_REPORT_ITEM_HEADER_SIZE;
    }
  else
    {
      minLength = M_GP_REPORT_HEADER_LENGTH + M_GP_MIN_REPORT_ITEM_SIZE;
      payloadOffset = M_GP_REPORT_HEADER_LENGTH;
      itemHeaderSize = M_GP_REPORT_ITEM_HEADER_SIZE;
      singleClusterOffset = M_GP_REPORT_CLUSTER_OFFSET;
    }
  if (ManufacturerSpecific)
    {
      minLength += M_GP_REPORT_MS_CODE_SIZE;
      payloadOffset += M_GP_REPORT_MS_CODE_SIZE;
      singleClusterOffset += M_GP_REPORT_MS_CODE_SIZE;
    }

  /* Validate inputs */
  if ( (dp == NULL) || (dp->data == NULL) || (dp->dataLength < minLength ) )
    {
      return ZCL_SOFTWARE_FAILURE;
    }
  zcl = dp->data;

  /* If MS, exit if the manufacturer code is not ours */
  if ( (ManufacturerSpecific == szl_true) &&
       (COPY_IN_16_BITS(&zcl[M_GP_REPORT_MS_CODE_OFFSET]) != ZB_SCHNEIDER_MANUFACTURE_ID) )
    {
      return ZCL_UNSUP_MANUF_GENERAL_COMMAND;
    }

  /* Get the number of attributes */
  attrs_ptr = &zcl[payloadOffset];
  num_attributes = 0;
  while ( attrs_ptr < (zcl + dp->dataLength))
    {
      num_attributes++;
      attrs_ptr += itemHeaderSize + ZbDataSize(attrs_ptr[itemHeaderSize-1], // Data Type is always last byte of item header
                                               &attrs_ptr[itemHeaderSize]); // Data Type follows last byte of item header
    }

  // If no attributes then something went wrong...
  if (num_attributes == 0)
    {
      return ZCL_MALFORMED_COMMAND;
    }

  /* Malloc notification */
  ntf = (SZL_AttributeReportNtfParams_t*)szl_mem_alloc(SZL_AttributeReportNtfParams_t_SIZE(num_attributes),
                                                       MALLOC_ID_SZL_GP_REP_NTF);
  if (ntf == NULL)
    {
      printError(Service, "zcl_GP_ProcessReportAttr_MultiCluster: malloc fail\n");
      return ZCL_SOFTWARE_FAILURE;
    }

  clusters = (szl_uint16*)szl_mem_alloc(num_attributes * sizeof(szl_uint16),
                                        MALLOC_ID_SZL_GP_REP_NTF);
  if (clusters == NULL)
    {
      szl_mem_free(ntf);
      printError(Service, "zcl_GP_ProcessReportAttr_MultiCluster: malloc2 fail\n");
      return ZCL_SOFTWARE_FAILURE;
    }
  /* WARNING - MAKE SURE YOU FREE rsp and clusters IF YOU ADD ANY RETURNS AFTER THIS POINT!!! */


  ntf->DestinationEndpoint = dp->dstAddr.endpoint;

  if (ZclM_ConvertZabToSzlAddress(&dp->srcAddr, &ntf->SourceAddress) == szl_true)
    {
      if (MultiCluster == szl_false)
        {
          ntf->ClusterID = COPY_IN_16_BITS(&zcl[singleClusterOffset]);
        }
      zclIndex = payloadOffset;

      /* Add attributes until we run out of data or reach the limit of our arrays */
      attrIndex = 0;
      while ( (dp->dataLength >= (zclIndex+(itemHeaderSize+1))) && (attrIndex < num_attributes) )
        {
          if (MultiCluster)
            {
              clusters[attrIndex] = COPY_IN_16_BITS(&(dp->data[zclIndex])); zclIndex+=2;
            }
          else
            {
              /* This is a little inefficient, but avoids big if's in the sort that follows */
              clusters[attrIndex] = ntf->ClusterID;
            }

          res = ZclM_ExtractAttributeReportItem(Service,
                                                &ntf->Attributes[attrIndex],
                                                &(dp->data[zclIndex]),
                                                dp->dataLength - zclIndex,
                                                &zbDataLength);
          if (res != SZL_RESULT_SUCCESS)
            {
              /* There was an error parsing the frame, so we will only indicate whatever we got so far */
              zclSts = ZCL_FAILURE;
              break;
            }

          zclIndex += zbDataLength;
          attrIndex++;
        }

      // Handle in case less attributes were found than expected
      if (attrIndex < num_attributes)
        {
          num_attributes = attrIndex;
        }

      // At this point we have all the attributes and their clusters but they are unsorted.
      // We want to gather up all of the attributes from the same cluster and MS to report at once, so we will:
      //   Start with the first attribute
      //   Check all other attributes and move them up to the front if they have the same:
      //    - Cluster
      //    - If MS, MS Attribute range
      //   Notify how ever many we found for the cluster and MS range
      //   Shuffle the remaining attributes down and repeat.
      while (num_attributes > 0)
        {
          // The index where we should copy any attribute whose cluster match cluster[0]
          szl_uint8 nextIndex = 1;

          // Temp buffer used when swapping attributes
          SZL_AttributeData_t tempAttr;
          szl_uint16 tempCluster;

          // Loop through each remaining attribute looking for matches to cluster [0]
          for (attrIndex = nextIndex; attrIndex < num_attributes; attrIndex++)
            {
              szl_bool rangeMatch;

              /* We are of the same attribute range if:
               *  - Clusters are the same,
               *  - AND
               *   - MS is false
               *   - OR (implied MS is true)
               *   - Cluster is in MS Range
               *   - OR
               *   - Attribute range is the same */
              if ( (clusters[attrIndex] == clusters[0]) &&
                   ( (ManufacturerSpecific == szl_false) ||
                     (clusters[0] >= M_MS_CLUSTER_RANGE_START) ||
                     ( (ntf->Attributes[attrIndex].AttributeID < M_MS_ATTRIBUTE_RANGE_START) == (ntf->Attributes[0].AttributeID < M_MS_ATTRIBUTE_RANGE_START) ) ) )
                {
                  rangeMatch = szl_true;
                }
              else
                {
                  rangeMatch = szl_false;
                }

              if (rangeMatch == szl_true)
                {
                  // If we are at nextIndex then we don't need to do anything as it is already ordered.
                  // If not at nextIndex then swap
                  if (attrIndex != nextIndex)
                    {
                      szl_memcpy(&tempAttr, &ntf->Attributes[nextIndex], sizeof(SZL_AttributeData_t));
                      szl_memcpy(&ntf->Attributes[nextIndex], &ntf->Attributes[attrIndex], sizeof(SZL_AttributeData_t));
                      szl_memcpy(&ntf->Attributes[attrIndex], &tempAttr, sizeof(SZL_AttributeData_t));

                      // ARTF148813: Also copy cluster here otherwise we jumble it up!
                      tempCluster = clusters[nextIndex];
                      clusters[nextIndex] = clusters[attrIndex];
                      clusters[attrIndex] = tempCluster;
                    }

                  nextIndex++;
                }
            }

          // Setup the number of attributes and the cluster and notify
          if ( (ManufacturerSpecific) &&
               ( (clusters[0] >= M_MS_CLUSTER_RANGE_START) ||
                 (ntf->Attributes[0].AttributeID >= M_MS_ATTRIBUTE_RANGE_START) ) )
            {
              ntf->ManufacturerSpecific = szl_true;
            }
          else
            {
              ntf->ManufacturerSpecific = szl_false;
            }
          ntf->NumberOfAttributes = nextIndex;
          ntf->ClusterID = clusters[0];
          Szl_AttributeReportNtfCB(Service, ntf);

          // Free data of reported attributes
          for (attrIndex = 0; attrIndex < ntf->NumberOfAttributes; attrIndex++)
            {
              szl_mem_free(ntf->Attributes[attrIndex].Data);
              ntf->Attributes[attrIndex].Data = NULL;
            }

          // Calculate the number of attributes remaining to report and copy them down to start at index 0
          num_attributes -= ntf->NumberOfAttributes;

          /* ARTF148744: The original memcpy could overlap and cause a crash on some system, hence they
           *             have been replaced with single copies to ensure overlap does not occur.
           *             Note: memmove was another option, but i prefer not to introduce more macros. */
          //szl_memcpy(&ntf->Attributes[0], &ntf->Attributes[nextIndex], num_attributes * sizeof(SZL_AttributeData_t));
          //szl_memcpy(&clusters[0], &clusters[nextIndex], num_attributes * sizeof(szl_uint16));
          for (attrIndex = 0; attrIndex < num_attributes; attrIndex++)
            {
              szl_memcpy(&ntf->Attributes[attrIndex], &ntf->Attributes[nextIndex+attrIndex], sizeof(SZL_AttributeData_t));
              szl_memcpy(&clusters[attrIndex], &clusters[nextIndex+attrIndex], sizeof(szl_uint16));
            }
        }
    }
  /* Free up the callback params */
  szl_mem_free(ntf);
  szl_mem_free(clusters);

  return zclSts;
}


/******************************************************************************
 * Process a GP Multi Cluster Read Attributes Response
 ******************************************************************************/
/* Options= 1 */
#define M_GP_READ_HEADER_LENGTH 1

/* Options + Cluster ID = 3 */
#define M_GP_MS_READ_HEADER_LENGTH 3

/* Cluster ID + Length of List = 3 */
#define M_GP_MIN_CLUSTER_REC_SIZE 3
static ZCL_STATUS SZL_GP_MultiCluster_ReadAttributesCfm(zabService* Service, zabMsgProDataInd* dp)
{
    syncCallback Callback;
    SZL_AttributeReadRespParams_t* errorResponseParams = NULL;
    SZLEXT_MultiCluster_AttributeReadRespParams_t* errorResponseParamsMulti = NULL;
    ZCL_PAYLOAD_t* zcl;
    szl_uint8* eod;
    szl_uint8* clusterEod;
    szl_uint16 cluster;
#ifdef SZL_CFG_ENABLE_GREEN_POWER
    szl_uint8* data_ptr;
#endif
    szl_uint8 tid = 0;
    int i;

    /* Get pointers to ZCL data and the start of the common section */
    zcl = (ZCL_PAYLOAD_t*)dp->data;
    eod = (szl_uint8*)zcl + dp->dataLength;


    if (dp->dataLength < (1 + M_GP_READ_HEADER_LENGTH + M_GP_MIN_CLUSTER_REC_SIZE) )
    {
      return ZCL_SOFTWARE_FAILURE;
    }

    /* Exit if Manufacturer Specific and the manufacturer code is not ours */
    if (zcl->frame_type.zgp_frame.payload[0] & 0x02)
      {
        if (COPY_IN_16_BITS(&zcl->frame_type.zgp_frame.payload[1]) != ZB_SCHNEIDER_MANUFACTURE_ID)
          {
            return ZCL_UNSUP_MANUF_GENERAL_COMMAND;
          }
        data_ptr = &zcl->frame_type.zgp_frame.payload[M_GP_MS_READ_HEADER_LENGTH];
      }
    else
      {
        data_ptr = &zcl->frame_type.zgp_frame.payload[M_GP_READ_HEADER_LENGTH];
      }

    /* If it's a multi cluster response then we must have a multi cluster request.
     * If its not a multi cluster response it could be a single cluster request, or part of a multi clustre response
     * split across multi frames... */
    szl_bool multi = szl_true;
    Callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspBySrcIdOptionalDestroy(Service,
                                                                                       SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MULTI_CLUSTER_READ_ATTRIBUTE_REQ,
                                                                                       dp->srcAddr.address.sourceId,
                                                                                       (void**)&errorResponseParamsMulti,
                                                                                       &tid,
                                                                                       szl_false);
    if ( (Callback.SZLEXT_CB_MultiCluster_AttributeReadResp == NULL) &&
         ((zcl->frame_type.zgp_frame.payload[0] & 0x01) == 0) )
      {
        multi = szl_false;
        Callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspBySrcIdOptionalDestroy(Service,
                                                                                           SZL_ZAB_SYNC_TRANS_TYPE_ZCL_READ_ATTRIBUTE_REQ,
                                                                                           dp->srcAddr.address.sourceId,
                                                                                           (void**)&errorResponseParams,
                                                                                           &tid,
                                                                                           szl_false);
      }

    // if we do not have a callback, then skip the handling for this message
    if (Callback.SZLZAB_CB_GenericResp == NULL)
      {
          printError(Service, "SZL GP: WARNING: readAttrRsp callback not found\n");
          return ZCL_SUCCESS;
      }

    // Check errorresponseParams are valid before we use the pointer later
    if ( ( (multi == szl_true) && (errorResponseParamsMulti == NULL) ) ||
         ( (multi == szl_false) && (errorResponseParams == NULL) ) )
      {
        printError(Service, "SZL GP: WARNING: errorResponseParams/Multi invalid\n");
        return ZCL_SUCCESS;
      }

    while (eod > data_ptr)
      {
        cluster = COPY_IN_16_BITS(data_ptr); data_ptr += 2;
        clusterEod = data_ptr + *data_ptr; data_ptr++;
        while (clusterEod > data_ptr)
          {
            // get pointer of attributes type
            ZCL_READ_ATTRIBUTE_STATUS_t* attrs = (ZCL_READ_ATTRIBUTE_STATUS_t*)(data_ptr);
            szl_uint16 attrId;
            SZL_STATUS_t convertStatus;
            NATIVE_DATA_t src;
            void *pSrc = NULL;
            szl_uint8 src_len = 0;
            szl_uint16 src_len_16 = 0;

            attrId = LE_TO_UINT16( attrs->attribute_id );
            convertStatus = attrs->status;
            if (attrs->status == ZCL_SUCCESS)
              {
                // supported data types
                switch (attrs->data_type)
                  {
                    case SZL_ZB_DATATYPE_BOOLEAN:
                    case SZL_ZB_DATATYPE_DATA8:
                    case SZL_ZB_DATATYPE_UINT8:
                    case SZL_ZB_DATATYPE_INT8:
                    case SZL_ZB_DATATYPE_ENUM8:
                    case SZL_ZB_DATATYPE_BITMAP8:
                    case SZL_ZB_DATATYPE_DATA16:
                    case SZL_ZB_DATATYPE_UINT16:
                    case SZL_ZB_DATATYPE_INT16:
                    case SZL_ZB_DATATYPE_ENUM16:
                    case SZL_ZB_DATATYPE_BITMAP16:
                    case SZL_ZB_DATATYPE_DATA24:
                    case SZL_ZB_DATATYPE_UINT24:
                    case SZL_ZB_DATATYPE_INT24:
                    case SZL_ZB_DATATYPE_BITMAP24:
                    case SZL_ZB_DATATYPE_DATA32:
                    case SZL_ZB_DATATYPE_UINT32:
                    case SZL_ZB_DATATYPE_INT32:
                    case SZL_ZB_DATATYPE_BITMAP32:
                    case SZL_ZB_DATATYPE_TOD:
                    case SZL_ZB_DATATYPE_DATE:
                    case SZL_ZB_DATATYPE_UTC:
                    case SZL_ZB_DATATYPE_DATA40:
                    case SZL_ZB_DATATYPE_UINT40:
                    case SZL_ZB_DATATYPE_INT40:
                    case SZL_ZB_DATATYPE_BITMAP40:
                    case SZL_ZB_DATATYPE_DATA48:
                    case SZL_ZB_DATATYPE_UINT48:
                    case SZL_ZB_DATATYPE_INT48:
                    case SZL_ZB_DATATYPE_BITMAP48:
                    case SZL_ZB_DATATYPE_DATA56:
                    case SZL_ZB_DATATYPE_UINT56:
                    case SZL_ZB_DATATYPE_INT56:
                    case SZL_ZB_DATATYPE_BITMAP56:
                    case SZL_ZB_DATATYPE_DATA64:
                    case SZL_ZB_DATATYPE_UINT64:
                    case SZL_ZB_DATATYPE_INT64:
                    case SZL_ZB_DATATYPE_BITMAP64:
                    case SZL_ZB_DATATYPE_CLUSTER_ID:
                    case SZL_ZB_DATATYPE_ATTR_ID:
                    case SZL_ZB_DATATYPE_BAC_OID:
                    case SZL_ZB_DATATYPE_IEEE_ADDR:
                    case SZL_ZB_DATATYPE_SINGLE_PREC:
                    case SZL_ZB_DATATYPE_DOUBLE_PREC:
                        // we converts the stream from LE into native type
                        // the src_len is the native length and the src is the native data ptr
                        if (!ZbToNative(attrs->data_type, attrs->data, ZbDataSize(attrs->data_type, attrs->data), &src, &src_len ) )
                        {
                            convertStatus = SZL_STATUS_INVALID_DATA_TYPE;
                        }
                        pSrc = &src;
                        break;

                    case SZL_ZB_DATATYPE_CHAR_STR:
                    case SZL_ZB_DATATYPE_OCTET_STR:
                        if (*(szl_uint8*)attrs->data == SZL_ZB_DATATYPE_CHAR_OCTET_STR_INVALID_VALUE)
                          {
                            src_len = 0;
                          }
                        else
                          {
                            src_len = *(szl_uint8*)attrs->data + 1; // the length field is in index[0], plus the null terminator
                          }
                        pSrc = (szl_uint8*)attrs->data + 1;// the real data is in index[1]
                        break;

                    case SZL_ZB_DATATYPE_LONG_OCTET_STR:
                        src_len_16 = (LE_TO_UINT16( *(szl_uint16*)attrs->data )); // the length field is in index[0-2]
                        src_len = (szl_uint8)src_len_16;  // DODGY - TODO hande 16 bit length!
                        pSrc = (szl_uint8*)attrs->data + 2;// the real data is in index[2]
                        break;

                    case SZL_ZB_DATATYPE_128_BIT_SEC_KEY:
                        src_len = 16;
                        pSrc = attrs->data;
                        break;

                    default:
                        printError(Service, "SZL GP: Error in SZL_GP_MultiCluster_ReadAttributesCfm() - Unsupported attribute data type 0x%02X\n", attrs->data_type);
                        convertStatus = SZL_STATUS_UNSUPPORTED_ATTRIBUTE;
                        break;
                  }
              }
            if (multi == szl_true)
              {
                for (i = 0; i < errorResponseParamsMulti->NumberOfAttributes; i++)
                  {
                    if ( (errorResponseParamsMulti->Attributes[i].ClusterID == cluster) &&
                         (errorResponseParamsMulti->Attributes[i].AttributeID == attrId) )
                      {
                        errorResponseParamsMulti->Attributes[i].Status = convertStatus;
                        if (errorResponseParamsMulti->Attributes[i].Status == ZCL_SUCCESS)
                          {
                            // Over alloc by 1 to ensure we have space to zero terminate strings
                            errorResponseParamsMulti->Attributes[i].Data = szl_mem_alloc(src_len+1,
                                                                                         MALLOC_ID_SZL_READ_ATTR_RSP_NATIVE_DATA);
                            if ( (errorResponseParamsMulti->Attributes[i].Data != NULL) &&
                                 (pSrc != NULL) )
                              {
                                errorResponseParamsMulti->Attributes[i].DataType = attrs->data_type;
                                errorResponseParamsMulti->Attributes[i].DataLength = src_len;
                                szl_memcpy(errorResponseParamsMulti->Attributes[i].Data, pSrc, src_len);
                                // Insert zero terminator. Only needed for strings, but doesn't hurt to do every time
                                ((szl_uint8*)errorResponseParamsMulti->Attributes[i].Data)[src_len] = 0;
                              }
                            else
                              {
                                errorResponseParamsMulti->Attributes[i].Status = ZCL_SOFTWARE_FAILURE;
                                errorResponseParamsMulti->Attributes[i].DataType = SZL_ZB_DATATYPE_NO_DATA;
                                errorResponseParamsMulti->Attributes[i].DataLength = 0;
                              }
                          }
                      }
                  }
              }
            else
              {
                if (errorResponseParams->ClusterID == cluster)
                  {
                    for (i = 0; i < errorResponseParams->NumberOfAttributes; i++)
                      {
                        if (errorResponseParams->Attributes[i].AttributeID == attrId)
                          {
                            errorResponseParams->Attributes[i].Status = convertStatus;
                            if (errorResponseParams->Attributes[i].Status == ZCL_SUCCESS)
                              {
                                // Over alloc by 1 to ensure we have space to zero terminate strings
                                errorResponseParams->Attributes[i].Data = szl_mem_alloc(src_len+1,
                                                                                             MALLOC_ID_SZL_READ_ATTR_RSP_NATIVE_DATA);
                                if ( (errorResponseParams->Attributes[i].Data != NULL) &&
                                     (pSrc != NULL) )
                                  {
                                    errorResponseParams->Attributes[i].DataType = attrs->data_type;
                                    errorResponseParams->Attributes[i].DataLength = src_len;
                                    szl_memcpy(errorResponseParams->Attributes[i].Data, pSrc, src_len);
                                    // Insert zero terminator. Only needed for strings, but doesn't hurt to do every time
                                    ((szl_uint8*)errorResponseParams->Attributes[i].Data)[src_len] = 0;
                                  }
                                else
                                  {
                                    errorResponseParams->Attributes[i].Status = ZCL_SOFTWARE_FAILURE;
                                    errorResponseParams->Attributes[i].DataType = SZL_ZB_DATATYPE_NO_DATA;
                                    errorResponseParams->Attributes[i].DataLength = 0;
                                  }
                              }
                          }
                      }
                  }
              }

            // advance to next attribute
            if (attrs->status == ZCL_SUCCESS)
              {
                data_ptr += ZCL_READ_ATTRIBUTE_STATUS_SIZE_GP(attrs->status, ZbDataSize(attrs->data_type, attrs->data));
              }
            else
              {
                data_ptr += ZCL_READ_ATTRIBUTE_STATUS_SIZE_GP(attrs->status, 0);
              }
          }
      }

    /* Now we check if all attributes have a status that is no longer SZL_STATUS_NO_RESPONSE.
     *  If so, the responses are complete and we call the callback.
     *  If not, put it back on the response queue for a while to await further frames */
    if (multi == szl_true)
      {
        for (i = 0; i < errorResponseParamsMulti->NumberOfAttributes; i++)
          {
            if (errorResponseParamsMulti->Attributes[i].Status == SZL_STATUS_NO_RESPONSE)
              {
                break;
              }
          }
        if (i < errorResponseParamsMulti->NumberOfAttributes)
          {
            SzlZab_UpdateGreenPowerSynchronousTransactiontimeout(Service,
                                                                 SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MULTI_CLUSTER_READ_ATTRIBUTE_REQ,
                                                                 dp->srcAddr.address.sourceId,
                                                                 SZL_ZAB_M_GP_MULTI_FRAME_RESPONSE_DELAY_S);
          }
        else
          {
            Callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspBySrcIdOptionalDestroy(Service,
                                                                                               SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MULTI_CLUSTER_READ_ATTRIBUTE_REQ,
                                                                                               dp->srcAddr.address.sourceId,
                                                                                               (void**)&errorResponseParamsMulti,
                                                                                               &tid,
                                                                                               szl_true);
            if (Callback.SZLEXT_CB_MultiCluster_AttributeReadResp != NULL)
              {
                Callback.SZLEXT_CB_MultiCluster_AttributeReadResp(Service, SZL_STATUS_SUCCESS, errorResponseParamsMulti, tid);
              }
            if (errorResponseParamsMulti != NULL)
              {
                for (i = 0; i < errorResponseParamsMulti->NumberOfAttributes; i ++)
                  {
                    if (errorResponseParamsMulti->Attributes[i].Data != NULL)
                      {
                        szl_mem_free(errorResponseParamsMulti->Attributes[i].Data);
                      }
                  }
                szl_mem_free(errorResponseParamsMulti);
              }
          }
      }
    else
      {
        for (i = 0; i < errorResponseParams->NumberOfAttributes; i++)
          {
            if (errorResponseParams->Attributes[i].Status == SZL_STATUS_NO_RESPONSE)
              {
                break;
              }
          }
        if (i < errorResponseParams->NumberOfAttributes)
          {
            SzlZab_UpdateGreenPowerSynchronousTransactiontimeout(Service,
                                                                 SZL_ZAB_SYNC_TRANS_TYPE_ZCL_READ_ATTRIBUTE_REQ,
                                                                 dp->srcAddr.address.sourceId,
                                                                 SZL_ZAB_M_GP_MULTI_FRAME_RESPONSE_DELAY_S);
          }
        else
          {
            Callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspBySrcIdOptionalDestroy(Service,
                                                                                       SZL_ZAB_SYNC_TRANS_TYPE_ZCL_READ_ATTRIBUTE_REQ,
                                                                                       dp->srcAddr.address.sourceId,
                                                                                       (void**)&errorResponseParams,
                                                                                       &tid,
                                                                                           szl_true);
            if (Callback.SZL_CB_AttributeReadResp != NULL)
              {
                Callback.SZL_CB_AttributeReadResp(Service, SZL_STATUS_SUCCESS, errorResponseParams, tid);
              }

            if (errorResponseParams != NULL)
              {
                for (i = 0; i < errorResponseParams->NumberOfAttributes; i ++)
                  {
                    if (errorResponseParams->Attributes[i].Data != NULL)
                      {
                        szl_mem_free(errorResponseParams->Attributes[i].Data);
                      }
                  }
                szl_mem_free(errorResponseParams);
              }
          }
      }

    return ZCL_SUCCESS;
}


/******************************************************************************
 * Process a GP Multi Cluster Write Attributes Response
 ******************************************************************************/
/* Options= 1 */
#define M_GP_WRITE_HEADER_LENGTH 1

/* Options + Cluster ID = 3 */
#define M_GP_MS_WRITE_HEADER_LENGTH 3

static ZCL_STATUS SZL_GP_MultiCluster_WriteAttributesCfm(zabService* Service, zabMsgProDataInd* dp)
{
    syncCallback Callback;
    SZLEXT_MultiCluster_AttributeWriteRespParams_t* errorResponseParamsMulti;
    SZL_AttributeWriteRespParams_t* errorResponseParams;
    ZCL_PAYLOAD_t* zcl;
    szl_uint8* eod;
    szl_uint8 payload_size;
    szl_uint16 cluster;
    szl_uint8* data_ptr;
    szl_uint8 tid = 0;
    int i;

    /* Get pointers to ZCL data and the start of the common section */
    zcl = (ZCL_PAYLOAD_t*)dp->data;
    eod = (szl_uint8*)zcl + dp->dataLength;

    if (dp->dataLength < (1 + M_GP_WRITE_HEADER_LENGTH + M_GP_MIN_CLUSTER_REC_SIZE) )
    {
      return ZCL_SOFTWARE_FAILURE;
    }

    /* Exit if Manufacturer Specific and the manufacturer code is not ours */
    if (zcl->frame_type.zgp_frame.payload[0] & 0x02)
      {
        if (COPY_IN_16_BITS(&zcl->frame_type.zgp_frame.payload[1]) != ZB_SCHNEIDER_MANUFACTURE_ID)
          {
            return ZCL_UNSUP_MANUF_GENERAL_COMMAND;
          }
        data_ptr = &zcl->frame_type.zgp_frame.payload[M_GP_MS_WRITE_HEADER_LENGTH];
      }
    else
      {
        data_ptr = &zcl->frame_type.zgp_frame.payload[M_GP_WRITE_HEADER_LENGTH];
      }



    /* If it's a multi cluster response then we must have a multi cluster request.
     * If its not a multi cluster response it could be a single cluster request, or part of a multi clustre response
     * split across multi frames... */
    szl_bool multi = szl_true;
    Callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspBySrcIdOptionalDestroy(Service,
                                                                                       SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MULTI_CLUSTER_WRITE_ATTRIBUTE_REQ,
                                                                                       dp->srcAddr.address.sourceId,
                                                                                       (void**)&errorResponseParamsMulti,
                                                                                       &tid,
                                                                                       szl_false);
    if ( (Callback.SZLEXT_CB_MultiCluster_AttributeWriteResp == NULL) &&
         ((zcl->frame_type.zgp_frame.payload[0] & 0x01) == 0) )
      {
        multi = szl_false;
        Callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspBySrcIdOptionalDestroy(Service,
                                                                                           SZL_ZAB_SYNC_TRANS_TYPE_ZCL_WRITE_ATTRIBUTE_REQ,
                                                                                           dp->srcAddr.address.sourceId,
                                                                                           (void**)&errorResponseParams,
                                                                                           &tid,
                                                                                           szl_false);
      }

    // if we do not have a callback, then skip the handling for this message
    if (Callback.SZLZAB_CB_GenericResp == NULL)
      {
          printError(Service, "SZL GP: WARNING: WriteAttrRsp callback not found\n");
          return ZCL_SUCCESS;
      }

    // Check errorresponseParams are valid before we use the pointer later
    if ( ( (multi == szl_true) && (errorResponseParamsMulti == NULL) ) ||
         ( (multi == szl_false) && (errorResponseParams == NULL) ) )
      {
        printError(Service, "SZL GP: WARNING: errorResponseParams/Multi invalid\n");
        return ZCL_SUCCESS;
      }

    szl_uint8* cluster_eod;
    while (data_ptr < eod)
      {
        cluster = COPY_IN_16_BITS(data_ptr); data_ptr+=2;

        payload_size = *data_ptr++;
        cluster_eod = data_ptr + payload_size;

        // Check for a single status for the whole cluster
        if (payload_size == 1)
          {
            if (multi == szl_true)
              {
                for (i = 0; i < errorResponseParamsMulti->NumberOfAttributes; i++)
                  {
                    if (errorResponseParamsMulti->Attributes[i].ClusterID == cluster)
                      {
                        errorResponseParamsMulti->Attributes[i].Status = *data_ptr;
                      }
                  }
              }
            else
              {
                if (errorResponseParams->ClusterID == cluster)
                  {
                    for (i = 0; i < errorResponseParams->NumberOfAttributes; i++)
                      {
                        errorResponseParams->Attributes[i].Status = *data_ptr;
                      }
                  }
                else
                  {
                    printError(Service, "SZL GP: WARNING: WriteAttrRsp single no cluster match\n");
                  }
              }
          }
        else
          {
            while (data_ptr < cluster_eod)
              {
                SZL_STATUS_t sts = *data_ptr++;
                szl_uint16 attrId = COPY_IN_16_BITS(data_ptr); data_ptr+=2;

                if (multi == szl_true)
                  {
                    for (i = 0; i < errorResponseParamsMulti->NumberOfAttributes; i++)
                      {
                        if ( (errorResponseParamsMulti->Attributes[i].ClusterID == cluster) &&
                             (errorResponseParamsMulti->Attributes[i].AttributeID == attrId) )
                          {
                            errorResponseParamsMulti->Attributes[i].Status = sts;
                            break;
                          }
                      }
                  }
                else
                  {
                    if (errorResponseParams->ClusterID == cluster)
                      {
                        for (i = 0; i < errorResponseParams->NumberOfAttributes; i++)
                          {
                            if (errorResponseParams->Attributes[i].AttributeID == attrId)
                              {
                                errorResponseParams->Attributes[i].Status = sts;
                                break;
                              }
                          }
                      }
                  }
              }
          }
      }

    /* Now we check if all attributes have a status that is no longer SZL_STATUS_NO_RESPONSE.
     *  If so, the responses are complete and we call the callback.
     *  If not, put it back on the response queue for a while to await further frames */
    if (multi == szl_true)
      {
        for (i = 0; i < errorResponseParamsMulti->NumberOfAttributes; i++)
          {
            if (errorResponseParamsMulti->Attributes[i].Status == SZL_STATUS_NO_RESPONSE)
              {
                break;
              }
          }
        if (i < errorResponseParamsMulti->NumberOfAttributes)
          {
            SzlZab_UpdateGreenPowerSynchronousTransactiontimeout(Service,
                                                                 SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MULTI_CLUSTER_WRITE_ATTRIBUTE_REQ,
                                                                 dp->srcAddr.address.sourceId,
                                                                 SZL_ZAB_M_GP_MULTI_FRAME_RESPONSE_DELAY_S);
          }
        else
          {

            // TODO This could be cleaned up into a Remove call
            Callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspBySrcIdOptionalDestroy(Service,
                                                                                               SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MULTI_CLUSTER_WRITE_ATTRIBUTE_REQ,
                                                                                               dp->srcAddr.address.sourceId,
                                                                                               (void**)&errorResponseParamsMulti,
                                                                                               &tid,
                                                                                               szl_true);
            Callback.SZLEXT_CB_MultiCluster_AttributeWriteResp(Service, SZL_STATUS_SUCCESS, errorResponseParamsMulti, tid);
            szl_mem_free(errorResponseParamsMulti);
          }
      }
    else
      {
        for (i = 0; i < errorResponseParams->NumberOfAttributes; i++)
          {
            if (errorResponseParams->Attributes[i].Status == SZL_STATUS_NO_RESPONSE)
              {
                break;
              }
          }
        if (i < errorResponseParams->NumberOfAttributes)
          {
            SzlZab_UpdateGreenPowerSynchronousTransactiontimeout(Service,
                                                                 SZL_ZAB_SYNC_TRANS_TYPE_ZCL_WRITE_ATTRIBUTE_REQ,
                                                                 dp->srcAddr.address.sourceId,
                                                                 SZL_ZAB_M_GP_MULTI_FRAME_RESPONSE_DELAY_S);
          }
        else
          {
            Callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspBySrcIdOptionalDestroy(Service,
                                                                                               SZL_ZAB_SYNC_TRANS_TYPE_ZCL_WRITE_ATTRIBUTE_REQ,
                                                                                               dp->srcAddr.address.sourceId,
                                                                                               (void**)&errorResponseParams,
                                                                                               &tid,
                                                                                               szl_true);
            Callback.SZL_CB_AttributeWriteResp(Service, SZL_STATUS_SUCCESS, errorResponseParams, tid);
            szl_mem_free(errorResponseParams);
          }
      }

    return ZCL_SUCCESS;
}


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/**
 * Register Handler for Green Power Commissioning Indications.
 * This will be called when a GPD requests to be commissioned to ZAB.
 * The host should inspect the parameters and return:
 *  - szl_true to accept the GPD.
 *  - szl_false to reject the GPD.
 */
SZL_RESULT_t SZL_GP_GpdCommissioningNtfRegisterCB(zabService* Service, SZL_CB_GpdCommissioningNtf_t Callback)
{
  return CbHandler_Add(Service, CB_FUNC_GPD_COMMISSIONING_NTF, (CBQueue_CallbackProto)Callback);
}

/**
 * Register Handler for Green Power Commissioned Notifications.
 * This will be called once a GPD has successfully been commissioned to ZAB.
 */
SZL_RESULT_t SZL_GP_GpdCommissionedNtfRegisterCB(zabService* Service, SZL_CB_GpdCommissionedNtf_t Callback)
{
  return CbHandler_Add(Service, CB_FUNC_GPD_COMMISSIONED_NTF, (CBQueue_CallbackProto)Callback);
}

/**
 * Green Power Command Request
 *
 * This is the function used by the application to send a generic Green Power
 * command to a Green Power Device.
 * No response is supported, so delivery cannot be guaranteed by ZAB. Applications must
 * confirm success by means specific to the command sent.
 *
 * @param[in]  Service  (@ref zabService*) Server Instance Pointer
 * @param[in]  Params   (@ref SZL_GP_CmdReqParams_t*) pointer to the parameters
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_GP_CmdReq(zabService* Service, SZL_GP_CmdReqParams_t* Params)
{
  SZL_RESULT_t result;
  szl_uint8* zgpBuffer;
  szl_uint8 totalCommandLength;

  /* Validate: Parameters, Address Mode, Data Length  */
  if ( (Service == NULL) || (Params == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
  if (Params->Address.AddressMode != SZL_ADDRESS_MODE_GP_SOURCE_ID)
    {
      return SZL_RESULT_INVALID_ADDRESS_MODE;
    }

  if (Params->PayloadLength >= SZL_GP_M_MAX_COMMAND_LENGTH)
    {
      return SZL_RESULT_DATA_TOO_BIG;
    }
  else
    {
      /* Allow for Command+payload in total length */
      totalCommandLength = Params->PayloadLength + 1;
    }

  /* Allocate a buffer for combining the command and the payload.
   * This is sadly inefficient, but that's the price of a consistent UI... */
  zgpBuffer = (szl_uint8*)szl_mem_alloc(totalCommandLength,
                                        MALLOC_ID_SZL_GP_TEMP_BUFFER);
  if (zgpBuffer == NULL)
    {
      return SZL_RESULT_INTERNAL_LIB_ERROR;
    }
  zgpBuffer[0] = Params->GpCommand;
  szl_memcpy(&zgpBuffer[1], Params->Payload, Params->PayloadLength);

  result = ZclM_SendCommand(Service,
                            Params->SourceEndpoint,
                            &Params->Address,
                            M_GP_CLUSTER_ID, // cluster
                            0, //TransactionId,
                            totalCommandLength,
                            zgpBuffer);
  szl_mem_free(zgpBuffer);

  /* If the command worked, then flush out any commands for the same GPD that were
   * awaiting response, as we just replaced them in the GpTxQueue*/
  if (result == SZL_RESULT_SUCCESS)
    {
      SzlZab_GenerateSynchronousTransactionFailureBySrcId(Service,
                                                          Params->Address.Addresses.GpSourceId.SourceId,
                                                          SZL_STATUS_ZGP_TXQUEUE_OVERWRITEN);
    }

  return result;
}

/******************************************************************************
 *                      ******************************
 *                ***** INTERNAL FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/
/*******************************************************************************
 * Data in handler for GP
 ******************************************************************************/
void szlGp_DataInHandler(erStatus* Status, zabService* Service, sapMsg* Message)
{
  ER_CHECK_NULL(Status, Message);

  switch(sapMsgGetAppType(Message))
    {
      case ZAB_MSG_APP_GP_COM_IND:
        commissioningIndHandler(Service, Message);
        break;

      case ZAB_MSG_APP_GP_COM_NTF_IND:
        commissioningNtfIndHandler(Service, Message);
        break;

      case ZAB_MSG_APP_GP_TX_QUEUE_EXPIRY:
        txQueueExpiryIndHandler(Service, Message);
        break;

      default:
        printError(Service, "SZL GP: Warning - Unhandled message type 0x%02X received\n", sapMsgGetAppType(Message));
        break;
    }
}


/*******************************************************************************
 * Data in handler for GP ZCL Commands
 ******************************************************************************/
void szlGp_HandleZclMsg(erStatus* Status, zabService* Service, zabMsgProDataInd* DataInd)
{
  if ( (DataInd != NULL) && (DataInd->data != NULL) && (DataInd->dataLength >= 1) )
  {
    switch((GP_ZCL_CMD_ID)DataInd->data[0])
    {
      case GP_ZCL_CMD_ID_ATTRIB_REPORT:
        zcl_GP_ProcessReportAttr(Status, Service, DataInd, szl_false, szl_false);
        break;

      case GP_ZCL_CMD_ID_ATTRIB_REPORT_MS:
        zcl_GP_ProcessReportAttr(Status, Service, DataInd, szl_true, szl_false);
        break;

      case GP_ZCL_CMD_ID_ATTRIB_REPORT_MULTI_CLUSTER:
        zcl_GP_ProcessReportAttr(Status, Service, DataInd, szl_false, szl_true);
        break;

      case GP_ZCL_CMD_ID_ATTRIB_REPORT_MULTI_CLUSTER_MS:
        zcl_GP_ProcessReportAttr(Status, Service, DataInd, szl_true, szl_true);
        break;

      case GP_ZCL_CMD_ID_ATTRIB_READ_RESPONSE:
        SZL_GP_MultiCluster_ReadAttributesCfm(Service, DataInd);
        break;

      case GP_ZCL_CMD_ID_ATTRIB_WRITE_RESPONSE:
        SZL_GP_MultiCluster_WriteAttributesCfm(Service, DataInd);
        break;

      default:
        printError(Service, "szlGp_HandleZclMsg: WARNING - Unsupported GP Cmd Id 0x%02X\n", DataInd->data[0]);
        break;
    }
  }
}

#endif // SZL_CFG_ENABLE_GREEN_POWER
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
