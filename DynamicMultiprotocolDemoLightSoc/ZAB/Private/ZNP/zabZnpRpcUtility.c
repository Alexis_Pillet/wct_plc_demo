/*
 * zabZnpRpcUtility.c
 *
 *  Created on: 5 oct. 2012
 *      Author: SESA236777
 */
#include "zabZnpService.h"

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/* Format of an RPC error */
typedef struct
{
  unsigned8 ErrCode;
  unsigned8 reqCmd0;
  unsigned8 reqCmd1;
} zabznp_RpcError;

/* RPC error codes */
typedef enum
{
  ZNPI_CMD_ERR_INVALID_SUBSYSTEM          = 0x01,   /* Invalid Subsystem */
  ZNPI_CMD_ERR_INVALID_COMMAND_ID         = 0x02,   /* Invalid Command Id */
  ZNPI_CMD_ERR_INVALID_PARAMETER          = 0x03,   /* Invalid Parameter */
  ZNPI_CMD_ERR_INVALID_LENGTH             = 0x04,   /* Invalid Length */
} ZNPI_CMD_ERROR_CODES;


/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Get string description of an error code
 ******************************************************************************/
static char *getRPCErrCodes(ZNPI_CMD_ERROR_CODES code)
{
    switch (code) {
        case ZNPI_CMD_ERR_INVALID_SUBSYSTEM:
            return "INVALID_SUBSYSTEM";
        case ZNPI_CMD_ERR_INVALID_LENGTH:
            return "INVALID_LENGTH";
        case ZNPI_CMD_ERR_INVALID_PARAMETER:
            return "INVALID_PARAMETER";
        case ZNPI_CMD_ERR_INVALID_COMMAND_ID:
            return "INVALID_COMMAND_ID";
        default:
            return "UNKNOWN";
    }
}

/******************************************************************************
 * Process an RPC error
 ******************************************************************************/
static void zabZnp_ProcessERR(erStatus* Status, zabService* Service, sapMsg* Message)
{
  zabznp_RpcError *msgData;
  unsigned8 msgLen;


  zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
  ER_CHECK_STATUS(Status);

  if (msgLen >= sizeof(zabznp_RpcError))
    {
      printError(Service, "zabZnpRpc: ERROR: ZNP RPC Error: \n\tError Code = %s(%d)\n\tReq CMD0 = 0x%02x\n\tReq CMD1 = 0x%02x\n",
                 getRPCErrCodes((ZNPI_CMD_ERROR_CODES)msgData->ErrCode),
                 msgData->ErrCode,
                 msgData->reqCmd0,
                 msgData->reqCmd1);

      /* Notify app data service of error in sending */
      zabParseZNP_AppErrorIn(Service, sapMsgGetAppTransactionId(Message), ZAB_ERROR_VENDOR_SENDING);
    }
  else
    {
      printError(Service, "zabZnpRpc: ERROR: ZNP RPC Error: Invalid length, Cannot parse.\n");
      erStatusSet( Status, ZAB_ERROR_VENDOR_PARSE);
    }
}

/******************************************************************************
 * Process an RPC error
 ******************************************************************************/
static void processDebugMsg(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8 *msgData;
  unsigned8 msgLen;

  zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
  ER_CHECK_STATUS(Status);

  if (msgLen > 0)
  {
    printApp(Service, "zabZnpRpc: ##### Debug Msg ##### = %s\n", msgData);
  }
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Process all messages of subsystem RPC
 ******************************************************************************/
void zabZnpRpcProcessMsg(erStatus* Status, zabService* Service, sapMsg* Message)
{
    unsigned8 cmd;

    zabParseZNPGetCommandID(Status, Message, &cmd);

    switch (cmd) {
        case ZNPI_CMD_RPC_COMMAND_ERROR:
            zabZnp_ProcessERR(Status, Service, Message);
            break;

        case ZNPI_CMD_RPC_COMMAND_DEBUG_MSG:
          processDebugMsg(Status, Service, Message);
          break;

        default:
            printError(Service, "zabZnpRPC: ERROR: unknown  ZNP message 0x%02X\n", cmd);
            break;
    }
}


/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
