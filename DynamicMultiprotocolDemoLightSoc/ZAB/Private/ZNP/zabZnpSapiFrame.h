/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the types for the ZNP SAPI interface
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *                           NJ    Original
 *              19-Jul-13   MvdB   Remove unused code
 *****************************************************************************/

#ifndef ZABZNPSAPIFRAME_H_
#define ZABZNPSAPIFRAME_H_






/********************************/
/* message constants GreenPower */
/********************************/
#define ZGP_LOGICALTYPE_GP15_4        0x03



/* ZCD_NV_STARTUP_OPTION values                                    */
/*   These are bit weighted - you can OR these together.           */
/*   Setting one of these bits will set their associated NV items  */
/*   to code initialized values.                                   */
#define ZNPI_ZB_ZCD_STARTOPT_DEFAULT_CONFIG_STATE  0x01
#define ZNPI_ZB_ZCD_STARTOPT_DEFAULT_NETWORK_STATE 0x02
#define ZNPI_ZB_ZCD_STARTOPT_AUTO_START            0x04
#define ZNPI_ZB_ZCD_STARTOPT_CLEAR_CONFIG          ZNPI_ZB_ZCD_STARTOPT_DEFAULT_CONFIG_STATE
#define ZNPI_ZB_ZCD_STARTOPT_CLEAR_STATE           ZNPI_ZB_ZCD_STARTOPT_DEFAULT_NETWORK_STATE

/* ZNP logical type */
typedef enum
{
  ZAB_ZNP_SAPI_LOGICAL_TYPE_COORDINATOR  = 0x00,
  ZAB_ZNP_SAPI_LOGICAL_TYPE_ROUTER       = 0x01,
  ZAB_ZNP_SAPI_LOGICAL_TYPE_END_DEVICE   = 0x02,
} znpSapiLogicalType;

/* NV configuration parameter IDs */
typedef enum
{
  ZAB_ZNP_SAPI_NV_CFG_ID_STARTUP_OPTION         = 0x0003,  /* Size: 1byte */
  ZAB_ZNP_SAPI_NV_CFG_ID_LOGICAL_TYPE           = 0x0087,  /* Size: 1byte */
  ZAB_ZNP_SAPI_NV_CFG_ID_DIRECT_CB              = 0x008F,  /* Size: 2byte */
  ZAB_ZNP_SAPI_NV_CFG_ID_POLL_RATE              = 0x0024,  /* Size: 2byte */
  ZAB_ZNP_SAPI_NV_CFG_ID_QUEUED_POLL_RATE       = 0x0025,  /* Size: 2bytes */
  ZAB_ZNP_SAPI_NV_CFG_ID_RESPONSE_POLL_RATE     = 0x0026,  /* Size: 2byte */
  ZAB_ZNP_SAPI_NV_CFG_ID_POLL_FAILURE_RETRIES   = 0x0029,  /* Size: 1byte */
  ZAB_ZNP_SAPI_NV_CFG_ID_INDIRECT_MSG_TIMEOUT   = 0x002B,  /* Size: 1byte */
  ZAB_ZNP_SAPI_NV_CFG_ID_EXTENDED_PAN_ID        = 0x002D,  /* Size: 8byte */
  ZAB_ZNP_SAPI_NV_CFG_ID_APS_FRAME_RETRIES      = 0x0043,  /* Size: 1bytes */
  ZAB_ZNP_SAPI_NV_CFG_ID_APS_ACK_WAIT_DURAION   = 0x0044,  /* Size: 2bytes */
  ZAB_ZNP_SAPI_NV_CFG_ID_BINDING_TIME           = 0x0046,  /* Size: 2bytes */
  ZAB_ZNP_SAPI_NV_CFG_ID_APSF_WINDOW_SIZE       = 0x0049,  /* Size: 3bytes */
  ZAB_ZNP_SAPI_NV_CFG_ID_APSF_INTERFRAME_DELAY  = 0x004A,  /* Size: 2bytes */
  ZAB_ZNP_SAPI_NV_CFG_ID_USERDESC               = 0x0081,  /* Size: 17bytes */
/* Network specific configuration parameters */
  ZAB_ZNP_SAPI_NV_CFG_ID_NWKKEY                 = 0x0082,  /* Size: ?bytes */  // NEW MVDB CLONE TEST
  ZAB_ZNP_SAPI_NV_CFG_ID_PANID                  = 0x0083,  /* Size: 2bytes */
  ZAB_ZNP_SAPI_NV_CFG_ID_CHANLIST               = 0x0084,  /* Size: 4bytes */
  ZAB_ZNP_SAPI_NV_CFG_ID_PRECFGKEY              = 0x0062,  /* Size: 16bytes */
  ZAB_ZNP_SAPI_NV_CFG_ID_PRECFGKEYS_ENABLE      = 0x0063,  /* Size: 1byte */
  ZAB_ZNP_SAPI_NV_CFG_ID_SECURITY_MODE          = 0x0064,  /* Size: 1byte */
  ZAB_ZNP_SAPI_NV_CFG_ID_USE_DEFAULT_TLCK       = 0x006D,  /* Size: 1byte */
  ZAB_ZNP_SAPI_NV_CFG_ID_BCAST_RETRIES          = 0x002E,  /* Size: 1byte */
  ZAB_ZNP_SAPI_NV_CFG_ID_PASSIVE_ACK_TIMEOUT    = 0x002F,  /* Size: 1byte */
  ZAB_ZNP_SAPI_NV_CFG_ID_BCAST_DELIVERY_TIME    = 0x0030,  /* Size: 1byte */
  ZAB_ZNP_SAPI_NV_CFG_ID_ROUTE_EXPIRY_TIME      = 0x002C,  /* Size: 1byte */
} znpSapiConfigId;

/* Device Info parameter IDs*/
typedef enum
{
  ZAB_ZNP_SAPI_DEV_INFO_TYPE_DEV_STATE =                 0,   /* 1 byte */
  ZAB_ZNP_SAPI_DEV_INFO_TYPE_DEV_IEEE_ADDR =             1,   /* 8 bytes */
  ZAB_ZNP_SAPI_DEV_INFO_TYPE_DEV_SHORT_ADDR =            2,   /* 2 bytes */
  ZAB_ZNP_SAPI_DEV_INFO_TYPE_PARENT_SHORT_ADDR =         3,   /* 2 bytes */
  ZAB_ZNP_SAPI_DEV_INFO_TYPE_PARENT_IEEE_ADDR =          4,   /* 8 bytes */
  ZAB_ZNP_SAPI_DEV_INFO_TYPE_ZB_CHANNEL =                5,   /* 1 byte */
  ZAB_ZNP_SAPI_DEV_INFO_TYPE_ZB_PAN_ID =                 6,   /* 2 bytes */
  ZAB_ZNP_SAPI_DEV_INFO_TYPE_ZB_EXTENDED_PAN_ID =        7,   /* 8 bytes */
    
  /* Schneider Extension */
  ZAB_ZNP_SAPI_DEV_INFO_TYPE_KEY_SEQUENCE_NUMBER =       0x80,/* 1 byte */
  ZAB_ZNP_SAPI_DEV_INFO_TYPE_MEM_METRICS =               0x81,/* 4 bytes: szl_uint16 Usage, szl_uint16 highWater */
} znpSapiDeviceInfoType;

/* Device info data lengths */
#define ZAB_ZNP_SAPI_DEV_INFO_TYPE_DEV_STATE_LENGTH           1
#define ZAB_ZNP_SAPI_DEV_INFO_TYPE_DEV_IEEE_ADDR_LENGTH       8
#define ZAB_ZNP_SAPI_DEV_INFO_TYPE_DEV_SHORT_ADDR_LENGTH      2
#define ZAB_ZNP_SAPI_DEV_INFO_TYPE_PARENT_SHORT_ADDR_LENGTH   2
#define ZAB_ZNP_SAPI_DEV_INFO_TYPE_PARENT_IEEE_ADDR_LENGTH    8
#define ZAB_ZNP_SAPI_DEV_INFO_TYPE_ZB_CHANNEL_LENGTH          1
#define ZAB_ZNP_SAPI_DEV_INFO_TYPE_ZB_PAN_ID_LENGTH           2
#define ZAB_ZNP_SAPI_DEV_INFO_TYPE_ZB_EXTENDED_PAN_ID_LENGTH  8

#endif /* ZABZNPSAPIFRAME_H_ */
