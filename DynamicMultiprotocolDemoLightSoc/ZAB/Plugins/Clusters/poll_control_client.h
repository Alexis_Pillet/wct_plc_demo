/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the Poll Control Cluster Client.
 *   This plugin is originally designed for MiGenie.
 * 
 *   It supports:
 *    - Operation on multiple endpoints
 *    - Mandatory commands only
 *
 * USAGE:
 *   Usage of this plugin is very simple:
 *    1. Initialise it with handler for Checkin and FastPollResposne callbacks
 *    2. Plugin will call Checkin handler when any SED checks in.
 *       App can return true to have SED remain in fast poll, or false to stop.
 *    3. If app requested fast polling it can then send pending commands to the
 *       SED and then a FastPollStop once all pending data is sent.
 * 
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         04-Jul-16   MvdB   Original
 *****************************************************************************/

#ifndef _POLL_CONTROL_CLIENT_
#define _POLL_CONTROL_CLIENT_

#include "zabTypes.h"
#include "szl.h"

#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/




/**
 * Poll Control Cluster Client - Checkin Notification Callback type
 * 
 * This callabck will be called on receipt of a Checkin command from a ZigBee end device.
 * This tells the app that the end device is "awake" and messages may be sent to it.
 * 
 * App should return:
 *  - szl_true if there are messages pending for the device SourceNetworkAddress. In this case:
 *    - The time the end device will remain in fast poll mode may optionally be set at *FastPollTimeout.
 *      - 0 (Default) = The device will fast poll for the time indicated in it's FastPollTimeout attribute 
 *                      has elapsed or it receives a Fast Poll Stop command.
 *      - Other = timeout in quarter seconds.
 *    - AFTER returning true the app should send any messages pending for the device.
 *    - Once all pending messages have been sent a FastPollStop should be sent to the device
 *      to inform it it may revert to slow polling (low power) mode.
 *  - szl_false if there are no messages pending.
 */
typedef szl_bool (*SzlPlugin_PollControlClient_CheckinNotification_CB_t) (zabService* Service, szl_uint8 DesintationEndpoint, szl_uint16 SourceNetworkAddress, szl_uint16* FastPollTimeout);

/**
 * Type for FastPollStopReqParams.
 */
typedef struct
{
  SZL_EP_t SourceEndpoint;                            /**< The Endpoint for the source (App) */
  SZL_Addresses_t DestAddrMode;                       /**< Destination of the command */
} SzlPlugin_PollControlClient_FastPollStopReqParams_t;

/**
 *
 * Fast Poll Stop response.
 *
 * This is a Callback function used by SZL in order to reply to the SzlPlugin_PollControlClient_FastPollStopReq
 */
typedef void (*SzlPlugin_PollControlClient_FastPollStopResp_CB_t) (zabService* Service, SZL_STATUS_t Status, szl_uint16 SourceNetworkAddress, szl_uint8 TransactionId);


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/ 

/**
 * Poll Control Cluster Client - Initialization
 *
 * This function must be called from the application after the Library has been initialized.
 * 
 * @param[in]  Service       (zabService*)  Service Instance Pointer
 * @param[in]  NumEndpoints  (szl_uint8)    Length of EndpointList
 * @param[in]  EndpointList  (szl_uint8*)   List of endpoints the cluster should be registered for
 * @param[in]  CheckinCallback  (SzlPlugin_PollControlClient_CheckinNotification_CB_t) the callback function for received Checking commands
 * @param[in]  FastPollStopRespCallback  (SzlPlugin_PollControlClient_FastPollStopResp_CB_t) the callback function for FastPollStop responses
 */
extern 
SZL_RESULT_t SzlPlugin_PollControlClient_Init(zabService* Service, 
                                              szl_uint8 NumEndpoints, 
                                              szl_uint8* EndpointList, 
                                              SzlPlugin_PollControlClient_CheckinNotification_CB_t CheckinCallback,
                                              SzlPlugin_PollControlClient_FastPollStopResp_CB_t FastPollStopRespCallback);

/**
 * Poll Control Cluster Client - Destroy
 * 
 * @param[in]  Service       (zabService*) Service Instance Pointer
 */
extern 
SZL_RESULT_t SzlPlugin_PollControlClient_Destroy(zabService* Service);

/**
 * Poll Control Cluster Client - Fast Poll Stop command
 * 
 * @param[in]  Service       (zabService*) Service Instance Pointer
 * @param[in]  Params        (SzlPlugin_PollControlClient_FastPollStopReqParams_t*) Pointer to the parameters
 * @param[out] TransactionId (szl_uint8*) Pointer to location where Transaction ID should be returned. Set to NULL if caller does not want the Transaction ID.
 */
extern 
SZL_RESULT_t SzlPlugin_PollControlClient_FastPollStopReq(zabService* Service,
                                                         SzlPlugin_PollControlClient_FastPollStopReqParams_t* Params,
                                                         szl_uint8* TransactionId);


#ifdef __cplusplus
}
#endif
#endif /*_POLL_CONTROL_CLIENT_*/
