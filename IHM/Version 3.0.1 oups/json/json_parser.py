# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to parse ZigBeeDevice object and store them in a json file
#       This json file is used by the Web Application to display the topology of the network
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************


class JsonParser:

    def __init__(self, filename):
        self.filename = filename

    def update_device(self, device):
        pass
