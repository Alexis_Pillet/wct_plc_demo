/*
 Name:    Service Access Point Message Utility
 Author:  ZigBee Excellence Center
 Company: Schneider Electric

 Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

 Description:
   These are messages that may be used with an SAP. If a service supports SAPs
   natively then using this message utility is recommended.

 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 00.00.06.02  30-Sep-14   MvdB   artf55512: Support network key change action
 * 00.00.06.05  08-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 * 002.000.004  01-Apr-15   MvdB   ARTF56504: Support missed channel change recovery, with actions/notifications/asks/gives to get/set parameters.
 * 002.002.005  11-Sep-15   MvdB   ARTF112436: Add sapMsgPutNotifyNetworkInfoState()
 * 002.002.011  12-Oct-15   MvdB   ARTF104108: Add notification of progress for Clone actions
*/                         

#ifndef __SAP_MSG_UTILITY_H__
#define __SAP_MSG_UTILITY_H__

#include "osTypes.h"
#include "erStatusUtility.h" 
#include "sapTypes.h"
#include "zabTypes.h"
#include "zabParseProPrivate.h"
#include "zabCoreConfigure.h"

/* Enumeration: Direction In to application or Direction Out to network.
   This is a default definition of direction. A message utility may define
   more values and define other meanings to each.
*/
typedef enum 
{
  SAP_MSG_DIRECTION_NULL   =  0x00, // invalid direction
  SAP_MSG_DIRECTION_IN     =  0x01, // inbound toward application
  SAP_MSG_DIRECTION_OUT    =  0x02, // outbound to network
  SAP_MSG_DIRECTION_ALL    =  0x03  // broadcast
  // WARNING: This enum is stored in 2 bits of sapMsgStruct. If more than 4 values are present this must be expanded
} sapMsgDirection;

// Enumeration: Message General Types 
typedef enum
{
  SAP_MSG_TYPE_NULL   =  0x00, // null, invalid or unknown for allocate
  SAP_MSG_TYPE_ERROR  =  0x01, // error message
  SAP_MSG_TYPE_APP    =  0x02, // processed application specific data (or raw data)
  SAP_MSG_TYPE_ACTION =  0x03, // action/command/verb to perform
  SAP_MSG_TYPE_NOTIFY =  0x04, // notify of some state
  SAP_MSG_TYPE_COUNT          
} sapMsgType;

#ifdef __cplusplus
extern "C" {
#endif

// S E R V I C E   D E C L A R A T I O N S ///////////////////////////////////////////////////////////////////////

  // allocate prototype pointer types to register with SAP
typedef void* (*sapMsgAllocateFunc)(erStatus* Status, sapHandle Sap, sapMsgType Type, sapMsgDirection Direction, unsigned32 DataLength );
typedef void  (*sapMsgFreeFunc)( erStatus* Status, sapHandle Sap, sapMsg* Message );


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/ 


/******************************************************************************
 *                      ******************************
 *                 *****      SERVICE FUNCTIONS       *****
 *                      ******************************
 ******************************************************************************/ 

/******************************************************************************
 * Register Allocate and Free function pointers for a SAP
 ******************************************************************************/
extern void sapMsgRegAllocate(erStatus* Status, sapHandle SAP, sapMsgAllocateFunc Allocate, sapMsgFreeFunc Free);



/******************************************************************************
 *                      ******************************
 *                 *****  DATA MANAGEMENT FUNCTIONS   *****
 *                      ******************************
 ******************************************************************************/ 

/******************************************************************************
 * Allocate a new message to be sent via a SAP
 ******************************************************************************/
extern sapMsg* sapMsgAllocateData(erStatus* Status, sapHandle Sap, sapMsgType Type, sapMsgDirection Direction, unsigned32 DataLength);

/******************************************************************************
 * Allocate a new message to be sent via a SAP, with no data.
 * Typically used for actions and notifications
 ******************************************************************************/
extern sapMsg* sapMsgAllocate(erStatus* Status, sapHandle Sap, sapMsgType Type, sapMsgDirection Direction);

/******************************************************************************
 * Free a message after the user is finished with it.
 * This actually decrements a use count and only frees if use count equals zero,
 * allowing multiple tasks to use the message.
 ******************************************************************************/
extern void sapMsgFree(erStatus* Status, sapMsg* Message);

/******************************************************************************
 * Flag usage of a message.
 * This allows a task to register it's interest in the message, then free
 * it when finished with it.
 * The message will only actually be freed once all tasks have finished using it.
 ******************************************************************************/
extern void sapMsgUse(erStatus* Status, sapMsg* Message);




/******************************************************************************
 *                      ******************************
 *                 *****  DATA TRANSMISSION FUNCTIONS *****
 *                      ******************************
 ******************************************************************************/ 

/******************************************************************************
 * Put a message across a SAP
 ******************************************************************************/
extern void sapMsgPut(erStatus* Status, sapHandle Sap, sapMsg* Message);

/******************************************************************************
 * Put a message across a SAP and free it afterwards
 ******************************************************************************/
extern void sapMsgPutFree(erStatus* Status, sapHandle Sap, sapMsg* Message);




/******************************************************************************
 *                      ******************************
 *                 *****        MESAGE PARSING        *****
 *                      ******************************
 ******************************************************************************/ 

/******************************************************************************
 * Get the mutex id of a message
 ******************************************************************************/
extern unsigned8 sapMsgMutexId(sapMsg* Message);

/******************************************************************************
 * Get the Service of a message
 ******************************************************************************/
extern sapService sapMsgService(sapMsg* Message);

/******************************************************************************
 * Get/Set the direction of a message
 ******************************************************************************/
extern sapMsgDirection sapMsgGetDirection(sapMsg* Message);
extern void sapMsgSetDirection(erStatus* Status, sapMsg* Message, sapMsgDirection Direction);

/******************************************************************************
 * Get the type of a message
 ******************************************************************************/
extern sapMsgType sapMsgGetType(sapMsg* Message);
extern void sapMsgSetType(erStatus* Status, sapMsg* Message, sapMsgType Type);

/******************************************************************************
 * Get/Set the type of action
 ******************************************************************************/
extern zabAction sapMsgGetAction(sapMsg* Message);
extern void sapMsgSetAction(erStatus* Status, sapMsg* Message, zabAction Action);

/******************************************************************************
 * Get/Set the type of a notification
 ******************************************************************************/
extern zabNotificationType sapMsgGetNotifyType(sapMsg* Message);
extern void sapMsgSetNotifyType(erStatus* Status, sapMsg* Message, zabNotificationType NotificationType);

/******************************************************************************
 * Get/Set the data of a notification
 ******************************************************************************/
extern zabNotificationData sapMsgGetNotifyData(sapMsg* Message);
extern void sapMsgSetNotifyData(erStatus* Status, sapMsg* Message, zabNotificationData NotificationData);

/******************************************************************************
 * Get/Set the app type
 ******************************************************************************/
extern zabMsgAppType sapMsgGetAppType(sapMsg* Message );
extern void sapMsgSetAppType(erStatus* Status, sapMsg* Message, zabMsgAppType App);

/******************************************************************************
 * Get/Set the max app data length
 ******************************************************************************/
extern unsigned16 sapMsgGetAppDataMax(sapMsg* Message);
extern void sapMsgSetAppDataMax(erStatus* Status, sapMsg* Message, unsigned16 DataMax);

/******************************************************************************
 * Get/Set the app data length
 ******************************************************************************/
extern unsigned16 sapMsgGetAppDataLength( sapMsg* Message );
extern void sapMsgSetAppDataLength(erStatus* Status, sapMsg* Message, unsigned16 Length);

/******************************************************************************
 * Get/Set the app data
 ******************************************************************************/
extern unsigned8* sapMsgGetAppData(sapMsg* Message);
extern void sapMsgCopyAppDataTo(erStatus* Status, sapMsg* Message, unsigned8* Buffer, unsigned16  Length);

/******************************************************************************
 * Get/Set the app data transaction id
 ******************************************************************************/
extern unsigned8 sapMsgGetAppTransactionId(sapMsg* Message);
extern void sapMsgSetAppTransactionId(erStatus* Status, sapMsg* Message, unsigned8 TransactionId);



/******************************************************************************
 *                      ******************************
 *                 *****      SHORTCUT FUNCTIONS      *****
 *                      ******************************
 ******************************************************************************/ 

/******************************************************************************
 * Put an action across a SAP
 ******************************************************************************/
extern 
void sapMsgPutAction(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabAction Action);

/******************************************************************************
 * Put a NOTIFY across a SAP - Generic Function
 ******************************************************************************/
extern
void sapMsgPutNotify(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabNotificationType Type, zabNotificationData Data);

/******************************************************************************
 * Put a NOTIFY(OPEN STATE) across a SAP
 ******************************************************************************/
extern
void sapMsgPutNotifyOpenState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabOpenState OpenState);

/******************************************************************************
 * Put a NOTIFY(NETWORK STATE) across a SAP
 ******************************************************************************/
extern
void sapMsgPutNotifyNetworkState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabNwkState NwkState);

/******************************************************************************
 * Put a NOTIFY(FIRMWARE UPDATE STATE) across a SAP
 ******************************************************************************/
extern
void sapMsgPutNotifyFwUpdateState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabFwUpdateState FwUpdateState);

/******************************************************************************
 * Put a NOTIFY(FIRMWARE UPDATE PROGRESS) across a SAP
 ******************************************************************************/
extern 
void sapMsgPutNotifyFwUpdateProgress(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, unsigned8 FwUpdateProgress);

/******************************************************************************
 * Put a NOTIFY(CLONE STATE) across a SAP
 ******************************************************************************/
extern 
void sapMsgPutNotifyCloneState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabCloneState CloneState);

/******************************************************************************
 * Put a NOTIFY(CLONE PROGRESS) across a SAP
 ******************************************************************************/
extern 
void sapMsgPutNotifyCloneProgress(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, unsigned8 Progress);

/******************************************************************************
 * Put a NOTIFY(ERROR) across a SAP
 ******************************************************************************/
extern 
void sapMsgPutNotifyError(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabError ErrorNumber);

/******************************************************************************
 * Put a NOTIFY(NETWORK PERMITJOIN STATE) across a SAP
 ******************************************************************************/
extern
void sapMsgPutNotifyNetworkPermitJoinState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, unsigned16 nwkPermitJoinTime);

/******************************************************************************
 * Put a NOTIFY(CHANNEL CHANGE STATE) across a SAP
 ******************************************************************************/
extern 
void sapMsgPutNotifyChannelChangeState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabChannelChangeState channelChangeState);

/******************************************************************************
 * Put a NOTIFY(NETWOKR KEY CHANGE STATE) across a SAP
 ******************************************************************************/
extern
void sapMsgPutNotifyNetworkKeyChangeState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabNetworkKeyChangeState nwkKeyChangeState);

/******************************************************************************
 * Put a NOTIFY(NETWORK MAINTENANCE PARAM STATE) across a SAP
 ******************************************************************************/
extern
void sapMsgPutNotifyNetworkMaintParamState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabNetworkMaintenanceParamState networkMaintenanceParamState);

/******************************************************************************
 * Put a NOTIFY(NETWORK INFO STATE) across a SAP
 ******************************************************************************/
extern 
void sapMsgPutNotifyNetworkInfoState(erStatus* Status, sapHandle Sap, sapMsgDirection Direction, zabNetworkInfoState networkInfoState);

#ifdef __cplusplus
}
#endif
#endif
