/*
  Name:    TI's ZNP Frame Format macros 
  Author:  ZigBee Excellence Center
  Company: Schneider Electric
 
  Copyright (c) 2011-2012 by Schneider Electric, all rights reserved
 
  Warning:
    Private for internal module use. Do not include or use in implementation.
 
  Reference Document(s): 
    ZEC0007 ZigBee Application Brick Design

  Description:
    These macros are required for each vendor implementation. Each implementation API or serial
    protocol has a specific set of frame formats nd sizes used to pass messages to the application, 
    in this case, our module. These macros abstract different frame formats.
    
    Each message passed is of sapMsg that is defined in  and works on the 
    raw vendor buffer part of the message. Parts of the buffered portion may be copied to the 
    visible part of the message as well.
 * 
 * 002.002.021  21-Apr-16   MvdB   ARTF167734: Move masks for command type and subsystem
*/

#ifndef __ZAB_ZNP_FRAME_H__
#define __ZAB_ZNP_FRAME_H__

/* ZNP Command Type - Cmd0 bits 7-5 */
typedef enum
{
  ZNPI_TYPE_POLL = 0x00,
  ZNPI_TYPE_SREQ = 0x20,
  ZNPI_TYPE_AREQ = 0x40,
  ZNPI_TYPE_SRSP = 0x60,
} ZNPI_TYPE;
/* The 3 MSB's of the 1st command field byte are for command type. */
#define ZNPI_MSG_CMD_0_TYPE_MASK        ((unsigned8)0xE0)

/* ZNP Sub Systems - Cmd0 bits 4-0 */
typedef enum
{
  ZNPI_SUBSYSTEM_RPC  = 0x00,
  ZNPI_SUBSYSTEM_SYS  = 0x01,
  ZNPI_SUBSYSTEM_MAC  = 0x02,
  ZNPI_SUBSYSTEM_NWK  = 0x03,
  ZNPI_SUBSYSTEM_AF   = 0x04,
  ZNPI_SUBSYSTEM_ZDO  = 0x05,
  ZNPI_SUBSYSTEM_SAPI = 0x06,
  ZNPI_SUBSYSTEM_UTIL = 0x07,
  ZNPI_SUBSYSTEM_DBG  = 0x08,
  ZNPI_SUBSYSTEM_APP  = 0x09,
  ZNPI_SUBSYSTEM_OTA  = 0x0A,
  ZNPI_SUBSYSTEM_ZNP  = 0x0B,
  ZNPI_SUBSYSTEM_SBL  = 0x0D,
  ZNPI_SUBSYSTEM_GP   = 0x15,
  ZNPI_SUBSYSTEM_RFT  = 0x1F, // was 0x15, but this is now taken by TI GP
} ZNPI_SUBSYSTEM;
/* The 5 LSB's of the 1st command field byte are for command subsystem. */
#define ZNPI_MSG_CMD_0_SUBSYSTEM_MASK   ((unsigned8)0x1F)

/* ZNP Error Codes  */
typedef enum 
{
  ZNPI_API_ERR_SUCCESS                    = 0x00,   /* Success */
  ZNPI_API_ERR_FAILURE                    = 0x01,   /* Failure */
  ZNPI_API_ERR_INVALID_PARAMETER          = 0x02,   /* Invalid Parameter */

  ZNPI_API_ERR_MEM_ERROR                  = 0x10,   /* Mem Error */
  ZNPI_API_ERR_BUFFER_FULL                = 0x11,   /* Buffer Full */
  ZNPI_API_ERR_UNSUPPORTED_MODE           = 0x12,   /* Unsupported Mode */
  ZNPI_API_ERR_MAC_MEM_ERROR              = 0x13,   /* Mac Mem Error */
  ZNPI_API_ERR_MAC_NO_RESOURCE            = 0x1A,   /* Mac No Resource */

  ZNPI_API_ERR_SAPI_IN_PROGRESS           = 0x20,   /* Sapi In Progress */
  ZNPI_API_ERR_SAPI_TIMEOUT               = 0x21,   /* Sapi Timeout */    
  ZNPI_API_ERR_SAPI_INIT                  = 0x22,   /* Sapi Init */       

  ZNPI_API_ERR_DO_INVALID_REQUEST_TYPE    = 0x80,   /* Invalid Request Type */
  ZNPI_API_ERR_DO_INVALID_ENDPOINT        = 0x82,   /* Invalid Endpoint */
  ZNPI_API_ERR_DO_UNSUPPORTED             = 0x84,   /* Unsupported */
  ZNPI_API_ERR_DO_TIMEOUT                 = 0x85,   /* Timeout */
  ZNPI_API_ERR_DO_NO_MATCH                = 0x86,   /* No Match */
  ZNPI_API_ERR_DO_TABLE_FULL              = 0x87,   /* Table Full */
  ZNPI_API_ERR_DO_NO_BIND_ENTRY           = 0x88,   /* No Bind Entry */

  ZNPI_API_ERR_SEC_NO_KEY                 = 0xa1,   /* Sec No Key */
  ZNPI_API_ERR_SEC_OLD_FRM_COUNT          = 0xa2,   /* SEC Old Frm Count */
  ZNPI_API_ERR_SEC_MAX_FRM_COUNT          = 0xa3,   /* Sec Max Frm Count */
  ZNPI_API_ERR_SEC_CCM_FAIL               = 0xa4,   /* SEC Ccm Fail */

  ZNPI_API_ERR_APS_FAIL                   = 0xb1,   /* Aps Fail */
  ZNPI_API_ERR_APS_TABLE_FULL             = 0xb2,   /* Aps Table Full */
  ZNPI_API_ERR_APS_ILLEGAL_REQUEST        = 0xb3,   /* Aps Illegal Request */
  ZNPI_API_ERR_APS_INVALID_BINDING        = 0xb4,   /* Aps Invalid Binding */
  ZNPI_API_ERR_APS_UNSUPPORTED_ATTRIB     = 0xb5,   /* Aps Unsupported Attrib */
  ZNPI_API_ERR_APS_NOT_SUPPORTED          = 0xb6,   /* Aps Not Supported */
  ZNPI_API_ERR_APS_NO_ACK                 = 0xb7,   /* Aps No Ack */
  ZNPI_API_ERR_APS_DUPLICATE_ENTRY        = 0xb8,   /* Aps Duplicate Entry */
  ZNPI_API_ERR_APS_NO_BOUND_DEVICE        = 0xb9,   /* Aps NoBound Device */
  ZNPI_API_ERR_APS_NOT_ALLOWED            = 0xba,   /* Aps Not Allowed */      
  ZNPI_API_ERR_APS_NOT_AUTHENTICATED      = 0xbb,   /* Aps Not Authenticated */ 

  ZNPI_API_ERR_NWK_INVALID_PARAM          = 0xc1,   /* Nwk Invalid Param */
  ZNPI_API_ERR_NWK_INVALID_REQUEST        = 0xc2,   /* Nwk Invalid Request */
  ZNPI_API_ERR_NWK_NOT_PERMITTED          = 0xc3,   /* Nwk Not Permitted */
  ZNPI_API_ERR_NWK_STARTUP_FAILURE        = 0xc4,   /* Nwk Startup Failure */
  ZNPI_API_ERR_NWK_TABLE_FULL             = 0xc7,   /* Nwk Table Full */
  ZNPI_API_ERR_NWK_UNKNOWN_DEVICE         = 0xc8,   /* Nwk Unknown Device */
  ZNPI_API_ERR_NWK_UNSUPPORTED_ATTRIBUTE  = 0xc9,   /* Nwk Unsupported Attribute */
  ZNPI_API_ERR_NWK_NO_NETWORKS            = 0xca,   /* Nwk No Networks */
  ZNPI_API_ERR_NWK_LEAVE_UNCONFIRMED      = 0xcb,   /* Nwk Leave Unconfirmed */
  ZNPI_API_ERR_NWK_NO_ACK                 = 0xcc,   /* Nwk No Ack */
  ZNPI_API_ERR_NWK_NO_ROUTE               = 0xcd,   /* Nwk No Route */

  ZNPI_API_ERR_MAC_BEACON_LOSS            = 0xe0,   /* Mac Beacon Loss */            
  ZNPI_API_ERR_MAC_CHANNEL_ACCESS_FAILURE = 0xe1,   /* Mac Channel Access Failure */ 
  ZNPI_API_ERR_MAC_DENIED                 = 0xe2,   /* Mac Denied */                 
  ZNPI_API_ERR_MAC_DISABLE_TRX_FAILURE    = 0xe3,   /* Mac Disable Trx Failure */    
  ZNPI_API_ERR_MAC_FAILED_SECURITY_CHECK  = 0xe4,   /* Mac Failed Security Check */  
  ZNPI_API_ERR_MAC_FRAME_TOO_LONG         = 0xe5,   /* Mac Frame Too Long */         
  ZNPI_API_ERR_MAC_INVALID_GTS            = 0xe6,   /* Mac Invalid Gts */            
  ZNPI_API_ERR_MAC_INVALID_HANDLE         = 0xe7,   /* Mac Invalid Handle */         
  ZNPI_API_ERR_MAC_INVALID_PARAMETER      = 0xe8,   /* Mac Invalid Parameter */      
  ZNPI_API_ERR_MAC_NO_ACK                 = 0xe9,   /* Mac No Ack */                 
  ZNPI_API_ERR_MAC_NO_BEACON              = 0xea,   /* Mac No Beacon */              
  ZNPI_API_ERR_MAC_NO_DATA                = 0xeb,   /* Mac No Data */                
  ZNPI_API_ERR_MAC_NO_SHORT_ADDR          = 0xec,   /* Mac No Short Addr */          
  ZNPI_API_ERR_MAC_OUT_OF_CAP             = 0xed,   /* Mac Out Of Cap */             
  ZNPI_API_ERR_MAC_PAN_ID_CONFLICT        = 0xee,   /* Mac Pan Id Conflict */        
  ZNPI_API_ERR_MAC_REALIGNMENT            = 0xef,   /* Mac Realignment */            
                                                                                 
  ZNPI_API_ERR_MAC_TRANSACTION_EXPIRED    = 0xf0,   /* Mac Transaction Expired */    
  ZNPI_API_ERR_MAC_TRANSACTION_OVERFLOW   = 0xf1,   /* Mac Transaction Overflow */   
  ZNPI_API_ERR_MAC_TX_ACTIVE              = 0xf2,   /* Mac Tx Active */              
  ZNPI_API_ERR_MAC_UNAVAILABLE_KEY        = 0xf3,   /* Mac Unavailable Key */        
  ZNPI_API_ERR_MAC_UNSUPPORTED_ATTRIBUTE  = 0xf4,   /* Mac Unsupported Attribute */  
  ZNPI_API_ERR_MAC_UNSUPPORTED            = 0xf5,   /* Mac Unsupported */    
  ZNPI_API_ERR_MAC_SCAN_IN_PROGRESS       = 0xfc,   /* Mac Scan In Progress */            
  ZNPI_API_ERR_MAC_SRC_MATCH_INVALID_INDEX= 0xff,   /* Mac Src Match Invalid Index */
} ZNPI_API_ERROR_CODES;

/* Extension mask and value used for convert GP Source Id into IEEE address for ZNP */
#define ZNP_M_GP_SRC_ID_IEEE_EXTENSION_MASK   0xFFFFFFFF00000000
#define ZNP_M_GP_SRC_ID_IEEE_EXTENSION        0x0200000000000000

#endif
