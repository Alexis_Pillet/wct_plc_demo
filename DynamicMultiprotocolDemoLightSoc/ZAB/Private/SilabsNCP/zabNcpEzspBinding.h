/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Binding Commands/Responses
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 002.002.039  17-Jan-17   MvdB   ARTF175875: Original
 *****************************************************************************/


#ifndef __ZAB_NCP_EZSP_BINDING_H__
#define __ZAB_NCP_EZSP_BINDING_H__

#include "ember-types.h"
#include "ezsp-enum.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 * Clear Binding Request + Response Handler
 ******************************************************************************/
void zabNcpEzspBinding_ClearAll(erStatus* Status, zabService* Service);
void zabNcpEzspBinding_ClearAllResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Get Network Parameters Request + Response Handler
 ******************************************************************************/
void zabNcpEzspBinding_Get(erStatus* Status, zabService* Service, unsigned8 Index);
void zabNcpEzspBinding_GetResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Remote Set Binding Table Handler
 ******************************************************************************/
void zabNcpEzspBinding_RemoteSetBindingHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Remote Delete Binding Table Handler
 ******************************************************************************/
void zabNcpEzspBinding_RemoteDeleteBindingHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);


#ifdef __cplusplus
}
#endif

#endif // __ZAB_NCP_EZSP_BINDING_H__
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/