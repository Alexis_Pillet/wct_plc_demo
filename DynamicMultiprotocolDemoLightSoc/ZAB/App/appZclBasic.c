/******************************************************************************
 *                          ZAB TEST APPLICATION
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the Basic Cluster ZCL handlers for the test app.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 01.100.07.00 13-Feb-15   MvdB   Split out from appZcl, which was getting too big
 *                                 artf108759: Szl_DataPointReadNative (and similar) should include maxDataLength
 * 002.002.047  31-Jan-17   MvdB   Add appZcl_SzlBasicPlugin_SetProductIdentifier()
 * 002.002.049  14-Feb-17   MvdB   Ensure *PayloadOutSize is set correctly by cluster command handlers
 *****************************************************************************/

#include "appConfig.h"
#include "appZcl.h"
#include "appZclBasic.h"
#include "appGp.h"
//#include "appSerialUtility.h"
#include "appMain.h"

#include "zabCoreService.h"

#include "basic_cluster.h"

//#include "platUtility.h"




/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/



/******************************************************************************
 * Nova Online/Offline Notification Handler
 ******************************************************************************/
szl_bool appZclBasic_NovaOnlineNtfHandler(zabService* Service,
                                          SZL_EP_t DestinationEndpoint,
                                          szl_bool ManufacturerSpecific,
                                          szl_uint16 ClusterID,
                                          szl_uint8 CmdID,
                                          szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize,
                                          szl_uint8* CmdOutID,
                                          szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize,
                                          szl_uint8 TransactionId,
                                          SZL_Addresses_t SourceAddress,
                                          SZL_ADDRESS_MODE_t DestAddressMode)
{
  unsigned8 i;
  char addrString[APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH];

  printApp(Service, "appZclBasic_NovaOnlineNtfHandler: TID = 0x%02X, SrcAddr = %s, DstEP = 0x%02X, DstAddrMode = %s, MS = 0x%02X, Cluster = 0x%04X, Cmd = 0x%02X, DataIn = ",
           TransactionId,
           appZcl_GetSzlAddressString(&SourceAddress, addrString, sizeof(addrString)),
           DestinationEndpoint,
           appZcl_GetSzlAddressModeString(DestAddressMode),
           ManufacturerSpecific,
           ClusterID,
           CmdID);

  for (i = 0; i < PayloadInSize; i++)
    {
      printApp(Service, "%02X ", ZCLPayloadIn[i]);
    }
  printApp(Service, "\n");

  *PayloadOutSize = 0;
  return szl_true;
}



#ifdef BASIC_M_USE_DIRECT_ACCESS
/******************************************************************************
 * Basic Cluster - Increment Values for an endpoint
 ******************************************************************************/
SZL_RESULT_t appZcl_SzlBasicPlugin_IncValues(szl_uint8 Endpoint)
{
  SzlPlugin_Basic_Attributes_t* basic;

  basic = SzlPlugin_BasicCluster_GetAttributePointer(appService, Endpoint);

  if (basic)
    {
      basic->ZclVersion = 1;
      strncpy(basic->ManufacturerName, "Schneider Electric", BASIC_M_MANUFACTURER_NAME_LENGTH); basic->ManufacturerName[BASIC_M_MANUFACTURER_NAME_LENGTH] = '\0';
      strncpy(basic->ModelIdentifier, "ZAB Test App", BASIC_M_MODEL_IDENTIFIER_LENGTH); basic->ModelIdentifier[BASIC_M_MODEL_IDENTIFIER_LENGTH] = '\0';
      strncpy(basic->DateCode, "19820617", BASIC_M_DATE_CODE_LENGTH); basic->DateCode[BASIC_M_DATE_CODE_LENGTH] = '\0';
      basic->PowerSource = 0x02;
      strncpy(basic->DeviceName, "ZAB Test App", BASIC_M_DEVICE_NAME_LENGTH); basic->DeviceName[BASIC_M_DEVICE_NAME_LENGTH] = '\0';
      strncpy(basic->NetworkProcessorFirmwareVersion, "NetProcFwVer", BASIC_M_NETWORK_PROCESSOR_FIRMWARE_VERSION_LENGTH); basic->NetworkProcessorFirmwareVersion[BASIC_M_NETWORK_PROCESSOR_FIRMWARE_VERSION_LENGTH] = '\0';
      strncpy(basic->ApplicationFirmwareVersion, "AppFwVer", BASIC_M_APPLICATION_FIRMWARE_VERSION_LENGTH); basic->ApplicationFirmwareVersion[BASIC_M_APPLICATION_FIRMWARE_VERSION_LENGTH] = '\0';
      strncpy(basic->ApplicationHardwareVersion, "AppHwVer", BASIC_M_APPLICATION_HARDWARE_VERSION_LENGTH); basic->ApplicationHardwareVersion[BASIC_M_APPLICATION_HARDWARE_VERSION_LENGTH] = '\0';
      strncpy(basic->ProductSerialNumber, "SerialNumber", BASIC_M_PRODUCT_SERIAL_NUMBER_LENGTH); basic->ProductSerialNumber[BASIC_M_PRODUCT_SERIAL_NUMBER_LENGTH] = '\0';
      strncpy(basic->NetworkProcessorHardwareVersion, "NetProcHwVer", BASIC_M_NETWORK_PROCESSOR_HARDWARE_VERSION_LENGTH); basic->NetworkProcessorHardwareVersion[BASIC_M_NETWORK_PROCESSOR_HARDWARE_VERSION_LENGTH] = '\0';
      basic->ProductIdentifier = 0x02AB;
      strncpy(basic->ProductRange, "ProductRange", BASIC_M_PRODUCT_RANGE_LENGTH); basic->ProductRange[BASIC_M_PRODUCT_RANGE_LENGTH] = '\0';
      strncpy(basic->ProductModel, "ProductModel", BASIC_M_PRODUCT_MODEL_LENGTH); basic->ProductModel[BASIC_M_PRODUCT_MODEL_LENGTH] = '\0';
      strncpy(basic->ProductFamily, "ProductFamily", BASIC_M_PRODUCT_FAMILY_LENGTH); basic->ProductFamily[BASIC_M_PRODUCT_FAMILY_LENGTH] = '\0';
      strncpy(basic->VendorUrl, "www.google.com", BASIC_M_VENDOR_URL_LENGTH); basic->VendorUrl[BASIC_M_VENDOR_URL_LENGTH] = '\0';
      strncpy(basic->ProductCapability1, "ProductCapability1", BASIC_M_PRODUCT_CAPABILITY_1_LENGTH); basic->ProductCapability1[BASIC_M_PRODUCT_CAPABILITY_1_LENGTH] = '\0';

      return SZL_RESULT_SUCCESS;
    }
  return SZL_RESULT_FAILED;
}

/******************************************************************************
 * Basic Cluster - Set product identifier for an endpoint
 ******************************************************************************/
SZL_RESULT_t appZcl_SzlBasicPlugin_SetProductIdentifier(szl_uint8 Endpoint, szl_uint16 ProductIdentifier)
{
  SzlPlugin_Basic_Attributes_t* basic;

  basic = SzlPlugin_BasicCluster_GetAttributePointer(appService, Endpoint);

  if (basic)
    {
      basic->ProductIdentifier = ProductIdentifier;

      return SZL_RESULT_SUCCESS;
    }
  return SZL_RESULT_FAILED;
}

#else
#define APP_ZCL_M_MAX_BASIC_CLUSTER_ENDPOINTS 2
static szl_uint8 BasicClusterEndpoints[APP_ZCL_M_MAX_BASIC_CLUSTER_ENDPOINTS];
static SzlPlugin_Basic_Attributes_t BasicClusterAttributes[APP_ZCL_M_MAX_BASIC_CLUSTER_ENDPOINTS];


/******************************************************************************
 * Basic Cluster - Initialise endpoints in the application
 ******************************************************************************/
SZL_RESULT_t appZcl_SzlBasicPlugin_InitEndpoints(szl_uint8 NumEndpoints, szl_uint8* EndpointList)
{
  if (NumEndpoints > APP_ZCL_M_MAX_BASIC_CLUSTER_ENDPOINTS)
    {
      return SZL_RESULT_INVALID_DATA;
    }

  memcpy(BasicClusterEndpoints, EndpointList, NumEndpoints);

  for (szl_uint8 i = 0; i < NumEndpoints; i++)
    {
      appZcl_SzlBasicPlugin_IncValues(EndpointList[i]);
    }
  return SZL_RESULT_SUCCESS;
}


/******************************************************************************
 * Basic Cluster - Get the index of the endpoint in the storage arrays
 ******************************************************************************/
static szl_uint8 getBasicClusterEndpointIndex(szl_uint8 Endpoint)
{
  szl_uint8 i;

  for (i = 0; i < APP_ZCL_M_MAX_BASIC_CLUSTER_ENDPOINTS; i++)
    {
      if (BasicClusterEndpoints[i] == Endpoint)
        {
          return i;
        }
    }
  return 0xFF;
}

/******************************************************************************
 * Basic Cluster Read Attribute Handler
 ******************************************************************************/
SZL_RESULT_t appZcl_SzlBasicPlugin_ReadAttrHandler(zabService* Service,
                                                      SZL_EP_t Endpoint,
                                                      szl_bool ManufacturerSpecific, szl_uint16 ClusterID,
                                                      szl_uint16 AttributeID,
                                                      void* Data, szl_uint8 *DataLength)
{
  szl_uint8 index;
  szl_uint8 maxDataLength;
  SZL_RESULT_t res = SZL_RESULT_FAILED;
  printApp(Service, "App Basic: Read Attribute Handler: Ep 0x%02X, AttributeID = 0x%04X\n",
                            Endpoint,
                            AttributeID);

  index = getBasicClusterEndpointIndex(Endpoint);
  if (index >= APP_ZCL_M_MAX_BASIC_CLUSTER_ENDPOINTS)
    {
      // error
      return res;
    }

  maxDataLength = *DataLength;
  if (maxDataLength == 0)
    {
      maxDataLength = 0xFF;
    }

  res = SZL_RESULT_DATA_TOO_BIG;
  switch(AttributeID)
    {

      case ATTRID_BASIC_ZCL_VERSION:
        if (sizeof(szl_uint8) <= maxDataLength)
          {
            *((szl_uint8*)Data) = BasicClusterAttributes[index].ZclVersion;
            *DataLength = sizeof(szl_uint8);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      case ATTRID_BASIC_MANUFACTURER_NAME:
        if (strlen(BasicClusterAttributes[index].ManufacturerName) <= maxDataLength)
          {
            strncpy((char*)Data, BasicClusterAttributes[index].ManufacturerName, maxDataLength);
            *DataLength = strlen(BasicClusterAttributes[index].ManufacturerName);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      case ATTRID_BASIC_MODEL_IDENTIFIER:
        if (strlen(BasicClusterAttributes[index].ModelIdentifier) <= maxDataLength)
          {
            strncpy((char*)Data, BasicClusterAttributes[index].ModelIdentifier, maxDataLength);
            *DataLength = strlen(BasicClusterAttributes[index].ModelIdentifier);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      case ATTRID_BASIC_DATE_CODE:
        if (strlen(BasicClusterAttributes[index].DateCode) <= maxDataLength)
          {
            strncpy((char*)Data, BasicClusterAttributes[index].DateCode, maxDataLength);
            *DataLength = strlen(BasicClusterAttributes[index].DateCode);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      case ATTRID_BASIC_POWER_SOURCE:
        if (sizeof(szl_uint8) <= maxDataLength)
          {
            *((szl_uint8*)Data) = BasicClusterAttributes[index].PowerSource;
            *DataLength = sizeof(szl_uint8);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      /* =S= MS Attributes */

      case ATTRID_BASIC_NETWORK_PROCESSOR_FIRMWARE_VERSION:
        if (strlen(BasicClusterAttributes[index].NetworkProcessorFirmwareVersion) <= maxDataLength)
          {
            strncpy((char*)Data, BasicClusterAttributes[index].NetworkProcessorFirmwareVersion, maxDataLength);
            *DataLength = strlen(BasicClusterAttributes[index].NetworkProcessorFirmwareVersion);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      case ATTRID_BASIC_APPLICATION_FIRMWARE_VERSION:
        if (strlen(BasicClusterAttributes[index].ApplicationFirmwareVersion) <= maxDataLength)
          {
            strncpy((char*)Data, BasicClusterAttributes[index].ApplicationFirmwareVersion, maxDataLength);
            *DataLength = strlen(BasicClusterAttributes[index].ApplicationFirmwareVersion);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      case ATTRID_BASIC_APPLICATION_HARDWARE_VERSION:
        if (strlen(BasicClusterAttributes[index].ApplicationHardwareVersion) <= maxDataLength)
          {
            strncpy((char*)Data, BasicClusterAttributes[index].ApplicationHardwareVersion, maxDataLength);
            *DataLength = strlen(BasicClusterAttributes[index].ApplicationHardwareVersion);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      case ATTRID_BASIC_PRODUCT_SERIAL_NUMBER:
        if (strlen(BasicClusterAttributes[index].ProductSerialNumber) <= maxDataLength)
          {
            strncpy((char*)Data, BasicClusterAttributes[index].ProductSerialNumber, maxDataLength);
            *DataLength = strlen(BasicClusterAttributes[index].ProductSerialNumber);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      case ATTRID_BASIC_NETWORK_PROCESSOR_HARDWARE_VERSION:
        if (strlen(BasicClusterAttributes[index].NetworkProcessorHardwareVersion) <= maxDataLength)
          {
            strncpy((char*)Data, BasicClusterAttributes[index].NetworkProcessorHardwareVersion, maxDataLength);
            *DataLength = strlen(BasicClusterAttributes[index].NetworkProcessorHardwareVersion);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      case ATTRID_BASIC_PRODUCT_IDENTIFIER:
        if (sizeof(szl_uint16) <= maxDataLength)
          {
            *((szl_uint16*)Data) = BasicClusterAttributes[index].ProductIdentifier;
            *DataLength = sizeof(szl_uint16);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      case ATTRID_BASIC_PRODUCT_RANGE:
        if (strlen(BasicClusterAttributes[index].ProductRange) <= maxDataLength)
          {
            strncpy((char*)Data, BasicClusterAttributes[index].ProductRange, maxDataLength);
            *DataLength = strlen(BasicClusterAttributes[index].ProductRange);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      case ATTRID_BASIC_PRODUCT_MODEL:
        if (strlen(BasicClusterAttributes[index].ProductModel) <= maxDataLength)
          {
            strncpy((char*)Data, BasicClusterAttributes[index].ProductModel, maxDataLength);
            *DataLength = strlen(BasicClusterAttributes[index].ProductModel);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      case ATTRID_BASIC_PRODUCT_FAMILY:
        if (strlen(BasicClusterAttributes[index].ProductFamily) <= maxDataLength)
          {
            strncpy((char*)Data, BasicClusterAttributes[index].ProductFamily, maxDataLength);
            *DataLength = strlen(BasicClusterAttributes[index].ProductFamily);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      case ATTRID_BASIC_VENDOR_URL:
        if (strlen(BasicClusterAttributes[index].VendorUrl) <= maxDataLength)
          {
            strncpy((char*)Data, BasicClusterAttributes[index].VendorUrl, maxDataLength);
            *DataLength = strlen(BasicClusterAttributes[index].VendorUrl);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      case ATTRID_BASIC_PRODUCT_CAPABILITY_1:
        if (strlen(BasicClusterAttributes[index].ProductCapability1) <= maxDataLength)
          {
            strncpy((char*)Data, BasicClusterAttributes[index].ProductCapability1, maxDataLength);
            *DataLength = strlen(BasicClusterAttributes[index].ProductCapability1);
            res = SZL_RESULT_SUCCESS;
          }
        break;

      default:
        res = SZL_RESULT_FAILED;
    }
  return res;
}


/******************************************************************************
 * Basic Cluster Write Attribute Handler
 ******************************************************************************/
SZL_RESULT_t appZcl_SzlBasicPlugin_WriteAttrHandler(zabService* Service,
                                                       SZL_EP_t Endpoint,
                                                       szl_bool ManufacturerSpecific, szl_uint16 ClusterID,
                                                       szl_uint16 AttributeID,
                                                       void* Data, szl_uint8 DataLength)
{
  szl_uint8 index;
  SZL_RESULT_t res = SZL_RESULT_FAILED;
  printApp(Service, "App Basic: Write Attribute Handler: Ep 0x%02X, AttributeID = 0x%04X, DataLength = %d\n",
                            Endpoint,
                            AttributeID,
                            DataLength);

  index = getBasicClusterEndpointIndex(Endpoint);
  if (index >= APP_ZCL_M_MAX_BASIC_CLUSTER_ENDPOINTS)
    {
      return res;
    }

  // Only these attributes are writeable

  return res;
}

/******************************************************************************
 * Basic Cluster - Increment Values for an endpoint
 ******************************************************************************/
SZL_RESULT_t appZcl_SzlBasicPlugin_IncValues(szl_uint8 Endpoint)
{
  szl_uint8 index;

  index = getBasicClusterEndpointIndex(Endpoint);

  if (index < APP_ZCL_M_MAX_BASIC_CLUSTER_ENDPOINTS)
    {
      BasicClusterAttributes[index].ZclVersion = 1;
      strncpy(BasicClusterAttributes[index].ManufacturerName, "Schneider Electric", BASIC_M_MANUFACTURER_NAME_LENGTH); BasicClusterAttributes[index].ManufacturerName[BASIC_M_MANUFACTURER_NAME_LENGTH] = '\0';
      strncpy(BasicClusterAttributes[index].ModelIdentifier, "ZAB Test App", BASIC_M_MODEL_IDENTIFIER_LENGTH); BasicClusterAttributes[index].ModelIdentifier[BASIC_M_MODEL_IDENTIFIER_LENGTH] = '\0';
      strncpy(BasicClusterAttributes[index].DateCode, "19820617", BASIC_M_DATE_CODE_LENGTH); BasicClusterAttributes[index].DateCode[BASIC_M_DATE_CODE_LENGTH] = '\0';
      BasicClusterAttributes[index].PowerSource = 0x02;
      strncpy(BasicClusterAttributes[index].DeviceName, "ZAB Test App", BASIC_M_DEVICE_NAME_LENGTH); BasicClusterAttributes[index].DeviceName[BASIC_M_DEVICE_NAME_LENGTH] = '\0';
      strncpy(BasicClusterAttributes[index].NetworkProcessorFirmwareVersion, "NetProcFwVer", BASIC_M_NETWORK_PROCESSOR_FIRMWARE_VERSION_LENGTH); BasicClusterAttributes[index].NetworkProcessorFirmwareVersion[BASIC_M_NETWORK_PROCESSOR_FIRMWARE_VERSION_LENGTH] = '\0';
      strncpy(BasicClusterAttributes[index].ApplicationFirmwareVersion, "AppFwVer", BASIC_M_APPLICATION_FIRMWARE_VERSION_LENGTH); BasicClusterAttributes[index].ApplicationFirmwareVersion[BASIC_M_APPLICATION_FIRMWARE_VERSION_LENGTH] = '\0';
      strncpy(BasicClusterAttributes[index].ApplicationHardwareVersion, "AppHwVer", BASIC_M_APPLICATION_HARDWARE_VERSION_LENGTH); BasicClusterAttributes[index].ApplicationHardwareVersion[BASIC_M_APPLICATION_HARDWARE_VERSION_LENGTH] = '\0';
      strncpy(BasicClusterAttributes[index].ProductSerialNumber, "SerialNumber", BASIC_M_PRODUCT_SERIAL_NUMBER_LENGTH); BasicClusterAttributes[index].ProductSerialNumber[BASIC_M_PRODUCT_SERIAL_NUMBER_LENGTH] = '\0';
      strncpy(BasicClusterAttributes[index].NetworkProcessorHardwareVersion, "NetProcHwVer", BASIC_M_NETWORK_PROCESSOR_HARDWARE_VERSION_LENGTH); BasicClusterAttributes[index].NetworkProcessorHardwareVersion[BASIC_M_NETWORK_PROCESSOR_HARDWARE_VERSION_LENGTH] = '\0';
      BasicClusterAttributes[index].ProductIdentifier = 0x02AB;
      strncpy(BasicClusterAttributes[index].ProductRange, "ProductRange", BASIC_M_PRODUCT_RANGE_LENGTH); BasicClusterAttributes[index].ProductRange[BASIC_M_PRODUCT_RANGE_LENGTH] = '\0';
      strncpy(BasicClusterAttributes[index].ProductModel, "ProductModel", BASIC_M_PRODUCT_MODEL_LENGTH); BasicClusterAttributes[index].ProductModel[BASIC_M_PRODUCT_MODEL_LENGTH] = '\0';
      strncpy(BasicClusterAttributes[index].ProductFamily, "ProductFamily", BASIC_M_PRODUCT_FAMILY_LENGTH); BasicClusterAttributes[index].ProductFamily[BASIC_M_PRODUCT_FAMILY_LENGTH] = '\0';
      strncpy(BasicClusterAttributes[index].VendorUrl, "www.vendorUrl.com", BASIC_M_VENDOR_URL_LENGTH); BasicClusterAttributes[index].VendorUrl[BASIC_M_VENDOR_URL_LENGTH] = '\0';
      strncpy(BasicClusterAttributes[index].ProductCapability1, "ProductCapability1", BASIC_M_PRODUCT_CAPABILITY_1_LENGTH); BasicClusterAttributes[index].ProductCapability1[BASIC_M_PRODUCT_CAPABILITY_1_LENGTH] = '\0';

      return SZL_RESULT_SUCCESS;
    }
  return SZL_RESULT_FAILED;
}

/******************************************************************************
 * Basic Cluster - Set product identifier for an endpoint
 ******************************************************************************/
SZL_RESULT_t appZcl_SzlBasicPlugin_SetProductIdentifier(szl_uint8 Endpoint, szl_uint16 ProductIdentifier)
{
  szl_uint8 index;

  index = getBasicClusterEndpointIndex(Endpoint);

  if (index < APP_ZCL_M_MAX_BASIC_CLUSTER_ENDPOINTS)
    {
      BasicClusterAttributes[index].ProductIdentifier = ProductIdentifier;
      return SZL_RESULT_SUCCESS;
    }
  return SZL_RESULT_FAILED;
}

#endif


/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/