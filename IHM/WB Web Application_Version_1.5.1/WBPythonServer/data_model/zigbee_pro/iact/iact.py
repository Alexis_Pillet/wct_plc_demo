
"""
data_model.zigbee_pro.iact.iact
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to control/model iACT Device with one End Point

"""
from data_model.zigbee_pro.zigbee_pro_device import ZigBeeProDevice


class IACT(ZigBeeProDevice):
    """Model IACT device with one End Point"""

    def __init__(self, address, id_wireless_bridge, application_frame_generator):
        """Initialization of the IACT device"""
        self.STATUS_OPEN = 0
        self.STATUS_CLOSE = 1
        self._ON_OFF_CLUSTER = "0006"
        self._END_POINT = 0x01
        self._ATTRIBUTE_ID_ON_OFF = '0000'
        self._COMMAND_ID_OFF = 0x00
        self._COMMAND_ID_ON = 0x01
        self._status = self.STATUS_OPEN
        # call super function
        super(IACT, self).__init__(address, id_wireless_bridge, application_frame_generator)

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        self._status = status

    def update_data(self, data):
        """Update the attributes of the device according to the value specified

        Args:
            data: dict of the value received
        """
        list_attribute = data[self._BASIC_CLUSTER]
        for attribute in list_attribute:
            if attribute.attribute_id == self._MANUFACTURER_NAME:
                self.manufacturer_name = attribute.attribute_value
            elif attribute.attribute_id == self._MODEL_IDENTIFIER:
                self.model_identifier = attribute.attribute_value
            elif attribute.attribute_id == self._APPLICATION_FIRMWARE_VERSION_ID:
                self.application_firmware_version = attribute.attribute_value
            elif attribute.attribute_id == self._APPLICATION_HARDWARE_VERSION_ID:
                self.application_hardware_version = attribute.attribute_value
            elif attribute.attribute_id == self._PRODUCT_SERIAL_NUMBER_ID:
                self.product_serial_number = attribute.attribute_value
            elif attribute.attribute_id == self._PRODUCT_IDENTIFIER_ID:
                self.product_identifier = attribute.attribute_value
            elif attribute.attribute_id == self._PRODUCT_MODEL:
                self.product_model = attribute.attribute_value

        list_attribute = data[self._ON_OFF_CLUSTER]
        for attribute in list_attribute:
            if attribute.attribute_id == self._ATTRIBUTE_ID_ON_OFF:
                self._status = attribute.attribute_value

    def update_rssi(self, lqi, rssi):
        """Update the lqi and rssi of the product

        Args:
            lqi: lqi value
            rssi: rssi value
        """
        self.rssi = rssi
        self.lqi = lqi

    def get_json_data(self):
        """Get the json representation of the data of the IACT product"""
        return {"product": {"address": self.address,
                            "lqi": self.lqi,
                            "rssi": self.rssi,
                            "model_identifier": self.model_identifier,
                            "application_firmware_version": self.application_firmware_version,
                            "application_hardware_version": self.application_hardware_version,
                            "product_serial_number": self.product_serial_number,
                            "product_identifier": self.product_identifier,
                            "product_model": self.product_model,
                            "manufacturer_name": self.manufacturer_name},
                "status": "OPEN" if self.status == self.STATUS_OPEN else "CLOSE"}

    def get_json_device(self, device_number):
        """Get the json representation of the IACT product

        Args:
            device_number: number of the device
        """
        return {"text": str(device_number) + ' - IACT',
                'id': "WB_" + str(self.id_wireless_bridge + 1) + "_DEVICE_" + self.address,
                'type': 'iact',
                'data': self.get_json_data()
                }

    def open(self):
        """Send a Open cluster command"""
        frame = bytearray.fromhex(self.address)
        # Transform in little endian
        frame.reverse()
        frame.append(self._END_POINT)
        cluster_id = bytearray.fromhex(self._ON_OFF_CLUSTER)
        # Transform in little endian
        cluster_id.reverse()
        for byte in cluster_id:
            frame.append(byte)
        frame.append(self._COMMAND_ID_OFF)
        self.application_frame_generator.send_cluster_command_frame(self.id_wireless_bridge, frame)

    def close(self):
        """Send a Close cluster command"""
        frame = bytearray.fromhex(self.address)
        # Transform in little endian
        frame.reverse()
        frame.append(self._END_POINT)
        cluster_id = bytearray.fromhex(self._ON_OFF_CLUSTER)
        # Transform in little endian
        cluster_id.reverse()
        for byte in cluster_id:
            frame.append(byte)
        frame.append(self._COMMAND_ID_ON)
        self.application_frame_generator.send_cluster_command_frame(self.id_wireless_bridge, frame)
