/*
 Name:    Service Private Utility
 Author:  Cam Williams
 Company: Schneider Electric

 Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

 Description:
   Private service utilities for internal service use.
*/

#ifndef __SRV_CORE_PRIVATE_H__
#define __SRV_CORE_PRIVATE_H__

#include "osTypes.h"
#include "zabTypes.h"

#include "erStatusUtility.h" 
#include "srvCoreUtility.h" 

#ifdef __cplusplus
extern "C" {
#endif

// S E R V I C E   S T R U C T U R E

// this must be the first part of a service structure (no app access)
// app shall use methods below to access these fields
typedef struct  {
  unsigned8 Id;
  srvConfigureBack ConfigureBack;
  srvGiveBack GiveBack;
} srvStruct;

#define SRV_STRUCT( s ) ((srvStruct*)(s))

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// S E R V I C E   M A N A G E M E N T

/* These tool methods are used by the service to manage the service, not by the app using the service. 
*/

/******************************************************************************
 * Set the service ID
 ******************************************************************************/
extern
void srvCoreSetServiceId( erStatus* Status, zabService* Service, unsigned8  Id ); // set servide id


/******************************************************************************
 * Set the ASK handler
 ******************************************************************************/
extern
void srvCoreSetConfigureBack( erStatus* Status, zabService* Service, srvConfigureBack ConfigureBack );

/******************************************************************************
 * Set the GIVE handler
 ******************************************************************************/
extern
void srvCoreSetGiveBack(erStatus* Status, zabService* Service, srvGiveBack GiveBack);


/******************************************************************************
 * ASK back config data from app
 * These functions may be called into the core from the vendor.
 * The core may answer, or forward the call onto the app to answer
 ******************************************************************************/
extern 
void srvCoreAskBack( erStatus* Status,   zabService* Service, 
                     zabAskId What, unsigned32 Param1, 
                     unsigned32* Size, void* Buffer );

/* Shortcut functions for easy handling of common data types */
extern unsigned8  srvCoreAskBack8(erStatus* Status, zabService* Service, zabAskId What, unsigned8  Default);
extern unsigned16 srvCoreAskBack16(erStatus* Status, zabService* Service, zabAskId What, unsigned16 Default);
extern unsigned32 srvCoreAskBack32(erStatus* Status, zabService* Service, zabAskId What, unsigned32 Default);
extern unsigned32 srvCoreAskBackBuffer(erStatus* Status, zabService* Service, zabAskId What, unsigned32 Param1, unsigned32 Size, unsigned8* Buffer);
extern unsigned64 srvCoreAskBack64( erStatus* Status, zabService* Service, zabAskId What, unsigned64 Default);

/******************************************************************************
 * Give back information to the app
 * Information is passed to the app via a pre-register function pointer
 ******************************************************************************/
void srvCoreGiveBack( erStatus* Status,  zabService* Service, 
                      zabGiveId What, unsigned32 Param1,
                      unsigned32 Size, unsigned8* Buffer );

/* Shortcut functions for easy handling of common data types */
extern void srvCoreGiveBack8(  erStatus* Status, zabService* Service, zabGiveId What, unsigned8  Value );
extern void srvCoreGiveBack16( erStatus* Status, zabService* Service, zabGiveId What, unsigned16 Value );
extern void srvCoreGiveBack32( erStatus* Status, zabService* Service, zabGiveId What, unsigned32 Value );
extern void srvCoreGiveBackBuffer ( erStatus* Status,  zabService* Service, zabGiveId What, unsigned32 Size, unsigned8* Buffer );

#ifdef __cplusplus
}
#endif

#endif 

