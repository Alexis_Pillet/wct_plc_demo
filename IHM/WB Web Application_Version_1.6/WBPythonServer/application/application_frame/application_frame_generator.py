
"""
application.application_frame.application_frame_generator
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to generate applicative frames

"""
from .application_frame_structure import *


class ApplicationFrameGenerator:
    """Functions to generate applicative frames"""

    DEFAULT_RESPONSE_OPTION = Application.OptionResponse.ENABLE
    DEFAULT_SOURCE_EP_OPTION = Application.OptionEP.NOT_PRESENT
    DEFAULT_DESTINATION_ADDRESS_TYPE_OPTION = Application.OptionAddressType.IEEE_ADDRESS
    DEFAULT_DESTINATION_EP_OPTION = Application.OptionEP.PRESENT
    DEFAULT_DIRECTION_OPTION = Application.OptionDirection.CLIENT_TO_SERVER
    DEFAULT_MS_OPTION = Application.OptionMS.NORMALIZED

    def __init__(self, application_process):
        """Save application process instance & default_option of the frames"""
        self._application_process = application_process
        self._default_option = self.DEFAULT_RESPONSE_OPTION + (self.DEFAULT_SOURCE_EP_OPTION << 1) \
                                                            + (self.DEFAULT_DESTINATION_ADDRESS_TYPE_OPTION << 2) \
                                                            + (self.DEFAULT_DESTINATION_EP_OPTION << 4)  \
                                                            + (self.DEFAULT_DIRECTION_OPTION << 5)  \
                                                            + (self.DEFAULT_MS_OPTION << 6)

    @property
    def default_option(self):
        return self._default_option

    @default_option.setter
    def default_option(self, default_option):
        self._default_option = default_option

    @staticmethod
    def set_option(option, mask, value_bit_option):
        """Allow to change the value of one bit of the option field

           Args:
               option: value of the option to change
               mask: mask to choose the bit to change
               value_bit_option: new value of the bit

           Returns:
               The new option
        """
        option &= ~mask          # Clear the bit indicated by the mask (if x is False)
        if value_bit_option:
            option |= mask       # If x was True, set the bit indicated by the mask.
        return option            # Return the result, we're done.

    def send_cluster_command_frame(self, device_number, payload, option=None):
        """Allow to send a cluster command frame

           Args:
               device_number: device number of the WB
               payload: payload of the frame
               option: default value self._default_option
        """
        if option is None:
            option = self.default_option
        self._application_process.send_applicative_frame(Application.Interface.ZIGBEE,
                                                         Application.CommandID.CLUSTER_COMMAND,
                                                         option, device_number, payload)

    def send_get_attribute_command_frame(self, device_number, payload, option=None):
        """Allow to send a get attribute frame

           Args:
               device_number: device number of the WB
               payload: payload of the frame
               option: default value self._default_option
        """
        if option is None:
            option = self.default_option
        self._application_process.send_applicative_frame(Application.Interface.ZIGBEE,
                                                         Application.CommandID.GET_COMMAND,
                                                         option, device_number, payload)

    def send_set_attribute_command_frame(self, device_number, payload, option=None):
        """Allow to send a set attribute frame

           Args:
               device_number: device number of the WB
               payload: payload of the frame
               option: default value self._default_option
        """
        if option is None:
            option = self.default_option
        self._application_process.send_applicative_frame(Application.Interface.ZIGBEE,
                                                         Application.CommandID.SET_COMMAND,
                                                         option, device_number, payload)
