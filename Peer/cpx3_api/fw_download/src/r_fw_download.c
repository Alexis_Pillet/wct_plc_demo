/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/******************************************************************************
* File Name     : r_fw_download.c
* Device(s)     : RX631
* Tool-chain    : KPIT GNURX-ELF 15.01
* H/W platform  : G-CPX / EU-CPX2 / G-CPX3
* Description   : Sample software
******************************************************************************/

/******************************************************************************
Includes <System Includes> , "Project Includes"
******************************************************************************/
#include "r_typedefs.h"
#include "r_bsp_api.h"
#include "r_timer_api.h"
#include "r_fw_download.h"
#include "r_config.h"
#include "r_c3sap_sys_if.h"

/******************************************************************************
Macro definitions
******************************************************************************/
#define COM_BAUDRATECHG                (0xA0u)                              /* Baudrate change command */
#define COM_BAUDRATECHG_ADV            (0xA1u)                              /* Advanced Baudrate change command */
#define COM_BAUDRATECHG_RESP           (0xAAu)                              /* Baudrate change command response */
#define COM_BAUDRATE_1                 (0xC1u)                              /* Baudrate command 1 */
#define COM_BAUDRATE_ACCEPT            (0xCFu)                              /* Baudrate command ACCEPPT */
#define COM_BOOT_END                   (0xB0u)                              /* Boot end command */
#define COM_BOOT_REQUEST               (0x80u)                              /* Request command */

#define MODE_BOOT_IDLE                 (0x00u)                              /* Idle (command wait) */
#define MODE_BOOT_BUSY                 (0x01u)                              /* Transmitting */
#define MODE_BOOT_SUSPEND              (0x02u)                              /* Transmit suspend */
#define MODE_BOOT_END                  (0x03u)                              /* CPX download mode end */

#define FWDL_INFO_TABLE_CHKSUM_OFFSET  (0x0Cu)                              /* Offset of info table checksum */
#define FWDL_INFO_TABLE_START_OFFSET   (0x10u)                              /* Offset of info table */

/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/

/******************************************************************************
Private global variables
******************************************************************************/
static const uint8_t *  pcpx_firmware_table = NULL;
static const uint8_t *  pit_pointer;
static uint32_t         it_remain;
static const uint8_t *  pseg_pointer;
static uint32_t         seg_remain;
static volatile uint8_t boot_mode           = MODE_BOOT_IDLE;               /* CPX Boot mode state */
static uint8_t          com_buffer[2];
static uint8_t          dl_baud;
static uint8_t          cmd_baud;
static uint32_t         dl_timeout;
static uint8_t          fw_seg_num;

/******************************************************************************
Functions
******************************************************************************/
static void fw_dl_cpx_tx_finished (void);
static void fw_dl_rx_byte_handle (const uint8_t rx_byte);
static r_result_t fw_dl_start_fw_download (const uint8_t * pfirmware_pointer);
static void fw_dl_generate_reset (void);
static r_result_t fw_dl_download_monitor (void);
static r_result_t fw_dl_check_contents (const uint8_t * pfirmware_pointer);

/******************************************************************************
* Function Name:fw_dl_generate_reset
* Description  : Generates CPX reset signal
* Arguments    : None
* Return Value : None
******************************************************************************/
/*!
   \fn          static void fw_dl_generate_reset(void)
   \brief       Generates CPX reset signal
 */
static void fw_dl_generate_reset (void)
{
    R_BSP_Cpx3Reset ();
}
/******************************************************************************
   End of function  fw_dl_generate_reset
******************************************************************************/

/******************************************************************************
* Function Name     : fw_dl_download_monitor
* Description       : Monitors CPX firmware download process
* Argument          : None
* Return Value      : R_RESULT_FAILED when timed out/error or
                      R_RESULT_SUCCESS when download finished
******************************************************************************/

/*!
   \fn          static r_result_t fw_dl_download_monitor(void)
   \brief       Monitors CPX firmware download process
   \return      R_RESULT_FAILED when timed out/error or
                R_RESULT_SUCCESS when download finished
 */
static r_result_t fw_dl_download_monitor (void)
{
    /* Check if firmware download has finished successfully or if a timeout occurred after 2s. */
    r_result_t result   = R_RESULT_FAILED;


    /* Start one shot timer with don't care handle */

    R_TIMER_TimerOneShotOn (R_TIMER_ID_FW_DL, dl_timeout, 0x00u);           // HANDLE_DONT_CARE

    /* Wait for time to elapse or until complete transfer has been flagged.*/
    while ((R_TIMER_TimerOneShotOn (R_TIMER_ID_FW_DL, 1u, 0x00u) != R_RESULT_SUCCESS) && (MODE_BOOT_END != boot_mode))
    {
        /* Do Nothing */
    }

    /* Deactivate the timer */
    R_TIMER_TimerOneShotOff (R_TIMER_ID_FW_DL, 0x00u);


    if (MODE_BOOT_END == boot_mode)
    {
        result = R_RESULT_SUCCESS;
    }

    boot_mode = MODE_BOOT_IDLE;                                             /* Reset boot_mode back to idle */

    return result;
} /* fw_dl_download_monitor */
/******************************************************************************
   End of function  fw_dl_download_monitor
******************************************************************************/

/******************************************************************************
* Function Name     : fw_dl_rx_byte_handle
* Description       : UART Rx byte finished handing function
* Argument          : rx_byte : the byte that has been received
* Return Value      : none
******************************************************************************/
/*!
   \fn          static void fw_dl_rx_byte_handle(const uint8_t rx_byte)
   \brief       UART Rx byte finished handing function
   \param[in]   rx_byte the byte that has been received
 */
static void fw_dl_rx_byte_handle (const uint8_t rx_byte)
{
    const uint8_t * ptemp;
    uint32_t        offset_adr;

    /* NULL pointer check for stored firmware image. */
    if (NULL == pcpx_firmware_table)
    {
        return;
    }

    switch (rx_byte)
    {
        case COM_BAUDRATECHG:
        case COM_BAUDRATE_ACCEPT:
            if (MODE_BOOT_IDLE == boot_mode)
            {
                /* SCI initialize */
                R_BSP_ConfigureUart (R_UART_CPX_CLOCK, R_SYS_GetDirectBaud (dl_baud), R_BSP_RX_CPX_UART, R_UART_TX_IPR, R_UART_RX_IPR, R_BSP_CLK_OUT_OFF
                                     , &fw_dl_cpx_tx_finished, &fw_dl_rx_byte_handle);

                /* Transmit baudrate change command response */
                com_buffer[0] = COM_BAUDRATECHG_RESP;
                pit_pointer   = com_buffer;
                it_remain     = 1uL;
                seg_remain    = 0uL;

                /* Start transmit */
                R_BSP_SendUart (pit_pointer, it_remain, R_BSP_RX_CPX_UART);
            }
            break;

        case COM_BAUDRATECHG_ADV:
            if (MODE_BOOT_IDLE == boot_mode)
            {
                com_buffer[0] = COM_BAUDRATE_1;
                com_buffer[1] = (uint8_t)((dl_baud << 4) | cmd_baud);
                pit_pointer   = com_buffer;
                it_remain     = 2uL;
                seg_remain    = 0uL;

                /* Start transmit */
                R_BSP_SendUart (pit_pointer, it_remain, R_BSP_RX_CPX_UART);
            }
            break;

        case COM_BOOT_END: /* Download finished. Set flag. */
            boot_mode = MODE_BOOT_END;
            break;

        default:
            if (COM_BOOT_REQUEST == (uint8_t)(rx_byte & 0xF0))
            {
                /* Program transfer command. */
                if (MODE_BOOT_IDLE == boot_mode)
                {
                    if ((rx_byte & 0x0F) > fw_seg_num)
                    {
                        /* invalid segment number */
                        return;
                    }

                    boot_mode = MODE_BOOT_BUSY;
                    ptemp     = (const uint8_t *)(pcpx_firmware_table + ((rx_byte & 0x0F) << 4));
                    if (NULL != ptemp)
                    {
                        offset_adr   = ((((uint32_t)ptemp[3] << 24) + ((uint32_t)ptemp[2] << 16)) + ((uint32_t)ptemp[1] << 8)) + (uint32_t)ptemp[0];

                        /* Compute info table pointer */
                        pit_pointer  = (const uint8_t *)&ptemp[4];
                        it_remain    = 12u;

                        /* Compute segment program pointer */
                        pseg_pointer = (const uint8_t *)(pcpx_firmware_table + offset_adr);
                        seg_remain   = ((((uint32_t)ptemp[11] << 24) + ((uint32_t)ptemp[10] << 16)) + ((uint32_t)ptemp[9] << 8)) + (uint32_t)ptemp[8];

                        /* Info Table Transmission */
                        R_BSP_SendUart (pit_pointer, it_remain, R_BSP_RX_CPX_UART);
                    }
                }
                else
                {
                    /* Illegal timing */
                }
            }
            else
            {
                /* Undefined command */
            }
            break;
    } /* switch */
} /* fw_dl_rx_byte_handle */
/******************************************************************************
   End of function  fw_dl_rx_byte_handle
******************************************************************************/

/******************************************************************************
* Function Name     : fw_dl_cpx_tx_finished
* Description       : UART Tx finished handling function
* Argument          : none
* Return Value      : none
******************************************************************************/
/*!
   \fn          static void fw_dl_cpx_tx_finished(void);
   \brief       UART Tx finished handling function
 */
static void fw_dl_cpx_tx_finished (void)
{
    if (0uL != seg_remain)
    {
        /* Segment Program Transmission */
        R_BSP_SendUart (pseg_pointer, seg_remain, R_BSP_RX_CPX_UART);
        seg_remain    = 0uL;
    }
    else
    {
        /* finished Segment Program Transmission, receive next message from CPX */
        boot_mode = MODE_BOOT_IDLE;
    }
} /* fw_dl_cpx_tx_finished */
/******************************************************************************
   End of function  fw_dl_cpx_tx_finished
******************************************************************************/

/******************************************************************************
* Function Name     : fw_dl_start_fw_download
* Description       : Initiates firmware download to CPX
* Argument          : pfirmware_pointer : uint8_t pointer to firmware array
* Return Value      : R_RESULT_FAILED when timed out/error or
                      R_RESULT_SUCCESS when download finished
******************************************************************************/
/*!
   \fn          static r_result_t fw_dl_start_fw_download(const uint8_t* pfirmware_pointer)
   \brief       Initiates firmware download to CPX
   \param[in]   pfirmware_pointer uint8_t pointer to firmware array
   \return      R_RESULT_FAILED when timed out/error or
                R_RESULT_SUCCESS when download finished
 */
static r_result_t fw_dl_start_fw_download (const uint8_t * pfirmware_pointer)
{
    /* Check for valid pointer. */
    if (NULL == pfirmware_pointer)
    {
        return R_RESULT_ILLEGAL_NULL_POINTER;
    }

    /* Set static pointer to firmware table */
    pcpx_firmware_table = pfirmware_pointer;


    /* Generate reset */
    fw_dl_generate_reset ();

    /* Call download monitor. */
    return fw_dl_download_monitor ();
} /* fw_dl_start_fw_download */
/******************************************************************************
   End of function  fw_dl_start_fw_download
******************************************************************************/

/******************************************************************************
* Function Name:R_FW_Download
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_FW_Download (r_sys_boot_info_t * boot_info)
{
    uint16_t   cnt       = 0u;
    r_result_t ret_value = R_RESULT_FAILED;

    /* NULL pointer check. */
    if ((NULL == boot_info) || (NULL == boot_info->pfw))
    {
        return R_RESULT_ILLEGAL_NULL_POINTER;
    }

    if (fw_dl_check_contents (boot_info->pfw) != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }

    dl_baud    = boot_info->dl_baud;
    cmd_baud   = boot_info->cmd_baud;
    dl_timeout = boot_info->dl_timeout;

    /* Start firmware download. Procedure is repeated until download has
       finished successfully (max R_CPX_TX_FW_DL_RETRIES times). */
    while ((R_RESULT_SUCCESS != ret_value) && (R_CPX_TX_FW_DL_RETRIES > cnt))
    {
        /* Configure UART RX_CPX_UART for firmware download (1Mbps for CPX1, 115200bps for first segment of CPX2) */
        if (R_BSP_ConfigureUart (R_UART_CPX_CLOCK,
                                 115200u,
                                 R_BSP_RX_CPX_UART,
                                 R_UART_TX_IPR,
                                 R_UART_RX_IPR,
                                 R_BSP_CLK_OUT_OFF,
                                 &fw_dl_cpx_tx_finished,
                                 &fw_dl_rx_byte_handle) != R_RESULT_SUCCESS)
        {
            return R_RESULT_FAILED;
        }

        /* Start UART reception to wait for CPX download request */
        ret_value = fw_dl_start_fw_download (boot_info->pfw + FWDL_INFO_TABLE_START_OFFSET);

        /* Increse counter */
        cnt++;
    }

    return ret_value;
} /* R_FW_Download */
/******************************************************************************
   End of function  R_FW_Download
******************************************************************************/

/******************************************************************************
* Function Name:fw_dl_check_contents
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t fw_dl_check_contents (const uint8_t * pfirmware_pointer)
{
    const uint8_t * ptemp    = pfirmware_pointer;
    uint32_t        calc_sum = 0;
    uint32_t        table_sum;
    uint32_t        table_size;
    uint32_t        i;
    r_result_t      status   = R_RESULT_SUCCESS;

    /* NULL pointer check. */
    if (NULL == pfirmware_pointer)
    {
        return R_RESULT_ILLEGAL_NULL_POINTER;
    }

    table_sum  = ((((uint32_t)ptemp[FWDL_INFO_TABLE_CHKSUM_OFFSET + 3] << 24) + /* PRQA S 2824 */
                   ((uint32_t)ptemp[FWDL_INFO_TABLE_CHKSUM_OFFSET + 2] << 16)) +
                  ((uint32_t)ptemp[FWDL_INFO_TABLE_CHKSUM_OFFSET + 1] << 8)) +
                 (uint32_t)ptemp[FWDL_INFO_TABLE_CHKSUM_OFFSET];

    table_size = ((((uint32_t)ptemp[FWDL_INFO_TABLE_START_OFFSET + 3] << 24) +
                   ((uint32_t)ptemp[FWDL_INFO_TABLE_START_OFFSET + 2] << 16)) +
                  ((uint32_t)ptemp[FWDL_INFO_TABLE_START_OFFSET + 1] << 8)) +
                 (uint32_t)ptemp[FWDL_INFO_TABLE_START_OFFSET];

    if (table_size <= 256)
    {
        for (i = 0; i < table_size; i++)
        {
            calc_sum += ptemp[FWDL_INFO_TABLE_START_OFFSET + i];
        }
        calc_sum = ~calc_sum;

        if (table_sum != calc_sum)
        {
            fw_seg_num = 0;
            status     = R_RESULT_FAILED;
        }
        else
        {
            fw_seg_num = (uint8_t)(table_size >> 4);
        }
    }
    else
    {
        fw_seg_num          = 0;
        pcpx_firmware_table = NULL;
        status              = R_RESULT_FAILED;
    }

    return status;
} /* fw_dl_check_contents */
/******************************************************************************
   End of function  fw_dl_check_contents
******************************************************************************/

