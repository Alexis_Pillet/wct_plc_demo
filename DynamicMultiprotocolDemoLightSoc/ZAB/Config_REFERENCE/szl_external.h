#ifndef _SZL_EXTERNAL_H_
#define _SZL_EXTERNAL_H_

#include "zabCoreService.h"
#include "osMemUtility.h"

#include <string.h>
#include "em_assert.h"

#ifdef __cplusplus
extern "C" {
#endif

// IMPORTANT!!!!
#ifndef PRAGMA_PACK
  #define PRAGMA_PACK
#endif

/* Convert ZAB types into SZL types */
typedef signed8     szl_int8;
typedef unsigned8   szl_uint8;
typedef unsigned8   szl_enum8;
typedef unsigned8   szl_bitmap8;
typedef signed16    szl_int16;
typedef unsigned16  szl_uint16;
typedef signed32    szl_int32;
typedef unsigned32  szl_uint32;
typedef signed64    szl_int64;
typedef unsigned64  szl_uint64;

typedef unsigned8   szl_byte;

typedef zab_bool    szl_bool;

#ifndef NULL
  #define NULL 0
#endif

// Set ENDIANNESS
#ifdef ZAB_M_BIG_ENDIAN
  #define SZL_BIG_ENDIANNESS
  #undef SZL_LITTLE_ENDIANNESS
#endif
#ifdef ZAB_M_LITTLE_ENDIAN
  #undef SZL_BIG_ENDIANNESS
  #define SZL_LITTLE_ENDIANNESS
#endif


// this system does not support 64bits datatypes
#define SUPPORTS_64_BIT_DATATYPES   ( 1 )
   
#include "appOsMemoryGlue.h"
#define szl_mem_alloc(_len, _mal_id)  APP_MEM_GLUE_MALLOC( (_len), SzlZab_GetServiceId(Service), (_mal_id) )
#define szl_mem_free(_ptr)            APP_MEM_GLUE_FREE( (_ptr), SzlZab_GetServiceId(Service) )
     
#define szl_strlen     strlen
#define szl_memset     memset
#define szl_memcpy     memcpy


// ASSERT -
#define SZL_ASSERT(expr) EFM_ASSERT(expr)
#ifdef __cplusplus
}
#endif

#endif /*_SZL_EXTERNAL_H_*/
