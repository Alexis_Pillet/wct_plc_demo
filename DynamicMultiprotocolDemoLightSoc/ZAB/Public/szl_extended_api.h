/**
 * @ingroup  szl_api
 * @defgroup szl_extended_api SZL Extended API
 * @author   Michael Thorsoe
 * <b>Extended API Documentation</b>
 *
 * Defines the Extended interface for towards the SZL
 *
 * The interface is aimed for the SZL Plugin developers to get access to the Extended API functions.
 * The interface consists of both Types, Functions, Macros and Defines
 *
 * NOTE: ALL data belongs to the calling party, meaning that the called party needs to take copy of the data to store
 *       This also means that the called party should NOT free data provided as parameter.
 *
 * 002.002.021  21-Apr-16   MvdB   ARTF167807: Support Multi-Cluster Attribute Read/Write for GP
 * 002.002.032  09-Jan-17   MvdB   ARTF172065: Rationalise duplicated private functions into public SZLEXT_ZigBeeDataTypeIsAnalog()
 */
/*
Copyright (c) 2014 - Schneider-Electric R&D

Schneider - ZigBee - Library. (SZL)
*/
#ifndef _SZL_EXTENDED_API_H_
#define _SZL_EXTENDED_API_H_

#ifdef __cplusplus
extern "C"
{
#endif


/**
 * @ingroup szl_extended_api_def
 * @defgroup szl_extended_api_str Structs
 * This section list all the different structures defined for the Extended API
 * @{
 */

/**
 * Type for AttributeReportOnDemandParams.
 * This struct holds the information for the attribute to report.
 */
typedef struct
{
    SZL_EP_t SourceEndpoint;                /**< The Endpoint for the source (App) */
    SZL_Addresses_t DestAddr;               /**< The Addressing mode to specify how the device communicates with the destination, either via binding table or direct addressing */
    szl_bool ManufacturerSpecific;          /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint16 ClusterID;                   /**< The Cluster ID that has been bound */
    szl_uint8 NumberOfAttributes;           /**< The number of attribute ID's in the following Attributes array */
    szl_uint16 Attributes[SZL_VLA_INIT];    /**< Attribute's - (extends beyond bounds) */
} SZLEXT_AttributeReportOnDemandParams_t;
#define SZLEXT_AttributeReportOnDemandParams_t_SIZE(_num_attributes) (sizeof(SZLEXT_AttributeReportOnDemandParams_t) + (sizeof(szl_uint16) * (_num_attributes)) - (sizeof(szl_uint16) * SZL_VLA_INIT))



/**
 * Type for Multi Cluster Attribute.
 */
typedef struct
{
    szl_uint16 ClusterID;                   /**< The Cluster ID of the attribute */
    szl_uint16 AttributeID;                 /**< AttributeID for the attribute */
} SZLEXT_MultiCluster_Attribute_t;

/**
 * Type for Multi Cluster Attribute Data.
 * This struct holds the definition for a attribute data value.
 */
typedef struct
{
    szl_uint16 ClusterID;                   /**< The Cluster ID of the attribute */
    szl_uint16 AttributeID;                 /**< AttributeID for the Attribute */
    SZL_STATUS_t Status;                    /**< The Status for the attribute - If NOT SUCCESS none of the following data is valid */
    SZL_ZIGBEE_DATA_TYPE_t DataType;        /**< The data type for the Attribute - if type is not present then set to SZL_ZB_DATATYPE_UNKNOWN */
    szl_uint8 DataLength;                   /**< The length of the data in bytes - if 0 then no data is available and Data should be null*/
    void* Data;                             /**< pointer to the data - @note all data will be in native endianness and in App data type */
} SZLEXT_MultiCluster_AttributeData_t;

/**
 * Type for Multi Cluster AttributeReadReqParams.
 * This struct holds the information for the attribute to read.
 */
typedef struct
{
    SZL_EP_t SourceEndpoint;                    /**< The Endpoint for the source (App) */
    SZL_Addresses_t DestAddrMode;               /**< The Addressing mode to specify how the device communicates with the destination, either via binding table or direct addressing */
    szl_bool ManufacturerSpecific;              /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint8 NumberOfAttributes;               /**< The number of attribute ID's in the following Attribute ID's array */
    SZLEXT_MultiCluster_Attribute_t AttributeIDs[SZL_VLA_INIT];      /**< Multi Cluster Attribute ID's - (extends beyond bounds) */
} SZLEXT_MultiCluster_AttributeReadReqParams_t;
#define SZLEXT_MultiCluster_AttributeReadReqParams_t_SIZE(_num_attributes) (sizeof(SZLEXT_MultiCluster_AttributeReadReqParams_t) + (sizeof(SZLEXT_MultiCluster_Attribute_t) * (_num_attributes)) - (sizeof(SZLEXT_MultiCluster_Attribute_t) * SZL_VLA_INIT))

/**
 * Type for Multi Cluster AttributeReadRespParams.
 * This struct holds the information for the read attributes.
 */
typedef struct
{
    SZL_EP_t DestinationEndpoint;                       /**< The Endpoint for the Destination (App) */
    SZL_Addresses_t SourceAddress;                      /**< The address of the source (remote device). */
    szl_bool ManufacturerSpecific;                      /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint8 NumberOfAttributes;                       /**< The number of attributes in the following Attributes array */
    SZLEXT_MultiCluster_AttributeData_t Attributes[SZL_VLA_INIT];       /**< Attributes - (extends beyond bounds) */
} SZLEXT_MultiCluster_AttributeReadRespParams_t;
#define SZLEXT_MultiCluster_AttributeReadRespParams_t_SIZE(_num_attributes) (sizeof(SZLEXT_MultiCluster_AttributeReadRespParams_t) + (sizeof(SZLEXT_MultiCluster_AttributeData_t) * (_num_attributes)) - (sizeof(SZLEXT_MultiCluster_AttributeData_t) * SZL_VLA_INIT))



/**
 * Type for Multi Cluster AttributeWriteReqParams.
 * This struct holds the information for the attribute to write.
 */
typedef struct
{
    SZL_EP_t SourceEndpoint;                            /**< The Endpoint for the source (App) */
    SZL_Addresses_t DestAddrMode;                       /**< The Addressing mode to specify how the device communicates with the destination, either via binding table or direct addressing */
    szl_bool ManufacturerSpecific;                      /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint8 NumberOfAttributes;                       /**< The number of attributes in the following Attributes array */
    SZLEXT_MultiCluster_AttributeData_t Attributes[SZL_VLA_INIT];       /**< Attributes - (extends beyond bounds) */
} SZLEXT_MultiCluster_AttributeWriteReqParams_t;
#define SZLEXT_MultiCluster_AttributeWriteReqParams_t_SIZE(_num_attributes) (sizeof(SZLEXT_MultiCluster_AttributeWriteReqParams_t) + (sizeof(SZLEXT_MultiCluster_AttributeData_t) * (_num_attributes)) - (sizeof(SZLEXT_MultiCluster_AttributeData_t) * SZL_VLA_INIT))

/**
 * Type for Multi Cluster Attribute Status.
 * This struct holds the definition for a attribute status value.
 */
typedef struct
{
    szl_uint16 ClusterID;                   /**< The Cluster ID of the attribute */
    szl_uint16 AttributeID;                 /**< AttributeID for the Attribute */
    SZL_STATUS_t Status;                    /**< The Status for the attribute */
} SZLEXT_MultiCluster_AttributeStatus_t;

/**
 * Type for Multi Cluster AttributeWriteRespParams.
 * This struct holds the result for the attribute write.
 */
typedef struct
{
    SZL_EP_t DestinationEndpoint;                      /**< The Endpoint for the Destination (App) */
    SZL_Addresses_t SourceAddress;                     /**< The address of the source (remote device). */
    szl_bool ManufacturerSpecific;                     /**< This is set to szl_true if the attributes are manufacturer specific, else set to szl_false */
    szl_uint8 NumberOfAttributes;                      /**< The number of attributes in the following AttributeID's array */
    SZLEXT_MultiCluster_AttributeStatus_t Attributes[SZL_VLA_INIT];    /**< Attribute ID's - (extends beyond bounds) */
} SZLEXT_MultiCluster_AttributeWriteRespParams_t;
#define SZLEXT_MultiCluster_AttributeWriteRespParams_t_SIZE(_num_failed) (sizeof(SZLEXT_MultiCluster_AttributeWriteRespParams_t) + (sizeof(SZLEXT_MultiCluster_AttributeStatus_t) * (_num_failed)) - (sizeof(SZLEXT_MultiCluster_AttributeStatus_t) * SZL_VLA_INIT))

/**
 * @}
 */

/**
 * @ingroup szl_extended_api
 * @defgroup szl_extended_api_func Extended Functions
 * This section defines all the Extended API functions implemented in the SZLibrary for the Plugins or Advanced App's to call.<br>
 *
 * @{
 */


/**
 *
 * Attribute Report On Demand
 *
 * This function is used to send a report for a specific attribute.<br>
 * This can be usefull for some attributes like if the device is booting up after a upgrade- we send out a report immediately to notify the new sw version
 * @note The attributes has to be registered as valid @ref SZL_DataPoint_t in advance using the @ref SZL_AttributesRegister.
 * All attributes has to have the same endpoint and the same manufacture status!
 *
 * \msc
 *  AP1  [label="Module", linecolour="#0000ff"],
 *  SZ1  [label="SZL", linecolour="#00ff60"],
 *  ZB1  [label="ZigBee Brick", linecolour="#ff0000"],
 *  NWK  [label="Network", linecolour="#C0C0C0"],
 *
 * AP1 =>   SZ1 [label="ReportOnDemand()", URL="\ref SZLEXT_AttributeReportOnDemandParams_t"];
 * AP1 <<   SZ1 [label="Result", URL="\ref SZL_RESULT_t"];
 * SZ1 :>   ZB1 [label="Report"];
 * ZB1 ->   NWK [label="Message"];
 * \endmsc
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | PLI |   |-------------+---|
  |-----+---| Router      | X |
  | SZL | X |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService*) Server Instance Pointer
 * @param[in]  Params   (@ref SZLEXT_AttributeReportOnDemandParams_t) The params for the operation
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZLEXT_AttributeReportOnDemand(zabService* Service, SZLEXT_AttributeReportOnDemandParams_t* Params);


/**
 *
 * Multi Cluster Attribute Read response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZLEXT_MultiCluster_AttributeReadReq <br>
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Status        (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params        (@ref SZLEXT_MultiCluster_AttributeReadRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZLEXT_CB_MultiCluster_AttributeReadResp_t) (zabService* Service, SZL_STATUS_t Status, SZLEXT_MultiCluster_AttributeReadRespParams_t* Params, szl_uint8 TransactionId);

/**
 *
 * Multi Cluster Attribute Read request - GREEN POWER ONLY
 *
 * This is the function used by the application to read attribute(s) value from another device. <br>
 * A cluster may be specified per attribute.
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback      (@ref SZLEXT_CB_MultiCluster_AttributeReadResp_t) the callback function for the response
 * @param[in]  Params        (@ref SZLEXT_MultiCluster_AttributeReadReqParams_t*) pointer to the parameters
 * @param[out] TransactionId pointer to location where Transaction ID should be returned. Set to NULL if caller does not want the Transaction ID.
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZLEXT_MultiCluster_AttributeReadReq(zabService* Service, SZLEXT_CB_MultiCluster_AttributeReadResp_t Callback, SZLEXT_MultiCluster_AttributeReadReqParams_t* Params, szl_uint8* TransactionId);


/**
 *
 * Multi Cluster Attribute Write response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZLEXT_MultiCluster_AttributeReadReq <br>
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Status        (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params        (@ref SZLEXT_MultiCluster_AttributeWriteRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZLEXT_CB_MultiCluster_AttributeWriteResp_t) (zabService* Service, SZL_STATUS_t Status, SZLEXT_MultiCluster_AttributeWriteRespParams_t* Params, szl_uint8 TransactionId);

/**
 *
 * Multi Cluster Attribute Write request - GREEN POWER ONLY
 *
 * This is the function used by the application to write attribute(s) value from another device. <br>
 * A cluster may be specified per attribute.
 *
 * @param[in]  Service       (@ref zabService*) Server Instance Pointer
 * @param[in]  Callback      (@ref SZLEXT_CB_MultiCluster_AttributeWriteResp_t) the callback function for the response
 * @param[in]  Params        (@ref SZLEXT_MultiCluster_AttributeWriteReqParams_t*) pointer to the parameters
 * @param[out] TransactionId pointer to location where Transaction ID should be returned. Set to NULL if caller does not want the Transaction ID.
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZLEXT_MultiCluster_AttributeWriteReq(zabService* Service, SZLEXT_CB_MultiCluster_AttributeWriteResp_t Callback, SZLEXT_MultiCluster_AttributeWriteReqParams_t* Params, szl_uint8* TransactionId);



/**
 *
 * Data Type is Analog
 *
 * This is the function used to check if a DataType is analog or discrete
 *
 * @param[in]  DataType      (@ref SZL_ZIGBEE_DATA_TYPE_t) Data Type
 *
 * @return @ref szl_bool
 */
szl_bool SZLEXT_ZigBeeDataTypeIsAnalog(SZL_ZIGBEE_DATA_TYPE_t DataType);

/**
 * @}
 */


#ifdef __cplusplus
}
#endif

#endif /*_SZL_EXTENDED_API_H_*/
