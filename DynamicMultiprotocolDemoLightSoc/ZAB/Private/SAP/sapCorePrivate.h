/*
 Name:    Private Message Shared Definitions
 Author:  ZigBee Excellence Center
 Company: Schneider Electric

 Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

 Description:
    Private for implementation that we do not want to expose outside the module.
*/

#ifndef __SAP_CORE_PRIVATE_H__
#define __SAP_CORE_PRIVATE_H__

#include "osTypes.h"
#include "sapTypes.h" 
#include "sapCoreUtility.h" 

#include "zabCoreConfigure.h"

#ifdef __cplusplus
extern "C" {
#endif

#define SAP_MSG_DIRECTION_MASK 0x0F

typedef struct {
  unsigned8       FlagsDir;   // Direction is a 4 bit mask, each bit is a direction.
  sapCoreCallBack CallBack;   // fixed service function called when message passed into SAP
} sapAccessStruct;

// instance of an SAP (service access point)
typedef struct sapStructure {
  void* Service;                // service behind interface instance
  void* Allocate;               // function to allocate some type of message
  void* Free;                   // function to free some type of message
  unsigned8 MutexId;            // id for mutex
  unsigned8 ConnectMax;         // max of output functions in distribution array below 
  sapAccessStruct Access[VLA_INIT];    // inside service function array called when message passed out of SAP 
} sapStruct; 

#define SAP_STRUCT( s )          ((sapStruct*)(s))
#define SAP_SERVICE( s )          (SAP_STRUCT( s )->Service)
#define SAP_ACCESS_SIZE           (sizeof( sapAccessStruct ))
#define SAP_ENTRY_SIZE( n )       (SAP_ACCESS_SIZE*(n))
#define SAP_HEADER_SIZE           (sizeof( sapStruct ) - (VLA_INIT * sizeof(sapAccessStruct)))
#define SAP_SIZE( n )             (SAP_HEADER_SIZE + SAP_ENTRY_SIZE( n ))

#define SAP_MATCH_DIR( a, d )     ((d) & ((a)->FlagsDir & SAP_MSG_DIRECTION_MASK)) // returns directions that match access call back

// FUNCTION PROTOTYPES HERE

/* get MutexId from a SAP 
*/
unsigned8 sapCoreGetMutexId( sapHandle Sap );

#ifdef __cplusplus
}
#endif

#endif
