/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Service
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.003  29-Jun-16   MvdB   Support link locking and timeouts in zabNcpEzsp
 * 000.000.012  21-Jul-16   MvdB   Support RFT for Silabs
 *                                  - Add zabNcpEzspMfglib, zabNcpEzspUtilities, zabNcpRft
 * 000.000.013  21-Jul-16   MvdB   Support Network Join
 * 000.000.017  26-Jul-16   MvdB   Support ZAB_ACTION_CHANGE_NETWORK_KEY
 * 000.000.018  26-Jul-16   MvdB   Support AF Multicasts
 * 000.000.019  26-Jul-16   MvdB   Notify upwards on timeout of data request acks
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 * 000.000.023  02-Aug-16   MvdB   Support EZSP_SET_MANUFACTURER_CODE
 * 002.002.039  17-Jan-17   MvdB   ARTF175875: Support binding primitives
 *****************************************************************************/

#include "zabNcpPrivate.h"
#include "zabNcpService.h"
#include "zabNcpAsh.h"

#include "ezsp-enum.h"
#include "ezsp-protocol.h"
#include "error.h"

#include "zabNcpEzspConfiguration.h"
#include "zabNcpEzspNetworking.h"
#include "zabNcpEzspSecurity.h"
#include "zabNcpEzspTrustCenter.h"
#include "zabNcpEzspMessaging.h"
#include "zabNcpEzspMfglib.h"
#include "zabNcpEzspUtilities.h"
#include "zabNcpEzspBinding.h"
#include "zabNcpEzsp.h"


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

#define EZSP_INFO( s )     (ZAB_SERVICE_VENDOR( s )->zabNcpEzspInfo)

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 * Lock the serial link
 ******************************************************************************/
static void setSerialLinkLocked(zabService* Service, zab_bool Locked, unsigned8 SequenceNumber, unsigned8 FrameId)
{
  EZSP_INFO(Service).linkLocked = Locked;

  EZSP_INFO(Service).sequenceNumber = SequenceNumber;
  EZSP_INFO(Service).frameId = FrameId;

  osTimeGetMilliseconds(Service, &EZSP_INFO(Service).timeoutTimeMs);
  EZSP_INFO(Service).timeoutTimeMs += ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS;
}

/******************************************************************************
 * Unlock the serial link
 ******************************************************************************/
static void setSerialLinkUnlocked(zabService* Service)
{
  EZSP_INFO(Service).linkLocked = zab_false;
}

/******************************************************************************
 * Unlock the serial link if parameters match last request
 ******************************************************************************/
static void unlockSerialLinkIfMatch(zabService* Service, unsigned8 SequenceNumber, unsigned8 FrameId)
{
  if ( (zabNcpEzsp_GetSerialLinkLocked(Service) == zab_true) &&
       (EZSP_INFO(Service).sequenceNumber == SequenceNumber) &&
       (EZSP_INFO(Service).frameId == FrameId) )
    {
      setSerialLinkUnlocked(Service);
    }
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise the EZSP service
 ******************************************************************************/
void zabNcpEzsp_InitService(erStatus* Status, zabService* Service)
{
  EZSP_INFO(Service).linkLocked = zab_false;
}

/******************************************************************************
 * Get status of serial link
 ******************************************************************************/
zab_bool zabNcpEzsp_GetSerialLinkLocked(zabService* Service)
{
  return EZSP_INFO(Service).linkLocked;
}

/******************************************************************************
 * Check for timeouts on EZSP responses
 ******************************************************************************/
unsigned32 zabNcpEzsp_UpdateTimeOut(erStatus* Status, zabService* Service, unsigned32 Time)
{
  unsigned32 timeoutRemaining = ZAB_WORK_DELAY_MAX_MS;

  if (zabNcpEzsp_GetSerialLinkLocked(Service) == zab_true)
    {
      timeoutRemaining = EZSP_INFO(Service).timeoutTimeMs - Time;
      if ( (timeoutRemaining == 0) || (timeoutRemaining > ZAB_VND_MS_TIMEOUT_MAX) )
        {
          timeoutRemaining = 0;

          printError(Service,  "zabNcpEzsp_UpdateTimeOut: UNLOCK after timeout! SeqNum = 0x%02X, FrameId = 0x%02X\n",
                     EZSP_INFO(Service).sequenceNumber,
                     EZSP_INFO(Service).frameId);
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_TIMEOUT);
          erStatusSet(Status, ZAB_ERROR_VENDOR_TIMEOUT);

          /* If it was a data request, then notify the failure upwards for fast timeouts! */
          if ( (EZSP_INFO(Service).frameId == EZSP_SEND_UNICAST) ||
               (EZSP_INFO(Service).frameId == EZSP_SEND_BROADCAST) ||
               (EZSP_INFO(Service).frameId == EZSP_SEND_MULTICAST) )
            {
              zabNcpEzspMessaging_AppErrorIn(Service, EZSP_INFO(Service).sequenceNumber, ZAB_ERROR_VENDOR_TIMEOUT);
            }

          setSerialLinkUnlocked(Service);
        }
    }

  return timeoutRemaining;
}

/******************************************************************************
 * Send an EZSP message
 *  BufferData must include EZSP header (EZSP_MIN_FRAME_LENGTH) plus any payload.
 *  EZSP Sequence number will be added internally
 ******************************************************************************/
void zabNcpEzsp_Send(erStatus* Status, zabService* Service, unsigned8 SequenceNumber, unsigned8 BufferLength, unsigned8* BufferData)
{
  ER_CHECK_STATUS(Status);

  if ( (BufferLength >= EZSP_MIN_FRAME_LENGTH) && (BufferData != NULL) )
    {
      if (zabNcpEzsp_GetSerialLinkLocked(Service) == zab_false)
        {
          BufferData[EZSP_SEQUENCE_INDEX] = SequenceNumber;

          zabNcpAsh_Data(Status, Service, BufferLength, BufferData, zab_false);
          if (erStatusIsOk(Status))
            {
              setSerialLinkLocked(Service, zab_true, SequenceNumber, BufferData[EZSP_FRAME_ID_INDEX]);
            }
        }
      else
        {
          printError(Service, "zabNcpEzsp_Send: Link locked!!!!\n");
          erStatusSet(Status, ZAB_ERROR_VENDOR_BUSY);
        }
    }
  else
    {
      printError(Service, "zabNcpEzsp_Send: Invalid Length = %d\n", BufferLength);
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process an incoming EZSP frame
 ******************************************************************************/
void zabNcpEzsp_ProcessIncomingFrame(erStatus* Status, zabService* Service, unsigned8 FrameLength, unsigned8* FrameData)
{
  unsigned8 index;
  unsigned8 responseFrameControl;
  unsigned8 payloadLength;
  unsigned8* payloadData;

  printVendor(Service,  "zabNcpEzsp_ProcessIncomingFrame:");
  for (index = 0; index < FrameLength; index++)
    {
      printVendor(Service,  " %02X", FrameData[index]);
    }
  printVendor(Service,  "\n");

  if ( (FrameLength < EZSP_MIN_FRAME_LENGTH) || (FrameLength > EZSP_MAX_FRAME_LENGTH) )
    {
      printError(Service,  "zabNcpEzsp_ProcessIncomingFrame: Invalid Length = %d\n", FrameLength);
      return;
    }

  /* Validate EZSP Frame Control */
  responseFrameControl = FrameData[EZSP_FRAME_CONTROL_INDEX];
  if ((responseFrameControl & EZSP_FRAME_CONTROL_DIRECTION_MASK) != EZSP_FRAME_CONTROL_RESPONSE)
    {
      printError(Service,  "zabNcpEzsp_ProcessIncomingFrame: Response frame control = 0x%02X: Wrong Direction\n", responseFrameControl);
      return;
    }
  if ((responseFrameControl & EZSP_FRAME_CONTROL_TRUNCATED_MASK) == EZSP_FRAME_CONTROL_TRUNCATED)
    {
      printError(Service,  "zabNcpEzsp_ProcessIncomingFrame: Response frame control = 0x%02X: Truncated\n", responseFrameControl);
      return;
    }
  if ((responseFrameControl & EZSP_FRAME_CONTROL_OVERFLOW_MASK) == EZSP_FRAME_CONTROL_OVERFLOW_MASK)
    {
      printError(Service,  "zabNcpEzsp_ProcessIncomingFrame: Response frame control = 0x%02X: Overflow\n", responseFrameControl);
      return;
    }
  if ( ((responseFrameControl & EZSP_FRAME_CONTROL_NETWORK_INDEX_MASK) >> EZSP_FRAME_CONTROL_NETWORK_INDEX_OFFSET) != 0)
    {
      printError(Service,  "zabNcpEzsp_ProcessIncomingFrame: Response frame control = 0x%02X: Non zero network index\n", responseFrameControl);
      return;
    }

  /* Unlock serial link if this is a synchronous callback */
  unlockSerialLinkIfMatch(Service, FrameData[EZSP_SEQUENCE_INDEX], FrameData[EZSP_FRAME_ID_INDEX]);

  /* Process by frame ID */
  payloadLength = FrameLength - EZSP_PARAMETERS_INDEX;
  payloadData = &FrameData[EZSP_PARAMETERS_INDEX];
  switch(FrameData[EZSP_FRAME_ID_INDEX])
    {
      case EZSP_VERSION:                        zabNcpEzspConfiguration_VersionResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_SET_POLICY:                     zabNcpEzspConfiguration_SetPolicyResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_GET_CONFIGURATION_VALUE:        zabNcpEzspConfiguration_GetConfigValueResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_SET_CONFIGURATION_VALUE:        zabNcpEzspConfiguration_SetConfigValueResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_GET_VALUE:
      case EZSP_GET_EXTENDED_VALUE:             zabNcpEzspConfiguration_GetValueResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_LAUNCH_STANDALONE_BOOTLOADER:   zabNcpEzspConfiguration_LaunchStandaloneBootloaderResponseHandler(Status, Service, payloadLength, payloadData); break;

      case EZSP_GET_MFG_TOKEN:                  zabNcpEzspUtilities_GetMfgTokenResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_SET_MFG_TOKEN:                  zabNcpEzspUtilities_SetMfgTokenResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_GET_EUI64:                      zabNcpEzspUtilities_GetNodeIeeeResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_GET_NODE_ID:                    zabNcpEzspUtilities_GetNodeIdResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_STACK_TOKEN_CHANGED_HANDLER:    zabNcpEzspUtilities_StackTokenChangedHandler(Status, Service, payloadLength, payloadData); break;

      case EZSP_NETWORK_INIT:                   zabNcpEzspNetworking_NetworkInitResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_START_SCAN:                     zabNcpEzspNetworking_StartScanResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_ENERGY_SCAN_RESULT_HANDLER:     zabNcpEzspNetworking_EnergyScanResultHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_NETWORK_FOUND_HANDLER:          zabNcpEzspNetworking_NetworkFoundResultHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_SCAN_COMPLETE_HANDLER:          zabNcpEzspNetworking_ScanCompleteHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_FORM_NETWORK:                   zabNcpEzspNetworking_NetworkFormResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_JOIN_NETWORK:                   zabNcpEzspNetworking_NetworkJoinResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_STACK_STATUS_HANDLER:           zabNcpEzspNetworking_StackStatusHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_GET_NETWORK_PARAMETERS:         zabNcpEzspNetworking_GetNetworkParametersResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_SET_RADIO_POWER:                zabNcpEzspNetworking_SetRadioPowerResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_PERMIT_JOINING:                 zabNcpEzspNetworking_PermitJoinResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_LEAVE_NETWORK:                  zabNcpEzspNetworking_LeaveResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_SET_MANUFACTURER_CODE:          zabNcpEzspNetworking_SetManufacturerCodeResponseHandler(Status, Service, payloadLength, payloadData); break;

      case EZSP_SET_INITIAL_SECURITY_STATE:     zabNcpEzspSecurity_SetInitialStateResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_GET_CURRENT_SECURITY_STATE:     zabNcpEzspSecurity_GetCurrentSecurityStateResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_GET_KEY:                        zabNcpEzspSecurity_GetKeyResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_ADD_TRANSIENT_LINK_KEY:         zabNcpEzspSecurity_AddTransientLinkKeyResponseHandler(Status, Service, payloadLength, payloadData); break;

      case EZSP_TRUST_CENTER_JOIN_HANDLER:      zabNcpEzspTrustCenter_JoinHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_BROADCAST_NEXT_NETWORK_KEY:     zabNcpEzspTrustCenter_BroadcastNextNetworkKeyResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_BROADCAST_NETWORK_KEY_SWITCH:   zabNcpEzspTrustCenter_BroadcastNextNetworkKeySwitchResponseHandler(Status, Service, payloadLength, payloadData); break;

      case EZSP_MAXIMUM_PAYLOAD_LENGTH:         zabNcpEzspMessaging_MaximumPayloadLengthResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_SEND_UNICAST:                   zabNcpEzspMessaging_SendUnicastResponseHandler(Status, Service, FrameData[EZSP_SEQUENCE_INDEX], payloadLength, payloadData); break;
      case EZSP_SEND_BROADCAST:                 zabNcpEzspMessaging_SendBroadcastResponseHandler(Status, Service, FrameData[EZSP_SEQUENCE_INDEX], payloadLength, payloadData); break;
      case EZSP_SEND_MULTICAST:                 zabNcpEzspMessaging_SendMulticastResponseHandler(Status, Service, FrameData[EZSP_SEQUENCE_INDEX], payloadLength, payloadData); break;
      case EZSP_INCOMING_MESSAGE_HANDLER:       zabNcpEzspMessaging_IncomingMessageHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_MESSAGE_SENT_HANDLER:           zabNcpEzspMessaging_MessageSentHandler(Status, Service, payloadLength, payloadData); break;

      case EZSP_CLEAR_BINDING_TABLE:            zabNcpEzspBinding_ClearAllResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_GET_BINDING:                    zabNcpEzspBinding_GetResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_REMOTE_SET_BINDING_HANDLER:     zabNcpEzspBinding_RemoteSetBindingHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_REMOTE_DELETE_BINDING_HANDLER:  zabNcpEzspBinding_RemoteDeleteBindingHandler(Status, Service, payloadLength, payloadData); break;

      case EZSP_MFGLIB_START:                   zabNcpEzspMfglib_StartResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_MFGLIB_END:                     zabNcpEzspMfglib_EndResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_MFGLIB_START_TONE:              zabNcpEzspMfglib_StartToneResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_MFGLIB_STOP_TONE:               zabNcpEzspMfglib_StopToneResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_MFGLIB_START_STREAM:            zabNcpEzspMfglib_StartStreamResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_MFGLIB_STOP_STREAM:             zabNcpEzspMfglib_StopStreamResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_MFGLIB_SEND_PACKET:             zabNcpEzspMfglib_SendPacketResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_MFGLIB_SET_CHANNEL:             zabNcpEzspMfglib_SetChannelResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_MFGLIB_GET_POWER:               zabNcpEzspMfglib_GetPowerResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_MFGLIB_SET_POWER:               zabNcpEzspMfglib_SetPowerResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_MFGLIB_RX_HANDLER:              zabNcpEzspMfglib_RxHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_GET_CTUNE:                      zabNcpEzspMfglib_GetCtuneResponseHandler(Status, Service, payloadLength, payloadData); break;
      case EZSP_SET_CTUNE:                      zabNcpEzspMfglib_SetCtuneResponseHandler(Status, Service, payloadLength, payloadData); break;

      // Frames suppressed to avoid excessive error printing
      case EZSP_INCOMING_ROUTE_RECORD_HANDLER:
      case EZSP_INCOMING_SENDER_EUI64_HANDLER:
      case EZSP_SWITCH_NETWORK_KEY_HANDLER:
        break;

      default:
        printError(Service,  "zabNcpEzsp_ProcessIncomingFrame: Unhandled EZSP Frame ID = 0x%02X\n", FrameData[EZSP_FRAME_ID_INDEX]);
    }
}


/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/