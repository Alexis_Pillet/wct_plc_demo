/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the messaging interface for the ZAB EZSP Vendor - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.018  26-Jul-16   MvdB   Support AF Multicasts
 * 000.000.019  26-Jul-16   MvdB   Notify upwards on timeout of data request acks
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/
#ifndef __ZAB_NCP_EZSP_MESSAGING_H__
#define __ZAB_NCP_EZSP_MESSAGING_H__

#include "ember-types.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Get Max Payload Length Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMessaging_MaximumPayloadLength(erStatus* Status, zabService* Service);
void zabNcpEzspMessaging_MaximumPayloadLengthResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Send Unicast Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMessaging_SendUnicast(erStatus* Status, zabService* Service,
                                     EmberOutgoingMessageType type,
                                     EmberNodeId indexOrDestination,
                                     EmberApsFrame *apsFrame,
                                     unsigned8 messageTag,
                                     unsigned8 messageLength,
                                     unsigned8 *messageContents);
void zabNcpEzspMessaging_SendUnicastResponseHandler(erStatus* Status, zabService* Service, unsigned8 TransactionId, unsigned8 PayloadLength, unsigned8* PayloadData);


/******************************************************************************
 * Send Broadcast Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMessaging_SendBroadcast(erStatus* Status, zabService* Service,
                                       EmberNodeId destination,
                                       EmberApsFrame *apsFrame,
                                       unsigned8 radius, // Zero is converted to EMBER_MAX_HOPS
                                       unsigned8 messageTag,
                                       unsigned8 messageLength,
                                       unsigned8 *messageContents);
void zabNcpEzspMessaging_SendBroadcastResponseHandler(erStatus* Status, zabService* Service, unsigned8 TransactionId, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Send Multicast Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMessaging_SendMulticast(erStatus* Status, zabService* Service,
                                       EmberApsFrame *apsFrame,
                                       unsigned8 hops, // Zero is converted to EMBER_MAX_HOPS
                                       unsigned8 nonmemberRadius, // A value of 7 or greater is treated as infinite
                                       unsigned8 messageTag,
                                       unsigned8 messageLength,
                                       unsigned8 *messageContents);
void zabNcpEzspMessaging_SendMulticastResponseHandler(erStatus* Status, zabService* Service, unsigned8 TransactionId, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Message Sent Handler
 ******************************************************************************/
void zabNcpEzspMessaging_MessageSentHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Incoming Message Handler
 ******************************************************************************/
void zabNcpEzspMessaging_IncomingMessageHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Create and send an Application Error Data SAP IN Message
 *
 * Note: This function deliberately does not use a status parameter, as it
 * is typically called upon detection of bad status, so using that status will
 * cause the error report to fail!
 * Error reports are "best effort" only. If this function fails then there
 * is no status returned and hence there will never be the opportunity to retry.
 ******************************************************************************/
void zabNcpEzspMessaging_AppErrorIn(zabService *Service, unsigned8 TransactionId, zabError Error);

#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/