
"""
network.network_frame.network_frame_generator
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to generate network frames

"""
from .network_frame_structure import *


class NetworkFrameGenerator:
    """Functions to generate network frames"""

    DEFAULT_ACK_OPTION = Network.OptionResponse.ENABLE
    DEFAULT_SOURCE_ADDRESS_OPTION = Network.OptionAddressSource.NOT_PRESENT
    DEFAULT_DESTINATION_ADDRESS_OPTION = Network.OptionAddressDestination.NOT_PRESENT
    DEFAULT_FRAGMENTATION_OPTION = Network.OptionFragmentation.NOT_PRESENT

    def __init__(self, network_process):
        """Save network process instance & default_option of the frames"""
        self._network_process = network_process
        self._default_option = self.DEFAULT_ACK_OPTION + (self.DEFAULT_SOURCE_ADDRESS_OPTION << 1) \
                                                       + (self.DEFAULT_DESTINATION_ADDRESS_OPTION << 2) \
                                                       + (self.DEFAULT_FRAGMENTATION_OPTION << 3)

    @property
    def default_option(self):
        return self._default_option

    @default_option.setter
    def default_option(self, default_option):
        self._default_option = default_option

    @staticmethod
    def set_option(option, mask, value_bit_option):
        """Allow to change the value of one bit of the option field

           Args:
               option: value of the option to change
               mask: mask to choose the bit to change
               value_bit_option: new value of the bit

           Returns:
               The new option
        """
        option &= ~mask          # Clear the bit indicated by the mask
        if value_bit_option:
            option |= mask       # If value_bit_option was True, set the bit indicated by the mask.
        return option            # Return the result, we're done.

    def send_data_frame(self, device_number, payload, option=None):
        """Allow to send a data network frame

           Args:
               device_number: device number of the WB
               payload: payload of the frame
               option: default value self._default_option
        """
        if option is None:
            option = self.default_option
        self._network_process.send_network_frame(Network.FrameControl.DATA, option, device_number, payload)
