/*******************************************************************************
  Filename    : discovery.c
  $Date       : 2013-10-22                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Mark van den Broeke                                           $:

  Description : This is the Discovery plugin for the SZL
 *
 * MODIFICATION HISTORY:
 *  ZAB Rev       Date     Author  Change Description
 * 00.00.04.00  04-Mar-14   MvdB   Upgrade to support multiple parallel endpoint discoveries.
 *              19-Mar-14   MvdB   Upgrade to read ModelId and SchneiderProductId parameters from basic cluster
 *              21-May-14   MvdB   artf57927: Add network address parameter to discover endpoints confirm callback
 * 00.00.07.00  30-Oct-14   MvdB   artf106710: Fix freeing of invalid pointers during failure
 * 00.00.07.02  12-Nov-14   MvdB   artf107459: Add retries during endpoint discovery
 * 01.100.06.00 10-Feb-15   MvdB   artf112481: ZAB Discovery sometimes returns a corrupted cluster list
 * 002.001.001  15-Jul-15   MvdB   ARTF136585: Add transaction IDs to over the air commands/responses
 * 002.001.002  15-Jul-15   MvdB   ARTF136585: Add transaction IDs to over the air ZDO commands/responses
 * 002.001.006  27-Jul-15   MvdB   ARTF138318: Improve plugin destroy
 * 002.002.006  18-Sep-15   MvdB   Tidy up null pointer checks in Callback_ZdoMgmtLqiResp()
 * 002.002.010  09-Oct-15   MvdB   ARTF150980: Support Service ID in memory allocation functions for WTB
 * 002.002.036  11-Jan-17   MvdB   ARTF167732: Upgrade discovery plugin to handle unknown IEEE/Nwk Addresses better
*******************************************************************************/

#include "zabCoreService.h"
#include "zabCorePrivate.h"
#include "szl_external.h"
#include "szl_plugin.h"

#include "discovery.h"

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/* Current Item of the Node state machine */
typedef szl_uint8 SZLPLUGIN_NODE_DISCOVERY_STATE_t;
typedef enum
{
  SZLPLUGIN_NODE_DISCOVERY_STATE_IDLE,
  SZLPLUGIN_NODE_DISCOVERY_STATE_LQI,
} ENUM_SZLPLUGIN_NODE_DISCOVERY_STATE_t;

/* Current Item of the Endpoint state machine */
typedef szl_uint8 SZLPLUGIN_ENDPOINT_DISCOVERY_STATE_t;
typedef enum
{
  SZLPLUGIN_ENDPOINT_DISCOVERY_STATE_IDLE,
  SZLPLUGIN_ENDPOINT_DISCOVERY_STATE_ACTIVE_EP,
  SZLPLUGIN_ENDPOINT_DISCOVERY_STATE_SIMPLE_DESC,
  SZLPLUGIN_ENDPOINT_DISCOVERY_STATE_MODEL_ID,
  SZLPLUGIN_ENDPOINT_DISCOVERY_STATE_PRODUCT_ID,
} ENUM_SZLPLUGIN_ENDPOINT_DISCOVERY_STATE_t;


typedef struct
{
  szl_uint16 NetworkAddress;
  ENUM_SZLPLUGIN_ENDPOINT_DISCOVERY_STATE_t CurrentEndpointState;
  SzlPlugin_Discovery_EndpointConfirm_CB_t EndpointConfirmCallback;
  SzlPlugin_Discovery_EndpointDiscovered_CB_t EndpointCallback;
  SzlPlugin_Discovery_EndpointInfo_t* EndpointInfo;
  szl_uint8 ActiveEndpointCount;
  szl_uint8* ActiveEndpointList;
  szl_uint8 CurrentEndpointIndex;
  szl_uint8 Retries;
  szl_uint8 RetryCount;
} Endpoint_Discovery_Params_t;


/* Parameter set for the plugin */
typedef struct
{
  szl_uint8 SourceEndpoint;
  SZLPLUGIN_NODE_DISCOVERY_STATE_t CurrentNodeState;
  unsigned16 NodeInfoTableSize;
  SzlPlugin_Discovery_NodeInfo_t* NodeInfoTable;
  SzlPlugin_Discovery_NodeConfirm_CB_t NodeDiscoveryConfirmCallback;
  SzlPlugin_Discovery_NodeDetected_CB_t NodeDetectedCallback;

  Endpoint_Discovery_Params_t* EpDiscoveryList[SZL_PLUGIN_DISCOVERY_M_MAX_CONCURRENT_DISCOVERIES];
  /* WARNING: If you add something here, be sure to think about its initialisation and destruction! */
}SzlPlugin_Discovery_Params_t;

/* Macros for quick access to parameters for the service */
#define EP_DISCOVERY_ENTRY( prms, e )   ((prms)->EpDiscoveryList[e])
#define EP_DISCOVERY_STATE( prms, e )   ((prms)->EpDiscoveryList[e]->CurrentEndpointState)
#define EP_CONFIRM_CALLBACK( prms, e )  ((prms)->EpDiscoveryList[e]->EndpointConfirmCallback)
#define EP_CALLBACK( prms, e )          ((prms)->EpDiscoveryList[e]->EndpointCallback)
#define EP_COUNT( prms, e )             ((prms)->EpDiscoveryList[e]->ActiveEndpointCount)
#define EP_LIST( prms, e )              ((prms)->EpDiscoveryList[e]->ActiveEndpointList)
#define EP_CURRENT_INDEX( prms, e )     ((prms)->EpDiscoveryList[e]->CurrentEndpointIndex)
#define EP_INFO( prms, e )              ((prms)->EpDiscoveryList[e]->EndpointInfo)
#define EP_NETWORK_ADDRESS( prms, e )   ((prms)->EpDiscoveryList[e]->NetworkAddress)
#define EP_RETRIES( prms, e )           ((prms)->EpDiscoveryList[e]->Retries)
#define EP_RETRY_COUNT( prms, e )       ((prms)->EpDiscoveryList[e]->RetryCount)

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/**
 * ZDO Device Announce Handler
 * Converts data and calls Node Detected Callback
 */
void ZdoDeviceAnnounceHandler (zabService* Service, SZL_ZdoDeviceAnnounceIndParams_t* Params)
{
  SzlPlugin_Discovery_NodeInfo_t nodeInfo;
  SzlPlugin_Discovery_Params_t * pluginParams = NULL;

  /* Validate inputs */
  if ( (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DISCOVERY, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return;
    }

  if (pluginParams->NodeDetectedCallback != NULL)
    {
      nodeInfo.IeeeAddress = Params->IeeeAddress;
      nodeInfo.NetworkAddress = Params->NwkAddress;
      nodeInfo.NodeStatus = SZLPLUGIN_DISCOVERY_NODESTATUS_UNKNOWN;
      if ((Params->DeviceType_Ffd == szl_true) || (Params->RxOnWhenIdle == szl_true) )
        {
          nodeInfo.NodeFunction = SZLPLUGIN_DISCOVERY_NODEFUNCTION_FULL;
        }
      else
        {
          nodeInfo.NodeFunction = SZLPLUGIN_DISCOVERY_NODEFUNCTION_REDUCED;
        }

      pluginParams->NodeDetectedCallback(Service, &nodeInfo);
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****        NODE DISCOVERY        *****
 *                      ******************************
 ******************************************************************************/

/**
 * Add information about a node into the Node Table
 * Parameters all have "unknown" values, which should not overwrite a known value.
 */
void AddToNodeInfoTable(zabService* Service,
                        SzlPlugin_Discovery_Params_t * pluginParams,
                        SZLPLUGIN_DISCOVERY_NODEFUNCTION_t NodeFunction,
                        szl_uint16 NetworkAddress,
                        szl_uint64 IeeeAddress,
                        SZLPLUGIN_DISCOVERY_NODESTATUS_t NodeStatus)
{
  szl_uint8 i;
  szl_bool cleanupRequired = szl_false;
  SzlPlugin_Discovery_NodeInfo_t* tempNode = NULL;

  /* Firstly search for the existing node, matching on short or long address*/
  for (i = 0; i < pluginParams->NodeInfoTableSize; i++)
    {
      if ( ( (NetworkAddress != SZL_ZDO_MGMT_LQI_NWK_ADDR_UNKNOWN) && (pluginParams->NodeInfoTable[i].NetworkAddress == NetworkAddress) ) ||
           ( (IeeeAddress != SZL_ZDO_MGMT_LQI_IEEE_ADDR_UNKNOWN) && (pluginParams->NodeInfoTable[i].IeeeAddress == IeeeAddress) ) )
        {
          if (tempNode == NULL)
            {
              tempNode = &pluginParams->NodeInfoTable[i];

              /* At this point tempNode points to the first entry for this node.
               * Accept new values if they are known.
               * There is a small risk of entries if we had an entry for the IEEE and another for the Nwk.
               * This is very unlikely to happen and considered acceptable for now. If it causes issues we can add a duplicate removal. */
              if (NetworkAddress != SZL_PLUGIN_DISCOVERY_NWK_ADDR_UNKNOWN)
                {
                  tempNode->NetworkAddress = NetworkAddress;
                }
              if (IeeeAddress != SZL_PLUGIN_DISCOVERY_IEEE_ADDR_UNKNOWN)
                {
                  tempNode->IeeeAddress = IeeeAddress;
                }
              if (NodeFunction != SZLPLUGIN_DISCOVERY_NODEFUNCTION_UNKNOWN)
                {
                  tempNode->NodeFunction = NodeFunction;
                }
              if (NodeStatus != SZLPLUGIN_DISCOVERY_NODESTATUS_UNKNOWN)
                {
                  tempNode->NodeStatus = NodeStatus;
                }
            }
          else
            {
              /* This is now a duplicate entry.
               * This can only have a happened if the new item has both IEEE and NWk address and we had an entry for each with the other unknown.
               * Save NodeFunction and NodeStatus from second entry if they are better tha nthe first.
               * Set second entry to defaults and we will clean out empty entries at the end */
              cleanupRequired = szl_true;

              if (tempNode->NodeFunction == SZLPLUGIN_DISCOVERY_NODEFUNCTION_UNKNOWN)
                {
                  tempNode->NodeFunction = pluginParams->NodeInfoTable[i].NodeFunction;
                }
              if (tempNode->NodeStatus == SZLPLUGIN_DISCOVERY_NODESTATUS_UNKNOWN)
                {
                  tempNode->NodeStatus = pluginParams->NodeInfoTable[i].NodeStatus;
                }

              pluginParams->NodeInfoTable[i].NetworkAddress = SZL_PLUGIN_DISCOVERY_NWK_ADDR_UNKNOWN;
              pluginParams->NodeInfoTable[i].IeeeAddress = SZL_PLUGIN_DISCOVERY_IEEE_ADDR_UNKNOWN;
              pluginParams->NodeInfoTable[i].NodeFunction = SZLPLUGIN_DISCOVERY_NODEFUNCTION_UNKNOWN;
              pluginParams->NodeInfoTable[i].NodeStatus = SZLPLUGIN_DISCOVERY_NODESTATUS_UNKNOWN;
            }
        }
    }

  /* If not found in the search, add it to the table */
  if (tempNode == NULL)
    {
      /* Allocate new table, one size bigger than before */
      tempNode = (SzlPlugin_Discovery_NodeInfo_t*)szl_mem_alloc(sizeof(SzlPlugin_Discovery_NodeInfo_t) * (pluginParams->NodeInfoTableSize + 1),
                                                                MALLOC_ID_CFG_DISC_NODE_INFO);
      if (tempNode == NULL)
        {
          printError(Service, "Disc: WARNING: Failed to add node to node table\n");
          return;
        }
      else
        {
          szl_memset(tempNode, 0, sizeof(SzlPlugin_Discovery_NodeInfo_t) * (pluginParams->NodeInfoTableSize + 1));

          /* Copy old table across to new table.
           * Free old table
           * Set pointer to new table
           * Leave tempNode pointing to new entry
           * Default new entry
           * Increment table size */
          if (pluginParams->NodeInfoTableSize > 0)
            {
              szl_memcpy(tempNode, pluginParams->NodeInfoTable, pluginParams->NodeInfoTableSize * sizeof(SzlPlugin_Discovery_NodeInfo_t));
            }
          szl_mem_free(pluginParams->NodeInfoTable);
          pluginParams->NodeInfoTable = tempNode;

          tempNode = &tempNode[pluginParams->NodeInfoTableSize];
          tempNode->NetworkAddress = NetworkAddress;
          tempNode->IeeeAddress = IeeeAddress;
          tempNode->NodeFunction = NodeFunction;
          tempNode->NodeStatus = NodeStatus;
          pluginParams->NodeInfoTableSize += 1;
        }
    }

  /* Perform cleanup if required */
  if (cleanupRequired == szl_true)
    {
      for (i = 0; i < pluginParams->NodeInfoTableSize; i++)
        {
          if ( (pluginParams->NodeInfoTable[i].NetworkAddress == SZL_PLUGIN_DISCOVERY_NWK_ADDR_UNKNOWN) &&
               (pluginParams->NodeInfoTable[i].IeeeAddress == SZL_PLUGIN_DISCOVERY_IEEE_ADDR_UNKNOWN) )
            {
              /* If this is not the only or last entry, copy in the last entry*/
              if ( (pluginParams->NodeInfoTableSize > 1) && (i < (pluginParams->NodeInfoTableSize-1) ) )
                {
                  szl_memcpy(&pluginParams->NodeInfoTable[i], &pluginParams->NodeInfoTable[pluginParams->NodeInfoTableSize-1], sizeof(SzlPlugin_Discovery_NodeInfo_t));
                }

              /* Decrement the size of the list */
              pluginParams->NodeInfoTableSize--;

              /* Step back in the loop to check this entry we move is not itself empty.
               * This is a bit funky, but will work:
               *  - If i is 0 it will wrap negative, but then the for will inc it back to 0.
               *  - If i was pluginParams->NodeInfoTableSize, then i will be decremented then incremented but
               *    pluginParams->NodeInfoTableSize is smaller, so loop will not run again*/
              i--;
            }
        }
    }
}

/**
 * Find the next node to discover.
 * If no more nodes return NULL
 */
SzlPlugin_Discovery_NodeInfo_t* FindNextNodeToDiscover(zabService* Service,
                                                       SzlPlugin_Discovery_Params_t * pluginParams)
{
  szl_uint8 i;
  SzlPlugin_Discovery_NodeInfo_t* tempNode = NULL;

  /* Search the table
   * Look for nodes that have not already been discovered and for which we have a network address
   * If we find one and it is full function, then return pointer to it
   * Otherwise, if no full function, return a pointer to any Unknown node
   * Otherwise return null*/
  for (i = 0; i < pluginParams->NodeInfoTableSize; i++)
    {
      if ( (pluginParams->NodeInfoTable[i].NodeStatus == SZLPLUGIN_DISCOVERY_NODESTATUS_UNKNOWN) &&
           (pluginParams->NodeInfoTable[i].NetworkAddress != SZL_ZDO_MGMT_LQI_NWK_ADDR_UNKNOWN) )
        {
          if (pluginParams->NodeInfoTable[i].NodeFunction == SZLPLUGIN_DISCOVERY_NODEFUNCTION_FULL)
            {
              return &pluginParams->NodeInfoTable[i];
            }
          if (pluginParams->NodeInfoTable[i].NodeFunction == SZLPLUGIN_DISCOVERY_NODEFUNCTION_UNKNOWN)
            {
              tempNode = &pluginParams->NodeInfoTable[i];
            }
        }
    }

  return tempNode;
}

/**
 * Node discovery complete.
 * Set state machine idle and call confirm handler
 */
static void NodeDiscoveryComplete(zabService* Service,
                                  SzlPlugin_Discovery_Params_t * pluginParams,
                                  SZL_STATUS_t Status)
{
  if (pluginParams != NULL)
    {
      pluginParams->CurrentNodeState = SZLPLUGIN_NODE_DISCOVERY_STATE_IDLE;
      if (pluginParams->NodeDiscoveryConfirmCallback != NULL)
        {
          pluginParams->NodeDiscoveryConfirmCallback(Service,
                                                     Status,
                                                     pluginParams->NodeInfoTableSize,
                                                     pluginParams->NodeInfoTable);

          // Note - I am not going to free the table for now, but keep it as it may be useful. Confirm if this is a good idea or not!
        }
    }
}

/**
 * Mgmt Lqi Response handler
 */
static void Callback_ZdoMgmtLqiResp(zabService* Service, SZL_STATUS_t Status, SZL_ZdoMgmtLqiRespParams_t* Params, szl_uint8 TransactionId)
{
  szl_uint8 i;
  SZLPLUGIN_DISCOVERY_NODEFUNCTION_t NodeFunction;
  SZL_ZdoMgmtLqiReqParams_t lqiParams;
  SZL_RESULT_t result;
  SzlPlugin_Discovery_NodeInfo_t* tempNode;
  szl_bool startNextNode = szl_false;
  SzlPlugin_Discovery_Params_t * pluginParams = NULL;



  /* Get plugin data for the service */
  if ( (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DISCOVERY, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return;
    }

  /* Status will be success if we got a response from the node */
  if ( (Status == SZL_RESULT_SUCCESS) && (Params != NULL) )
    {

      /* Parse each node in the list and add it's info to the Node Info Table */
      for (i = 0; i < Params->NeighborTableListCount; i++)
        {
          /* Determine NodeFunction based on device type and RxOnWhenIdle */
          switch (Params->NeighborTableList[i].DeviceType)
            {
              case SZL_ZDO_MGMT_LQI_DEVICE_COORDINATOR:
              case SZL_ZDO_MGMT_LQI_DEVICE_ROUTER:
                NodeFunction = SZLPLUGIN_DISCOVERY_NODEFUNCTION_FULL;
                break;

              case SZL_ZDO_MGMT_LQI_DEVICE_END_DEVICE:
                switch (Params->NeighborTableList[i].RxOnWhenIdle)
                  {
                    case SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_FALSE:
                      NodeFunction = SZLPLUGIN_DISCOVERY_NODEFUNCTION_REDUCED;
                      break;

                    case SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_TRUE:
                      NodeFunction = SZLPLUGIN_DISCOVERY_NODEFUNCTION_FULL;
                      break;

                    default:
                      NodeFunction = SZLPLUGIN_DISCOVERY_NODEFUNCTION_UNKNOWN;
                      break;
                  }
                break;

              default:
                NodeFunction = SZLPLUGIN_DISCOVERY_NODEFUNCTION_UNKNOWN;
                break;
            }

          AddToNodeInfoTable(Service,
                             pluginParams,
                             NodeFunction,
                             Params->NeighborTableList[i].NetworkAddress,
                             Params->NeighborTableList[i].Ieee,
                             SZLPLUGIN_DISCOVERY_NODESTATUS_UNKNOWN);
        }

      /* Check out the number of neighbour table entries, the start index and the list count and work out
       * if there are more entries to be read from the node.*/
      if ( (Params->StartIndex + Params->NeighborTableListCount) < Params->NeighborTableEntries)
        {
          lqiParams.NetworkAddress = Params->NetworkAddress;
          lqiParams.StartIndex = Params->StartIndex + Params->NeighborTableListCount;

          result = SZL_ZDO_MgmtLqiReq(Service, Callback_ZdoMgmtLqiResp,&lqiParams, NULL);
          if (result != SZL_RESULT_SUCCESS)
            {
              // Fail
              NodeDiscoveryComplete(Service, pluginParams, SZL_STATUS_FAILED);
              return;
            }
        }
      /* If there are no more entries to read, make sure this node is in the table with CommsConfirmed szl_true
       * Then move onto next undiscovered node */
      else
        {
          AddToNodeInfoTable(Service,
                             pluginParams,
                             SZLPLUGIN_DISCOVERY_NODEFUNCTION_FULL, // It must be full as it responding to Mgmt Lqi
                             Params->NetworkAddress,
                             SZL_ZDO_MGMT_LQI_IEEE_ADDR_UNKNOWN,
                             SZLPLUGIN_DISCOVERY_NODESTATUS_COMMS_OK);

          startNextNode = szl_true;
        }
    }
  else if (Params != NULL)
    {
      /* This is a timeout, which will happen for any powered off or SED note we hit.
       * Ensure the node is in the table, with comms status bad!
       * Then move to the next node */
      AddToNodeInfoTable(Service,
                         pluginParams,
                         SZLPLUGIN_DISCOVERY_NODEFUNCTION_UNKNOWN,
                         Params->NetworkAddress,
                         SZL_ZDO_MGMT_LQI_IEEE_ADDR_UNKNOWN,
                         SZLPLUGIN_DISCOVERY_NODESTATUS_COMMS_FAILED);

      startNextNode = szl_true;
    }
  else
    {
      startNextNode = szl_true;
    }

  /* Move on to next node */
  if (startNextNode == szl_true)
    {
      tempNode = FindNextNodeToDiscover(Service, pluginParams);

      if (tempNode == NULL)
        {
          /* Complete! */
          NodeDiscoveryComplete(Service, pluginParams, SZL_RESULT_SUCCESS);
        }
      else
        {
          lqiParams.NetworkAddress = tempNode->NetworkAddress;
          lqiParams.StartIndex = 0;

          result = SZL_ZDO_MgmtLqiReq(Service, Callback_ZdoMgmtLqiResp, &lqiParams, NULL);
          if (result != SZL_RESULT_SUCCESS)
            {
              // Fail
              NodeDiscoveryComplete(Service, pluginParams, SZL_RESULT_FAILED);
            }
        }
    }
}

/**
 * Discover Nodes in the Pro Network, starting at StartNetworkAddress.
 * Callback will be called upon compeltion with a status adn a list of nodes discovered
 */
SZL_RESULT_t SzlPlugin_Discovery_DiscoverNodes(zabService* Service, SzlPlugin_Discovery_NodeConfirm_CB_t Callback, szl_uint16 StartNetworkAddress)
{
  SZL_ZdoMgmtLqiReqParams_t lqiParams;
  SZL_RESULT_t result;
  SzlPlugin_Discovery_Params_t * pluginParams = NULL;

  /* Validate params and state */
  if (Callback == NULL)
    {
      return SZL_RESULT_INVALID_DATA;
    }
  if ( (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DISCOVERY, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      // Fail
      return SZL_RESULT_LIBRARY_NOT_INITIALIZED;
    }
  if (pluginParams->CurrentNodeState != SZLPLUGIN_NODE_DISCOVERY_STATE_IDLE)
    {
      return SZL_RESULT_OPERATION_NOT_POSSIBLE;
    }

  /* Initialise state machine, being sure to free any old table */
  pluginParams->NodeDiscoveryConfirmCallback = Callback;
  pluginParams->NodeInfoTableSize = 0;
  szl_mem_free(pluginParams->NodeInfoTable);
  pluginParams->NodeInfoTable = NULL;

  /* Start with Lqi req to the specified node, index 0 */
  lqiParams.NetworkAddress = StartNetworkAddress;
  lqiParams.StartIndex = 0;
  result = SZL_ZDO_MgmtLqiReq(Service, Callback_ZdoMgmtLqiResp, &lqiParams, NULL);
  if (result == SZL_RESULT_SUCCESS)
    {
      pluginParams->CurrentNodeState = SZLPLUGIN_NODE_DISCOVERY_STATE_LQI;
    }
  return result;
}

/******************************************************************************
 *                      ******************************
 *                 *****      ENDPOINT DISCOVERY      *****
 *                      ******************************
 ******************************************************************************/
/* Cluster ID */
#define CLUSTER_BASIC 0x0000

/* Basic Cluster Attribute IDs */
#define ATTRIBUTE_MODEL_ID 0x0005
#define ATTRIBUTE_MS_PRODUCT_ID 0xE007

static void ProductIdentifierRespRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_AttributeReadRespParams_t *Params, szl_uint8 TransactionId);
static void ModelIdRespRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_AttributeReadRespParams_t *Params, szl_uint8 TransactionId);
static void SimpleDescRespHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoSimpleDescriptorRespParams_t* Params, szl_uint8 TransactionId);
static void ActiveEpRespHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoActiveEndpointRespParams_t* Params, szl_uint8 TransactionId);

/**
 * Clear endpoint discovery for a single node
 * Deallocate data and set state machine idle
 */
static void ClearEndpointDiscoverySingle(zabService* Service, SzlPlugin_Discovery_Params_t* pluginParams, szl_uint8 epIndex)
{
  if (EP_DISCOVERY_ENTRY(pluginParams, epIndex) != NULL)
    {
      szl_mem_free(EP_INFO(pluginParams, epIndex));
      EP_INFO(pluginParams, epIndex) = NULL;
      szl_mem_free(EP_LIST(pluginParams, epIndex));
      EP_LIST(pluginParams, epIndex) = NULL;
      szl_mem_free(EP_DISCOVERY_ENTRY(pluginParams, epIndex));
      EP_DISCOVERY_ENTRY(pluginParams, epIndex) = NULL;
    }
}

/**
 * Clear endpoint discovery for all nodes
 * Deallocate data and set state machine idle
 */
static void ClearEndpointDiscoveryAll(zabService* Service, SzlPlugin_Discovery_Params_t* pluginParams)
{
  szl_uint8 epIndex;

  for (epIndex = 0; epIndex < SZL_PLUGIN_DISCOVERY_M_MAX_CONCURRENT_DISCOVERIES; epIndex++)
    {
      ClearEndpointDiscoverySingle(Service, pluginParams, epIndex);
    }
}

/**
 * Complete endpoint discovery
 * Calls the confirm callback and tidies up
 */
static void EndEndpointDiscovery(zabService* Service, SzlPlugin_Discovery_Params_t* pluginParams, szl_uint8 epIndex, SZL_STATUS_t Status)
{
  szl_uint16 nwkAddress;

  if (EP_CONFIRM_CALLBACK(pluginParams, epIndex) != NULL)
    {
        nwkAddress = EP_NETWORK_ADDRESS(pluginParams, epIndex);
      (EP_CONFIRM_CALLBACK(pluginParams, epIndex))(Service, Status, nwkAddress);
    }
  ClearEndpointDiscoverySingle(Service, pluginParams, epIndex);
}


/**
 * Move to the next endpoint to be discovered
 */
static void NextEndpoint(zabService* Service, SzlPlugin_Discovery_Params_t* pluginParams, szl_uint8 epIndex)
{
  SZL_RESULT_t result = SZL_RESULT_FAILED;
  SZL_ZdoSimpleDescriptorReqParams_t SimpleDescParams;

  /* Firstly, report on this endpoint */
  if (EP_CALLBACK(pluginParams, epIndex) != NULL)
    {
      (EP_CALLBACK(pluginParams, epIndex))(Service, EP_INFO(pluginParams, epIndex));
    }

  /* Simple Desc Response is going to re-allocate EP_INFO, so free it now we are done with this endpoint */
  szl_mem_free(EP_INFO(pluginParams, epIndex));
  EP_INFO(pluginParams, epIndex) = NULL;

  /* Go to the next endpoint in the list and start its discovery*/
  EP_CURRENT_INDEX(pluginParams, epIndex)++;
  if (EP_CURRENT_INDEX(pluginParams, epIndex) < EP_COUNT(pluginParams, epIndex))
    {
      SimpleDescParams.NetworkAddress = EP_NETWORK_ADDRESS(pluginParams, epIndex);
      SimpleDescParams.NetworkAddressOfInterest = EP_NETWORK_ADDRESS(pluginParams, epIndex);
      SimpleDescParams.Endpoint = EP_LIST(pluginParams, epIndex)[EP_CURRENT_INDEX(pluginParams, epIndex)];
      result = SZL_ZDO_SimpleDescriptorReq(Service, SimpleDescRespHandler, &SimpleDescParams, NULL);
      if (result != SZL_RESULT_SUCCESS)
        {
          EndEndpointDiscovery(Service, pluginParams, epIndex, SZL_RESULT_FAILED);
        }
    }
  /* If no more nodes, we are complete */
  else
    {
      EndEndpointDiscovery(Service, pluginParams, epIndex, SZL_STATUS_SUCCESS);
    }
}


/**
 * Read Basic Cluster (MS) Product Id
 */
static void readBasicClusterProductIdReq(zabService* Service, SzlPlugin_Discovery_Params_t* pluginParams, szl_uint8 epIndex)
{
  SZL_RESULT_t result = SZL_RESULT_NOT_FOUND;
  SZL_AttributeReadReqParams_t* ReadAttrParams;

  if (EP_RETRY_COUNT(pluginParams, epIndex) < EP_RETRIES(pluginParams, epIndex))
    {
      EP_RETRY_COUNT(pluginParams, epIndex)++;

      printInfo(Service, "Discovery: readBasicClusterProductIdReq(0x%04X:%02X) Attempt %d of %d %s\n",
               EP_NETWORK_ADDRESS(pluginParams, epIndex),
               EP_LIST(pluginParams, epIndex)[EP_CURRENT_INDEX(pluginParams, epIndex)],
               EP_RETRY_COUNT(pluginParams, epIndex),
               EP_RETRIES(pluginParams, epIndex),
               (EP_RETRY_COUNT(pluginParams, epIndex) > 1) ? "- RETRY##############" : "");

      /* Now read Model Id */
      /* Note: This is a bit messy for now as the SZL_AttributeReadReqParams_t is variable length and needs space allocated for each attribute.
       *       It would be prettier to just do this on the stack, but to do that we need to change the struct definition */
      ReadAttrParams = (SZL_AttributeReadReqParams_t*)szl_mem_alloc(SZL_AttributeReadReqParams_t_SIZE(1),
                                                                    MALLOC_ID_PLG_DISC_READ_ATTR);
      if (ReadAttrParams != NULL)
        {
          ReadAttrParams->DestAddrMode.AddressMode = SZL_ADDRESS_MODE_NWK_ADDRESS;
          ReadAttrParams->DestAddrMode.Addresses.Nwk.Address = EP_NETWORK_ADDRESS(pluginParams, epIndex);
          ReadAttrParams->DestAddrMode.Addresses.Nwk.Endpoint = EP_LIST(pluginParams, epIndex)[EP_CURRENT_INDEX(pluginParams, epIndex)];
          ReadAttrParams->SourceEndpoint = pluginParams->SourceEndpoint;
          ReadAttrParams->ManufacturerSpecific = szl_true;
          ReadAttrParams->ClusterID = CLUSTER_BASIC;
          ReadAttrParams->NumberOfAttributes = 1;
          ReadAttrParams->AttributeIDs[0] = ATTRIBUTE_MS_PRODUCT_ID;

          result = SZL_AttributeReadReq(Service, ProductIdentifierRespRspHandler, ReadAttrParams, NULL);
          szl_mem_free(ReadAttrParams);
        }
      /* For any errors, set result bad and tidy up at the end */
      else
        {
          result = SZL_RESULT_INTERNAL_LIB_ERROR;
        }

      if (result == SZL_RESULT_SUCCESS)
        {
          EP_DISCOVERY_STATE(pluginParams, epIndex) = SZLPLUGIN_ENDPOINT_DISCOVERY_STATE_PRODUCT_ID;
        }
      else
        {
          EndEndpointDiscovery(Service, pluginParams, epIndex, result);
        }
    }
  else
    {
      EndEndpointDiscovery(Service, pluginParams, epIndex, result);
    }
}


/**
 * Read Basic Cluster Model Id
 */
static void readBasicClusterModelIdReq(zabService* Service, SzlPlugin_Discovery_Params_t* pluginParams, szl_uint8 epIndex)
{
  SZL_RESULT_t result = SZL_RESULT_NOT_FOUND;
  SZL_AttributeReadReqParams_t* ReadAttrParams;

  if (EP_RETRY_COUNT(pluginParams, epIndex) < EP_RETRIES(pluginParams, epIndex))
    {
      EP_RETRY_COUNT(pluginParams, epIndex)++;

      printInfo(Service, "Discovery: readBasicClusterModelIdReq(0x%04X:%02X) Attempt %d of %d %s\n",
               EP_NETWORK_ADDRESS(pluginParams, epIndex),
               EP_LIST(pluginParams, epIndex)[EP_CURRENT_INDEX(pluginParams, epIndex)],
               EP_RETRY_COUNT(pluginParams, epIndex),
               EP_RETRIES(pluginParams, epIndex),
               (EP_RETRY_COUNT(pluginParams, epIndex) > 1) ? "- RETRY##############" : "");

      /* Now read Model Id */
      /* Note: This is a bit messy for now as the SZL_AttributeReadReqParams_t is variable length and needs space allocated for each attribute.
       *       It would be prettier to just do this on the stack, but to do that we need to change the struct definition */
      ReadAttrParams = (SZL_AttributeReadReqParams_t*)szl_mem_alloc(SZL_AttributeReadReqParams_t_SIZE(1),
                                                                    MALLOC_ID_PLG_DISC_READ_ATTR);
      if (ReadAttrParams != NULL)
        {
          ReadAttrParams->DestAddrMode.AddressMode = SZL_ADDRESS_MODE_NWK_ADDRESS;
          ReadAttrParams->DestAddrMode.Addresses.Nwk.Address = EP_NETWORK_ADDRESS(pluginParams, epIndex);
          ReadAttrParams->DestAddrMode.Addresses.Nwk.Endpoint = EP_LIST(pluginParams, epIndex)[EP_CURRENT_INDEX(pluginParams, epIndex)]; //Params->Endpoint;
          ReadAttrParams->SourceEndpoint = pluginParams->SourceEndpoint;
          ReadAttrParams->ManufacturerSpecific = szl_false;
          ReadAttrParams->ClusterID = CLUSTER_BASIC;
          ReadAttrParams->NumberOfAttributes = 1;
          ReadAttrParams->AttributeIDs[0] = ATTRIBUTE_MODEL_ID;

          result = SZL_AttributeReadReq(Service, ModelIdRespRspHandler, ReadAttrParams, NULL);
          szl_mem_free(ReadAttrParams);
        }
      /* For any errors, set result bad and tidy up at the end */
      else
        {
          result = SZL_RESULT_INTERNAL_LIB_ERROR;
        }

      if (result == SZL_RESULT_SUCCESS)
        {
          EP_DISCOVERY_STATE(pluginParams, epIndex) = SZLPLUGIN_ENDPOINT_DISCOVERY_STATE_MODEL_ID;
        }
      else
        {
          EndEndpointDiscovery(Service, pluginParams, epIndex, result);
        }
    }
  else
    {
      EndEndpointDiscovery(Service, pluginParams, epIndex, result);
    }
}


/**
 * Read Simple Descriptor
 */
static void simpleDescReq(zabService* Service, SzlPlugin_Discovery_Params_t* pluginParams, szl_uint8 epIndex)
{
  SZL_RESULT_t result = SZL_RESULT_NOT_FOUND;
  SZL_ZdoSimpleDescriptorReqParams_t SimpleDescParams;

  if (EP_RETRY_COUNT(pluginParams, epIndex) < EP_RETRIES(pluginParams, epIndex))
    {
      EP_RETRY_COUNT(pluginParams, epIndex)++;

      printInfo(Service, "Discovery: simpleDescReq(0x%04X:%02X) Attempt %d of %d %s\n",
                EP_NETWORK_ADDRESS(pluginParams, epIndex),
                EP_LIST(pluginParams, epIndex)[EP_CURRENT_INDEX(pluginParams, epIndex)],
                EP_RETRY_COUNT(pluginParams, epIndex),
                EP_RETRIES(pluginParams, epIndex),
               (EP_RETRY_COUNT(pluginParams, epIndex) > 1) ? "- RETRY##############" : "");

      SimpleDescParams.NetworkAddress = EP_NETWORK_ADDRESS(pluginParams, epIndex);
      SimpleDescParams.NetworkAddressOfInterest = EP_NETWORK_ADDRESS(pluginParams, epIndex);
      SimpleDescParams.Endpoint = EP_LIST(pluginParams, epIndex)[EP_CURRENT_INDEX(pluginParams, epIndex)];

      result = SZL_ZDO_SimpleDescriptorReq(Service, SimpleDescRespHandler, &SimpleDescParams, NULL);
      if (result == SZL_RESULT_SUCCESS)
        {
          EP_DISCOVERY_STATE(pluginParams, epIndex) = SZLPLUGIN_ENDPOINT_DISCOVERY_STATE_SIMPLE_DESC;
        }
      else
        {
          EndEndpointDiscovery(Service, pluginParams, epIndex, result);
        }
    }
  else
    {
      EndEndpointDiscovery(Service, pluginParams, epIndex, result);
    }
}


/**
 * Read Active Endpoints
 */
static SZL_RESULT_t activeEpReq(zabService* Service, SzlPlugin_Discovery_Params_t* pluginParams, szl_uint8 epIndex)
{
  SZL_RESULT_t result = SZL_RESULT_NOT_FOUND;
  SZL_ZdoActiveEndpointReqParams_t params;

  if (EP_RETRY_COUNT(pluginParams, epIndex) < EP_RETRIES(pluginParams, epIndex))
    {
      EP_RETRY_COUNT(pluginParams, epIndex)++;

      printInfo(Service, "Discovery: activeEpReq(0x%04X) Attempt %d of %d %s\n",
               EP_NETWORK_ADDRESS(pluginParams, epIndex),
               EP_RETRY_COUNT(pluginParams, epIndex),
               EP_RETRIES(pluginParams, epIndex),
               (EP_RETRY_COUNT(pluginParams, epIndex) > 1) ? "- RETRY##############" : "");

      params.NetworkAddress = EP_NETWORK_ADDRESS(pluginParams, epIndex);
      params.NetworkAddressOfInterest = EP_NETWORK_ADDRESS(pluginParams, epIndex);

      result = SZL_ZDO_ActiveEndpointReq(Service, ActiveEpRespHandler, &params, NULL);
      if (result == SZL_RESULT_SUCCESS)
        {
          EP_DISCOVERY_STATE(pluginParams, epIndex) = SZLPLUGIN_ENDPOINT_DISCOVERY_STATE_ACTIVE_EP;
        }
      else
        {
          /* Small hack - on the first time we will return failure and not start discovery so we just clear the state machine.
           *              on retries we can no longer return the original call, so must call the callback */
          if (EP_RETRY_COUNT(pluginParams, epIndex) == 1)
            {
              ClearEndpointDiscoverySingle(Service, pluginParams, epIndex);
            }
          else
            {
              EndEndpointDiscovery(Service, pluginParams, epIndex, result);
            }
        }
    }
  else
    {
      EndEndpointDiscovery(Service, pluginParams, epIndex, result);
    }
  return result;
}


/**
 * Product Id response handler
 * Stores the model Id of the endpoint, then calls the endpoint callback to report
 * the discovered info to the app.
 */
static void ProductIdentifierRespRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_AttributeReadRespParams_t *Params, szl_uint8 TransactionId)
{
  szl_uint8 epIndex;
  SzlPlugin_Discovery_Params_t * pluginParams = NULL;

  /* Validate inputs */
  if ( (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DISCOVERY, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return;
    }

  for (epIndex = 0; epIndex < SZL_PLUGIN_DISCOVERY_M_MAX_CONCURRENT_DISCOVERIES; epIndex++)
    {
      if (EP_DISCOVERY_ENTRY(pluginParams, epIndex) != NULL)
        {
          if ( (EP_DISCOVERY_ENTRY(pluginParams, epIndex)->NetworkAddress == Params->SourceAddress.Addresses.Nwk.Address) &&
               (EP_LIST(pluginParams, epIndex)[EP_CURRENT_INDEX(pluginParams, epIndex)]) == Params->SourceAddress.Addresses.Nwk.Endpoint)
            {
              if (Status == SZL_RESULT_SUCCESS)
                {
                  /* Check we got a response for the attribute we wanted */
                  if ( (Params->ClusterID == 0) &&
                       (Params->ManufacturerSpecific == szl_true) &&
                       (Params->NumberOfAttributes > 0) )
                    {
                      /* If its the right attribute and status is ok, copy out the data respecting max length.
                       * Discovery is then complete for the endpoint, so move to the next one */
                      if (Params->Attributes[0].AttributeID == 0xE007)
                        {
                          if ( (Params->Attributes[0].Status == SZL_STATUS_SUCCESS) &&
                               (Params->Attributes[0].DataLength == sizeof(szl_uint16)) )
                            {
                              EP_INFO(pluginParams, epIndex)->ProductIdentifier = *(szl_uint16*)Params->Attributes[0].Data;
                            }
                          /* Else just leave data defaulted, attribute is most likely unsupported */
                        }
                      /* It's the wrong attribute - something is very wrong, end. */
                      else
                        {
                          EndEndpointDiscovery(Service, pluginParams, epIndex, SZL_RESULT_INVALID_DATA);
                          return;
                        }
                    }
                  /* If any of the checks failed, leave it as a null string and move on */
                  NextEndpoint(Service, pluginParams, epIndex);
                }
              /* We got a default response saying the MS command, or the attribute are not supported.
               * Leave data as default and go to the next endpoint  */
              else if ( (Status == SZL_STATUS_UNSUP_MANU_GENERAL_COMMAND) ||
                        (Status == SZL_STATUS_UNSUPPORTED_ATTRIBUTE) )
                {
                  NextEndpoint(Service, pluginParams, epIndex);
                }
              /* No response timeout. Give up quickly as the node has stopped responding for some reason */
              else
                {
                  readBasicClusterProductIdReq(Service, pluginParams, epIndex);
                }
              return;
            }
        }
    }
}

/**
 * Model Id response handler
 * Stores the model Id of the endpoint, then calls the endpoint callback to report
 * the discovered info to the app.
 */
static void ModelIdRespRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_AttributeReadRespParams_t *Params, szl_uint8 TransactionId)
{
  szl_uint8 epIndex;
  SzlPlugin_Discovery_Params_t * pluginParams = NULL;

  /* Validate inputs */
  if ( (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DISCOVERY, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return;
    }

  for (epIndex = 0; epIndex < SZL_PLUGIN_DISCOVERY_M_MAX_CONCURRENT_DISCOVERIES; epIndex++)
    {
      if (EP_DISCOVERY_ENTRY(pluginParams, epIndex) != NULL)
        {
          if ( (EP_DISCOVERY_ENTRY(pluginParams, epIndex)->NetworkAddress == Params->SourceAddress.Addresses.Nwk.Address) &&
               (EP_LIST(pluginParams, epIndex)[EP_CURRENT_INDEX(pluginParams, epIndex)] == Params->SourceAddress.Addresses.Nwk.Endpoint) )
            {
              if ( (Status == SZL_RESULT_SUCCESS) ||
                   (Status == SZL_STATUS_UNSUP_GENERAL_COMMAND) ||
                   (Status == SZL_STATUS_UNSUPPORTED_ATTRIBUTE) )
                {
                  /* Check we got a response for the attribute we wanted */
                  if ( (Status == SZL_RESULT_SUCCESS) &&
                       (Params->ClusterID == 0) &&
                       (Params->ManufacturerSpecific == szl_false) &&
                       (Params->NumberOfAttributes > 0) )
                    {
                      /* If its the right attribute and status is ok, copy out the data respecting max length.
                       * If status is bad (unsupported etc.), just move on to next item */
                      if (Params->Attributes[0].AttributeID == ATTRIBUTE_MODEL_ID)
                        {
                          if (Params->Attributes[0].Status == SZL_STATUS_SUCCESS)
                            {
                              if (Params->Attributes[0].DataLength <= M_MAX_MODEL_ID_LENGTH)
                                {
                                  szl_memcpy(EP_INFO(pluginParams, epIndex)->ModelId, Params->Attributes[0].Data, Params->Attributes[0].DataLength);
                                  EP_INFO(pluginParams, epIndex)->ModelId[Params->Attributes[0].DataLength] = 0;
                                }
                              else
                                {
                                  szl_memcpy(EP_INFO(pluginParams, epIndex)->ModelId, Params->Attributes[0].Data, M_MAX_MODEL_ID_LENGTH);
                                  EP_INFO(pluginParams, epIndex)->ModelId[M_MAX_MODEL_ID_LENGTH] = 0;
                                }
                            }
                        }
                      /* It's the wrong attribute - something is very wrong, end. */
                      else
                        {
                          EndEndpointDiscovery(Service, pluginParams, epIndex, SZL_RESULT_INVALID_DATA);
                          return;
                        }
                    }
                  /* Else it was a failure due to basic cluster not supported (strange!) or ModelId unsupported (this is optional in ZigBee)
                   * Leave data as default and try the next command */

                  /* Now read next attribute */
                  EP_RETRY_COUNT(pluginParams, epIndex) = 0;
                  readBasicClusterProductIdReq(Service, pluginParams, epIndex);

                }
              else
                {
                  readBasicClusterModelIdReq(Service, pluginParams, epIndex);
                }
            }
        }
    }
}

/**
 * Simple Descriptor response handler
 * Stores the simple descriptor of the endpoint, then starts read of Model Id
 */
static void SimpleDescRespHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoSimpleDescriptorRespParams_t* Params, szl_uint8 TransactionId)
{
  szl_uint8 epIndex;
  szl_uint8 clusterIndex;
  szl_bool basicClusterExists;
  SzlPlugin_Discovery_Params_t * pluginParams = NULL;

  /* Validate inputs */
  if ( (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DISCOVERY, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return;
    }


  for (epIndex = 0; epIndex < SZL_PLUGIN_DISCOVERY_M_MAX_CONCURRENT_DISCOVERIES; epIndex++)
    {
      if (EP_DISCOVERY_ENTRY(pluginParams, epIndex) != NULL)
        {
          if  (EP_DISCOVERY_ENTRY(pluginParams, epIndex)->NetworkAddress == Params->NetworkAddressOfInterest)
            {
              /* Careful: We only get the endpoint in the response if it was ZDO Success */
              if ( (Status == SZL_STATUS_SUCCESS) && (Params->Status == SZL_ZDO_STATUS_SUCCESS) &&
                   (EP_LIST(pluginParams, epIndex)[EP_CURRENT_INDEX(pluginParams, epIndex)] == Params->Endpoint))
                {
                  /* Good response from the new endpoint. Create new entry for EP data */
                  EP_INFO(pluginParams, epIndex) = (SzlPlugin_Discovery_EndpointInfo_t*)szl_mem_alloc(SzlPlugin_Discovery_EndpointInfo_t_SIZE(Params->NumInClusters + Params->NumOutClusters),
                                                                                                      MALLOC_ID_PLG_DISC_EP_INFO);
                  if (EP_INFO(pluginParams, epIndex) != NULL)
                    {

                      szl_memset(EP_INFO(pluginParams, epIndex), 0, SzlPlugin_Discovery_EndpointInfo_t_SIZE(Params->NumInClusters + Params->NumOutClusters));

                      /* Initialise data incase we don't get nice values for them */
                      EP_INFO(pluginParams, epIndex)->ModelId[0] = 0;
                      EP_INFO(pluginParams, epIndex)->ProductIdentifier = SZL_PLUGIN_DISCOVERY_PRODUCT_IDENTIFIER_UNDEFINED;

                      /* Copy the SD info to our new storage space */
                      szl_memcpy(&EP_INFO(pluginParams, epIndex)->SimpleDesc, Params, sizeof(SZL_ZdoSimpleDescriptorRespParams_t));
                      szl_memcpy(&EP_INFO(pluginParams, epIndex)->ClusterLists[0], Params->InClusterList, (sizeof(szl_uint16) * Params->NumInClusters));
                      szl_memcpy(&EP_INFO(pluginParams, epIndex)->ClusterLists[Params->NumInClusters], Params->OutClusterList, (sizeof(szl_uint16) * Params->NumOutClusters));                      /* PATCH */

                      /* ARTF112481: Cluster lists sometimes corrupted
                       * Above we have copied the simple descriptor and In/Out Cluster Arrays from Params to the DiscoveryInfo,
                       * but the simple descriptor contains pointers to the In/Out Cluster arrays.
                       * These pointers were not being updated, so were still pointing into Params, which is freed on completion of this function.
                       * Most of the time this was still working, as the data at the pointers would not have changed before discovery completed,
                       * but in high traffic networks it sometimes does, leading to corrupted cluster lists.
                       * We now correctly update the in/out cluster pointers to correctly reference the data in DiscoveryInfo
                       */
                      EP_INFO(pluginParams, epIndex)->SimpleDesc.InClusterList = &EP_INFO(pluginParams, epIndex)->ClusterLists[0];
                      EP_INFO(pluginParams, epIndex)->SimpleDesc.OutClusterList = &EP_INFO(pluginParams, epIndex)->ClusterLists[Params->NumInClusters];

                      /* If basic cluster exists then start reading attributes. If not move to next endpoint.
                       * WARNING: If we want to read attributes other than from the basic cluster this will need to be udpated! */
                      basicClusterExists = szl_false;
                      for (clusterIndex = 0; clusterIndex < Params->NumInClusters; clusterIndex++)
                        {
                          if (EP_INFO(pluginParams, epIndex)->ClusterLists[clusterIndex] == CLUSTER_BASIC)
                            {
                              basicClusterExists = szl_true;
                              break;
                            }
                        }
                      if (basicClusterExists == szl_true)
                        {
                          EP_RETRY_COUNT(pluginParams, epIndex) = 0;
                          readBasicClusterModelIdReq(Service, pluginParams, epIndex);
                        }
                      else
                        {
                          NextEndpoint(Service, pluginParams, epIndex);
                        }
                    }
                  else
                    {
                      EndEndpointDiscovery(Service, pluginParams, epIndex, SZL_RESULT_INTERNAL_LIB_ERROR);
                    }

                }
              else
                {
                  simpleDescReq(Service, pluginParams, epIndex);
                }
              return;
            }
        }
    }
}


/**
 * Active endpoint response handler
 * Stores the list of endpoint on the nodes, which will then each be discovered further
 */
static void ActiveEpRespHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoActiveEndpointRespParams_t* Params, szl_uint8 TransactionId)
{
  szl_uint8 epIndex;
  SZL_RESULT_t result = SZL_RESULT_FAILED;
  SzlPlugin_Discovery_Params_t * pluginParams = NULL;

  /* Validate inputs */
  if ( (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DISCOVERY, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return;
    }

  for (epIndex = 0; epIndex < SZL_PLUGIN_DISCOVERY_M_MAX_CONCURRENT_DISCOVERIES; epIndex++)
    {
      if (EP_DISCOVERY_ENTRY(pluginParams, epIndex) != NULL)
        {
          if (EP_DISCOVERY_ENTRY(pluginParams, epIndex)->NetworkAddress == Params->NetworkAddressOfInterest)
            {
              if ( (Status == SZL_STATUS_SUCCESS) && (Params->ActiveEndpointCount > 0) )
                {
                  /* Allocate space for the Endpoint List, then copy data in.
                   * Initialise states for discovery of each EP in the list, then kick off with Simple Descriptor */
                    EP_LIST(pluginParams, epIndex) = (szl_uint8*)szl_mem_alloc(Params->ActiveEndpointCount,
                                                                               MALLOC_ID_PLG_DISC_EP_LIST);
                    if (EP_LIST(pluginParams, epIndex) != NULL)
                      {
                        szl_memcpy(EP_LIST(pluginParams, epIndex), Params->ActiveEndpointList, Params->ActiveEndpointCount);
                        EP_COUNT(pluginParams, epIndex) = Params->ActiveEndpointCount;
                        EP_CURRENT_INDEX(pluginParams, epIndex) = 0;

                        EP_RETRY_COUNT(pluginParams, epIndex) = 0;
                        simpleDescReq(Service, pluginParams, epIndex);
                      }
                    else
                      {
                        result  = SZL_RESULT_INTERNAL_LIB_ERROR;
                        EndEndpointDiscovery(Service, pluginParams, epIndex, result);
                      }
                }
              /* Weird case: There are no endpoints, so confirm success but without any EPs reported */
              else if ( (Status == SZL_STATUS_SUCCESS) && (Params->Status == SZL_ZDO_STATUS_SUCCESS) && (Params->ActiveEndpointCount == 0) )
                {
                  result  = SZL_RESULT_SUCCESS;

                  // TODO - Test this case!!!
                  EndEndpointDiscovery(Service, pluginParams, epIndex, result);
                  return;
                }
              /* Error */
              else
                {
                  activeEpReq(Service, pluginParams, epIndex);
                }
              return;
            }
        }
    }
}


/**
 * Discover Endpoints on a Node in the Pro Network
 *
 * Parameters:
 *  NetworkAddress: network address of node to be discovered
 *  EndpointCallback: Callback to be called with info about each endpoint.
 *                    Callback will be called once per endpoint on the node.
 *  ConfirmCallback: Callback to be called on completion of discovery, with status
 *                   indicating success/failure of the process.
 *  Retries: Number of times any discovery command will be retried if the first attempt fails.
 *           Total number of attempts to send the command = (Retries+1)
 *           A minimum value of 1 is recommended to ensure reliable discovery.
 *           Each retry will add up to SZL_ZAB_M_SYNCHRONOUS_TRANSACTION_TIMEOUT_S for each command,
 *            so total process can become significantly slower if retries are required.
 */
SZL_RESULT_t SzlPlugin_Discovery_DiscoverEndpoints(zabService* Service,
                                                   SzlPlugin_Discovery_EndpointConfirm_CB_t ConfirmCallback,
                                                   SzlPlugin_Discovery_EndpointDiscovered_CB_t EndpointCallback,
                                                   szl_uint16 NetworkAddress,
                                                   szl_uint8 Retries)
{
  SZL_RESULT_t result;
  szl_uint8 epIndex;
  SzlPlugin_Discovery_Params_t * pluginParams = NULL;

  /* Valid params and state */
  if ( (ConfirmCallback == NULL) || (EndpointCallback == NULL) || (Retries == 0xFF) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
  if ( (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DISCOVERY, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_LIBRARY_NOT_INITIALIZED;
    }


  /* Check discovery is not already running for the Node */
  for (epIndex = 0; epIndex < SZL_PLUGIN_DISCOVERY_M_MAX_CONCURRENT_DISCOVERIES; epIndex++)
    {
      if ( (EP_DISCOVERY_ENTRY(pluginParams, epIndex) != NULL) &&
           (EP_NETWORK_ADDRESS(pluginParams, epIndex) == NetworkAddress) )
        {
          return SZL_RESULT_ALREADY_EXISTS;
        }
    }

  /* Find a free entry */
  for (epIndex = 0; epIndex < SZL_PLUGIN_DISCOVERY_M_MAX_CONCURRENT_DISCOVERIES; epIndex++)
    {
      if (EP_DISCOVERY_ENTRY(pluginParams, epIndex) == NULL)
        {
          break;
        }
    }
  if (epIndex < SZL_PLUGIN_DISCOVERY_M_MAX_CONCURRENT_DISCOVERIES)
    {
      /* Now malloc the entry and confirm it is ok before proceeding */
      EP_DISCOVERY_ENTRY(pluginParams, epIndex) = (Endpoint_Discovery_Params_t *) szl_mem_alloc(sizeof(Endpoint_Discovery_Params_t),
                                                                                                MALLOC_ID_PLG_DISC_EP_ITEM);
      if (EP_DISCOVERY_ENTRY(pluginParams, epIndex) == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }

      szl_memset(EP_DISCOVERY_ENTRY(pluginParams, epIndex), 0, sizeof(Endpoint_Discovery_Params_t));
    }
  else
    {
      return SZL_RESULT_OPERATION_NOT_POSSIBLE;
    }

  /* Initialise state machine, being sure to free any old table */
  EP_NETWORK_ADDRESS(pluginParams, epIndex) = NetworkAddress;
  EP_CONFIRM_CALLBACK(pluginParams, epIndex) = ConfirmCallback;
  EP_CALLBACK(pluginParams, epIndex) = EndpointCallback;
  EP_RETRIES(pluginParams, epIndex) = Retries + 1;


  EP_RETRY_COUNT(pluginParams, epIndex) = 0;
  result = activeEpReq(Service, pluginParams, epIndex);

  return result;
}


/******************************************************************************
 *                      ******************************
 *                 *****        INITIALISATION        *****
 *                      ******************************
 ******************************************************************************/

/**
 * Plugin Initialize
 *
 * SourceEndpoint: This must be a previously registered endpoint. It is used
 *                 as the source for ZCL read attributes commands.
 */
SZL_RESULT_t SzlPlugin_Discovery_Init(zabService* Service, szl_uint8 SourceEndpoint, SzlPlugin_Discovery_NodeDetected_CB_t NodeDetectedCallback)
{
  SZL_RESULT_t result;
  SzlPlugin_Discovery_Params_t * pluginParams = NULL;

  /* Validate inputs */
  if (Service == NULL)
    {
      return SZL_RESULT_LIBRARY_NOT_INITIALIZED;
    }

  result = SZL_PluginRegister(Service, SZL_PLUGIN_ID_DISCOVERY, sizeof(SzlPlugin_Discovery_Params_t), (void**)&pluginParams);
  if ( (result == SZL_RESULT_SUCCESS) && (pluginParams != NULL) )
    {

      /* Init parameters */
      pluginParams->CurrentNodeState = SZLPLUGIN_NODE_DISCOVERY_STATE_IDLE;
      pluginParams->NodeDetectedCallback = NodeDetectedCallback;
      pluginParams->SourceEndpoint = SourceEndpoint;

      /* Init SZL items required by the plugin */
      result = SZL_ZDO_DeviceAnnounceNtfRegisterCB(Service, ZdoDeviceAnnounceHandler);

      if (result != SZL_RESULT_SUCCESS)
        {
          SZL_PluginUnregister(Service, SZL_PLUGIN_ID_DISCOVERY);
        }
    }
  return result;
}

/**
 * Plugin Destroy
 */
SZL_RESULT_t SzlPlugin_Discovery_Destroy(zabService* Service)
{
  void * temp;
  SzlPlugin_Discovery_Params_t * pluginParams = NULL;

  if ( (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_DISCOVERY, (void**)&pluginParams) == SZL_RESULT_SUCCESS) &&
       (pluginParams != NULL) )
    {
      /* Remove registered callback */
      SZL_ZDO_DeviceAnnounceNtfUnregisterCB(Service, ZdoDeviceAnnounceHandler);

      /* Free node discovery data */
      temp = pluginParams->NodeInfoTable;
      szl_mem_free(temp);

      /* Free endpoint discovery data */
      ClearEndpointDiscoveryAll(Service, pluginParams);

      /* Free the plugin parameters */
      return SZL_PluginUnregister(Service, SZL_PLUGIN_ID_DISCOVERY);
    }
  return SZL_RESULT_FAILED;
}
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/