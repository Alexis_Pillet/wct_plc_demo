/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the header for the SYS interface for the ZNP
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.02.00  18-Oct-13   MvdB   Upgraded from oldPP GIVE: HW Version file
 * 00.00.06_GP  15-Oct-14   MvdB   ARTF105854: Support Smartlink IPZ hardware for Pro+GP ZNP
 *                                 ARTF105860: Change SYS_UPGRADE_RESET value to avoid crossover with TI
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 * 002.001.001  29-Apr-15   MvdB   ARTF132260: Support firmware upgrade of CC2538 network processors
 * 002.002.020  18-Mar-16   MvdB   ARTF165342: Support performing SBL firmware updates of ZAB_NET_PROC_MODEL_CC2538_SBS_SERIAL_GATEWAY
 * 002.002.052  30-Mar-17   MvdB   Support ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE
 * 002.002.053  12-Apr-17   SMon   Support ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE_PA
 *****************************************************************************/
#ifndef __ZAB_ZNP_SYS_UTILITY_H__
#define __ZAB_ZNP_SYS_UTILITY_H__



#ifdef __cplusplus
extern "C" {
#endif

#include "osTypes.h"
#include "erStatusUtility.h"
#include "sapTypes.h"
#include "zabTypes.h"

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/* Type of reset Requests  */
typedef enum
{
  SYS_HARD_RESET      = 0,    /* Hard: Full hardware reset */
  SYS_SOFT_RESET      = 1,    /* Soft: Software reset. Useful for USB dongle where this reset keep the usb connection active */
  SYS_UPGRADE_RESET   = 2,    /* Upgrade: Reset into Serial Boot Loader (SBL) for firmware upgrade */
} RESET_TYPE;

/* Product ID, as returned in Sys Version / Sys Rest Indication */
typedef enum
{
  znpProductId_TI_Znp                           = 0x00,
  znpProductId_Schneider_Pro_Znp                = 0x10,
  znpProductId_Schneider_GP_15_4_Brick          = 0x14,
  znpProductId_Schneider_ZB_Emc_Brick           = 0x16,
  znpProductId_Schneider_GPD_15_4_Brick         = 0x17,
  znpProductId_Schneider_Pro_GP_Znp             = 0x18,
  znpProductId_Schneider_Reserved_Future_Znp1   = 0x19,
  znpProductId_Schneider_Reserved_Future_Znp2   = 0x1A,
  znpProductId_Schneider_Reserved_Future_Znp3   = 0x1B,
  znpProductId_Schneider_Reserved_Future_Znp4   = 0x1C,
  znpProductId_Schneider_Reserved_Future_Znp5   = 0x1D,
  znpProductId_Schneider_Reserved_Future_Znp6   = 0x1E,
  znpProductId_Schneider_Reserved_Future_Znp7   = 0x1F,
  znpProductId_Schneider_Generic                = 0xFD,
  znpProductId_Schneider_Sbl                    = 0xFE,
  znpProductId_Unknown                          = 0xFF
} znpProductId;

/* Hardware Type, as returned in =S= extension to Sys Version / Sys Rest Indication */
typedef enum
{
  znpHardwareType_Error                              = 0x00,
  znpHardwareType_CC2530_TI_EB                       = 0x01,
  znpHardwareType_CC2531_TI_USB_Dongle               = 0x02,
  znpHardwareType_CC2531_Spectec_USB_Dongle_PA       = 0x03,
  znpHardwareType_CC2530_OUREA_V2                    = 0x04,
  znpHardwareType_CC2531_SE_Inventek_USB_Dongle_PA   = 0x05,
  znpHardwareType_CC2538_TI_EB                       = 0x06,
  znpHardwareType_CC2530_Nova                        = 0x07,
  znpHardwareType_CC2530_SmartlinkIpz                = 0x08,
  znpHardwareType_CC2538_OUREA_UART                  = 0x09,
  znpHardwareType_CC2538_OUREA_USB                   = 0x0A,
  znpHardwareType_CC2538_SBS_SerialGateway           = 0x0B,
  znpHardwareType_CC2538_SZCYIT_Dongle               = 0x0C,
  znpHardwareType_CC2538_SZCYIT_Dongle_PA            = 0x0D,
  znpHardwareType_Not_Init                           = 0xFF
} znpHardwareType;

/* GPIO operation */
typedef enum
{
  znpGpioOperation_Set        = 0x02,
  znpGpioOperation_Clear      = 0x03,
  znpGpioOperation_Toggle     = 0x04,
  znpGpioOperation_Read       = 0x05,
} znpGpioOperation;

/* GPIO Item Mask */
typedef enum
{
  znpGpioItemMask_Led1        = 0x01,
  znpGpioItemMask_Led2        = 0x02,
  znpGpioItemMask_Button1     = 0x04,
  znpGpioItemMask_Button2     = 0x08,
} znpGpioItemMask;


/* ADC Channel */
typedef enum
{
  znpAdcChannel_AIN0        = 0x06,
  znpAdcChannel_AIN1        = 0x07,
  znpAdcChannel_AIN0Minus1  = 0x0B,
  znpAdcChannel_Temperature = 0x0E,
  znpAdcChannel_Volatage    = 0x0F,
} znpAdcChannel;
/* ADC Resolution */
typedef enum
{
  znpAdcResolution_7        = 0x00,
  znpAdcResolution_9        = 0x01,
  znpAdcResolution_10       = 0x02,
  znpAdcResolution_12       = 0x03,
} znpAdcResolution;

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Check if the network processor is compatible
 * This checks if the transport revision is compatible with this ZAB.
 ******************************************************************************/
extern
zab_bool zabZnpSysUtility_CompatibilityCheck(unsigned8 TransportRev,
                                            znpProductId ProductId,
                                            unsigned8 MajorVersion,
                                            unsigned8 MinorVersion,
                                            unsigned8 ReleaseVersion);

/******************************************************************************
 * Check if the SBL is compatible with ZAB
 ******************************************************************************/
extern
zab_bool zabZnpSysUtility_BootloaderCompatibilityCheck(unsigned8 TransportRev,
                                            znpProductId ProductId,
                                            unsigned8 MajorVersion,
                                            unsigned8 MinorVersion,
                                            unsigned8 ReleaseVersion);

/******************************************************************************
 * Check if a new firmware image is compatible with the current network processor.
 * This confirms that the:
 *  - Image will function with ZAB once loaded
 *  - The image is built for compatible hardware
 ******************************************************************************/
extern
zab_bool zabZnpSysUtility_CompatibilityCheckNewFirmwareImage(zabService* Service,
                                                             unsigned8 TransportRev,
                                                             znpProductId ProductId,
                                                             unsigned8 MajorVersion,
                                                             unsigned8 MinorVersion,
                                                             unsigned8 ReleaseVersion,
                                                             znpHardwareType HardwareType);

/******************************************************************************
 * Request reset of the ZNP (Send SYS_RESET command)
 ******************************************************************************/
extern
void znpi_sys_reset( erStatus* Status, zabService* Service, RESET_TYPE type);

/******************************************************************************
 * Request Version of the ZNP (Send SYS_VERSION command)
 ******************************************************************************/
extern
void zabZnpSysUtility_VersionRequest(erStatus* Status, zabService* Service);

/******************************************************************************
 * Ping the ZNP (Send SYS_PING command)
 * Also returns capabilities bit field
 ******************************************************************************/
extern
void zabZnpSysUtility_PingRequest(erStatus* Status, zabService* Service);

/******************************************************************************
 * Set the IEEE Address of the Network Processor
 * WARNING: Anyone who uses this is responsible for ensuring IEEE addresses remain unique!
 ******************************************************************************/
extern
void zabZnpSysUtility_SetIeeeAddr(erStatus* Status, zabService* Service, unsigned64 IeeeAddr);

/******************************************************************************
 * Get the IEEE Address of the Network Processor
 ******************************************************************************/
extern
void zabZnpSysUtility_GetIeeeAddr(erStatus* Status, zabService* Service);

/******************************************************************************
 * Get the length of an NV Item
 ******************************************************************************/
extern
void zabZnpSysUtility_NvLength(erStatus* Status, zabService* Service, unsigned16 NvId);

/******************************************************************************
 * Read an NV Item
 ******************************************************************************/
extern
void zabZnpSysUtility_NvRead(erStatus* Status, zabService* Service, unsigned16 NvId, unsigned16 Offset);

/******************************************************************************
 * Write an NV Item
 ******************************************************************************/
extern
void zabZnpSysUtility_NvWrite(erStatus* Status,
                              zabService* Service,
                              unsigned16 NvId,
                              unsigned16 Offset,
                              unsigned8 Length,
                              unsigned8* DataPointer);

/******************************************************************************
 * Set the TX power of a ZNP
 ******************************************************************************/
extern
void zabZnpSysUtility_SetTxPower(erStatus* Status, zabService* Service, signed8 TxPowerDbm);

/******************************************************************************
 * Get/Set GPIO in the ZNP
 ******************************************************************************/
extern
void zabZnpSysUtility_GpioControl(erStatus* Status, zabService* Service, znpGpioOperation gpioOperation, znpGpioItemMask gpioItemMask);

/******************************************************************************
 * ADC Read Request
 ******************************************************************************/
extern
void zabZnpSysUtility_AdcRead(erStatus* Status, zabService* Service, znpAdcChannel adcChannel, znpAdcResolution adcResolution);

/******************************************************************************
 * Process All Message of subsystem SYS
 ******************************************************************************/
extern
void zabZnpSysProcessMsg(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * Get Antenna Mode Request
 ******************************************************************************/
extern
void zabZnpSysUtility_GetAntennaMode(erStatus* Status, zabService* Service);

/******************************************************************************
 * Set Antenna Mode Request
 ******************************************************************************/
extern
void zabZnpSysUtility_SetAntennaMode(erStatus* Status, zabService* Service, zabAntennaOperatingMode AntennaOperatingMode);


/******************************************************************************
 * Set ZGP Stub Disabled - TEST FUNCTION ONLY!!!
 ******************************************************************************/
extern
void zabZnpSysUtility_SetZgpStubDisabled(erStatus* Status, zabService* Service, zab_bool stubDisabled);

#ifdef __cplusplus
}
#endif

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif
