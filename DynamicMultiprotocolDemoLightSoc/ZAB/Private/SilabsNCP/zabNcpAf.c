/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the AF Data Service
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.018  26-Jul-16   MvdB   Support AF Broadcasts and Groupcasts
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 * 002.002.040  17-Jan-17   MvdB   ARTF175875: Upgrade zabNcpAf_DataRequest() to support send via bind
 *****************************************************************************/

#include "zabNcpService.h"
#include "zabNcpPrivate.h"
#include "zabNcpEzspMessaging.h"

#include "zabNcpNwk.h"

#include "zabNcpAf.h"


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

#define HA_PROFILE_ID           ( 0x0104 )


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/



/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/



/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 * Send an AF Data Request
 *
 * Return zab_bool:
 *  - zab_true = retain message on output queue as it needs to be sent again
 *  - zab_false = remove message from output queue as we are finished with it
 ******************************************************************************/
zab_bool zabNcpAf_DataRequest(erStatus* Status, zabService* Service, sapMsg* Message)
{
  zabMsgProDataReq *dataReqMsg;
  EmberApsFrame apsFrame;
  zab_bool retainMessage = zab_false;
  const EmberBindingTableEntry* bindItem;
  unsigned8 bindingIndex;

  /* Validate the vendor is open and networked */
  zabVendorService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS_VALUE(Status, retainMessage);

  /* Extract params from Message */
  dataReqMsg = (zabMsgProDataReq*)sapMsgGetAppData(Message);
  if(dataReqMsg->dataLength > 230)
  {
    erStatusSet( Status, ZAB_ERROR_MSG_APS_INVALID);
    return retainMessage;
  }

  /* Setup APS frame and send based on address mode*/
  apsFrame.clusterId = dataReqMsg->cluster;
  apsFrame.options = EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY;
  apsFrame.profileId = dataReqMsg->profile;
  apsFrame.sequence = sapMsgGetAppTransactionId(Message);
  apsFrame.sourceEndpoint = dataReqMsg->srcEndpoint;
  switch (dataReqMsg->dstAddress.addressMode)
    {
      case ZAB_ADDRESS_MODE_NWK_ADDRESS:
        apsFrame.destinationEndpoint = dataReqMsg->dstAddress.endpoint;
        zabNcpEzspMessaging_SendUnicast(Status, Service,
                                        EMBER_OUTGOING_DIRECT,
                                        dataReqMsg->dstAddress.address.shortAddress,
                                        &apsFrame,
                                        sapMsgGetAppTransactionId(Message), //    uint8_t messageTag,
                                        dataReqMsg->dataLength,
                                        dataReqMsg->data);
        break;

      case ZAB_ADDRESS_MODE_GROUP:
        apsFrame.groupId = dataReqMsg->dstAddress.address.shortAddress;
        zabNcpEzspMessaging_SendMulticast(Status, Service,
                                          &apsFrame,
                                          0, // hops, // Zero is converted to EMBER_MAX_HOPS
                                          0xFF, //nonmemberRadius, // A value of 7 or greater is treated as infinite
                                          sapMsgGetAppTransactionId(Message), //    uint8_t messageTag,
                                          dataReqMsg->dataLength,
                                          dataReqMsg->data);
        break;
/*
      case ZAB_ADDRESS_MODE_IEEE_ADDRESS:
        msgData->destinationAddrMode = afAddr64Bit;
        COPY_OUT_64_BITS(msgData->destinationAddr, address->address.ieeeAddress);
        msgData->destinationEndPoint = address->endpoint;
        break;
      case ZAB_ADDRESS_MODE_GP_SOURCE_ID:
        msgData->destinationAddrMode = afAddr64Bit;
        COPY_OUT_64_BITS(msgData->destinationAddr, (address->address.sourceId | ZNP_M_GP_SRC_ID_IEEE_EXTENSION));
        msgData->destinationEndPoint = ZAB_GP_ENDPOINT;
        break;
*/
      case ZAB_ADDRESS_MODE_BROADCAST:
        zabNcpEzspMessaging_SendBroadcast(Status, Service,
                                          dataReqMsg->dstAddress.address.shortAddress,
                                          &apsFrame,
                                          0, // Zero is converted to EMBER_MAX_HOPS
                                          sapMsgGetAppTransactionId(Message), //    uint8_t messageTag,
                                          dataReqMsg->dataLength,
                                          dataReqMsg->data);
        break;

      case ZAB_ADDRESS_MODE_VIA_BIND:
        bindingIndex = zabNcpNwk_GetBindingIndex(Status, Service,
                                                 dataReqMsg->srcEndpoint, dataReqMsg->cluster, sapMsgGetAppTransactionId(Message),
                                                 &bindItem,
                                                 &retainMessage);
        if ( (bindingIndex < ZAB_NCP_NWK_BINDING_INDEX_NONE) && (bindItem != NULL) )
          {
            if (bindItem->type == EMBER_UNICAST_BINDING)
              {
                apsFrame.destinationEndpoint = bindItem->remote;
                zabNcpEzspMessaging_SendUnicast(Status, Service,
                                                EMBER_OUTGOING_VIA_BINDING,
                                                bindingIndex,
                                                &apsFrame,
                                                sapMsgGetAppTransactionId(Message), //    uint8_t messageTag,
                                                dataReqMsg->dataLength,
                                                dataReqMsg->data);
              }
            else if (bindItem->type == EMBER_MULTICAST_BINDING)
              {
                apsFrame.groupId = COPY_IN_16_BITS(bindItem->identifier);
                zabNcpEzspMessaging_SendMulticast(Status, Service,
                                                  &apsFrame,
                                                  0, // hops, // Zero is converted to EMBER_MAX_HOPS
                                                  0xFF, //nonmemberRadius, // A value of 7 or greater is treated as infinite
                                                  sapMsgGetAppTransactionId(Message), //    uint8_t messageTag,
                                                  dataReqMsg->dataLength,
                                                  dataReqMsg->data);
              }
            else
              {
                printVendor(Service, "Unsupported bindingType %d for end point 0x%02x Cluster 0x%04X\n", bindItem->type, dataReqMsg->srcEndpoint, dataReqMsg->cluster);
                retainMessage = zab_false;
              }
          }
        else
          {
            printVendor(Service, "zabNcpAf_DataRequest: WARNING: No binding for end point 0x%02x Cluster 0x%04X\n", dataReqMsg->srcEndpoint, dataReqMsg->cluster);
            retainMessage = zab_false;
          }
        break;

      default:
        printError(Service, "Unsupported dataReqMsg->dstAddress.addressMode %d\n", dataReqMsg->dstAddress.addressMode);
        erStatusSet( Status, ZAB_ERROR_MSG_PARAM_INVALID);
        break;
    }

  return retainMessage;
}

/******************************************************************************
 * Process an incoming AF Data frame
 ******************************************************************************/
void zabNcpAf_IncomingMessageHandler(erStatus* Status, zabService* Service,
                                     EmberIncomingMessageType IncomingMessageType,
                                     unsigned16 SourceAddress,
                                     EmberApsFrame* ApsFrame,
                                     unsigned8 Lqi,
                                     unsigned8 Rssi,
                                     unsigned8 PayloadLength, unsigned8* PayloadData)
{
  sapMsg* dataMessage = NULL;
  unsigned16 dataMessageLength;
  zabMsgProDataInd* dataInd;

  if (ApsFrame->profileId != HA_PROFILE_ID)
    {
      printVendor(Service, "WARNING: zabNcpAf_IncomingMessageHandler: Unsupported Profile 0x%04X - frame dropped\n", ApsFrame->profileId);
      return;
    }

  if (PayloadLength < MAX_APS_PAYLOAD_LENGTH)
    {
      // Allocate message upwards
      dataMessageLength = zabMsgProDataInd_SIZE() + PayloadLength;
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, dataMessageLength);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_AF_DATA_IN);
      sapMsgSetAppDataLength(Status, dataMessage, dataMessageLength);

      /* Populate message data */
      dataInd = (zabMsgProDataInd*)sapMsgGetAppData(dataMessage);
      dataInd->version = 0;

      dataInd->srcAddr.addressMode = ZAB_ADDRESS_MODE_NWK_ADDRESS;
      dataInd->srcAddr.address.shortAddress = SourceAddress;
      dataInd->srcAddr.endpoint = ApsFrame->sourceEndpoint;

      switch(IncomingMessageType)
        {
          case EMBER_INCOMING_UNICAST:
          case EMBER_INCOMING_UNICAST_REPLY:
            dataInd->dstAddr.addressMode = ZAB_ADDRESS_MODE_NWK_ADDRESS;
            dataInd->dstAddr.address.shortAddress = SourceAddress;
            dataInd->dstAddr.endpoint = ApsFrame->destinationEndpoint;
            break;

          case EMBER_INCOMING_MULTICAST:
          case EMBER_INCOMING_MULTICAST_LOOPBACK:
            dataInd->dstAddr.addressMode = ZAB_ADDRESS_MODE_GROUP;
            dataInd->dstAddr.address.shortAddress = ApsFrame->groupId;
            break;

          case EMBER_INCOMING_BROADCAST:
          case EMBER_INCOMING_BROADCAST_LOOPBACK:
            dataInd->dstAddr.addressMode = ZAB_ADDRESS_MODE_BROADCAST;
            dataInd->dstAddr.address.shortAddress = ZAB_BROADCAST_ALL;
            break;

          default:
            printError(Service, "Unsupported IncomingMessageType %d\n", IncomingMessageType);
            sapMsgFree(Status, dataMessage);
        }

      dataInd->cluster = ApsFrame->clusterId;
      dataInd->macSourceAddress = 0xFFFF;
      dataInd->lqi = Lqi;
      dataInd->rssi = Rssi;
      dataInd->securityStatus = 0;
      dataInd->timeStamp = 0;
      dataInd->dataLength = PayloadLength;
      osMemCopy( Status, dataInd->data, PayloadData, PayloadLength);

      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_PARSE);
    }
}
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/