/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/******************************************************************************
* File Name     : r_flash_api_rx600.h
* Device(s)     : RX631
* Tool-chain    : KPIT GNURX-ELF 15.01
* H/W platform  : G-CPX / EU-CPX2 / G-CPX3
* Description   : Sample software
******************************************************************************/

#ifndef R_FLASH_API_RX600_H
#define R_FLASH_API_RX600_H

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/

/* Defines standard typedefs used in this file */
#include <stdint.h>

/* Used for boolean definitions */
/*#include <stdbool.h> */

/* User specific options for Flash API */
#include "r_config.h"

/******************************************************************************
   Macro definitions
******************************************************************************/

/* Version Number of API. */
#define RX600_FLASH_API_VERSION_MAJOR   (2u)
#define RX600_FLASH_API_VERSION_MINOR   (20u)

/* Pointer definitions for what should be sent in to R_FLASH_Write */
#define FLASH_PTR_TYPE                  (uint32_t)
#define   BUF_PTR_TYPE                  (uint32_t)

/**** Function Return Values ****/
/* Operation was successful */
#define FLASH_SUCCESS                   (0x00u)

/* Flash area checked was blank, making this 0x00 as well to keep existing
   code checking compatibility */
#define FLASH_BLANK                     (0x00u)

/* The address that was supplied was not on aligned correctly for ROM or DF */
#define FLASH_ERROR_ALIGNED             (0x01u)

/* Flash area checked was not blank, making this 0x01 as well to keep existing
   code checking compatibility */
#define FLASH_NOT_BLANK                 (0x01u)

/* The number of bytes supplied to write was incorrect */
#define FLASH_ERROR_BYTES               (0x02u)

/* The address provided is not a valid ROM or DF address */
#define FLASH_ERROR_ADDRESS             (0x03u)

/* Writes cannot cross the 1MB boundary on some parts */
#define FLASH_ERROR_BOUNDARY            (0x04u)

/* Flash is busy with another operation */
#define FLASH_BUSY                      (0x05u)

/* Operation failed */
#define FLASH_FAILURE                   (0x06u)

/* Lock bit was set for the block in question */
#define FLASH_LOCK_BIT_SET              (0x07u)

/* Lock bit was not set for the block in question */
#define FLASH_LOCK_BIT_NOT_SET          (0x08u)

#if (MCU_RX631 == 1)

/* 'size' parameter for R_FLASH_DataAreaBlankCheck */
    #define BLANK_CHECK_2_BYTE          (0u)
#endif

/* 'size' parameter for R_FLASH_DataAreaBlankCheck */
#define BLANK_CHECK_ENTIRE_BLOCK        (1u)

/* Memory specifics for the RX group */
#if (MCU_RX631 == 1)

/* User ROM Block Area           Size: Start Addr -   End Addr */
    #define BLOCK_0  (0u)  /*    4KB: 0xFFFFF000 - 0xFFFFFFFF */
    #define BLOCK_1  (1u)  /*    4KB: 0xFFFFE000 - 0xFFFFEFFF */
    #define BLOCK_2  (2u)  /*    4KB: 0xFFFFD000 - 0xFFFFDFFF */
    #define BLOCK_3  (3u)  /*    4KB: 0xFFFFC000 - 0xFFFFCFFF */
    #define BLOCK_4  (4u)  /*    4KB: 0xFFFFB000 - 0xFFFFBFFF */
    #define BLOCK_5  (5u)  /*    4KB: 0xFFFFA000 - 0xFFFFAFFF */
    #define BLOCK_6  (6u)  /*    4KB: 0xFFFF9000 - 0xFFFF9FFF */
    #define BLOCK_7  (7u)  /*    4KB: 0xFFFF8000 - 0xFFFF8FFF */
    #define BLOCK_8  (8u)  /*   16KB: 0xFFFF4000 - 0xFFFF7FFF */
    #define BLOCK_9  (9u)  /*   16KB: 0xFFFF0000 - 0xFFFF3FFF */
    #define BLOCK_10 (10u) /*   16KB: 0xFFFEC000 - 0xFFFEFFFF */
    #define BLOCK_11 (11u) /*   16KB: 0xFFFE8000 - 0xFFFEBFFF */
    #define BLOCK_12 (12u) /*   16KB: 0xFFFE4000 - 0xFFFE7FFF */
    #define BLOCK_13 (13u) /*   16KB: 0xFFFE0000 - 0xFFFE3FFF */
    #define BLOCK_14 (14u) /*   16KB: 0xFFFDC000 - 0xFFFDFFFF */
    #define BLOCK_15 (15u) /*   16KB: 0xFFFD8000 - 0xFFFDBFFF */
    #define BLOCK_16 (16u) /*   16KB: 0xFFFD4000 - 0xFFFD7FFF */
    #define BLOCK_17 (17u) /*   16KB: 0xFFFD0000 - 0xFFFD3FFF */
    #define BLOCK_18 (18u) /*   16KB: 0xFFFCC000 - 0xFFFCFFFF */
    #define BLOCK_19 (19u) /*   16KB: 0xFFFC8000 - 0xFFFCBFFF */
    #define BLOCK_20 (20u) /*   16KB: 0xFFFC4000 - 0xFFFC7FFF */
    #define BLOCK_21 (21u) /*   16KB: 0xFFFC0000 - 0xFFFC3FFF */
    #define BLOCK_22 (22u) /*   16KB: 0xFFFBC000 - 0xFFFBFFFF */
    #define BLOCK_23 (23u) /*   16KB: 0xFFFB8000 - 0xFFFBBFFF */
    #define BLOCK_24 (24u) /*   16KB: 0xFFFB4000 - 0xFFFB7FFF */
    #define BLOCK_25 (25u) /*   16KB: 0xFFFB0000 - 0xFFFB3FFF */
    #define BLOCK_26 (26u) /*   16KB: 0xFFFAC000 - 0xFFFAFFFF */
    #define BLOCK_27 (27u) /*   16KB: 0xFFFA8000 - 0xFFFABFFF */
    #define BLOCK_28 (28u) /*   16KB: 0xFFFA4000 - 0xFFFA7FFF */
    #define BLOCK_29 (29u) /*   16KB: 0xFFFA0000 - 0xFFFA3FFF */
    #define BLOCK_30 (30u) /*   16KB: 0xFFF9C000 - 0xFFF9FFFF */
    #define BLOCK_31 (31u) /*   16KB: 0xFFF98000 - 0xFFF9BFFF */
    #define BLOCK_32 (32u) /*   16KB: 0xFFF94000 - 0xFFF97FFF */
    #define BLOCK_33 (33u) /*   16KB: 0xFFF90000 - 0xFFF93FFF */
    #define BLOCK_34 (34u) /*   16KB: 0xFFF8C000 - 0xFFF8FFFF */
    #define BLOCK_35 (35u) /*   16KB: 0xFFF88000 - 0xFFF8BFFF */
    #define BLOCK_36 (36u) /*   16KB: 0xFFF84000 - 0xFFF87FFF */
    #define BLOCK_37 (37u) /*   16KB: 0xFFF80000 - 0xFFF83FFF */
    #define BLOCK_38 (38u) /*   32KB: 0xFFF78000 - 0xFFF7FFFF */
    #define BLOCK_39 (39u) /*   32KB: 0xFFF70000 - 0xFFF77FFF */
    #define BLOCK_40 (40u) /*   32KB: 0xFFF68000 - 0xFFF6FFFF */
    #define BLOCK_41 (41u) /*   32KB: 0xFFF60000 - 0xFFF67FFF */
    #define BLOCK_42 (42u) /*   32KB: 0xFFF58000 - 0xFFF5FFFF */
    #define BLOCK_43 (43u) /*   32KB: 0xFFF50000 - 0xFFF57FFF */
    #define BLOCK_44 (44u) /*   32KB: 0xFFF48000 - 0xFFF4FFFF */
    #define BLOCK_45 (45u) /*   32KB: 0xFFF40000 - 0xFFF47FFF */
    #define BLOCK_46 (46u) /*   32KB: 0xFFF38000 - 0xFFF3FFFF */
    #define BLOCK_47 (47u) /*   32KB: 0xFFF30000 - 0xFFF37FFF */
    #define BLOCK_48 (48u) /*   32KB: 0xFFF28000 - 0xFFF2FFFF */
    #define BLOCK_49 (49u) /*   32KB: 0xFFF20000 - 0xFFF27FFF */
    #define BLOCK_50 (50u) /*   32KB: 0xFFF18000 - 0xFFF1FFFF */
    #define BLOCK_51 (51u) /*   32KB: 0xFFF10000 - 0xFFF17FFF */
    #define BLOCK_52 (52u) /*   32KB: 0xFFF08000 - 0xFFF0FFFF */
    #define BLOCK_53 (53u) /*   32KB: 0xFFF00000 - 0xFFF07FFF */
    #define BLOCK_54 (54u) /*   64KB: 0xFFEF0000 - 0xFFEFFFFF */
    #define BLOCK_55 (55u) /*   64KB: 0xFFEE0000 - 0xFFEEFFFF */
    #define BLOCK_56 (56u) /*   64KB: 0xFFED0000 - 0xFFEDFFFF */
    #define BLOCK_57 (57u) /*   64KB: 0xFFEC0000 - 0xFFECFFFF */
    #define BLOCK_58 (58u) /*   64KB: 0xFFEB0000 - 0xFFEBFFFF */
    #define BLOCK_59 (59u) /*   64KB: 0xFFEA0000 - 0xFFEAFFFF */
    #define BLOCK_60 (60u) /*   64KB: 0xFFE90000 - 0xFFE9FFFF */
    #define BLOCK_61 (61u) /*   64KB: 0xFFE80000 - 0xFFE8FFFF */
    #define BLOCK_62 (62u) /*   64KB: 0xFFE70000 - 0xFFE7FFFF */
    #define BLOCK_63 (63u) /*   64KB: 0xFFE60000 - 0xFFE6FFFF */
    #define BLOCK_64 (64u) /*   64KB: 0xFFE50000 - 0xFFE5FFFF */
    #define BLOCK_65 (65u) /*   64KB: 0xFFE40000 - 0xFFE4FFFF */
    #define BLOCK_66 (66u) /*   64KB: 0xFFE30000 - 0xFFE3FFFF */
    #define BLOCK_67 (67u) /*   64KB: 0xFFE20000 - 0xFFE2FFFF */
    #define BLOCK_68 (68u) /*   64KB: 0xFFE10000 - 0xFFE1FFFF */
    #define BLOCK_69 (69u) /*   64KB: 0xFFE00000 - 0xFFE0FFFF */

/* NOTE:
   The RX631 actually has 1024 x 32 byte blocks instead of the
   16 x 2Kbyte blocks shown below. These are grouped into 16 blocks to
   make it easier for the user to delete larger sections of the data
   flash. The user can still delete individual blocks but they will
   need to use the new flash erase function that takes addresses
   instead of blocks. */

/* Data Flash Block Area         Size: Start Addr -   End Addr */
    #define BLOCK_DB0  (70u) /*    2KB: 0x00100000 - 0x001007FF */
    #define BLOCK_DB1  (71u) /*    2KB: 0x00100800 - 0x00100FFF */
    #define BLOCK_DB2  (72u) /*    2KB: 0x00101000 - 0x001017FF */
    #define BLOCK_DB3  (73u) /*    2KB: 0x00101800 - 0x00101FFF */
    #define BLOCK_DB4  (74u) /*    2KB: 0x00102000 - 0x001027FF */
    #define BLOCK_DB5  (75u) /*    2KB: 0x00102800 - 0x00102FFF */
    #define BLOCK_DB6  (76u) /*    2KB: 0x00103000 - 0x001037FF */
    #define BLOCK_DB7  (77u) /*    2KB: 0x00103800 - 0x00103FFF */
    #define BLOCK_DB8  (78u) /*    2KB: 0x00104000 - 0x001047FF */
    #define BLOCK_DB9  (79u) /*    2KB: 0x00104800 - 0x00104FFF */
    #define BLOCK_DB10 (80u) /*    2KB: 0x00105000 - 0x001057FF */
    #define BLOCK_DB11 (81u) /*    2KB: 0x00105800 - 0x00105FFF */
    #define BLOCK_DB12 (82u) /*    2KB: 0x00106000 - 0x001067FF */
    #define BLOCK_DB13 (83u) /*    2KB: 0x00106800 - 0x00106FFF */
    #define BLOCK_DB14 (84u) /*    2KB: 0x00107000 - 0x001077FF */
    #define BLOCK_DB15 (85u) /*    2KB: 0x00107800 - 0x00107FFF */

/* Array of flash addresses used for writing */
extern const uint32_t g_flash_BlockAddresses[86];

uint8_t R_FLASH_EraseRange(uint32_t start_addr,
                          uint32_t bytes);


#else /* (MCU_RX631 == 1) */
    #error "!!! Need to define memory specifics for this RX600 family \
    in r_flash_api_rx600.h !!!"
#endif

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/
uint8_t R_FLASH_Erase(uint8_t block);
uint8_t R_FLASH_Write(uint32_t flash_addr,
                     uint32_t buffer_addr,
                     uint16_t bytes);
uint8_t R_FLASH_ProgramLockBit(uint8_t block);
uint8_t R_FLASH_ReadLockBit(uint8_t block);
uint8_t R_FLASH_SetLockBitProtection(uint8_t lock_bit);
uint8_t R_FLASH_GetStatus(void);

/* Data Flash Only Functions */
void R_FLASH_DataAreaAccess(uint16_t read_en_mask,
                           uint16_t write_en_mask);
uint8_t R_FLASH_DataAreaBlankCheck(uint32_t address,
                                  uint8_t size);

#endif /* R_FLASH_API_RX600_H */
