/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the Metering Cluster.
 *   This plugin is originally designed for Nova (partner).
 *
 *   It supports:
 *    - Multiple, application specified endpoints
 *    - Standard attributes
 *    - =S= MS attributes defined in ZigBee&GreenPower_Invariants_Partner_Products_Vxx.pdf
 *    - Default report configuration for time based reports (fast or slow)
 *    - Callback notification to app when a writable attribute is changed
 *
 *   It does not (currently) support:
 *    - Non-volatile backing of any data.
 *    - Report on change. These are not required for Nova, but would be required for certification.
 *
 * CONFIGURATION:
 *   METERING_M_USE_DIRECT_ACCESS:
 *    - Define to have plugin malloc data storage for attributes and access the data directly.
 *    - Undefine to have plugin use callbacks to request attribute read/write via callbacks
 *
 * USAGE:
 *   Usage of this plugin is very simple:
 *   If using METERING_M_USE_DIRECT_ACCESS:
 *    1. Initialise it with SzlPlugin_MeteringClusterInit(), specify the endpoints and a write notification callback
 *    2. Use SzlPlugin_MeteringCluster_GetAttributePointer() to get access to the data for each endpoint.
 *    3. Populate the data
 *    4. Call SzlPlugin_MeteringCluster_ConfigureReporting() to start attribute reporting
 *    5. Update data as it changes, the plugin will do the rest.
 *   If not using METERING_M_USE_DIRECT_ACCESS:
 *    1. Initialise it with SzlPlugin_MeteringClusterInit(), specify the endpoints and read/write attribute callbacks
 *    2. Call SzlPlugin_MeteringCluster_ConfigureReporting() to start attribute reporting
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         21-Feb-14   MvdB   Original
 *    2         18-Apr-14   MvdB   Extend attribute list
 *    3         19-May-14   MvdB   Remove endpoint number from public attribute data
 *    4         18-Aug-14   MvdB   Add support for callback based operation
 *    5         19-Feb-15   MvdB   Add Attributes: 4014-4017, 4100-4105, 4200-4205, 4300-4305
 *                                 Correct name and R/W as of SZCL 1.0.2
 *    6         01-Apr-15   MvdB   ARTF116256: Ensure SZL uses szl_memset and szl_memcpy rather than memset and memcpy
 *    7         20-Jul-15   MvdB   ARTF134686: Update allocation method to be generic and protect against multiple initialisation
 * 002.001.006  27-Jul-15   MvdB   ARTF134686: Standardise on SZL_RESULT_t for plugin init return value
 *                                 ARTF138318: Improve plugin destroy, add disable reporting function
 * 002.002.033  10-Jan-17   MvdB   ARTF170315: Fix incorrect data types for some MS attributes in the Metering Plugin
 *****************************************************************************/

#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include "zabCoreService.h"
#include "zabCorePrivate.h"
#include "szl.h"
#include "szl_plugin.h"
#include "szl_report.h"

#include "metering_cluster.h"

/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/

/* Fast and slow reporting periods
 * Only time is applied, reportable change is not used */
#define METERING_M_FAST_REPORT_TIME_S 5
#define METERING_M_SLOW_REPORT_TIME_S 60

/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Cluster ID */
#define ZCL_CLUSTER_ID_SE_SIMPLE_METERING           0x0702

/* A metering Endpoint - contains attribute data plus the endpoint number */
typedef struct
{
  SzlPlugin_Metering_Attributes_t AttributeData;
  szl_uint8 EndpointNumber;
}SzlPlugin_Metering_Endpoint_t;

/* Parameters for the service */
typedef struct
{
  szl_uint8 numEndpoints;
#ifdef METERING_M_USE_DIRECT_ACCESS
  SzlPlugin_MeteringCluster_AttributeWriteNotification_CB_t AttributeWriteCallback;
  SzlPlugin_Metering_Endpoint_t MeteringEndpoints[VLA_INIT];
#else
  szl_uint8 Endpoints[VLA_INIT];
#endif
}SzlPlugin_Metering_Params_t;

#ifdef METERING_M_USE_DIRECT_ACCESS
#define SzlPlugin_Metering_Params_t_SIZE(numEndpoints) (sizeof(SzlPlugin_Metering_Params_t) - (VLA_INIT * sizeof(SzlPlugin_Metering_Endpoint_t))+ (sizeof(SzlPlugin_Metering_Endpoint_t) * (numEndpoints)))
#else
#define SzlPlugin_Metering_Params_t_SIZE(numEndpoints) (sizeof(SzlPlugin_Metering_Params_t) - (VLA_INIT * sizeof(szl_uint8))+ (sizeof(szl_uint8) * (numEndpoints)))
#endif

/*
 * The set of attributes for the cluster.
 * This const data is used to initialise the majority of the attribute table.
 * Once it is copied into RAM, Endpoint and Data Access pointers are set for each attribute.
 *
 * Read only attributes use direct access by SZL.
 * Writable attributes use Read/Write callbacks, as the need the write callback to notify the plugin that
 *   a write is happening, so it can notify the app.
 *
 * New attributes must be added here.
 */
static const SZL_DataPoint_t meteringDatapointConfig[] = {
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_READING_CURRENT_SUM_DELIVERED,        0, SZL_ZB_DATATYPE_UINT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, CurrentSummationDelivered)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_READING_CURRENT_SUM_RECEIVED,         0, SZL_ZB_DATATYPE_UINT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, CurrentSummationReceived)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_POWER_FACTOR,                         0, SZL_ZB_DATATYPE_INT8,    1, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, PowerFactor)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_METER_STATUS_SET,                     0, SZL_ZB_DATATYPE_BITMAP8, 1, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, StatusSet)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_UNIT_OF_MEASURE,                      0, SZL_ZB_DATATYPE_ENUM8,   1, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, UnitOfMeasure)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_MULTIPLIER,                           0, SZL_ZB_DATATYPE_UINT24,  4, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, Multiplier)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_DIVISOR,                              0, SZL_ZB_DATATYPE_UINT24,  4, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, Divisor)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_SUMMATION_FORMATTING,                 0, SZL_ZB_DATATYPE_BITMAP8, 1, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, SummationFormatting)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_DEVICE_TYPE,                          0, SZL_ZB_DATATYPE_BITMAP8, 1, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, DeviceType)}}},

  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_READING_CURRENT_SUM_DELIVERED_RAZ,                  0, SZL_ZB_DATATYPE_UINT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW,   szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, CurrentSummationDeliveredRaz)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_TOTAL,        0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyActiveTotal)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_TOTAL,      0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyReactiveTotal)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_TOTAL,      0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyApparentTotal)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_CURRENT_SUMMATION_RECEIVED_RAZ,                     0, SZL_ZB_DATATYPE_UINT48,  1, {DP_METHOD_DIRECT, DP_ACCESS_RW,   szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, CurrentSummationReceivedRaz)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_RAZ_TOTAL,    0, SZL_ZB_DATATYPE_INT48,  1, {DP_METHOD_DIRECT, DP_ACCESS_RW,   szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyActiveInMinusOutRazTotal)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_RAZ_TOTAL,  0, SZL_ZB_DATATYPE_INT48,  1, {DP_METHOD_DIRECT, DP_ACCESS_RW,   szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyReactiveInMinusOutRazTotal)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_RAZ_TOTAL,  0, SZL_ZB_DATATYPE_INT48,  1, {DP_METHOD_DIRECT, DP_ACCESS_RW,   szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyApparentInMinusOutRazTotal)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_PLUS_OUT_TOTAL,         0, SZL_ZB_DATATYPE_UINT48,  1, {DP_METHOD_DIRECT, DP_ACCESS_READ,   szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyActiveInPlusOutTotal)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_PLUS_OUT_TOTAL,       0, SZL_ZB_DATATYPE_UINT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyReactiveInPlusOutTotal)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_PLUS_OUT_TOTAL,       0, SZL_ZB_DATATYPE_UINT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyApparentInPlusOutTotal)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_PLUS_OUT_RAZ_TOTAL,     0, SZL_ZB_DATATYPE_UINT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyActiveInPlusOutRazTotal)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_PLUS_OUT_RAZ_TOTAL,   0, SZL_ZB_DATATYPE_UINT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyReactiveInPlusOutRazTotal)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_PLUS_OUT_RAZ_TOTAL,   0, SZL_ZB_DATATYPE_UINT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyApparentInPlusOutRazTotal)}}},

  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_RAZ_PHA,    0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyActiveInMinusOutRazPhA)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_RAZ_PHA,  0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyReactiveInMinusOutRazPhA)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_RAZ_PHA,  0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyApparentInMinusOutRazPhA)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_PHA,        0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyActiveInMinusOutPhA)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_PHA,      0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyReactiveInMinusOutPhA)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_PHA,      0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyApparentInMinusOutPhA)}}},

  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_RAZ_PHB,    0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyActiveInMinusOutRazPhB)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_RAZ_PHB,  0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyReactiveInMinusOutRazPhB)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_RAZ_PHB,  0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyApparentInMinusOutRazPhB)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_PHB,        0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyActiveInMinusOutPhB)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_PHB,      0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyReactiveInMinusOutPhB)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_PHB,      0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyApparentInMinusOutPhB)}}},

  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_RAZ_PHC,    0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyActiveInMinusOutRazPhC)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_RAZ_PHC,  0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyReactiveInMinusOutRazPhC)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_RAZ_PHC,  0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyApparentInMinusOutRazPhC)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_PHC,        0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyActiveInMinusOutPhC)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_PHC,      0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyReactiveInMinusOutPhC)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_PHC,      0, SZL_ZB_DATATYPE_INT48,  8, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyApparentInMinusOutPhC)}}},

  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_MULTIPLIER,  0, SZL_ZB_DATATYPE_UINT24,  4, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyActiveMultiplier)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_DIVISOR,     0, SZL_ZB_DATATYPE_UINT24,  4, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyActiveDivisor)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_MULTIPLIER,0, SZL_ZB_DATATYPE_UINT24,  4, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyReactiveMultiplier)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_DIVISOR,   0, SZL_ZB_DATATYPE_UINT24,  4, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyReactiveDivisor)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_MULTIPLIER,0, SZL_ZB_DATATYPE_UINT24,  4, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyApparentMultiplier)}}},
  {ZCL_CLUSTER_ID_SE_SIMPLE_METERING, ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_DIVISOR,   0, SZL_ZB_DATATYPE_UINT24,  4, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true,  DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Metering_Attributes_t, ElectricalEnergyApparentDivisor)}}},
};
#define METERING_M_NUM_ATTRIBUTES_PER_ENDPOINT ( sizeof(meteringDatapointConfig) / sizeof(SZL_DataPoint_t))

/* Reporting Configuration Struct - used to define default reports */
typedef struct
{
  szl_uint16 AttributeID;
  szl_uint16 MinTime;
  szl_uint16 MaxTime;
  /*
  // Reportable change values - not supported for now as Nova only requires timed reports
  union
    {
      szl_uint16 RCuint16;
      szl_int32 RCint32;
      szl_uint32 RCuint32;
    };*/
}ReportingConfiguration_t;

/* Default Reporting Configuration
 * If an attribute is to be reported, it should be added to this table. */
static const ReportingConfiguration_t DefaultReportingConfiguration[] = {
  {ATTR_ID_SE_METERING_READING_CURRENT_SUM_DELIVERED,     METERING_M_FAST_REPORT_TIME_S, METERING_M_FAST_REPORT_TIME_S},
  {ATTR_ID_SE_METERING_READING_CURRENT_SUM_RECEIVED,      METERING_M_FAST_REPORT_TIME_S, METERING_M_FAST_REPORT_TIME_S},
  {ATTR_ID_SE_METERING_READING_CURRENT_SUM_DELIVERED_RAZ, METERING_M_FAST_REPORT_TIME_S, METERING_M_FAST_REPORT_TIME_S},
  {ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_TOTAL,    METERING_M_FAST_REPORT_TIME_S, METERING_M_FAST_REPORT_TIME_S},
  {ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_TOTAL,  METERING_M_FAST_REPORT_TIME_S, METERING_M_FAST_REPORT_TIME_S},
  {ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_TOTAL,  METERING_M_FAST_REPORT_TIME_S, METERING_M_FAST_REPORT_TIME_S},
};
#define METERING_M_NUM_DEFAULT_REPORTS (sizeof(DefaultReportingConfiguration) / sizeof(ReportingConfiguration_t))



/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Attribute Changed Notification Handler
 ******************************************************************************/
#ifdef METERING_M_USE_DIRECT_ACCESS
static void AttributeChangedNotificationHandler(zabService* Service, struct _SZL_AttributeChangedNtfParams_t* Params)
{
  SzlPlugin_Metering_Params_t * pluginParams = NULL;

  /* Validate inputs */
  if ( (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_METERING, (void**)&pluginParams) == SZL_RESULT_SUCCESS) &&
       (pluginParams != NULL) &&
       (pluginParams->AttributeWriteCallback != NULL) )
    {
      pluginParams->AttributeWriteCallback(Service,
                                           Params->Endpoint,
                                           Params->AttributeID);
    }
}
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Plugin Initialisation
 * This function must be called from the application after the Library has been initialized.
 *
 * Parameters:
 *  Service: Instance of library
 *  numEndpoints: The length of endpointList
 *  endpointList: The endpoints to be initialised with this cluster
 *
 *  For Direct Data Access:
 *  WriteAttributesCallback: Callback will be called to notify application that a writable attribute has been written from ZigBee.
 *                           It is optional. If the app does not want to be notified this may be set to NULL.
 *
 *  For Callback Data Access:
 *  ReadCallback: Callback that will be called to read the value of an attributes data
 *  WriteCallback: Callback that will be called to write the value of an attributes data
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_MeteringClusterInit(zabService* Service,
                                           szl_uint8 numEndpoints,
                                           szl_uint8 * endpointList,
#ifdef METERING_M_USE_DIRECT_ACCESS
                                           SzlPlugin_MeteringCluster_AttributeWriteNotification_CB_t WriteAttributesCallback
#else
                                           SZL_CB_DataPointRead_t ReadCallback,
                                           SZL_CB_DataPointWrite_t WriteCallback
#endif
                                           )
{
    SZL_RESULT_t result;
    szl_uint8 epIndex;
    szl_uint8 attr;
    SzlPlugin_Metering_Params_t* pluginParams = NULL;
    SZL_DataPoint_t serverAttributes[METERING_M_NUM_ATTRIBUTES_PER_ENDPOINT];
    szl_bool anyRegisterWorked;

    /* Validate inputs */
    if ( (Service == NULL) || (numEndpoints == 0) || (endpointList == NULL)
#ifndef METERING_M_USE_DIRECT_ACCESS
          || (ReadCallback == NULL) || (WriteCallback == NULL)
#endif
                                                      )
      {
        return SZL_RESULT_INVALID_DATA;
      }

    /* Malloc parameters the plugin needs to store and link from the SZL */
    result = SZL_PluginRegister(Service, SZL_PLUGIN_ID_METERING, SzlPlugin_Metering_Params_t_SIZE(numEndpoints), (void**)&pluginParams);
    if ( (result == SZL_RESULT_SUCCESS) && (pluginParams != NULL) )
      {
        /* Set parameters that are not per endpoint */
        pluginParams->numEndpoints = numEndpoints;

#ifdef METERING_M_USE_DIRECT_ACCESS
        params->AttributeWriteCallback = WriteAttributesCallback;
        if (WriteAttributesCallback != NULL)
          {
            if (SZL_AttributeChangedNtfRegisterCB(Service, AttributeChangedNotificationHandler) == SZL_RESULT_TABLE_FULL)
              {
                printError(Service, "PLUGIN MTR: Init failed - callback table full\n");
                SZL_PluginUnregister(Service, SZL_PLUGIN_ID_METERING);
                return SZL_RESULT_TABLE_FULL;
              }
          }
#else
        szl_memcpy(pluginParams->Endpoints, endpointList, numEndpoints);
#endif
        anyRegisterWorked = szl_false;
        result = SZL_RESULT_SUCCESS;
        for (epIndex = 0;
             (epIndex < numEndpoints) && (result == SZL_RESULT_SUCCESS || result == SZL_RESULT_ALREADY_EXISTS);
             epIndex++)
          {

            /* Init table of attributes to be registered for each endpoint
             * This is mostly the same for each EP, but we set the endpoint number and data pointers differently */
            szl_memcpy(serverAttributes,
                       meteringDatapointConfig,
                       sizeof(meteringDatapointConfig));


            /* Initialise endpoint number in data
             * All other data is un-initialised - this is up to the app unless we choose to add NVM support later */
#ifdef METERING_M_USE_DIRECT_ACCESS
            params->MeteringEndpoints[epIndex].EndpointNumber = endpointList[epIndex];
#else
            pluginParams->Endpoints[epIndex] = endpointList[epIndex];
#endif

            /* Set the data pointers in to params correctly, based on the endpoint number and attribute ID*/
            for (attr = 0; attr < METERING_M_NUM_ATTRIBUTES_PER_ENDPOINT; attr++)
              {
                serverAttributes[attr].Endpoint = endpointList[epIndex];
#ifdef METERING_M_USE_DIRECT_ACCESS
                serverAttributes[attr].AccessType.DirectAccess.Data = (void*)((szl_uint8*)serverAttributes[attr].AccessType.DirectAccess.Data + (size_t)&params->MeteringEndpoints[epIndex].AttributeData);
#else
                serverAttributes[attr].Property.Method = DP_METHOD_CALLBACK;
                serverAttributes[attr].AccessType.Callback.Read = ReadCallback;
                serverAttributes[attr].AccessType.Callback.Write = WriteCallback;
#endif
              }

            /* Register the cluster attributes */
            result = SZL_AttributesRegister(Service, serverAttributes, METERING_M_NUM_ATTRIBUTES_PER_ENDPOINT);

            /* Keep boolean to show if any register worked, as if it did we must ensure the data it points to is valid */
            if(result == SZL_RESULT_SUCCESS || result == SZL_RESULT_ALREADY_EXISTS)
              {
                anyRegisterWorked = szl_true;
              }
          }

        /* If any register worked, we must keep Params to ensure data pointers are valid. If all failed we can free */
        if(anyRegisterWorked == szl_true)
          {
            printInfo(Service, "PLUGIN MTR: Init Successful\n");
            return SZL_RESULT_SUCCESS;
          }

        /* Failed. Cleanup */
        printError(Service, "PLUGIN MTR: Init failed - Result = 0x%02X\n", result);
        SZL_PluginUnregister(Service, SZL_PLUGIN_ID_METERING);
        result = SZL_RESULT_FAILED;
      }

    return result;
}

/******************************************************************************
 * Plugin Destruction
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_MeteringCluster_Destroy(zabService* Service)
{
  SZL_RESULT_t result;
  szl_uint8 epIndex;
  SZL_EP_t endpoint;
  szl_uint8 attrIndex;
  SzlPlugin_Metering_Params_t * pluginParams = NULL;
  SZL_DataPoint_t serverAttributes[METERING_M_NUM_ATTRIBUTES_PER_ENDPOINT];

  /* Validate inputs */
  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_METERING, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }

  for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
    {
      /* Get the endpoint number */
#ifdef METERING_M_USE_DIRECT_ACCESS
      endpoint = pluginParams->MeteringEndpoints[epIndex].EndpointNumber;
#else
      endpoint = pluginParams->Endpoints[epIndex];
#endif

      /* Init table of attributes to be registered for each endpoint
       * This is mostly the same for each EP, but we set the endpoint number and data pointers differently */
      szl_memcpy(serverAttributes,
                  meteringDatapointConfig,
                  sizeof(meteringDatapointConfig));

      /* Register attributes in chunks to keep the stack usage under control */
      for (attrIndex = 0; attrIndex < METERING_M_NUM_ATTRIBUTES_PER_ENDPOINT; attrIndex++)
        {
          serverAttributes[attrIndex].Endpoint = endpoint;
        }

      /* De-Register the cluster attributes.
       * Don't care too much about the result as we will make best effort only */
      result = SZL_AttributesDeregister(Service, serverAttributes, METERING_M_NUM_ATTRIBUTES_PER_ENDPOINT);
      if (result != SZL_RESULT_SUCCESS)
        {
          printError(Service, "PLUGIN METERING: WARNING: Deregister failed for EP 0x%02X with Result 0x%02X\n",
                     endpoint,
                     result);
        }
    }

  /* Now unregister the plugin */
  return SZL_PluginUnregister(Service, SZL_PLUGIN_ID_METERING);
}

/******************************************************************************
 * Get pointer to Metering Cluster Attributes for an endpoint
 * This can be used to get access to the endpoints parameters for the local
 *  application to read or write them
 * Return NULL if not found
 ******************************************************************************/
#ifdef METERING_M_USE_DIRECT_ACCESS
SzlPlugin_Metering_Attributes_t* SzlPlugin_MeteringCluster_GetAttributePointer(zabService* Service, szl_uint8 endpoint)
{
  szl_uint8 epIndex;
  SzlPlugin_Metering_Params_t * pluginParams = NULL;

  /* Validate inputs */
  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_METERING, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return NULL;
    }

  for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
    {
      if (pluginParams->MeteringEndpoints[epIndex].EndpointNumber == endpoint)
        {
          return &pluginParams->MeteringEndpoints[epIndex].AttributeData;
        }
    }
  return NULL;
}
#endif

/******************************************************************************
 * Configure reporting for the cluster.
 * Data must be initialised before this function is called to avoid reporting uninitialised values
 *
 * This function currently only configures a time based default report.
 * If report on change is required the plugin must be extended.
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_MeteringCluster_ConfigureReporting(zabService* Service)
{
  szl_uint8 epIndex;
  szl_uint8 reportIndex;
  szl_uint8 attrIndex;
  szl_uint8 endpoint;
  SZL_STATUS_t sts;
  szl_bool manufacturerSpecific;
  SzlPlugin_Metering_Params_t * pluginParams = NULL;

  /* Validate inputs */
  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_METERING, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /*
   * Configure default reports:
   * For each default report in DefaultReportingConfiguration[]
   * Find the matching attribute in meteringDatapointConfig[]
   * Use values from meteringDatapointConfig and DefaultReportingConfiguration to configure reports
   * Repeat for each endpoint
   */
  for (reportIndex = 0; reportIndex < METERING_M_NUM_DEFAULT_REPORTS; reportIndex++)
    {
      for (attrIndex = 0; attrIndex < METERING_M_NUM_ATTRIBUTES_PER_ENDPOINT; attrIndex++)
        {
          if (DefaultReportingConfiguration[reportIndex].AttributeID == meteringDatapointConfig[attrIndex].AttributeID)
            {
              //Match found
              break;
            }
        }

      // If we found a match, then attrIndex will be < METERING_M_NUM_ATTRIBUTES_PER_ENDPOINT
      if (attrIndex < METERING_M_NUM_ATTRIBUTES_PER_ENDPOINT)
        {

          /* Register the default report for each endpoint */
          for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
            {
#ifdef METERING_M_USE_DIRECT_ACCESS
              endpoint = pluginParams->MeteringEndpoints[epIndex].EndpointNumber;
#else
              endpoint = pluginParams->Endpoints[epIndex];
#endif
              if (meteringDatapointConfig[attrIndex].Property.ManufacturerSpecific == szl_true)
                {
                  manufacturerSpecific = szl_true;
                }
              else
                {
                  manufacturerSpecific = szl_false;
                }


              sts = Szl_ReportSendingConfigure(Service,
                                                  endpoint,
                                                  manufacturerSpecific,
                                                  ZCL_CLUSTER_ID_SE_SIMPLE_METERING,
                                                  DefaultReportingConfiguration[reportIndex].AttributeID,
                                                  meteringDatapointConfig[attrIndex].DataType,
                                                  DefaultReportingConfiguration[reportIndex].MinTime,
                                                  DefaultReportingConfiguration[reportIndex].MaxTime,
                                                  NULL); // Reportable change not used for now as not required by Nova
              if (sts != SZL_STATUS_SUCCESS)
                {
                  printError(Service, "PLUGIN MTR: ERROR - Default report configuration for AttrID 0x%04X failed: 0x%02X\n", DefaultReportingConfiguration[reportIndex].AttributeID, sts);
                  return SZL_RESULT_FAILED;
                }
            }
        }
      else
        {
          printError(Service, "PLUGIN MTR: ERROR - Default report configuration for unknown attribute ID 0x%04X\n", DefaultReportingConfiguration[reportIndex].AttributeID);
          return SZL_RESULT_INVALID_DATA;
        }
    }

  // If or when we need to support report configuration in NVM, this is probably the place to load them out

  return SZL_RESULT_SUCCESS;
}


/******************************************************************************
 * Disable reporting for the cluster
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_MeteringCluster_DisableReporting(zabService* Service)
{
  szl_uint8 epIndex;
  szl_uint8 reportIndex;
  szl_uint8 attrIndex;
  szl_uint8 endpoint;
  SZL_STATUS_t sts;
  SZL_RESULT_t result = SZL_RESULT_SUCCESS;
  szl_bool manufacturerSpecific;
  SzlPlugin_Metering_Params_t * pluginParams = NULL;

  /* Validate inputs */
  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_METERING, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /*
   * Configure default reports:
   * For each default report in DefaultReportingConfiguration[]
   * Find the matching attribute in DatapointConfig[]
   * Set SZL_REPORT_M_REPORT_DISABLED_TIME
   * Repeat for each endpoint
   */
  for (reportIndex = 0; reportIndex < METERING_M_NUM_DEFAULT_REPORTS; reportIndex++)
    {
      for (attrIndex = 0; attrIndex < METERING_M_NUM_ATTRIBUTES_PER_ENDPOINT; attrIndex++)
        {
          if (DefaultReportingConfiguration[reportIndex].AttributeID == meteringDatapointConfig[attrIndex].AttributeID)
            {
              //Match found
              break;
            }
        }

      // If we found a match, then attrIndex will be < METERING_M_NUM_ATTRIBUTES_PER_ENDPOINT
      if (attrIndex < METERING_M_NUM_ATTRIBUTES_PER_ENDPOINT)
        {

          /* Disable the default report for each endpoint */
          for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
            {
#ifdef METERING_M_USE_DIRECT_ACCESS
              endpoint = pluginParams->MeteringEndpoints[epIndex].EndpointNumber;
#else
              endpoint = pluginParams->Endpoints[epIndex];
#endif

              if (meteringDatapointConfig[attrIndex].Property.ManufacturerSpecific == szl_true)
                {
                  manufacturerSpecific = szl_true;
                }
              else
                {
                  manufacturerSpecific = szl_false;
                }


              /* Configure report with disabled time */
              sts = Szl_ReportSendingConfigure(Service,
                                                endpoint,
                                                manufacturerSpecific,
                                                ZCL_CLUSTER_ID_SE_SIMPLE_METERING,
                                                DefaultReportingConfiguration[reportIndex].AttributeID,
                                                meteringDatapointConfig[attrIndex].DataType,
                                                SZL_REPORT_M_REPORT_DISABLED_TIME,
                                                SZL_REPORT_M_REPORT_DISABLED_TIME,
                                                NULL);
              if (sts != SZL_STATUS_SUCCESS)
                {
                  result = SZL_RESULT_FAILED;
                  printError(Service, "PLUGIN METERING: ERROR - Disable report configuration for AttrID 0x%04X failed: 0x%02X\n", DefaultReportingConfiguration[reportIndex].AttributeID, sts);
                }
            }
        }
    }

  return result;
}
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/