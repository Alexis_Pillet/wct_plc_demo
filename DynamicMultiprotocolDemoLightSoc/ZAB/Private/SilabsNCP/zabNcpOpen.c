/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the SiLabs NCP Open State Machine
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.009  12-Jul-16   MvdB   Give IEEE during open action
 * 000.000.011  19-Jul-16   MvdB   SBL Upgrades: Detect bootloader active during open
 * 000.000.012  19-Jul-16   MvdB   Move utilities functions from zabNcpEzspConfiguration into zabNcpEzspUtilities
 * 000.000.020  27-Jul-16   MvdB   Add network processor ping
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 * 002.002.031  09-Jan-17   MvdB   ARTF172881: Avoid work() returning zero due to errors like szl not initialised
 * 002.002.041  18-Jan-17   MvdB   ARTF199128: Fix NCP networking after reception of a leave command
 * 002.002.043  20-Jan-17   MvdB   ARTF190099: Make ping time run time controllable for ZNP & NCP. Private APIs only at this time, for industrial tester.
 *****************************************************************************/

#include "zabNcpService.h"
#include "zabNcpPrivate.h"
#include "zabNcpEzsp.h"
#include "zabNcpEzspConfiguration.h"
#include "zabNcpEzspUtilities.h"
#include "zabNcpNwk.h"
#include "zabNcpXModem.h"
#include "zabNcpAsh.h"
#include "zabNcpSbl.h"
#include "zabNcpOpen.h"


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* Timer disabled value */
#define M_TIMEOUT_DISABLED 0xFFFFFFFF

#define M_PING_TIME_MINIMUM_MS ( 1000 )
#if (ZAB_VND_CFG_M_NET_PROC_PING_MS > ZAB_OPEN_M_NET_PROC_PING_DISABLED)
  #error ZAB_VND_CFG_M_NET_PROC_PING_MS > ZAB_OPEN_M_NET_PROC_PING_DISABLED
#endif
#if (ZAB_VND_CFG_M_NET_PROC_PING_MS < M_PING_TIME_MINIMUM_MS)
  #error ZAB_VND_CFG_M_NET_PROC_PING_MS < M_PING_TIME_MINIMUM_MS
#endif

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/* Macros for accessing Open info */
#define ZAB_SERVICE_VND_OPEN( _srv )  (ZAB_SERVICE_VENDOR( (_srv) ) ->zabOpenInfo)
#define GET_OPEN_STATE( _srv )  (ZAB_SERVICE_VND_OPEN( (_srv) ).openState)
#define GET_OPEN_ITEM( _srv )  (ZAB_SERVICE_VND_OPEN( (_srv) ).currentItem)
#define GET_LAST_PING_TIME( _srv )  ( ZAB_SERVICE_VND_OPEN( (_srv) ).lastNetworkProcessorPingTimeMs )
#define GET_PING_FAIL_COUNT( _srv )  ( ZAB_SERVICE_VND_OPEN( (_srv) ).networkProcessorPingFailCount )
#define PING_TIME_MS( _srv )  ( ZAB_SERVICE_VND_OPEN( (_srv) ).networkProcessorPingTimeMs )


/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Set state machine inactive
 ******************************************************************************/
static void setInactive(zabService* Service)
{
  ZAB_SERVICE_VND_OPEN(Service).currentItem = ZAB_OPEN_CURRENT_ITEM_NONE;
  ZAB_SERVICE_VND_OPEN(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
}

/******************************************************************************
 * Set item if status is good
 ******************************************************************************/
static void checkStatusSetItem(erStatus* Status, zabService* Service, zabOpenCurrentItem item, unsigned32 Timeout)
{
  if (erStatusIsOk(Status))
    {
      ZAB_SERVICE_VND_OPEN(Service).currentItem = item;

      if (Timeout == M_TIMEOUT_DISABLED)
        {
          ZAB_SERVICE_VND_OPEN(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
        }
      else
        {
          osTimeGetMilliseconds(Service, &ZAB_SERVICE_VND_OPEN(Service).timeoutExpiryMs);
          ZAB_SERVICE_VND_OPEN(Service).timeoutExpiryMs += Timeout;

          /* Handle the (very unlikely) case our timeout works out to the disabled value */
          if (ZAB_SERVICE_VND_OPEN(Service).timeoutExpiryMs == M_TIMEOUT_DISABLED)
            {
              ZAB_SERVICE_VND_OPEN(Service).timeoutExpiryMs++;
            }
        }
    }
  else
    {
      printError(Service, "Open: Error: Command failed at item 0x%02X with error 0x%04X. Resetting state machine\n", item, Status->Error);
      /* We had an error sending a command, so things are broken*/
      zabNcpOpen_Create(Status, Service);
    }
}

/******************************************************************************
 * Set network state and notify the manage sap
 ******************************************************************************/
static void setAndNotifyOpenState(erStatus* Status, zabService* Service, zabOpenState openState)
{
  ZAB_SERVICE_VND_OPEN(Service).openState = openState;
  sapMsgPutNotifyOpenState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, openState);
}

/******************************************************************************
 * Close due to an Error
 * This can be called by the vendor code when a critical error happens and
 * ZAB needs to be re-initialised. Typically failure of serial comms.
 ******************************************************************************/
static void zabNcpOpen_CloseDueToError(erStatus* Status, zabService* Service, zabError Error)
{
  /* Use a local status for sending messages as the parametised one is an error and will cause message creation to fail*/
  erStatus localStatus;
  erStatusClear(&localStatus, Service);

  /* Notify the app of the error that has caused the closure */
  sapMsgPutNotifyError(&localStatus, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, Error);

  /* Notify the app we are closing */
  setAndNotifyOpenState(&localStatus, Service, ZAB_OPEN_STATE_CLOSING);

  /* Close serial glue */
  sapMsgPutAction(&localStatus, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_CLOSE);
  checkStatusSetItem(&localStatus, Service, ZAB_OPEN_CURRENT_ITEM_CLOSE_SERIAL_GLUE, ZAB_VND_CFG_M_OPEN_SERIAL_GLUE_TIMEOUT_MS);
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 * Create
 * Initialises open state / state machine
 ******************************************************************************/
void zabNcpOpen_Create(erStatus* Status, zabService* Service)
{
  printVendor(Service, "zabNcpOpen_Create\n");
  setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_CLOSED);
  ZAB_SERVICE_VND_OPEN(Service).currentItem = ZAB_OPEN_CURRENT_ITEM_NONE;
  ZAB_SERVICE_VND_OPEN(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
  osTimeGetMilliseconds(Service, &ZAB_SERVICE_VND_OPEN(Service).lastNetworkProcessorPingTimeMs);
  ZAB_SERVICE_VND_OPEN(Service).networkProcessorPingFailCount = 0;
  ZAB_SERVICE_VND_OPEN(Service).networkProcessorPingTimeMs = ZAB_VND_CFG_M_NET_PROC_PING_MS;
}

/******************************************************************************
 * Get Open State
 ******************************************************************************/
zabOpenState zabNcpOpen_GetOpenState(zabService* Service)
{
  return ZAB_SERVICE_VND_OPEN(Service).openState;
}

/******************************************************************************
 * Update timeout
 * Check for timeouts within the open state machine
 ******************************************************************************/
unsigned32 zabNcpOpen_UpdateTimeout(erStatus* Status, zabService* Service, unsigned32 Time)
{
  unsigned32 timeoutRemaining = ZAB_WORK_DELAY_MAX_MS;
  zabOpenCurrentItem currentItem = GET_OPEN_ITEM(Service);
  ER_CHECK_STATUS_VALUE(Status, timeoutRemaining);
  ER_CHECK_NULL_VALUE(Status, Service, timeoutRemaining);

  if (ZAB_SERVICE_VND_OPEN(Service).timeoutExpiryMs != M_TIMEOUT_DISABLED)
    {
      /* Check timeout while handling wrapped time*/
      timeoutRemaining = ZAB_SERVICE_VND_OPEN(Service).timeoutExpiryMs - Time;
      if ( (timeoutRemaining == 0) || (timeoutRemaining > ZAB_VND_MS_TIMEOUT_MAX) )
        {
          printVendor(Service, "zabNcpOpen_UpdateTimeout: Timeout expired for item %d\n", currentItem);

          setInactive(Service);

          /* Ask for fast work if timer expired as it may have triggered some other event */
          timeoutRemaining = 0;

          /* Deal with timeouts based on the item we were waiting for */
          switch(currentItem)
            {
              case ZAB_OPEN_CURRENT_ITEM_OPEN_SERIAL_GLUE:
                /* Open failed, so close glue and notify app */
                zabNcpOpen_CloseDueToError(Status, Service, ZAB_ERROR_SERIAL_GLUE_ACTION_TIMEOUT);
                break;

              case ZAB_OPEN_CURRENT_ITEM_CLOSE_SERIAL_GLUE:
                /* Close failed. Not much we can do! */
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_SERIAL_GLUE_ACTION_TIMEOUT);

                // Simulate the action from the glue so everything is cleaned up consistently
                zabNcpOpen_InNotificationHandler(Status, Service, ZAB_OPEN_STATE_CLOSED);
                break;

              /* ASH reset failed ,so maybe it's the bootloader running */
              case ZAB_OPEN_CURRENT_ITEM_OPENING_SOFT_RESET:

                /* Reset xmodem service, switch serial parser to bootloader mode and try a carriage return */
                zabNcpXModem_Reset(Status, Service);
                sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_INTERNAL_SERIAL_MODE_BOOTLOADER);
                zabNcpXModem_SendCarriageReturn(Status, Service);

                checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_OPENING_TEST_FOR_SBL, ZAB_VND_CFG_M_OPEN_SERIAL_GLUE_TIMEOUT_MS);
                break;

              /* Test for SBL failed, so revert to normal mode and fall through to close */
              case ZAB_OPEN_CURRENT_ITEM_OPENING_TEST_FOR_SBL:
                sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_INTERNAL_SERIAL_MODE_NORMAL);
              /* Opening failed, so tell serial glue to close*/
              case ZAB_OPEN_CURRENT_ITEM_OPENING_VERSION:
              case ZAB_OPEN_CURRENT_ITEM_OPENING_IEEE:
                sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_CLOSE);
                zabNcpOpen_Create(Status, Service);
                break;

              /* Ping failed. BAckoff+Retry or close if we have reached to limit */
              case ZAB_OPEN_CURRENT_ITEM_PING_SYS_VERSION:
                GET_PING_FAIL_COUNT(Service)++;
                printVendor(Service, "Network Processor Ping - Send timeout %d\n", GET_PING_FAIL_COUNT(Service));
                if (GET_PING_FAIL_COUNT(Service) >= ZAB_VND_CFG_M_NET_PROC_PING_FAIL_LIMIT)
                  {
                    zabNcpOpen_CloseDueToError(Status, Service, ZAB_ERROR_VENDOR_TIMEOUT);
                  }
                else
                  {
                    ZAB_SERVICE_VND_OPEN(Service).currentItem = ZAB_OPEN_CURRENT_ITEM_NONE;
                    timeoutRemaining = ZAB_VND_CFG_M_NET_PROC_PING_BACKOFF_MS;
                    GET_LAST_PING_TIME(Service) =  (Time - PING_TIME_MS(Service)) + ZAB_VND_CFG_M_NET_PROC_PING_BACKOFF_MS;
                  }
                break;

              default:
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
                zabNcpOpen_Create(Status, Service);
            }
        }
    }
  return timeoutRemaining;
}


/******************************************************************************
 * Init network processor ping service time
 ******************************************************************************/
void zabNcpOpen_InitNetworkProcessorPing(erStatus* Status, zabService* Service, unsigned16 PingTimeMs)
{
  unsigned32 time;

  printVendor(Service, "Network Processor Ping - Init = %dms\n", PingTimeMs);

  if (PingTimeMs >= M_PING_TIME_MINIMUM_MS)
    {
      PING_TIME_MS(Service) = PingTimeMs;
      osTimeGetMilliseconds(Service, &time);
      GET_LAST_PING_TIME(Service) = time;
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
    }
}


/******************************************************************************
 * Run network processor ping service
 ******************************************************************************/
unsigned32 zabNcpOpen_UpdateNetworkProcessorPing(erStatus* Status, zabService* Service, unsigned32 Time)
{
  zabOpenState openState;
  unsigned32 timeoutRemaining = ZAB_WORK_DELAY_MAX_MS;
  zab_bool pingDue;
  erStatus LocalStatus;
  erStatusClear(&LocalStatus, Service);
  ER_CHECK_STATUS_VALUE(Status, timeoutRemaining);
  ER_CHECK_NULL_VALUE(Status, Service, timeoutRemaining);

  if (PING_TIME_MS(Service) < ZAB_OPEN_M_NET_PROC_PING_DISABLED)
    {
      /* First check if we are in a state when a ping is desired */
      openState = GET_OPEN_STATE(Service);
      if ( (openState == ZAB_OPEN_STATE_OPENED) ||
           (openState == ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE) )
        {
          /* Calculate if a ping is due to be sent */
          pingDue = zab_false;
          if ( (Time+PING_TIME_MS(Service)) > GET_LAST_PING_TIME(Service))
            {
              if (Time >= (GET_LAST_PING_TIME(Service) + PING_TIME_MS(Service)))
                {
                  pingDue = zab_true;
                }
              else
                {
                  timeoutRemaining = (GET_LAST_PING_TIME(Service) + PING_TIME_MS(Service)) - Time;
                }
            }
          /* Time is close to wrapping, just update the last tick time to timeNow until timeNow gets to zero */
          else
            {
              GET_LAST_PING_TIME(Service) = Time;
              timeoutRemaining = PING_TIME_MS(Service);
            }

          if (pingDue == zab_true)
            {
              /* Now check if we are ok to send it now or need to delay as something else is going on */
              if ( (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_NONE) &&
                   (zabNcpNwk_GetNwkStateMachineInProgress(Service) == zab_false) &&
                   (zabNcpEzsp_GetSerialLinkLocked(Service) == zab_false) )
                {
                  printVendor(Service, "Network Processor Ping - Send ping!!!\n");

                  /* Ping network processor */
                  zabNcpEzspConfiguration_Version(&LocalStatus, Service);
                  if (erStatusIsOk(&LocalStatus))
                    {
                      GET_LAST_PING_TIME(Service) = Time;
                      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_PING_SYS_VERSION, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
                    }
                  /* If it fails it could be just a temporary full buffer scenario, or maybe that the glue (or something else) is broken.
                   * Increment fail count and backoff for some time */
                  else
                    {
                      GET_PING_FAIL_COUNT(Service)++;

                      printVendor(Service, "Network Processor Ping - Send Failed %d\n", GET_PING_FAIL_COUNT(Service));

                      if (GET_PING_FAIL_COUNT(Service) >= ZAB_VND_CFG_M_NET_PROC_PING_FAIL_LIMIT)
                        {
                          zabNcpOpen_CloseDueToError(Status, Service, ZAB_ERROR_VENDOR_TIMEOUT);
                        }
                      else
                        {
                          timeoutRemaining = ZAB_VND_CFG_M_NET_PROC_PING_BACKOFF_MS;
                          GET_LAST_PING_TIME(Service) += ZAB_VND_CFG_M_NET_PROC_PING_BACKOFF_MS;
                        }
                    }
                }
              else
                {
                  /* If delayed lets have fairly fast work.
                   * Does not have to be incredibly quick as this is not a critical process. */
                  timeoutRemaining = 100;
                }
            }
        }
    }
  return timeoutRemaining;
}

/******************************************************************************
 * Action
 * Handles Open and Close actions for the open state machines
 ******************************************************************************/
void zabNcpOpen_Action(erStatus* Status, zabService* Service, unsigned8 Action)
{
  unsigned8 stopNetworkProcessor;
  ER_CHECK_STATUS(Status);
  ER_CHECK_NULL(Status, Service);

  switch(Action)
    {
      /*
      ** Open
      */
      case ZAB_ACTION_OPEN:
        /* Normal open */
        if (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_CLOSED)
          {
            setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_OPENING);
            sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_OPEN);
            checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_OPEN_SERIAL_GLUE, ZAB_VND_CFG_M_OPEN_SERIAL_GLUE_TIMEOUT_MS);
          }
        /* Re-open after firmware update */
        else if (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE)
          {
            setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_OPENING);

            /* Switch serial parser back to normal mode */
            sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_INTERNAL_SERIAL_MODE_NORMAL);

            /* Jump into the opening process as if serial glue is just opened */
            zabNcpAsh_Reset(Status, Service);
            checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_OPENING_SOFT_RESET, ZAB_VND_CFG_M_OPEN_SOFT_RST_TIMEOUT_MS);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
            setAndNotifyOpenState(Status, Service, GET_OPEN_STATE(Service));
          }
        break;

      /*
      ** Close
      */
      case ZAB_ACTION_CLOSE:
        if (GET_OPEN_STATE(Service) != ZAB_OPEN_STATE_CLOSED)
          {
            /* Only ask about stopping the network processor if it is open - if not its not running anyway */
            stopNetworkProcessor = zab_false;
            if (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_OPENED)
              {
                stopNetworkProcessor = (unsigned8)srvCoreAskBack8( Status, Service, ZAB_ASK_NETWORK_PROCESSOR_STOP_ON_CLOSE, zab_true);
                printVendor(Service, "ZAB Znp Open: Ask Network Processor Stop on Close = 0x%02X", stopNetworkProcessor);
              }

            setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_CLOSING);

            if (stopNetworkProcessor)
              {
                /* Soft reset NCP */
                zabNcpAsh_Reset(Status, Service);
                checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_CLOSING_SOFT_RESET, ZAB_VND_CFG_M_OPEN_SOFT_RST_TIMEOUT_MS);
              }
            else
              {
                sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_CLOSE);
                checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_CLOSE_SERIAL_GLUE, ZAB_VND_CFG_M_OPEN_SERIAL_GLUE_TIMEOUT_MS);
              }
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
            setAndNotifyOpenState(Status, Service, GET_OPEN_STATE(Service));
          }
        break;

      /*
      ** Go To Firmware Update
      */
      case ZAB_ACTION_GO_TO_FIRMWARE_UPDATER:
        if ( (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_OPENED) ||
             (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE) )
          {
            setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_GOING_TO_FW_UPDATE);
            zabNcpEzspConfiguration_LaunchStandaloneBootloader(Status, Service);
            checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_GO_TO_SBL, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
            setAndNotifyOpenState(Status, Service, GET_OPEN_STATE(Service));
          }
        break;
    }
}


/******************************************************************************
 * In Notification Handler
 * Handles notification from the serial glue for the open state machine
 ******************************************************************************/
void zabNcpOpen_InNotificationHandler(erStatus* Status, zabService* Service, zabOpenState OpenState)
{
  if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_OPEN_SERIAL_GLUE)
    {
      if (OpenState == ZAB_OPEN_STATE_OPENED)
        {
          /* We're open with the glue, now send a soft reset */
          zabNcpAsh_Reset(Status, Service);
          checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_OPENING_SOFT_RESET, ZAB_VND_CFG_M_OPEN_SOFT_RST_TIMEOUT_MS);
        }
      else
        {
          zabNcpNwk_Create(Status, Service);
          zabNcpOpen_Create(Status, Service);
        }
    }

  /* Default Close */
  else if (OpenState == ZAB_OPEN_STATE_CLOSED)
    {
      zabNcpNwk_Create(Status, Service);
      zabNcpOpen_Create(Status, Service);
    }
}

/******************************************************************************
 * Reopen Request
 * This allows other services to request re-opening of the connection.
 * For example, after a network leave is performed.
 ******************************************************************************/
void zabNcpOpen_ReopenRequest(erStatus* Status, zabService* Service)
{
  if ( (zabNcpOpen_GetOpenState(Service) == ZAB_OPEN_STATE_OPENED) || (zabNcpOpen_GetOpenState(Service) == ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE) )
    {
      /* Reset during normal operation, or leave from network state machine. Silently re-init */
      zabNcpAsh_Reset(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_OPENING_SOFT_RESET_SILENT, ZAB_VND_CFG_M_OPEN_SOFT_RST_TIMEOUT_MS);
    }
  else
    {
      zabNcpOpen_CloseDueToError(Status, Service, ZAB_ERROR_VENDOR_NWK_PROC_RESET);
    }
}

/******************************************************************************
 * Reset Indication Handler
 ******************************************************************************/
void zabNcpOpen_ResetIndicationHandler(erStatus* Status,
                                       zabService* Service,
                                       unsigned8 ReasonForReset)
{
  if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_OPENING_SOFT_RESET)
    {
      /* Reset done, so get version */
      zabNcpEzspConfiguration_Version(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_OPENING_VERSION, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
    }
  else if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_CLOSING_SOFT_RESET)
    {
      /* Reset done, so close glue */
      sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_CLOSE);
      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_CLOSE_SERIAL_GLUE, ZAB_VND_CFG_M_OPEN_SERIAL_GLUE_TIMEOUT_MS);
    }
  else if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_OPENING_SOFT_RESET_SILENT)
    {
      /* If we either got an a-synchronous reset, or did it ourselves as part of a silent reopen, then continue with version  */
      zabNcpEzspConfiguration_Version(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_OPENING_VERSION_SILENT, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
    }
  else if ( (zabNcpOpen_GetOpenState(Service) == ZAB_OPEN_STATE_OPENED) || (zabNcpOpen_GetOpenState(Service) == ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE) )
    {
      /* Reset/Disconnect during normal operation, or leave from network state machine. Silently re-init */
      zabNcpOpen_ReopenRequest(Status, Service);
    }
  else
    {
      zabNcpOpen_CloseDueToError(Status, Service, ZAB_ERROR_VENDOR_NWK_PROC_RESET);
    }
}

/******************************************************************************
 * Version Response Handler
 ******************************************************************************/
void zabNcpOpen_VersionHandler(erStatus* Status,
                               zabService* Service,
                               unsigned16 StackVersion)
{
  unsigned8 versionArray[4];

  if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_OPENING_VERSION)
    {
      setInactive(Service);

      /* Version info */
      srvCoreGiveBack8(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ACTIVE_APP_TYPE, (unsigned8)ZAB_NET_PROC_APP_ZIGBEE_PRO);
      srvCoreGiveBack8(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_MODEL, (unsigned8)ZAB_NET_PROC_MODEL_SILABS_EFR32_NCP);

      versionArray[0] = (StackVersion >> 12) & 0x000F;
      versionArray[1] = (StackVersion >> 8)  & 0x000F;
      versionArray[2] = (StackVersion >> 4)  & 0x000F;
      versionArray[3] = StackVersion & 0x000F;
      srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_VERSION, sizeof(versionArray), versionArray);

      /* Now get IEEE */
      zabNcpEzspUtilities_GetNodeIeee(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_OPENING_IEEE, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
    }
  else if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_OPENING_VERSION_SILENT)
    {
      setInactive(Service);
      zabNcpNwk_Create(Status, Service);
      setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_OPENED);
    }
  else if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_PING_SYS_VERSION)
    {
      GET_PING_FAIL_COUNT(Service) = 0;
      setInactive(Service);
    }
}

/******************************************************************************
 * IEEE Response Handler
 ******************************************************************************/
void zabNcpOpen_GetIeeeAddressResponseHandler(erStatus* Status, zabService* Service, unsigned64 IeeeAddress)
{
  if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_OPENING_IEEE)
    {
      setInactive(Service);

      srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_IEEE, 8, (unsigned8*)&IeeeAddress);

      zabNcpNwk_ResetHandler(Status, Service);

      /* Finished! */
      setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_OPENED);
    }
}

/******************************************************************************
 * Launch Bootloader Response Handler
 ******************************************************************************/
void zabNcpOpen_LaunchStandaloneBootloaderResponseHandler(erStatus* Status,
                                                          zabService* Service,
                                                          unsigned8 BootloaderStatus)
{
  if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_GO_TO_SBL)
    {
      if (BootloaderStatus == EMBER_SUCCESS)
        {
          zabNcpXModem_Reset(Status, Service);
          sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_INTERNAL_SERIAL_MODE_BOOTLOADER);
          zabNcpXModem_SendCarriageReturn(Status, Service);

          checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_GO_TO_SBL_AWAIT_PROMPT, ZAB_VND_CFG_M_OPEN_SERIAL_GLUE_TIMEOUT_MS);
        }
      else
        {
          sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_CLOSE);
          checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_CLOSE_SERIAL_GLUE, ZAB_VND_CFG_M_OPEN_SERIAL_GLUE_TIMEOUT_MS);
        }
    }
}

/******************************************************************************
 * Bootloader Prompt Received Handler
 ******************************************************************************/
void zabNcpOpen_SblPromptReceivedHandler(erStatus* Status,
                                         zabService* Service)
{
  if ( (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_GO_TO_SBL_AWAIT_PROMPT) ||
       (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_OPENING_TEST_FOR_SBL) )
    {
      setInactive(Service);

      zabNcpSbl_Create(Status, Service);
      setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE);
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/