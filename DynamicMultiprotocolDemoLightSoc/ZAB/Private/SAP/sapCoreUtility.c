/*
 Name:    Service Access Point Management Utility
 Author:  Cam Williams
 Company: Schneider Electric

 Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

 Description:

 * 002.002.010  09-Oct-15   MvdB   ARTF150980: Support Service ID in memory allocation functions for WTB
 */

#include "sapCoreUtility.h"
#include "sapCorePrivate.h"

#include "osMemUtility.h"
#include "osTypes.h"

/* get MutexId from a SAP 
 */
unsigned8 sapCoreGetMutexId (sapHandle Sap)
{
    return SAP_STRUCT(Sap)->MutexId;
}

/* get service from a SAP 
 */
sapService sapCoreGetService (sapHandle Sap)
{
    return SAP_SERVICE(Sap);
}

void sapCoreRegisterCallBack (erStatus* Status, sapHandle Sap, sapMsgDirection Direction, sapCoreCallBack CallBack)
{

    sapStruct* This = SAP_STRUCT(Sap);
    unsigned8 i ;

    ER_CHECK_STATUS_NULL(Status, This);
    i = This->ConnectMax;
    
    while (i--)
    {
        sapAccessStruct* Access = &This->Access[ i ];

        if (Access->CallBack == NULL) // if record available
        {
            Access->CallBack = CallBack;
            Access->FlagsDir = Direction & SAP_MSG_DIRECTION_MASK;
            return;
        }
    }

    erStatusSet(Status, SAP_ERROR_ACCESS_OVERFLOW);

}

void sapCorePut (erStatus* Status, sapHandle Sap, sapMsgDirection Direction, sapMsg* Message)
{
    sapStruct* This = SAP_STRUCT(Sap);
    unsigned8 i = This->ConnectMax;

    ER_CHECK_NULL(Status, Message);

    while (i--)
    {
        sapAccessStruct* Access = &This->Access[ i ];
        sapCoreCallBack CallBack = Access->CallBack;
        unsigned8 Match = SAP_MATCH_DIR(Access, Direction);

        if (CallBack && Match) // if output in right direction and there is an input function to receive
        {
            CallBack(Status, Sap, Message); // send it into the service input function
        }
    }
}

/* This function creates an interface. The only requirements is a context pointer to the service (even null is ok),
   and these function pointers: input function to pass a message into the service, and a function to register and output
   function for a service call back. 
 */
sapHandle sapCoreCreate (erStatus* Status, sapService Service, unsigned32 AccessCount, unsigned8 MutexId)
{
    unsigned32 Size = SAP_SIZE(AccessCount);
    sapStruct* Struct;

    Struct = (sapStruct*) OS_MEM_MALLOC(Status, 
                                        srvCoreGetServiceId(Status, Service),
                                        Size, 
                                        MALLOC_ID_CFG_SAP);
    ER_CHECK_STATUS_NULL_ZERO(Status, Struct);

    Struct->Service = Service;
    Struct->ConnectMax = (unsigned8) AccessCount;
    Struct->MutexId = MutexId;

    return (sapHandle) Struct;
}

