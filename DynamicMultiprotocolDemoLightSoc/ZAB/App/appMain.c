/******************************************************************************
 *                          ZAB TEST APPLICATION
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the main entry point for the test console, and:
 *    - Functions to launch serial and work threads
 *    - ZAB Initialisation
 *    - Console management
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 *****************************************************************************/
#include "appOsMemoryGlue.h"
#include "appMain.h"
#include "appConfig.h"
#include "mutex.h"
#include "appGp.h"
#include "app/framework/include/af.h"

#include "appZabCoreGlue.h"

#include "zabVendorConfigureZnp.h"

#include "szl_zab_types.h"
#include "appZcl.h"
#include "appCmdLine.h"
#include "appGateway.h"
 /******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/

/* Time (in milli-seconds) work threrad should sleep for before waking to check
 * another work is required. This effects how long after some applciation event the next
 * work actually happens.
 * WARNING: If you set this to 2 or less things will run slowly. It's not very clear
 *          why, but probably as usleep suspends for AT LEAST the specified time
 *          and there is more than 2ms of overheads. */
#define WORK_THREAD_SLEEP_MS 10
   
/******************************************************************************
 *                      ******************************
 *                 *****       LOCAL VARIABLES        *****
 *                      ******************************
 ******************************************************************************/   
 /* The state for main thread */
static erStatus CoreStatus;
 /* Boolean to notify work thread that work is required now due to some application event */
zab_bool workRequested;



 /******************************************************************************
 *                      ******************************
 *                 *****       EXTERN VARIABLES       *****
 *                      ******************************
 ******************************************************************************/
   /* Service pointer for our ZAB instance */
zabService* appService = NULL;

/* Variable to allow pausing of calling of work function.
 * Useful for testing full queues */
zab_bool appMain_WorkEnabled = zab_true;

//uint8_t nextToWrite = 0;
//uint8_t nextToRead = 0;
//SZL_AttributeReportNtfParams_t AttributReport[NB_MAX_REPORT];

OS_FLAG_GRP MyEventFlagGrp;

/*ZB_Report_t report[NB_MAX_REPORT];
uint8_t report_nb = 0;*/

uint8_t HeapMsgQ[MSGQ_HEAP_SIZE];
uint16_t current=0;
uint8_t asnValue=0;

uint8_t asnAndIid[20][3];
uint8_t lastTidToAsn=0;
/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Notify thread that work is required as a ZAB API has been called
 ******************************************************************************/
void appMain_WorkRequired(void)
{
  workRequested = zab_true;
}

/******************************************************************************
 * Initialise ZAB
 ******************************************************************************/
static void zabInitial(void)
{
  appService = NULL;
  sapHandle appManageSAP;

  erStatusClear(&CoreStatus, appService); 
  /* Create an instance of ZAB and save the service pointer to it */
  appService = zabCoreCreate(&CoreStatus,
                             APP_CONFIG_M_SERVICE_ID,
                             appAskCfg,
                             appGiveCfg,
                             appZabCoreGlue_SerialOutActionHandler,
                             appZabCoreGlue_SerialOutDataHandler);
  if (appService != NULL)
    {
      erStatusClear(&CoreStatus, appService);

      /* Get the management SAP and register an IN handler for notification */
      appManageSAP = zabCoreSapManage(appService);
      sapCoreRegisterCallBack(&CoreStatus, appManageSAP, SAP_MSG_DIRECTION_IN, appZabManageCallBack);


      /* Init application modules that need to register callbacks */
#ifdef APP_CONFIG_ENABLE_CLONING
      appClone_Init();
#endif
    }
  else
    {
      printApp(appService, "ERROR: Failed to create ZAB. Terminating.\n");
    }
  
}
/******************************************************************************
 * Get the Service ID of a ZAB instance
 ******************************************************************************/
unsigned8 appMain_GetServiceId(zabService* Service)
{
  erStatus LocalStatus;
  erStatusClear(&LocalStatus, Service);

  // We could just use APP_CONFIG_M_SERVICE_ID, but this is a better example
  return srvCoreGetServiceId(&LocalStatus, Service );
}
static uint32_t alternate = 0;
void printReceive()
{
  alternate+=1;
  if((zabCoreGetNetworkState(appService) == ZAB_NWK_STATE_NETWORKING) && (alternate%50==0))
  {
    alternate = 0;
    printApp(appService,"serial OK\n");
  }
}

void initAppZigbee(void)
{
 // platDebug_DebugInfoOn = zab_true;
  //platDebug_DebugSerialOn = zab_true;
  //  platDebug_DebugSapOn = zab_true;
  //    platDebug_DebugVendorOn = zab_true;
  
  /* Init Variable */
  memset(asnAndIid,0,20*3);
  
  /* Init malloc simulator */
  appOsMemoryGlue_LocalMallocInit();
  
  /* Set default application configuration */
  appConfig_Init();
  
  MutexInit();
	
#ifdef APP_CONFIG_ENABLE_GREEN_POWER
  appGp_Init();
#endif	

  /* Init ZAB */
  zabInitial();
 
}
  
void startSerial(void)
{
  emberAfCorePrintln("RTOS App Task 1: startSerial.");
  appGateway_Start();
}
  
/******************************************************************************
 * Thread to call ZAB work
 ******************************************************************************/
void * zabProcess(void )
{
  unsigned32 msDelayToNextWork = 0;
  workRequested = zab_false;
  unsigned32 sleepTime;
  //appConsoleMain_SeedRand();

  while (1)
    {
      /* If we have a valid instance of ZAB (appService not null):
       *  - Perform ZAB work
       *    - If WorkEnabled is true. This allows testing of full queues etc.
       *  - Check for any errors. If found, print it and clear for the next cycle */
      if ( appService &&
          ( (msDelayToNextWork == 0) || (workRequested == zab_true) ) &&
          (appMain_WorkEnabled == zab_true) )
        {
          workRequested = zab_false;
          msDelayToNextWork = zabCoreWork(&CoreStatus, appService);

          if(erStatusIsError(&CoreStatus))
            {
              printApp(appService, "APP: Warning clearing error 0x%04X from last execution\n", CoreStatus.Error);
              /* Parse error, log it, restart etc. */
              erStatusClear(&CoreStatus, appService);
            }
        }

      /*
       * Do a sleep so we do not consume a whole core!
       * Sleep for WORK_THREAD_SLEEP_MS or msDelayToNextWork, whichever is smaller
       */
      if (workRequested == zab_false)
        {
          if (msDelayToNextWork > WORK_THREAD_SLEEP_MS)
            {
              /* Decrement before the sleep incase something else accesses it while we are asleep */
              msDelayToNextWork -= WORK_THREAD_SLEEP_MS;
              sleepTime = WORK_THREAD_SLEEP_MS;
              emberRtosIdleHandler(&sleepTime);
            }
          else
            {
              if(msDelayToNextWork==0)
              {
                msDelayToNextWork=1;  //let other task to work
              }
              emberRtosIdleHandler(&msDelayToNextWork);
            }
        }

      /* Run safety checks on our Malloc simulator */
      appOsMemoryGlue_LocalMallocCheck();
    }
}  
                     
void serialReader(unsigned8 realRcvLength,unsigned8* rcvBuff)
{
  erStatus serialStatus;
  erStatusClear(&serialStatus, appService);
  //emberAfCorePrintln("RTOS App Task 3: receive data.");
#ifdef INCLUDE_SERIAL_DEBUG
    if (realRcvLength > 0)
    {
      int i;
      char* myString = NULL;
      char* myStringEnd;
      myString = (char*)malloc(50 + (realRcvLength * 3));
      if (myString != NULL)
        {
          myStringEnd = myString;
          myStringEnd += sprintf(myStringEnd, "READ  <--");
          for (i = 0; i < realRcvLength; i++)
          {
              myStringEnd += sprintf(myStringEnd, " %02x", (unsigned int)rcvBuff[i]);
          }
          myStringEnd += sprintf(myStringEnd, "\n");
          platPrintSerial(appService, myString);
          free(myString);
        }
    }
#endif  
  if (zabSerialService_SerialReceiveHandler(&serialStatus, appService, realRcvLength, rcvBuff) == ZAB_SERIAL_RECEIVE_WORK_READY)
    {
      appMain_WorkRequired();
    }
}










uint8_t formNetworkCmd(uint8_t argument,unsigned64 Epid, uint8_t channel, uint16_t panId)
{
  erStatus status;
  erStatusClear(&status, appService);
  
  if((argument&FORM_ARG_EPID)==FORM_ARG_EPID)
  {
    appConfig_Config.extPanId = Epid;
  }
      
  if((argument&FORM_ARG_CHAN)==FORM_ARG_CHAN)
  {
    appConfig_Config.channelNumber = channel;
  }
      
  if((argument&FORM_ARG_PANID)==FORM_ARG_PANID)
  {
    appConfig_Config.panId = panId;
  }
  
  if(zabCoreGetNetworkState(appService) ==  ZAB_NWK_STATE_NONE)
  {
    appConfig_Config.nwkResume = 0;
    zabCoreAction(&status, appService, ZAB_ACTION_NWK_INIT);
    return true;
  }
  
  return false;    
}
 

uint8_t permitJoin(uint16_t timeForJoin)
{
  erStatus status;
  erStatusClear(&status, appService);
  
  if(zabCoreGetNetworkState(appService) ==  ZAB_NWK_STATE_NETWORKED)
  {
    appConfig_Config.permitJoinTime = timeForJoin;
    zabCoreAction(&status, appService, ZAB_ACTION_NWK_PERMIT_JOIN_ENABLE);
    return true;
  }
  return false;
}




uint8_t GetAsnAndTidPosition(void)
{
  uint8_t i;
  
  for(i=0;i<20;i++)
  {
    //asnAndIid[20][3];
    if(asnAndIid[i][0] == 0)
    {
      return i;
    }
  }
  return 255;
}
void sendDefaultResp(uint8_t tid, uint8_t res)
{
  uint8_t i;
  uint8_t tmpPosition;
  
  for(i=0;i<20;i++)
  {
    if((asnAndIid[i][0] == 1) && (asnAndIid[i][2] == tid))
    {
      tmpPosition = current;
      HeapMsgQ[tmpPosition] = APP_INTER_ZIGBEE;
      tmpPosition += 1;
      
      HeapMsgQ[tmpPosition] = DEFAULT_RESP;
      tmpPosition += 1;
      
      HeapMsgQ[tmpPosition] = asnAndIid[i][1];
      tmpPosition += 1;
      
      HeapMsgQ[tmpPosition] = res;
      tmpPosition += 1;
      
      sendMsgQ(&ZigbeeToPlcMsgQ, &HeapMsgQ[current], (tmpPosition-current));
      current = tmpPosition; 
      asnAndIid[i][0] = 0;
    }  
  } 
}

void manageClusterCmd(uint8_t* command, uint8_t cmdSize)
{
  uint8_t tmpPosition;
  uint8_t asnReceived;
  SZL_ClusterCmdReqParams_t cmdReq;
  uint8_t option;
  SZL_RESULT_t res;
  szl_uint8 tid = SZL_ZAB_M_TRANSACTION_ID_INVALID;
  uint8_t storeAsn;
  
  //if it's not a zigbee interface or not a cluster cmd, command received not OK
  if((command[0] != APP_INTER_ZIGBEE) || (command[1] != CLUSTER_CMD))
  {
    return;
  }
  tmpPosition = 2;
  
  asnReceived = command[tmpPosition];  
  tmpPosition += 1;
  
  //if the reserved bits are not at 0, command received not OK
  if((command[tmpPosition]&CLUSTER_CMD_OPTION_RESERVED_MASK)!=0)
  {
    return;
  }
  //recup option
  option = command[tmpPosition];
  tmpPosition += 1;
  if((option & CL_OPT_DIRECTION) == CL_OPT_DIRECTION)
  {
    cmdReq.Direction = ZCL_FRAME_DIR_SERVER_CLIENT;
  }
  else
  {
    cmdReq.Direction = ZCL_FRAME_DIR_CLIENT_SERVER;
  }
  
  //src ep
  if((option & CL_OPT_SRC_EP) == CL_OPT_SRC_EP)
  {
    cmdReq.SourceEndpoint = command[tmpPosition];
    tmpPosition += 1;
  }
  else
  {
    cmdReq.SourceEndpoint = 1;
  }
  //dest@
  if((option & CL_OPT_DEST_ADR_MASK) == CL_OPT_DEST_ADR_SRCID)
  {
    cmdReq.DestAddrMode.AddressMode = SZL_ADDRESS_MODE_GP_SOURCE_ID;

    cmdReq.DestAddrMode.Addresses.GpSourceId.SourceId  = (command[tmpPosition]<<24)+(command[tmpPosition+1]<<16)+(command[tmpPosition+2]<<8)+command[tmpPosition+3];
    current += sizeof(cmdReq.DestAddrMode.Addresses.GpSourceId.SourceId);
  }
  else if((option & CL_OPT_DEST_ADR_MASK) == CL_OPT_DEST_ADR_IEEE)
  {
    cmdReq.DestAddrMode.AddressMode = SZL_ADDRESS_MODE_GP_SOURCE_ID;

    cmdReq.DestAddrMode.Addresses.IEEE.Address = (command[tmpPosition]<<16)+(command[tmpPosition+1]<<8)+command[tmpPosition+2];
    cmdReq.DestAddrMode.Addresses.IEEE.Address = (cmdReq.DestAddrMode.Addresses.IEEE.Address<<24) +(command[tmpPosition+3]<<16)+  (command[tmpPosition+4]<<8)+command[tmpPosition+5];
    cmdReq.DestAddrMode.Addresses.IEEE.Address = (cmdReq.DestAddrMode.Addresses.IEEE.Address<<16)   +(command[tmpPosition+6]<<8)+command[tmpPosition+7];
    tmpPosition += sizeof(cmdReq.DestAddrMode.Addresses.IEEE.Address);
    
    //dst EP 
    if((option & CL_OPT_DST_EP) == CL_OPT_DST_EP)
    {
      cmdReq.DestAddrMode.Addresses.IEEE.Endpoint = command[tmpPosition];
      tmpPosition +=1;
    }
    else
    {
      cmdReq.DestAddrMode.Addresses.IEEE.Endpoint = 1;
    }
  }
  else
  {
    //unknown dest addr
    return;
  }
    
  //clusterId
  cmdReq.ClusterID = ((command[tmpPosition]<<8)&0xff00)+command[tmpPosition+1];
  tmpPosition = 2;
  
  //cluster cmd
  cmdReq.Command = command[tmpPosition];
  tmpPosition += 1;
  
  //payload
  switch(cmdReq.ClusterID)
  {
    case ON_OFF_CLUSTER_ID:
      cmdReq.ManufacturerSpecific = 0;
      cmdReq.PayloadLength = 0;
      break;
    default: 
      return;
    
  }
 
  if((option & DEFAULT_RESP) == DEFAULT_RESP)
  {
    res = SZL_RESULT_FAILED;
    storeAsn = GetAsnAndTidPosition();
    
    if(storeAsn != 255)
    {
      res = SZL_ClusterCmdReq(appService, appZcl_ClusterCmdRspHandler, &cmdReq, &tid);
    }
    if(res != SZL_RESULT_SUCCESS)
    {
      tmpPosition = current;
      HeapMsgQ[tmpPosition] = APP_INTER_ZIGBEE;
      tmpPosition += 1;
      
      HeapMsgQ[tmpPosition] = DEFAULT_RESP;
      tmpPosition += 1;
      
      HeapMsgQ[tmpPosition] = asnReceived;
      tmpPosition += 1;
      
      HeapMsgQ[tmpPosition] = res;
      tmpPosition += 1;
      
      sendMsgQ(&ZigbeeToPlcMsgQ, &HeapMsgQ[current], (tmpPosition-current));
      current = tmpPosition;
    }
    else
    {
      asnAndIid[storeAsn][0] = 1;
      asnAndIid[storeAsn][1] = asnReceived;
      asnAndIid[storeAsn][2] = tid;     
    }
  }
  else
  {
    res = SZL_ClusterCmdReq(appService, appZcl_ClusterCmdRspHandler, &cmdReq, &tid);
  }
}



