/*
 Name:    Safe Queue Implementation
 Author:  ZigBee Excellence Center
 Company: Schneider Electric

 Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

 Description:

 Light weight queue. Thread safe for multi-producers and multiple consumers

 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 * 002.000.008  15-Apr-15   MvdB   ARTF130931: Add ZAB_ASSERT macro in zabCoreConfigure.h
*/
#include "zabCoreService.h"
#include "qSafeUtility.h"

#include "zabCoreConfigure.h"

// Define the mutex macros below, if you wish to protect your Safe Circular Queue
#define Q_SAFE_LOCK( Service, id )   while (id) { ZAB_PROTECT_ENTER( (void*)(Service), (unsigned8)(id) ); break; }    // enters a mutex if id > 0
#define Q_SAFE_UNLOCK( Service, id ) while (id) { ZAB_PROTECT_EXIT(  (void*)(Service), (unsigned8)(id) ); break; }    // exits a mutex if id > 0

// helper functions not to be used
#define SET_NEXT( I, N ) do { ((qSafeItem*)(I))->Next = ((qSafeItem*)(N)); } while (0)
#define NEXT( I )             (((qSafeItem*)(I))->Next)

// clears and initializes queue
void qSafeInitialize( void* Service, qSafeType* Q, unsigned8 Id )
{
  printInfo(Service, "qSafeInitialize :: Id = %u\n", Id);
  Q_SAFE_LOCK( Service, Id );
  Q->Service = Service;
  Q->MutexId    = Id;  
  Q->First = NULL;
  Q->Last  = NULL;
  Q->Count = 0;
  Q_SAFE_UNLOCK( Service, Id );
}

void qSafeClear( qSafeType* Q )
{
  Q_SAFE_LOCK( Q->Service, Q->MutexId );
  Q->First = NULL;
  Q->Last  = NULL;
  Q->Count = 0;
  Q_SAFE_UNLOCK( Q->Service, Q->MutexId );
}

unsigned8 qSafeId( qSafeType* Q )
{
  return Q->MutexId;
}

unsigned8 qSafeCount( qSafeType* Q )
{
  unsigned8 Count;
  Q_SAFE_LOCK( Q->Service, Q->MutexId );
  Count = Q->Count;
  Q_SAFE_UNLOCK( Q->Service, Q->MutexId );
  return Count;
}

void qSafeAppend( qSafeType* Q, void* Item )
{
  SET_NEXT( Item, NULL );          // next points to null
  
  Q_SAFE_LOCK( Q->Service, Q->MutexId );
  
  if (Q->Last != NULL)                  // item(s) on queue
  {
    ZAB_ASSERT( Q->Count > 0 );
    SET_NEXT( Q->Last, Item );  // after last is item
  }
  else                          // queue empty
  {
    ZAB_ASSERT( Q->Count == 0 );
    Q->First = (qSafeItem*)Item;            // last and first the same
  }

  Q->Last = (qSafeItem*)Item;               // new last
  Q->Count++;
  
  Q_SAFE_UNLOCK( Q->Service, Q->MutexId );
}

void qSafePush( qSafeType* Q, void* Item )
{
  SET_NEXT( Item, 0 );          // next points to null
  
  Q_SAFE_LOCK( Q->Service, Q->MutexId );
  
  if (Q->Last)                  // item(s) on queue
  {
    ZAB_ASSERT( Q->Count > 0 );
    SET_NEXT( Item, Q->First ); // next points to first
  }
  else                          // queue empty
  {
    ZAB_ASSERT( Q->Count == 0 );
    Q->Last = (qSafeItem*)Item;             // last and first the same
  }

  Q->First = (qSafeItem*)Item;              // new first
  Q->Count++;
  Q_SAFE_UNLOCK( Q->Service, Q->MutexId );
}

void* qSafeFirst( qSafeType* Q )
{
  void* Item;

  Q_SAFE_LOCK( Q->Service, Q->MutexId );
  
  Item = Q->First;     // this is the first item
  
  Q_SAFE_UNLOCK( Q->Service, Q->MutexId );
  
  return Item;
}

void* qSafeRemove( qSafeType* Q )
{
  void* Item = NULL;

  Q_SAFE_LOCK( Q->Service, Q->MutexId );
  
  Item = Q->First;     // this is the first item
  
  if (Item != NULL)            // queue not empty
  {
    if (Q->Count == 1) // if one item on queue
    {
      ZAB_ASSERT( Q->First == Q->Last );
      Q->Last  = NULL;
      Q->First = NULL;
      Q->Count = 0;
    }
    else                       // more than one item on queue
    {
      ZAB_ASSERT( Q->Count > 1 );
      Q->First = NEXT( Item ); // next first
      Q->Count--;
    }
  }
  else                         // queue empty
  {
    ZAB_ASSERT( Q->Count == 0 );  
    Q_SAFE_UNLOCK( Q->Service, Q->MutexId );

    return Item;   //return Item here since it is a NULL pointer, we can't do more operation on it.
  }

  SET_NEXT( Item, NULL );        // next is zero if not on a queue
  Q_SAFE_UNLOCK( Q->Service, Q->MutexId );  

  return Item;
}
