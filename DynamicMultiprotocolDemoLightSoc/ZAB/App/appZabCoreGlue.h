#ifndef APPZABCOREGLUE_H_
#define APPZABCOREGLUE_H_

#include "zabCoreService.h"

/******************************************************************************
 * Ask Handler
 * Returns configuration values ASKED from the APP by ZAB
 ******************************************************************************/
extern
void appAskCfg(erStatus* Status, zabService* Service, zabAskId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer);

/******************************************************************************
 * Give Handler
 * Accepts configuration values GIVEN by ZAB to the app
 ******************************************************************************/
extern 
void appGiveCfg(erStatus* Status, zabService* Service, zabGiveId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer);

/******************************************************************************
 * Management SAP Callback.
 * This function is registered to handle notification coming IN from ZAB.
 ******************************************************************************/
extern 
void appZabManageCallBack(erStatus* Status, sapHandle Sap, sapMsg* Message);

/******************************************************************************
 * Serial Out Action Handler
 * This function is registered to handle actions coming out of ZAB to the serial glue.
 ******************************************************************************/
extern 
void appZabCoreGlue_SerialOutActionHandler(erStatus* Status, zabService* Service, zabAction Action);

/******************************************************************************
 * Serial Out Data Handler
 * This function is registered to handle serial data coming out of ZAB to the serial glue.
 ******************************************************************************/
extern 
void appZabCoreGlue_SerialOutDataHandler(erStatus* Status, zabService* Service, unsigned16 DataLength, unsigned8* Data);


#endif /* APPZABCOREGLUE_H_ */