/******************************************************************************
 *                          ZAB TEST APPLICATION
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains an example of ZAB being used in a typical gateway application.
 *   It will:
 *    - Open ZAB
 *    - Update the network processors firmware if it has a newer image available
 *    - Check GATEWAY_FILENAME for previous network info:
 *      - if file found and details match network settings in the dongle, then resume the network.
 *      - else form a new network.
 * 
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 002.002.005  1-Sep-15   MvdB   Original
 *****************************************************************************/

#ifndef _APP_GATEWAY_H_
#define _APP_GATEWAY_H_

#include "zabCoreService.h"

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Start the Gateway process
 ******************************************************************************/
extern 
void appGateway_Start(void);

/******************************************************************************
 * Handle Open State Notifications
 ******************************************************************************/
extern 
void appGateway_OpenStateNtfHandler(zabOpenState openState);

/******************************************************************************
 * Handle Network State Notifications
 ******************************************************************************/
extern 
void appGateway_NwkStateNtfHandler(zabNwkState nwkState);

/******************************************************************************
 * Handle Firmware Update State Notifications
 ******************************************************************************/
extern 
void appGateway_FwUpdateStateNtfHandler(zabFwUpdateState fwUpdateState);

/******************************************************************************
 * Handle Network Info State Notifications
 ******************************************************************************/
extern 
void appGateway_NetworkInfoStateNtfHandler(zabNetworkInfoState networkInfoState);

/******************************************************************************
 * Handle Asks
 ******************************************************************************/
extern 
zab_bool appGateway_AskCfgHandler(erStatus* Status, zabService* Service, zabAskId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer);


/******************************************************************************
 * Handle Gives
 ******************************************************************************/
extern 
void appGateway_GiveCfgHandler(erStatus* Status, zabService* Service, zabGiveId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer);

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif /* _APP_ZCL_BASIC_H_ */
