# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to manage the serial connection
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
import codecs
import sys

import serial

from threading import Lock, Thread
from serial.tools.list_ports import comports
from serial.tools import hexlify_codec
from queue import Queue, Empty
from frame import Command, Status
from frame_parser import check_crc, compute_crc
import time

codecs.register(lambda c: hexlify_codec.getregentry() if c == 'hexlify' else None)


def ask_for_port():
    """\
    Show a list of ports and ask the user for a choice. To make selection
    easier on systems with long device names, also allow the input of an
    index.
    """
    sys.stderr.write('\n--- Available ports:\n')
    ports = []
    for n, (port, desc, hwid) in enumerate(sorted(comports()), 1):
        sys.stderr.write('--- {:2}: {:20} {!r}\n'.format(n, port, desc))
        ports.append(port)
    while True:
        port = input('--- Enter port index or full name: ')
        try:
            index = int(port) - 1
            if not 0 <= index < len(ports):
                sys.stderr.write('--- Invalid index!\n')
                continue
        except ValueError:
            pass
        else:
            port = ports[index]
        return port


class SerialDriver(object):
    """\
    Terminal application. Copy data from serial port to console and vice versa.
    Handle special keys from the console to show menu etc.
    """

    def __init__(self, serial_instance, reference):
        self.serial = serial_instance
        self.engine_reference = reference
        self.queue = Queue()
        self.input_encoding = 'UTF-8'
        self.output_encoding = 'UTF-8'
        self.alive = None
        self._reader_alive = None
        self.receiver_thread = None
        self.process_thread = None
        self.sender_thread = None
        self.rx_decoder = None
        self.tx_decoder = None
        self.tx_encoder = None
        self.default_starter_frame = 0xFE
        # Read/Write management
        self.mutex = Lock()
        self.waiting_for_ack = False
        self.local_frame_counter = 0
        self.plc_frame_counter = -1
        self.retry_counter = 0

    def _start_reader(self):
        """Start reader thread"""
        self._reader_alive = True
        self.receiver_thread = Thread(target=self.reader, name='rx', args=())
        self.receiver_thread.daemon = True
        self.receiver_thread.start()

    def _stop_reader(self):
        """Stop reader thread only, wait for clean exit of thread"""
        self._reader_alive = False

    def start(self):
        """start worker thread"""
        self.alive = True
        self._start_reader()
        self.process_thread = Thread(target=self._treat_data, name='treat', args=())
        self.process_thread.daemon = True
        self.process_thread.start()

    def stop(self):
        """set flag to stop worker threads"""
        self.alive = False
        self.waiting_for_ack = False
        self._stop_reader()

    def join(self):
        """wait for worker threads to terminate"""
        self.process_thread.join()
        self.receiver_thread.join()
        self.receiver_thread.join()

    def close(self):
        self.serial.close()

    def reader(self):
        """loop and copy serial->console"""
        try:
            while self._reader_alive:

                # Read the start of frame
                start_of_frame = self.serial.read(1)
                if len(start_of_frame) == 1:
                    # Start of frame detected
                    if start_of_frame[0] == self.default_starter_frame:
                        # Read length
                        length = self.serial.read(1)
                        if len(length) == 1:
                            if length[0] > 3:
                                # Acquire mutex => we received one data; we need to treat it before sending other data
                                self.mutex.acquire()
                                # Read data from serial
                                data = self.serial.read(length[0]-1)
                                if len(data) == (length[0]-1):

                                    frame_is_perhaps_command = True
                                    # We sent a command we are waiting for an acknowledgment
                                    if self.waiting_for_ack:
                                        # Check if this is an acknowledgment
                                        result = self._parse_ack_frame(data)
                                        # Report to applicative
                                        if result[0]:
                                            # Send data in queue
                                            self.queue.put(data[1:])
                                            frame_is_perhaps_command = False
                                            # Release mutex => Frame processed
                                            self.mutex.release()
                                        # Check if we should to retry to send the command
                                        self.waiting_for_ack = result[1]
                                    # Check if this is a command
                                    # We can receive a command while we are waiting for an acknowledgment
                                    # We need still need to treat it
                                    if frame_is_perhaps_command:
                                        result = self._parse_frame(data)
                                        if result[0]:
                                            # Send data in queue
                                            self.queue.put(data[1:])
                                        # Send acknowledgment
                                        self._send_ack(result[1])
                                        # Release mutex => Frame processed
                                        self.mutex.release()
                                else:
                                    # Release mutex => Didn't received the expected length of payload
                                    self.mutex.release()
        except serial.SerialException:
            self.alive = False
            raise  # XXX handle instead of re-raise?

    # Return (Report_To_Applicative, Continue_Retry_Process)
    def _parse_ack_frame(self, data):

        # Check crc
        if check_crc(data):
            # Frame is an ACk ?
            if int(data[0]) == Command.ACK.value:
                # Check frame counter => Same as previous
                if int(data[1]) == self.local_frame_counter:
                    # Check if Error = CRC
                    if int(data[2]) == Status.ACK_BAD_CRC.value:
                        # Retry if CRC error
                        return True, True
                    # Wrong CRC of the command
                    else:
                        return True, False
                # Wrong Frame counter
                else:
                    return False, True
            # Wrong Frame
            else:
                return False, True
        # Received frame got a bad CRC
        else:
            return False, True

    # Return (Report_To_Applicative, ACK Status To Send)
    def _parse_frame(self, data):

        # Check crc
        if check_crc(data):
            # Compare two frames
            if (int(data[0]) == Command.CONTROL.value) | (int(data[0]) == Command.DATA.value):
                # Check frame counter
                if int(data[1]) != self.plc_frame_counter:
                    # Save frame counter
                    self.plc_frame_counter = int(data[1])
                    # Good frame
                    return True, Status.ACK_SUCCESS.value
                # Wrong Frame counter
                else:
                    return False, Status.ACK_BAD_FRAME_COUNTER.value
            # ACK
            elif int(data[1]) == Command.ACK.value:
                return False, Status.SEND_NO_ACK.value
            # Wrong Frame
            else:
                return False, Status.ACK_INVALID_COMMAND.value
        # Received frame got a bad CRC
        else:
            return False, Status.ACK_BAD_CRC.value

    def _treat_data(self):
        while self.alive:
            try:
                data = self.queue.get(True, 0.1)
            except Empty:
                continue
            # Pass new data to engine to treat it
            self.engine_reference.new_data(data)
            self.queue.task_done()

    def _send_ack(self, result):

        # Don't need to send an acknowledgment when received an other acknowledgment
        if result != Status.SEND_NO_ACK.value:

            # Calculate length - Always 4
            length = 4

            # Create frame
            frame = bytearray([length, Command.ACK.value, self.plc_frame_counter])

            # Add payload of the frame
            frame.append(result)

            # add crc
            crc = compute_crc(frame, len(frame))
            frame.append(crc)

            # Write data on serial
            self.serial.write(frame)

    def write_data(self, command, payload):

        self.receiver_thread = Thread(target=self._writer, name='tx', args=(command, payload,))
        self.receiver_thread.daemon = True
        self.receiver_thread.start()

    def _writer(self, command, payload):

        # Acquire mutex
        self.mutex.acquire()

        # Calculate length - "+ 3" because we will add
        # the frame length, frame counter, frame command
        length = len(payload) + 3

        # Increment frame counter
        if self.local_frame_counter == 255:
            self.local_frame_counter = 0
        else:
            self.local_frame_counter += 1

        # Create frame
        frame = bytearray([length, command, self.local_frame_counter])

        # Add payload of the frame
        for element in payload:
            frame.append(element)

        # add crc
        crc = compute_crc(frame, len(frame))
        frame.append(crc)

        try:
            if self.alive:

                ''' First transmission'''
                # Send a new command waiting for an acknowledgment
                self.waiting_for_ack = True
                self.retry_counter = 0
                # Write data on serial
                self.serial.write(frame)
                # Release mutex
                self.mutex.release()
                # Wait for an acknowledgment = 100ms
                time.sleep(0.10)

                ''' Retry process'''
                while self.waiting_for_ack & (self.retry_counter < 3):
                    # Acquire mutex
                    self.mutex.acquire()
                    # Write data on serial
                    self.serial.write(frame)
                    # Release mutex
                    self.mutex.release()
                    # Wait for an acknowledgment = 100ms
                    time.sleep(0.10)
                    # Increase counter
                    self.retry_counter += 1

                # We tried 3 times
                if self.waiting_for_ack:
                    # Send error to applicative
                    error = bytearray([Command.ACK.value, Status.ACK_RETRY_EXPIRED.value])
                    self.queue.put(error)

                # Command treated
                self.waiting_for_ack = False

        except Exception:
            self.alive = False
            raise

    def set_rx_encoding(self, encoding, errors='replace'):
        """set encoding for received data"""
        self.input_encoding = encoding
        self.rx_decoder = codecs.getincrementaldecoder(encoding)(errors)

    def set_tx_encoding(self, encoding, errors='replace'):
        """set encoding for transmitted data"""
        self.output_encoding = encoding
        self.tx_encoder = codecs.getincrementalencoder(encoding)(errors)

    def change_encoding(self):
        """change encoding on the serial port"""
        sys.stderr.write('\n--- Enter new encoding name [{}]: '.format(self.input_encoding))
        new_encoding = sys.stdin.readline().strip()
        if new_encoding:
            try:
                codecs.lookup(new_encoding)
            except LookupError:
                sys.stderr.write('--- invalid encoding name: {}\n'.format(new_encoding))
            else:
                self.set_rx_encoding(new_encoding)
                self.set_tx_encoding(new_encoding)
            sys.stderr.write('--- serial input encoding: {}\n'.format(self.input_encoding))
            sys.stderr.write('--- serial output encoding: {}\n'.format(self.output_encoding))

    def change_port(self):
        """Have a conversation with the user to change the serial port"""
        try:
            port = ask_for_port()
        except KeyboardInterrupt:
            port = None
        if port and port != self.serial.port:
            # reader thread needs to be shut down
            self._stop_reader()
            # save settings
            settings = self.serial.getSettingsDict()
            new_serial = None
            try:
                new_serial = serial.serial_for_url(port, do_not_open=True)
                # restore settings and open
                new_serial.applySettingsDict(settings)
                new_serial.rts = self.serial.rts
                new_serial.dtr = self.serial.dtr
                new_serial.open()
                new_serial.break_condition = self.serial.break_condition
            except Exception as e:
                sys.stderr.write('--- ERROR opening new port: {} ---\n'.format(e))
                new_serial.close()
            else:
                self.serial.close()
                self.serial = new_serial
                sys.stderr.write('--- Port changed to: {} ---\n'.format(self.serial.port))
            # and restart the reader thread
            self._start_reader()
