/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Trust Center Commands/Responses
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.008  11-Jul-16   MvdB   Forward leave indications to SZL
 * 000.000.017  26-Jul-16   MvdB   Support ZAB_ACTION_CHANGE_NETWORK_KEY
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/

#include "zabNcpService.h"
#include "zabNcpEzsp.h"
#include "zabNcpNwk.h"
#include "zabNcpEzspTrustCenter.h"

#include "ezsp-enum.h"
#include "ezsp-enum-decode.h"
#include "ezsp-protocol.h"
#include "ember-types.h"

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

#define M_TC_JOIN_HANDLER_LENGTH_MIN  ( 14 ) // newNodeId(2), newNodeEui64(8), status(1), policyDecision(1), parentOfNewNodeId(2)

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Join Notification Handler
 ******************************************************************************/
void zabNcpEzspTrustCenter_JoinHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  zabMsgPro_NwkLeaveInd* leaveInd;
  unsigned8 index;
  unsigned16 nwkAddress;
  unsigned64 ieee;
  EmberDeviceUpdate devUpdate;
  EmberJoinDecision joinDecision;
  unsigned16 parentOfNewNode;
#ifdef ZAB_VENDOR_VERBOSE_PRINTING
  const char deviceUpdateString[8][32] = {EMBER_DEVICE_UPDATE_STRINGS};
  const char joinDecisionString[4][32] = {EMBER_JOIN_DECISION_STRINGS};
#endif

  if (PayloadLength >= M_TC_JOIN_HANDLER_LENGTH_MIN)
    {
      index = 0;
      nwkAddress = COPY_IN_16_BITS(&PayloadData[index]); index=+2;
      ieee = COPY_IN_64_BITS(&PayloadData[index]); index+=8;
      devUpdate = (EmberDeviceUpdate)PayloadData[index++];
      joinDecision = (EmberJoinDecision)PayloadData[index++];
      parentOfNewNode = COPY_IN_16_BITS(&PayloadData[index]); index=+2;

      if ( (devUpdate <= EMBER_HIGH_SECURITY_UNSECURED_REJOIN) && (joinDecision <= EMBER_NO_ACTION) )
        {
          printVendor(Service, "EZSP_TRUST_CENTER_JOIN_HANDLER: NodeId = 0x%04X, IEEE = 0x%llX, Status = %s (%d), Decision = %s (%d), Parent = 0x%04X\n",
                      nwkAddress,
                      ieee,
#ifdef ZAB_VENDOR_VERBOSE_PRINTING
                      deviceUpdateString[devUpdate],
#else
                      "?",
#endif
                      devUpdate,
#ifdef ZAB_VENDOR_VERBOSE_PRINTING
                      joinDecisionString[joinDecision],
#else
                      "?",
#endif
                      joinDecision,
                      parentOfNewNode);

          if (devUpdate == EMBER_DEVICE_LEFT)
            {
              // Allocate New Message
              length = zabMsgPro_NwkLeaveInd_SIZE;
              dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
              if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
                {
                  erStatusSet(Status, SAP_ERROR_NULL_PTR);
                  return;
                }
              sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_NWK_LEAVE_IND);
              sapMsgSetAppDataLength(Status, dataMessage, length);
              leaveInd = (zabMsgPro_NwkLeaveInd*)sapMsgGetAppData(dataMessage);

              /* Build up data. For stuff we don't have data for apply the best guess! */
              leaveInd->srcNwkAddress = nwkAddress;
              leaveInd->srcIeeeAddress = ieee;
              leaveInd->request = szl_false;
              leaveInd->removeChildren = szl_false;
              leaveInd->rejoin = szl_false;

              /* Send up through data sap */
              sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
            }
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
        }
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Broadcast Next Network Key Request + Response Handler
 ******************************************************************************/
void zabNcpEzspTrustCenter_BroadcastNextNetworkKey(erStatus* Status, zabService* Service)
{
  unsigned8 index;
  unsigned8 keyIndex;
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH + EMBER_ENCRYPTION_KEY_SIZE] = {0/*seq*/, 0/*FrameControl*/, EZSP_BROADCAST_NEXT_NETWORK_KEY};

  index = EZSP_PARAMETERS_INDEX;
  printVendor(Service, "zabNcpEzspTrustCenter_BroadcastNextNetworkKey: ");
  for (keyIndex = 0; keyIndex < EMBER_ENCRYPTION_KEY_SIZE; keyIndex++)
    {
      // We can generate the key here ourselves if we wish
      //cmd[index] = (unsigned8)ZAB_RAND_16();
      //printVendor(Service, "%02X ", cmd[index]);

      // If we set all zeros then stack will randomly generate key itself which is safer!
      printVendor(Service, "Generated randomly by stack\n");
      cmd[index] = 0;
      index++;
    }
  printVendor(Service, "\n");

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, index, cmd);
}
void zabNcpEzspTrustCenter_BroadcastNextNetworkKeyResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_BROADCAST_NEXT_NETWORK_KEY: Status: %s (0x%02X)\n",
                  decodeEmberStatus(PayloadData[0]),
                  PayloadData[0]);

      zabNcpNwk_BroadcastNextNetworkKeyResponseHandler(Status, Service, PayloadData[0]);
    }
}

/******************************************************************************
 * Broadcast Next Network Key Switch Request + Response Handler
 ******************************************************************************/
void zabNcpEzspTrustCenter_BroadcastNextNetworkKeySwitch(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH] = {0/*seq*/, 0/*FrameControl*/, EZSP_BROADCAST_NETWORK_KEY_SWITCH};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspTrustCenter_BroadcastNextNetworkKeySwitchResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_BROADCAST_NETWORK_KEY_SWITCH: Status: %s (0x%02X)\n",
                  decodeEmberStatus(PayloadData[0]),
                  PayloadData[0]);

      zabNcpNwk_BroadcastNextNetworkKeySwitchResponseHandler(Status, Service, PayloadData[0]);
    }
}


/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
