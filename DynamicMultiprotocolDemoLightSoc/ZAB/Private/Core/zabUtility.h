/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains general utility functions for ZAB
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 02.00.00.04  22-Feb-14   MvdB   Cleanup
 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 00.00.06.02  30-Sep-14   MvdB   artf55512: Support network key change action
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 * 002.000.004  01-Apr-15   MvdB   ARTF56504: Support missed channel change recovery, with actions/notifications/asks/gives to get/set parameters.
 * 002.002.005  11-Sep-15   MvdB   ARTF112436: Add zabUtility_GetNetworkInfoStateString()
 * 002.002.012  14-Oct-15   MvdB   ARTF104107: Support zabDeviceType for End Device operation
 *****************************************************************************/

#ifndef __ZAB_UTILITY_H__
#define __ZAB_UTILITY_H__

#include "sapMsgUtility.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 *****            MACROS            *****
 *                      ******************************
 ******************************************************************************/

/* Define the different data sizes */
#define MACRO_DATA_SIZE_BYTE        1                                /* size of a byte */
#define MACRO_DATA_SIZE_WORD        MACRO_DATA_SIZE_BYTE * 2         /* size of a word */
#define MACRO_DATA_SIZE_DOUBLE_WORD MACRO_DATA_SIZE_WORD * 2         /* size of a double word */
#define MACRO_DATA_SIZE_QUAD_WORD   MACRO_DATA_SIZE_DOUBLE_WORD * 2  /* size of a quad word */


/******************************************************************************
 * Convert unsigned8[2] into a unsigned16
 *  src = unsigned8*
 *  return =  unsigned16
 ******************************************************************************/
#define COPY_IN_16_BITS(src)\
  ((unsigned16)( \
   ((((src)[0])     ) & 0x00FF) | \
   ((((src)[1]) << 8) & 0xFF00) \
  ))

/******************************************************************************
 * Convert unsigned16 into unsigned8[2]
 *  dest = unsigned8*
 *  src =  unsigned16
 ******************************************************************************/
#define COPY_OUT_16_BITS(dest, src) do {\
  (dest)[0] = (unsigned8)(((src)     ) & 0xFF); \
  (dest)[1] = (unsigned8)(((src) >> 8) & 0xFF); \
  } while (0)

/******************************************************************************
 * Convert unsigned8[3] into a unsigned32
 *  src = unsigned8*
 *  return =  unsigned32
 ******************************************************************************/
#define COPY_IN_24_BITS(src)\
  ((unsigned32)(\
    ((unsigned32)(((src)[0])      ) & 0x000000FF) | \
    ((unsigned32)(((src)[1]) <<  8) & 0x0000FF00) | \
    ((unsigned32)(((src)[2]) << 16) & 0x00FF0000) \
  ))
  
/******************************************************************************
 * Convert unsigned32 into unsigned8[3]
 *  dest = unsigned8*
 *  src =  unsigned32
 ******************************************************************************/
#define COPY_OUT_24_BITS(dest, src) do {\
  (dest)[0] = (unsigned8)(((src)      ) & 0xFF); \
  (dest)[1] = (unsigned8)(((src) >>  8) & 0xFF); \
  (dest)[2] = (unsigned8)(((src) >> 16) & 0xFF); \
 } while (0)

/******************************************************************************
 * Convert unsigned8[4] into a unsigned32
 *  src = unsigned8*
 *  return =  unsigned32
 ******************************************************************************/
#define COPY_IN_32_BITS(src)\
  ((unsigned32)(\
    ((unsigned32)(((src)[0])      ) & 0x000000FF) | \
    ((unsigned32)(((src)[1]) <<  8) & 0x0000FF00) | \
    ((unsigned32)(((src)[2]) << 16) & 0x00FF0000) | \
    ((unsigned32)(((src)[3]) << 24) & 0xFF000000) \
  ))

/******************************************************************************
 * Convert unsigned32 into unsigned8[4]
 *  dest = unsigned8*
 *  src =  unsigned32
 ******************************************************************************/
#define COPY_OUT_32_BITS(dest, src) do {\
  (dest)[0] = (unsigned8)(((src)      ) & 0xFF); \
  (dest)[1] = (unsigned8)(((src) >>  8) & 0xFF); \
  (dest)[2] = (unsigned8)(((src) >> 16) & 0xFF); \
  (dest)[3] = (unsigned8)(((src) >> 24) & 0xFF); \
 } while (0)

  
/******************************************************************************
 * Convert unsigned8[5] into a unsigned64
 *  src = unsigned8*
 *  return =  unsigned64
 ******************************************************************************/
#define COPY_IN_40_BITS(src)\
  ((unsigned64)( \
    (((unsigned64)((src)[0])      ) & 0x00000000000000FF) + \
    (((unsigned64)((src)[1]) <<  8) & 0x000000000000FF00) + \
    (((unsigned64)((src)[2]) << 16) & 0x0000000000FF0000) + \
    (((unsigned64)((src)[3]) << 24) & 0x00000000FF000000) + \
    (((unsigned64)((src)[4]) << 32) & 0x000000FF00000000) \
  )) 

/******************************************************************************
 * Convert unsigned64 into unsigned8[5]
 *  dest = unsigned8*
 *  src =  unsigned64
 ******************************************************************************/
#define COPY_OUT_40_BITS(dest, src) do {\
  (dest)[0] = (((src)      ) & 0xFF); \
  (dest)[1] = (((src) >>  8) & 0xFF); \
  (dest)[2] = (((src) >> 16) & 0xFF); \
  (dest)[3] = (((src) >> 24) & 0xFF); \
  (dest)[4] = (((src) >> 32) & 0xFF); \
  } while (0)

/******************************************************************************
 * Convert unsigned8[6] into a unsigned64
 *  src = unsigned8*
 *  return =  unsigned64
 ******************************************************************************/
#define COPY_IN_48_BITS(src)\
  ((unsigned64)( \
    (((unsigned64)((src)[0])      ) & 0x00000000000000FF) + \
    (((unsigned64)((src)[1]) <<  8) & 0x000000000000FF00) + \
    (((unsigned64)((src)[2]) << 16) & 0x0000000000FF0000) + \
    (((unsigned64)((src)[3]) << 24) & 0x00000000FF000000) + \
    (((unsigned64)((src)[4]) << 32) & 0x000000FF00000000) + \
    (((unsigned64)((src)[5]) << 40) & 0x0000FF0000000000) \
  ))  

/******************************************************************************
 * Convert unsigned64 into unsigned8[6]
 *  dest = unsigned8*
 *  src =  unsigned64
 ******************************************************************************/
#define COPY_OUT_48_BITS(dest, src) do {\
  (dest)[0] = (((src)      ) & 0xFF); \
  (dest)[1] = (((src) >>  8) & 0xFF); \
  (dest)[2] = (((src) >> 16) & 0xFF); \
  (dest)[3] = (((src) >> 24) & 0xFF); \
  (dest)[4] = (((src) >> 32) & 0xFF); \
  (dest)[5] = (((src) >> 40) & 0xFF); \
  } while (0)

/******************************************************************************
 * Convert unsigned8[7] into a unsigned64
 *  src = unsigned8*
 *  return =  unsigned64
 ******************************************************************************/
#define COPY_IN_56_BITS(src)\
  ((unsigned64)( \
    (((unsigned64)((src)[0])      ) & 0x00000000000000FF) + \
    (((unsigned64)((src)[1]) <<  8) & 0x000000000000FF00) + \
    (((unsigned64)((src)[2]) << 16) & 0x0000000000FF0000) + \
    (((unsigned64)((src)[3]) << 24) & 0x00000000FF000000) + \
    (((unsigned64)((src)[4]) << 32) & 0x000000FF00000000) + \
    (((unsigned64)((src)[5]) << 40) & 0x0000FF0000000000) + \
    (((unsigned64)((src)[6]) << 48) & 0x00FF000000000000) \
  ))

/******************************************************************************
 * Convert unsigned64 into unsigned8[7]
 *  dest = unsigned8*
 *  src =  unsigned64
 ******************************************************************************/
#define COPY_OUT_56_BITS(dest, src) do {\
  (dest)[0] = (((src)      ) & 0xFF); \
  (dest)[1] = (((src) >>  8) & 0xFF); \
  (dest)[2] = (((src) >> 16) & 0xFF); \
  (dest)[3] = (((src) >> 24) & 0xFF); \
  (dest)[4] = (((src) >> 32) & 0xFF); \
  (dest)[5] = (((src) >> 40) & 0xFF); \
  (dest)[6] = (((src) >> 48) & 0xFF); \
  } while (0)

/******************************************************************************
 * Convert unsigned8[8] into a unsigned64
 *  src = unsigned8*
 *  return =  unsigned64
 ******************************************************************************/
#define COPY_IN_64_BITS(src)\
  ((unsigned64)( \
  (((unsigned64)((src)[0])      ) & 0x00000000000000FF) + \
  (((unsigned64)((src)[1]) <<  8) & 0x000000000000FF00) + \
  (((unsigned64)((src)[2]) << 16) & 0x0000000000FF0000) + \
  (((unsigned64)((src)[3]) << 24) & 0x00000000FF000000) + \
  (((unsigned64)((src)[4]) << 32) & 0x000000FF00000000) + \
  (((unsigned64)((src)[5]) << 40) & 0x0000FF0000000000) + \
  (((unsigned64)((src)[6]) << 48) & 0x00FF000000000000) + \
  (((unsigned64)((src)[7]) << 56) & 0xFF00000000000000) \
  ))

/******************************************************************************
 * Convert unsigned64 into unsigned8[8]
 *  dest = unsigned8*
 *  src =  unsigned64
 ******************************************************************************/
#define COPY_OUT_64_BITS(dest, src) do {\
  (dest)[0] = (((src)      ) & 0xFF); \
  (dest)[1] = (((src) >>  8) & 0xFF); \
  (dest)[2] = (((src) >> 16) & 0xFF); \
  (dest)[3] = (((src) >> 24) & 0xFF); \
  (dest)[4] = (((src) >> 32) & 0xFF); \
  (dest)[5] = (((src) >> 40) & 0xFF); \
  (dest)[6] = (((src) >> 48) & 0xFF); \
  (dest)[7] = (((src) >> 56) & 0xFF); \
  } while (0)

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Calculate the TI ZNP Frame Check Sequence (FCS) for a buffer
 * This is used for ZNP commands transported over UART.
 ******************************************************************************/
extern 
unsigned8 zabUtility_ComputeFCS(unsigned8* pBuffer, unsigned16 length);

/******************************************************************************
 * Get a string description of zabOpenState
 ******************************************************************************/
extern 
char* zabUtility_GetOpenStateString(zabOpenState openState);

/******************************************************************************
 * Get a string description of zabFwUpdateState
 ******************************************************************************/
extern 
char* zabUtility_GetFirmwareUpdateStateString(zabFwUpdateState updateState);

/******************************************************************************
 * Get a string description of zabNwkState
 ******************************************************************************/ 
extern 
char* zabUtility_GetNetworkStateString(zabNwkState nwkState);

/******************************************************************************
 * Get a string description of zabNwkSteerOptions
 ******************************************************************************/
extern 
char* zabUtility_GetNwkSteerString(zabNwkSteerOptions nwkSteerOptions);

/******************************************************************************
 * Get a string description of zabDeviceType
 ******************************************************************************/
extern 
char* zabUtility_GetDeviceTypeString(zabDeviceType nwkDevType);

/******************************************************************************
 * Get a string description of zabCloneState
 ******************************************************************************/ 
extern
char* zabUtility_GetCloneStateString(zabCloneState cloneState);

/******************************************************************************
 * Get a string description of zabChannelChangeState
 ******************************************************************************/ 
extern
char* zabUtility_GetChannelChangeStateString(zabChannelChangeState channelChangeState);

/******************************************************************************
 * Get a string description of zabNetworkKeyChangeState
 ******************************************************************************/ 
extern
char* zabUtility_GetNetworkKeyChangeStateString(zabNetworkKeyChangeState nwkKeyChangeState);

/******************************************************************************
 * Get a string description of zabNetworkMaintenanceParamState
 ******************************************************************************/ 
extern
char* zabUtility_GetNetworkMaintenanceParamStateString(zabNetworkMaintenanceParamState nwkMaintenanceParamState);

/******************************************************************************
 * Get a string description of zabNetworkInfoState
 ******************************************************************************/ 
extern 
char* zabUtility_GetNetworkInfoStateString(zabNetworkInfoState nwkInfoState);

/******************************************************************************
 * Get a string description of zabAction
 ******************************************************************************/
extern 
char* zabUtility_GetZabActionString(zabAction Action);

/******************************************************************************
 * Get a string description of sapMsgDirection
 ******************************************************************************/ 
extern 
char* zabUtility_GetSapMsgDirectionString(sapMsgDirection sapMsgDirection);

/******************************************************************************
 * Get a string description of sapMsgType
 ******************************************************************************/ 
extern 
char* zabUtility_GetSapMsgTypeString(sapMsgType MsgType);

/******************************************************************************
 * Get a string description of zabVendorType
 ******************************************************************************/ 
extern 
char* zabUtility_GetVendorTypeString(zabVendorType vendorType);

/******************************************************************************
 * Get a string description of zabNetworkProcessorModel
 ******************************************************************************/ 
extern 
char* zabUtility_GetNetworkProcessorModelString(zabNetworkProcessorModel networkProcessorModel);

/******************************************************************************
 * Get a string description of zabNetworkProcessorApplication
 ******************************************************************************/ 
extern 
char* zabUtility_GetNetworkProcessorApplicationString(zabNetworkProcessorApplication networkProcessorApplication);


/******************************************************************************
 * Get a string description of zabAntennaOperatingMode
 ******************************************************************************/ 
extern 
char* zabUtility_GetAntennaOperatingModeString(zabAntennaOperatingMode antennaOperatingMode);

#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/