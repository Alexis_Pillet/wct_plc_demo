/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the configuration values for the ZNP Vendor.
 *
 *   This file is to be copied and modified for a particular implementation of the
 *   ZigBee Application Brick.
 *
 *   For details and flow charts see ZEC007.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *  00.00.00.02 30-Oct-13   MvdB   Tidy up
 *****************************************************************************/

#ifndef __ZAB_VENDOR_CONFIGURE_H__
#define __ZAB_VENDOR_CONFIGURE_H__

#include "zabCoreConfigure.h"


/******************************************************************************
 *                      ******************************
 *                 *****        VENDOR CHOICES        *****
 *                 *****       USER CONFIGURABLE      *****
 *                      ******************************
 ******************************************************************************/

/* The following vendors are supported.
 * One and only one may be selected
 *
 * ZAB_VENDOR_TI_ZNP: Texas Instruments ZigBee Network Processor */
#ifdef APP_CONFIG_M_VENDOR_TI_ZNP
  #define ZAB_VENDOR_TI_ZNP
#endif
#ifdef APP_CONFIG_M_VENDOR_SILABS_NCP
  #define ZAB_VENDOR_SILABS_NCP
#endif






/******************************************************************************
 *                      ******************************
 *                 ***** VENDOR SPECIFIC CONFIGURATION *****
 *               ***** ONLY ZAB DEVELOPERS SHOULD CHANGE *****
 *                      ******************************
 ******************************************************************************/

/* Each vendor should include a file here with their specific configuration values */
#if defined ZAB_VENDOR_TI_ZNP
  #include "zabVendorConfigureZnp.h"
#elif defined ZAB_VENDOR_SILABS_NCP
  #include "zabVendorConfigureSilabsNcp.h"
#else
  #error zabVendorConfigure.h: No vendor selected.
#endif


/******************************************************************************
 *                      ******************************
 *                 *****        ERROR CHECKING        *****
 *               ***** ONLY ZAB DEVELOPERS SHOULD CHANGE *****
 *                      ******************************
 ******************************************************************************/



/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif

