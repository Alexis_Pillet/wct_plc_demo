/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_status2text.h
*    @version
*        $Rev: 3203 $
*    @last editor
*        $Author: a0202438 $
*    @date
*        $Date:: 2017-05-10 11:25:47 +0900#$
* Description :
******************************************************************************/

#ifndef R_DEMO_STATUS2TEXT_H
#define R_DEMO_STATUS2TEXT_H

/******************************************************************************
Macro definitions
******************************************************************************/
#define APL_LENGTH_8      (sizeof (uint8_t))
#define APL_LENGTH_16     (sizeof (uint16_t))
#define APL_LENGTH_32     (sizeof (uint32_t))
#define APL_LENGTH_TABLE  (0u)

/******************************************************************************
Typedef definitions
******************************************************************************/
/******************************************************************************
Functions prototype
******************************************************************************/
const char *status_to_text (uint8_t id, uint8_t status);
const char *ibid_to_text (uint8_t Layer, uint16_t id, uint8_t * len);
const char *statsindex_to_text (uint8_t id, uint32_t index);

const char *R_LOG_Stats2TextSys (uint8_t id, uint32_t index);

#endif /* R_DEMO_STATUS2TEXT_H */

