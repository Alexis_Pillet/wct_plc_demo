 var disable_button = function($id) {
     $('#' + $id).attr('class', 'button button-command-disabled');
     $('#' + $id).attr('disabled', 'true');
   };

    const detail_iact = ({
      name, status, action, id_wireless_bridge, address, type_device, name_function
    }) => `
    <div class="iact-toto">
    <p id="title_device">${name}</p>
    <section class="box">
        <p id="title_detail">Status: <span class="text-green">${status}</<span></p>
        <iframe name="votar" style="display:none;"></iframe>
        <form id="iact_action" onsubmit="disable_button('button_action_${id_wireless_bridge}_${address}')" action="/WirelessBridge" method="post" target="votar">
            <input id='button_action_${id_wireless_bridge}_${address}' class="button-primary button-command" type="submit" value="${action}">
            <input type="hidden" name="id_wireless_bridge" value="${id_wireless_bridge}">
            <input type="hidden" name="address" value="${address}">
            <input type="hidden" name="type_device" value="${type_device}">
            <input type="hidden" name="name_function" value="${name_function}">
        </form>
    </section>
    </div>`;

    const detail_power_tag = ({
      name, status, action
    }) => `
    <div class="iact-toto">
    <p id="title_device">${name}</p>
    <section class="box">
        <p id="title_detail">Status: <span class="text-green">${status}</<span></p>
        <input class="button-primary button-command" type="submit" value="${action}">
    </section>
    </div>`;


    var set_on_changed_action = function() {

      $('#jstree_demo_div').on('ready.jstree', function(e, data) {
          if(data.instance.get_node('WB_1')){
          $('#date').html(data.instance.get_node('WB_1').data.date);
        }else{
          $('#date').html('');
        }
      });


      $('#jstree_demo_div')
        // listen for event
        .on('changed.jstree', function(e, data) {
          var i, j, r = [];
          data_product_iact = [];
          data_product_power_tag = [];
          for (i = 0, j = data.selected.length; i < j; i++) {

            if (data.instance.get_node(data.selected[i]).type == 'iact') {
              data_product_iact.push({
                name: data.instance.get_node(data.selected[i]).text,
                status: data.instance.get_node(data.selected[i]).data.status == 'OPEN' ? 'OPEN' : 'CLOSE',
                action: data.instance.get_node(data.selected[i]).data.status == 'OPEN' ? 'CLOSE' : 'OPEN',
                id_wireless_bridge: data.instance.get_node( data.instance.get_node(data.selected[i]).parents[0]).data.device_number,
                address: data.instance.get_node(data.selected[i]).data.address,
                type_device: data.instance.get_node(data.selected[i]).type,
                name_function: data.instance.get_node(data.selected[i]).data.status == 'OPEN' ? 'open' : 'close',
              })
            }
            if (data.instance.get_node(data.selected[i]).type == 'power_tag') {
              data_product_power_tag.push({
                name: data.instance.get_node(data.selected[i]).text,
                status: data.instance.get_node(data.selected[i]).data.status == 'OPEN' ? 'OPEN' : 'CLOSE',
                action: data.instance.get_node(data.selected[i]).data.status == 'OPEN' ? 'CLOSE' : 'OPEN',
              })
            }
          }

          $('#detail-product').html(data_product_iact.map(detail_iact).join(''));
          $('#detail-product').append(data_product_power_tag.map(detail_power_tag).join(''));

        })

    }

    var load_tree = function() {
        var $treeview = $("#jstree_demo_div");
        $.getJSON('./topology.json?nocache=' + (new Date()).getTime(), function (data) {
        $treeview.jstree({
                  "state": {
                    "key": "123456"
                  },
                  "checkbox": {
                    "keep_selected_style": true
                  },
                  "state": {
                    "key": "zigbeeView"
                  },
                  "types": {
                    "default": {
                      "icon": "./images/logo_wb.png"
                    },
                    "iact": {
                      "icon": "./images/logo_iact.png"
                    },
                    "power_tag": {
                      "icon": "./images/logo_power_tag.png"
                    }
                  },
                  "plugins": ["checkbox", "sort", "state", "types"],
                  "core": {
                    "themes": {
                      "name": "default",
                      "dots": true,
                      "icons": true
                    },
                    "data": data
                  }
                });
                localStorage.setItem("topology-data",JSON.stringify(data));
        });
    };

    var reload_tree = function($id) {
      $.getJSON('./topology.json?nocache=' + (new Date()).getTime(), function (data) {
        if(localStorage.getItem("topology-data") != JSON.stringify(data)){
          localStorage.setItem("topology-data",JSON.stringify(data));
          var jsTree = $('#jstree_demo_div').jstree(true);
          jsTree.save_state();
          $json = jsTree.get_json();
          $("#jstree_demo_div").jstree("destroy");
          load_tree();
          set_on_changed_action();
        }
      });
    };

    setInterval(function() {
      reload_tree();
    }, 5000);

    $(function() {
      load_tree();
      set_on_changed_action();
    });