/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/******************************************************************************
* File Name     : resetprg.c
* Device(s)     : RX631
* Tool-chain    : KPIT GNURX-ELF 15.01
* H/W platform  : G-CPX / EU-CPX2 / G-CPX3
* Description   : Sample software
******************************************************************************/
#include "CCRXmachine.h"

#include "secaddr.h"
#include "vect.h"

#ifdef __ROZ                        /* Initialize FPSW */
#define _ROUND (0x00000001)         /* Let FPSW RMbits=01 (round to zero) */
#else
#define _ROUND (0x00000000)         /* Let FPSW RMbits=00 (round to nearest) */
#endif
#ifdef __DOFF
#define _DENOM (0x00000100)         /* Let FPSW DNbit=1 (denormal as zero) */
#else
#define _DENOM (0x00000000)         /* Let FPSW DNbit=0 (denormal as is) */
#endif

#define PSW_init  (0x00000000)    /* PSW bit pattern */
#define FPSW_init (0x00000000)    /* FPSW bit base pattern */

static void  __attribute((section("PSTART"))) start_up_point(void);
extern void main(void);

/******************************************************************************
* Function Name:PowerON_Reset_PC
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
void  __attribute((section("PResetPRG"))) PowerON_Reset_PC(void)
{
    __asm("mvtc    #_g_ustack,USP");

    __asm("mvtc    #_g_istack,ISP");

    __builtin_rx_mvtc (0xC, (int)__SECTOP_CVECT);

    __builtin_rx_mvtc (0x3, ((FPSW_init | _ROUND) | _DENOM) );

    __asm("nop");

    _INITSCT();
    
    __builtin_rx_setpsw (PSW_init);
    start_up_point();

    __builtin_rx_brk ();
}
/******************************************************************************
   End of function  PowerON_Reset_PC
******************************************************************************/

/******************************************************************************
* Function Name:start_up_point
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
static void  __attribute((section("PSTART"))) start_up_point(void)
{
    main();
}
/******************************************************************************
   End of function  start_up_point
******************************************************************************/

