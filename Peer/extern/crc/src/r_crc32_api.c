/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/******************************************************************************
* File Name     : r_crc_api.c
* Device(s)     : RX631
* Tool-chain    : KPIT GNURX-ELF 15.01
* H/W platform  : G-CPX / EU-CPX2 / G-CPX3
* Description   : Sample software
******************************************************************************/

/******************************************************************************
Includes <System Includes> , "Project Includes"
******************************************************************************/
#include <stdlib.h>
#include "r_typedefs.h"
#include "r_config.h"
#include "r_stdio_api.h"
#include "r_crc32_api.h"


/******************************************************************************
Macro definitions
******************************************************************************/

#define R_CRC_POLYNOMIAL             (0x04C11DB7uL)
#define R_CRC_INITIAL_REMAINDER      (0x00000000uL)
#define R_CRC_FINAL_XOR_VALUE        (0x00000000uL)
#define R_CRC_WIDTH                  (32uL)

/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/

/******************************************************************************
Private global variables and functions
******************************************************************************/

/******************************************************************************
* Static variables
******************************************************************************/
static uint32_t crc32_table[256]; //!< Used for the CRC32 computation, initialized during CRC init


/******************************************************************************
* Local function headers
******************************************************************************/
static uint32_t crc_calc_crc32_software(const r_iovec_t iovec[],uint32_t residue,uint8_t iovec_length);


/******************************************************************************
* Extern variables
******************************************************************************/

/******************************************************************************
* Global variables
******************************************************************************/

/******************************************************************************
Function implementations
******************************************************************************/

/******************************************************************************
* GENERAL PURPOSE FUNCTIONS
******************************************************************************/

/******************************************************************************
* Function Name:R_CRC_InitCRC32
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_CRC_InitCRC32(void)
{
    uint32_t residue;
    uint32_t dividend;
    uint8_t  bits;

    /* Compute the remainder of each possible dividend */
    for (dividend = 0u ; dividend < 256u ; ++dividend)
    {
        /* Start with the dividend followed by zeros */
        residue = dividend << (R_CRC_WIDTH - 8u);

        /* Perform modulo-2 division, a bits at a time */
        for (bits = 8u ; bits > 0u ; --bits)
        {
            /* Try to divide the current data bit */
            if ((residue & (1uL << (R_CRC_WIDTH - 1uL))) != 0x0uL)
            {
                /* left shift of unsigned is ok here */
                residue = (residue << 0x1u) ^ R_CRC_POLYNOMIAL;
            }
            else
            {
                residue = (residue << 0x1u);
            }
        }

        /* Store the result into the table */
        crc32_table[dividend] = residue;
    }


    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_CRC_InitCRC32
******************************************************************************/

/******************************************************************************
* Function Name:R_CRC_CalcCrc32
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
uint32_t R_CRC_CalcCrc32(const r_iovec_t iovec[],
                         uint32_t residue,
                         uint8_t iovec_length)
{
    uint32_t crc = 0;

    if (NULL == iovec)
    {
        return 0u;
    }

    crc = crc_calc_crc32_software(iovec, residue, iovec_length);

    /* The final remainder is the CRC */
    return crc ^ R_CRC_FINAL_XOR_VALUE;
}
/******************************************************************************
   End of function  R_CRC_CalcCrc32
******************************************************************************/

/******************************************************************************
* Function Name:R_CRC_ValidateCrc32
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
r_result_t R_CRC_ValidateCrc32(const uint8_t pinput[],
                               uint16_t      length)
{
    r_iovec_t iovec[1];
    uint32_t  crc_result;
    uint32_t  recv_crc;

    iovec[0].paddress = pinput;
    iovec[0].length  = (uint32_t)(length - 4u);

    crc_result = R_CRC_CalcCrc32(iovec,R_CRC_INITIAL_REMAINDER, 1u);

    recv_crc = (uint32_t)(((((uint32_t)pinput[length - 4] << 24u)  +
                            ((uint32_t)pinput[length - 3] << 16u)) +
                            ((uint32_t)pinput[length - 2] <<  8u)) +
                            ((uint32_t)pinput[length - 1]       ));
    if (crc_result == recv_crc)
    {
        return R_RESULT_SUCCESS;
    }
    else
    {
        return R_RESULT_FAILED;
    }
}
/******************************************************************************
   End of function  R_CRC_ValidateCrc32
******************************************************************************/

/******************************************************************************
* Function Name:crc_calc_crc32_software
* Description :
* Arguments : 
* Return Value : 
******************************************************************************/
static uint32_t crc_calc_crc32_software(const r_iovec_t iovec[],
                                        uint32_t residue,
                                        uint8_t iovec_length)
{
    uint32_t byte;
    uint8_t  data;
    uint8_t  i;

    if (NULL == iovec)
    {
        return 0u;
    }

    /* Divide the message by the polynomial, a byte at a time */
    for (i = 0u ; i < iovec_length ; i++)
    {
        for (byte = 0u ; byte < iovec[i].length ; ++byte)
        {
            data    = (uint8_t)((*(iovec[i].paddress + byte)) ^ (residue >> (R_CRC_WIDTH - 8u)));
            residue = crc32_table[data] ^ ((residue & 0x00FFFFFFu) << 0x8u);
        }
    }

    return residue;
}
/******************************************************************************
   End of function  crc_calc_crc32_software
******************************************************************************/


