/*
 * plc_Gateway.c
 *
 *  Created on: 25 sept. 2017
 *      Author: SESA260450
 */

/******************************************************************************
Includes
******************************************************************************/
#include <stdlib.h>
#include <string.h>
#if defined(__RENESAS__)
/* Intrinsic functions provided by compiler. */
#include <machine.h>
#else
#include "CCRXmachine.h"
#endif
#include "r_typedefs.h"
#include "r_stdio_api.h"
#include "r_bsp_api.h"
#include "r_timer_api.h"
#include "r_byte_swap.h"



/* g3 part */
#include "r_c3sap_api.h"

#include "r_demo_app.h"
#include "r_demo_sys.h"
#include "r_demo_app_eap.h"
#include "r_demo_nvm_process.h"
#include "r_demo_print.h"

#include "r_ipv6_headers.h"
#include "r_udp_headers.h"
#include "r_demo_tools.h"
#include "r_demo_api.h"
#include "r_demo_status2text.h"
#include "r_demo_common.h"
#include "cpx3_api\cpx3_sap\uif\src\core\r_uif_base.h"


#include "plc_Gateway.h"

/******************************************************************************
Exported global variables
******************************************************************************/
extern r_demo_config_t      g_demo_config;
extern r_demo_entity_t      g_demo_entity;
extern r_demo_buff_t        g_demo_buff;

/******************************************************************************
Macro definitions
******************************************************************************/
#define HEADER_FRAME_SIZE	3
#define FOOTER_FRAME_SIZE	1
#define SIZE_ACK_FRAME		5
#define SIZE_CONFIG_NVM		6

#define PEER_DEVICE
#define CONCENTRATOR_DEVICE_no
#define TEST_FLOW_no

#define TEST_FAB_no

#define PEER_MAC_ADDRESS 0xABCDABCDABCD0002


/******************************************************************************
Typedef definitions
******************************************************************************/
/* Index of command and associated value in the UART frame between bridge and PLC */
typedef enum
{
	IDX_START_OF_FRAME		= 0x00,
	IDX_FRAME_SIZE			= 0x01,
	IDX_CMD					= 0x02,
	IDX_FRAME_COUNTER		= 0x03,
	IDX_CONTROL				= 0x04,
	IDX_DATA				= 0x04,
	IDX_ACK					= 0x04,
	IDX_STATE				= 0x05,
	IDX_DEVICE_TYPE			= 0x05,
	IDX_ID_NETWORK			= 0x05,
	IDX_NETWORK_STATE		= 0x06,
	IDX_DISCOVERY_STATE		= 0x06,
	IDX_ADV_STATE			= 0x06,
	IDX_MAC_ADDRESS_MSB		= 0x06,
	IDX_MAC_ADDRESS_LSB		= 0x07,
	IDX_PANID_MSB			= 0x08,
	IDX_PANID_LSB			= 0x09,
}e_frame_index;




/* Struct of frame received from the bridge */
typedef struct
{
	uint8_t network_address[2];
}peer_device;

/* Struct of config saved in internal memory before shut down */
typedef struct
{
	uint8_t							Config_saved;	/* 0- No Config,  1-Existing config */
	r_adp_device_type_t				devtype;		/* 0-Peer, 1-Concentrator */
	uint16_t						Mac_address;	/* Mac address device */
	uint16_t						PanID;			/* PanID network  */
}device_config;

/* State of the entire gateway process */
typedef struct
{
	e_network_state 				nwk_state;
	e_network_state_discovery	 	nwk_state_discovery;
	r_boolean_t 					config_saved;
	uint8_t 						timeout_connection;
	r_boolean_t						discover_continue;
	uint32_t 						start_timeout_time;
	uint8_t							peer_device_counter;
	peer_device 					Peer_Address[10];
	uint8_t							rx_frame_counter;
	uint8_t							tx_frame_counter;
	r_boolean_t						frame_to_send;
	t_uart_data						frames_buffer[10];
	uint32_t						ack_timeout;
	uint8_t							tx_retry;
	r_boolean_t						wait_for_ack;
	uint32_t						error_plc;
	uint8_t 						rx_frame_size;
	uint32_t						debug_cnt_tx_retry;
	uint32_t						debug_cnt_rx_ack;
	uint32_t						debug_error_rx_ack;
	uint8_t							missing_rx_frame;
	uint8_t							debug_ack_success;
	uint8_t							debug_ack_error;
}Gateway_State;

/******************************************************************************
Private global variables and functions
******************************************************************************/
/******************************************************************************
* Static variables
******************************************************************************/
static Gateway_State				Gateway;

/******************************************************************************
* Local function headers
******************************************************************************/


/******************************************************************************
Exported global variables
******************************************************************************/
extern volatile r_demo_g3_cb_str_t g_g3cb[R_G3_CH_MAX];
extern const uint8_t               g_rom_nvm_def_psk[16];
extern const uint8_t               g_rom_nvm_def_gmk[2][16];

/******************************************************************************
Functions
******************************************************************************/

/******************************************************************************
* Function Name: Peer_Display_Error
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void Peer_Display_Error ( r_result_t result )
{
	switch( result )
	{
		case 0x01:
			R_STDIO_Printf( " Process failed" );
			break;

		case 0x02:
			R_STDIO_Printf( " Bad input arguments" );
			break;

		case 0x03:
			R_STDIO_Printf( " Illegal null pointer" );
			break;

		case 0x04:
			R_STDIO_Printf( " Invalid request" );
			break;

		case 0x05:
			R_STDIO_Printf( " Timeout" );
			break;

		case 0xFF:
			R_STDIO_Printf( " Unknown error" );
			break;

		default:
			break;
	}
}
/******************************************************************************
   End of function  Peer_Display_Error
******************************************************************************/



/******************************************************************************
* Function Name: Gateway_Get_Slave_Index
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint8_t Gateway_Get_Slave_Index( uint16_t address )
{
	uint8_t i;

	for( i=0; i<Gateway.peer_device_counter; i++ )
	{
		if( ( ( ( address & 0xFF00 ) >> 8 ) == Gateway.Peer_Address[i].network_address[0] )
		&&    ( ( address & 0x00FF )        == Gateway.Peer_Address[i].network_address[1] ) )
		{
			break;
		}
	}

	return i;
}
/******************************************************************************
   End of function  Gateway_Reset_Peer_Address
******************************************************************************/

/******************************************************************************
* Function Name: Uart_Send_Ack
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t Uart_Send_Ack (  t_uart_data* tx_frame  )
{
	r_result_t 		result = R_RESULT_FAILED;

	/* Complete the buffer to send */
	tx_frame->packet.start_of_frame 		= 0xFE;
	tx_frame->packet.size					= SIZE_ACK_FRAME;
	tx_frame->packet.frame_counter			= Gateway.rx_frame_counter;
	tx_frame->packet.command				= COMMAND_TYPE_ACK;
	tx_frame->data[tx_frame->packet.size] 	= calcFCS( &tx_frame->data[2], tx_frame->packet.size - 2 );

	if( ( g_demo_config.devType == R_ADP_DEVICE_TYPE_PEER ) || ( g_demo_config.devType == R_ADP_DEVICE_TYPE_NOT_DEFINED ) )
	{
		/* Send command */
		result = R_STDIO_Send_Uart5( (uint8_t*)tx_frame, tx_frame->packet.size + 1 );

		/* Wait for previous transmission to finish */
		while( R_TRUE == R_STDIO_Debug_Get_Busy_Tx() )
		{
			nop();
		}
	}

	if( ( g_demo_config.devType == R_ADP_DEVICE_TYPE_COORDINATOR ) || ( g_demo_config.devType == R_ADP_DEVICE_TYPE_NOT_DEFINED ) )
	{
		/* Wait for previous transmission to finish */
		while( R_TRUE == R_STDIO_Get_Busy_Tx() )
		{
			nop();
		}

		R_STDIO_Set_Busy( R_TRUE );	// TODO macro ou fonction pour l'attente

		/* Send command to IHM */
		result = R_BSP_SendUart( (uint8_t*)tx_frame, tx_frame->packet.size + 1, R_BSP_RX_HOST_UART );

		/* Wait for previous transmission to finish */
		while( R_TRUE == R_STDIO_Get_Busy_Tx() )
		{
			nop();
		}
	}

	if( result == R_RESULT_SUCCESS )
	{
		R_STDIO_Printf( "\nSend Ack OK");
	}
	else
	{
		R_STDIO_Printf( "\nError, Send Ack KO, reason:  ");
		Peer_Display_Error( result );
	}

    return result;
}
/******************************************************************************
   End of function  Uart_Send_Ack
******************************************************************************/


/******************************************************************************
* Function Name: Uart_Send_Data_Frame
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t Uart_Send_Frame ( void )
{
	r_result_t 			result = R_RESULT_FAILED;
	uint8_t 			i;
	static t_uart_data	ptx_frame;
	static t_uart_data*	tx_frame = &ptx_frame;

	/* Copy the data into the tx frame buffer */
	for( i=0; i<=Gateway.frames_buffer[0].packet.size; i++ )
	{
		tx_frame->data[IDX_DATA+i] = Gateway.frames_buffer[0].data[IDX_DATA + i];;
	}

	/* Complete the buffer to send */
    tx_frame->packet.start_of_frame 		= 0xFE;
	tx_frame->packet.size					= ( Gateway.frames_buffer[0].packet.size + HEADER_FRAME_SIZE + FOOTER_FRAME_SIZE );
	tx_frame->packet.command				= Gateway.frames_buffer[0].packet.command;
	tx_frame->packet.frame_counter 			= Gateway.tx_frame_counter;
	tx_frame->data[tx_frame->packet.size] 	= calcFCS( &tx_frame->data[2], tx_frame->packet.size - 2 );

	if( ( g_demo_config.devType == R_ADP_DEVICE_TYPE_PEER ) || ( g_demo_config.devType == R_ADP_DEVICE_TYPE_NOT_DEFINED ) )
	{
		/* Send command to bridge */
		result = R_STDIO_Send_Uart5( (uint8_t*)tx_frame, tx_frame->packet.size + 1 );

		/* Wait for previous transmission to finish */
		while( R_TRUE == R_STDIO_Debug_Get_Busy_Tx() )
		{
			nop();
		}
	}

	if( ( g_demo_config.devType == R_ADP_DEVICE_TYPE_COORDINATOR ) || ( g_demo_config.devType == R_ADP_DEVICE_TYPE_NOT_DEFINED ) )
	{
		/* Wait for previous transmission to finish */
		while( R_TRUE == R_STDIO_Get_Busy_Tx() )
		{
			nop();
		}

		R_STDIO_Set_Busy( R_TRUE );

		/* Send command to IHM */
		result = R_BSP_SendUart( (uint8_t*)tx_frame, tx_frame->packet.size + 1, R_BSP_RX_HOST_UART );

		/* Wait for previous transmission to finish */
		while( R_TRUE == R_STDIO_Get_Busy_Tx() )
		{
			nop();
		}
	}


	if( ( result == R_RESULT_SUCCESS ) && ( Gateway.frames_buffer[0].packet.command == COMMAND_TYPE_CONTROL ) )
	{
		Gateway.wait_for_ack = 1;
	}



	if( result == R_RESULT_SUCCESS )
	{
		R_STDIO_Printf( "\nSend Frame OK");
	}
	else
	{
		R_STDIO_Printf( "\nError, Send Frame KO, reason:  ");
		Peer_Display_Error( result );
	}

    return result;
}
/******************************************************************************
   End of function  Uart_Send_Data_Frame
******************************************************************************/


/******************************************************************************
* Function Name: Gateway_Transmit_Data
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t Gateway_Transmit_Frame ( uint8_t type, uint8_t* data, uint8_t size_data, uint16_t exp )
{
	uint8_t		i 		= 0;
	r_result_t 	result 	= R_RESULT_FAILED;

	if( Gateway.frame_to_send < 9 )
	{
		/* Store the frame in the frame sender fifo */
		Gateway.frames_buffer[Gateway.frame_to_send].packet.size						= size_data;
		Gateway.frames_buffer[Gateway.frame_to_send].packet.command 					= type;
		if( exp == 0xFFFF )
		{
			Gateway.frames_buffer[Gateway.frame_to_send].packet.data.peer_device_idx 	= data[0];
		}
		else
		{
			Gateway.frames_buffer[Gateway.frame_to_send].packet.data.peer_device_idx 	= Gateway_Get_Slave_Index( exp );
		}
		for( i=1; i<=size_data; i++ )
		{
			Gateway.frames_buffer[Gateway.frame_to_send].packet.data.data[i-1] 			= data[i];
		}

		Gateway.frame_to_send ++;

		result = R_RESULT_SUCCESS;

		R_STDIO_Printf( "\nFrame %d bufferised", Gateway.frame_to_send );
	}
	else
	{
		R_STDIO_Printf( "\nError, tx frame buffer full");
	}

	return result;
}
/******************************************************************************
   End of function  Gateway_Transmit_Data
******************************************************************************/


/******************************************************************************
* Function Name: PLC_Send_Frame
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t PLC_Send_Frame (const char* data, const uint8_t size, const uint16_t address )
{
	r_result_t 	result = R_RESULT_FAILED;
	uint16_t 	dstAddress;
    uint16_t 	i;
    uint16_t 	nrOfFrames;
    uint16_t 	frameLength;

    frameLength = size;

    /* Concentrator address TODO */
	dstAddress = address;

    /* Check length */
    if (((frameLength + R_IPV6_HEADER_SIZE) + R_UDP_HEADER_SIZE) > R_DEMO_APP_NSDU_BUFFER_SIZE)
    {
    	frameLength = (R_DEMO_APP_NSDU_BUFFER_SIZE - R_IPV6_HEADER_SIZE) - R_UDP_HEADER_SIZE;
    }

    /* Discover route - TODO, config smartphone */
    g_demo_config.discoverRoute = 0;

    /* Priority normal - TODO, config smartphone */
    g_demo_config.qualityOfService = R_G3MAC_QOS_NORMAL;

	/* Call UDP frame send function. */
	result = R_DEMO_SendUdpFrame (	frameLength,
									g_demo_entity.panId,
									g_demo_entity.shortAddress,
									dstAddress,
									data );

	if( result == R_RESULT_SUCCESS )
	{
		R_STDIO_Printf( "\nSend Udp Frame OK, ");

	    R_STDIO_Printf ("\n");
	    for (i = 0; i < size; i++)
	    {
	        R_STDIO_Printf ("%.2X", data[i]);
	    }
	    R_STDIO_Printf ("\n");
	}
	else
	{
		R_STDIO_Printf( "\nError, Send Udp Frame failed, reason: ");
		Peer_Display_Error( result );
	}

    return result;
}
/******************************************************************************
   End of function  PLC_Send_Frame
******************************************************************************/


/******************************************************************************
* Function Name: Gateway_Set_Peer_Address
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void Gateway_Set_Peer_Address( uint8_t* network_address )
{
	r_result_t 	result 		= R_RESULT_FAILED;
    t_uart_data	tx_frame;
    r_boolean_t new_device	= 1;
    uint8_t		i 			= 0;

    /* Check if the slave is already known */
    for( i=0; i<Gateway.peer_device_counter; i++ )
    {
    	if( ( Gateway.Peer_Address[i].network_address[0] == network_address[0] )
    	 && ( Gateway.Peer_Address[i].network_address[1] == network_address[1] ) )
    	{
    		new_device = 0;
    		break;
    	}
    }

	Gateway.Peer_Address[i].network_address[0]  = network_address[0];
	Gateway.Peer_Address[i].network_address[1]  = network_address[1];

	/* Fill the buffer to send */
	tx_frame.packet.size					= 4;
	tx_frame.packet.command					= COMMAND_TYPE_DATA;
	tx_frame.packet.data.peer_device_idx	= i;

	/* Send an empty frame to start the plc, the first one takes more times than the others */
	result = PLC_Send_Frame(  tx_frame.packet.data.data,
							( tx_frame.packet.size-HEADER_FRAME_SIZE ),
							( ( Gateway.Peer_Address[i].network_address[0] << 8 )
							  + Gateway.Peer_Address[i].network_address[1] ) );

	/* Change state */
	Gateway.nwk_state = NWK_STATE_NETWORKED;

	/* Fill the buffer to send */
	tx_frame.packet.control.type	 						= CONTROL_CMD_SATE;
	tx_frame.packet.control.state.type						= CONTROL_STATE_NETWORK;
	tx_frame.packet.control.state.network.state				= Gateway.nwk_state;
	tx_frame.packet.control.state.network.peer_device_idx 	= i;
	tx_frame.packet.control.state.network.peer_address[0] 	= network_address[0];
	tx_frame.packet.control.state.network.peer_address[1]	= network_address[1];

	/* Send network state and firmware version to bridge */
	/* Send on UART bridge and UART IHM */
	Gateway_Transmit_Frame( COMMAND_TYPE_CONTROL,  &tx_frame.data[IDX_CONTROL], 6, 0xFFFF );

	if( new_device )
	{
		Gateway.peer_device_counter++;
	}
}
/******************************************************************************
   End of function  Gateway_Set_Peer_Address

******************************************************************************/

/******************************************************************************
* Function Name: Gateway_Reset_Peer_Address
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void Gateway_Reset_Peer_Address( uint8_t peer_device )
{
	uint8_t i;

	for( i=(peer_device-1); i<(Gateway.peer_device_counter-1); i++ )
	{
		Gateway.Peer_Address[i].network_address[0] = Gateway.Peer_Address[i+1].network_address[0];
		Gateway.Peer_Address[i].network_address[1] = Gateway.Peer_Address[i+1].network_address[1];
	}

	Gateway.Peer_Address[i].network_address[0] = 0xFF;
	Gateway.Peer_Address[i].network_address[1] = 0xFF;

	Gateway.peer_device_counter--;
}
/******************************************************************************
   End of function  Gateway_Reset_Peer_Address
******************************************************************************/

/******************************************************************************
* Function Name: Gateway_Set_Config
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void Gateway_Reset_Config( void )
{
	device_config	pconfig;
	device_config*	config = &pconfig;

	config->Config_saved	= 0;
	config->devtype 		= R_ADP_DEVICE_TYPE_NOT_DEFINED;
	config->Mac_address		= 0;
	config->PanID			= 0;
	r_demo_nvm_write( R_DEMO_G3_USE_PRIMARY_CH, R_NVM_ID_DEVICE_CONFIG, sizeof( config ), (uint8_t*)config );

	Gateway.config_saved = 0;
}
/******************************************************************************
   End of function  Gateway_Set_Config
******************************************************************************/

/******************************************************************************
* Function Name: Gateway_Set_Config
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void Gateway_Set_Config( r_adp_device_type_t device_type, uint16_t mac_address, uint16_t panID )
{
	device_config	pconfig;
	device_config*	config = &pconfig;

	config->Config_saved	= 1;
	config->devtype 		= device_type;
	config->Mac_address		= mac_address;
	config->PanID			= panID;
	r_demo_nvm_write( R_DEMO_G3_USE_PRIMARY_CH, R_NVM_ID_DEVICE_CONFIG, 8, (uint8_t*)config );

	Gateway.config_saved = 1;
}
/******************************************************************************
   End of function  Gateway_Set_Config
******************************************************************************/

/******************************************************************************
* Function Name: Gateway_Get_Config
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void Gateway_Get_Config( device_config* config )
{
	r_demo_nvm_read( R_DEMO_G3_USE_PRIMARY_CH, R_NVM_ID_DEVICE_CONFIG, 8, (uint8_t*)config );
}
/******************************************************************************
   End of function  Gateway_Get_Config
******************************************************************************/


/******************************************************************************
* Function Name: PLC_Gateway
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t PLC_Gateway( void )
{
	r_result_t 						result 	= R_RESULT_FAILED;
    uint8_t							i;
    uint16_t                        panIndex;
    r_adp_adpm_discovery_cnf_t *    disCfm;
    r_adp_adpm_network_join_req_t   nwjReq;
    r_adp_adpm_network_join_cnf_t * nwjCfm;
    uint8_t eui64arr[8];
    device_config* 					config;
    device_config					pconfig_r;
	device_config*					config_r = &pconfig_r;
    t_uart_data						tx_frame;
	t_uart_data						prx_frame;
	t_uart_data*					rx_frame = &prx_frame;
	uint8_t							retry_start_network = 0;
	uint8_t							retry_join_network = 0;
	t_data_frame					pplc_frame;
	t_data_frame*					plc_frame = &pplc_frame;

#ifdef TEST_FAB
	uint8_t test_frame_nb = 0;

	uint8_t test_fab_tab[5][25] = { { 0x01, 0x01, 0x01, 0x01, 0x10, 0x01, 0x0B, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x01, 0x00, 0x06, 0x00, 0x00, 0x00, 0x10, 0x01 },
									{ 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x08, 0x01, 0x02, 0x03, 0x04, 0x10, 0x20, 0x21, 0x01, 0x02, 0x03, 0x01, 0x01, 0x01, 0x01 },
									{ 0x01, 0x02, 0x01, 0x01, 0x01, 0x01, 0x09, 0x01, 0x02, 0x03, 0x04, 0x10, 0x20, 0x21, 0x01, 0x02, 0x03, 0x01, 0x01, 0x01, 0x01 },
									{ 0x01, 0x03, 0x01, 0x01, 0x01, 0x01, 0x09, 0x01, 0x02, 0x03, 0x04, 0x10, 0x20, 0x21, 0x01, 0x02, 0x03, 0x01, 0x01, 0x01, 0x01 },
									{ 0x01, 0x04, 0x01, 0x01, 0x01, 0x02, 0x0B, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x17, 0x18, 0x10, 0x20, 0x21, 0x01, 0x02, 0x03, 0x01, 0x01, 0x01, 0x01 } };

#endif

    Gateway.nwk_state 			= NWK_STATE_UNINITIALISED;
    Gateway.nwk_state_discovery = NWK_STATE_NO_DISCOVERY;
    Gateway.config_saved		= 0;
    Gateway.timeout_connection	= 0;
    Gateway.start_timeout_time	= 0;
    Gateway.peer_device_counter = 0;
    Gateway.tx_frame_counter	= 0;
    Gateway.rx_frame_counter	= 0;
    Gateway.frame_to_send		= 0;
    Gateway.ack_timeout			= 0;
    Gateway.tx_retry			= 0;
    Gateway.wait_for_ack		= 0;
    Gateway.error_plc			= 0;
    Gateway.rx_frame_size		= 0;
    Gateway.debug_cnt_tx_retry	= 0;
	Gateway.debug_cnt_rx_ack	= 0;
	Gateway.missing_rx_frame	= 0;
	Gateway.debug_ack_success	= 0;
	Gateway.debug_ack_error		= 0;
    g_demo_config.devType 		= R_ADP_DEVICE_TYPE_NOT_DEFINED;

    /* Reset peer address */
    for( i=0; i<10; i++ )
    {
    	Gateway.Peer_Address[i].network_address[0] = 0xFF;
    	Gateway.Peer_Address[i].network_address[1] = 0xFF;
    }

	/* Init On-Chip ROM */
    r_demo_nvm_config_init ();

	/* Read config in nv memory */
	Gateway_Get_Config( config_r );

/********* Bypass Peer config - Test gateway plc *********/
#ifdef PEER_DEVICE
	Gateway.nwk_state 			= NWK_STATE_NETWORKED;
	g_demo_config.appMode 		= R_DEMO_MODE_SIMPLE;
	g_demo_config.devType 		= R_ADP_DEVICE_TYPE_PEER;
	g_demo_config.deviceEUI64 	= PEER_MAC_ADDRESS;

	/* Set EUI64 address. */
    R_STDIO_Printf ("\n -> MAC address set to 0x%.8X%.8X \n", (uint32_t)(g_demo_config.deviceEUI64 >> 32), (uint32_t)g_demo_config.deviceEUI64);
    R_BYTE_UInt64ToArr (g_demo_config.deviceEUI64, eui64arr);

    /* Set PSK. */
    R_STDIO_Printf ("\n----------------------Setting PSK.------------------------");
    R_memcpy (g_demo_config.pskKey, g_rom_nvm_def_psk, sizeof (g_rom_nvm_def_psk));

    /* Init the ADP */
    if (R_DEMO_AdpInit (R_DEMO_G3_USE_PRIMARY_CH) != R_RESULT_SUCCESS)
    {
        R_DEMO_HndFatalError ();
    }

    /* Set the device type */
    if (R_DEMO_AppResetDevice () != R_RESULT_SUCCESS)
    {
        R_DEMO_HndFatalError ();
    }

	/* Start discovery and check if a network has been discovered. */
    while (R_DEMO_AppNetworkDiscovery () == 0)
    {
        /* wait */
    }

	/* Save number of network available */
	disCfm = (r_adp_adpm_discovery_cnf_t *)&g_g3cb[R_DEMO_G3_USE_PRIMARY_CH].adpmDiscoveryCnf;

	/* Select the first PanID */
	nwjReq.panId = disCfm->PANDescriptor[0].panId;

	/* Look for the coordinator */
	for( i=0; i<disCfm->PANCount; i++ )
	{
		if( ( disCfm->PANDescriptor[i].address[0] == 0 ) && ( disCfm->PANDescriptor[i].address[1] == 0 ) )
		{
			break;
		}
	}

    R_memcpy (nwjReq.lbaAddress, disCfm->PANDescriptor[i].address, 2);

    /* Try to connect */
    do{
    	result = R_DEMO_AdpmNetworkJoin (R_DEMO_G3_USE_PRIMARY_CH, &nwjReq, &nwjCfm);
    }while( ( R_RESULT_SUCCESS != result ) && ( Gateway.timeout_connection++ < 3 ) );

    if (( result == R_RESULT_SUCCESS) &&
        (R_ADP_STATUS_SUCCESS == nwjCfm->status))
    {
        g_demo_entity.panId           = nwjCfm->panId;
        g_demo_entity.shortAddress    = R_BYTE_ArrToUInt16 ((uint8_t *)nwjCfm->networkAddress);

    	R_STDIO_Printf( "\n Wait for data from bridge" );
    }
    else
    {
        R_STDIO_Printf( "\nConnection failed" );
    }

#endif
/*********************** End bypass ***********************/

/********* Bypass Concentrator config - Test gateway plc *********/
#ifdef CONCENTRATOR_DEVICE
	Gateway.nwk_state 			= NWK_STATE_NETWORKED;
	g_demo_config.appMode 		= R_DEMO_MODE_SIMPLE;

	/* Set EUI64 address. */
	R_STDIO_Printf ("\n -> MAC address set to 0x%.8X%.8X \n", (uint32_t)(g_demo_config.deviceEUI64 >> 32), (uint32_t)g_demo_config.deviceEUI64);
	R_BYTE_UInt64ToArr (g_demo_config.deviceEUI64, eui64arr);

	/* Set PSK. */
	R_STDIO_Printf ("\n----------------------Setting PSK.------------------------");
	R_memcpy (g_demo_config.pskKey, g_rom_nvm_def_psk, sizeof (g_rom_nvm_def_psk));

	/* Set GMK. */
	R_STDIO_Printf ("\n----------------------Setting GMK.------------------------");
	R_memcpy (g_demo_config.gmk0, g_rom_nvm_def_gmk[0], 16);
	R_memcpy (g_demo_config.gmk1, g_rom_nvm_def_gmk[1], 16);

	/* Init the EAP */
	if (R_DEMO_EapInit (R_DEMO_G3_USE_PRIMARY_CH) != R_RESULT_SUCCESS)
	{
		R_DEMO_HndFatalError ();
	}

	/* Set the device type */
	if (R_DEMO_AppResetDevice () != R_RESULT_SUCCESS)
	{
		R_DEMO_HndFatalError ();
	}

	R_DEMO_AppNetworkStart (g_demo_config.panId);
	R_STDIO_Printf ("\nNetwork ready, wait for Peer device");

#endif
/*********************** End bypass ***********************/

#ifndef PEER_DEVICE
	/* Auto config after shut down */
	/* Existing config? */
	if( config_r->Config_saved )
	{
		/* gte back the previous config */
		g_demo_config.devType		= config_r->devtype;
		g_demo_config.deviceEUI64 	= ( 0xABCDABCDABCD0000 + config_r->Mac_address );
		g_demo_config.panId			= config_r->PanID;
		g_demo_config.appMode 		= R_DEMO_MODE_SIMPLE;

		/* Set EUI64 address. */
		R_STDIO_Printf ("\n -> MAC address set to 0x%.8X%.8X \n", (uint32_t)(g_demo_config.deviceEUI64 >> 32), (uint32_t)g_demo_config.deviceEUI64);
		R_BYTE_UInt64ToArr (g_demo_config.deviceEUI64, eui64arr);

		/* Set PSK. */
		R_STDIO_Printf ("\n----------------------Setting PSK.------------------------");
		R_memcpy (g_demo_config.pskKey, g_rom_nvm_def_psk, sizeof (g_rom_nvm_def_psk));

		if( g_demo_config.devType == R_ADP_DEVICE_TYPE_PEER )
		{
			/* Init the ADP */
			if (R_DEMO_AdpInit (R_DEMO_G3_USE_PRIMARY_CH) != R_RESULT_SUCCESS)
			{
				R_DEMO_HndFatalError ();
			}

			/* Set the device type */
			if (R_DEMO_AppResetDevice () != R_RESULT_SUCCESS)
			{
				R_DEMO_HndFatalError ();
			}

			/* Start discovery and check if a network has been discovered. */
			while (R_DEMO_AppNetworkDiscovery () == 0)
			{
				/* wait */
			}

			/* Save number of network available */
			disCfm = (r_adp_adpm_discovery_cnf_t *)&g_g3cb[R_DEMO_G3_USE_PRIMARY_CH].adpmDiscoveryCnf;

			/* Select the first PanID */
			nwjReq.panId = disCfm->PANDescriptor[0].panId;

			/* Look for the coordinator */
			for( i=0; i<disCfm->PANCount; i++ )
			{
				if( ( disCfm->PANDescriptor[i].address[0] == 0 ) && ( disCfm->PANDescriptor[i].address[1] == 0 ) )
				{
					break;
				}
			}

			R_memcpy( nwjReq.lbaAddress, disCfm->PANDescriptor[i].address, 2 );

			/* Try to connect */
			do{
				result = R_DEMO_AdpmNetworkJoin( R_DEMO_G3_USE_PRIMARY_CH, &nwjReq, &nwjCfm );
			}while( ( R_RESULT_SUCCESS != result ) && ( Gateway.timeout_connection++ < 3 ) );

			if( ( result == R_RESULT_SUCCESS) &&
				( R_ADP_STATUS_SUCCESS == nwjCfm->status ) )
			{
				g_demo_entity.panId        	= nwjCfm->panId;
				g_demo_entity.shortAddress  = R_BYTE_ArrToUInt16 ( ( uint8_t * )nwjCfm->networkAddress );

				/* Ready like before the shut down */
				Gateway.nwk_state 			= NWK_STATE_NETWORKED;
			}
			else
			{
				R_STDIO_Printf( "\nConnection failed" );
			}
		}
		else if( g_demo_config.devType == R_ADP_DEVICE_TYPE_COORDINATOR )
		{
			/* Set GMK. */
			R_STDIO_Printf ("\n----------------------Setting GMK.------------------------");
			R_memcpy( g_demo_config.gmk0, g_rom_nvm_def_gmk[0], 16);
			R_memcpy( g_demo_config.gmk1, g_rom_nvm_def_gmk[1], 16);

			/* Init the EAP */
			if ( R_DEMO_EapInit( R_DEMO_G3_USE_PRIMARY_CH ) != R_RESULT_SUCCESS )
			{
				R_DEMO_HndFatalError();
			}

			/* Set the device type */
			if (R_DEMO_AppResetDevice() != R_RESULT_SUCCESS )
			{
				R_DEMO_HndFatalError();
			}

			/* Relaunch the previous network */
			result = R_DEMO_AppNetworkStart( g_demo_config.panId );
			if( result == R_RESULT_SUCCESS )
			{
				/* Change staten, ready for peer device */
				Gateway.nwk_state = NWK_STATE_NETWORKED;
			}
			else
			{
				/* Unable to relaunch the previous network, restart as new device */
				Gateway.nwk_state = NWK_STATE_UNINITIALISED;
			}
		}
		else
		{
			/* Shit! */
		}
	}
	else
	{
		/* Save config */
		Gateway_Set_Config( g_demo_config.devType, (g_demo_config.deviceEUI64 & 0x000000000000FFFF), g_demo_config.panId );
	}
#endif


    while( 1 )
    {

		switch( Gateway.nwk_state )
		{
			case NWK_STATE_UNINITIALISED:
				/* Ack received ? */
				if( Gateway.wait_for_ack )
				{
					Gateway.wait_for_ack = 0;

					/* Get time to start timeout */
					Gateway.start_timeout_time = R_UIF_GetCurrentTimeMsec();

					/* Change state */
					Gateway.nwk_state = NWK_STATE_WAIT_CONFIG;
				}
				else
				{
					/* Set control frame parameters */
					tx_frame.packet.control.type	 			= CONTROL_CMD_SATE;
					tx_frame.packet.control.state.type			= CONTROL_STATE_NETWORK;
					tx_frame.packet.control.state.network.state	= NWK_STATE_UNINITIALISED;

					/* Send network state and firmware version to bridge */
					/* Send on UART bridge and UART IHM */
					result = Gateway_Transmit_Frame( COMMAND_TYPE_CONTROL,  &tx_frame.data[IDX_CONTROL], 3, 0xFFFF );

					if( result == R_RESULT_SUCCESS )
					{

					}
				}
				break;

			case NWK_STATE_WAIT_CONFIG:
				/* Ack recevied ? */
				if( Gateway.wait_for_ack )
				{
					Gateway.wait_for_ack = 0;

					/* Change state */
					Gateway.nwk_state = NWK_STATE_NONE;
				}
				/* Check if the bridge sent a frame */
				else if( Gateway.rx_frame_size != 0 ) // TODO remove?
				{
					/* Ready for next frame */
//					Gateway.rx_frame_size = 0; // TODO manage it differently

					/* Check frame parameters */
				   if(  ( rx_frame->packet.command					== COMMAND_TYPE_CONTROL 		 )
				   &&   ( rx_frame->packet.control.type				== CONTROL_CMD_DEVICE_TYPE 		 )
				   && ( ( rx_frame->packet.control.device_plc.type	== R_ADP_DEVICE_TYPE_PEER 		 )
					 || ( rx_frame->packet.control.device_plc.type	== R_ADP_DEVICE_TYPE_COORDINATOR ) ) )
					{
						/* Save device type */
						g_demo_config.devType = rx_frame->packet.control.device_plc.type;


						/* Get mac address */
						g_demo_config.deviceEUI64 = ( ( rx_frame->packet.control.device_plc.mac_address[0] << 8 )
													  + rx_frame->packet.control.device_plc.mac_address[1] );

						/* Check configuration received */
						if( g_demo_config.devType == R_ADP_DEVICE_TYPE_PEER )
						{
							/* Check mac address */
							if( ( g_demo_config.deviceEUI64!= 0 ) && ( g_demo_config.deviceEUI64 != 0xFFFF ) )
							{
								/* Validate mac address */
								g_demo_config.deviceEUI64 += 0xABCDABCDABCD0000;

								/* Change state */
								//Gateway.nwk_state = NWK_STATE_NONE;
								R_STDIO_Printf( "\n Network state: NWK_STATE_NONE" );

								/* Set control frame parameters */
								tx_frame.packet.control.type 				= CONTROL_CMD_SATE;
								tx_frame.packet.control.state.type			= CONTROL_STATE_NETWORK;
								tx_frame.packet.control.state.network.state	= NWK_STATE_NONE;

								/* Send state to confirm or not the configuration */
								result = Gateway_Transmit_Frame( COMMAND_TYPE_CONTROL,  &tx_frame.data[IDX_CONTROL], 3, 0xFFFF );
							}
							else
							{
								/* Change state */
								Gateway.nwk_state = NWK_STATE_UNINITIALISED;
							}
						}
						else if( g_demo_config.devType == R_ADP_DEVICE_TYPE_COORDINATOR )
						{
							/* Check mac address */
							if( g_demo_config.deviceEUI64 != 0xFFFF )
							{
								/* Validate mac address */
								g_demo_config.deviceEUI64 += 0xABCDABCDABCD0000;

								/* Save PanID */
								g_demo_config.panId = ( ( rx_frame->packet.control.device_plc.panID[0] << 8 ) + rx_frame->packet.control.device_plc.panID[1] );

								/* Set EUI64 address. */
								R_BYTE_UInt64ToArr( g_demo_config.deviceEUI64, eui64arr );

								/* Set PSK. */
								R_memcpy( g_demo_config.pskKey, g_rom_nvm_def_psk, sizeof( g_rom_nvm_def_psk ) );

								/* Set GMK. */
								R_memcpy( g_demo_config.gmk0, g_rom_nvm_def_gmk[0], 16 );
								R_memcpy( g_demo_config.gmk1, g_rom_nvm_def_gmk[1], 16 );

								/* Init the EAP */
								if( R_DEMO_EapInit( R_DEMO_G3_USE_PRIMARY_CH ) == R_RESULT_SUCCESS )
								{
									/* Set the device type */
									if( R_DEMO_AppResetDevice() == R_RESULT_SUCCESS )
									{
										/* Set control frame parameters */
										tx_frame.packet.control.type 				= CONTROL_CMD_SATE;
										tx_frame.packet.control.state.type			= CONTROL_STATE_NETWORK;
										tx_frame.packet.control.state.network.state	= NWK_STATE_NONE;

										/* Send state to confirm or not the configuration */
										result = Gateway_Transmit_Frame( COMMAND_TYPE_CONTROL,  &tx_frame.data[IDX_CONTROL], 3, 0xFFFF );
									}
									else
									{
										/* Change state */
										Gateway.nwk_state = NWK_STATE_UNINITIALISED;
									}
								}
								else
								{
									/* Change state */
									Gateway.nwk_state = NWK_STATE_UNINITIALISED;
								}
							}
							else
							{
								/* Change state */
								Gateway.nwk_state = NWK_STATE_UNINITIALISED;
							}
						}
						else
						{
							/* Change state */
							Gateway.nwk_state = NWK_STATE_UNINITIALISED;
						}
					}
					else
					{
						/* Change state */
						Gateway.nwk_state = NWK_STATE_UNINITIALISED;
					}
				}
				/* Timeout wait config frame ? */
				if( R_UIF_ChkTimeoutMsec( Gateway.start_timeout_time, 500 ) )
				{
					/* Restart process until a bridge answers */
					Gateway.nwk_state = NWK_STATE_UNINITIALISED;
				}
				break;

			case NWK_STATE_NONE:
				if( g_demo_config.devType == R_ADP_DEVICE_TYPE_PEER )
				{
					switch( Gateway.nwk_state_discovery )
					{
						case NWK_STATE_NO_DISCOVERY:
							/* Change discover state */
							Gateway.nwk_state_discovery = NWK_STATE_DISCOVERING;
							R_STDIO_Printf( "\n Network discovery state: NWK_STATE_DISCOVERING" );

							/* Set control frame parameters */
							tx_frame.packet.control.type 			= CONTROL_CMD_SATE;
							tx_frame.packet.control.state.type		= CONTROL_STATE_DISCOVERY;
							tx_frame.packet.control.state.discovey	= Gateway.nwk_state_discovery;

							/* Send new state */
							result = Gateway_Transmit_Frame( COMMAND_TYPE_CONTROL,  &tx_frame.data[IDX_CONTROL], 3, 0xFFFF );
							break;

						case NWK_STATE_DISCOVERING:
							while( Gateway.discover_continue )
							{
								/* Start discovery and check if a network has been discovered. */
								R_DEMO_AppNetworkDiscovery ();

								/* Save number of network available */
								disCfm = ( r_adp_adpm_discovery_cnf_t * )&g_g3cb[R_DEMO_G3_USE_PRIMARY_CH].adpmDiscoveryCnf;

								/* Discovery ok? */
								if( disCfm->PANCount != 0 )
								{
									/* Send networks available to bridge */
									R_STDIO_Gets( ( char * )g_demo_buff.getStringBuffer );			// ????????????? todo send to bridge

									if( strlen ( ( char * )g_demo_buff.getStringBuffer ) == 1 )
									{
									   panIndex = ( uint16_t )atoi( ( char const * )g_demo_buff.getStringBuffer );
									}

									/* Change discover state */
									Gateway.nwk_state_discovery = NWK_STATE_DISCOVERY_COMPLETE;
									R_STDIO_Printf( "\n Network discovery state: NWK_STATE_DISCOVERY_COMPLETE" );

									/* Set control frame parameters */
									tx_frame.packet.control.type 			= CONTROL_CMD_SATE;
									tx_frame.packet.control.state.type		= CONTROL_STATE_DISCOVERY;
									tx_frame.packet.control.state.discovey	= Gateway.nwk_state_discovery;

									/* Send new state */
									result = Gateway_Transmit_Frame( COMMAND_TYPE_CONTROL,  &tx_frame.data[IDX_CONTROL], 3, 0xFFFF );
								}
								else
								{
									/* Restart discover */
								}
							}
							break;

						case NWK_STATE_DISCOVERY_COMPLETE:
							/* Wait network selection from bridge */

	//						panIndex = id_network;

							/* Network selected ok */
							if ( panIndex < disCfm->PANCount )
							{
								/* Reset discover state */
								Gateway.nwk_state_discovery = NWK_STATE_NO_DISCOVERY;
								R_STDIO_Printf( "\n Network discovery state: NWK_STATE_NO_DISCOVERY" );

								/* Change state */
								Gateway.nwk_state = NWK_STATE_NETWORKING;
								R_STDIO_Printf( "\n Network state: NWK_STATE_NETWORKING" );
							}
							break;

						default:
							break;
					}
				}
				else if( g_demo_config.devType == R_ADP_DEVICE_TYPE_COORDINATOR )
				{
					/* Ack received ? */ // TODO modifiy one bridge com ready
					if( Gateway.wait_for_ack )
					{
						Gateway.wait_for_ack = 0;

						/* Change state */
						Gateway.nwk_state = NWK_STATE_NETWORKING;
					}
					else
					{
						/* Start network */
						do{
							result = R_DEMO_AppNetworkStart( g_demo_config.panId );
						}while( ( result != R_RESULT_SUCCESS ) && ( retry_start_network++ < 3 ) );

						if( result == R_RESULT_SUCCESS )
						{
							/* Set control frame parameters */
							tx_frame.packet.control.type 				= CONTROL_CMD_SATE;
							tx_frame.packet.control.state.type			= CONTROL_STATE_NETWORK;
							tx_frame.packet.control.state.network.state	= NWK_STATE_NETWORKING;

							/* Send new state */
							result = Gateway_Transmit_Frame( COMMAND_TYPE_CONTROL,  &tx_frame.data[IDX_CONTROL], 3, 0xFFFF );
						}
						else
						{
							/* Error starting network, send error */
							Gateway.nwk_state = NWK_STATE_UNINITIALISED;
						}
					}
				}
				break;

			case NWK_STATE_NETWORKING:
				if( g_demo_config.devType == R_ADP_DEVICE_TYPE_PEER )
				{
					/* Join network */
					/* Get corresponding PAN ID and LBA address. */
					nwjReq.panId = disCfm->PANDescriptor[panIndex].panId;
					R_memcpy( nwjReq.lbaAddress, disCfm->PANDescriptor[panIndex].address, 2 );

					/* Network joined */
					do
					{
						result = R_DEMO_AdpmNetworkJoin( R_DEMO_G3_USE_PRIMARY_CH, &nwjReq, &nwjCfm );
					}while( ( result != R_RESULT_SUCCESS ) && ( R_ADP_STATUS_SUCCESS == nwjCfm->status ) && ( retry_join_network++ < 3 ) );

					if( ( result == R_RESULT_SUCCESS ) && ( R_ADP_STATUS_SUCCESS == nwjCfm->status ) )
					{
						g_demo_entity.panId           = nwjCfm->panId;
						g_demo_entity.shortAddress    = R_BYTE_ArrToUInt16 ( ( uint8_t * )nwjCfm->networkAddress );

						/* Change state */
						Gateway.nwk_state = NWK_STATE_NETWORKED;

						/* Save config */
						Gateway_Set_Config( g_demo_config.devType, ( g_demo_config.deviceEUI64 & 0x000000000000FFFF ), g_demo_config.panId );
					}
					else
					{
						/* Join failed */

							/* Send error to bridge */
					}
				}
				else if( g_demo_config.devType == R_ADP_DEVICE_TYPE_COORDINATOR )
				{
					/* Save the configuration once a peer is connected */
					if( !Gateway.config_saved )
					{
						/* Save config */
						Gateway_Set_Config( g_demo_config.devType, (g_demo_config.deviceEUI64 & 0x000000000000FFFF), g_demo_config.panId );
					}
				}
				break;

			case NWK_STATE_NETWORKED:
#ifdef TEST_FLOW
					/* Set control frame parameters */
					tx_frame.packet.size					= 21;
					tx_frame.packet.data.peer_device_idx 	= 0;
					tx_frame.packet.data.data[0]			= Gateway.tx_frame_counter;
					for( i=1; i<tx_frame.packet.size; i++ )
					{
						tx_frame.packet.data.dat = i;
					}

					/* Send new state */
					result = Gateway_Transmit_Frame( COMMAND_TYPE_DATA,  &tx_frame.data[IDX_DATA], tx_frame.packet.size, 0xFFFF );
#endif


#ifdef TEST_FAB
					if( test_frame_nb == 0)
					{
						tx_frame.packet.size 					= 24;
						tx_frame.packet.data.peer_device_idx 	= 0;

						for( i=0; i<tx_frame.packet.size; i++ )
						{
							tx_frame.packet.data.data[i] = test_fab_tab[test_frame_nb][i];
						}

						/* Send frame */
						result = Gateway_Transmit_Frame( COMMAND_TYPE_DATA,  &tx_frame.data[IDX_DATA], tx_frame.packet.size, 0xFFFF );
//						test_frame_nb++;
					}

#endif

					if( Gateway.rx_frame_size != 0 )
					{
						switch( rx_frame->packet.command )
						{
							case COMMAND_TYPE_CONTROL:
								/* Ack received ? */  // TODO verify ack
								if( Gateway.wait_for_ack )
								{
									Gateway.wait_for_ack = 0;
								}
								else if( Gateway.rx_frame_size != 0 )
								{
									switch( rx_frame->packet.control.type)
									{
										case CONTROL_CMD_CONFIG:
											/* Reset config? */
											if( rx_frame->packet.control.config.cmd == 0 )
											{
												/* Reset config */
												Gateway_Reset_Config( );

												/* Send ack applicative */
											}
											/* Get config ? */
											else if( rx_frame->packet.control.config.cmd == 1 )
											{
												/* Read config in nv memory */
												Gateway_Get_Config( config_r );

												/* Set control frame parameters */
												tx_frame.packet.command								= COMMAND_TYPE_CONTROL;
												tx_frame.packet.control.type 						= CONTROL_CMD_DEVICE_TYPE;
												tx_frame.packet.control.device_plc.type				= config_r->devtype;
												tx_frame.packet.control.device_plc.mac_address[0]	= ( ( config_r->Mac_address & 0xFF00 ) >> 8 );
												tx_frame.packet.control.device_plc.mac_address[1]	=   ( config_r->Mac_address & 0x00FF );
												tx_frame.packet.control.device_plc.panID[0]			= ( ( config_r->PanID & 0xFF00 ) >> 8 );
												tx_frame.packet.control.device_plc.panID[1]			=   ( config_r->PanID & 0x00FF );

												/* Send state to confirm or not the configuration */
												// TODO error data.data index
												result = Gateway_Transmit_Frame( COMMAND_TYPE_CONTROL,  &tx_frame.data[IDX_CONTROL], 7, 0xFFFF );
											}
											break;

										default:
											break;
									}
	//								/* Ready for next frame */
	//								Gateway.rx_frame_size = 0;
								}
								break;

							case COMMAND_TYPE_DATA:
								if( Gateway.rx_frame_size != 0 )
								{
									/* Save the device index */
									plc_frame->peer_device_idx = rx_frame->packet.data.peer_device_idx;

									/* Save the frame data */
									for( i=0; i<rx_frame->packet.size-HEADER_FRAME_SIZE-FOOTER_FRAME_SIZE-1; i++ )
									{
										plc_frame->data[i] = rx_frame->packet.data.data[i];
									}

									if( g_demo_config.devType == R_ADP_DEVICE_TYPE_COORDINATOR )
									{
										/* Send data to slave */
										result = PLC_Send_Frame(  (uint8_t*)plc_frame,
																( rx_frame->packet.size-HEADER_FRAME_SIZE-FOOTER_FRAME_SIZE ),
																( ( Gateway.Peer_Address[plc_frame->peer_device_idx].network_address[0] << 8 )
																  + Gateway.Peer_Address[plc_frame->peer_device_idx].network_address[1] ) );

										if( result == R_RESULT_SUCCESS )
										{
											/* Set control frame parameters */
											tx_frame.packet.control.type 				= CONTROL_CMD_PLC_OK;

											/* Send state to confirm or not the configuration */
											result = Gateway_Transmit_Frame( COMMAND_TYPE_CONTROL,  &tx_frame.data[IDX_CONTROL], 1, 0xFFFF );
										}
										else
										{
											/* Set control frame parameters */
											tx_frame.packet.control.type 				= CONTROL_CMD_PLC_KO;

											/* Send state to confirm or not the configuration */
											result = Gateway_Transmit_Frame( COMMAND_TYPE_CONTROL,  &tx_frame.data[IDX_CONTROL], 1, 0xFFFF );

										}

	#ifdef TEST_FAB
	//									if( test_frame_nb < 4 )
	//									{
	//										if( test_frame_nb == 2 )
	//											tx_frame.packet.size 				= 22;
	//										else if( test_frame_nb == 3 )
	//											tx_frame.packet.size 				= 26;
	//
	//										tx_frame.packet.data.peer_device_idx 	= 0;
	//
	//										for( i=0; i<tx_frame.packet.size; i++ )
	//										{
	//											tx_frame.packet.data.data[i] = test_fab_tab[test_frame_nb][i];
	//										}
	//
	//										/* Send frame */
	//										result = Gateway_Transmit_Frame( COMMAND_TYPE_DATA,  &tx_frame.data[IDX_DATA], tx_frame.packet.size, 0xFFFF );
	//										test_frame_nb++;
	//									}
	#endif
									}
									else if( g_demo_config.devType == R_ADP_DEVICE_TYPE_PEER )
									{
										/* Send data to master */
										result = PLC_Send_Frame(  (uint8_t*)plc_frame,
																( rx_frame->packet.size-HEADER_FRAME_SIZE-FOOTER_FRAME_SIZE ),
																  0x0000 );
									}

									if( result == R_RESULT_FAILED )
									{
										Gateway.error_plc ++;
									}

	//								/* Ready for next frame */
	//								Gateway.rx_frame_size = 0;
								}
								break;

							case COMMAND_TYPE_ACK:
								/* Ack received ? */
								if( Gateway.wait_for_ack )
								{
									Gateway.wait_for_ack = 0;
								}
								break;

							default:
								break;
						} /* End switch */
					}
				break;

			default:
				break;
		}


		/* Ready for next frame */
		Gateway.rx_frame_size = 0;

		do
		{
		    /****************************** SEND FRAME *******************************/
			/* Frame to send, no ack to wait ? */
			if( ( Gateway.frame_to_send != 0 ) && ( Gateway.ack_timeout == 0 ) )
			{
				/* Frame sent? */
				if( R_RESULT_SUCCESS == Uart_Send_Frame() )
				{
					/* Start ack timeout */
					Gateway.ack_timeout = R_UIF_GetCurrentTimeMsec( );
				}
				else
				{
					Gateway.ack_timeout = 0;
				}
			}
		    /**************************** END SEND FRAME *****************************/


		    /****************************** CHECK UART *******************************/
			if( Gateway.rx_frame_size == 0 )
			{
				/* Get back the frame, check the uart associated to the device type */
				switch( g_demo_config.devType )
				{
					case R_ADP_DEVICE_TYPE_PEER:
						Gateway.rx_frame_size = Uart_Check_Frame( (uint8_t*)rx_frame );
						break;

					case R_ADP_DEVICE_TYPE_COORDINATOR:
						Gateway.rx_frame_size = Uart_IHM_Check_Frame( (uint8_t*)rx_frame );
						break;

					default:
						Gateway.rx_frame_size = Uart_IHM_Check_Frame( (uint8_t*)rx_frame );

						/* Nothing on uart ihm, checks bridge */
						if( Gateway.rx_frame_size == 0 )
						{
							Gateway.rx_frame_size = Uart_Check_Frame( (uint8_t*)rx_frame );
						}
						break;
				}
			}
		    /**************************** END CHECK UART *****************************/


		    /*************************** FRAME PROCESSING ****************************/
			/* Check frame received */
			if( Gateway.rx_frame_size != 0 )
			{
				/* Type of frame received ? */
				switch( rx_frame->packet.command )
				{
					case COMMAND_TYPE_CONTROL:
					case COMMAND_TYPE_DATA:
						/* Frame counter correct? */
						if( rx_frame->packet.frame_counter != Gateway.rx_frame_counter )
						{
							/* Check if there is a lack of frame */
							if( rx_frame->packet.frame_counter > ( Gateway.rx_frame_counter + 1 ) )
							{
								Gateway.missing_rx_frame++;
							}

							/* Save frame counter */
							Gateway.rx_frame_counter = rx_frame->packet.frame_counter;

							/* Set Ack */
							tx_frame.packet.ack.value = FRAME_SUCCESS;

							Gateway.debug_ack_success++;

							R_STDIO_Printf( "\nRx frame %d OK", Gateway.rx_frame_counter );
						}
						else
						{
							/* Wrong frame counter, send error */
							tx_frame.packet.ack.value = FRAME_BAD_COUNTER;

							/* Ready for next frame */
							Gateway.rx_frame_size = 0;

							Gateway.debug_ack_error++;

							R_STDIO_Printf( "\nRx frame KO, bad frame counter: %d", Gateway.rx_frame_counter);
						}

						/* Send ack */
						result = Uart_Send_Ack( &tx_frame );
						break;

					case COMMAND_TYPE_ACK:
						/* Frame counter correct? */
						if( ( rx_frame->packet.frame_counter == Gateway.tx_frame_counter )
						&& ( rx_frame->packet.ack.value == FRAME_SUCCESS ) )
						{
							/* Ok good ack, throw the frame from the tx FIFO */
							if( Gateway.frame_to_send != 0)
							{
								Gateway.frame_to_send--;

								for( i=0; i<Gateway.frame_to_send; i++ )
								{
									Gateway.frames_buffer[i] = Gateway.frames_buffer[i+1];
								}
							}

							/* Reset retry counter and timeout */
							Gateway.tx_retry = 0;
							Gateway.ack_timeout = 0;

							/* Update frame counter */
							if( Gateway.tx_frame_counter == 255 )
							{
								Gateway.tx_frame_counter = 0;
							}
							else
							{
								Gateway.tx_frame_counter++;
							}
							Gateway.debug_cnt_rx_ack++;

							R_STDIO_Printf( "\nRx Ack frame");
						}
						else
						{
							Gateway.debug_ack_error++;
						}

						/* Ready for next frame */
						Gateway.rx_frame_size = 0;
						break;

					default:
						R_STDIO_Printf( "\nRx invalid frame, unknow command");

						/* Wrong frame counter, send error */
						tx_frame.packet.ack.value = FRAME_INVALID_CMD;

						/* Drop the frame */
						Gateway.rx_frame_size = 0;

						/* Send ack */
						result = Uart_Send_Ack( &tx_frame );
						break;
				}
			}
			else
			{
				/* Ack timeout started? */
				if( Gateway.ack_timeout != 0 )
				{
					/* Check UART until we receive something or until the timeout is reached, 50ms */
					if( R_UIF_ChkTimeoutMsec( Gateway.ack_timeout, 10 ) )
					{
						Gateway.debug_error_rx_ack++;

						/* Retry if possible */
						if( Gateway.tx_retry < 3 )
						{
							/* Send retry */
							Gateway.tx_retry++;
							Gateway.debug_cnt_tx_retry++;

							R_STDIO_Printf( "\nRetry send frame, %d",Gateway.tx_retry);
						}
						/* Retry max, drop the frame */
						else
						{
							/* Unable to send the frame, throw the frame from the tx FIFO */
							if( Gateway.frame_to_send != 0 )
							{
								Gateway.frame_to_send--;
								for( i=0; i<Gateway.frame_to_send; i++ )
								{
									Gateway.frames_buffer[i] = Gateway.frames_buffer[i+1];
								}
							}
							/* Reset retry counter */
							Gateway.tx_retry = 0;

							/* Update frame counter */
							if( Gateway.tx_frame_counter == 255 )
							{
								Gateway.tx_frame_counter = 0;
							}
							else
							{
								Gateway.tx_frame_counter ++;
							}
						}
						Gateway.ack_timeout 	= 0;
						Gateway.wait_for_ack 	= 0;
					}
				}
			}
		    /************************* END FRAME PROCESSING **************************/
		}while( ( Gateway.tx_retry != 0 ) || ( Gateway.ack_timeout != 0 ) );
    }
}
/******************************************************************************
   End of function  PLC_Gateway
******************************************************************************/


