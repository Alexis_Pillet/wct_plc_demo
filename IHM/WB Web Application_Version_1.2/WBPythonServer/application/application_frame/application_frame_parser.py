
"""
application.application_frame.application_frame_generator
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to parse the applicative frame

"""
from .application_frame_structure import Application
from collections import namedtuple, defaultdict
from enum import IntEnum


def get_address(frame):
    """Extract the address source of the frame

    Take into account if this is a IEEE address or Source ID address

    Args:
        frame: data of the frame

    Returns:
        A dict with the result
        "address": Address of the device
        "data": payload of the frame
        example:
        >>> {"address": '01020304', "data": {[0x00, 0x00]}}
    """
    result = {"address": None, "data": None}

    if (int(frame[0]) & Application.OptionMask.SOURCE_ADDRESS_TYPE) == Application.OptionAddressType.IEEE_ADDRESS:
        ieee_address = bytearray(frame[1:9])
        # Big endian
        ieee_address.reverse()
        result["address"] = ''.join('{:02x}'.format(x) for x in ieee_address)
        result["data"] = frame[9:]
    elif (int(frame[0]) & Application.OptionMask.SOURCE_ADDRESS_TYPE) == Application.OptionAddressType.SOURCE_ID_ADDRESS:
        source_id = bytearray(frame[1:5])
        # Big endian
        source_id.reverse()
        result["address"] = ''.join('{:02x}'.format(x) for x in source_id)
        result["data"] = frame[5:]

    return result


class Attribute:
    """Attribute class"""

    @staticmethod
    def parse():
        """ Association between Attribute type and function to parse it"""
        return {Attribute.Type.BOOLEAN: _parse_boolean}

    class Type(IntEnum):
        """Type of the attribute"""
        BOOLEAN = 0x10


def _parse_boolean(data):
    """Extract a boolean attribute value

    Args:
        data: data to parse

    Returns:
        A dict with the result
        "data": Value of the attribute
        "length": length of the value
        example:
        >>> {"data": 1, "length": 1}
    """
    return {"data": int(data[0]), "length": 1}


def get_attributes(data):
    """Extract the attributes of the specified data

    Args:
        data: data to parse

    Returns:
        A defaultdict of named tuple of the parsed attributes
        Named tuple
        >>> AttributeType = namedtuple('Attribute', ['attribute_id', 'attribute_type', 'attribute_value'])
        Key of the defaultdict will be the cluster_id
    """
    AttributeType = namedtuple('AttributeType', ['attribute_id', 'attribute_type', 'attribute_value'])
    result = defaultdict(list)

    success = 0x00
    # Start at the beginning of the cluster id
    index = 1
    cluster_id = bytearray(data[index:index+2])
    # Big endian
    cluster_id.reverse()
    cluster_id = cluster_id.hex()
    # Place index at the start of the attribute list
    index += 2
    while index < len(data):
        attribute_id = bytearray(data[index:index+2])
        # Big endian
        attribute_id.reverse()
        attribute_id = attribute_id.hex()
        index += 2
        if int(data[index]) != success:
            # Pass to the next attribute and start a new treatment
            index += 1
            continue
        # Pass to the next byte and parse the current attribute
        index += 1
        attribute_type = data[index]
        # Parse attribute value
        index += 1
        # Get the function to parse the current attribute of type attribute_type
        value = Attribute.parse()[attribute_type](data[index:])
        attribute = AttributeType(attribute_id, attribute_type, value["data"])
        result[cluster_id].append(attribute)
        # Pass to the next attribute
        index += value["length"]

    return result












