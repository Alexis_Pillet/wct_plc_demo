
"""
json.json_parser
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to parse ZigBeeDevice object and store them in a json file
This json file is used by the Web Application to display the topology of the network

"""
import json
import copy
from datetime import datetime


class JsonParser:
    """JSON parser to create a json file representing the topology of the network"""

    def __init__(self, filename):
        """Initialization of the name of the file"""
        self._filename = filename

    def update_device(self, device_instance):
        """Update the json file with the specified device

        Create the entry if not present otherwise update it with new data

        Args:
            device_instance: dict of the value received
        """
        with open(self._filename, 'r') as data_file:

            json_data = json.load(data_file)

            # Looking for the Wireless Bridge Data
            json_id_wireless_bridge = "WB_" + str(device_instance.id_wireless_bridge + 1)
            wireless_bridge_data_found = False
            list_device = None
            data_wireless_bridge = None
            number_device = 0
            for wireless_bridge in json_data:
                if wireless_bridge["id"] == json_id_wireless_bridge:
                    wireless_bridge_data_found = True
                    list_device = wireless_bridge["children"]
                    data_wireless_bridge = wireless_bridge["data"]
                    number_device = wireless_bridge["data"]["number_device"]

            if not wireless_bridge_data_found:

                json_data.append({"text": "Wireless bridge " + str(device_instance.id_wireless_bridge + 1),
                                  "id": json_id_wireless_bridge,
                                  "data": {"date": "",
                                           "device_number": device_instance.id_wireless_bridge,
                                           "number_device": 0},

                                  "children": []
                                  })
                # Get the list of device of the created wireless bridge
                list_device = json_data[-1]["children"]
                data_wireless_bridge = json_data[-1]["data"]
                number_device = 0

            # Looking for the device
            json_id_device = json_id_wireless_bridge + "_DEVICE_" + device_instance.address
            device_data_found = False
            for device in list_device:
                if device["id"] == json_id_device:
                    device_data_found = True
                    device["data"] = device_instance.get_json_data()

            if not device_data_found:

                json_device = device_instance.get_json_device(number_device + 1)
                list_device.append(json_device)
                data_wireless_bridge["number_device"] = number_device + 1

            # Change the date of the fast update
            json_data[0]["data"]["date"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        with open(self._filename, 'w') as data_file:
            json.dump(json_data, data_file)
