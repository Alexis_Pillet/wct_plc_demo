#ifndef _SZL_CONFIG_H_
#define _SZL_CONFIG_H_

/**
 * SZL_DEBUG
 *
 * This is used to get some debug information from the library
 *
 * NOTE if enabled the App should provide a callback for the SzlTrace function
 *
 * Default: this is disabled
 */
#ifndef SZL_DEBUG
  //#define SZL_DEBUG
#endif


/**
 * SZL_CFG_ENDPOINT_CNT
 *
 * This is the number of endpoints for the device
 *
 * Default value: 6
 */
#ifndef SZL_CFG_ENDPOINT_CNT
  #define SZL_CFG_ENDPOINT_CNT                            ( 6 )
#endif


/**
 * SZL_CFG_MAX_PLUGINS
 *
 * This is the size of the internal table where Plugins are registered
 *
 * Default value: 10
 */
#ifndef SZL_CFG_MAX_PLUGINS
  #define SZL_CFG_MAX_PLUGINS                           ( 10 )
#endif


/**
 * SZL_CFG_MAX_CLIENT_CLUSTERS
 *
 * This is the max size of the internal table holding the client clusters
 *
 * Default value: 10
 */
#ifndef SZL_CFG_MAX_CLIENT_CLUSTERS
  #define SZL_CFG_MAX_CLIENT_CLUSTERS                   ( 10 )
#endif


/**
 * SZL_CFG_MAX_DATAPOINTS
 *
 * This is the size of the internal table holding the app(s) data points
 *
 * Default value: 20
 */
#ifndef SZL_CFG_MAX_DATAPOINTS
  #define SZL_CFG_MAX_DATAPOINTS                          ( 250 )
#endif

/**
 * SZL_CFG_MAX_REPORTS
 *
 * This is the size of the internal table holding the report configuration
 *
 * Default value: 10
 */
#ifndef SZL_CFG_MAX_REPORTS
  #define SZL_CFG_MAX_REPORTS                             ( 250 )
#endif

/**
 * SZL_CFG_MAX_REPORTABLE_ATTR_LENGTH
 *
 * This is the maximum data length of a reportable attribute.
 * If you support strings you will probably need to increase this.
 * It is only put on the stack a short time, so no major or static impact.
 * It DOES NOT control the maximum size of a attribute that can perform ReportableChange.
 *
 * Default value: 8
 */
#ifndef SZL_CFG_MAX_REPORTABLE_ATTR_LENGTH
  #define SZL_CFG_MAX_REPORTABLE_ATTR_LENGTH              ( 8 )
#endif

/**
 * SZL_CFG_MAX_CLUSTER_CMDS
 *
 * This is the size of the internal table holding the app(s) cluster commands
 *
 * Default value: 10
 */
#ifndef SZL_CFG_MAX_CLUSTER_CMDS
  #define SZL_CFG_MAX_CLUSTER_CMDS                        ( 25 )
#endif

/**
 * SZL_CFG_CB_HANDLER_SIZE
 *
 * This is the size of the internal table holding the registered CB handlers
 *
 * Default value: 10
 */
#ifndef SZL_CFG_CB_HANDLER_SIZE
  #define SZL_CFG_CB_HANDLER_SIZE                         ( 20 )
#endif

/**
 * SZL_CFG_EXTERNAL_TIMER_COUNT
 *
 * This is the size of the external timer table
 * The external timers are used by the plugins
 *
 * Default value: 10
 */
#ifndef SZL_CFG_EXTERNAL_TIMER_COUNT
    #define SZL_CFG_EXTERNAL_TIMER_COUNT                  ( 10 )
#endif




/**
 * SZL_VLA_INIT
 *
 * This is the size at which variable length arrays are initialised.
 * Required as C++ compilers do not like arrays of size 0.
 * Leave it as 0 unless you get compile errors/warnings, in which case set it to 1.
 *
 * Default value: 0
 */
#ifndef SZL_VLA_INIT
    #define SZL_VLA_INIT                                  ( VLA_INIT )
#endif

/**
 * SZL_CFG_PROFILE_ID
 *
 * ZigBee Profile ID
 *
 * Default value: 0x0104 (Home Automation)
 */
#ifndef SZL_CFG_PROFILE_ID
    #define SZL_CFG_PROFILE_ID                            ( 0x0104 )
#endif

/**
 * Szl_GetDeviceID
 *
 * ZigBee Device ID per Endpoint
 * This macro may return a constant value, or map to a function to return
 * different values based on the endpoint parameter.
 *
 * Parameters:
 *  - ep: szl_uint8 endpoint number
 *
 * Return:
 *  - Device ID
 *   - Format: szl_uint16
 *   - Default value: 0x0007 (ZB_HA_DEVICEID_COMBINED_INTERFACE)
 */
#include "appConfig.h"
#define Szl_GetDeviceID( ep )                             ( appConfig_GetDeviceId( ep ) )

/**
 * SZL_CFG_DEVICE_VERSION
 *
 * ZigBee Device Version
 *
 * Default value: 0 for all endpoint in Home Automation
 */
#ifndef SZL_CFG_DEVICE_VERSION
    #define SZL_CFG_DEVICE_VERSION                        ( 0 )
#endif

/**
 * SZL_CFG_MAX_CLUSTERS
 *
 * This is the maximum number of cluster per endpoint
 *
 * Default value: 16
 */
#ifndef SZL_CFG_MAX_CLUSTERS
    #define SZL_CFG_MAX_CLUSTERS                          ( 16 )
#endif

/**
 * SZL_CFG_ENABLE_GREEN_POWER
 *
 * Enable Green Power in the SZL
 *
 * Default value: Defined
 */
#define SZL_CFG_ENABLE_GREEN_POWER

/******************************************************************************
 * ZAB implementation specific configuration
 ******************************************************************************/
/* Maximum number of synchronous transaction on the air at any time */
#define SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS            ( 32 )

/* Timeout for a response to a synchronous transaction in seconds */
#define SZL_ZAB_M_SYNCHRONOUS_TRANSACTION_TIMEOUT_S       ( 50 ) //( 10 )

/* Timeout for a Green Power response to a synchronous transaction in seconds */
#define SZL_ZAB_M_GP_SYNCHRONOUS_TRANSACTION_TIMEOUT_S    ( 65 )

/* GP device can split responses to commands such as Read Attributes across multiple frames.
 * To handle this, after each frame, if we have not received all expected data we set the transaction timeout
 * to this value to allow later frames to build up the response.
 * If later frames are not received within this time a timeout will be generated with the partial response.
 * Settings:
 *  Minimum = 2 (1 second could expire very quickly depending on where we are within the second)
 *  Default = 5 */
#define SZL_ZAB_M_GP_MULTI_FRAME_RESPONSE_DELAY_S         ( 5 )


#endif /*_SZL_CONFIG_H_*/
