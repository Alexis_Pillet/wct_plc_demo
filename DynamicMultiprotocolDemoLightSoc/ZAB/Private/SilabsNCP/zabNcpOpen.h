/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the SiLabs NCP Open State Machine - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 * 002.002.041  18-Jan-17   MvdB   ARTF199128: Fix NCP networking after reception of a leave command
 * 002.002.043  20-Jan-17   MvdB   ARTF190099: Make ping time run time controllable for ZNP & NCP. Private APIs only at this time, for industrial tester.
 *****************************************************************************/
#ifndef __ZAB_NCP_OPEN_H__
#define __ZAB_NCP_OPEN_H__

#include "zabNcpOpenTypes.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Create
 * Initialises open state / state machine
 ******************************************************************************/
void zabNcpOpen_Create(erStatus* Status, zabService* Service);

/******************************************************************************
 * Get Open State
 ******************************************************************************/
zabOpenState zabNcpOpen_GetOpenState(zabService* Service);

/******************************************************************************
 * Update timeout
 * Check for timeouts within the open state machine
 ******************************************************************************/
unsigned32 zabNcpOpen_UpdateTimeout(erStatus* Status, zabService* Service, unsigned32 Time);

/******************************************************************************
 * Init network processor ping service time
 ******************************************************************************/
void zabNcpOpen_InitNetworkProcessorPing(erStatus* Status, zabService* Service, unsigned16 PingTimeMs);

/******************************************************************************
 * Run network processor ping service
 ******************************************************************************/
unsigned32 zabNcpOpen_UpdateNetworkProcessorPing(erStatus* Status, zabService* Service, unsigned32 Time);

/******************************************************************************
 * Action
 * Handles Open and Close actions for the open state machines
 ******************************************************************************/
void zabNcpOpen_Action(erStatus* Status, zabService* Service, unsigned8 Action);

/******************************************************************************
 * In Notification Handler
 * Handles notification from the serial glue for the open state machine
 ******************************************************************************/
void zabNcpOpen_InNotificationHandler(erStatus* Status, zabService* Service, zabOpenState OpenState);

/******************************************************************************
 * Reopen Request
 * This allows other services to request re-opening of the connection.
 * For example, after a network leave is performed.
 ******************************************************************************/
void zabNcpOpen_ReopenRequest(erStatus* Status, zabService* Service);

/******************************************************************************
 * Various Response Handlers - Self explanatory!
 ******************************************************************************/
void zabNcpOpen_ResetIndicationHandler(erStatus* Status, zabService* Service, unsigned8 ReasonForReset);
void zabNcpOpen_VersionHandler(erStatus* Status, zabService* Service, unsigned16 StackVersion);
void zabNcpOpen_GetIeeeAddressResponseHandler(erStatus* Status, zabService* Service, unsigned64 IeeeAddress);
void zabNcpOpen_LaunchStandaloneBootloaderResponseHandler(erStatus* Status, zabService* Service, unsigned8 BootloaderStatus);
void zabNcpOpen_SblPromptReceivedHandler(erStatus* Status, zabService* Service);

#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/