/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the types for the networking state machine for the ZAB Pro ZNP
 *   vendor.
 *   For details and flow charts see XXX.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 02.00.00.01  31-May-13   MvdB   Original
 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 00.00.06.02  30-Sep-14   MvdB   artf55512: Support network key change action
 * 002.000.003  06-Mar-15   MvdB   ARTF116061: Support run time configuration of Green Power endpoint.
 * 002.000.004  01-Apr-15   MvdB   ARTF56504: Support missed channel change recovery, with actions/notifications/asks/gives to get/set parameters.
 * 002.002.005  11-Sep-15   MvdB   ARTF112436: Add getNetworkInfoActive
 * 002.002.012  14-Oct-15   MvdB   ARTF104107: Support End Device operation
 * 002.002.020  18-Mar-16   MvdB   ARTF165341: Delay Networked ntf during join until key received (DEV_ROUTER)
 *****************************************************************************/
#ifndef __ZAB_ZNP_NWK_TYPES_H__
#define __ZAB_ZNP_NWK_TYPES_H__

#include "osTypes.h"
#include "zabTypes.h"
#include "zabZnpZdoFrame.h"


/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Value of Network Address when ZNP has no network */
#define ZAB_ZNP_NWK_M_NWK_ADDR_NO_PREVIOUS_SETTINGS 0xFFFE


/* Supported channel range */
#define ZAB_ZNP_NWK_M_CHANNEL_MIN 11
#define ZAB_ZNP_NWK_M_CHANNEL_MAX 26


/* Current Item of the state machine */
typedef enum
{
  ZAB_NWK_CURRENT_ITEM_NONE,
  ZAB_NWK_CURRENT_ITEM_GET_SHORT_ADDR,
  ZAB_NWK_CURRENT_ITEM_CONFIRM_SHORT_ADDR_INVALID,
  ZAB_NWK_CURRENT_ITEM_NWK_SETTING_TX_POWER_BEFORE_RESUME,
  ZAB_NWK_CURRENT_ITEM_NWK_RESUME_STARTUP,
  ZAB_NWK_CURRENT_ITEM_NWK_RESUME_SET_LOOPBACK,
  ZAB_NWK_CURRENT_ITEM_NWK_DISCOVER,
  ZAB_NWK_CURRENT_ITEM_NWK_DISCOVER_IN_PROGRESS,
  ZAB_NWK_CURRENT_ITEM_NWK_STEER_SETUP_ZDO_CB,
  ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_LT,
  ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_CH_MASK,
  ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_SECURITY,
  ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_PAN_ID,
  ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_EPID,
  ZAB_NWK_CURRENT_ITEM_NWK_FORM_STARTUP,
  ZAB_NWK_CURRENT_ITEM_NWK_FORM_IN_PROGRESS,
  ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_LOOPBACK,
  ZAB_NWK_CURRENT_ITEM_NWK_JOIN_REQ,
  ZAB_NWK_CURRENT_ITEM_NWK_JOIN_IN_PROGRESS,
  ZAB_NWK_CURRENT_ITEM_NWK_JOIN_AWAITING_AUTH,
  ZAB_NWK_CURRENT_ITEM_NWK_JOIN_SET_LOOPBACK,
  ZAB_NWK_CURRENT_ITEM_NWK_CLEAR,
  ZAB_NWK_CURRENT_ITEM_NWK_RESTART,
  ZAB_NWK_CURRENT_ITEM_GET_CFG_SHORT_ADDR,
  ZAB_NWK_CURRENT_ITEM_GET_CFG_CHANNEL,
  ZAB_NWK_CURRENT_ITEM_GET_CFG_PAN,
  ZAB_NWK_CURRENT_ITEM_GET_CFG_EPID,
  ZAB_NWK_CURRENT_ITEM_GET_CFG_DEV_TYPE,
  ZAB_NWK_CURRENT_ITEM_GET_CFG_DEV_STATE,
  ZAB_NWK_CURRENT_ITEM_GET_CFG_PARENT_SHORT_ADDR,
  ZAB_NWK_CURRENT_ITEM_GET_CFG_PARENT_IEEE_ADDR,
          
  ZAB_NWK_CURRENT_ITEM_LEAVING,
  
          
  ZAB_NWK_CURRENT_ITEM_ENABLE_PERMIT_JOIN,
  ZAB_NWK_CURRENT_ITEM_ENABLE_PERMIT_JOIN_GP_NEXT,
  ZAB_NWK_CURRENT_ITEM_ENABLE_PERMIT_JOIN_UPDATE,
  ZAB_NWK_CURRENT_ITEM_DISABLE_PERMIT_JOIN,   
  ZAB_NWK_CURRENT_ITEM_DISABLE_PERMIT_JOIN_GP_NEXT,     

    
  ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE,
    
  /* Network Key Management States */
  ZAB_NWK_CURRENT_ITEM_NWK_KEY_SEQ_NUM_REQ,
  ZAB_NWK_CURRENT_ITEM_NWK_KEY_UPDATE_REQ,
  ZAB_NWK_CURRENT_ITEM_NWK_KEY_SWITCH_DELAY,
  ZAB_NWK_CURRENT_ITEM_NWK_KEY_SWITCH_REQ,
    
    
  /* Network Maintenance Parameter States */
  ZAB_NWK_CURRENT_ITEM_NWK_MGMT_GET,
  ZAB_NWK_CURRENT_ITEM_NWK_MGMT_SET,
    
  
  /* End Device Join States */
  ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_SET_LT,
  ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_SET_CH_MASK,
  ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_SET_EPID,
  ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_STARTUP,
  ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_IN_PROGRESS,
  ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_FAILED_RESET,
} zabNwkCurrentItem;

/* State information for this state machine */
typedef struct
{
  zabNwkState nwkState;                       /* Current public state */
  zabNwkCurrentItem currentItem;              /* Current internal state - PRIVATE*/
  unsigned32 timeoutExpiryMs;                 /* Timeout for confirms - PRIVATE */
  unsigned16 nwkAddr;                         /* Nwk Address of the ZNP - PRIVATE */
  unsigned64 ieeeAddr;                        /* IEEE Address of the ZNP - PRIVATE */
  unsigned16 permitJoinTimeRemaining;         /* Remaining Permit Join time in seconds - PRIVATE */
  unsigned8 currentPermitJoinTimeRemaining;   /* Remaining time of the current Permit Join req in seconds - PRIVATE */
  unsigned8 nwkKeySeqNum;                     /* Key sequence number during a network key change */
  unsigned8 retries;                          /* Number of times something has been retried */
  unsigned8 gpLoopbackEndpoint;               /* Green Power Loopback Endpoint. Valid Range = 1 - 0xEF */
  unsigned8 getNetworkInfoActive;             /* Get network info in progress */
  unsigned8 channel;                          /* Operating Channel */
  ZDO_STATE znpState;                         /* State of the ZNP */
} zabNwkInfo_t;



#endif /* __ZAB_ZNP_NWK_TYPES_H__ */
