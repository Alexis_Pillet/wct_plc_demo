/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Networking Commands/Responses
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.005  xx-Jun-16   MvdB   Support energy scan for quietest channel during form
 * 000.000.013  21-Jul-16   MvdB   Support Network Discover and Join
 * 000.000.014  22-Jul-16   MvdB   Support ZAB_ACTION_SET_TX_POWER
 * 000.000.019  26-Jul-16   MvdB   Replace memcpy with osMemCopy, memset with osMemSet
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 * 000.000.023  02-Aug-16   MvdB   Support EZSP_SET_MANUFACTURER_CODE
 *****************************************************************************/

#include "zabNcpService.h"
#include "zabNcpEzsp.h"
#include "zabNcpNwk.h"
#include "zabNcpEzspNetworking.h"

#include "ezsp-enum.h"
#include "ezsp-enum-decode.h"
#include "ezsp-protocol.h"
#include "ember-types.h"


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* Size of serialised EmberNetworkParameters */
#define EMBER_NETWORK_PARAMS_SERIALISED_SIZE (8 + 2 + 1 + 1 + 1 + 2 + 1 + 4)

/* Size of serialised EmberZigbeeNetwork */
#define EMBER_ZIGBEE_NETWORK_SIZE ( 14 )

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Network Init Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_NetworkInit(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_NETWORK_INIT};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspNetworking_NetworkInitResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_NETWORK_INIT: Status: %s (0x%02X)\n",
                  decodeEmberStatus(PayloadData[0]),
                  PayloadData[0]);

      zabNcpNwk_NetworkInitResponseHandler(Status, Service, PayloadData[0]);
    }
}


/******************************************************************************
 * Energy or Active Scan: Request, Response, Callbacks and Confirm
 ******************************************************************************/
void zabNcpEzspNetworking_StartScan(erStatus* Status, zabService* Service, EzspNetworkScanType ScanType, unsigned32 ChannelMask, unsigned8 Duration)
{
  unsigned8 index;
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH + 1 + 4 + 1] = {0/*seq*/, 0/*FrameControl*/, EZSP_START_SCAN};

  index = EZSP_PARAMETERS_INDEX;
  cmd[index++] = (unsigned8)ScanType;
  COPY_OUT_32_BITS(&cmd[index], ChannelMask); index+=4;
  cmd[index++] = Duration;

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspNetworking_StartScanResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_START_SCAN: Status: %s (0x%02X)\n",
                  decodeEmberStatus(PayloadData[0]),
                  PayloadData[0]);

      zabNcpNwk_StartScanResponseHandler(Status, Service, PayloadData[0]);
    }
}
void zabNcpEzspNetworking_EnergyScanResultHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 index;
  unsigned8 channel;
  signed8 maxRssiValue;

  if (PayloadLength >= 2)
    {
      index = 0;
      channel = PayloadData[index++];
      maxRssiValue = (signed8)PayloadData[index++];

      printVendor(Service, "EZSP_ENERGY_SCAN_RESULT_HANDLER: Channel 0x%02X, Rssi %d\n",
                  channel, maxRssiValue);

      zabNcpNwk_EnergyScanResultHandler(Status, Service, channel, maxRssiValue);
    }
}
void zabNcpEzspNetworking_NetworkFoundResultHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 index;
  EmberZigbeeNetwork network;
  unsigned8 lastHopLqi;
  signed8 lastHopRssi;

  if (PayloadLength >= (EMBER_ZIGBEE_NETWORK_SIZE + 2))
    {
      index = 0;

      /* WARNING: These are arranged differently in EZSP than in the struct... */
      network.channel = PayloadData[index++];
      network.panId = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
      osMemCopy(Status, network.extendedPanId, &PayloadData[index], 8); index+=8;
      network.allowingJoin = PayloadData[index++];
      network.stackProfile = PayloadData[index++];
      network.nwkUpdateId = PayloadData[index++];

      lastHopLqi = PayloadData[index++];
      lastHopRssi = (signed8)PayloadData[index++];

      printVendor(Service, "EZSP_NETWORK_FOUND_HANDLER: Channel 0x%02X, PAN 0x%04X, PJ=%d\n",
                  network.channel, network.panId, network.allowingJoin);

      zabNcpNwk_NetworkFoundResultHandler(Status, Service, network, lastHopLqi, lastHopRssi);
    }
}
void zabNcpEzspNetworking_ScanCompleteHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 index;
  unsigned8 channel;
  unsigned8 ezspStatus;

  if (PayloadLength >= 2)
    {
      index = 0;
      channel = PayloadData[index++];
      ezspStatus = PayloadData[index++];

      printVendor(Service, "EZSP_SCAN_COMPLETE_HANDLER: Channel 0x%02X, Status 0x%02X\n",
                  channel, ezspStatus);

      zabNcpNwk_ScanCompleteHandler(Status, Service, ezspStatus);
    }
}

/******************************************************************************
 * Network Form Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_NetworkForm(erStatus* Status, zabService* Service, EmberNetworkParameters* NetworkParameters)
{
  unsigned8 index;
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH + EMBER_NETWORK_PARAMS_SERIALISED_SIZE] = {0/*seq*/, 0/*FrameControl*/, EZSP_FORM_NETWORK};

  index = EZSP_PARAMETERS_INDEX;
  osMemCopy(Status, &cmd[index], NetworkParameters->extendedPanId, 8); index+=8;
  COPY_OUT_16_BITS(&cmd[index], NetworkParameters->panId); index+=2;
  cmd[index++] = NetworkParameters->radioTxPower;
  cmd[index++] = NetworkParameters->radioChannel;
  cmd[index++] = NetworkParameters->joinMethod;
  COPY_OUT_16_BITS(&cmd[index], NetworkParameters->nwkManagerId); index+=2;
  cmd[index++] = NetworkParameters->nwkUpdateId;
  COPY_OUT_32_BITS(&cmd[index], NetworkParameters->channels); index+=4;
  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspNetworking_NetworkFormResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_FORM_NETWORK: Status: %s (0x%02X)\n",
                  decodeEmberStatus(PayloadData[0]),
                  PayloadData[0]);

      zabNcpNwk_NetworkFormResponseHandler(Status, Service, PayloadData[0]);
    }
}

/******************************************************************************
 * Network Join Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_NetworkJoin(erStatus* Status, zabService* Service, EmberNodeType NodeType, EmberNetworkParameters* NetworkParameters)
{
  unsigned8 index;
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH + 1 + EMBER_NETWORK_PARAMS_SERIALISED_SIZE] = {0/*seq*/, 0/*FrameControl*/, EZSP_JOIN_NETWORK};

  index = EZSP_PARAMETERS_INDEX;
  cmd[index++] = NodeType;
  osMemCopy(Status, &cmd[index], NetworkParameters->extendedPanId, 8); index+=8;
  COPY_OUT_16_BITS(&cmd[index], NetworkParameters->panId); index+=2;
  cmd[index++] = NetworkParameters->radioTxPower;
  cmd[index++] = NetworkParameters->radioChannel;
  cmd[index++] = NetworkParameters->joinMethod;
  COPY_OUT_16_BITS(&cmd[index], NetworkParameters->nwkManagerId); index+=2;
  cmd[index++] = NetworkParameters->nwkUpdateId;
  COPY_OUT_32_BITS(&cmd[index], NetworkParameters->channels); index+=4;
  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspNetworking_NetworkJoinResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_JOIN_NETWORK: Status: %s (0x%02X)\n",
                  decodeEmberStatus(PayloadData[0]),
                  PayloadData[0]);

      zabNcpNwk_NetworkJoinResponseHandler(Status, Service, PayloadData[0]);
    }
}

/******************************************************************************
 * Stack Status Handler
 ******************************************************************************/
void zabNcpEzspNetworking_StackStatusHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_STACK_STATUS_HANDLER: Status: %s (0x%02X)\n",
                  decodeEmberStatus(PayloadData[0]),
                  PayloadData[0]);

      zabNcpNwk_StackStatusHandler(Status, Service, PayloadData[0]);
    }
}

/******************************************************************************
 * Get Network Parameters Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_GetNetworkParameters(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_GET_NETWORK_PARAMETERS};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspNetworking_GetNetworkParametersResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 index;
  EmberNetworkParameters nwkParams;

  if (PayloadLength >= (2 + EMBER_NETWORK_PARAMS_SERIALISED_SIZE))
    {
      printVendor(Service, "EZSP_GET_NETWORK_PARAMETERS: Status: %s (0x%02X), NodeType: %d\n",
                  decodeEmberStatus(PayloadData[0]),
                  PayloadData[0],
                  PayloadData[1]);

      index = 2;
      osMemCopy(Status, nwkParams.extendedPanId, &PayloadData[index], 8); index += 8;
      nwkParams.panId = COPY_IN_16_BITS(&PayloadData[index]); index += 2;
      nwkParams.radioTxPower = PayloadData[index++];
      nwkParams.radioChannel = PayloadData[index++];
      nwkParams.joinMethod = PayloadData[index++];
      nwkParams.nwkManagerId = COPY_IN_16_BITS(&PayloadData[index]); index += 2;
      nwkParams.nwkUpdateId = PayloadData[index++];
      nwkParams.channels = COPY_IN_32_BITS(&PayloadData[index]); index += 4;
      zabNcpNwk_GetNetworkParametersResponseHandler(Status, Service, PayloadData[0], PayloadData[1], &nwkParams);
    }
}

/******************************************************************************
 * Set Radio Power Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_SetRadioPower(erStatus* Status, zabService* Service, signed8 TxPower)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_SET_RADIO_POWER, TxPower};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspNetworking_SetRadioPowerResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_SET_RADIO_POWER: Status: %s (0x%02X)\n",
                  decodeEmberStatus(PayloadData[0]),
                  PayloadData[0]);

      zabNcpNwk_SetTxPowerResponseHandler(Status, Service, PayloadData[0]);
    }
}

/******************************************************************************
 * Local Permit Join Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_PermitJoin(erStatus* Status, zabService* Service, unsigned8 PermitJoinTime)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_PERMIT_JOINING, PermitJoinTime};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspNetworking_PermitJoinResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_PERMIT_JOINING: Status: %s (0x%02X)\n",
                  decodeEmberStatus(PayloadData[0]),
                  PayloadData[0]);

      zabNcpNwk_PermitJoinResponseHandler(Status, Service, PayloadData[0]);
    }
}

/******************************************************************************
 * Local Leave Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_Leave(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_LEAVE_NETWORK};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspNetworking_LeaveResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_LEAVE_NETWORK: Status: %s (0x%02X)\n",
                  decodeEmberStatus(PayloadData[0]),
                  PayloadData[0]);

      zabNcpNwk_LeaveResponseHandler(Status, Service, PayloadData[0]);
    }
}

/******************************************************************************
 * Set Manufacturer Code Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_SetManufacturerCode(erStatus* Status, zabService* Service, unsigned16 ManufacturerCode)
{
  unsigned8 index;
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH + sizeof(unsigned16)] = {0/*seq*/, 0/*FrameControl*/, EZSP_SET_MANUFACTURER_CODE};

  index = EZSP_PARAMETERS_INDEX;
  COPY_OUT_16_BITS(&cmd[index], ManufacturerCode); index+=2;

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspNetworking_SetManufacturerCodeResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  printVendor(Service, "EZSP_SET_MANUFACTURER_CODE: Rsp received\n");

  zabNcpNwk_SetManufacturerCodeResponseHandler(Status, Service);
}
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/