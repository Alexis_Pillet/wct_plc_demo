
"""
data_model.zigbee_device_factory
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to create ZigBee Device object (abstract factory)

"""
from abc import ABC, abstractmethod
from enum import Enum
from data_model.zigbee_pro.iact.iact import IACT
from data_model.zigbee_green_power.co2_multisensor.co2_multisensor import CO2MultiSensor
from data_model.zigbee_green_power.power_tag.power_tag_1p import PowerTag1P
from data_model.zigbee_green_power.power_tag.power_tag_3p import PowerTag3P
from data_model.zigbee_green_power.power_tag.power_tag_3pn import PowerTag3PN


class ProductIdentifier(Enum):
    """Enumeration of the product Identifier"""
    IACT = '4524'
    IATL = '4525'
    CO2_MULTISENSOR = '4399'
    POWER_TAG_1P = '4330'
    POWER_TAG_1P_UP = '4331'
    POWER_TAG_1P_DOWN = '4332'
    POWER_TAG_3P = '4333'
    POWER_TAG_3PN_UP = '4334'
    POWER_TAG_3PN_DOWN = '4335'


class ZigBeeDeviceFactory(ABC):
    """Abstract Class - ZigBee Device Factory"""

    def __init__(self):
        """Initialization"""
        super(ZigBeeDeviceFactory, self).__init__()

    @staticmethod
    def get_factory(product_identifier):
        """Get the good factory to create the specified devices

        Args:
            product_identifier: product identifier to find the good factory
        """
        return factory_storage[product_identifier]

    @abstractmethod
    def create(self, address, id_wireless_bridge, application_frame_generator):
        """Create the device with the specified parameters

        Args:
            address: address of the product
            id_wireless_bridge: id of the wireless bridge
            application_frame_generator: application frame generator instance(Dependency Injections)
        """
        pass


class IACTFactory(ZigBeeDeviceFactory):
    def __init__(self):
        super(IACTFactory, self).__init__()

    def create(self, address, id_wireless_bridge, application_frame_generator):
        return IACT(address, id_wireless_bridge, application_frame_generator)


class CO2MultiSensorFactory(ZigBeeDeviceFactory):
    def __init__(self):
        super(CO2MultiSensorFactory, self).__init__()

    def create(self, address, id_wireless_bridge, application_frame_generator):
        return CO2MultiSensor(address, id_wireless_bridge, application_frame_generator)


class PowerTag1PFactory(ZigBeeDeviceFactory):
    def __init__(self):
        super(PowerTag1PFactory, self).__init__()

    def create(self, address, id_wireless_bridge, application_frame_generator):
        return PowerTag1P(address, id_wireless_bridge, application_frame_generator)


class PowerTag3PFactory(ZigBeeDeviceFactory):
    def __init__(self):
        super(PowerTag3PFactory, self).__init__()

    def create(self, address, id_wireless_bridge, application_frame_generator):
        return PowerTag3P(address, id_wireless_bridge, application_frame_generator)


class PowerTag3PNFactory(ZigBeeDeviceFactory):
    def __init__(self):
        super(PowerTag3PNFactory, self).__init__()

    def create(self, address, id_wireless_bridge, application_frame_generator):
        return PowerTag3PN(address, id_wireless_bridge, application_frame_generator)


factory_storage = {  # Association between ZigBeeDevice Factory and Model_ID
            ProductIdentifier.IACT.value: IACTFactory(),
            ProductIdentifier.IATL.value: IACTFactory(),
            ProductIdentifier.CO2_MULTISENSOR.value: CO2MultiSensorFactory(),
            ProductIdentifier.POWER_TAG_1P.value: PowerTag1PFactory(),
            ProductIdentifier.POWER_TAG_1P_UP.value: PowerTag1PFactory(),
            ProductIdentifier.POWER_TAG_1P_DOWN.value: PowerTag1PFactory(),
            ProductIdentifier.POWER_TAG_3P.value: PowerTag3PFactory(),
            ProductIdentifier.POWER_TAG_3PN_UP.value: PowerTag3PNFactory(),
            ProductIdentifier.POWER_TAG_3PN_DOWN.value: PowerTag3PNFactory()}



