/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Manufacturing Library Commands/Responses
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.012  19-Jul-16   MvdB   Original
 * 000.000.014  22-Jul-16   MvdB   Add zabNcpEzspMfglib_GetPower for test
 * 000.000.019  26-Jul-16   MvdB   Replace memcpy with osMemCopy, memset with osMemSet
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 * 000.000.023  02-Aug-16   MvdB   Support getting of TxPower by nwk state machine
 *****************************************************************************/

#include "zabNcpPrivate.h"
#include "zabNcpService.h"
#include "zabNcpEzsp.h"
#include "zabNcpRft.h"
#include "zabNcpNwk.h"

#include "ember-types.h"
#include "ezsp-enum-decode.h"

#include "zabNcpEzspMfglib.h"

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Start Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_Start(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_MFGLIB_START, zab_true};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspMfglib_StartResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus responseStatus;
  if (PayloadLength >= 1)
    {
      responseStatus = PayloadData[0];
      printVendor(Service, "EZSP_MFGLIB_START: Status: %s (0x%02X)\n",
                  decodeEmberStatus(responseStatus),
                  responseStatus);

      zabNcpRft_GeneralResponseHandler(Status, Service, responseStatus);
    }
}

/******************************************************************************
 * End Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_End(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_MFGLIB_END};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspMfglib_EndResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus responseStatus;
  if (PayloadLength >= 1)
    {
      responseStatus = PayloadData[0];
      printVendor(Service, "EZSP_MFGLIB_END: Status: %s (0x%02X)\n",
                  decodeEmberStatus(responseStatus),
                  responseStatus);

      zabNcpRft_GeneralResponseHandler(Status, Service, responseStatus);
    }
}

/******************************************************************************
 * Start Tone Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_StartTone(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_MFGLIB_START_TONE};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspMfglib_StartToneResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus responseStatus;
  if (PayloadLength >= 1)
    {
      responseStatus = PayloadData[0];
      printVendor(Service, "EZSP_MFGLIB_START_TONE: Status: %s (0x%02X)\n",
                  decodeEmberStatus(responseStatus),
                  responseStatus);

      zabNcpRft_GeneralResponseHandler(Status, Service, responseStatus);
    }
}

/******************************************************************************
 * Stop Tone Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_StopTone(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_MFGLIB_STOP_TONE};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspMfglib_StopToneResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus responseStatus;
  if (PayloadLength >= 1)
    {
      responseStatus = PayloadData[0];
      printVendor(Service, "EZSP_MFGLIB_STOP_TONE: Status: %s (0x%02X)\n",
                  decodeEmberStatus(responseStatus),
                  responseStatus);

      zabNcpRft_GeneralResponseHandler(Status, Service, responseStatus);
    }
}

/******************************************************************************
 * Start Stream Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_StartStream(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_MFGLIB_START_STREAM};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspMfglib_StartStreamResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus responseStatus;
  if (PayloadLength >= 1)
    {
      responseStatus = PayloadData[0];
      printVendor(Service, "EZSP_MFGLIB_START_STREAM: Status: %s (0x%02X)\n",
                  decodeEmberStatus(responseStatus),
                  responseStatus);

      zabNcpRft_GeneralResponseHandler(Status, Service, responseStatus);
    }
}

/******************************************************************************
 * Stop Stream Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_StopStream(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_MFGLIB_STOP_STREAM};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspMfglib_StopStreamResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus responseStatus;
  if (PayloadLength >= 1)
    {
      responseStatus = PayloadData[0];
      printVendor(Service, "EZSP_MFGLIB_STOP_STREAM: Status: %s (0x%02X)\n",
                  decodeEmberStatus(responseStatus),
                  responseStatus);

      zabNcpRft_GeneralResponseHandler(Status, Service, responseStatus);
    }
}

/******************************************************************************
 * Send Packet Request + Response Handler
 ******************************************************************************/
#define MFGLIB_MAX_PACKET_LENGTH  ( 64 )
#define MFGLIB_PACKET_CRC_LENGTH  ( 2 )
void zabNcpEzspMfglib_SendPacket(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH + 1 + MFGLIB_MAX_PACKET_LENGTH + MFGLIB_PACKET_CRC_LENGTH] = {0/*seq*/, 0/*FrameControl*/, EZSP_MFGLIB_SEND_PACKET};
  unsigned8 index;
  ER_CHECK_STATUS(Status);

  index = EZSP_PARAMETERS_INDEX;
  if ( PayloadLength <= MFGLIB_MAX_PACKET_LENGTH)
    {
      cmd[index++] = PayloadLength + MFGLIB_PACKET_CRC_LENGTH;
      osMemCopy(Status, &cmd[index], PayloadData, PayloadLength + MFGLIB_PACKET_CRC_LENGTH); index+=(PayloadLength + MFGLIB_PACKET_CRC_LENGTH);
      zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, index, cmd);
    }
  else
    {
      printError(Service, "EZSP_MFGLIB_SEND_PACKET: Length %d exceeds max %d\n", PayloadLength, MFGLIB_MAX_PACKET_LENGTH);
      erStatusSet(Status, ZAB_ERROR_VENDOR_PARSE);
    }
}
void zabNcpEzspMfglib_SendPacketResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus responseStatus;
  if (PayloadLength >= 1)
    {
      responseStatus = PayloadData[0];
      printVendor(Service, "EZSP_MFGLIB_SEND_PACKET: Status: %s (0x%02X)\n",
                  decodeEmberStatus(responseStatus),
                  responseStatus);

      zabNcpRft_GeneralResponseHandler(Status, Service, responseStatus);
    }
}

/******************************************************************************
 * Set Channel Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_SetChannel(erStatus* Status, zabService* Service, unsigned8 Channel)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH + 1] = {0/*seq*/, 0/*FrameControl*/, EZSP_MFGLIB_SET_CHANNEL};
  unsigned8 index;
  ER_CHECK_STATUS(Status);

  index = EZSP_PARAMETERS_INDEX;
  cmd[index++] = Channel;

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, index, cmd);
}
void zabNcpEzspMfglib_SetChannelResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus responseStatus;
  if (PayloadLength >= 1)
    {
      responseStatus = PayloadData[0];
      printVendor(Service, "EZSP_MFGLIB_SET_CHANNEL: Status: %s (0x%02X)\n",
                  decodeEmberStatus(responseStatus),
                  responseStatus);

      zabNcpRft_GeneralResponseHandler(Status, Service, responseStatus);
    }
}

/******************************************************************************
 * Get Power Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_GetPower(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH] = {0/*seq*/, 0/*FrameControl*/, EZSP_MFGLIB_GET_POWER};
  ER_CHECK_STATUS(Status);

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspMfglib_GetPowerResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  signed8 Power;
  if (PayloadLength >= 1)
    {
      Power = (signed8)PayloadData[0];
      printVendor(Service, "EZSP_MFGLIB_GET_POWER: %d dBm\n", Power);
      zabNcpNwk_GetPowerResponseHandler(Status, Service, Power);
    }
}

/******************************************************************************
 * Set Power Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_SetPower(erStatus* Status, zabService* Service, signed8 Power)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH + 3] = {0/*seq*/, 0/*FrameControl*/, EZSP_MFGLIB_SET_POWER};
  unsigned8 index;
  ER_CHECK_STATUS(Status);

  index = EZSP_PARAMETERS_INDEX;
  COPY_OUT_16_BITS(&cmd[index], EMBER_TX_POWER_MODE_DEFAULT); index+=2;
  cmd[index++] = (unsigned8)Power;

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, index, cmd);
}
void zabNcpEzspMfglib_SetPowerResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus responseStatus;
  if (PayloadLength >= 1)
    {
      responseStatus = PayloadData[0];
      printVendor(Service, "EZSP_MFGLIB_SET_POWER: Status: %s (0x%02X)\n",
                  decodeEmberStatus(responseStatus),
                  responseStatus);

      zabNcpRft_GeneralResponseHandler(Status, Service, responseStatus);
    }
}

/******************************************************************************
 * Message Received Handler
 ******************************************************************************/
void zabNcpEzspMfglib_RxHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 lqi;
  signed8 rssi;
  unsigned8 packetLength;
  unsigned8 index;
  unsigned8 packetIndex;

  if (PayloadLength >= 3)
    {
      index = 0;
      lqi = PayloadData[index++];
      rssi = (signed8)PayloadData[index++];
      packetLength = PayloadData[index++];

      printVendor(Service, "EZSP_MFGLIB_RX_HANDLER: LQI=0x%02X, Rssi=%d, Length = %d, Data =",
                  lqi, rssi, packetLength);
      for (packetIndex = 0; packetIndex < packetLength; packetIndex++)
        {
          printVendor(Service, " %02X", PayloadData[index+packetIndex]);
        }
      printVendor(Service, "\n");

      zabNcpRft_RxHandler(Status, Service, rssi, packetLength, &PayloadData[index]);
    }
}

/******************************************************************************
 * Get CTune Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_GetCtune(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH] = {0/*seq*/, 0/*FrameControl*/, EZSP_GET_CTUNE};
  ER_CHECK_STATUS(Status);

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspMfglib_GetCtuneResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned16 cTune;
  if (PayloadLength >= 2)
    {
      cTune = COPY_IN_16_BITS(PayloadData);
      printVendor(Service, "EZSP_GET_CTUNE: 0x%04X\n", cTune);

      zabNcpRft_GetFreqTuneResponseHandler(Status, Service, cTune);
    }
}

/******************************************************************************
 * Set CTune Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_SetCtune(erStatus* Status, zabService* Service, unsigned16 Ctune)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH + 2] = {0/*seq*/, 0/*FrameControl*/, EZSP_SET_CTUNE};
  unsigned8 index;
  ER_CHECK_STATUS(Status);

  index = EZSP_PARAMETERS_INDEX;
  COPY_OUT_16_BITS(&cmd[index], Ctune); index+=2;

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, index, cmd);
}
void zabNcpEzspMfglib_SetCtuneResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  printVendor(Service, "EZSP_SET_CTUNE: Response, assume success\n");

  zabNcpRft_GeneralResponseHandler(Status, Service, EMBER_SUCCESS);
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/