/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains manages Mutexes for the ZAB ConsoleTest Application
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 *                                            Major upgrade and improvements to handle services.
 * 00.00.07.01  03-Nov-14   MvdB   ARTF107075: Correct usage of pthread_mutex_t, change Mutex array to pointers
 * 002.002.010  09-Oct-15   MvdB   ARTF150980: Support Service ID in memory allocation functions for WTB
 *****************************************************************************/


#include "zabCoreService.h"
#include "mutex.h"
#include "appConfig.h"
#include "appMain.h"
#include "appOsMemoryGlue.h"

#include "../include/os.h"

//#include <pthread.h>
//#include <assert.h>

/******************************************************************************
 *                      *****************************
 *                 *****       LOCAL VARIABLES       *****
 *                      *****************************
 ******************************************************************************/

/* Array of services using Mutexes */
static zabService*      MutexService[APP_CONFIG_M_MAX_INSTANCES];

/* Two dimensional array of APP_CONFIG_M_MAX_INSTANCES instances each with MUTEX_MAX mutexes */
static OS_MUTEX*  Mutex[APP_CONFIG_M_MAX_INSTANCES][MUTEX_MAX];



/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise mutex management
 ******************************************************************************/
void MutexInit(void)
{
  unsigned8 serviceIndex;
  unsigned8 mutexIndex;
  
  for (serviceIndex = 0; serviceIndex < APP_CONFIG_M_MAX_INSTANCES; serviceIndex++)
    {
      MutexService[serviceIndex] = NULL;
      
      for (mutexIndex = 0; mutexIndex < MUTEX_MAX; mutexIndex++)
        {
          Mutex[serviceIndex][mutexIndex] = NULL;
        }
    }
}


/******************************************************************************
 * Destroy all mutexes.
 ******************************************************************************/
void MutexDestroy(void)
{
  unsigned8 serviceIndex;
  unsigned8 mutexIndex;
  RTOS_ERR  errOSMutex;
  
  for (serviceIndex = 0; serviceIndex < APP_CONFIG_M_MAX_INSTANCES; serviceIndex++)
    {
      MutexService[serviceIndex] = NULL;
      
      for (mutexIndex = 0; mutexIndex < MUTEX_MAX; mutexIndex++)
        {
          if (Mutex[serviceIndex][mutexIndex] != NULL)
            {
              printError(MutexService[serviceIndex], "MutexDestroy: WARNING ServiceIndex %d MutexIndex %d was not destroyed!\n", serviceIndex, mutexIndex);
			  OSMutexDel(Mutex[serviceIndex][mutexIndex],OS_OPT_DEL_ALWAYS,&errOSMutex);             
              APP_MEM_GLUE_FREE(Mutex[serviceIndex][mutexIndex], appMain_GetServiceId(MutexService[serviceIndex]));
              Mutex[serviceIndex][mutexIndex] = NULL;
            }
        }
    }
}


/******************************************************************************
 * Create a new Mutex for a Service.
 * Return index+1 if successful, or zero if unsuccessful
 ******************************************************************************/
unsigned8 MutexCreate(void* Service)
{  

  unsigned8 serviceIndex;
  unsigned8 newServiceIndex;
  unsigned8 mutexIndex;
  RTOS_ERR  errOSMutex;
  
  /* Loop through services:
   *  - If service already exists, break out with the correct serviceIndex 
   *  - If we find an unused (null) service, store it's index so we can use it if we don't find the service already exists*/
  newServiceIndex = 0xFF;
  for (serviceIndex = 0; serviceIndex < APP_CONFIG_M_MAX_INSTANCES; serviceIndex++)
    {
      if (MutexService[serviceIndex] == Service)
        {
          break;
        }
      else if ( (MutexService[serviceIndex] == NULL) &&
                (newServiceIndex >= APP_CONFIG_M_MAX_INSTANCES) )
        {
          newServiceIndex = serviceIndex;
        }
    }
  
  /* If serviceIndex is good then service exists, so use it.
   * Else if newServiceIndex is good then we can create the new service and use that.
   * Else it does not exist and we are full, so fail */
  if (serviceIndex >= APP_CONFIG_M_MAX_INSTANCES)
    {
      if (newServiceIndex >= APP_CONFIG_M_MAX_INSTANCES)
        {
          printError(Service, "MutexCreate: ERROR - All Services full\n");
          return 0;
        }
      else
        {
          serviceIndex = newServiceIndex;
          MutexService[serviceIndex] = Service;
        }
    }
  
  for (mutexIndex = 0; mutexIndex < MUTEX_MAX; mutexIndex++)
    {
      if (Mutex[serviceIndex][mutexIndex] == NULL)
        {
        
          Mutex[serviceIndex][mutexIndex] = APP_MEM_GLUE_MALLOC(sizeof(OS_MUTEX), 
                                                                appMain_GetServiceId(Service),
                                                                MALLOC_ID_CFG_APP_MUTEX); 
          OSMutexCreate(Mutex[serviceIndex][mutexIndex], NULL,&errOSMutex );
          if ( (Mutex[serviceIndex][mutexIndex] != NULL) &&
               ( errOSMutex.Code == RTOS_ERR_NONE) )
            {
              return mutexIndex+1;
            }  
          else
            {
              printError(Service, "MutexCreate: ERROR - pthread_mutex_init failed\n");
              return 0;
            }
        }
    }
  
  printError(Service, "MutexCreate: ERROR - All mutexes used for service\n");

  return 0;
}


/******************************************************************************
 * Release a Mutex for a Service.
 ******************************************************************************/
void MutexRelease(void* Service, unsigned8 MutexId)
{
  unsigned8 serviceIndex;
  unsigned8 mutexIndex;
  RTOS_ERR  errOSMutex;
  
  if (MutexId > 0)
    {
      mutexIndex = MutexId-1;
          
      for (serviceIndex = 0; serviceIndex < APP_CONFIG_M_MAX_INSTANCES; serviceIndex++)
        {
          if (MutexService[serviceIndex] == Service)
            {
				OSMutexDel(Mutex[serviceIndex][mutexIndex],OS_OPT_DEL_ALWAYS,&errOSMutex);
              if (errOSMutex.Code == RTOS_ERR_NONE)
                {
                  APP_MEM_GLUE_FREE(Mutex[serviceIndex][mutexIndex], appMain_GetServiceId(Service));
                  Mutex[serviceIndex][mutexIndex] = NULL;
                }
              else
                {
                  printError(Service, "MutexRelease: ERROR Mutex release FAILED Service=%d, Mutex=%d\n", serviceIndex, mutexIndex);
                }
              return;
            }
        }
    }
  printError(Service, "MutexRelease: ERROR Mutex not found to release Service=%x, Mutex=%d\n", Service, MutexId);
}

/******************************************************************************
 * Enter a Mutex for a Service.
 ******************************************************************************/
void MutexEnter(void* Service, unsigned8 MutexId)
{  
  unsigned8 serviceIndex;
  unsigned8 mutexIndex;
  RTOS_ERR  errOSMutex;
  
  if (MutexId > 0)
    {
      mutexIndex = MutexId-1;
          
      for (serviceIndex = 0; serviceIndex < APP_CONFIG_M_MAX_INSTANCES; serviceIndex++)
        {
          if (MutexService[serviceIndex] == Service)
            {
              OSMutexPend(Mutex[serviceIndex][mutexIndex], 0, OS_OPT_PEND_BLOCKING, NULL, &errOSMutex);
              if (errOSMutex.Code != RTOS_ERR_NONE)
                {
                  printError(Service, "MutexEnter: ERROR Mutex enter FAILED Service=%d, Mutex=%d\n", serviceIndex, mutexIndex);
                }
              return;
            }
        }
    }
  printError(Service, "MutexEnter: ERROR Mutex not found to enter Service=%x, MutexId=%d\n", Service, MutexId);
}

/******************************************************************************
 * Exit a Mutex for a Service.
 ******************************************************************************/
void MutexExit(void* Service, unsigned8 MutexId)
{
  
  unsigned8 serviceIndex;
  unsigned8 mutexIndex;
  RTOS_ERR  errOSMutex;
  
  if (MutexId > 0)
    {
      mutexIndex = MutexId-1;
          
      for (serviceIndex = 0; serviceIndex < APP_CONFIG_M_MAX_INSTANCES; serviceIndex++)
        {
          if (MutexService[serviceIndex] == Service)
            {
              OSMutexPost (Mutex[serviceIndex][mutexIndex] , OS_OPT_POST_NONE, &errOSMutex);
              if (errOSMutex.Code != RTOS_ERR_NONE)
                {
                  printError(Service, "MutexExit: ERROR Mutex exit FAILED Service=%d, Mutex=%d\n", serviceIndex, mutexIndex);
                }
              return;
            }
        }
    }
  printError(Service, "MutexExit: ERROR Mutex not found to exit Service=%x, MutexId=%d\n", Service, MutexId);

}



/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/