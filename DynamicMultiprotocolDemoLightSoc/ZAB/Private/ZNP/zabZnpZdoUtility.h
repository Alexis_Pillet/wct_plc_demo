/*
 * zabZnpZdoUtility.h
 *
 *  Created on: 1 oct. 2012
 *      Author: SESA236777
 * 
 * 00.00.03.01  19-Mar-14   MvdB   artf53864: Support mgmt leave request/response
 * 00.00.06.00  27-Jun-14   MvdB   Add User Descriptor support
 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 00.00.06.02  30-Sep-14   MvdB   artf55512: Support network key change action
 * 00.00.06.03  01-Oct-14   MvdB   artf104879: Support energy scans via Mgmt Nwk Update Request
 * 002.000.001  03-Feb-15   MvdB   ARTF115770: Support ZDO Node Descriptor Request 
 * 002.000.004  01-Apr-15   MvdB   ARTF56504: Support missed channel change recovery, with actions/notifications/asks/gives to get/set parameters.
 * 002.002.015  21-Oct-15   MvdB   ARTF104106: Support SZL_ZDO_MatchDescriptorReq()
 */

#ifndef ZABZNPZDOUTILITY_H_
#define ZABZNPZDOUTILITY_H_


#include "zabZnpZdoFrame.h"



#ifdef __cplusplus
extern "C" {
#endif
  
#include "osTypes.h"
#include "erStatusUtility.h" 
#include "sapTypes.h"
#include "zabTypes.h"
  
/* Values of ScanDuration for Mgmt Network Update Request*/
#define ZAB_ZNP_ZDO_M_SCAN_DURATION_ENERGY_SCAN_ONE     0x01  
#define ZAB_ZNP_ZDO_M_SCAN_DURATION_ENERGY_SCAN_FIVE    0x05  
#define ZAB_ZNP_ZDO_M_SCAN_DURATION_CHANGE_CHANNEL      0xFE
#define ZAB_ZNP_ZDO_M_SCAN_DURATION_CHANGE_NWK_MANAGER  0xFF
  
#define ZAB_ZNP_ZDO_M_KEY_LENGTH 16
  
  
/******************************************************************************
 * ZDO Network Rejoin Request
 * TEMP TEMP TEMP - EXPERIMENTAL CODE
 ******************************************************************************/
extern
void zabZnpZdoUtility_NwkRejoinReq( erStatus* Status, zabService* Service, unsigned8 Secure, unsigned8 Channel);
/******************************************************************************
 * ZDO Network Discovery Request 2
 * TEMP TEMP TEMP - EXPERIMENTAL CODE
 ******************************************************************************/
extern 
void zabZnpZdoUtility_NwkDisc2Req( erStatus* Status, zabService* Service);
/******************************************************************************
 * ZDO Network Discovery Request 2 Cleanup
 * TEMP TEMP TEMP - EXPERIMENTAL CODE
 ******************************************************************************/
extern 
void zabZnpZdoUtility_NwkDisc2CleanupReq( erStatus* Status, zabService* Service);

/******************************************************************************
 * ZDO Network Key Update Request
 * This sends a APS Transport Key command with a new network key.
 ******************************************************************************/
extern
void zabZnpZdoUtility_NwkKeyUpdateReq(erStatus* Status, zabService* Service,
                                      unsigned16 DestinationAddress,
                                      unsigned8 KeySequenceNumber,
                                      unsigned8* NetworkKey);

/******************************************************************************
 * ZDO Network Key Switch Request
 * This sends a APS Switch Key command with the parametised Key Sequence Number
 ******************************************************************************/
extern
void zabZnpZdoUtility_NwkKeySwitchReq(erStatus* Status, zabService* Service, 
                                      unsigned16 DestinationAddress,
                                      unsigned8 KeySequenceNumber);

/******************************************************************************
 * ZDO TCLK Request
 * TEMP TEMP TEMP - EXPERIMENTAL CODE
 ******************************************************************************/
extern 
void zabZnpZdoUtility_TclkReq( erStatus* Status, zabService* Service);
  
/******************************************************************************
 * ZDO IEEE Address Request
 * Convert from ZAB format to vendor format
 ******************************************************************************/  
extern 
void zabZnpZdoUtility_IeeeMsgConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Network Address Request
 * Convert from ZAB format to vendor format
 ******************************************************************************/ 
extern 
void zabZnpZdoUtility_NwkMsgConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Power Descriptor Request
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern 
void zabZnpZdoUtility_PowerDescReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Node Descriptor Request
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern 
void zabZnpZdoUtility_NodeDescReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Active Endpoint Request
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern 
void zabZnpZdoUtility_ActiveEndpointReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Simple Descriptor Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern 
void zabZnpZdoUtility_SimpleDescriptorReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Match Descriptor Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern 
void zabZnpZdoUtility_MatchDescriptorReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO User Descriptor Request
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern 
void zabZnpZdoUtility_UserDescriptorReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO User Descriptor Set Request
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern 
void zabZnpZdoUtility_UserDescriptorSetReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Mgmt LQI Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern
void zabZnpZdoUtility_MgmtLqiReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Mgmt RTG Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern
void zabZnpZdoUtility_MgmtRtgReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Mgmt Bind Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern 
void zabZnpZdoUtility_MgmtBindReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Mgmt Leave Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern
void zabZnpZdoUtility_MgmtLeaveReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);


/******************************************************************************
 * MGMT Network Update Request
 ******************************************************************************/
extern
void zabZnpZdoUtility_MgmtNwkUpdateReq(erStatus* Status, zabService* Service, 
                                       zabAddressMode DestinationAddressMode,
                                       unsigned16 DestinationAddress,
                                       unsigned32 ChannelMask,
                                       unsigned8 ScanDuration,
                                       unsigned8 ScanCount,
                                       unsigned16 NetworkManagerAddress,
                                       unsigned8 tid);

/******************************************************************************
 * MGMT Network Update Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern 
void zabZnpZdoUtility_MgmtNwkUpdateReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Bind Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern
void zabZnpZdoUtility_BindReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO UnBind Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
extern 
void zabZnpZdoUtility_UnBindReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * Join Request
 ******************************************************************************/
extern 
void zabZnpZdoUtility_JoinReq(erStatus* Status, zabService* Service, 
                              unsigned8 Channel,
                              unsigned16 PanId,
                              unsigned64 Epid,
                              unsigned16 ParentNwkAddr,
                              unsigned8 ParentDepth,
                              unsigned8 StackProfile);

/******************************************************************************
 * ZDO Device Announce Request
 ******************************************************************************/
extern 
void zabZnpZdoUtility_DeviceAnnounceReq(erStatus* Status, zabService* Service, 
                                        unsigned16 nwkAddr, 
                                        unsigned64 ieeeAddr, 
                                        unsigned8 capabilities);

/******************************************************************************
 * Startup From App Request
 ******************************************************************************/
extern 
void zabZnpZdoUtility_StartupFromApp(erStatus* Status, zabService* Service, unsigned16 startDelay);

/******************************************************************************
 * Network Discovery Request
 * Starts an Active Scan to search for networks to join
 ******************************************************************************/
extern 
void zabZnpZdoUtility_NetworkDiscoveryReq(erStatus* Status, zabService* Service, unsigned32 channelMask, unsigned8 scanDuration);

/******************************************************************************
 * MGMT Permit Join Request
 ******************************************************************************/
extern
void zabZnpZdoUtility_PermitJoinReq(erStatus* Status, zabService* Service, 
                                    zabAddressMode AddressMode,
                                    unsigned16 DestinationAddress,
                                    unsigned8 Duration,
                                    zab_bool TCSignificance, 
                                    unsigned8 tid);

/******************************************************************************
 * ZDO Get Trust Center Ping Time Request
 ******************************************************************************/
extern
void zabZnpZdoUtility_TcPingTimeGetReq( erStatus* Status, zabService* Service);

/******************************************************************************
 * ZDO Set Trust Center Ping Time Request
 ******************************************************************************/
extern
void zabZnpZdoUtility_TcPingTimeSetReq( erStatus* Status, zabService* Service, 
                                        unsigned32 PingTimeSlowMs, unsigned32 PingTimeFastMs);

/******************************************************************************
 * Process incoming ZDO messages
 ******************************************************************************/
extern
void zabZnpZdoUtility_ProcessIncomingMessage(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * This function returns string of ZDO State
 ******************************************************************************/
extern
char* zabZnpZdoUtility_GetZdoStateString(ZDO_STATE zdoStatus);

#ifdef __cplusplus
}
#endif



#endif /* ZABZNPZDOUTILITY_H_ */
