/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the open state machine for the ZAB Pro ZNP
 *   vendor.
 *   For details and flow charts see XXX.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.02.00  22-Oct-13   MvdB   Original.
 *                                 Support notification of errors
 * 00.00.04.00  18-Mar-14   MvdB   artf53850: Add support for asking to stop NP on close
 * 00.00.06.00  26-Jun-14   MvdB   artf67560: Tidy up order of notification to make state management easier
 *                                 artf103951: Improve close from bootloader
 * 00.00.06_GP  15-Oct-14   MvdB   ARTF105854: Support Smartlink IPZ hardware for Pro+GP ZNP
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 * 01.100.07.00 11-Feb-15   MvdB   When opening, give App Type/Version from SBL even if not compatible
 * 001.101.000  20-Feb-15   MvdB   Improve handling of future ZNP ProductIds
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
 * 002.000.007  13-Apr-15   MvdB   ARTF130627: Handle SRSP errors in Open state machine for better error management
 *                                             Ensure glue is closed on error before indicating CLOSED to the app
 * 002.001.001  29-Apr-15   MvdB   ARTF132254: Clean up type casts in vendor for IAR compiler
 *                                 ARTF131778: zabZnpOpen.c uses erStatusSet(Service,..) instead of erStatusSet(Status, ...)
 *                                 ARTF132260: Support firmware upgrade of CC2538 network processors
 * 002.001.004  21-Jul-15   MvdB   ARTF132296: Add zabZnpOpen_ReadyForMessagesIn()
 * 002.002.004  02-Sep-15   MvdB   ARTF146123: Wait for close notification from glue when going to SBL.
 *                                             Only perform the close for the network processors that need it.
 * 002.002.013  14-Oct-15   MvdB   ARTF151072: Add service pointer to osTimeGetMilliseconds() and osTimeGetSeconds()
 * 002.002.023  28-Jun-16   MvdB   ARTF172483: Modify reset to bootloader for CC2538USB to handle re-enumeration required to disable watchdog
 * 002.002.024  28-Jun-16   MvdB   ARTF172101: Implement ZNP ping to handle lost Reset indication when hidden inside a serial command that was not completed before reset
 * 002.002.028  13-Oct-16   MvdB   ARTF186964: ZNP Ping should check network action not in process before starting ping
 *                                 ARTF186981: SLIPZ stuck with CLOSED but NETWORKED when ZNP ping fails first send
 * 002.002.031  09-Jan-17   MvdB   ARTF172881: Avoid work() returning zero due to errors like szl not initialised
 * 002.002.043  20-Jan-17   MvdB   ARTF190099: Make ping time run time controllable for ZNP & NCP. Private APIs only at this time, for industrial tester.
 *                                             Imported from 2.2.9 Patch Set branch rec 2.2.8
 * 002.002.053  12-Apr-17   SMon   Support ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE_PA
 *****************************************************************************/

#include "zabZnpService.h"



/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* Timer disabled value */
#define M_TIMEOUT_DISABLED 0xFFFFFFFF

/* Validation of timeouts */
#define M_PING_TIME_MINIMUM_MS ( 1000 )
#if (ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS > ZAB_OPEN_M_NET_PROC_PING_DISABLED)
  #error ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS > ZAB_OPEN_M_NET_PROC_PING_DISABLED
#endif
#if (ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS < ZAB_VND_CFG_ZNP_M_OPEN_SERIAL_GLUE_TIMEOUT_MS)
  #warning ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS < ZAB_VND_CFG_ZNP_M_OPEN_SERIAL_GLUE_TIMEOUT_MS
#endif
#if (ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS < ZAB_VND_CFG_ZNP_M_OPEN_SOFT_RST_TIMEOUT_MS)
  #warning ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS < ZAB_VND_CFG_ZNP_M_OPEN_SOFT_RST_TIMEOUT_MS
#endif
#if (ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS < ZAB_VND_CFG_ZNP_M_OPEN_SBL_RST_TIMEOUT_MS)
  #warning ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS < ZAB_VND_CFG_ZNP_M_OPEN_SBL_RST_TIMEOUT_MS
#endif
#if (ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS < ZAB_VND_CFG_ZNP_M_OPEN_GENERAL_TIMEOUT_MS)
  #warning ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS < ZAB_VND_CFG_ZNP_M_OPEN_GENERAL_TIMEOUT_MS
#endif
#if (ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS < M_PING_TIME_MINIMUM_MS)
  #error ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS < M_PING_TIME_MINIMUM_MS
#endif



/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/* Macros for accessing Open info */
#define ZAB_SERVICE_ZNP_OPEN( _srv )  (ZAB_SERVICE_VENDOR( (_srv) ) ->zabOpenInfo)
#define GET_OPEN_STATE( _srv )  (ZAB_SERVICE_ZNP_OPEN( (_srv) ).openState)
#define GET_OPEN_ITEM( _srv )  (ZAB_SERVICE_ZNP_OPEN( (_srv) ).currentItem)

#define GET_PING_FAIL_COUNT( _srv )  ( ZAB_SERVICE_ZNP_OPEN( (_srv) ).networkProcessorSendPingFailCount )


#define PING_TIME_MS( _srv )  ( ZAB_SERVICE_ZNP_OPEN( (_srv) ).networkProcessorPingTimeMs )

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Set item if status is good
 * Otherwise graceful harakiri
 ******************************************************************************/
static void checkStatusSetItem(erStatus* Status, zabService* Service, zabOpenCurrentItem item)
{
  if (erStatusIsOk(Status))
    {
      ZAB_SERVICE_ZNP_OPEN(Service).currentItem = item;
    }
  else
    {
      printError(Service, "Open: Error: Command failed at item 0x%02X with error 0x%04X. Resetting state machine\n", item, Status->Error);
      /* We had an error sending a command, so things are broken*/
      zabZnpOpen_Create(Status, Service);
    }
}

/******************************************************************************
 * Set network state and notify the manage sap
 ******************************************************************************/
static void setAndNotifyOpenState(erStatus* Status, zabService* Service, zabOpenState openState)
{
  ZAB_SERVICE_ZNP_OPEN(Service).openState = openState;
  sapMsgPutNotifyOpenState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, openState);
}

/******************************************************************************
 * Set a timeout
 ******************************************************************************/
static void setTimeout(erStatus* Status, zabService* Service, unsigned32 Timeout)
{
  if (erStatusIsOk(Status))
    {
      osTimeGetMilliseconds(Service, &ZAB_SERVICE_ZNP_OPEN(Service).timeoutExpiryMs);
      ZAB_SERVICE_ZNP_OPEN(Service).timeoutExpiryMs += Timeout;

      /* Handle the (very unlikely) case our timeout works out to the disabled value */
      if (ZAB_SERVICE_ZNP_OPEN(Service).timeoutExpiryMs == M_TIMEOUT_DISABLED)
        {
          ZAB_SERVICE_ZNP_OPEN(Service).timeoutExpiryMs++;
        }
    }
}

/******************************************************************************
 * Disable timeout
 ******************************************************************************/
static void disableTimeout(erStatus* Status, zabService* Service)
{
  ZAB_SERVICE_ZNP_OPEN(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
}


/******************************************************************************
 * Handle a Reset Indication
 ******************************************************************************/
static void localSysResetIndHandler(erStatus* Status,
                                    zabService* Service,
                                    zab_bool Compatible,
                                    znpProductId ProductId)
{
  erStatus LocalStatus;
  erStatusClear(&LocalStatus, Service);
  disableTimeout(Status, Service);
  checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_NONE);

  if (Compatible)
    {

      /* */
      switch (ProductId)
        {
          /* Its a ZNP - perform soft reset so we know all endpoints are cleared
           * and we are starting with a known state */
          case znpProductId_TI_Znp:
          case znpProductId_Schneider_Pro_Znp:
          case znpProductId_Schneider_Pro_GP_Znp:
          case znpProductId_Schneider_Reserved_Future_Znp1:
          case znpProductId_Schneider_Reserved_Future_Znp2:
          case znpProductId_Schneider_Reserved_Future_Znp3:
          case znpProductId_Schneider_Reserved_Future_Znp4:
          case znpProductId_Schneider_Reserved_Future_Znp5:
          case znpProductId_Schneider_Reserved_Future_Znp6:
          case znpProductId_Schneider_Reserved_Future_Znp7:
            znpi_sapi_getDeviceInfo(Status, Service, ZAB_ZNP_SAPI_DEV_INFO_TYPE_DEV_IEEE_ADDR);
            checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_GET_IEEE_BEFORE_OPENED);
            // SRSP will handle timeout
            break;

          /* Its an SBL - check application valid/version */
          case znpProductId_Schneider_Sbl:
            zabZnpSblUtility_AppVersionReq(Status, Service);
            checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_SBL_APP_VERSION);
            setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_OPEN_GENERAL_TIMEOUT_MS);
            break;

          default:
            erStatusSet(&LocalStatus, ZAB_ERROR_VENDOR_UNKNOWN_NETWORK_PROCESSOR);
            zabZnpOpen_CloseDueToError(&LocalStatus, Service);
            break;
        }
    }
  else
    {
      zabZnpOpen_InitNetworkProcessorPing(Status, Service, ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS);
      setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE);
    }
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 * Create
 * Initialises open state / state machine
 ******************************************************************************/
void zabZnpOpen_Create(erStatus* Status, zabService* Service)
{
  setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_CLOSED);
  ZAB_SERVICE_ZNP_OPEN(Service).currentItem = ZAB_OPEN_CURRENT_ITEM_NONE;
  ZAB_SERVICE_ZNP_OPEN(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
  ZAB_SERVICE_ZNP_OPEN(Service).networkProcessorPingTimeMs = ZAB_OPEN_M_NET_PROC_PING_DISABLED;
  GET_PING_FAIL_COUNT(Service) = 0;
}

/******************************************************************************
 * Get Open State
 ******************************************************************************/
zabOpenState zabZnpOpen_GetOpenState(zabService* Service)
{
  return ZAB_SERVICE_ZNP_OPEN(Service).openState;
}

/******************************************************************************
 * Is ZAB in a state that is ready to accept messages from the Serial Glue?
 ******************************************************************************/
zab_bool zabZnpOpen_ReadyForMessagesIn(zabService* Service)
{
  if ( (ZAB_SERVICE_ZNP_OPEN(Service).openState >= ZAB_OPEN_STATE_CLOSING) &&
       (ZAB_SERVICE_ZNP_OPEN(Service).openState <= ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE) )
    {
      return zab_true;
    }
  return zab_false;
}

/******************************************************************************
 * Update timeout
 * Check for timeouts within the networking state machine
 ******************************************************************************/
unsigned32 zabZnpOpen_UpdateTimeout(erStatus* Status, zabService* Service, unsigned32 Time)
{
  erStatus LocalStatus;
  unsigned32 timeoutRemaining = ZAB_WORK_DELAY_MAX_MS;
  ER_CHECK_STATUS_VALUE(Status, timeoutRemaining);
  ER_CHECK_NULL_VALUE(Status, Service, timeoutRemaining);

  if (ZAB_SERVICE_ZNP_OPEN(Service).timeoutExpiryMs != M_TIMEOUT_DISABLED)
    {
      /* Check timeout while handling wrapped time*/
      timeoutRemaining = ZAB_SERVICE_ZNP_OPEN(Service).timeoutExpiryMs - Time;
      if ( (timeoutRemaining == 0) || (timeoutRemaining > ZAB_ZNP_MS_TIMEOUT_MAX) )
        {
          disableTimeout(Status, Service);
          erStatusClear(&LocalStatus, Service);

          /* Ask for fast work if timer expired as it may have triggered some other event */
          timeoutRemaining = 0;

          /* Deal with timeouts based on the item we were waiting for */
          switch(GET_OPEN_ITEM(Service))
            {

              case ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_ZNP: /* Sys reset failed, so close glue and notify app */
              case ZAB_OPEN_CURRENT_ITEM_SBL_APP_VERSION:  /* App version timeout means something went very wrong. Report error and tidy up. */
              case ZAB_OPEN_CURRENT_ITEM_SYS_RESET_FROM_SBL: /* Timeout waiting for reset indication after start app */
                erStatusSet(&LocalStatus, ZAB_ERROR_ACTION_FAILED);
                zabZnpOpen_CloseDueToError(&LocalStatus, Service);
                break;

              case ZAB_OPEN_CURRENT_ITEM_OPEN_SERIAL_GLUE:
                /* Open failed, so close glue and notify app */
                erStatusSet(&LocalStatus, ZAB_ERROR_SERIAL_GLUE_ACTION_TIMEOUT);
                zabZnpOpen_CloseDueToError(&LocalStatus, Service);
                break;

              case ZAB_OPEN_CURRENT_ITEM_CLOSE_SERIAL_GLUE:
                /* Close failed. Not much we can do! */
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_SERIAL_GLUE_ACTION_TIMEOUT);

                // Simulate the action from the glue so everything is cleaned up consistently
                zabZnpOpen_InNotificationHandler(Status, Service, ZAB_OPEN_STATE_CLOSED);
                break;

              case ZAB_OPEN_CURRENT_ITEM_STOP_NP_DURING_CLOSE:
                /* Stop of network processor failed. Complete the close */
                sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_CLOSE);
                checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_CLOSE_SERIAL_GLUE);
                setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_OPEN_SERIAL_GLUE_TIMEOUT_MS);
                break;

              case ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_SBL_WAIT_BEFORE_REOPEN:
                sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_OPEN);
                checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_SBL_REOPEN);
                setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_OPEN_SERIAL_GLUE_TIMEOUT_MS);
                break;

              case ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_SBL_WITHOUT_CLOSE:
                /* We have timed out without getting a reset indication, so fail */
                erStatusSet(&LocalStatus, ZAB_ERROR_VENDOR_GO_TO_BOOTLOADER_FAILED);
                zabZnpOpen_CloseDueToError(&LocalStatus, Service);
                break;

              case ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_SBL_WAIT_CLOSE:
                /* We have now done our x second delay without getting a close notification from the glue, so fail.
                 * This will attempt another close, then if that also times out clean up.*/
                erStatusSet(&LocalStatus, ZAB_ERROR_VENDOR_GO_TO_BOOTLOADER_FAILED);
                zabZnpOpen_CloseDueToError(&LocalStatus, Service);
                break;

              case ZAB_OPEN_CURRENT_ITEM_SBL_REOPEN:
                /* We have now done our x second delay without getting open, so fail */
                erStatusSet(&LocalStatus, ZAB_ERROR_VENDOR_GO_TO_BOOTLOADER_FAILED);
                zabZnpOpen_CloseDueToError(&LocalStatus, Service);
                break;

              default:
                erStatusSet(&LocalStatus, ZAB_ERROR_ACTION_FAILED);
                zabZnpOpen_CloseDueToError(&LocalStatus, Service);
                break;
            }
        }
    }
  return timeoutRemaining;
}

/******************************************************************************
 * SRSP Timeout Handler
 * Notifies state machine a command has timed out. It will handle it if
 * it generated the command.
 ******************************************************************************/
void zabZnpOpen_srspTimeoutHandler(erStatus* Status, zabService* Service)
{
  if (ZAB_SERVICE_ZNP_OPEN(Service).currentItem != ZAB_OPEN_CURRENT_ITEM_NONE)
    {
      disableTimeout(Status, Service);

      /* Antenna is optional, so we let it complete as if we got a response */
      if (ZAB_SERVICE_ZNP_OPEN(Service).currentItem == ZAB_OPEN_CURRENT_ITEM_GET_ANTENNA_BEFORE_OPENED)
        {
          zabZnpOpen_AntennaRspHandler(Status, Service);
        }
      /* Error closing, so force closed */
      else if (ZAB_SERVICE_ZNP_OPEN(Service).currentItem == ZAB_OPEN_CURRENT_ITEM_CLOSE_SERIAL_GLUE)
        {
          zabZnpOpen_Create(Status, Service);
        }
      /* Everything else is a general failure so we will try to close the glue nicely */
      else
        {
          if (ZAB_SERVICE_ZNP_OPEN(Service).currentItem == ZAB_OPEN_CURRENT_ITEM_PING_SYS_VERSION)
            {
              printError(Service, "Network Processor Ping - No response!!!\n");
            }
          zabZnpOpen_CloseDueToError(Status, Service);
        }
    }
}

/******************************************************************************
 * Action
 * Handles Open and Close actions for the open state machines
 ******************************************************************************/
void zabZnpOpen_Action(erStatus* Status, zabService* Service, unsigned8 Action)
{
  unsigned8 stopNetworkProcessor;
  ER_CHECK_STATUS(Status);
  ER_CHECK_NULL(Status, Service);

  switch(Action)
    {
      /*
      ** Open
      */
      case ZAB_ACTION_OPEN:
        if (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_CLOSED)
          {
            setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_OPENING);
            sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_OPEN);
            checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_OPEN_SERIAL_GLUE);
            setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_OPEN_SERIAL_GLUE_TIMEOUT_MS);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
            setAndNotifyOpenState(Status, Service, GET_OPEN_STATE(Service));
          }
        break;

      /*
      ** Close
      */
      case ZAB_ACTION_CLOSE:
        if (GET_OPEN_STATE(Service) != ZAB_OPEN_STATE_CLOSED)
          {
            /* Only ask about stopping the network processor if it is open - if not its not running anyway */
            stopNetworkProcessor = zab_false;
            if ( (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_OPENED) ||
                 (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE) ||
                 (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE) )
              {
                stopNetworkProcessor = (unsigned8)srvCoreAskBack8( Status, Service, ZAB_ASK_NETWORK_PROCESSOR_STOP_ON_CLOSE, zab_true);
                printVendor(Service, "ZAB Znp Open: Ask Network Processor Stop on Close = 0x%02X", stopNetworkProcessor);
              }

            setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_CLOSING);

            if (stopNetworkProcessor)
              {
                znpi_sys_reset(Status, Service, SYS_SOFT_RESET);
                checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_STOP_NP_DURING_CLOSE);

                // Run a local timeout as reset is an AREQ
                setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_OPEN_SOFT_RST_TIMEOUT_MS);
              }
            else
              {
                sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_CLOSE);
                checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_CLOSE_SERIAL_GLUE);
                setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_OPEN_SERIAL_GLUE_TIMEOUT_MS);
              }
          }
        else
          {
            setAndNotifyOpenState(Status, Service, GET_OPEN_STATE(Service));
          }
        break;

      /*
      ** Firmware Update
      */
      case ZAB_ACTION_GO_TO_FIRMWARE_UPDATER:
        if ( (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_OPENED) ||
             (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE) )
          {
            setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_GOING_TO_FW_UPDATE);

            /* We now send a reset to bootloader command, but some USB devices will re-enumerate during this reset.
             * Handle device differently based on whether they will re-enumerate or not  */
            switch (ZAB_SERVICE_VENDOR(Service)->networkProcessorHardwareType)
              {
                /* Devices that will re-enumerate and hence break the connection if we do not manage it carefully.
                 * For these devices we will:
                 *  1. Send reset to SBL command
                 *  2. Close connection in glue.
                 *  3. Wait for SBL to have reset and re-enumerated.
                 *  4. Open connection in glue.
                 *  5. Continue.
                 * This process is higher risk as it relies on several timeouts. It is necessary for some products but
                 * should be avoided unless necessary! */
                default:
                  printError(Service, "zabZnpOpen_Action: ZAB_ACTION_GO_TO_FIRMWARE_UPDATER unknown hardware type. Defaulting to serial close & reopen.\n");
                case znpHardwareType_CC2531_TI_USB_Dongle:
                case znpHardwareType_CC2531_Spectec_USB_Dongle_PA:
                case znpHardwareType_CC2531_SE_Inventek_USB_Dongle_PA:
                case znpHardwareType_CC2538_OUREA_USB:
                  checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_SBL_WAIT_CLOSE);
                  setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_OPEN_SERIAL_GLUE_TIMEOUT_MS);
                  znpi_sys_reset(Status, Service, SYS_UPGRADE_RESET);
                  sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_CLOSE);
                  break;

                /* Devices that will maintain the serial connection are much simpler:
                 *  1. Send reset to SBL command
                 *  2. Wait for sys reset indication
                 *  5. Continue */
                case znpHardwareType_CC2530_TI_EB:
                case znpHardwareType_CC2530_OUREA_V2:
                case znpHardwareType_CC2530_Nova:
                case znpHardwareType_CC2530_SmartlinkIpz:
                case znpHardwareType_CC2538_TI_EB:
                case znpHardwareType_CC2538_OUREA_UART:
                case znpHardwareType_CC2538_SZCYIT_Dongle:
                case znpHardwareType_CC2538_SZCYIT_Dongle_PA:
                  checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_SBL_WITHOUT_CLOSE);
                  setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_OPEN_SBL_RST_TIMEOUT_MS);
                  znpi_sys_reset(Status, Service, SYS_UPGRADE_RESET);
                  break;
              }
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
            setAndNotifyOpenState(Status, Service, GET_OPEN_STATE(Service));
          }
        break;

    }
}

/******************************************************************************
 * Close due to an Error
 * This can be called by the vendor code when a critical error happens and
 * ZAB needs to be re-initialised. Typically failure of serial comms.
 ******************************************************************************/
void zabZnpOpen_CloseDueToError(erStatus* Status, zabService* Service)
{
  /* Use a local status for sending messages as the parametised one is an error and will cause message creation to fail*/
  erStatus localStatus;
  erStatusClear(&localStatus, Service);

  /* Notify the app of the error that has caused the closure */
  sapMsgPutNotifyError(&localStatus, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, (zabError)Status->Error);

  /* Notify the app we are closing */
  setAndNotifyOpenState(&localStatus, Service, ZAB_OPEN_STATE_CLOSING);

  /* Close serial glue */
  sapMsgPutAction(&localStatus, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_CLOSE);
  checkStatusSetItem(&localStatus, Service, ZAB_OPEN_CURRENT_ITEM_CLOSE_SERIAL_GLUE);
  setTimeout(&localStatus, Service, ZAB_VND_CFG_ZNP_M_OPEN_SERIAL_GLUE_TIMEOUT_MS);
}

/******************************************************************************
 * In Notification Handler
 * Handles notification from the serial glue for the open state machine
 ******************************************************************************/
void zabZnpOpen_InNotificationHandler(erStatus* Status, zabService* Service, zabOpenState OpenState)
{
  if ( ( (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_OPEN_SERIAL_GLUE) ||
         (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_SBL_REOPEN) )
          &&
       (OpenState == ZAB_OPEN_STATE_OPENED) )
    {
      disableTimeout(Status, Service);

      /* We're open with the glue, so now check if it's a bootloader, normal image or unknown device via SysVersion*/
      zabZnpSysUtility_VersionRequest(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_SYS_VERSION);
      // Use SRSP for timeout.
    }

  if (OpenState == ZAB_OPEN_STATE_CLOSED)
    {
      switch (GET_OPEN_ITEM(Service))
        {
          /* Do nothing in special cases where we transition this state */
          case ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_SBL_WAIT_BEFORE_REOPEN:
          case ZAB_OPEN_CURRENT_ITEM_SBL_REOPEN:
              break;

          /* We were waiting on the close, now start timer to re-open*/
          case ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_SBL_WAIT_CLOSE:
              checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_SBL_WAIT_BEFORE_REOPEN);
              setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_OPEN_SBL_RST_TIMEOUT_MS);
              break;

          default:
            /* Reset all state machines as all info is now invalid as we are closed */
            zabZnpNwk_create(Status, Service);
            zabZnpSblUtility_Create(Status, Service);
            zabZnpClone_Reset(Status, Service);
            zabZnpOpen_Create(Status, Service);
            break;
        }
    }

  /* If there was an error from the glue then shut everything down*/
  if (OpenState == ZAB_OPEN_STATE_ERROR)
    {
      zabZnpOpen_CloseDueToError(Status, Service);
    }
}




/******************************************************************************
 * SYS Version Response Handler
 * Accepts the version info of SBL or ZNP
 ******************************************************************************/
void zabZnpOpen_SysVersionRspHandler(erStatus* Status,
                                     zabService* Service,
                                     unsigned8 TransportRev,
                                     unsigned8 ProductId,
                                     unsigned8 MajorVersion,
                                     unsigned8 MinorVersion,
                                     unsigned8 ReleaseVersion)
{
  unsigned8 hardwareVersion[4];
  zab_bool networkProcessorStateMatchesZab;
  erStatus LocalStatus;
  erStatusClear(&LocalStatus, Service);

  if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_SYS_VERSION)
    {
      disableTimeout(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_NONE);

      /* */
      switch ((znpProductId)ProductId)
        {
#ifdef ZAB_VND_CFG_ALLOW_STANDARD_TI_ZNP
          case znpProductId_TI_Znp:
            printError(Service,
                  "*************************************************************\n"
                  "zabZnpOpen: WARNING - Standard TI ZNP\n"
                  "zabZnpOpen: WARNING - Schneider features will not work!\n"
                  "zabZnpOpen: WARNING - ZDO responses will not work (no TID)!\n"
                  "zabZnpOpen: WARNING - Permit Join will be permanently on at startup!\n"
                  "zabZnpOpen: WARNING - Endpoint re-registration will fail!\n"
                  "zabZnpOpen: WARNING - NV reads won't work!\n"
                  "zabZnpOpen: WARNING - HA joining probably won't work if using security!!!!!!\n"
                  "zabZnpOpen: WARNING - Any more surprises... Good luck!\n"
                  "*************************************************************\n");

            /* Deliberately allow to slide through to znpProductId_Schneider_Pro_Znp */
#endif

          /* Its a ZNP - perform soft reset so we know all endpoints are cleared
           * and we are starting with a known state */
          case znpProductId_Schneider_Pro_Znp:
          case znpProductId_Schneider_Pro_GP_Znp:
          case znpProductId_Schneider_Reserved_Future_Znp1:
          case znpProductId_Schneider_Reserved_Future_Znp2:
          case znpProductId_Schneider_Reserved_Future_Znp3:
          case znpProductId_Schneider_Reserved_Future_Znp4:
          case znpProductId_Schneider_Reserved_Future_Znp5:
          case znpProductId_Schneider_Reserved_Future_Znp6:
          case znpProductId_Schneider_Reserved_Future_Znp7:
            znpi_sys_reset(Status, Service, SYS_SOFT_RESET);
            checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_ZNP);
            setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_OPEN_SOFT_RST_TIMEOUT_MS);
            break;

          /* Its an SBL - check application valid/version */
          case znpProductId_Schneider_Sbl:
            srvCoreGiveBack8(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ACTIVE_APP_TYPE, (unsigned8)ZAB_NET_PROC_APP_BOOTLOADER);

            hardwareVersion[0] = MajorVersion;
            hardwareVersion[1] = MinorVersion;
            hardwareVersion[2] = ReleaseVersion;
            hardwareVersion[3] = 0;
            srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_BOOTLOADER_VERSION, sizeof(hardwareVersion), hardwareVersion);


            zabZnpSblUtility_AppVersionReq(Status, Service);
            checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_SBL_APP_VERSION);

            // Run a timeout of SBL stuff is AREQ
            setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_SBL_TIMEOUT_MS);
            break;

          default:
            erStatusSet(&LocalStatus, ZAB_ERROR_VENDOR_UNKNOWN_NETWORK_PROCESSOR);
            zabZnpOpen_CloseDueToError(&LocalStatus, Service);
            break;
        }
    }

  /* Ping Response - Confirm active application is correct! */
  else if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_PING_SYS_VERSION)
    {
      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_NONE);

      printVendor(Service, "Network Processor Ping - Response!!!\n");

      switch ((znpProductId)ProductId)
        {
          case znpProductId_TI_Znp:
          case znpProductId_Schneider_Pro_Znp:
          case znpProductId_Schneider_Pro_GP_Znp:
            if ( (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_OPENED) ||
                 (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE) )
              {
                // Open State and Network Processor application match
                networkProcessorStateMatchesZab = zab_true;
              }
            else
              {
                networkProcessorStateMatchesZab = zab_false;
              }
            break;

          case znpProductId_Schneider_Sbl:
            if (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE)
              {
                // Open State and Network Processor application match
                networkProcessorStateMatchesZab = zab_true;
              }
            else
              {
                networkProcessorStateMatchesZab = zab_false;
              }
            break;

          default:
            networkProcessorStateMatchesZab = zab_false;
            break;
        }

      if (networkProcessorStateMatchesZab == zab_false)
        {
          printError(Service, "Network Processor Ping - Mismatch Detected!!!\n");

          // Set compatible = true so it does a "normal" restart
          localSysResetIndHandler(Status, Service, zab_true, (znpProductId)ProductId);
        }
    }
}

/******************************************************************************
 * SBL App Version Response Handler
 * Accepts the version info of the app seen by the SBL
 ******************************************************************************/
void zabZnpOpen_SblAppVersionRspHandler(erStatus* Status,
                                        zabService* Service,
                                        sblStatus SblStatus,
                                        unsigned8 TransportRev,
                                        znpProductId ProductId,
                                        unsigned8 MajorVersion,
                                        unsigned8 MinorVersion,
                                        unsigned8 ReleaseVersion)
{
  unsigned8 npVersion[4];
  unsigned8 StartApp;

  if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_SBL_APP_VERSION)
    {
      disableTimeout(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_NONE);

      /* If there is a valid app, check it is compatible */
      if(SblStatus == SBL_STATUS_SUCCESS)
        {

          /* Give the model and version  */
          switch (ProductId)
            {
              case znpProductId_Schneider_Pro_Znp:
                srvCoreGiveBack8(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_TYPE, (unsigned8)ZAB_NET_PROC_APP_ZIGBEE_PRO);
                break;

              case znpProductId_Schneider_Pro_GP_Znp:
                srvCoreGiveBack8(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_TYPE, (unsigned8)ZAB_NET_PROC_APP_ZIGBEE_PRO_GP);
                break;

              default:
                srvCoreGiveBack8(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_TYPE, (unsigned8)ZAB_NET_PROC_APP_UNKNOWN);
                break;
            }

          npVersion[0] = MajorVersion;
          npVersion[1] = MinorVersion;
          npVersion[2] = ReleaseVersion;
          npVersion[3] = 0;
          srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_VERSION, sizeof(npVersion), npVersion);

          if (zabZnpSysUtility_CompatibilityCheck(TransportRev,
                                                  (znpProductId)ProductId,
                                                  MajorVersion,
                                                  MinorVersion,
                                                  ReleaseVersion) == zab_true)
            {

              /* App is valid and compatible, so ask if the app wants to start it */
              StartApp = (unsigned8)srvCoreAskBack8( Status, Service, ZAB_ASK_NETWORK_PROCESSOR_START, zab_true);
              printVendor(Service, "ZAB Znp Open: Ask Network Processor Start = 0x%02X", StartApp);

              if (StartApp != zab_false)
                {
                  zabZnpSblUtility_AppStartReq(Status, Service);
                  checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_SBL_START_APP);
                  setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_SBL_TIMEOUT_MS);
                  return;
                }
            }
        }

      /* If we get to here it means the App is not valid or the host asked not to start it, so we are open for firmware update*/
      zabZnpOpen_InitNetworkProcessorPing(Status, Service, ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS);
      setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE);
    }
}

/******************************************************************************
 * SBL App Start Response Handler
 * Accepts the status of the start request
 ******************************************************************************/
void zabZnpOpen_SblAppStartRspHandler(erStatus* Status,
                                      zabService* Service,
                                      sblStatus SblStatus)
{
  erStatus LocalStatus;
  erStatusClear(&LocalStatus, Service);

  if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_SBL_START_APP)
    {
      disableTimeout(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_NONE);

      if (SblStatus == SBL_STATUS_SUCCESS)
        {
          checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_SYS_RESET_FROM_SBL);
          setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_OPEN_SBL_RST_TIMEOUT_MS);
        }
      else
        {
          erStatusSet(&LocalStatus, ZAB_ERROR_ACTION_FAILED);
          zabZnpOpen_CloseDueToError(&LocalStatus, Service);
        }
    }
}


/******************************************************************************
 * SYS Reset Indication Handler
 * Accepts zab_bool parameter which indicates if the vendor is compatible
 * with the version of network processor.
 ******************************************************************************/
void zabZnpOpen_SysResetIndHandler(erStatus* Status,
                                   zabService* Service,
                                   zab_bool Compatible,
                                   znpProductId ProductId)
{
  erStatus LocalStatus;
  erStatusClear(&LocalStatus, Service);


  /* If we are resetting to SBL without close then this indication tells us the SBL (hopefully) has started */
  if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_SBL_WITHOUT_CLOSE)
    {
      disableTimeout(Status, Service);

      /* Hook back into the state machine with a version req as for ZAB_OPEN_CURRENT_ITEM_SBL_REOPEN */
      zabZnpSysUtility_VersionRequest(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_SYS_VERSION);
    }

  else if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_STOP_NP_DURING_CLOSE)
    {
      sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_CLOSE);
      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_CLOSE_SERIAL_GLUE);
      setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_OPEN_SERIAL_GLUE_TIMEOUT_MS);
    }

  // Do nothing as we are already opening, so don't want to interfere with the state machine
  // This case happens commonly on CC2538 USB, which buffers the reset indication and delivers it upon connect
  else if ( (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_OPEN_SERIAL_GLUE) ||
            (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_SYS_VERSION) )

    {

    }
  else
    {
      localSysResetIndHandler(Status, Service, Compatible, ProductId);
    }
}

/******************************************************************************
 * IEEE Response Handler
 * This just confirms we got an IEEE address, and now we can be open.
 ******************************************************************************/
void zabZnpOpen_IeeeRspHandler(erStatus* Status, zabService* Service)
{
  if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_GET_IEEE_BEFORE_OPENED)
    {
      zabZnpSysUtility_GetAntennaMode(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_GET_ANTENNA_BEFORE_OPENED);
      // SRSP will handle the timeout
    }
}

/******************************************************************************
 * IEEE Response Handler
 * This just confirms we got an IEEE address, and now we can be open.
 ******************************************************************************/
void zabZnpOpen_AntennaRspHandler(erStatus* Status, zabService* Service)
{
  /* WARNING - This command should always be last as it may not be supported!
   *           If not supported we allow it to complete via the timeout handler, but
   *           we want to avoid more commands as these (Should not but may) hit a busy serial link */
  if (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_GET_ANTENNA_BEFORE_OPENED)
    {
      disableTimeout(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_NONE);

      /* Notify network state machine we are no open and it can kick init */
      zabZnpNwk_resetHandler(Status, Service);

      /* Notify Opened last, so all network state is nicely initialised */
      zabZnpOpen_InitNetworkProcessorPing(Status, Service, ZAB_VND_CFG_ZNP_M_NET_PROC_PING_MS);
      setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_OPENED);
    }
}

/******************************************************************************
 * Notify open state machine that the SBL has started the app and it should expect a reset indication soon
 ******************************************************************************/
void zabZnpOpen_SblAppStartNotification(erStatus* Status, zabService* Service)
{
  setAndNotifyOpenState(Status, Service, ZAB_OPEN_STATE_OPENING);
  checkStatusSetItem(Status, Service, ZAB_OPEN_CURRENT_ITEM_SBL_LAUNCH_APP);
  setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_OPEN_SOFT_RST_TIMEOUT_MS);
}

/******************************************************************************
 * Init network processor ping service
 ******************************************************************************/
void zabZnpOpen_InitNetworkProcessorPing(erStatus* Status, zabService* Service, unsigned16 PingTimeMs)
{
  unsigned32 time;

  printVendor(Service, "Network Processor Ping - Init = %dms\n", PingTimeMs);

  if (PingTimeMs >= M_PING_TIME_MINIMUM_MS)
    {
      PING_TIME_MS(Service) = PingTimeMs;
      osTimeGetMilliseconds(Service, &time);
      ZAB_SERVICE_ZNP_OPEN(Service).lastNetworkProcessorPingTimeMs = time;
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
    }
}

/******************************************************************************
 * Run network processor ping service
 ******************************************************************************/
unsigned32 zabZnpOpen_UpdateNetworkProcessorPing(erStatus* Status, zabService* Service, unsigned32 Time)
{
  zabOpenState openState;
  unsigned32 timeoutRemaining = ZAB_WORK_DELAY_MAX_MS;
  zab_bool pingDue;
  erStatus LocalStatus;
  erStatusClear(&LocalStatus, Service);
  ER_CHECK_STATUS_VALUE(Status, timeoutRemaining);
  ER_CHECK_NULL_VALUE(Status, Service, timeoutRemaining);

  if (PING_TIME_MS(Service) < ZAB_OPEN_M_NET_PROC_PING_DISABLED)
    {

      /* First check if we are in a state when a ping is desired */
      openState = GET_OPEN_STATE(Service);
      if ( (openState == ZAB_OPEN_STATE_OPENED) ||
           (openState == ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE) ||
           ( (openState == ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE) && (zabZnpSblUtility_UpdateActive(Service) == zab_false) ) )
        {

          /* Calculate if a ping is due to be sent */
          pingDue = zab_false;
          if ( (Time + PING_TIME_MS(Service)) > ZAB_SERVICE_ZNP_OPEN(Service).lastNetworkProcessorPingTimeMs)
            {
              if (Time >= (ZAB_SERVICE_ZNP_OPEN(Service).lastNetworkProcessorPingTimeMs + PING_TIME_MS(Service)))
                {
                  pingDue = zab_true;
                }
              else
                {
                  timeoutRemaining = (ZAB_SERVICE_ZNP_OPEN(Service).lastNetworkProcessorPingTimeMs + PING_TIME_MS(Service)) - Time;
                }
            }
          /* Time is close to wrapping, just update the last tick time to timeNow until timeNow gets to zero */
          else
            {
              ZAB_SERVICE_ZNP_OPEN(Service).lastNetworkProcessorPingTimeMs = Time;
              timeoutRemaining = PING_TIME_MS(Service);
            }

          if (pingDue == zab_true)
            {
              /* Now check if we are ok to send it now or need to delay as something else is going on */
              if ( (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_NONE) &&
                   (zabZnpNwk_GetNwkStateMachineInProgress(Service) == zab_false) && // ARTF186964
                   (zabZnpService_GetSerialLinkLocked(Service) == zab_false) )
                {
                  printVendor(Service, "Network Processor Ping - Send ping!!!\n");

                  // Ping network processor
                  zabZnpSysUtility_VersionRequest(&LocalStatus, Service);
                  if (erStatusIsOk(&LocalStatus))
                    {
                      GET_PING_FAIL_COUNT(Service) = 0;
                      ZAB_SERVICE_ZNP_OPEN(Service).lastNetworkProcessorPingTimeMs = Time;
                      GET_OPEN_ITEM(Service) = ZAB_OPEN_CURRENT_ITEM_PING_SYS_VERSION;
                    }
                  /* If it fails it could be just a temporary full buffer scenario, or maybe that the glue (or something else) is broken.
                   * Increment fail count and backoff for some time */
                  else
                    {
                      GET_PING_FAIL_COUNT(Service)++;

                      printVendor(Service, "Network Processor Ping - Send Failed %d\n", GET_PING_FAIL_COUNT(Service));

                      if (GET_PING_FAIL_COUNT(Service) > ZAB_VENDOR_SERIAL_RETRIES)
                        {
                          erStatusSet(Status, erStatusGetError(&LocalStatus));
                          zabZnpOpen_CloseDueToError(Status, Service);
                        }
                      else
                        {
                          timeoutRemaining = ZAB_VENDOR_SERIAL_BACKOFF_MS;
                          ZAB_SERVICE_ZNP_OPEN(Service).lastNetworkProcessorPingTimeMs += ZAB_VENDOR_SERIAL_BACKOFF_MS;
                        }
                    }
                }
              else
                {
                  // If delayed lets have fairly fast work.
                  // Does not have to be incredibly quick as this is not a critical process.
                  timeoutRemaining = 100;
                }
            }
        }
    }
  return timeoutRemaining;
}

/******************************************************************************
 * Notify network processor model (if necessary)
 ******************************************************************************/
void zabZnpOpen_NetworkProcessorModelHandler(erStatus* Status, zabService* Service, zabNetworkProcessorModel NpModel)
{
  if ( (GET_OPEN_ITEM(Service) != ZAB_OPEN_CURRENT_ITEM_PING_SYS_VERSION) &&
       ( (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_SBL_VERSION) ||
         (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_SYS_RESET_TO_ZNP) ||
         (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_SBL_REOPEN) ||
         (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_SYS_VERSION) ||
         (GET_OPEN_ITEM(Service) == ZAB_OPEN_CURRENT_ITEM_SBL_APP_VERSION) ||
         (GET_OPEN_STATE(Service) == ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE) ) )
    {
      srvCoreGiveBack8(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_MODEL, (unsigned8)NpModel);
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
