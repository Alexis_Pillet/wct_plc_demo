/*******************************************************************************
  Filename    : endianness.h
  $Date       : 2013-05-13                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : Endianness functions for the SZL
*******************************************************************************/
#ifndef _ENDIANNESS_H_
#define _ENDIANNESS_H_
#include "szl_external.h"
#include "szl.h"


/**************************************************************************************************
* INCLUDES
**************************************************************************************************/

/**************************************************************************************************
* DEFINES
**************************************************************************************************/
/*
 * Global defines
*/
#define UINT16_HI(var) (((var) >> 8) & 0xFF)
#define UINT16_LO(var) ((var) & 0xFF)
#define UINT16_SWAP(var) ( UINT16_HI(var) | (UINT16_LO(var) << 8) )

#define UINT32_HI(var) (((var) >> 16) & 0xFFFF)
#define UINT32_LO(var) ((var) & 0xFFFF)
#define UINT32_SWAP(var) (UINT16_SWAP( UINT32_HI(var) ) | (UINT16_SWAP( UINT32_LO(var)) << 16) )

#if !SUPPORTS_64_BIT_DATATYPES
  #define UINT64_HI(var) ((var).hi)
  #define UINT64_LO(var) ((var).lo)
  #define UINT64_SWAP(var) UINT64_FROM_HI_LO( UINT32_SWAP(UINT64_HI(var)), UINT32_SWAP(UINT64_LO(var)) )
#else
  #define UINT64_HI(var) (((var) >> 32) & 0xFFFFFFFF)
  #define UINT64_LO(var) ((var) & 0xFFFFFFFF)
  #define UINT64_SWAP(var) (UINT32_SWAP( UINT64_HI(var) ) | (UINT32_SWAP( UINT64_LO(var)) << 32) )
#endif

#if !SUPPORTS_64_BIT_DATATYPES
  #define LARGE_DATATYPE_IS_ZERO(v) ( (v.hi == 0 && v.lo == 0))
#else
  #define LARGE_DATATYPE_IS_ZERO(v) (v == 0)
#endif

// simple data types
#if defined( SZL_LITTLE_ENDIANNESS )
#define UINT16_TO_LE(var) (var)
#define LE_TO_UINT16(var) (var)
#define UINT32_TO_LE(var) (var)
#define LE_TO_UINT32(var) (var)

#define UINT16_TO_BE(var) UINT16_SWAP(var)
#define BE_TO_UINT16(var) UINT16_SWAP(var)
#define UINT32_TO_BE(var) UINT32_SWAP(var)
#define BE_TO_UINT32(var) UINT32_SWAP(var)

#elif defined( SZL_BIG_ENDIANNESS )
#define UINT16_TO_LE(var) UINT16_SWAP(var)
#define LE_TO_UINT16(var) UINT16_SWAP(var)
#define UINT32_TO_LE(var) UINT32_SWAP(var)
#define LE_TO_UINT32(var) UINT32_SWAP(var)

#define UINT16_TO_BE(var) (var)
#define BE_TO_UINT16(var) (var)
#define UINT32_TO_BE(var) (var)
#define BE_TO_UINT32(var) (var)

#else
  #error "Endianness not configured in szl_external.h for the library"
#endif /* IS_LITTLE_ENDIANNES */

/*
 * Application data defines
 */

/**************************************************************************************************
* ENUMs, STRUCTs & UNIONs
**************************************************************************************************/

/**************************************************************************************************
* PROTOS
**************************************************************************************************/

#if !SUPPORTS_64_BIT_DATATYPES
szl_uint64 UINT64_FROM_HI_LO(szl_uint32 hi, szl_uint32 lo);
#endif

//specific data types
le_uint24 UINT24_TO_LE(szl_uint24 na);
szl_uint24 LE_TO_UINT24(le_uint24 le);
le_uint40 UINT40_TO_LE(szl_uint40 na);
szl_uint40 LE_TO_UINT40(le_uint40 le);
le_uint48 UINT48_TO_LE(szl_uint48 na);
szl_uint48 LE_TO_UINT48(le_uint48 le);
le_uint56 UINT56_TO_LE(szl_uint56 na);
szl_uint56 LE_TO_UINT56(le_uint56 le);
le_uint64 UINT64_TO_LE(szl_uint64 na);
szl_uint64 LE_TO_UINT64(le_uint64 le);

#endif /* _ENDIANNESS_H_ */
