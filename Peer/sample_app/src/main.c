/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : main.c
*    @version
*        $Rev: 3384 $
*    @last editor
*        $Author: a5089752 $
*    @date  
*        $Date:: 2017-05-31 13:49:31 +0900#$
* Description : 
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include "r_typedefs.h"
#include "r_config.h"
#include "r_bsp_api.h"
#include "r_stdio_api.h"
#include "r_timer_api.h"
#include "r_c3sap_api.h"
#include "r_demo_main.h"
#include "r_demo_tools.h"
#include "r_demo_app.h"

/******************************************************************************
Macro definitions
******************************************************************************/
/******************************************************************************
Typedef definitions
******************************************************************************/
/******************************************************************************
Exported global variables
******************************************************************************/
/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/
/******************************************************************************
Private global variables and functions
******************************************************************************/
/******************************************************************************
Functions
******************************************************************************/

/******************************************************************************
* Function Name: main
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void main(void)
{ 
    /* Set board type */
    R_BSP_SetBoardType(R_BOARD_G_CPX3);
    
    /* Configure clocks */
    R_BSP_SetClock();
    
    /* Activate LEDs on RSK */
    R_BSP_InitLeds();
    
    /* Set boot mode */
#if ( R_DEFINE_APP_BOOT == R_DEMO_APP_SROM_BOOT )
    R_BSP_SetBootMode(R_BSP_BOOT_SROM);
#else
    R_BSP_SetBootMode(R_BSP_BOOT_UART);
#endif
    
    /* Configure oneshot timer */
    if (R_BSP_ConfigureTimer(R_PCLKB_HZ, R_HW_TIMER_TICK_PERIOD, R_TIMER_IPR, R_BSP_CLK_OUT_OFF, &R_TIMER_Handle) != R_RESULT_SUCCESS)
    {
        /* Indicate error. */
        R_DEMO_LED(0u, R_DEMO_LED_ALERT);
        while(1){/**/};
    }
    
    /* STDIO init. */
    if (R_STDIO_Init() != R_RESULT_SUCCESS)
    {
        /* Indicate error. */
        R_DEMO_LED(0u, R_DEMO_LED_ALERT);
        while(1){/**/};
    }
    
    /* Initialize timer module */
    if (R_TIMER_Init(R_HW_TIMER_TICK_PERIOD) != R_RESULT_SUCCESS)
    {
        /* Indicate error. */
        R_DEMO_LED(0u, R_DEMO_LED_ALERT);
        while(1){/**/};
    }
    
    /* Start demo application menu. */
    R_DEMO_Main();
}
/******************************************************************************
   End of function  main
******************************************************************************/

