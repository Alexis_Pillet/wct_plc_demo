/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the configuration values for the ZNP Core.
 * 
 *   This file is to be copied and modified for a particular implementation of the 
 *   ZigBee Application Brick. It configures the queuing resource, buffer sizes, intervals,
 *   protocol defaults, etc. Most defined constants, if not defined here, will have
 *   defaults. Improper combination of definitions will generate compiler errors.
 * 
 *   For details and flow charts see ZEC007.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.00.02  30-Oct-13   MvdB   Start with a bit of a tidy up
 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 *                                 ARTF104098: Move mutex config to zabMutexConfigure.h
 * 002.000.008  15-Apr-15   MvdB   ARTF130931: Add ZAB_ASSERT macro in zabCoreConfigure.h
 *****************************************************************************/

#ifndef _ZAB_CORE_CONFIGURE_H_
#define _ZAB_CORE_CONFIGURE_H_

#include "em_assert.h"

/******************************************************************************
 *                      ******************************
 *                 *****     MODULE CONFIGURATION     *****
 *                      ******************************
 ******************************************************************************/


/* Define to enable EZ Mode Network Steering 
 * Default = Defined 
 * May be undefined by products that are never going to use EZ Mode Network Steering. */
#define ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING

/* Define to enable Forming of networks during EZ Mode Network Steering 
 * Default = Defined
 * May be undefined by products that only wishing to join networks, not form. */
#define ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING_FORM


/* Define to enable the Schneider ZigBee Library */
#define ZAB_CFG_ENABLE_SZL

/* Define to enable ZigBee Green Power support */
#define ZAB_CFG_ENABLE_GREEN_POWER

/* Define to enable support for Wireless Test Bench */
//#define ZAB_CFG_ENABLE_WIRELESS_TEST_BENCH

/******************************************************************************
 *                      ******************************
 *                 *****          CONSTANTS           *****
 *                      ******************************
 ******************************************************************************/ 

/* Time in seconds ZAB will use for Permit Join Time upon Network Permit Join action 
 * Default = 180 seconds */
#define ZAB_M_PERMIT_JOIN_TIME 180

/* Longest permissable time to set permit join for in one network command */
#define ZAB_M_NWK_MAX_PERMIT_JOIN_TIME 254

/******************************************************************************
 *                      ******************************
 *                 *****      DEVELOPER OPTIONS       *****
 *                      ******************************
 ******************************************************************************/ 

/* Enable tracking of in/out queue size/max usage/over runs */
//#define ZAB_CORE_M_ENABLE_QUEUE_PROFILING

/* Enable asserts in ZAB Core */
#define ZAB_ASSERT(expr) EFM_ASSERT(expr) // Asserts Enabled
//define ZAB_ASSERT(expr)              // Asserts disabled

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif
