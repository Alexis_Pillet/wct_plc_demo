/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the Radio Factory Test interface for ZNP.
 *   This is a Schneider specific extension to ZNP intended for use in NOVA.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *  00.00.02.00 03-Oct-13   MvdB   Original
 *  00.00.02.00 22-Oct-13   MvdB   Add parameters to zabZnpRftUtility_ConfigSendTest
 *  00.00.06.00 25-Jun-14   MvdB   Re-purpose API for server side Nova Factory Test
 * 002.002.001  19-Aug-15   MvdB   ARTF147630: Support stored FREQTUNE for industrialisation (SZNP 2.2.5)        
 *****************************************************************************/

#include "zabZnpService.h"


#ifdef ZAB_VND_CFG_ZNP_ENABLE_RFT

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

#define ZAB_SERVICE_ZNP_RFT( _srv )  (ZAB_SERVICE_VENDOR( (_srv) )->zabRftInfo)


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

typedef enum
{

  ZNPI_CMD_RFT_ENTER_TEST                = 0x00,
  ZNPI_CMD_RFT_CONFIG_CARRIER_TEST       = 0x01,
  ZNPI_CMD_RFT_CONFIG_RECEIVE_TEST       = 0x03,
  ZNPI_CMD_RFT_START_TEST                = 0x10,
  ZNPI_CMD_RFT_STOP_TEST                 = 0x11,
  ZNPI_CMD_RFT_PING_RSP_REQUEST          = 0x18,
  ZNPI_CMD_RFT_GET_FREQTUNE              = 0x20,
  ZNPI_CMD_RFT_SET_FREQTUNE              = 0x21,
  ZNPI_CMD_RFT_GET_STORED_FREQTUNE       = 0x22,
  ZNPI_CMD_RFT_SET_STORED_FREQTUNE       = 0x23,
  ZNPI_CMD_RFT_SEND_CONFIRM              = 0x82,
  ZNPI_CMD_RFT_PING_IND               = 0x83,
} ZNPI_CMD_RFT_COMMANDCODES;


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/  

/******************************************************************************
 * Initialise Radio Factory Test with a source ID.
 ******************************************************************************/
void zabZnpRftUtility_EnterTest(erStatus* Status, zabService* Service, unsigned32 SourceID)
{
  sapMsg* Msg = NULL;
  unsigned8* znp;
  unsigned8 data_length;

  data_length = 4;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);
  
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_RFT);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_RFT_ENTER_TEST);
  zabParseZNPSetLength(Status, Msg, data_length);
  
  znp = (ZAB_MSG_ZNP(Msg)->Data);
  COPY_OUT_32_BITS(znp, SourceID);
  
  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Configure the Carrier Test
 ******************************************************************************/
void zabZnpRftUtility_ConfigCarrierTest(erStatus* Status, 
                                        zabService* Service, 
                                        zab_bool Modulated,
                                        unsigned8 RfChannel,
                                        unsigned8 Power)
{
  sapMsg* Msg = NULL;
  unsigned8* znp;
  unsigned8 data_length;

  data_length = 3;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);
  
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_RFT);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_RFT_CONFIG_CARRIER_TEST);
  zabParseZNPSetLength(Status, Msg, data_length);
  
  znp = (ZAB_MSG_ZNP(Msg)->Data);
  *znp++ = (unsigned8)Modulated;
  *znp++ = RfChannel;
  *znp++ = Power;  
    
  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * Configure the Receive Test
 ******************************************************************************/
void zabZnpRftUtility_ConfigReceiveTest(erStatus* Status, 
                                        zabService* Service, 
                                        unsigned8 RfChannel)
{
  sapMsg* Msg = NULL;
  unsigned8* znp;
  unsigned8 data_length;

  data_length = 1;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);
  
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_RFT);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_RFT_CONFIG_RECEIVE_TEST);
  zabParseZNPSetLength(Status, Msg, data_length);
  
  znp = (ZAB_MSG_ZNP(Msg)->Data);
  *znp++ = RfChannel;
  
  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Start the previously configured test
 ******************************************************************************/
void zabZnpRftUtility_StartTest(erStatus* Status, 
                                zabService* Service)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;

  data_length = 0;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);
  
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_RFT);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_RFT_START_TEST);
  zabParseZNPSetLength(Status, Msg, data_length);
  
  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Stop the current test
 ******************************************************************************/
void zabZnpRftUtility_StopTest(erStatus* Status, 
                                zabService* Service)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;

  data_length = 0;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);
  
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_RFT);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_RFT_STOP_TEST);
  zabParseZNPSetLength(Status, Msg, data_length);
  
  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * Ping Response - For responses during receive test
 ******************************************************************************/
void zabZnpRftUtility_PingResponse(erStatus* Status, 
                                   zabService* Service,
                                   unsigned32 FrameCounter,
                                   unsigned8 Rssi)
{
  sapMsg* Msg = NULL;
  unsigned8* znp;
  unsigned8 data_length;

  data_length = 7;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);
  
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_RFT);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_RFT_PING_RSP_REQUEST);
  zabParseZNPSetLength(Status, Msg, data_length);
    
  znp = (ZAB_MSG_ZNP(Msg)->Data);
  *znp++ = data_length-1;  
  *znp++ = 0x02; // Test Command
  COPY_OUT_32_BITS(znp, FrameCounter); znp+=4;
  *znp++ = Rssi;
  
  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * Get Freq Tune Request
 ******************************************************************************/
void zabZnpRftUtility_GetFreqTuneRequest(erStatus* Status, 
                                         zabService* Service)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;

  data_length = 0;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);
  
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_RFT);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_RFT_GET_FREQTUNE);
  zabParseZNPSetLength(Status, Msg, data_length);
  
  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * Set Freq Tune Request
 ******************************************************************************/
void zabZnpRftUtility_SetFreqTuneRequest(erStatus* Status, 
                                         zabService* Service,
                                         unsigned8 FreqTune)
{
  sapMsg* Msg = NULL;
  unsigned8* znp;
  unsigned8 data_length;

  data_length = 1;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);
  
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_RFT);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_RFT_SET_FREQTUNE);
  zabParseZNPSetLength(Status, Msg, data_length);
    
  znp = (ZAB_MSG_ZNP(Msg)->Data);
  *znp = FreqTune;  
  
  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * Get Stored Freq Tune Request
 ******************************************************************************/
void zabZnpRftUtility_GetStoredFreqTuneRequest(erStatus* Status, 
                                               zabService* Service)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;

  data_length = 0;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);
  
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_RFT);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_RFT_GET_STORED_FREQTUNE);
  zabParseZNPSetLength(Status, Msg, data_length);
  
  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * Set Stored Freq Tune Request
 ******************************************************************************/
void zabZnpRftUtility_SetStoredFreqTuneRequest(erStatus* Status, 
                                               zabService* Service,
                                               unsigned8 FreqTune)
{
  sapMsg* Msg = NULL;
  unsigned8* znp;
  unsigned8 data_length;

  data_length = 1;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);
  
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_RFT);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_RFT_SET_STORED_FREQTUNE);
  zabParseZNPSetLength(Status, Msg, data_length);
    
  znp = (ZAB_MSG_ZNP(Msg)->Data);
  *znp = FreqTune;  
  
  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Register a callback for Standard (status only) Responses
 * The zabZnpRftUtility_ResponseType enumeration shows which command the response is for.
 ******************************************************************************/
void zabZnpRftUtility_RegisterStandardResponseHandler(erStatus* Status, 
                                                      zabService* Service,
                                                      zabZnpRftUtility_CB_StandardResponse Callback)
{  
  ZAB_SERVICE_ZNP_RFT(Service).StandardResponseCallback = Callback;
}

/******************************************************************************
 * Register a callback for Receive Indications
 ******************************************************************************/
void zabZnpRftUtility_RegisterPingIndicationHandler(erStatus* Status, 
                                                       zabService* Service,
                                                       zabZnpRftUtility_CB_PingInd Callback)
{  
  ZAB_SERVICE_ZNP_RFT(Service).ReceiveIndCallback = Callback;
}


/******************************************************************************
 * Register a callback for Get Freq Tune Response
 ******************************************************************************/
void zabZnpRftUtility_RegisterGetFreqTuneResponseHandler(erStatus* Status, 
                                                         zabService* Service,
                                                         zabZnpRftUtility_CB_GetFreqTuneResponse Callback)
{  
  ZAB_SERVICE_ZNP_RFT(Service).GetFreqTuneResponseCallback = Callback;
}


/******************************************************************************
 * Register a callback for Get Stored Freq Tune Response
 ******************************************************************************/
void zabZnpRftUtility_RegisterGetStoredFreqTuneResponseHandler(erStatus* Status, 
                                                               zabService* Service,
                                                               zabZnpRftUtility_CB_GetFreqTuneResponse Callback)
{  
  ZAB_SERVICE_ZNP_RFT(Service).GetStoredFreqTuneResponseCallback = Callback;
}


/******************************************************************************
 *                      ******************************
 *               ***** ZAB PRIVATE FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/  

/******************************************************************************
 * Process all messages in the RFT subsystem
 ******************************************************************************/
void zabZnpRftUtility_ProcessMsg(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8 cmd;
  unsigned8* data;
  unsigned8 length;
  
  zabParseZNPGetCommandID(Status, Message, &cmd);
  data = NULL;
  zabParseZNPGetData(Status, Message, &length, (unsigned8 **)&data);

  switch (cmd)
    {
      case ZNPI_CMD_RFT_ENTER_TEST:
      case ZNPI_CMD_RFT_CONFIG_CARRIER_TEST:
      case ZNPI_CMD_RFT_CONFIG_RECEIVE_TEST:
      case ZNPI_CMD_RFT_START_TEST:
      case ZNPI_CMD_RFT_STOP_TEST:
      case ZNPI_CMD_RFT_PING_RSP_REQUEST:
      case ZNPI_CMD_RFT_SET_FREQTUNE:
      case ZNPI_CMD_RFT_SET_STORED_FREQTUNE:
        if ((length > 0) && (data != NULL) && (ZAB_SERVICE_ZNP_RFT(Service).StandardResponseCallback != NULL))
          {
            ZAB_SERVICE_ZNP_RFT(Service).StandardResponseCallback(Service, 
                                                                  (zabZnpRftUtility_ResponseType)cmd, /* ResponseType */
                                                                  *data);                             /* Status */
          }
        break;
        
      case ZNPI_CMD_RFT_PING_IND:
        if ((length >= 10) && (data != NULL) && (ZAB_SERVICE_ZNP_RFT(Service).ReceiveIndCallback != NULL))
          {
            ZAB_SERVICE_ZNP_RFT(Service).ReceiveIndCallback(Service, 
                                                            *data,                    /* Status */
                                                            COPY_IN_32_BITS(data+1),  /* RxTimeStamp */
                                                            *(data+5),                /* RSSI */
                                                            COPY_IN_32_BITS(data+6)); /* FrameCounter */
          }
        break;
        
      case ZNPI_CMD_RFT_GET_FREQTUNE:
        if ((length >= 2) && (data != NULL) && (ZAB_SERVICE_ZNP_RFT(Service).GetFreqTuneResponseCallback != NULL))
          {
            ZAB_SERVICE_ZNP_RFT(Service).GetFreqTuneResponseCallback(Service, 
                                                                     data[0],    /* Status */
                                                                     data[1]);   /* FreqTune */
          }
        break;
        
      case ZNPI_CMD_RFT_GET_STORED_FREQTUNE:
        if ((length >= 2) && (data != NULL) && (ZAB_SERVICE_ZNP_RFT(Service).GetStoredFreqTuneResponseCallback != NULL))
          {
            ZAB_SERVICE_ZNP_RFT(Service).GetStoredFreqTuneResponseCallback(Service, 
                                                                           data[0],    /* Status */
                                                                           data[1]);   /* FreqTune */
          }
        break;

      default:
          printError(Service, "zabZnpRftService: WARNING! Unknown ZNP message 0x%02X\n", cmd);
          break;
    }
}

#endif // ZAB_VND_CFG_ZNP_ENABLE_RFT
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
