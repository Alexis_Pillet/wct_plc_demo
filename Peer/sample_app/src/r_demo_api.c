/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_api.c
*    @version
*        $Rev: 3384 $
*    @last editor
*        $Author: a5089752 $
*    @date
*        $Date:: 2017-05-31 13:49:31 +0900#$
* Description :
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include "r_typedefs.h"
#include "r_stdio_api.h"
#include "r_byte_swap.h"

/* g3 part */
#include "r_c3sap_api.h"


/* app part */
#include "r_demo_tools.h"
#include "r_demo_api.h"
#include "r_demo_app.h"
#include "r_demo_app_thread.h"
#include "r_demo_status2text.h"

/******************************************************************************
Macro definitions
******************************************************************************/
    #define R_VOLATILE  volatile

/******************************************************************************
Typedef definitions
******************************************************************************/
/******************************************************************************
Exported global variables
******************************************************************************/
extern r_demo_config_t             g_demo_config;
extern volatile r_demo_g3_cb_str_t g_g3cb[R_G3_CH_MAX];

/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/
/******************************************************************************
Private global variables and functions
******************************************************************************/


/******************************************************************************
Functions
******************************************************************************/


/*===========================================================================*/
/*    G3CTRL APIs                                                            */
/*===========================================================================*/


/******************************************************************************
* Function Name: R_DEMO_G3SetConfig
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_G3SetConfig (uint8_t chId, r_g3_set_config_req_t * config,
                               r_g3_set_config_cnf_t ** cnf)
{
    r_result_t                         status;
    R_VOLATILE r_g3_set_config_cnf_t * cfgCfm = (R_VOLATILE r_g3_set_config_cnf_t *)&g_g3cb[chId].setConfig;
    *cnf = (r_g3_set_config_cnf_t *)cfgCfm;

    cfgCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Setting AdpConfig ");
    }

    status = R_G3_SetConfigReq (chId, config);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of R_ADP_AdpmSet */
        while (R_DEMO_G3_STATUS_NOT_SET == cfgCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_G3_STATUS_SUCCESS == cfgCfm->status)
            {
                R_STDIO_Printf ("success. \n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (0, cfgCfm->status), cfgCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_G3SetConfig */
/******************************************************************************
   End of function  R_DEMO_G3SetConfig
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_G3GetInfo
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_G3GetInfo (uint8_t chId, r_g3_get_info_req_t * req,
                             r_g3_get_info_cnf_t ** cnf)
{
    r_result_t                       status;
    R_VOLATILE r_g3_get_info_cnf_t * getInfoCfm = (R_VOLATILE r_g3_get_info_cnf_t *)&g_g3cb[chId].getInfo;
    *cnf = (r_g3_get_info_cnf_t *)getInfoCfm;

    getInfoCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> G3-GetInfo requesting... ");
    }

    status = R_G3_GetInfoReq (chId, req);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of R_ADP_AdpmSet */
        while (R_DEMO_G3_STATUS_NOT_SET == getInfoCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_G3_STATUS_SUCCESS == getInfoCfm->status)
            {
                R_STDIO_Printf ("success. \n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (0, getInfoCfm->status), getInfoCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_G3GetInfo */
/******************************************************************************
   End of function  R_DEMO_G3GetInfo
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_G3ClearInfo
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_G3ClearInfo (uint8_t chId, r_g3_clear_info_req_t * req,
                               r_g3_clear_info_cnf_t ** cnf)
{
    r_result_t                         status;
    R_VOLATILE r_g3_clear_info_cnf_t * clrInfoCfm = (R_VOLATILE r_g3_clear_info_cnf_t *)&g_g3cb[chId].clrInfo;
    *cnf = (r_g3_clear_info_cnf_t *)clrInfoCfm;

    clrInfoCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> G3-ClearInfo requesting... ");
    }

    status = R_G3_ClearInfoReq (chId, req);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of R_ADP_AdpmSet */
        while (R_DEMO_G3_STATUS_NOT_SET == clrInfoCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_G3_STATUS_SUCCESS == clrInfoCfm->status)
            {
                R_STDIO_Printf ("success. \n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (0, clrInfoCfm->status), clrInfoCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_G3ClearInfo */
/******************************************************************************
   End of function  R_DEMO_G3ClearInfo
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_DeInit
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_DeInit (uint8_t chId)
{
    r_result_t status;

    R_STDIO_Printf ("\n -> CPX3 DeInit (ch%d)...", chId);

    status = R_G3_Deinit (chId, 2000);
    if (R_RESULT_SUCCESS != status)
    {
        return R_RESULT_FAILED;
    }
    else
    {
        R_STDIO_Printf ("done.");
        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_DeInit */
/******************************************************************************
   End of function  R_DEMO_DeInit
******************************************************************************/


/*===========================================================================*/
/*    MAC APIs                                                               */
/*===========================================================================*/

/******************************************************************************
* Function Name: R_DEMO_MacInit
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_MacInit (uint8_t chId)
{
    r_g3_init_req_t req;
    r_g3_callback_t callBack;
    r_result_t      status;

    R_STDIO_Printf ("\n -> CPX3 Init as MAC mode(ch%d)...", chId);

    req.g3mode = R_G3_MODE_MAC;
    req.init.mac.neighbourTableSize = R_DEMO_G3MAC_NEIGBOUR_TABLE_SIZE;
    req.init.mac.deviceTableSize = R_DEMO_G3MAC_DEVICE_TABLE_SIZE;
    req.init.mac.panDescriptorNum = R_DEMO_ADP_MAX_PAN_DESCRIPTORS;

    if (R_DEMO_InitMacCallBack (chId, &callBack) != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }

    status = R_G3_Init (chId, &callBack, &req, 2000);
    if (R_RESULT_SUCCESS != status)
    {
        return R_RESULT_FAILED;
    }
    else
    {
        R_DEMO_LED (chId, R_DEMO_LED_BOOT);
        R_STDIO_Printf ("done.");
        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_MacInit */
/******************************************************************************
   End of function  R_DEMO_MacInit
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_McpsData
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_McpsData (uint8_t chId, const r_g3mac_mcps_data_req_t * mcpsDataReq,
                            r_g3mac_mcps_data_cnf_t ** cnf)
{
    r_result_t                           status;
    R_VOLATILE r_g3mac_mcps_data_cnf_t * mcpsDataCfm = (R_VOLATILE r_g3mac_mcps_data_cnf_t *)&g_g3cb[chId].mcpsDataCnf;
    *cnf = (r_g3mac_mcps_data_cnf_t *)mcpsDataCfm;

    mcpsDataCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Sending MAC Data frame...");
    }

    status = R_G3MAC_McpsDataReq (chId, (r_g3mac_mcps_data_req_t *)mcpsDataReq);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!\n");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of R_ADP_AdpmSet */
        while (R_DEMO_G3_STATUS_NOT_SET == mcpsDataCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_G3MAC_STATUS_SUCCESS == mcpsDataCfm->status)
            {
                R_STDIO_Printf ("success.\n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_MAC, mcpsDataCfm->status), mcpsDataCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_McpsData */
/******************************************************************************
   End of function  R_DEMO_McpsData
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_MlmeReset
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_MlmeReset (uint8_t chId, const r_g3mac_mlme_reset_req_t * mlmeResetReq,
                             r_g3mac_mlme_reset_cnf_t ** cnf)
{
    r_result_t                            status;
    R_VOLATILE r_g3mac_mlme_reset_cnf_t * mlmeResetCfm = (R_VOLATILE r_g3mac_mlme_reset_cnf_t *)&g_g3cb[chId].mlmeResetCnf;
    *cnf = (r_g3mac_mlme_reset_cnf_t *)mlmeResetCfm;

    mlmeResetCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Resetting MAC device...");
    }

    status = R_G3MAC_MlmeResetReq (chId, (r_g3mac_mlme_reset_req_t *)mlmeResetReq);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!\n");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of R_ADP_AdpmReset */
        while (R_DEMO_G3_STATUS_NOT_SET == mlmeResetCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_G3MAC_STATUS_SUCCESS == mlmeResetCfm->status)
            {
                R_STDIO_Printf ("success.\n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_MAC, mlmeResetCfm->status), mlmeResetCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_MlmeReset */
/******************************************************************************
   End of function  R_DEMO_MlmeReset
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_MlmeStart
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_MlmeStart (uint8_t chId, const r_g3mac_mlme_start_req_t * mlmeStartReq,
                             r_g3mac_mlme_start_cnf_t ** cnf)
{
    r_result_t                            status;
    R_VOLATILE r_g3mac_mlme_start_cnf_t * mlmeStartCfm = (R_VOLATILE r_g3mac_mlme_start_cnf_t *)&g_g3cb[chId].mlmeStartCnf;
    *cnf = (r_g3mac_mlme_start_cnf_t *)mlmeStartCfm;

    mlmeStartCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Startting MAC device...");
    }

    status = R_G3MAC_MlmeStartReq (chId, (r_g3mac_mlme_start_req_t *)mlmeStartReq);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!\n");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of R_ADP_AdpmStart */
        while (R_DEMO_G3_STATUS_NOT_SET == mlmeStartCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_G3MAC_STATUS_SUCCESS == mlmeStartCfm->status)
            {
                R_STDIO_Printf ("success.\n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_MAC, mlmeStartCfm->status), mlmeStartCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_MlmeStart */
/******************************************************************************
   End of function  R_DEMO_MlmeStart
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_MlmeScan
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_MlmeScan (uint8_t chId, const r_g3mac_mlme_scan_req_t * mlmeScanReq,
                            r_g3mac_mlme_scan_cnf_t ** cnf)
{
    uint16_t                             i;
    r_result_t                           status;
    R_VOLATILE r_g3mac_mlme_scan_cnf_t * mlmeScanCfm = (R_VOLATILE r_g3mac_mlme_scan_cnf_t *)&g_g3cb[chId].mlmeScanCnf;
    *cnf = (r_g3mac_mlme_scan_cnf_t *)mlmeScanCfm;

    mlmeScanCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Scanting MAC device...");
    }

    status = R_G3MAC_MlmeScanReq (chId, (r_g3mac_mlme_scan_req_t *)mlmeScanReq);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!\n");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of R_ADP_AdpmScan */
        while (R_DEMO_G3_STATUS_NOT_SET == mlmeScanCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_G3MAC_STATUS_SUCCESS == mlmeScanCfm->status)
            {
                /* Show scan results */
                if (0 != mlmeScanCfm->panCount)
                {
                    R_STDIO_Printf ("\n\n---------------Active network(s) found---------------------");

                    for (i = 0; i < mlmeScanCfm->panCount; i++)
                    {
                        R_STDIO_Printf ("\n %d - PAN ID: 0x%.4X Short address: 0x%.4X RC Coordinator: 0x%.4X LQI: 0x%.2X", i, mlmeScanCfm->pPanList[i].panId,
                                        R_BYTE_ArrToUInt16 (mlmeScanCfm->pPanList[i].address),
                                        mlmeScanCfm->pPanList[i].rcCoord,
                                        mlmeScanCfm->pPanList[i].linkQuality);
                    }

                    R_STDIO_Printf ("\n");
                }
                else
                {
                    R_STDIO_Printf ("\n -> No active network(s) found.\n");
                }
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_MAC, mlmeScanCfm->status), mlmeScanCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_MlmeScan */
/******************************************************************************
   End of function  R_DEMO_MlmeScan
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_MlmeSet
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_MlmeSet (uint8_t chId, const r_g3mac_mlme_set_req_t * mlmeSetReq,
                           r_g3mac_mlme_set_cnf_t ** cnf)
{
    uint8_t                             len;
    r_result_t                          status;
    R_VOLATILE r_g3mac_mlme_set_cnf_t * mlmeSetCfm = (R_VOLATILE r_g3mac_mlme_set_cnf_t *)&g_g3cb[chId].mlmeSetCnf;
    *cnf = (r_g3mac_mlme_set_cnf_t *)mlmeSetCfm;

    mlmeSetCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Setting MAC PIB %s(0x%.4X) Index: %d...", ibid_to_text (R_G3_MODE_MAC, mlmeSetReq->pibAttributeId, &len), mlmeSetReq->pibAttributeId, mlmeSetReq->pibAttributeIndex);
    }

    status = R_G3MAC_MlmeSetReq (chId, (r_g3mac_mlme_set_req_t *)mlmeSetReq);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!\n");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of R_ADP_AdpmSet */
        while (R_DEMO_G3_STATUS_NOT_SET == mlmeSetCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_G3MAC_STATUS_SUCCESS == mlmeSetCfm->status)
            {
                R_STDIO_Printf ("success.\n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_MAC, mlmeSetCfm->status), mlmeSetCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_MlmeSet */
/******************************************************************************
   End of function  R_DEMO_MlmeSet
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_MlmeGet
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_MlmeGet (uint8_t chId, const r_g3mac_mlme_get_req_t * mlmeGetReq,
                           r_g3mac_mlme_get_cnf_t ** cnf)
{
    uint8_t                             len;
    r_result_t                          status;
    R_VOLATILE r_g3mac_mlme_get_cnf_t * mlmeGetCfm = (R_VOLATILE r_g3mac_mlme_get_cnf_t *)&g_g3cb[chId].mlmeGetCnf;
    *cnf = (r_g3mac_mlme_get_cnf_t *)mlmeGetCfm;

    mlmeGetCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Getting MAC PIB %s(0x%.4X) Index: %d...", ibid_to_text (R_G3_MODE_MAC, mlmeGetReq->pibAttributeId, &len), mlmeGetReq->pibAttributeId, mlmeGetReq->pibAttributeIndex);
    }

    status = R_G3MAC_MlmeGetReq (chId, (r_g3mac_mlme_get_req_t *)mlmeGetReq);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed.\n");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of R_ADP_AdpmGet */
        while (R_DEMO_G3_STATUS_NOT_SET == mlmeGetCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_ADP_STATUS_SUCCESS == mlmeGetCfm->status)
            {
                R_STDIO_Printf ("success.\n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_MAC, mlmeGetCfm->status), mlmeGetCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_MlmeGet */
/******************************************************************************
   End of function  R_DEMO_MlmeGet
******************************************************************************/




/******************************************************************************
* Function Name: R_DEMO_MlmeGetWrap
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_g3mac_status_t R_DEMO_MlmeGetWrap (uint8_t chId, uint16_t id, uint16_t index, uint8_t * val)
{
    r_g3mac_mlme_get_req_t   req;
    r_g3mac_mlme_get_cnf_t * mlmeGetCfm;

    req.pibAttributeId = id;
    req.pibAttributeIndex = index;

    if (R_DEMO_MlmeGet (chId, &req, &mlmeGetCfm) == R_RESULT_SUCCESS)
    {
        R_memcpy (val, mlmeGetCfm->pibAttributeValue, sizeof (mlmeGetCfm->pibAttributeValue));
        return (r_g3mac_status_t)mlmeGetCfm->status;
    }
    else
    {
        return (r_g3mac_status_t)R_DEMO_G3_STATUS_FAILED;
    }
} /* R_DEMO_MlmeGetWrap */
/******************************************************************************
   End of function  R_DEMO_MlmeGetWrap
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_MlmeSetWrap
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_g3mac_status_t R_DEMO_MlmeSetWrap (uint8_t chId, uint16_t id, uint16_t index, uint8_t * val)
{
    r_g3mac_mlme_set_req_t   req;
    r_g3mac_mlme_set_cnf_t * pCnf;

    req.pibAttributeId = id;
    req.pibAttributeIndex = index;
    req.pibAttributeValue = val;
    if (R_DEMO_MlmeSet (chId, &req, &pCnf) == R_RESULT_SUCCESS)
    {
        return (r_g3mac_status_t)pCnf->status;
    }
    else
    {
        return (r_g3mac_status_t)R_DEMO_G3_STATUS_FAILED;
    }
}
/******************************************************************************
   End of function  R_DEMO_MlmeSetWrap
******************************************************************************/




/*===========================================================================*/
/*    ADP APIs                                                               */
/*===========================================================================*/

/******************************************************************************
* Function Name: R_DEMO_AdpInit
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpInit (uint8_t chId)
{
    r_g3_init_req_t req;
    r_g3_callback_t callBack;
    r_result_t      status;


    R_STDIO_Printf ("\n -> CPX3 Init as ADP mode(ch%d)...", chId);

    req.g3mode = R_G3_MODE_ADP;
    req.init.adp.adpdBuffNum = R_DEMO_ADP_ADPD_DATA_QUEUE_SIZE;
    req.init.adp.routeTableSize = R_DEMO_ADP_ROUTING_TABLE_SIZE;
    req.init.adp.neighbourTableSize = R_DEMO_G3MAC_NEIGBOUR_TABLE_SIZE;
    req.init.adp.deviceTableSize = R_DEMO_G3MAC_DEVICE_TABLE_SIZE;
    req.init.adp.panDescriptorNum = R_DEMO_ADP_MAX_PAN_DESCRIPTORS;
    req.init.adp.routeType = g_demo_config.routeType;

    if (R_DEMO_InitAdpCallBack (chId, &callBack) != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }

    status = R_G3_Init (chId, &callBack, &req, 2000);

    if (R_RESULT_SUCCESS != status)
    {
        return R_RESULT_FAILED;
    }
    else
    {
        R_STDIO_Printf ("done.");
        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_AdpInit */
/******************************************************************************
   End of function  R_DEMO_AdpInit
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmLbp
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmLbp (uint8_t chId, const r_adp_adpm_lbp_req_t * lbpReq,
                           r_adp_adpm_lbp_cnf_t ** cnf)
{
    r_result_t                        status;
    R_VOLATILE r_adp_adpm_lbp_cnf_t * lbpCfm = (R_VOLATILE r_adp_adpm_lbp_cnf_t *)&g_g3cb[chId].adpmLbpCnf;
    *cnf = (r_adp_adpm_lbp_cnf_t *)lbpCfm;

    lbpCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Sending LBP frame...");
    }

    /* Invoke ADPM-LBP.request */
    status = R_ADP_AdpmLbpReq (chId, (r_adp_adpm_lbp_req_t *)lbpReq);
    if (R_RESULT_SUCCESS != status)
    {
        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for ADPM-LBP to finish. */
        while (R_DEMO_G3_STATUS_NOT_SET == lbpCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_ADP_STATUS_SUCCESS == lbpCfm->status)
            {
                R_STDIO_Printf ("success. NSDU handle: 0x%.2X \n", lbpCfm->nsduHandle);
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_ADP, lbpCfm->status), lbpCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_AdpmLbp */
/******************************************************************************
   End of function  R_DEMO_AdpmLbp
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AdpmNetworkLeave
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmNetworkLeave (uint8_t chId, r_adp_adpm_network_leave_cnf_t ** cnf)
{
    r_result_t                                  status;
    R_VOLATILE r_adp_adpm_network_leave_cnf_t * nwlCfm = (R_VOLATILE r_adp_adpm_network_leave_cnf_t *)&g_g3cb[chId].adpmNetworkLeaveCnf;
    *cnf = (r_adp_adpm_network_leave_cnf_t *)nwlCfm;

    nwlCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Leaving network...");
    }

    status = R_ADP_AdpmNetworkLeaveReq (chId);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for network leave to finish */
        while (R_DEMO_G3_STATUS_NOT_SET == nwlCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_ADP_STATUS_SUCCESS == nwlCfm->status)
            {
                R_STDIO_Printf ("success.\n");
                R_DEMO_LED (chId, R_DEMO_LED_BOOT);
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_ADP, nwlCfm->status), nwlCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_AdpmNetworkLeave */
/******************************************************************************
   End of function  R_DEMO_AdpmNetworkLeave
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmNetworkJoin
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmNetworkJoin (uint8_t chId, const r_adp_adpm_network_join_req_t * nwjReq,
                                   r_adp_adpm_network_join_cnf_t ** cnf)
{
    r_result_t                                 status;
    R_VOLATILE r_adp_adpm_network_join_cnf_t * nwjCfm = (R_VOLATILE r_adp_adpm_network_join_cnf_t *)&g_g3cb[chId].adpmNetworkJoinCnf;
    *cnf = (r_adp_adpm_network_join_cnf_t *)nwjCfm;

    nwjCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Joining PAN with PanID:0x%.4X LBA:0x%02X%02X...", nwjReq->panId, nwjReq->lbaAddress[0], nwjReq->lbaAddress[1]);
    }

    status = R_ADP_AdpmNetworkJoinReq (chId, (r_adp_adpm_network_join_req_t *)nwjReq);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for network leave to finish */
        while (R_DEMO_G3_STATUS_NOT_SET == nwjCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_ADP_STATUS_SUCCESS == nwjCfm->status)
        {
            if (R_TRUE == g_demo_config.verboseEnabled)
            {
                R_STDIO_Printf ("success.");
                R_STDIO_Printf (" Network Address: 0x%.4X\n", R_BYTE_ArrToUInt16 ((const uint8_t *)nwjCfm->networkAddress));
            }
            R_DEMO_LED (chId, R_DEMO_LED_IDLE);
        }
        else
        {
            if (R_TRUE == g_demo_config.verboseEnabled)
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_ADP, nwjCfm->status), nwjCfm->status);
                return R_RESULT_FAILED;
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_AdpmNetworkJoin */
/******************************************************************************
   End of function  R_DEMO_AdpmNetworkJoin
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmDiscovery
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmDiscovery (uint8_t chId, const r_adp_adpm_discovery_req_t * disReq,
                                 r_adp_adpm_discovery_cnf_t ** cnf)
{
    uint16_t                                i;
    r_result_t                              status;
    R_VOLATILE r_adp_adpm_discovery_cnf_t * disCfm = (R_VOLATILE r_adp_adpm_discovery_cnf_t *)&g_g3cb[chId].adpmDiscoveryCnf;
    *cnf = (r_adp_adpm_discovery_cnf_t *)disCfm;

    disCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Scanning for active networks...");
    }

    /* Start scanning */
    status = R_ADP_AdpmDiscoveryReq (chId, (r_adp_adpm_discovery_req_t *)disReq);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for the scan to finish */
        while (R_DEMO_G3_STATUS_NOT_SET == disCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_ADP_STATUS_SUCCESS == disCfm->status)
            {
                /* Show scan results */
                if (0 != disCfm->PANCount)
                {
                    R_STDIO_Printf ("\n\n---------------Active network(s) found---------------------");

                    for (i = 0; i < disCfm->PANCount; i++)
                    {
                        R_STDIO_Printf ("\n %d - PAN ID: 0x%.4X Short address: 0x%.4X RC Coordinator: 0x%.4X LQI: 0x%.2X", i, disCfm->PANDescriptor[i].panId,
                                        R_BYTE_ArrToUInt16 (disCfm->PANDescriptor[i].address),
                                        disCfm->PANDescriptor[i].rcCoord,
                                        disCfm->PANDescriptor[i].linkQuality);
                    }

                    R_STDIO_Printf ("\n");
                }
                else
                {
                    R_STDIO_Printf ("\n -> No active network(s) found.\n");
                }
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_ADP, disCfm->status), disCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_AdpmDiscovery */
/******************************************************************************
   End of function  R_DEMO_AdpmDiscovery
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AdpdData
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpdData (uint8_t chId, const r_adp_adpd_data_req_t * dataReq,
                            r_adp_adpd_data_cnf_t ** cnf)
{
    r_result_t                         status;
    R_VOLATILE r_adp_adpd_data_cnf_t * dataCfm = (R_VOLATILE r_adp_adpd_data_cnf_t *)&g_g3cb[chId].adpdDataCnf;
    *cnf = (r_adp_adpd_data_cnf_t *)dataCfm;

    dataCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Sending data frame..., size: %d   ", ( dataReq->nsduLength - 48 ));
    }

    R_DEMO_WaitcnfTimerOn ();

    /* Invoke ADPD-DATA.request */
    status = R_ADP_AdpdDataReq (chId, (r_adp_adpd_data_req_t *)dataReq);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }
        R_DEMO_WaitcnfTimerOff ();
        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for ADPD-DATA to finish */
        while (
            (R_DEMO_G3_STATUS_NOT_SET == dataCfm->status) &&
            (R_DEMO_WaitcnfTimeout () == R_FALSE))
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_ADP_STATUS_SUCCESS == dataCfm->status)
            {
                R_STDIO_Printf ("success. NSDU handle: 0x%.2X \n", dataCfm->nsduHandle);
            }
            else
            {
                if (R_DEMO_WaitcnfTimeout ())
                {
                    R_STDIO_Printf ("failed. by timeout \n");
                    R_DEMO_WaitcnfTimerOff ();
                    return R_RESULT_TIMEOUT;
                }
                else
                {
                    R_STDIO_Printf ("failed.Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_ADP, dataCfm->status), dataCfm->status);
                    R_DEMO_WaitcnfTimerOff ();
                    return R_RESULT_FAILED;
                }
            }
        }
        R_DEMO_WaitcnfTimerOff ();
        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_AdpdData */
/******************************************************************************
   End of function  R_DEMO_AdpdData
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmNetworkStart
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmNetworkStart (uint8_t chId, const r_adp_adpm_network_start_req_t * nwsReq,
                                    r_adp_adpm_network_start_cnf_t ** cnf)
{
    r_result_t                                  status;
    R_VOLATILE r_adp_adpm_network_start_cnf_t * nwsCfm = (R_VOLATILE r_adp_adpm_network_start_cnf_t *)&g_g3cb[chId].adpmNetworkStartCnf;
    *cnf = (r_adp_adpm_network_start_cnf_t *)nwsCfm;

    nwsCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Starting PAN with ID: 0x%.4X...",  nwsReq->panId & 0xFCFF);
    }

    status = R_ADP_AdpmNetworkStartReq (chId, (r_adp_adpm_network_start_req_t *)nwsReq);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of R_ADP_AdpmNetworkStart */
        while (R_DEMO_G3_STATUS_NOT_SET == nwsCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_ADP_STATUS_SUCCESS == nwsCfm->status)
        {
            if (R_TRUE == g_demo_config.verboseEnabled)
            {
                R_STDIO_Printf ("success.\n");
            }
            R_DEMO_LED (chId, R_DEMO_LED_IDLE);
        }
        else
        {
            if (R_TRUE == g_demo_config.verboseEnabled)
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_ADP, nwsCfm->status), nwsCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_AdpmNetworkStart */
/******************************************************************************
   End of function  R_DEMO_AdpmNetworkStart
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmSet
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmSet (uint8_t chId, const r_adp_adpm_set_req_t * setReq,
                           r_adp_adpm_set_cnf_t ** cnf)
{
    r_result_t                        status;
    uint8_t                           len;
    R_VOLATILE r_adp_adpm_set_cnf_t * setCfm = (R_VOLATILE r_adp_adpm_set_cnf_t *)&g_g3cb[chId].adpmSetCnf;
    *cnf = (r_adp_adpm_set_cnf_t *)setCfm;

    setCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Setting ADP IB %s(0x%.2X) Index: %d...", ibid_to_text (R_G3_MODE_ADP, setReq->aibAttributeId, &len), setReq->aibAttributeId, setReq->aibAttributeIndex);
    }

    status = R_ADP_AdpmSetReq (chId, (r_adp_adpm_set_req_t *)setReq);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of R_ADP_AdpmSet */
        while (R_DEMO_G3_STATUS_NOT_SET == setCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_ADP_STATUS_SUCCESS == setCfm->status)
            {
                R_STDIO_Printf ("success.\n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_ADP, setCfm->status), setCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_AdpmSet */
/******************************************************************************
   End of function  R_DEMO_AdpmSet
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AdpmGet
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmGet (uint8_t chId, const r_adp_adpm_get_req_t * getReq,
                           r_adp_adpm_get_cnf_t ** cnf)
{
    uint8_t                           len;
    r_result_t                        status;
    R_VOLATILE r_adp_adpm_get_cnf_t * getCfm = (R_VOLATILE r_adp_adpm_get_cnf_t *)&g_g3cb[chId].adpmGetCnf;
    *cnf = (r_adp_adpm_get_cnf_t *)getCfm;

    getCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Getting ADP IB %s(0x%.2X) Index: %d...", ibid_to_text (R_G3_MODE_ADP, getReq->aibAttributeId, &len), getReq->aibAttributeId, getReq->aibAttributeIndex);
    }

    status = R_ADP_AdpmGetReq (chId, (r_adp_adpm_get_req_t *)getReq);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of R_ADP_AdpmGet */
        while (R_DEMO_G3_STATUS_NOT_SET == getCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_ADP_STATUS_SUCCESS == getCfm->status)
            {
                R_STDIO_Printf ("success.\n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_ADP, getCfm->status), getCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_AdpmGet */
/******************************************************************************
   End of function  R_DEMO_AdpmGet
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AdpmRouteDiscovery
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmRouteDiscovery (uint8_t chId, const r_adp_adpm_route_disc_req_t * rdisReq,
                                      r_adp_adpm_route_disc_cnf_t ** cnf)
{
    r_result_t                               status;
    R_VOLATILE r_adp_adpm_route_disc_cnf_t * rdisCfm = (R_VOLATILE r_adp_adpm_route_disc_cnf_t *)&g_g3cb[chId].adpmRouteDiscoveryCnf;
    *cnf = (r_adp_adpm_route_disc_cnf_t *)rdisCfm;

    rdisCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Starting route discovery...");
    }

    status = R_ADP_AdpmRouteDiscoveryReq (chId, (r_adp_adpm_route_disc_req_t *)rdisReq);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of Route Discovery */
        while (R_DEMO_G3_STATUS_NOT_SET == rdisCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_ADP_STATUS_SUCCESS == rdisCfm->status)
            {
                R_STDIO_Printf ("success.\n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_ADP, rdisCfm->status), rdisCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_AdpmRouteDiscovery */
/******************************************************************************
   End of function  R_DEMO_AdpmRouteDiscovery
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmPathDiscovery
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmPathDiscovery (uint8_t chId, const r_adp_adpm_path_discovery_req_t * pdisReq,
                                     r_adp_adpm_path_discovery_cnf_t ** cnf)
{
    uint8_t                                      i;
    r_result_t                                   status;
    R_VOLATILE r_adp_adpm_path_discovery_cnf_t * pdisCfm = (R_VOLATILE r_adp_adpm_path_discovery_cnf_t *)&g_g3cb[chId].adpmPathDiscoveryCnf;
    *cnf = (r_adp_adpm_path_discovery_cnf_t *)pdisCfm;

    pdisCfm->status = R_DEMO_G3_STATUS_NOT_SET;



    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Starting path discovery...");
    }

    status = R_ADP_AdpmPathDiscoveryReq (chId, (r_adp_adpm_path_discovery_req_t *)pdisReq);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of Path Discovery */
        while (R_DEMO_G3_STATUS_NOT_SET == pdisCfm->status)
        {
            /* wait */
        }

        if (R_ADP_STATUS_SUCCESS == pdisCfm->status)
        {
            R_STDIO_Printf ("success.");
        }
        else
        {
            R_STDIO_Printf ("failed.");
        }

        R_STDIO_Printf ("\n");

        R_STDIO_Printf ("\nDestination address: 0x%.4X", R_BYTE_ArrToUInt16 ((const uint8_t *)pdisCfm->dstAddr));
        R_STDIO_Printf ("\nMetric Type: 0x%.2X", pdisCfm->pathMetricType);
        R_STDIO_Printf ("\nPath Table Entries: 0x%.2X", pdisCfm->pathTableEntries);
        R_STDIO_Printf ("\nStatus: %s(0x%.2X)", status_to_text (R_G3_MODE_ADP, pdisCfm->status), pdisCfm->status);

        R_STDIO_Printf ("\n\n---------------Route hops---------------------");

        for (i = 0; i < pdisCfm->pathTableEntries; i++)
        {
            R_STDIO_Printf ("\n %d - Path address: 0x%.4X", i, R_BYTE_ArrToUInt16 (pdisCfm->pathTable[i].pathAddress));
            R_STDIO_Printf ("\n %d - Link cost: 0x%.2X", i, pdisCfm->pathTable[i].linkCost);
            R_STDIO_Printf ("\n %d - Metric not supported: 0x%.2X", i, pdisCfm->pathTable[i].mns);
        }

        R_STDIO_Printf ("\n");

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_AdpmPathDiscovery */
/******************************************************************************
   End of function  R_DEMO_AdpmPathDiscovery
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmReset
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmReset (uint8_t chId, r_adp_adpm_reset_cnf_t ** cnf)
{
    r_result_t                          status;
    R_VOLATILE r_adp_adpm_reset_cnf_t * rstCfm = (R_VOLATILE r_adp_adpm_reset_cnf_t *)&g_g3cb[chId].adpmResetCnf;
    *cnf = (r_adp_adpm_reset_cnf_t *)rstCfm;

    rstCfm->status = R_DEMO_G3_STATUS_NOT_SET;

    /* Check if verbose is enabled */
    if (R_TRUE == g_demo_config.verboseEnabled)
    {
        R_STDIO_Printf ("\n -> Resetting device...");
    }

    status = R_ADP_AdpmResetReq (chId);
    if (R_RESULT_SUCCESS != status)
    {
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            R_STDIO_Printf ("failed!");
        }

        return R_RESULT_FAILED;
    }
    else
    {

        /* Wait for completion of R_ADP_AdpmNetworkStart */
        while (R_DEMO_G3_STATUS_NOT_SET == rstCfm->status)
        {
            /* wait */
        }

        /* Check if verbose is enabled */
        if (R_TRUE == g_demo_config.verboseEnabled)
        {
            if (R_ADP_STATUS_SUCCESS == rstCfm->status)
            {
                R_STDIO_Printf ("success.\n");
            }
            else
            {
                R_STDIO_Printf ("failed. Status: %s(0x%.2X)\n", status_to_text (R_G3_MODE_ADP, rstCfm->status), rstCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
} /* R_DEMO_AdpmReset */
/******************************************************************************
   End of function  R_DEMO_AdpmReset
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmGetWrap
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_adp_status_t R_DEMO_AdpmGetWrap (uint8_t chId, r_adp_ib_id_t id, uint16_t index, uint8_t * val)
{
    r_adp_adpm_get_req_t   req;
    r_adp_adpm_get_cnf_t * getCfm;

    req.aibAttributeId = id;
    req.aibAttributeIndex = index;

    if (R_DEMO_AdpmGet (chId, &req, &getCfm) == R_RESULT_SUCCESS)
    {
        R_memcpy (val, getCfm->aibAttributeValue, sizeof (getCfm->aibAttributeValue));
        return (r_adp_status_t)getCfm->status;
    }
    else
    {
        return (r_adp_status_t)R_DEMO_G3_STATUS_FAILED;
    }
} /* R_DEMO_AdpmGetWrap */
/******************************************************************************
   End of function  R_DEMO_AdpmGetWrap
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AdpmSetWrap
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_adp_status_t R_DEMO_AdpmSetWrap (uint8_t chId, r_adp_ib_id_t id, uint16_t index, uint8_t * val)
{
    r_adp_adpm_set_req_t   req;
    r_adp_adpm_set_cnf_t * pCnf;

    req.aibAttributeId = id;
    req.aibAttributeIndex = index;
    req.aibAttributeValue = val;
    if (R_DEMO_AdpmSet (chId, &req, &pCnf) == R_RESULT_SUCCESS)
    {
        return (r_adp_status_t)pCnf->status;
    }
    else
    {
        return (r_adp_status_t)R_DEMO_G3_STATUS_FAILED;
    }
}
/******************************************************************************
   End of function  R_DEMO_AdpmSetWrap
******************************************************************************/




/******************************************************************************
* Function Name: R_DEMO_AdpSetConfig
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_adp_status_t R_DEMO_AdpSetConfig (uint8_t chId)
{
    r_g3_set_config_req_t   req = {0};
    r_g3_set_config_cnf_t * setcfgCfm;
    r_g3_config_extid_t     tmpExtId = {0};

    R_BYTE_UInt64ToArr (g_demo_config.deviceEUI64, (uint8_t *)&tmpExtId);

    req.g3mode = R_G3_MODE_ADP;
    req.config.adp.bandPlan = g_demo_config.bandPlan;
    R_memcpy (req.config.adp.extendedAddress, (uint8_t *)&tmpExtId, 8);
    R_memcpy (req.config.adp.psk, g_demo_config.pskKey, 16);

    if (R_G3_ROUTE_TYPE_JP_B == g_demo_config.routeType)
    {
        req.config.adp.extIDFlg = R_TRUE;
        req.config.adp.pExtId = &tmpExtId;
        R_memcpy (req.config.adp.pExtId, &g_demo_config.extId, sizeof (r_g3_config_extid_t));
    }
    else
    {
        /**/
    }

    if (R_DEMO_G3SetConfig (chId, &req, &setcfgCfm) == R_RESULT_SUCCESS)
    {
        return (r_adp_status_t)setcfgCfm->status;
    }
    else
    {
        return (r_adp_status_t)R_DEMO_G3_STATUS_FAILED;
    }
} /* R_DEMO_AdpSetConfig */
/******************************************************************************
   End of function  R_DEMO_AdpSetConfig
******************************************************************************/

