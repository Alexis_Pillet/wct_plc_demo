/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_dflash_lib.c
*    @version
*        $Rev: 2562 $
*    @last editor
*        $Author: a5089763 $
*    @date  
*        $Date:: 2016-12-26 16:38:20 +0900#$
* Description : 
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include "r_typedefs.h"

#include "r_flash_api_rx600.h"
#include "r_flash_api_rx600_private.h"

#include "r_dflash_lib.h"
#include "r_dflash_usage_map.h"

/******************************************************************************
Macro definitions
******************************************************************************/
/******************************************************************************
Typedef definitions
******************************************************************************/
/******************************************************************************
Private global variables and functions
******************************************************************************/
static uint8_t s_dflash_rmem[DFLASH_BLOCK_SIZE_LARGE];

/******************************************************************************
Exported global variables
******************************************************************************/
/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/
/******************************************************************************
Functions
******************************************************************************/



/******************************************************************************
* Function Name: r_dflash_write
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint8_t r_dflash_write(uint32_t df_dstaddr, uint8_t *src_buff, uint16_t size_byte)
{
    uint8_t ret = 0;
    uint16_t erase_byte = ((size_byte + DFLASH_BLOCK_SIZE) - 1) & DFLASH_BLOCK_MASK;
    uint16_t write_byte = (size_byte + 1) & (0xFFFE);
    
    R_FLASH_DataAreaAccess(DF_USE_BLOCK_MASK, DF_USE_BLOCK_MASK);
    
    ret = R_FLASH_EraseRange(df_dstaddr,erase_byte);
    
    if(FLASH_SUCCESS == ret)
    {
        ret = R_FLASH_Write(df_dstaddr, (uint32_t)src_buff, write_byte);
    }
    
    R_FLASH_DataAreaAccess(DF_USE_BLOCK_MASK, DF_USE_BLOCK_MASK);
    
    return ret;
}
/******************************************************************************
   End of function  r_dflash_write
******************************************************************************/

/******************************************************************************
* Function Name: r_dflash_tmp_buff
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint8_t* r_dflash_tmp_buff()
{
    return s_dflash_rmem;
}
/******************************************************************************
   End of function  r_dflash_tmp_buff
******************************************************************************/

/******************************************************************************
* Function Name: r_dflash_erase
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint8_t r_dflash_erase(uint32_t df_dstaddr, uint16_t size_byte)
{
    uint8_t ret = 0;
    uint16_t erase_byte = ((size_byte + DFLASH_BLOCK_SIZE) - 1) & DFLASH_BLOCK_MASK;
    
    R_FLASH_DataAreaAccess(DF_USE_BLOCK_MASK, DF_USE_BLOCK_MASK);
    
    ret = R_FLASH_EraseRange(df_dstaddr,erase_byte);
#if 1
    /* Future Remove */
    R_memset(s_dflash_rmem, 0, erase_byte);
    if(FLASH_SUCCESS == ret)
    {
        ret = R_FLASH_Write(df_dstaddr, (uint32_t)&s_dflash_rmem, erase_byte);
    }
#endif
    return ret;
}
/******************************************************************************
   End of function  r_dflash_erase
******************************************************************************/

/******************************************************************************
* Function Name: r_dflash_read_set
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_dflash_read_set()
{
    R_FLASH_DataAreaAccess(DF_USE_BLOCK_MASK, DF_USE_BLOCK_MASK);
}
/******************************************************************************
   End of function  r_dflash_read_set
******************************************************************************/

/******************************************************************************
* Function Name: r_dflash_read
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_dflash_read(uint8_t **dst_buff, uint32_t df_srcaddr, uint16_t size_byte)
{
    r_dflash_read_set();
    *dst_buff = s_dflash_rmem;

#ifndef _MSC_VER
    R_memcpy(s_dflash_rmem,(uint32_t *)df_srcaddr,size_byte);
#else

#endif
}
/******************************************************************************
   End of function  r_dflash_read
******************************************************************************/

