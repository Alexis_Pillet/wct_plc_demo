/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Utility Commands/Responses - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.012  20-Jul-16   MvdB   Original
 * 000.000.019  26-Jul-16   MvdB   Replace memcpy with osMemCopy, memset with osMemSet
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/

#include "zabNcpPrivate.h"
#include "zabNcpService.h"
#include "zabNcpEzsp.h"

#include "ezsp-enum.h"
#include "ezsp-enum-decode.h"
#include "ezsp-protocol.h"

#include "zabNcpOpen.h"
#include "zabNcpNwk.h"
#include "zabNcpRft.h"

#include "zabNcpEzspUtilities.h"


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

#define MFG_TOKEN_MAX_LENGTH    ( 8 )

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Get Manufacturing Token Request + Response Handler
 ******************************************************************************/
void zabNcpEzspUtilities_GetMfgToken(erStatus* Status, zabService* Service, EzspMfgTokenId TokenId)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH+1] = {0/*seq*/, 0/*FrameControl*/, EZSP_GET_MFG_TOKEN};
  unsigned8 index = EZSP_PARAMETERS_INDEX;
  ER_CHECK_STATUS(Status);

  printVendor(Service, "EZSP_GET_MFG_TOKEN: %s\n",
              decodeEzspMfgTokenId(TokenId));

  cmd[index++] = TokenId;
  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspUtilities_GetMfgTokenResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 tokenDataLength;
  unsigned8 tokenDataIndex;
  unsigned8 index = 0;

  if (PayloadLength >= 1)
    {
      tokenDataLength = PayloadData[index++];

      printVendor(Service, "EZSP_GET_MFG_TOKEN: Length = %d, Data =", tokenDataLength);

      if (PayloadLength >= (1 + tokenDataLength))
        {
          for (tokenDataIndex = 0; tokenDataIndex < tokenDataLength; tokenDataIndex++)
            {
              printVendor(Service, " %02X", PayloadData[index+tokenDataIndex]);
            }
        }
      printVendor(Service, "\n");

      zabNcpRft_GetMfgTokenResponseHandler(Status, Service, tokenDataLength, &PayloadData[index]);
    }
}

/******************************************************************************
 * Set Manufacturing Token Request + Response Handler
 ******************************************************************************/
void zabNcpEzspUtilities_SetMfgToken(erStatus* Status, zabService* Service, EzspMfgTokenId TokenId, unsigned8 TokenDataLength, unsigned8* TokenData)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH+2+MFG_TOKEN_MAX_LENGTH] = {0/*seq*/, 0/*FrameControl*/, EZSP_SET_MFG_TOKEN};
  unsigned8 index = EZSP_PARAMETERS_INDEX;
  ER_CHECK_STATUS(Status);

  printVendor(Service, "EZSP_SET_MFG_TOKEN: %s (%d)\n", decodeEzspMfgTokenId(TokenId), TokenId);

  if ( (TokenDataLength > 0) && (TokenDataLength <= MFG_TOKEN_MAX_LENGTH) )
    {
      cmd[index++] = TokenId;
      cmd[index++] = TokenDataLength;
      osMemCopy(Status, &cmd[index], TokenData, TokenDataLength); index+=TokenDataLength;

      zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, index, cmd);
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}
void zabNcpEzspUtilities_SetMfgTokenResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus responseStatus;
  if (PayloadLength >= 1)
    {
      responseStatus = PayloadData[0];
      printVendor(Service, "EZSP_SET_MFG_TOKEN: Status = %s (0x%02X)\n",
                  decodeEzspStatus(responseStatus),
                  responseStatus);

      zabNcpRft_SetMfgTokenResponseHandler(Status, Service, responseStatus);
    }
}

/******************************************************************************
 * Get Local IEEE Address Request + Response Handler
 ******************************************************************************/
void zabNcpEzspUtilities_GetNodeIeee(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH] = {0/*seq*/, 0/*FrameControl*/, EZSP_GET_EUI64};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspUtilities_GetNodeIeeeResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= EUI64_SIZE)
    {
      printVendor(Service, "EZSP_GET_EUI64: 0x%016llX\n",
                  COPY_IN_64_BITS(PayloadData));

      zabNcpOpen_GetIeeeAddressResponseHandler(Status, Service, COPY_IN_64_BITS(PayloadData));
    }
}

/******************************************************************************
 * Get Local Network Address Request + Response Handler
 ******************************************************************************/
void zabNcpEzspUtilities_GetNodeId(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH] = {0/*seq*/, 0/*FrameControl*/, EZSP_GET_NODE_ID};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspUtilities_GetNodeIdResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 2)
    {
      printVendor(Service, "EZSP_GET_NODE_ID: 0x%04X\n",
                  COPY_IN_16_BITS(PayloadData));

      zabNcpNwk_GetNetworkAddressResponseHandler(Status, Service, COPY_IN_16_BITS(PayloadData));
    }
}

/******************************************************************************
 * Stack Token Changed Indication Handler
 ******************************************************************************/
void zabNcpEzspUtilities_StackTokenChangedHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 2)
    {
      printVendor(Service, "EZSP_STACK_TOKEN_CHANGED_HANDLER: 0x%04X\n",
                  COPY_IN_16_BITS(PayloadData));
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/