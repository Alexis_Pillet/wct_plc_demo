  /*******************************************************************************
  Filename    : data_converter.c
  $Date       : 2013-05-13                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : ZB and Native data converter functions for the SZL

 * 002.002.032  09-Jan-17   MvdB   ARTF172065: Rationalise duplicated private functions into public SZLEXT_ZigBeeDataTypeIsAnalog()
 * 002.002.037  11-Jan-17   MvdB   Re-enable SZL_ZB_DATATYPE_LONG_OCTET_STR as it is used by ZGP Sink Table attribute
*******************************************************************************/

/**************************************************************************************************
* INCLUDES
**************************************************************************************************/
#include <string.h>
#include <assert.h>

#include "endianness.h"
#include "szl_types.h"
#include "szl.h"
#include "szl_external.h"
#include "data_converter.h"

/**************************************************************************************************
* DEFINES
**************************************************************************************************/
/*
 * Global defines
 */

/*
 * Application data defines
 */

/**************************************************************************************************
* ENUMs, STRUCTs & UNIONs
**************************************************************************************************/

/**************************************************************************************************
* PROTOS
**************************************************************************************************/

/**************************************************************************************************
* VARIABLES
**************************************************************************************************/

/**************************************************************************************************
* FUNCTIONS
**************************************************************************************************/

/**
 * @brief   Sets the NATIVE or LE data to Zero
 *
 * @param   Data - pointer to native Data
 *
 * @return  N/A
 */
void DCDataSetToZero(void *data)
{
    if (data != NULL)
    {
        // since the native and le data type each has the same raw data size we just use the native
        NATIVE_DATA_t* nd = (NATIVE_DATA_t*)data;

        szl_memset(nd->data.raw, 0, sizeof(nd->data.raw));
    }
}

/**
 * @brief   Calculate the Native PAD size for a giben ZB Data type
 *
 * @param   DataType - Data to be padded
 *
 * @return  The pad size of a native data type for a given ZB data type
 */
szl_uint8 NativePadSize(SZL_ZIGBEE_DATA_TYPE_t DataType)
{
    szl_uint8 padSize = 0;
    switch (DataType)
        {
//          case SZL_ZB_DATATYPE_DATA16:
//          case SZL_ZB_DATATYPE_BITMAP16:
//          case SZL_ZB_DATATYPE_UINT16:
//          case SZL_ZB_DATATYPE_INT16:
//          case SZL_ZB_DATATYPE_ENUM16:
//          case SZL_ZB_DATATYPE_SEMI_PREC:
//          case SZL_ZB_DATATYPE_CLUSTER_ID:
//          case SZL_ZB_DATATYPE_ATTR_ID:
//              padSize = 0;
//              break;

            case SZL_ZB_DATATYPE_DATA24:
            case SZL_ZB_DATATYPE_BITMAP24:
            case SZL_ZB_DATATYPE_UINT24:
            case SZL_ZB_DATATYPE_INT24:
                padSize = 1;
                break;

//          case SZL_ZB_DATATYPE_DATA32:
//          case SZL_ZB_DATATYPE_BITMAP32:
//          case SZL_ZB_DATATYPE_UINT32:
//          case SZL_ZB_DATATYPE_INT32:
//          case SZL_ZB_DATATYPE_SINGLE_PREC:
//          case SZL_ZB_DATATYPE_TOD:
//          case SZL_ZB_DATATYPE_DATE:
//          case SZL_ZB_DATATYPE_UTC:
//            padSize = 0;
//            break;

            case SZL_ZB_DATATYPE_DATA40:
            case SZL_ZB_DATATYPE_BITMAP40:
            case SZL_ZB_DATATYPE_UINT40:
            case SZL_ZB_DATATYPE_INT40:
                padSize = 3;
                break;

            case SZL_ZB_DATATYPE_DATA48:
            case SZL_ZB_DATATYPE_BITMAP48:
            case SZL_ZB_DATATYPE_UINT48:
            case SZL_ZB_DATATYPE_INT48:
                padSize = 2;
                break;

            case SZL_ZB_DATATYPE_DATA56:
            case SZL_ZB_DATATYPE_BITMAP56:
            case SZL_ZB_DATATYPE_UINT56:
            case SZL_ZB_DATATYPE_INT56:
                padSize = 1;
                break;

//          case SZL_ZB_DATATYPE_DATA64:
//          case SZL_ZB_DATATYPE_BITMAP64:
//          case SZL_ZB_DATATYPE_UINT64:
//          case SZL_ZB_DATATYPE_INT64:
//          case SZL_ZB_DATATYPE_DOUBLE_PREC:
//          case SZL_ZB_DATATYPE_IEEE_ADDR:
//              padSize = 0;
//              break;
        }

    return padSize;
}

/**
 * @brief   Check whether datatype is Primitive data type or not
 *
 * @param   DataType - Datatype to be checked
 *
 * @return  szl_true if datatype is primitive
 */
szl_bool DataTypeIsPrimitive(SZL_ZIGBEE_DATA_TYPE_t DataType)
{
    switch (DataType)
    {
        case SZL_ZB_DATATYPE_DATA8:
        case SZL_ZB_DATATYPE_DATA16:
        case SZL_ZB_DATATYPE_DATA24:
        case SZL_ZB_DATATYPE_DATA32:
        case SZL_ZB_DATATYPE_DATA40:
        case SZL_ZB_DATATYPE_DATA48:
        case SZL_ZB_DATATYPE_DATA56:
        case SZL_ZB_DATATYPE_DATA64:
        case SZL_ZB_DATATYPE_BOOLEAN:
        case SZL_ZB_DATATYPE_BITMAP8:
        case SZL_ZB_DATATYPE_BITMAP16:
        case SZL_ZB_DATATYPE_BITMAP24:
        case SZL_ZB_DATATYPE_BITMAP32:
        case SZL_ZB_DATATYPE_BITMAP40:
        case SZL_ZB_DATATYPE_BITMAP48:
        case SZL_ZB_DATATYPE_BITMAP56:
        case SZL_ZB_DATATYPE_BITMAP64:
        case SZL_ZB_DATATYPE_UINT8:
        case SZL_ZB_DATATYPE_UINT16:
        case SZL_ZB_DATATYPE_UINT24:
        case SZL_ZB_DATATYPE_UINT32:
        case SZL_ZB_DATATYPE_UINT40:
        case SZL_ZB_DATATYPE_UINT48:
        case SZL_ZB_DATATYPE_UINT56:
        case SZL_ZB_DATATYPE_UINT64:
        case SZL_ZB_DATATYPE_INT8:
        case SZL_ZB_DATATYPE_INT16:
        case SZL_ZB_DATATYPE_INT24:
        case SZL_ZB_DATATYPE_INT32:
        case SZL_ZB_DATATYPE_INT40:
        case SZL_ZB_DATATYPE_INT48:
        case SZL_ZB_DATATYPE_INT56:
        case SZL_ZB_DATATYPE_INT64:
        case SZL_ZB_DATATYPE_ENUM8:
        case SZL_ZB_DATATYPE_ENUM16:
        case SZL_ZB_DATATYPE_SEMI_PREC:
        case SZL_ZB_DATATYPE_SINGLE_PREC:
        case SZL_ZB_DATATYPE_DOUBLE_PREC:
        case SZL_ZB_DATATYPE_TOD:
        case SZL_ZB_DATATYPE_DATE:
        case SZL_ZB_DATATYPE_UTC:
        case SZL_ZB_DATATYPE_CLUSTER_ID:
        case SZL_ZB_DATATYPE_ATTR_ID:
        case SZL_ZB_DATATYPE_BAC_OID:
        case SZL_ZB_DATATYPE_IEEE_ADDR:
        case SZL_ZB_DATATYPE_128_BIT_SEC_KEY:
            return szl_true;
    }

    return szl_false;
}

/**
 * @brief   Check whether datatype is Signed data type or not
 *
 * @param   DataType - Datatype to be checked
 *
 * @return  szl_true if datatype is signed
 */
szl_bool DataTypeIsSigned(SZL_ZIGBEE_DATA_TYPE_t DataType)
{
    szl_bool isSigned = szl_false;

    switch (DataType)
    {
        case SZL_ZB_DATATYPE_INT8:
        case SZL_ZB_DATATYPE_INT16:
        case SZL_ZB_DATATYPE_INT24:
        case SZL_ZB_DATATYPE_INT32:
        case SZL_ZB_DATATYPE_INT40:
        case SZL_ZB_DATATYPE_INT48:
        case SZL_ZB_DATATYPE_INT56:
        case SZL_ZB_DATATYPE_INT64:
        case SZL_ZB_DATATYPE_SEMI_PREC:
        case SZL_ZB_DATATYPE_SINGLE_PREC:
        case SZL_ZB_DATATYPE_DOUBLE_PREC:
            isSigned = szl_true;
            break;
    }

    return isSigned;
}

/**
 * @brief   Check the size of the given datatype
 *
 * @param   DataType - Datatype to be checked
 * @param   data     - pointer to the string if datatype is char string
 *
 * @return  The size of a datatype in the zigbee world
 */
szl_uint8 ZbDataSize(SZL_ZIGBEE_DATA_TYPE_t DataType, void* data)
{
    szl_uint8 length = 0;

    switch (DataType)
    {
        case SZL_ZB_DATATYPE_BOOLEAN:
        case SZL_ZB_DATATYPE_BITMAP8:
        case SZL_ZB_DATATYPE_DATA8:
        case SZL_ZB_DATATYPE_UINT8:
        case SZL_ZB_DATATYPE_INT8:
        case SZL_ZB_DATATYPE_ENUM8:
            length = 1;
            break;

        case SZL_ZB_DATATYPE_BITMAP16:
        case SZL_ZB_DATATYPE_DATA16:
        case SZL_ZB_DATATYPE_UINT16:
        case SZL_ZB_DATATYPE_INT16:
        case SZL_ZB_DATATYPE_ENUM16:
        case SZL_ZB_DATATYPE_SEMI_PREC:
        case SZL_ZB_DATATYPE_CLUSTER_ID:
        case SZL_ZB_DATATYPE_ATTR_ID:
            length = 2;
            break;

        case SZL_ZB_DATATYPE_BITMAP24:
        case SZL_ZB_DATATYPE_DATA24:
        case SZL_ZB_DATATYPE_UINT24:
        case SZL_ZB_DATATYPE_INT24:
            length = 3;
            break;

        case SZL_ZB_DATATYPE_BITMAP32:
        case SZL_ZB_DATATYPE_DATA32:
        case SZL_ZB_DATATYPE_UINT32:
        case SZL_ZB_DATATYPE_INT32:
        case SZL_ZB_DATATYPE_SINGLE_PREC:
        case SZL_ZB_DATATYPE_TOD:
        case SZL_ZB_DATATYPE_DATE:
        case SZL_ZB_DATATYPE_UTC:
        case SZL_ZB_DATATYPE_BAC_OID:
            length = 4;
            break;

        case SZL_ZB_DATATYPE_BITMAP40:
        case SZL_ZB_DATATYPE_DATA40:
        case SZL_ZB_DATATYPE_UINT40:
        case SZL_ZB_DATATYPE_INT40:
            length = 5;
            break;

        case SZL_ZB_DATATYPE_BITMAP48:
        case SZL_ZB_DATATYPE_DATA48:
        case SZL_ZB_DATATYPE_UINT48:
        case SZL_ZB_DATATYPE_INT48:
            length = 6;
            break;

        case SZL_ZB_DATATYPE_BITMAP56:
        case SZL_ZB_DATATYPE_DATA56:
        case SZL_ZB_DATATYPE_UINT56:
        case SZL_ZB_DATATYPE_INT56:
            length = 7;
            break;

        case SZL_ZB_DATATYPE_BITMAP64:
        case SZL_ZB_DATATYPE_DATA64:
        case SZL_ZB_DATATYPE_UINT64:
        case SZL_ZB_DATATYPE_INT64:
        case SZL_ZB_DATATYPE_DOUBLE_PREC:
        case SZL_ZB_DATATYPE_IEEE_ADDR:
            length = 8;
            break;

        case SZL_ZB_DATATYPE_CHAR_STR:
        case SZL_ZB_DATATYPE_OCTET_STR:
            if (data != NULL)
              {
                /* Length = 0xFF -> Invalid String. Set length to 1 (just the length byte) */
                if (*(szl_uint8*)data == SZL_ZB_DATATYPE_CHAR_OCTET_STR_INVALID_VALUE)
                  {
                    length = 1;
                  }
                else
                  {
                    length = 1 + *(szl_uint8*)data; // the length is placed in index[0] for zb string, or null is placed at end for native string
                  }
              }
            break;

        case SZL_ZB_DATATYPE_LONG_OCTET_STR:
            if (data != NULL)
              {
                szl_uint16 length16 = LE_TO_UINT16(*(szl_uint16*)data);
                length = (szl_uint8)(2 + length16); // the length is placed in index[0-1]
              }
            break;

        case SZL_ZB_DATATYPE_128_BIT_SEC_KEY:
            length = 16;
            break;
    }

    return length;
}

/**
 * @brief   Calculate the native data size of the given ZigBee datatype
 *
 * @param   DataType - Datatype to be checked
 * @param   data     - pointer to the string if datatype is char string
 *
 * @return  The size of a datatype in the application world
 */
szl_uint8 NativeDataSize(SZL_ZIGBEE_DATA_TYPE_t DataType, void* data)
{
    szl_uint8 length = 0;

    switch (DataType)
    {
        case SZL_ZB_DATATYPE_BOOLEAN:
        case SZL_ZB_DATATYPE_BITMAP8:
        case SZL_ZB_DATATYPE_DATA8:
        case SZL_ZB_DATATYPE_UINT8:
        case SZL_ZB_DATATYPE_INT8:
        case SZL_ZB_DATATYPE_ENUM8:
            length = 1;
            break;

        case SZL_ZB_DATATYPE_BITMAP16:
        case SZL_ZB_DATATYPE_DATA16:
        case SZL_ZB_DATATYPE_UINT16:
        case SZL_ZB_DATATYPE_INT16:
        case SZL_ZB_DATATYPE_ENUM16:
        case SZL_ZB_DATATYPE_SEMI_PREC:
        case SZL_ZB_DATATYPE_CLUSTER_ID:
        case SZL_ZB_DATATYPE_ATTR_ID:
            length = 2;
            break;

        case SZL_ZB_DATATYPE_BITMAP24:
        case SZL_ZB_DATATYPE_DATA24:
        case SZL_ZB_DATATYPE_UINT24:
        case SZL_ZB_DATATYPE_INT24:
        case SZL_ZB_DATATYPE_BITMAP32:
        case SZL_ZB_DATATYPE_DATA32:
        case SZL_ZB_DATATYPE_UINT32:
        case SZL_ZB_DATATYPE_INT32:
        case SZL_ZB_DATATYPE_SINGLE_PREC:
        case SZL_ZB_DATATYPE_TOD:
        case SZL_ZB_DATATYPE_DATE:
        case SZL_ZB_DATATYPE_UTC:
            length = 4;
            break;

        case SZL_ZB_DATATYPE_BITMAP40:
        case SZL_ZB_DATATYPE_DATA40:
        case SZL_ZB_DATATYPE_UINT40:
        case SZL_ZB_DATATYPE_INT40:
        case SZL_ZB_DATATYPE_BITMAP48:
        case SZL_ZB_DATATYPE_DATA48:
        case SZL_ZB_DATATYPE_UINT48:
        case SZL_ZB_DATATYPE_INT48:
        case SZL_ZB_DATATYPE_BITMAP56:
        case SZL_ZB_DATATYPE_DATA56:
        case SZL_ZB_DATATYPE_UINT56:
        case SZL_ZB_DATATYPE_INT56:
        case SZL_ZB_DATATYPE_BITMAP64:
        case SZL_ZB_DATATYPE_DATA64:
        case SZL_ZB_DATATYPE_UINT64:
        case SZL_ZB_DATATYPE_INT64:
        case SZL_ZB_DATATYPE_DOUBLE_PREC:
        case SZL_ZB_DATATYPE_IEEE_ADDR:
            length = 8;
            break;

        case SZL_ZB_DATATYPE_CHAR_STR:
        case SZL_ZB_DATATYPE_OCTET_STR:
            if (data != NULL)
              {
                /* Length = 0xFF -> Invalid String. Set length to 1 (just the terminator) */
                if (*(szl_uint8*)data == SZL_ZB_DATATYPE_CHAR_OCTET_STR_INVALID_VALUE)
                  {
                    length = 1;
                  }
                else
                  {
                    length = 1 + *(szl_uint8*)data; // string length + terminator
                  }
              }
            break;
    }

    return length;
}

/**
 * @brief   This function converts a ZigBee Little Endian data into a native data type in native endian
 *
 * @param   dataType          - ZigBee Data Type
 * @param   zb                - ZigBee Little endian
 * @param   zbLength          - length of the zb little endian
 * @param   pNativeData       - NativeData pointer for the native endian
 * @param   lengthNativeData  - Length of the native endian
 *
 * @return  szl_true, if data has been converted
 */
szl_bool ZbToNative(SZL_ZIGBEE_DATA_TYPE_t dataType, szl_uint8* zb, szl_uint8 zbLength, NATIVE_DATA_t* pNativeData, szl_uint8* lengthNativeData )
{
    szl_uint8 padSize;
    // this table states how big a data type is needed for the zigbee data length
    const szl_uint8 nativeDataSizes[] = { 1, 2, 4, 4, 8, 8 ,8 ,8 };

    //Note:  Due to the void pointer zb being unaligned (and accessing a random string of
    //serial data) the value needs to be copied to an aligned address for an double-word
    //access on a Cortex-M3
    LE_DATA_t aligned_le;
    LE_DATA_t* le;
    szl_memcpy(&aligned_le,zb,sizeof(LE_DATA_t));
    le = &aligned_le;
    *lengthNativeData = 0;


    // check if out of range
    if ( zbLength == 0 || zbLength > sizeof(nativeDataSizes))
    {
        return szl_false;
    }

    // set the return values
    *lengthNativeData = nativeDataSizes[zbLength - 1];

    // clear to set the padding data
    DCDataSetToZero(pNativeData);

    // calculate native pad size
    padSize = *lengthNativeData - zbLength;

    // do the copy and endian conversion from Little Endian to Native Endian
    switch (*lengthNativeData)
    {
        case 1:
            pNativeData->data.primitive8 = le->data.primitive8;
            break;

        case 2:
            pNativeData->data.primitive16 = LE_TO_UINT16(le->data.primitive16);
            break;

        case 4:
            switch (padSize)
            {
                case 0: // szl_uint32
                    pNativeData->data.primitive32 = LE_TO_UINT32(le->data.primitive32);
                    break;

                case 1: // szl_uint24
                    pNativeData->data.primitive24 = LE_TO_UINT24(le->data.primitive24);
                    break;

                default:
                    return szl_false;
            }
            break;

        case 8:
            switch (padSize)
            {
                case 0: // szl_uint64
                    pNativeData->data.primitive64 = LE_TO_UINT64(le->data.primitive64);
                    break;

                case 1: // szl_uint56
                    pNativeData->data.primitive56 = LE_TO_UINT56(le->data.primitive56);
                    break;

                case 2: // szl_uint48
                    pNativeData->data.primitive48 = LE_TO_UINT48(le->data.primitive48);
                    break;

                case 3: // szl_uint40
                    pNativeData->data.primitive40 = LE_TO_UINT40(le->data.primitive40);
                    break;

                default:
                    return szl_false;
            }
            break;

        default:
            return szl_false;
    }

    // If signed datatype with negative value then we must pad with 1's
    if ( (padSize > 0) &&
         DataTypeIsSigned(dataType) &&
         (le->data.raw[zbLength-1] & 0x80))
    {
        while (padSize > 0)
        {
            pNativeData->data.raw[zbLength++] = 0xFF;
            padSize--;
        }
    }

    return szl_true;
}

/**
 * @brief   This function converts a native data type in native endian into a ZigBee data in Little Endian
 *
 * @param   nativeData      - the native endian
 * @param   nativeDataSize  - length of the native endian
 * @param   DataType        - data type of the zb endian
 * @param   zb              - pointer to the ZB native endian
 *
 * @return  > 0, if data is successfully converted otherwise 0
 */
szl_uint8 NativeToZb(void* nativeData, szl_uint8 nativeDataSize, SZL_ZIGBEE_DATA_TYPE_t DataType, void* zb )
{
    szl_uint8 padSize = NativePadSize(DataType);

    //Note:  Due to the void pointer nativeData being (potentially) unaligned (and accessing
    //a random string of serial data) the value needs to be copied to an aligned address for
    //double-word access on a Cortex-M3.  The output also needs to be copied back to a
    //possibly unaligned address.
    NATIVE_DATA_t alignedNativeData;
    NATIVE_DATA_t* native = &alignedNativeData;
    LE_DATA_t aligned_le;
    LE_DATA_t* le = &aligned_le;
    szl_memcpy(&alignedNativeData,nativeData,sizeof(NATIVE_DATA_t));

    switch (nativeDataSize)
    {
        case 1:
            le->data.primitive8 = native->data.primitive8;
            break;

        case 2:
            le->data.primitive16 = UINT16_TO_LE(native->data.primitive16);
            break;

        case 4:
            switch (padSize)
            {
                case 0: // szl_uint32
                    le->data.primitive32 = UINT32_TO_LE(native->data.primitive32);
                    break;

                case 1: // szl_uint24
                    le->data.primitive24 = UINT24_TO_LE(native->data.primitive24);
                    break;

                default:
                    return 0;
            }
            break;

        case 8:
            switch (padSize)
            {
                case 0: // szl_uint64
                    le->data.primitive64 = UINT64_TO_LE(native->data.primitive64);
                    break;

                case 1: // szl_uint56
                    le->data.primitive56 = UINT56_TO_LE(native->data.primitive56);
                    break;

                case 2: // szl_uint48
                    le->data.primitive48 = UINT48_TO_LE(native->data.primitive48);
                    break;

                case 3: // szl_uint40
                    le->data.primitive40 = UINT40_TO_LE(native->data.primitive40);
                    break;

                default:
                    return 0;
            }
            break;

        default:
            return 0;
    }

    //Copy the aligned data into the (potentially) unaligned memory space
    szl_memcpy(zb,&aligned_le,sizeof(LE_DATA_t));
    return nativeDataSize - padSize;
}
