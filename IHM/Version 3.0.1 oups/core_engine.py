# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to manage the thread/engine controlling the process of the program
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************

from mac import MacProcess
from network import NetworkProcess
from application import ApplicationProcess


class CoreEngine:

    def __init__(self):
        self._mac_process = MacProcess()
        self._network_process = NetworkProcess()
        self._application_process = ApplicationProcess()
        self._debug = False

    # Activate/Deactivate debug
    def set_debug(self, value):
        self._debug = value

    def start(self):

        # Initialization mac process
        self._mac_process.set_debug(self._debug)
        self._mac_process.network_queue = self._network_process.queue
        # Start mac process
        self._mac_process.start()

        # Initialization network process
        self._network_process.mac_frame_generator = self._mac_process.mac_frame_generator
        self._network_process.application_queue = self._application_process.queue
        self._network_process.set_debug(self._debug)
        # Start network process
        self._network_process.start()

        # Initialization application process
        self._application_process.network_frame_generator = self._network_process.network_frame_generator
        self._application_process.set_debug(self._debug)
        # Start application process
        self._application_process.start()

        # Test create device
        self._application_process.zigbee_device_container.add_device(model_id='0b0000', id_wireless_bridge=0x00,
                                                                     address='0b0001')
        if self._application_process.zigbee_device_container.is_device_saved(id_wireless_bridge=0x00, address='0b0002'):
            self._application_process.zigbee_device_container.get_device(id_wireless_bridge=0x00, address='0b0001')

    def stop(self):
        # Stop mac process
        self._mac_process.stop()
        # Stop network process
        self._network_process.stop()
        # Stop application process
        self._application_process.stop()
