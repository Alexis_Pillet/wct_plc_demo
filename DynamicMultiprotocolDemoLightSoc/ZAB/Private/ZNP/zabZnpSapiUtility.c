/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the ZNP SAPI interface
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *                           NJ    Original
 *              19-Jul-13   MvdB   Remove unused code
 * 00.00.06.00  09-Sep-14   MvdB   artf103725: Fix EPID presentation
 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 002.002.012  14-Oct-15   MvdB   ARTF104107: Support device info/config params for End Device operation
 * 002.002.020  18-Mar-16   MvdB   Print nwk key and sequence number for ZAB_ZNP_SAPI_NV_CFG_ID_NWKKEY
 *****************************************************************************/

#include "zabZnpService.h"

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* Length macros */
#define ZNPI_ZB_WRITE_CONFIGURATION_DATALENGTH(lp)  (zabznpSapi_ZbConf_SIZE + (lp))



/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

typedef enum
{
    /************************/
    /* Simple API interface */
    /************************/

    ZNPI_CMD_ZB_START_REQUEST           = 0x00, /* ZB_START_REQUEST */
    ZNPI_CMD_ZB_BIND_DEVICE             = 0x01, /* ZB_BIND_DEVICE */
    ZNPI_CMD_ZB_ALLOW_BIND              = 0x02, /* ZB_ALLOW_BIND */
    ZNPI_CMD_ZB_SEND_DATA_REQUEST       = 0x03, /* ZB_SEND_DATA_REQUEST */
    /***************************/
    /* Configuration interface */
    /***************************/
    ZNPI_CMD_ZB_READ_CONFIGURATION      = 0x04, /* ZB_READ_CONFIGURATION */
    ZNPI_CMD_ZB_WRITE_CONFIGURATION     = 0x05, /* ZB_WRITE_CONFIGURATION */
    /************************************/
    /* Simple API interface (continued) */
    /************************************/
    ZNPI_CMD_ZB_GET_DEVICE_INFO         = 0x06, /* ZB_GET_DEVICE_INFO */
    ZNPI_CMD_ZB_FIND_DEVICE_REQUEST     = 0x07, /* ZB_FIND_DEVICE_REQUEST */
    ZNPI_CMD_ZB_PERMIT_JOINING_REQUEST  = 0x08, /* ZB_PERMIT_JOINING_REQUEST */
    ZNPI_CMD_ZB_SYSTEM_RESET            = 0x09, /* ZB_SYSTEM_RESET */
    ZNPI_CMD_ZB_APP_REGISTER_REQUEST    = 0x0A, /* ZB_APP_REGISTER_REQUEST */



    ZNPI_CMD_ZB_START_CONFIRM           = 0x80, /* ZB_START_CONFIRM */
    ZNPI_CMD_ZB_BIND_CONFIRM            = 0x81, /* ZB_BIND_CONFIRM */
    ZNPI_CMD_ZB_ALLOW_BIND_CONFIRM      = 0x82, /* ZB_ALLOW_BIND_CONFIRM */
    ZNPI_CMD_ZB_SEND_DATA_CONFIRM       = 0x83, /* ZB_SEND_DATA_CONFIRM */
    ZNPI_CMD_ZB_FIND_DEVICE_CONFIRM     = 0x85, /* ZB_FIND_DEVICE_CONFIRM */
    ZNPI_CMD_ZB_RECEIVE_DATA_INDICATION = 0x87, /* ZB_RECEIVE_DATA_INDICATION */
      
    // Artf104182: Schneider extension for notification of device info changes
    ZNPI_CMD_ZB_DEVICE_INFO_NOTIFICATION         = 0xC6, /* ZB_GET_DEVICE_INFO */
} ZNPI_CMD_ZB_COMMANDCODES;


//------------------------------------------------------------------------------
// Struct Status For All ZNP RESPONSE
// - ZB_WRITE_CONFIGURATION_RSP
typedef struct s_znpSapiStatus
{
    unsigned8 status;       // Status
} zabznpSapi_ZbStatusResp;


//------------------------------------------------------------------------------
// Struct for ZNP MESSAGE :
//  - ZB_WRITE_CONFIGURATION_REQ
//  - ZB_READ_CONFIGURATION_REQ
//  - ZB_READ_CONFIGURATION_RESP
typedef struct s_znpZbConf
{
    unsigned8 configId;     // use by All
    unsigned8 Length;       // use by ZB_WRITE_CONFIGURATION_REQ / ZB_READ_CONFIGURATION_RESP
    unsigned8 Value[VLA_INIT];     // use by ZB_WRITE_CONFIGURATION_REQ / ZB_READ_CONFIGURATION_RESP
} zabznpSapi_ZbConf;
#define zabznpSapi_ZbConf_SIZE (sizeof(zabznpSapi_ZbConf) - (VLA_INIT*sizeof(unsigned8)))

/*
 * Struct ZNP of ZB READ configuration parameter
 */
typedef struct s_znpZbRdConfResp
{
    zabznpSapi_ZbStatusResp Status;
    zabznpSapi_ZbConf       Conf;
} zabznpSapi_ZB_READ_CONFIGURATION_RESP;

//------------------------------------------------------------------------------
// Struct for ZNP MESSAGE :
//  - ZB_DEVICE_INFO_REQ
//  - ZB_DEVICE_INFO_RESP
typedef struct s_znpDeviceInfo
{
    unsigned8 paramId;      // For All
    unsigned8 value[VLA_INIT];        // For ZB_DEVICE_INFO_RESP only
} zabznpSapi_ZbDeviceInfo;
#define zabznpSapi_ZbDeviceInfo_SIZE (sizeof(zabznpSapi_ZbDeviceInfo) - (VLA_INIT*sizeof(unsigned8)))


/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Get status from an incoming response
 ******************************************************************************/
static void getSapiStatus( erStatus* Status, sapMsg* Message, ZNPI_API_ERROR_CODES* sapiStatus )
{
    zabznpSapi_ZbStatusResp *data;
    unsigned8 len;

    ER_CHECK_STATUS_NULL(Status, sapiStatus);

    zabParseZNPGetData(Status, Message, &len, (unsigned8 **)&data);

    ER_CHECK_STATUS(Status);

    if (len >= sizeof(*data))
    {
        *sapiStatus = (ZNPI_API_ERROR_CODES)data->status;
    }
    else
    {
        *sapiStatus = ZNPI_API_ERR_FAILURE;
        erStatusSet( Status, ZAB_ERROR_VENDOR_PARSE);
    }
}

/******************************************************************************
 * Check status of an incoming response
 ******************************************************************************/
static void zabZnp_ProcessStatus(erStatus* Status, zabService* Service, sapMsg* Message)
{
  ZNPI_API_ERROR_CODES rspStatus;
  
  getSapiStatus(Status, Message, &rspStatus);
          
  if (rspStatus != ZNPI_API_ERR_SUCCESS)
    {
      printError(Service, "zabZnpSapi: WARNING: Bad response status = 0x%02X\n", (unsigned8)rspStatus);
      erStatusSet(Status, ZAB_ERROR_VENDOR_SENDING);
    }
}




/******************************************************************************
 * Process incoming Device Info response
 ******************************************************************************/
static void zabZnp_ProcessSapiDeviceInfo(erStatus* Status, zabService* Service, sapMsg* Message)
{
  zabznpSapi_ZbDeviceInfo *msgData;
  unsigned8 *data;
  unsigned16 data16;
  unsigned64 data64;

  ER_CHECK_STATUS_NULL(Status, Message);

  msgData = (zabznpSapi_ZbDeviceInfo*)ZAB_MSG_ZNP_DATA(Message);
  ER_CHECK_STATUS_NULL(Status, msgData);
  
  printVendor(Service, "zabZnpSapiUtility: Device Info Rsp\n");
        
  data = msgData->value;

  switch((znpSapiDeviceInfoType)msgData->paramId)
  {
    case ZAB_ZNP_SAPI_DEV_INFO_TYPE_DEV_STATE:
      zabZnpNwk_deviceStateHandler(Status, Service, (ZDO_STATE)data[0]);
      break;
      
    case ZAB_ZNP_SAPI_DEV_INFO_TYPE_DEV_IEEE_ADDR:
      data64 = COPY_IN_64_BITS(data);
      srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_IEEE, 8, (unsigned8*)&data64);
      
      zabZnpOpen_IeeeRspHandler(Status, Service);
      zabZnpNwk_ieeeAddressHandler(Status, Service, data64);
      break;
      
    case ZAB_ZNP_SAPI_DEV_INFO_TYPE_DEV_SHORT_ADDR:
      data16 = COPY_IN_16_BITS(data);
      if (data16 != ZAB_ZNP_NWK_M_NWK_ADDR_NO_PREVIOUS_SETTINGS)
        {
          srvCoreGiveBack16(Status, Service, ZAB_GIVE_NWK_ADDRESS, data16);
        }
      zabZnpNwk_nwkAddrHandler(Status, Service, COPY_IN_16_BITS(data));
      break;
      
    case ZAB_ZNP_SAPI_DEV_INFO_TYPE_PARENT_SHORT_ADDR:
      zabZnpNwk_parentNwkAddrHandler(Status, Service, COPY_IN_16_BITS(data));
      break;
       
    case ZAB_ZNP_SAPI_DEV_INFO_TYPE_PARENT_IEEE_ADDR:
      zabZnpNwk_parentIeeeAddressHandler(Status, Service, COPY_IN_64_BITS(data));
      break;
      
    case ZAB_ZNP_SAPI_DEV_INFO_TYPE_ZB_CHANNEL:
      srvCoreGiveBack8(Status, Service, ZAB_GIVE_NWK_CHANNEL_NUMBER, data[0]);
      zabZnpNwk_channelHandler(Status, Service, data[0]);
      break;
      
    case ZAB_ZNP_SAPI_DEV_INFO_TYPE_ZB_PAN_ID:
      data16 = COPY_IN_16_BITS(data);
      srvCoreGiveBack16(Status, Service, ZAB_GIVE_NWK_PAN_ID, data16);
      zabZnpNwk_panIdlHandler(Status, Service, data16);
      break;
      
    case ZAB_ZNP_SAPI_DEV_INFO_TYPE_ZB_EXTENDED_PAN_ID:
      data64 = COPY_IN_64_BITS(data);
      srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_NWK_EPID, 8, (unsigned8*)&data64);
      zabZnpNwk_extendedPanIdHandler(Status, Service, data64);
      break;
      
    case ZAB_ZNP_SAPI_DEV_INFO_TYPE_KEY_SEQUENCE_NUMBER:
      zabZnpNwk_nwkKeySequenceNumberHandler(Status, Service, *data);
      break;
      
    case ZAB_ZNP_SAPI_DEV_INFO_TYPE_MEM_METRICS:
      data16 = COPY_IN_16_BITS(data);      
      printApp(Service, "ZNP Memory Usage: Current = %d, ", data16);
      data16 = COPY_IN_16_BITS(data+2);
      printApp(Service, "High Water = %d\n", data16);
      break;
      
    default:
        printError(Service, "ZAN ZNP SAPI: Unknown device info id 0x%02X\n", msgData->paramId);
      break;
  }
}

/******************************************************************************
 * Process incoming Read Configuration response
 * 
 * WARNING: THRE IS NO REQUEST FOR THIS RESPONSE!!!!!!
 ******************************************************************************/
static void zabZnp_ProcessSapiReadConfiguration(erStatus* Status, zabService* Service, sapMsg* Message)
{
  zabznpSapi_ZB_READ_CONFIGURATION_RESP* rsp;
  unsigned8 len;
  unsigned8* dp;
  unsigned8 buffer[20];
  unsigned8 index;

  osMemZero(Status, buffer, 20);

  ER_CHECK_STATUS_NULL(Status, Message);

  /* Get the response data cast into a structure*/
  zabParseZNPGetData(Status, Message, &len, (unsigned8 **)&rsp);
  ER_CHECK_STATUS_NULL(Status, rsp);
  
  // Confirm we have enough data for the status, config id, length and a value of data
  if ( (len >= (sizeof(zabznpSapi_ZbStatusResp) + zabznpSapi_ZbConf_SIZE + 1) ) &&
       (rsp->Status.status == ZNPI_API_ERR_SUCCESS) )
    {
      dp = rsp->Conf.Value;
      
      switch ((znpSapiConfigId)rsp->Conf.configId) 
        {
          case ZAB_ZNP_SAPI_NV_CFG_ID_LOGICAL_TYPE:
            zabZnpNwk_deviceLogicalTypeHandler(Status, Service, (znpSapiLogicalType)dp[0]);
            break;
            
          case ZAB_ZNP_SAPI_NV_CFG_ID_NWKKEY:
            /* Developer Mode: Easy key access */
            if (rsp->Conf.Length >= 17)
              {
                printApp(Service, "NwkKey: SeqNum = 0x%02X, Key =", dp[0]);
                for (index = 0; index < 16; index++)
                  {
                    printApp(Service, " %02X", dp[index+1]);
                  }
                printApp(Service, "\n");
              }
            break;
            
          case ZAB_ZNP_SAPI_NV_CFG_ID_PRECFGKEY:
            /* Developer Mode: Easy key access */
            printApp(Service, "PreCfgKey =");
            for (index = 0; index < rsp->Conf.Length; index++)
              {
                printApp(Service, " %02X", dp[index]);
              }
            printApp(Service, "\n");
            break;
            
          default:
            break;
        }
    }
  else
    {
      // Let the nwk state machine know we got a response but it was invalid.
      zabZnpNwk_srspTimeoutHandler(Status, Service);
    }
}



/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Process all messages of subsystem SAPI
 ******************************************************************************/
void zabZnpSapiProcessMsg(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8 cmd;
  ZNPI_API_ERROR_CODES result;

  zabParseZNPGetCommandID(Status, Message, &cmd);

  switch (cmd)
    {
      case ZNPI_CMD_ZB_WRITE_CONFIGURATION:
        printVendor(Service, "zabZnpSapiUtility: Write Cfg Rsp\n");
        zabZnp_ProcessStatus(Status, Service, Message);      
        getSapiStatus(Status, Message, &result);
        zabZnpNwk_writeCfgRspHandler(Status, Service, result);
        break;

      case ZNPI_CMD_ZB_GET_DEVICE_INFO:
      case ZNPI_CMD_ZB_DEVICE_INFO_NOTIFICATION:
        zabZnp_ProcessSapiDeviceInfo(Status, Service, Message);
        break;

      case ZNPI_CMD_ZB_READ_CONFIGURATION:
        zabZnp_ProcessSapiReadConfiguration(Status, Service, Message);
        break;

      default:
        printError(Service, "zabZnpSapiUtility: WARNING: unknown  ZNP message 0x%02X\n", cmd);
        break;
    }
}


/******************************************************************************
 * Get Device Info
 * 
 * Parameters:
 *   deviceInfoParam - The identifier of the Device Info parameter to be read
 ******************************************************************************/
void znpi_sapi_getDeviceInfo( erStatus* Status, zabService* Service, znpSapiDeviceInfoType deviceInfoParam)
{  
  sapMsg* Msg = NULL;
  zabznpSapi_ZbDeviceInfo* data;

  // Allocate Msg ZNP
  Msg = zabParseZNPCreateRequest(Status, Service, zabznpSapi_ZbDeviceInfo_SIZE);
  ER_CHECK_STATUS_NULL(Status, Msg);
  
  printVendor(Service, "zabZnpSapiUtility: Get Device Info Req - Param 0x%02X\n", (unsigned8)deviceInfoParam);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SAPI);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZB_GET_DEVICE_INFO);
  zabParseZNPSetLength(Status, Msg, zabznpSapi_ZbDeviceInfo_SIZE);

  // Set Parameter Id
  data = (zabznpSapi_ZbDeviceInfo*)ZAB_MSG_ZNP_DATA(Msg);
  data->paramId = (unsigned8)deviceInfoParam;

  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * Read Configuration Data
 * 
 * Parameters:
 *   configId - The identifier for the configuration property
 ******************************************************************************/
#define M_ZNP_READ_CFG_LENGTH 1
void znpi_sapi_read_config( erStatus* Status, zabService* Service, znpSapiConfigId configId)
{
  sapMsg* Msg = NULL;
  unsigned8 *msgData;

  // Allocate Msg ZNP
  Msg = zabParseZNPCreateRequest(Status, Service, M_ZNP_READ_CFG_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);
  
  printVendor(Service, "zabZnpSapiUtility: Read Cfg Req - Param 0x%04X\n", (unsigned8)configId);

  // Set Header ZNP
  zabParseZNPSetLength(Status, Msg, M_ZNP_READ_CFG_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SAPI);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZB_READ_CONFIGURATION);

  // Set Parameter
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  *msgData = (unsigned8)configId;
  
  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Write Configuration Data
 * 
 * Parameters:
 *   configId - The identifier for the configuration property
 *   length - Specifies the size of the Value buffer in bytes.
 *   pValue -  pointer to the value, in its native format (unsigned8/16/32)
 ******************************************************************************/
void znpi_sapi_write_config( erStatus* Status, zabService* Service, znpSapiConfigId configId, unsigned8 length, void *pValue)
{
  sapMsg* Msg = NULL;
  zabznpSapi_ZbConf *msgData;
  
  ER_CHECK_STATUS_NULL(Status, pValue);

  // Allocate Msg ZNP
  Msg = zabParseZNPCreateRequest(Status, Service, ZNPI_ZB_WRITE_CONFIGURATION_DATALENGTH(length)/* + length*/);
  ER_CHECK_STATUS_NULL(Status, Msg);
  
  printVendor(Service, "zabZnpSapiUtility: Write Cfg Req - Param 0x%04X\n", (unsigned8)configId);

  // Set Header ZNP
  zabParseZNPSetLength(Status, Msg, ZNPI_ZB_WRITE_CONFIGURATION_DATALENGTH(length));
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SAPI);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZB_WRITE_CONFIGURATION);

  // Set Parameter
  msgData = (zabznpSapi_ZbConf*)ZAB_MSG_ZNP_DATA(Msg);
  msgData->configId = configId;
  msgData->Length = length;

  // Store data, converted from native format
  switch (configId) 
    {
      // 1 byte data
      case ZAB_ZNP_SAPI_NV_CFG_ID_STARTUP_OPTION:
      case ZAB_ZNP_SAPI_NV_CFG_ID_LOGICAL_TYPE:
      case ZAB_ZNP_SAPI_NV_CFG_ID_SECURITY_MODE:
      case ZAB_ZNP_SAPI_NV_CFG_ID_DIRECT_CB:
      case ZAB_ZNP_SAPI_NV_CFG_ID_PASSIVE_ACK_TIMEOUT:
          *msgData->Value =  *((unsigned8*)pValue);
          break;
          
      // Multibyte array
      case ZAB_ZNP_SAPI_NV_CFG_ID_USERDESC:
      case ZAB_ZNP_SAPI_NV_CFG_ID_PRECFGKEY:
          osMemCopy(Status, msgData->Value, (unsigned8*)pValue, length);
          break;
          
      // 16 bit data
      case ZAB_ZNP_SAPI_NV_CFG_ID_PANID:
      case ZAB_ZNP_SAPI_NV_CFG_ID_POLL_RATE:
      case ZAB_ZNP_SAPI_NV_CFG_ID_QUEUED_POLL_RATE:
      case ZAB_ZNP_SAPI_NV_CFG_ID_RESPONSE_POLL_RATE:
          COPY_OUT_16_BITS(msgData->Value, *((unsigned16 *)pValue));
          break;
          
      // 32bit data
      case ZAB_ZNP_SAPI_NV_CFG_ID_CHANLIST:
          COPY_OUT_32_BITS(msgData->Value, *((unsigned32 *)pValue));
          break;
          
      // 64bit data
      case ZAB_ZNP_SAPI_NV_CFG_ID_EXTENDED_PAN_ID:
          COPY_OUT_64_BITS(msgData->Value, *((unsigned64 *)pValue));
          break;
          
      // Long or raw data - memcpy
      case ZAB_ZNP_SAPI_NV_CFG_ID_NWKKEY:
          osMemCopy(Status, msgData->Value, (unsigned8*)pValue, length);
          break;
          
      default:
        printError(Service, "zabZnpSapi: WARNING: Write Config: ConfigID 0x%02X not supported - your message will fail!\n", configId);
          break;
    }

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
