/*
 Name:    Private Core Shared Definitions
 Author:  ZigBee Excellence Center
 Company: Schneider Electric

 Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

 Description:
    Private for implementation that we do not want to expose outside the module.


 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
*/

#ifndef __ZAB_CORE_PRIVATE_H__
#define __ZAB_CORE_PRIVATE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "zabCoreService.h"

#include "zabUtility.h"
#include "zabCoreEzMode.h"

#include "szl_zab.h"

#include "zabVendorService.h"

#include "sapCorePrivate.h"
#include "sapMsgPrivate.h"
#include "srvCorePrivate.h" 

  

/******************************************************************************
 *                      ******************************
 *                 *****            CONFIG             *****
 *                      ******************************
 ******************************************************************************/
  
/* Maximum delay allowed to be returned by work 
 * Max = 1000 as 1 second tick will require this anyway */
#define ZAB_WORK_DELAY_MAX_MS 1000
  
  
/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Direction management for free message buffers
 * There are queues of free messages for data in and out.
 * Unused buffers sit on those queues and are used when messages are created/received.
 ******************************************************************************/
typedef enum {
  ZAB_FREE_DIRECTION_IN  = 0,
  ZAB_FREE_DIRECTION_OUT = 1,
  ZAB_FREE_DIRECTION_COUNT 
} ZAB_FREE_DIRECTION;
#define ZAB_FREE_DIRECTION( d ) ((d) == SAP_MSG_DIRECTION_IN ? ZAB_FREE_DIRECTION_IN : ZAB_FREE_DIRECTION_OUT)


/******************************************************************************
 * Core Service Data.
 * zabService* actually refers to this struct, but it is hidden form the user.
 * All data required by the core or its services must be referenced from here.
 * An instance of this is created for each network.
 ******************************************************************************/
typedef struct
{
  srvStruct  Service;           // THIS MUST BE THE FIRST FIELD
  unsigned8  MutexId;           // this identifier is created never changes (so can be read, but not written!)
  zabOpenState openState;
  zabNwkState nwkState;
  unsigned8 channel;
  unsigned16 nwkAddress;
  unsigned16 panID;
  unsigned64 epid;
  unsigned64 ieee;
  zab_bool workActive;          // zab_true if work is active. Used for detecting multi-threaded nastiness
  
  // Last asked instance of Mutex
  unsigned8  MutexIdInstance;

  sapHandle ManageSAP;
  sapHandle DataSAP;
  sapHandle SerialSAP;

  // Queues
  qSafeType* FreeCoreQ;         // Free event (Action/Notification) buffers
  qSafeType VendorOutQ;         // Messages ready to be sent by vendor
  qSafeType VendorInQ;          // Received messages waiting to be processed by the vendor
  qSafeType* FreeMsgQ[ ZAB_FREE_DIRECTION_COUNT ][ SAP_MSG_POOL_COUNT ]; // Free message buffers (in and out, long and short)
   
#ifdef ZAB_CORE_M_ENABLE_QUEUE_PROFILING
  unsigned8 FreeMsgQSize[ ZAB_FREE_DIRECTION_COUNT ][ SAP_MSG_POOL_COUNT ]; 
  unsigned8 FreeMsgQMinFreeCount[ ZAB_FREE_DIRECTION_COUNT ][ SAP_MSG_POOL_COUNT ]; 
  unsigned16 FreeMsgQFullCount[ ZAB_FREE_DIRECTION_COUNT ][ SAP_MSG_POOL_COUNT ];  
#endif
  
  void* Vendor;             // Vendor service data. This is actually a zabVendorServiceStruct*
  
#ifdef ZAB_CFG_ENABLE_SZL
  szlZabServiceType szlService;
#endif
  
#ifdef ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING
  zabCoreEzModeInfo_t ezModeInfo;  // EZ Mode Data
#endif
  
} zabServiceStruct;
/* Macro for converting zabService* into zabServiceStruct* and hence accessing the data hidden from the user */
#define ZAB_SRV( s ) ((zabServiceStruct*)(s))


/******************************************************************************
 * Mutex Usage Macros
 ******************************************************************************/
// enters a mutex if id > 0
#define ZAB_MUTEX_ENTER( Service, id ) while (id) { ZAB_PROTECT_ENTER( (void*)(Service), (unsigned8)(id) ); break; }
// exits a mutex if id > 0
#define ZAB_MUTEX_EXIT(  Service, id ) while (id) { ZAB_PROTECT_EXIT(  (void*)(Service), (unsigned8)(id) ); break; }


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/ 

/******************************************************************************
 * Free all items in a queue
 ******************************************************************************/
extern 
void zabCoreFreeQueue( erStatus* Status, qSafeType* Queue );

/******************************************************************************
 * Allocate a message for the Application.
 * Used by data and serial SAPs.
 * Gets a spare buffer from one of the free queues
 ******************************************************************************/
extern 
void* zabCoreAllocateApp( erStatus* Status, sapHandle Sap, sapMsgType Type, sapMsgDirection Direction, unsigned32 DataLength );

/******************************************************************************
 * Free a message from the Application.
 * Used by data and serial SAPs.
 * Puts buffer back onto the free queues
 ******************************************************************************/
extern
void zabCoreFreeApp( erStatus* Status, sapHandle Sap, sapMsg* Message );


#ifdef ZAB_CORE_M_ENABLE_QUEUE_PROFILING
/******************************************************************************
 * Print info about Queue status for queue profiling
 ******************************************************************************/
extern 
void zabCore_PrintQInfo(zabService* Service);
#endif

#ifdef __cplusplus
}
#endif

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif
