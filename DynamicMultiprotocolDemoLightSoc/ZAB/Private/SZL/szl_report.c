/*******************************************************************************
  Filename    : szl_report.c
  $Date       : 2013-08-02                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : This is the Report functions for the  SZL
 * 
 *  Core Rev      Date     Author  Change Description
 * 002.001.001  29-Apr-15   MvdB   ARTF132259: ReportCheckExpired(): Suspend report generation if zab is not opened & networked
 * 002.002.004  04-Sep-15   MvdB   ARTF146124: Tidy up return of Szl_ReportSendingConfigure() when removing non-existing items
 * 002.002.005  11-Sep-15   MvdB   ARTF146124: Change return to success when removing non-existing items
*******************************************************************************/
#include "szl_config.h"
#include "zabCorePrivate.h"


#include "data_converter.h"
#include "sort.h"
#include "szl_external.h"
#include "szl_zab.h"
#include "szl_timer.h"
#include "szl.h"
#include "szl_report.h"

#include <stdlib.h>

//#define SZL_REPORTING_M_USE_DEFAULT_RESPONSE


#define TIME_INVALID 0xFFFFFFFF

#define SZL_REPORT_CFG_VERSION 0xA0


//SZL_REPORT_ENTRY_t gReports[SZL_CFG_MAX_REPORTS];
#define ZAB_SZL_REPORTS( s ) (ZAB_SRV( s )->szlService.gReports)

// TRUE if the report is a PERIODIC report type
#define REPORT_TYPE_PERIODIC(_r)        (((_r)->Data.Config.Max) != 0 )

// TRUE if MIN is greater than zero
#define REPORT_MIN_CONFIGURED(_r)       (((_r)->Data.Config.Min) != 0 )

// TRUE if periodic report and the MAX timer has exceeded
#define REPORT_MAX_EXCEEDED(_r, _t)     ((REPORT_TYPE_PERIODIC(_r))  && ( ( (_r)->Data.Info.LastReportedTime + (_r)->Data.Config.Max ) <= (_t) ) )

// TRUE if MIN configured and the MIN timer has exceeded
#define REPORT_MIN_EXCEEDED(_r, _t)     ((REPORT_MIN_CONFIGURED(_r)) && ( ( (_r)->Data.Info.LastReportedTime + (_r)->Data.Config.Min ) <= (_t) ) )

// TRUE if Value has changed and needs to be reported NOW
#define REPORT_VALUE_CHANGED(_s, _r, _t)    (ReportValueHasChanged(_s, _r, _t))

// The seconds to the next report should be send
#define REPORT_NEXT_TIMER(_r, _t)       (ReportNextTimer(_r, _t))

// TRUE if config is empty
#define IS_CFG_EMPTY(_pCfg) ((_pCfg)->ClusterID == 0xFFFF && (_pCfg)->AttributeID == 0xFFFF && (_pCfg)->Endpoint == 0xFF)




/**
 * Cfg To Key
 *
 * This function creates a key for the config entry
 */
static szl_uint64 CFG_TO_KEY(SZL_REPORT_CFG_t* dp)
{
    szl_uint64 key;

#if !SUPPORTS_64_BIT_DATATYPES
    key.hi = ((szl_uint32)dp->Endpoint << 8 | dp->ManufacturerSpecific);
    key.lo = ((szl_uint32)dp->ClusterID << 16 | dp->AttributeID);
#else
    key = (szl_uint64)dp->Endpoint << 40 | (szl_uint64)dp->ManufacturerSpecific << 32 | ((szl_uint32)dp->ClusterID << 16 | dp->AttributeID);
#endif

    return key;
}
/**
 * Cfg Compare
 *
 * This function is the compare function for the bubble sort
 */
static int ReportCfgsCompare(const void* a, const void* b)
{
    szl_uint64 da = CFG_TO_KEY(&((SZL_REPORT_ENTRY_t*) a)->Data.Config);
    szl_uint64 db = CFG_TO_KEY(&((SZL_REPORT_ENTRY_t*) b)->Data.Config);

#if !SUPPORTS_64_BIT_DATATYPES
    if (da.hi == db.hi)
        return (da.lo > db.lo) - (da.lo < db.lo);
    else
        return (da.hi > db.hi) - (da.hi < db.hi);
#else
    return (da > db) - (da < db);
#endif
}

szl_uint8 addReportToZCL(zabService* Service, SZL_REPORT_ENTRY_t * pReport, szl_uint8* zcl, szl_uint8 maxLength)
{
  //SZL_DataPoint_t* dp;
  szl_uint8 index;
  //szl_uint8 bob;
  
  
  szl_uint8 data_type;
  szl_uint8 data_length = SZL_CFG_MAX_REPORTABLE_ATTR_LENGTH;
//  NATIVE_DATA_t dataNative; 
  szl_uint8 datazb[SZL_CFG_MAX_REPORTABLE_ATTR_LENGTH];
  SZL_STATUS_t szlStatus;
  
  /* Validate inputs. Min length = 4 bytes: AttrId, Data Type + 1 byte of data */
  if ( (pReport == NULL) || (zcl == NULL) || (maxLength < 4))
    {
      return 0;
    }
 
  //dp = Szl_DataPointFind(pReport->Data.Config.Endpoint, pReport->Data.Config.ClusterID, pReport->Data.Config.AttributeID, szl_true);

  /* Get the current value and check its ok */
  szlStatus = Szl_DataPointReadZB(Service, 
                                  pReport->Data.Config.Endpoint,
                                  pReport->Data.Config.ManufacturerSpecific,
                                  pReport->Data.Config.ClusterID,  
                                  pReport->Data.Config.AttributeID,
                                  &data_type,
                                  (void*)datazb,
                                  &data_length);
  if ( (szlStatus != SZL_STATUS_SUCCESS) || (data_type != pReport->Data.Config.DataType) )
    {
      printError(Service, "SZL Report: ERROR from Szl_DataPointReadZB or DataType mismatch when trying to add Cl 0x%04X Attr 0x%04X to report\n",
                 pReport->Data.Config.ClusterID,
                 pReport->Data.Config.AttributeID);
      return 0;
    }
  
  /* If we have enough space, add the AttrId, Data Type and data. If not, do not add */
  index = 0;
  if (maxLength >= (3 + data_length))
    {
      COPY_OUT_16_BITS(&zcl[index], pReport->Data.Config.AttributeID);
      index += 2;
      zcl[index++] = pReport->Data.Config.DataType;
      szl_memcpy(&zcl[index], datazb, data_length);
      index += data_length;
      
      
      // TODO - It might be worth just doing a ZBtoNative for the last reported value here, but then we don't actually know it was sent

      return index;
    }

  return 0;
}

/**
 * Reportable Change Exceeded
 *
 * This function checks to see if a reportable change has been exceeded
 */
static szl_bool ReportableChangeExceeded(szl_uint8 DataType, NATIVE_DATA_t* ReportableChange, NATIVE_DATA_t* LastReportedValue, NATIVE_DATA_t* CurrentValue)
{
    szl_bool exceeded = szl_false;

    switch (NativeDataSize(DataType, NULL))
    {
        case 1:
        case 2:
        case 4:
            if (DataTypeIsSigned(DataType))
                exceeded = labs(CurrentValue->data.primitive32 - LastReportedValue->data.primitive32) >= labs(ReportableChange->data.primitive32);
            else
                exceeded = (CurrentValue->data.primitive32 - LastReportedValue->data.primitive32) >= ReportableChange->data.primitive32;
            break;

        case 8:
            // Use special math func24ons since also runs on 32bit processors
            if (DataTypeIsSigned(DataType))
            {
                szl_int64 diff = labs(CurrentValue->data.primitive64_signed - LastReportedValue->data.primitive64_signed);

                exceeded = diff > labs(ReportableChange->data.primitive64_signed);
            }
            else
            {
              if (CurrentValue->data.primitive64 > LastReportedValue->data.primitive64)
                {
                  exceeded = ((CurrentValue->data.primitive64 - LastReportedValue->data.primitive64) > ReportableChange->data.primitive64);
                }
              else if (LastReportedValue->data.primitive64 > CurrentValue->data.primitive64)
                {
                  exceeded = ((LastReportedValue->data.primitive64 - CurrentValue->data.primitive64) > ReportableChange->data.primitive64);
                }
                
            }
            break;

    }

    return exceeded;
}

/**
 * Report Value Has Changed
 *
 * This function checks if a report is to report on change and the data change fulfills a report to be send
 */
static szl_bool ReportValueHasChanged(zabService* Service, SZL_REPORT_ENTRY_t* pReport, szl_uint32 timeNow )
{
  SZL_DataPoint_t* dp;
  NATIVE_DATA_t dpData;
  szl_uint8 data_type;
  szl_uint8 length = sizeof(NATIVE_DATA_t);

    if (!REPORT_MIN_EXCEEDED(pReport, timeNow))
    {
        return szl_false;
    }

    dp = Szl_DataPointFind(Service, pReport->Data.Config.Endpoint, pReport->Data.Config.ManufacturerSpecific, pReport->Data.Config.ClusterID, pReport->Data.Config.AttributeID, szl_true);

    // validate the datapoint
    if (dp == NULL || dp->DataType != pReport->Data.Config.DataType)
    {
        return szl_false;
    }


    // get native data from the datapoint
    Szl_DataPointReadNative(Service, pReport->Data.Config.Endpoint, pReport->Data.Config.ManufacturerSpecific, pReport->Data.Config.ClusterID, pReport->Data.Config.AttributeID, &data_type, &dpData, &length);

    // validate size of data
    if (length != NativeDataSize(pReport->Data.Config.DataType, NULL))
    {
        return szl_false;
    }

    // if report on change value specified - check to see if it has exceeded the change value
    if (pReport->Data.Config.ReportOnChange)
    {
        return ReportableChangeExceeded( pReport->Data.Config.DataType, &pReport->Data.Config.ReportableChange, &pReport->Data.Info.LastReportedValue, &dpData);
    }

    // otherwise report if changed since last reported value
    // Note: This looks a bit unsafe, as it relies on the union being packed in a way that length makes sense.....
    return (memcmp(pReport->Data.Info.LastReportedValue.data.raw, dpData.data.raw, length) != 0);
}


/**
 * Send all reports marked for sending
 */
static void ReportGeneratorSend(zabService* Service)
{
  unsigned8 zclCmd[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
  unsigned8 zclCmdIndex;
  SZL_Addresses_t  destAddr;
  szl_uint16 i, j;
  szl_uint8 addedLength;
  szl_uint8 tid;
  SZL_RESULT_t result;
  SZL_STATUS_t szlStatus;
  szl_uint8 data_type;
  szl_uint8 data_length;
  
  /* All reports are sent via the binding table */
  destAddr.AddressMode = SZL_ADDRESS_MODE_VIA_BIND;
  
  /*
   * Parse through each report
   * If it needs to send now:
   *   - Build ZCL header and add attribute. Clear flag.
   *   - While there is still room left in the ZCL body:
   *     Parse through the rest of the list looking for attributes that need to send now and have 
   *     a matching EP and Cluster. If found, add to ZCL body and clear send flag.
   *   - Send via bind
   */
  
  for (i=0; i < SZL_CFG_MAX_REPORTS; i++)
    {
      if ( !(ZAB_SZL_REPORTS(Service)[i].Status & REPORT_SEND_NOW) )
        {
          continue;
        }      
      
      /* Parse the ZCL header, leaving index pointing to the start of the data */
      if (ZAB_SZL_REPORTS(Service)[i].Data.Config.ManufacturerSpecific)
        {
#ifdef SZL_REPORTING_M_USE_DEFAULT_RESPONSE
          zclCmd[0] = ZCL_FRAME_TYPE_PROFILE_CMD | ZCL_FRAME_CONTROL_DIRECTION_SERVER_TO_CLIENT | ZCL_FRAME_CONTROL_MANU_SPECIFIC;
#else        
          zclCmd[0] = ZCL_FRAME_TYPE_PROFILE_CMD | ZCL_FRAME_CONTROL_DIRECTION_SERVER_TO_CLIENT | ZCL_FRAME_CONTROL_MANU_SPECIFIC | ZCL_FRAME_CONTROL_DISABLE_DEFAULT_RSP;
#endif
          COPY_OUT_16_BITS(&zclCmd[1], ZB_SCHNEIDER_MANUFACTURE_ID);
          zclCmdIndex = 3;
        }
      else
        {
#ifdef SZL_REPORTING_M_USE_DEFAULT_RESPONSE
          zclCmd[0] = ZCL_FRAME_TYPE_PROFILE_CMD | ZCL_FRAME_CONTROL_DIRECTION_SERVER_TO_CLIENT;
#else       
          zclCmd[0] = ZCL_FRAME_TYPE_PROFILE_CMD | ZCL_FRAME_CONTROL_DIRECTION_SERVER_TO_CLIENT | ZCL_FRAME_CONTROL_DISABLE_DEFAULT_RSP;
#endif
          zclCmdIndex = 1;
        }

      /* Transaction ID and Cmd ID */ 
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return;
        }
      zclCmd[zclCmdIndex++] = tid;
      zclCmd[zclCmdIndex++] = ZCL_CMD_ID_REPORT_ATTRIBUTES; 
      
      /* Add report payload */     
      /* Start by adding the item at i, then look for more for the same EP/cluster */
      for (j = i; j < SZL_CFG_MAX_REPORTS; j++)
        {
          if (ZAB_SZL_REPORTS(Service)[j].Status & REPORT_SEND_NOW)
            {
              if ( (ZAB_SZL_REPORTS(Service)[j].Data.Config.ManufacturerSpecific == ZAB_SZL_REPORTS(Service)[i].Data.Config.ManufacturerSpecific) &&
                   (ZAB_SZL_REPORTS(Service)[j].Data.Config.ClusterID == ZAB_SZL_REPORTS(Service)[i].Data.Config.ClusterID) &&
                   (ZAB_SZL_REPORTS(Service)[j].Data.Config.Endpoint == ZAB_SZL_REPORTS(Service)[i].Data.Config.Endpoint) )
                {                
                  addedLength = addReportToZCL(Service, &ZAB_SZL_REPORTS(Service)[j], &zclCmd[zclCmdIndex], SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH - zclCmdIndex);
                  if (addedLength > 0)
                    {
                      zclCmdIndex += addedLength;
                      
                      /* Flag that we are trying to send this report. If it is successful we will then clear this and SEND_NOW */
                      ZAB_SZL_REPORTS(Service)[j].Status |= REPORT_SEND_IN_PROGRESS;
                      
                      /* There is not enough space to fit another report, so break out */
                      if ( (SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH - zclCmdIndex) < 4)
                        {
                          j = SZL_CFG_MAX_REPORTS;
                        }
                    }
                  else if ( (SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH - zclCmdIndex) >= (3 + SZL_CFG_MAX_REPORTABLE_ATTR_LENGTH))
                    {
                      /* There was a problem forming this attribute's data (probably too long)
                       * AND it we have enough space for the AttrId + DataType + SZL_CFG_MAX_REPORTABLE_ATTR_LENGTH, so it will never fit.
                       * Give up on this report and delay until the next timeout, otherwise we try to send it every second */
                      ZAB_SZL_REPORTS(Service)[j].Status &= ~(REPORT_SEND_NOW | REPORT_SEND_IN_PROGRESS);
                      ZAB_SZL_REPORTS(Service)[j].Data.Info.LastReportedTime = Szl_TimeNow(Service);
                    }
                }            
            }
        }
            
      /* Suppress transmission in case where the addReportToZCL failed and we only have the header */
      if (zclCmdIndex > 5)
        {      
          /* Send command.
           * If it is successful we want to clear any reports with REPORT_SEND_NOW & REPORT_SEND_IN_PROGRESS as they have been sent.
           * IF it fails, we just want to clear REPORT_SEND_IN_PROGRESS and let the report try to be generated next second.
           * This will help significantly with out buffer usage in ZAB, as we will now not drop reports, just delay them */
          result = ZclM_SendCommand(Service,
                                    ZAB_SZL_REPORTS(Service)[i].Data.Config.Endpoint,
                                    &destAddr,
                                    ZAB_SZL_REPORTS(Service)[i].Data.Config.ClusterID,
                                    tid,
                                    zclCmdIndex,
                                    zclCmd);
          if (result == SZL_RESULT_SUCCESS)
            {
              for (j = i; j < SZL_CFG_MAX_REPORTS; j++)
                {
                  if ( (ZAB_SZL_REPORTS(Service)[j].Status & REPORT_SEND_IN_PROGRESS) > 0)
                    {
                      ZAB_SZL_REPORTS(Service)[j].Status &= ~(REPORT_SEND_NOW | REPORT_SEND_IN_PROGRESS);
                      ZAB_SZL_REPORTS(Service)[j].Data.Info.LastReportedTime = Szl_TimeNow(Service);

                      /* Update last reported value to current value if it is reporting on change */
                      if (ZAB_SZL_REPORTS(Service)[j].Data.Config.ReportOnChange)
                        {
                          data_length = sizeof(NATIVE_DATA_t);
                          szlStatus = Szl_DataPointReadNative(Service, 
                                                              ZAB_SZL_REPORTS(Service)[j].Data.Config.Endpoint,
                                                              ZAB_SZL_REPORTS(Service)[j].Data.Config.ManufacturerSpecific,
                                                              ZAB_SZL_REPORTS(Service)[j].Data.Config.ClusterID,
                                                              ZAB_SZL_REPORTS(Service)[j].Data.Config.AttributeID,
                                                              &data_type,
                                                              (void*)ZAB_SZL_REPORTS(Service)[j].Data.Info.LastReportedValue.data.raw,
                                                              &data_length);
                          if ( (szlStatus != SZL_STATUS_SUCCESS) || (data_type != ZAB_SZL_REPORTS(Service)[j].Data.Config.DataType) )
                            {
                              printError(Service, "SZL Report: ERROR - Last updated value no good\n");
                            }
                        }
                    }
                }  
            }
          else
            {
              printError(Service, "szl_report: ERROR - Report failed with status 0x%02X. Delaying to next tick.\n", result);
              for (j = i; j < SZL_CFG_MAX_REPORTS; j++)
                {
                  if ( (ZAB_SZL_REPORTS(Service)[j].Status & REPORT_SEND_IN_PROGRESS) > 0)
                    {
                      ZAB_SZL_REPORTS(Service)[j].Status &= ~REPORT_SEND_IN_PROGRESS;
                    }
                }  

              /* If we ran out of buffers, then return now as trying more is a waste of time */
              if (result == SZL_RESULT_INTERNAL_LIB_ERROR)
                {
                  return;
                }
            }          
        }
    }
}




/**
 * Check Expired
 *
 * This function checks the reports to see if one or more has expired
 * In case of reports expired it adds the local task to be handled in the Process Handler
 * otherwise it starts a timer to expire when the next report is due.
 */
static void ReportCheckExpired(zabService* Service)
{
  szl_bool sendReportNow = szl_false;
//szl_uint16 nextReportTimerTick = 0xFFFF;
  szl_uint16 i;
  szl_uint32 timeNow = Szl_TimeNow(Service);
    
  // Only process reports if we are open and networked. Otherwise it is a waste of effort.
  if (Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED ) == SZL_RESULT_SUCCESS)
    {
      for (i=0; i < SZL_CFG_MAX_REPORTS; i++)
      {
          if (!(ZAB_SZL_REPORTS(Service)[i].Status & REPORT_CONFIGURED))
          {
              continue;
          }

          if ( ZAB_SZL_REPORTS(Service)[i].Data.Info.LastReportedTime == TIME_INVALID ||      // OR - never reported before
               REPORT_MAX_EXCEEDED(&ZAB_SZL_REPORTS(Service)[i], timeNow) ||                  // OR - MAX has exceeded
               REPORT_VALUE_CHANGED( Service, &ZAB_SZL_REPORTS(Service)[i], timeNow))                  // OR - Value has to be reported OnChange
          {
              ZAB_SZL_REPORTS(Service)[i].Status |= REPORT_SEND_NOW;                          // Then - Set to be reported now
              printInfo(Service, "SZL Report: report due for Ep 0x%02X, Cluster 0x%04X, Att 0x%04X\n",
                          ZAB_SZL_REPORTS(Service)[i].Data.Config.Endpoint,
                          ZAB_SZL_REPORTS(Service)[i].Data.Config.ClusterID,
                          ZAB_SZL_REPORTS(Service)[i].Data.Config.AttributeID);
          }

          if (ZAB_SZL_REPORTS(Service)[i].Status & REPORT_SEND_NOW)
          {
              sendReportNow = szl_true;
          }
      }

      if (sendReportNow)
        {
            ReportGeneratorSend(Service);
        }
    }
}


/**
 * Timer Expired
 *
 * This function is called when the "Next Report Timer" expires
 * It then add a task to check if any reports has expired.
 * The task is processed in the ReportProcessHandler.
 */
static void ReportTimerExpired(zabService* Service, szl_uint8 ID, szl_uint32 duration)
{
  ReportCheckExpired(Service);
}

/**
 * Initialize
 *
 * This function has to be called before the report functionality will be available
 */
void Szl_ReportInitialize(zabService* Service)
{
  szl_uint8 ReportTimer;
  szl_uint16 i;
    szl_memset(ZAB_SZL_REPORTS(Service), 0xFF, sizeof(ZAB_SZL_REPORTS(Service)));

    for (i=0; i < SZL_CFG_MAX_REPORTS; i++)
    {
        //ZAB_SZL_REPORTS(Service)[i].NV.Key = i + NV_REPORT_CFG_FIRST_ID;
        //ZAB_SZL_REPORTS(Service)[i].NV.Task = NV_READ;
        ZAB_SZL_REPORTS(Service)[i].Status = REPORT_EMPTY;
    }

    // register the timer
    //Szl_ExtTimerUnregister(ReportTimer);
    printInfo(Service, "SZL Report: Timer registered\n");
    ReportTimer = Szl_ExtTimerRegister(Service, szl_true, 1, ReportTimerExpired);
    if (Szl_ExtTimerStart(Service, ReportTimer) != SZL_RESULT_SUCCESS)
      {
        printError(Service, "SZL Report: ERROR - Timer start failed\n");
      }

    // we register the REPORT as an INTERNAL PLUGIN
 //   SZLPlugin_RegisterLocal(SZL_PLUGIN_REPORT, ReportTaskHandler, ReportProcessHandler);

    //initialized = true;
}


/**
 * Find Entry
 *
 * This function finds a report entry matching the endpoint, clusterID and attributeID
 * If no match is found, the first empty record will be returned if the ReturnFirstEmpty is set to true
 */
static SZL_REPORT_ENTRY_t* ReportFindEntry(zabService* Service, SZL_EP_t Endpoint, szl_bool ManufacturerSpecific, szl_uint16 ClusterID, szl_uint16 AttributeID, szl_bool ReturnFirstEmpty)
{
  szl_uint16 i;
  
    // find configuration in the table or the first empty slot
    SZL_REPORT_ENTRY_t* pReport = NULL;
    for (i=0; i < SZL_CFG_MAX_REPORTS; i++)
    {
        if ((ReturnFirstEmpty && ZAB_SZL_REPORTS(Service)[i].Status == REPORT_EMPTY) ||
            ((ZAB_SZL_REPORTS(Service)[i].Status & REPORT_CONFIGURED) &&
             ZAB_SZL_REPORTS(Service)[i].Data.Config.ManufacturerSpecific == ManufacturerSpecific &&
             ZAB_SZL_REPORTS(Service)[i].Data.Config.ClusterID == ClusterID &&
             ZAB_SZL_REPORTS(Service)[i].Data.Config.AttributeID == AttributeID &&
             ZAB_SZL_REPORTS(Service)[i].Data.Config.Endpoint == Endpoint ) )
        {
            pReport = &ZAB_SZL_REPORTS(Service)[i];
            break;
        }
    }

    return pReport;
}

/**
 * Sending Configure
 *
 * This function adds/updates or deletes a Report Configuration send to it via ZigBee.
 * Setting Max = 0xFFFF causes report to be deleted.
 */
SZL_STATUS_t Szl_ReportSendingConfigure(zabService* Service, 
                                        SZL_EP_t Endpoint, 
                                        szl_bool ManufacturerSpecific, 
                                        szl_uint16 ClusterID, 
                                        szl_uint16 AttributeID, 
                                        szl_uint8 DataType, 
                                        szl_uint16 Min, 
                                        szl_uint16 Max, 
                                        NATIVE_DATA_t* ReportableChangeValue)
{
    SZL_DataPoint_t* dp = Szl_DataPointFind(Service, Endpoint, ManufacturerSpecific, ClusterID, AttributeID, szl_true);
    SZL_REPORT_ENTRY_t* pReport = NULL;
    SZL_STATUS_t status = SZL_STATUS_SUCCESS;
    szl_bool searchForEmptySlot;

    if (dp == NULL /*|| !initialized*/ )
    {
        return SZL_STATUS_UNSUPPORTED_ATTRIBUTE;
    }

    if (ManufacturerSpecific != dp->Property.ManufacturerSpecific || dp->Property.Direction != DP_DIRECTION_SERVER)
    {
        return SZL_STATUS_UNREPORTABLE_ATTRIBUTE;
    }

    if (DataType != dp->DataType)
    {
        return SZL_STATUS_INVALID_DATA_TYPE;
    }

    // find configuration in the table or the first empty slot if adding
    if (Max == 0xFFFF)
      {
        searchForEmptySlot = szl_false;
      }
    else
      {
        searchForEmptySlot = szl_true;
      }
    pReport = ReportFindEntry(Service, Endpoint, ManufacturerSpecific, ClusterID, AttributeID, searchForEmptySlot);

    /* Report not found. If adding return insufficient space. If deleting return sucess as it is not there any way! */
    if (!pReport)
      {
        if (Max == 0xFFFF)
          {
            return SZL_STATUS_SUCCESS;
          }
        else
          {
            return SZL_STATUS_INSUFFICIENT_SPACE;
          }
      }

//    if (pReport->NV.Task & NV_READ)
//        return SZL_STATUS_ACTION_DENIED;

   // szl_bool configAdded = szl_false;

    if (Max == 0xFFFF)
    {
        // delete the configuration
        if (pReport->Status != REPORT_EMPTY)
        {
            // clear all data
            szl_memset(&pReport->Data, 0xFF, sizeof(pReport->Data));
            pReport->Status = REPORT_EMPTY;
 //           pReport->NV.Task = NV_DELETE;
        }
    }
    else
    {
        if (Max > 0 && Min > Max)
        {
            if (pReport->Status & REPORT_CONFIGURED)
            {
                // clear all data
                szl_memset(&pReport->Data, 0xFF, sizeof(pReport->Data));
                pReport->Status = REPORT_EMPTY;
  //              pReport->NV.Task = NV_DELETE;
#ifdef _REPORT_DEBUG_
                Szl_Trace("REPORT DELETED:\tcluster/attribute:", (char*)ltoa(ClusterID, 16), (char*)ltoa(AttributeID, 16));
#endif
            }
            status = SZL_STATUS_INVALID_VALUE;
        }
        else
        {
            pReport->Status = REPORT_CONFIGURED;
 //           pReport->NV.Task = NV_WRITE;

            // create or update configuration
            if (IS_CFG_EMPTY(&pReport->Data.Config))
            {
                // configuration created, update key values
                pReport->Data.Config.Version = SZL_REPORT_CFG_VERSION;
                pReport->Data.Config.ClusterID = ClusterID;
                pReport->Data.Config.AttributeID = AttributeID;
                pReport->Data.Config.Endpoint = Endpoint;
                pReport->Data.Config.ManufacturerSpecific = ManufacturerSpecific;
                pReport->Data.Config.DataType = dp->DataType;
                pReport->Data.Info.LastReportedTime = TIME_INVALID; // report NOW
#ifdef _REPORT_DEBUG_
                Szl_Trace("REPORT ADDED:\tcluster/attribute:", (char*)ltoa(ClusterID, 16), (char*)ltoa(AttributeID, 16));
#endif
            }
#ifdef _REPORT_DEBUG_
            else
            {
                Szl_Trace("REPORT UPDATED:\tcluster/attribute:", (char*)ltoa(ClusterID, 16), (char*)ltoa(AttributeID, 16));
            }
#endif

            // update configuration
            pReport->Data.Info.Postponed = 0;
            pReport->Data.Config.Min = Min;
            pReport->Data.Config.Max = Max;
            pReport->Data.Config.ReportOnChange = szl_false;
            if ( (ReportableChangeValue) &&
                 (NativeDataSize(pReport->Data.Config.DataType, NULL) <= sizeof(NATIVE_DATA_t)) )
              {
                pReport->Data.Config.ReportOnChange = szl_true;
                DCDataSetToZero(&pReport->Data.Config.ReportableChange);
                szl_memcpy(pReport->Data.Config.ReportableChange.data.raw, ReportableChangeValue, NativeDataSize(pReport->Data.Config.DataType, NULL));
              }
#ifdef _REPORT_DEBUG_LOW_
            Szl_Trace((ReportableChangeValue == NULL) ? "\tPeriodic min/max:" : "\tOn change min/max:", (char*)ltoa(Min, 10), (char*)ltoa(Max, 10));
#endif
      //      ReportStartSynchronize(pReport);
      //      configAdded = szl_true;
        }
    }
    
    // sort the table
    bubble_sort(ZAB_SZL_REPORTS(Service), SZL_CFG_MAX_REPORTS, sizeof(SZL_REPORT_ENTRY_t), ReportCfgsCompare);
/*
    if (pReport->NV.Task & NV_WRITE || pReport->NV.Task & NV_DELETE)
    {
        // sort the table
        bubble_sort(ZAB_SZL_REPORTS(Service), SZL_CFG_MAX_REPORTS, sizeof(SZL_REPORT_ENTRY_t), ReportCfgsCompare);
    }

    if (configAdded)
    {
        // start the report timer
        Szl_ExtTimerStartWithDuration(ReportTimer, TIMER_AFTER_CFG);
    }
*/
    return status;
}



/**
 * Read Reporting Sending Configuration
 *
 * This function reads a configuration for sending a report
 */
SZL_STATUS_t Szl_ReadReportingSendingConfiguration(zabService* Service, SZL_EP_t Endpoint, szl_bool ManufacturerSpecific, szl_uint16 ClusterID, szl_uint16 AttributeID, szl_uint8* DataType, szl_uint16* Min, szl_uint16* Max, szl_uint8* RCVLength, void* ReportableChangeValue)
{
    *RCVLength = 0;
    SZL_DataPoint_t* dp = Szl_DataPointFind(Service, Endpoint, ManufacturerSpecific, ClusterID, AttributeID, szl_true);

    if (dp == NULL /*|| !initialized*/ )
        return SZL_STATUS_UNSUPPORTED_ATTRIBUTE;
       
    if (dp->Property.ManufacturerSpecific != ManufacturerSpecific || dp->Property.Direction != DP_DIRECTION_SERVER)  
        return SZL_STATUS_UNREPORTABLE_ATTRIBUTE;

    // find configuration in the table or the first empty slot
    SZL_REPORT_ENTRY_t* pReport = ReportFindEntry(Service, Endpoint, ManufacturerSpecific, ClusterID, AttributeID, szl_false);

    if (!pReport)
        return SZL_STATUS_UNREPORTABLE_ATTRIBUTE;

    // return the configuration in native values
    *DataType = pReport->Data.Config.DataType;
    *Min = pReport->Data.Config.Min;
    *Max = pReport->Data.Config.Max;
    if (pReport->Data.Config.ReportOnChange)
    {
        *RCVLength = NativeDataSize(pReport->Data.Config.DataType, NULL);
        szl_memcpy((char*)ReportableChangeValue, pReport->Data.Config.ReportableChange.data.raw, *RCVLength);
    }

    return SZL_STATUS_SUCCESS;
}
