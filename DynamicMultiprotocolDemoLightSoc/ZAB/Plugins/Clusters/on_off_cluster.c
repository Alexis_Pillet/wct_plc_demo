/*******************************************************************************
  Filename    : identify_cluster.c
  $Date       : 2013-08-20                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : This is the Identify Cluster plugin for the SZL
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    7         20-Jul-15   MvdB   ARTF134686: Update allocation method to be generic and protect against multiple initialisation
 * 002.001.006  27-Jul-15   MvdB   ARTF134686: Standardise on SZL_RESULT_t for plugin init return value
 *                                 ARTF138318: Improve plugin destroy
*******************************************************************************/
/*
*/
#include <stdio.h>
#include <string.h>
#include "zabCoreService.h"

#include "szl_plugin.h"

#include "zabCorePrivate.h"
#include "on_off_cluster.h"

#define ZCL_CLUSTER_ID_GEN_ON_OFF     0x0006


typedef struct
{
  szl_uint8 endpoint;
  szl_bool onOff;
}onOffEndpointParams;

typedef struct
{
  SzlPlugin_OnOffClusterNotification_CB_t OnOffCallback;
  szl_uint8 numEndpoints;
  onOffEndpointParams OnOffEndpoints[VLA_INIT];
}SzlPlugin_OnOff_Params_t;
#define SzlPlugin_OnOff_Params_t_SIZE(numEndpoints) (sizeof(SzlPlugin_OnOff_Params_t) - (sizeof(onOffEndpointParams) * VLA_INIT) + (sizeof(onOffEndpointParams) * (numEndpoints)))


szl_bool SzlOO_ClusterCommandHandler(zabService* Service,
                                     szl_uint8 DestinationEndpoint,
                                     szl_bool manufacturerSpecific,
                                     szl_uint16 ClusterID,
                                     szl_uint8 CmdID,
                                     szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize,
                                     szl_uint8* CmdOutID,
                                     szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize,
                                     szl_uint8 TransactionId,
                                     SZL_Addresses_t SourceAddress,
                                     SZL_ADDRESS_MODE_t DestAddressMode);

/**
 * On/Off cluster attributes used
 */
typedef enum
{
    ATTRID_ON_OFF                = 0x0000,
} ON_OFF_CLUSTER_ATTRIBUTES;

/**
 * Identify cluster commands
 */
typedef enum
{
  COMMAND_OFF                       = 0x00,
  COMMAND_ON                        = 0x01,
  COMMAND_TOGGLE                    = 0x02
} ON_OFF_CLUSTER_CMD_ATTRIBS;

// Cluster Commands
const SZL_ClusterCmd_t SzlOO_clusterCommands[] ={
  {szl_false, SZL_ADDRESS_EP_ALL, ZCL_CLUSTER_ID_GEN_ON_OFF, COMMAND_OFF,     SZL_CLU_OP_TYPE_CUSTOM_CLIENT_TO_SERVER, {.Callback = SzlOO_ClusterCommandHandler}},
  {szl_false, SZL_ADDRESS_EP_ALL, ZCL_CLUSTER_ID_GEN_ON_OFF, COMMAND_ON,      SZL_CLU_OP_TYPE_CUSTOM_CLIENT_TO_SERVER, {.Callback = SzlOO_ClusterCommandHandler}},
  {szl_false, SZL_ADDRESS_EP_ALL, ZCL_CLUSTER_ID_GEN_ON_OFF, COMMAND_TOGGLE,  SZL_CLU_OP_TYPE_CUSTOM_CLIENT_TO_SERVER, {.Callback = SzlOO_ClusterCommandHandler}},
};
#define CLUSTER_COMMANDS_COUNT    ( sizeof(SzlOO_clusterCommands) / sizeof(SZL_ClusterCmd_t))




/**
 * On/Off Cluster Initialize
 *
 * This function will register the cluster and it's attributes
 *
 * If the plugin isn't able to register the attributes the brick will be reset
 *
 */
SZL_RESULT_t SzlPlugin_OnOffClusterInit(zabService* Service, szl_uint8 numEndpoints, szl_uint8 * endpointList, SzlPlugin_OnOffClusterNotification_CB_t callback)
{
    SZL_RESULT_t result;
    szl_uint8 index;
    szl_uint8 commandIndex;
    SzlPlugin_OnOff_Params_t * pluginParams = NULL;
    SZL_DataPoint_t serverAttributes[SZL_CFG_ENDPOINT_CNT];
    SZL_ClusterCmd_t clusterCommands[CLUSTER_COMMANDS_COUNT];

    printInfo(Service, "PLUGIN On/Off: Init\n");

    /* Validate inputs */
    if ( (Service == NULL) || (callback == NULL) || (endpointList == NULL) || (numEndpoints > SZL_CFG_ENDPOINT_CNT))
      {
        return SZL_RESULT_INVALID_DATA;
      }

    /* Malloc parameters the plugin needs to store and link from the SZL */
    result = SZL_PluginRegister(Service, SZL_PLUGIN_ID_ON_OFF, SzlPlugin_OnOff_Params_t_SIZE(numEndpoints), (void**)&pluginParams);
    if ( (result == SZL_RESULT_SUCCESS) && (pluginParams != NULL) )
      {
        pluginParams->OnOffCallback = callback;
        pluginParams->numEndpoints = numEndpoints;

        for (index = 0; index < numEndpoints; index++)
          {
            pluginParams->OnOffEndpoints[index].endpoint = endpointList[index];

            serverAttributes[index].ClusterID = ZCL_CLUSTER_ID_GEN_ON_OFF;
            serverAttributes[index].AttributeID = ATTRID_ON_OFF;
            serverAttributes[index].Endpoint = endpointList[index];
            serverAttributes[index].DataType = SZL_ZB_DATATYPE_BOOLEAN;
            serverAttributes[index].DataSize = sizeof(szl_uint8);

            serverAttributes[index].Property.Access = DP_ACCESS_READ;
            serverAttributes[index].Property.Direction = DP_DIRECTION_SERVER;
            serverAttributes[index].Property.Method = DP_METHOD_DIRECT;
            serverAttributes[index].Property.ManufacturerSpecific = szl_false;

            serverAttributes[index].AccessType.DirectAccess.Data = &(pluginParams->OnOffEndpoints[index].onOff);
          }

        // register the cluster attributes
        result = SZL_AttributesRegister(Service, serverAttributes, numEndpoints);
        if( (result == SZL_RESULT_SUCCESS) || (result == SZL_RESULT_ALREADY_EXISTS) )
          {

            // Register the clusters commands for each endpoint
            szl_memcpy(clusterCommands, SzlOO_clusterCommands, sizeof(clusterCommands));
            for (index = 0; index < numEndpoints; index++)
              {
                for (commandIndex = 0; commandIndex < CLUSTER_COMMANDS_COUNT; commandIndex++)
                  {
                    clusterCommands[commandIndex].Endpoint = endpointList[index];
                  }
                result = SZL_ClusterCmdRegister(Service, clusterCommands, CLUSTER_COMMANDS_COUNT);
                if( (result != SZL_RESULT_SUCCESS) && (result != SZL_RESULT_ALREADY_EXISTS) )
                  {
                    break;
                  }
              }
            if( (result == SZL_RESULT_SUCCESS) || (result == SZL_RESULT_ALREADY_EXISTS) )
              {
                /* All good - assign Id parameters into SZL and cleanup*/
                printInfo(Service, "PLUGIN On/Off: Init Successful\n");
                return SZL_RESULT_SUCCESS;
              }
          }
      }

    /* Failed. Cleanup */
    printError(Service, "PLUGIN On/Off: Init failed\n");
    SZL_PluginUnregister(Service, SZL_PLUGIN_ID_ON_OFF);
    return result;
}

/**
 * On/Off Cluster Destroy
 */
SZL_RESULT_t SzlPlugin_OnOffCluster_Destroy(zabService* Service)
{
  SZL_RESULT_t result;
  szl_uint8 epIndex;
  SZL_EP_t endpoint;
  SzlPlugin_OnOff_Params_t* pluginParams = NULL;
  SZL_DataPoint_t serverAttribute;

  /* Validate inputs */
  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_ON_OFF, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /* Remove commands */
  result = SZL_ClusterCmdDeregister(Service, SzlOO_clusterCommands, CLUSTER_COMMANDS_COUNT);
  if (result != SZL_RESULT_SUCCESS)
    {
      printError(Service, "PLUGIN ON/OFF: WARNING: Deregister commands failed with Result 0x%02X\n",
                 result);
    }

  /* Remove Attributes */
  for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
    {
      /* Get the endpoint number */
      endpoint = pluginParams->OnOffEndpoints[epIndex].endpoint;

      serverAttribute.ClusterID = ZCL_CLUSTER_ID_GEN_ON_OFF;
      serverAttribute.AttributeID = ATTRID_ON_OFF;
      serverAttribute.Endpoint = endpoint;
      serverAttribute.DataType = SZL_ZB_DATATYPE_BOOLEAN;
      serverAttribute.Property.ManufacturerSpecific = szl_false;

      /* De-Register the cluster attributes.
       * Don't care too much about the result as we will make best effort only */
      result = SZL_AttributesDeregister(Service, &serverAttribute, 1);
      if (result != SZL_RESULT_SUCCESS)
        {
          printError(Service, "PLUGIN ON/OFF: WARNING: Deregister failed for EP 0x%02X with Result 0x%02X\n",
                     endpoint,
                     result);
        }
    }

  /* Now unregister the plugin */
  return SZL_PluginUnregister(Service, SZL_PLUGIN_ID_ON_OFF);
}

// -----------------------------------------------------------------------------
// Internal functions
// -----------------------------------------------------------------------------
/*
 * Callback function for the on/off cluster command handler
 */
szl_bool SzlOO_ClusterCommandHandler(zabService* Service,
                                     szl_uint8 DestinationEndpoint,
                                     szl_bool manufacturerSpecific,
                                     szl_uint16 ClusterID,
                                     szl_uint8 CmdID,
                                     szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize,
                                     szl_uint8* CmdOutID,
                                     szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize,
                                     szl_uint8 TransactionId,
                                     SZL_Addresses_t SourceAddress,
                                     SZL_ADDRESS_MODE_t DestAddressMode)
{
  szl_uint8 onOff;
  szl_uint8 length = sizeof(szl_uint8);
  szl_uint8 data_type;
  SzlPlugin_OnOff_Params_t * pluginParams = NULL;

  printInfo(Service, "PLUGIN On/Off: Command Handler 0x%02X\n", CmdID);

  /* Validate inputs */
  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_ON_OFF, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return szl_false;
    }

  switch (CmdID)
    {
        case COMMAND_OFF:
          onOff = 0;
          if (Szl_DataPointWriteNative(Service, DestinationEndpoint, manufacturerSpecific, ClusterID, ATTRID_ON_OFF, SZL_ZB_DATATYPE_BOOLEAN, &onOff, 1) == SZL_STATUS_SUCCESS)
            {
              pluginParams->OnOffCallback(Service, DestinationEndpoint, szl_false);
              *PayloadOutSize = 0;
              return szl_true;
            }
          break;

        case COMMAND_ON:
          onOff = 1;
          if (Szl_DataPointWriteNative(Service, DestinationEndpoint, manufacturerSpecific, ClusterID, ATTRID_ON_OFF, SZL_ZB_DATATYPE_BOOLEAN, &onOff, 1) == SZL_STATUS_SUCCESS)
            {
              pluginParams->OnOffCallback(Service, DestinationEndpoint, szl_true);
              *PayloadOutSize = 0;
              return szl_true;
            }
          break;

        case COMMAND_TOGGLE:
          if (Szl_DataPointReadNative(Service, DestinationEndpoint, manufacturerSpecific, ClusterID, ATTRID_ON_OFF, &data_type, &onOff, &length) == SZL_STATUS_SUCCESS)
            {
              onOff ? (onOff = 0) : (onOff = 1);
              if (Szl_DataPointWriteNative(Service, DestinationEndpoint, manufacturerSpecific, ClusterID, ATTRID_ON_OFF, SZL_ZB_DATATYPE_BOOLEAN, &onOff, 1) == SZL_STATUS_SUCCESS)
                {
                  pluginParams->OnOffCallback(Service, DestinationEndpoint, onOff? szl_true : szl_false);
                  *PayloadOutSize = 0;
                  return szl_true;
                }
            }
          break;
    }
    return szl_false;
}
