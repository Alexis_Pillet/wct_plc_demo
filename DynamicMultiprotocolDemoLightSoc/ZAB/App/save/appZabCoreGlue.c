/******************************************************************************
 *                          ZAB Test Console
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the application glue for the test console:
 *    - Serial Out ACtion and Data Handlers
 *    - Management In Handler (For notifications)
 *    - Ask and Give Handlers
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.03.00  16-Jan-14   MvdB   Major change to serial action and data handling
 * 00.00.04.00  21-May-14   MvdB   artf58379: Change ask/give Instance from 8 to 32 bits and rename Param1
 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 00.00.06.02  30-Sep-14   MvdB   artf55512: Support network key change action
 * 00.00.06.05  08-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 * 00.00.08.01  27-Nov-14   MvdB   Upgrade appZabCoreGlue_OpenFirmwareImage() to catch all possible errors
 * 01.00.00.02  28-Jan-15   MvdB   ARTF111653: Split in/out buffer ask into separate items for short and long
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
 * 002.000.003  06-Mar-15   MvdB   ARTF113874: Print list of available bin files from OpenForFirmwareUpdate in ConsoleTest
 *                                 ARTF116061: Support run time configuration of Green Power endpoint.
 * 002.000.004  01-Apr-15   MvdB   ARTF56504: Support missed channel change recovery, with actions/notifications/asks/gives to get/set parameters.
 * 002.000.006  07-Apr-15   MvdB   ARTF116255: Review enabling/disabling of GP via SZL_CFG_ENABLE_GREEN_POWER, ZAB_CFG_ENABLE_GREEN_POWER
 * 002.000.007  14-Apr-15   MvdB   ConsoleTest: Add command line function to simulate serial glue errors
 * 002.001.001  29-Apr-15   MvdB   ARTF132260: Support firmware upgrade of CC2538 network processors
 * 002.002.005  11-Sep-15   MvdB   ConsoleTest: Support appGateway by passing ask/give/notifications
 *                                 ARTF112436: Complete Get Network Info action
 * 002.002.012  14-Oct-15   MvdB   ARTF104107: Support new gives for End Device operation
 *****************************************************************************/

#include "appZabCoreGlue.h"
//#include "appSerialUtility.h"
//#include "appConsoleMain.h"
#include "appConfig.h"
//#include "appGateway.h"
//#include "appClone.h"
#include "mutex.h"

#include "zabCoreService.h"
//#include "platUtility.h"

#include <assert.h>
#include <string.h>
#include "app/framework/include/af.h"

/******************************************************************************
 *                      ******************************
 *                 *****       LOCAL VARIABLES        *****
 *                      ******************************
 ******************************************************************************/

/* Flags to tell the glue to fail on next actions, used for testing serial glue failures*/
static zab_bool nextOpenTimeout = zab_false;
static zab_bool nextCloseTimeout = zab_false;

/* Buffer for firmware update image.
 * Flash size is
 *  - 256kB for CC2530/1
 *  - 512kB for CC2538 */
#define M_MAX_FIRMWARE_SIZE 0x80000
unsigned8 fwUpdateBuffer[M_MAX_FIRMWARE_SIZE];
unsigned32 fwUpdateBuffer_Length;
static unsigned8 fwUpdateProgress = 0xFF;


static unsigned8 cloneUpdateProgress = 0xFF;

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Print error info
 ******************************************************************************/
static void UserZabErrorDisplay(erStatus* Status, zabService* Service )
{
  if (erStatusIsOk(Status))
      return;

  printApp(Service, "<<ZAB Error Warning!!!>> <0x%04X> <%s>\n",
            erStatusGetError(Status),
            erStatusUtility_GetErrorString(erStatusGetError(Status)));

  erStatusClear(Status, Service);
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Make the next Open action timeout.
 * Used for testing effect of glue failure.
 ******************************************************************************/
void appZabCoreGlue_NextOpenTimeout(void)
{
  nextOpenTimeout = zab_true;
}
/******************************************************************************
 * Make the next Close action timeout.
 * Used for testing effect of glue failure.
 ******************************************************************************/
void appZabCoreGlue_NextCloseTimeout(void)
{
  nextCloseTimeout = zab_true;
}

/******************************************************************************
 * Management SAP Callback.
 * This function is registered to handle notification coming IN from ZAB.
 ******************************************************************************/
void appZabManageCallBack (erStatus* Status, sapHandle Sap, sapMsg* Message)
{
  sapMsgType Type = 0;
  zabNotificationData notifyData;
  zabService* Service = (zabService *)sapCoreGetService( Sap );

  ER_CHECK_NULL(Status, Message);

  Type = sapMsgGetType(Message);

  UserZabErrorDisplay(Status, Service);

  if (Type == SAP_MSG_TYPE_NOTIFY)
    {

      notifyData = sapMsgGetNotifyData(Message);
      switch(sapMsgGetNotifyType(Message))
        {
          case ZAB_NOTIFICATION_OPEN_STATE:
            printApp(Service, "AppNotify: Open State = %s\n",
                      zabUtility_GetOpenStateString(notifyData.openState));

            if (notifyData.openState == ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE)
              {
                //platUtility_PrintAvailableBinFiles(Service, ".bin");
                //platUtility_PrintAvailableBinFiles(Service, ".ebl");
              }

            appGateway_OpenStateNtfHandler(notifyData.openState);
            break;

          case ZAB_NOTIFICATION_NETWORK_STATE:
            printApp(Service, "AppNotify: Network State = %s\n",
                      zabUtility_GetNetworkStateString(notifyData.nwkState));

            appGateway_NwkStateNtfHandler(notifyData.nwkState);
            break;

          case ZAB_NOTIFICATION_FIRMWARE_UPDATE_STATE:
            printApp(Service, "AppNotify: Firmware Update State = %s\n",
                      zabUtility_GetFirmwareUpdateStateString(notifyData.fwUpdateState));

            appGateway_FwUpdateStateNtfHandler(notifyData.fwUpdateState);
            break;

          case ZAB_NOTIFICATION_FIRMWARE_UPDATE_PROGRESS:
            if ( (fwUpdateProgress != notifyData.progress) &&
                 ((notifyData.progress % 5) == 0) )
              {
                fwUpdateProgress = notifyData.progress;
                printApp(Service, "AppNotify: Firmware Update Progress = %d/255\n",
                          notifyData.progress);
              }
            break;

          case ZAB_NOTIFICATION_NETWORK_PERMIT_JOIN:
            printApp(Service, "AppNotify: Permit Join Time = %d seconds\n",
                      notifyData.nwkPermitJoinTime);
            break;


          case ZAB_NOTIFICATION_CLONE_STATE:
            printApp(Service, "AppNotify: Clone State = %s\n",
                      zabUtility_GetCloneStateString(notifyData.cloneState));
#ifdef APP_CONFIG_ENABLE_CLONING
            appClone_CloneStateNotificationHandler(notifyData.cloneState);
#endif
            break;

          case ZAB_NOTIFICATION_CLONE_PROGRESS:
            if ( ( (notifyData.progress == 0) && (cloneUpdateProgress != 0) ) ||
                 ( (notifyData.progress - cloneUpdateProgress) >= 5) )
              {
                cloneUpdateProgress = notifyData.progress;
                printApp(Service, "AppNotify: Clone Progress = %d/255\n",
                          notifyData.progress);
              }
            break;

          case ZAB_NOTIFICATION_ERROR:
            printError(Service, "AppNotify: <<<ERROR 0x%04X>>> %s\n",
                       (unsigned16)notifyData.errorNumber,
                       erStatusUtility_GetErrorString(notifyData.errorNumber));
            break;

          case ZAB_NOTIFICATION_CHANNEL_CHANGE_STATE:
            printApp(Service, "AppNotify: Channel Change State = %s\n",
                      zabUtility_GetChannelChangeStateString(notifyData.channelChangeState));
            break;

          case ZAB_NOTIFICATION_NETWORK_KEY_CHANGE_STATE:
            printApp(Service, "AppNotify: Network Key Change State = %s\n",
                      zabUtility_GetNetworkKeyChangeStateString(notifyData.nwkKeyChangeState));
            break;

          case ZAB_NOTIFICATION_NETWORK_MAINTENANCE_PARAM_STATE:
            printApp(Service, "AppNotify: Network Maintenance State = %s\n",
                      zabUtility_GetNetworkMaintenanceParamStateString(notifyData.nwkMaintenanceParamState));
            break;

          case ZAB_NOTIFICATION_NETWORK_INFO_STATE:
            printApp(Service, "AppNotify: Network Info State = %s\n",
                      zabUtility_GetNetworkInfoStateString(notifyData.nwkInfoState));

            appGateway_NetworkInfoStateNtfHandler(notifyData.nwkInfoState);
            break;

          default:
            printApp(Service, "AppNotify: WARNING: Unknown notification type 0x%02X\n",
                      sapMsgGetNotifyType(Message));
        }
    }
  else
    {
      printApp(Service, "APP Manage SAP Handler: WARNING: Other Sap Type receive :%s\n", zabUtility_GetSapMsgTypeString(Type) );
    }
}


/******************************************************************************
 * Serial Out Action Handler
 * This function is registered to handle actions coming out of ZAB to the serial glue.
 ******************************************************************************/
void appZabCoreGlue_SerialOutActionHandler(erStatus* Status, zabService* Service, zabAction Action)
{
  switch (Action)
    {
      case ZAB_ACTION_OPEN:
        if (nextOpenTimeout == zab_true)
          {
            printApp(Service, "Serial Glue Action = ZAB_ACTION_OPEN - DROPPED FOR TEST\n");
            /* Do nothing, so we can see what ZAB does with the glue failure */
            nextOpenTimeout = zab_false;
          }
        else
          {
            printApp(Service, "Serial Glue Action = ZAB_ACTION_OPEN\n");
            
            ;
            if (emberSerialInit(COM_USART1,115200,1,1) == EMBER_SUCCESS)
              {
                printApp(Service, "Serial Glue Notify = ZAB_OPEN_STATE_OPENED\n");
                zabSerialService_NotifyOpenState(Status, Service, ZAB_OPEN_STATE_OPENED);
              }
            else
              {
                printApp(Service, "Serial Glue Notify = ZAB_OPEN_STATE_CLOSED\n");
                zabSerialService_NotifyOpenState(Status, Service, ZAB_OPEN_STATE_CLOSED);
              }
          }
        break;

      case ZAB_ACTION_CLOSE:
        if (nextCloseTimeout == zab_true)
          {
            printApp(Service, "Serial Glue Action = ZAB_ACTION_CLOSE - DROPPED FOR TEST\n");
            /* Indicate nothing, so we can see what ZAB does with the glue failure.
             * Actually close the serial connection so it's not left open and broken */
            nextCloseTimeout = zab_false;
            //UserCloseSerialPort(Service);
          }
        else
          {
            printApp(Service, "Serial Glue Action = ZAB_ACTION_CLOSE\n");
            //UserCloseSerialPort(Service);
            printApp(Service, "Serial Glue Notify = ZAB_OPEN_STATE_CLOSED\n");
            zabSerialService_NotifyOpenState(Status, Service, ZAB_OPEN_STATE_CLOSED);
          }
        break;

      default:
        printApp(Service, "appZabCoreGlue_SerialOutActionHandler: Unknown Action = 0x%02X\n", (unsigned8)Action);
        break;
    }
  appConsoleMain_WorkRequired();
}

/******************************************************************************
 * Serial Out Data Handler
 * This function is registered to handle serial data coming out of ZAB to the serial glue.
 ******************************************************************************/
void appZabCoreGlue_SerialOutDataHandler(erStatus* Status, zabService* Service, unsigned16 DataLength, unsigned8* Data)
{
  //UserSendMsgSerialPort(Status, Service, Data, DataLength);
  emberSerialWriteData(COM_USART1, Data, DataLength);
}


/******************************************************************************
 * Ask Handler
 * Returns configuration values ASKED from the APP by ZAB
 ******************************************************************************/
void appAskCfg (erStatus* Status, zabService* Service, zabAskId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer)
{
  unsigned8  Out8 = 0;
  unsigned16 Out16 = 0;
  unsigned32 Out32 = 0;
  unsigned64 Out64 = 0;
  unsigned32 OutSize = 0;
  unsigned8* OutPointer = NULL;

  ER_CHECK_STATUS_NULL(Status, Service);
  ER_CHECK_NULL(Status, Buffer);

  if (srvCoreGetServiceId(Status, Service) != APP_CONFIG_M_SERVICE_ID)
    {
      return;
    }

  // See if App Gateway wishes to hadnle the item. If it does the nwe jsut return.
  if (appGateway_AskCfgHandler(Status, Service, What, Param1, Size, Buffer) == zab_true)
  {
    return;
  }

  switch (What)
    {

      case ZAB_ASK_BUFFER_COUNT_IN:
        OutSize = 2;
        Out16 = 0x0020;
        printApp(Service, "AppAsk: ZAB_ASK_BUFFER_COUNT_IN = 0x%04X\n", Out16);
        osMemCopy(Status, Buffer, (unsigned8*)&Out16, OutSize);
        break;

      case ZAB_ASK_BUFFER_COUNT_IN_LONG:
        OutSize = 2;
        Out16 = 0x0020;
        printApp(Service, "AppAsk: ZAB_ASK_BUFFER_COUNT_IN_LONG = 0x%04X\n", Out16);
        osMemCopy(Status, Buffer, (unsigned8*)&Out16, OutSize);
        break;

      case ZAB_ASK_BUFFER_COUNT_OUT:
        OutSize = 2;
        Out16 = 0x0020;
        printApp(Service, "AppAsk: ZAB_ASK_BUFFER_COUNT_OUT = 0x%04X\n", Out16);
        osMemCopy(Status, Buffer, (unsigned8*)&Out16, OutSize);
        break;

      case ZAB_ASK_BUFFER_COUNT_OUT_LONG:
        OutSize = 2;
        Out16 = 0x0020;
        printApp(Service, "AppAsk: ZAB_ASK_BUFFER_COUNT_OUT_LONG = 0x%04X\n", Out16);
        osMemCopy(Status, Buffer, (unsigned8*)&Out16, OutSize);
        break;

      case ZAB_ASK_BUFFER_COUNT_EVENT:
        OutSize = 2;
        Out16 = 0x0004;
        printApp(Service, "AppAsk: ZAB_ASK_BUFFER_COUNT_EVENT = 0x%04X\n", Out16);
        osMemCopy(Status, Buffer, (unsigned8*)&Out16, OutSize);
        break;

      case ZAB_ASK_SAP_MANAGE_MAX:
        OutSize = 1;
        Out8 = 1; // In notification
        printApp(Service, "AppAsk: ZAB_ASK_SAP_MANAGE_MAX = 0x%02X\n", Out8);
        osMemCopy(Status, Buffer, &Out8, OutSize);
        break;

      case ZAB_ASK_SAP_DATA_MAX:
        OutSize = 1;
        Out8 = 0; // No application requirements for data sap access
        printApp(Service, "AppAsk: ZAB_ASK_SAP_DATA_MAX = 0x%02X\n", Out8);
        osMemCopy(Status, Buffer, &Out8, OutSize);
        break;

      case ZAB_ASK_SAP_SERIAL_MAX:
        OutSize = 1;
        Out8 = 0; // No additional access required
        printApp(Service, "AppAsk: ZAB_ASK_SAP_SERIAL_MAX = 0x%02X\n", Out8);
        osMemCopy(Status, Buffer, &Out8, OutSize);
        break;

      case ZAB_ASK_NETWORK_PROCESSOR_START:
        OutSize = 1;
        printApp(Service, "AppAsk: ZAB_ASK_NETWORK_PROCESSOR_START = %s\n", appConfig_Config.startNetworkProcessorApplication ? "TRUE" : "FALSE");
        osMemCopy( Status, Buffer, &appConfig_Config.startNetworkProcessorApplication, OutSize );
        break;

      case ZAB_ASK_NETWORK_PROCESSOR_STOP_ON_CLOSE:
        /* Leave as default */
        printApp(Service, "AppAsk: ZAB_ASK_NETWORK_PROCESSOR_STOP_ON_CLOSE = Default (%s)\n",
                 *Buffer ? "TRUE" : "FALSE");
        break;

      case ZAB_ASK_NWK_RESUME:
        OutSize = 1;
        printApp(Service, "AppAsk: ZAB_ASK_NWK_RESUME = %s\n", appConfig_Config.nwkResume ? "TRUE" : "FALSE");
        osMemCopy( Status, Buffer, &appConfig_Config.nwkResume, OutSize );
        break;

      case ZAB_ASK_NWK_CHANNEL_MASK:
        OutSize = 4;
        // Use a channel if one is specified, otherwise all
        if ( (appConfig_Config.channelNumber > 10) && (appConfig_Config.channelNumber < 27) )
          {
            printApp(Service, "AppAsk: ZAB_ASK_NWK_CHANNEL_MASK = Channel 0x%02X (%d)\n", appConfig_Config.channelNumber, appConfig_Config.channelNumber);
            Out32 = 1 << appConfig_Config.channelNumber;
          }
        else
          {
            printApp(Service, "AppAsk: ZAB_ASK_NWK_CHANNEL_MASK = All Channels\n");
            Out32 = 0x07FFF800;
          }
        osMemCopy( Status, Buffer, (unsigned8*)&Out32, OutSize );
        break;

      case ZAB_ASK_NWK_STEER:
        OutSize = 1;
        Out8 = appConfig_Config.nwkSteerOptions;
        printApp(Service, "AppAsk: ZAB_ASK_NWK_STEER = %s\n", zabUtility_GetNwkSteerString(appConfig_Config.nwkSteerOptions));
        osMemCopy( Status, Buffer, &Out8, OutSize );
        break;

      case ZAB_ASK_NWK_SECURITY_LEVEL:
        OutSize = 1;
        Out8 = 1;
        printApp(Service, "AppAsk: ZAB_ASK_NWK_SECURITY_LEVEL = 0x%02X\n", Out8);
        osMemCopy( Status, Buffer, &Out8, OutSize );
        break;

      case ZAB_ASK_NWK_PAN_ID:
        OutSize = 2;
        if (appConfig_Config.panId != APP_CONFIG_M_PAN_ID_UNDEFINED)
          {
            printApp(Service, "AppAsk: ZAB_ASK_NWK_PAN_ID = 0x%04X\n", appConfig_Config.panId);
            osMemCopy(Status, Buffer, (unsigned8*)&appConfig_Config.panId, OutSize);
          }
        else
          {
            osMemCopy(Status, (unsigned8*)&Out16, Buffer, sizeof(unsigned16));
            printApp(Service, "AppAsk: ZAB_ASK_NWK_PAN_ID = Not set. Using ZAB default: 0x%04X\n", Out16);
          }
        break;

      case ZAB_ASK_NWK_EPID:
        if (appConfig_Config.extPanId != APP_CONFIG_M_EPID_UNDEFINED)
          {
            OutSize = 8;
            printApp(Service, "AppAsk: ZAB_ASK_NWK_EPID = 0x%016llX\n", appConfig_Config.extPanId);
            osMemCopy(Status, Buffer, (unsigned8*)&appConfig_Config.extPanId, OutSize);
          }
        else
          {
            osMemCopy(Status, (unsigned8*)&Out64, Buffer, sizeof(unsigned64));
            printApp(Service, "AppAsk: ZAB_ASK_NWK_EPID = Not set. Using ZAB default: 0x%016llX\n", Out64);
          }
        break;

      case ZAB_ASK_NWK_JOIN_BEACON:
        OutSize = sizeof(BeaconFormat_t);
        printApp(Service, "AppAsk: ZAB_ASK_NWK_JOIN_BEACON = Index %d\n", appConfig_Config.beaconSelected);
        osMemCopy( Status, Buffer, (unsigned8*)&appConfig_Config.beaconStorage[appConfig_Config.beaconSelected], OutSize);
        break;

      case ZAB_ASK_FIRMWARE_IMAGE_LENGTH:
        OutSize = 4;
        printApp(Service, "AppAsk: ZAB_ASK_FIRMWARE_IMAGE_LENGTH = %d\n", fwUpdateBuffer_Length);
        osMemCopy( Status, Buffer, (unsigned8*)&fwUpdateBuffer_Length, OutSize );
        break;

      case ZAB_ASK_FIRMWARE_IMAGE_DATA:
        OutSize = *Size;
        OutPointer = &fwUpdateBuffer[Param1];
        //printApp(Service, "AppAsk: ZAB_ASK_FIRMWARE_IMAGE_DATA: Offset = %d\n", Param1);
        osMemCopy( Status, Buffer, OutPointer, OutSize );
        break;

      case ZAB_ASK_TX_POWER_DBM:
        OutSize = 1;
        Out8 = (unsigned8)appConfig_Config.txPower;
        printApp(Service, "AppAsk: ZAB_ASK_TX_POWER_DBM = %d\n", appConfig_Config.txPower);
        osMemCopy( Status, Buffer, &Out8, OutSize );
        break;

      case ZAB_ASK_PERMIT_JOIN_TIME:
        OutSize = 2;
        printApp(Service, "AppAsk: ZAB_ASK_PERMIT_JOIN_TIME = %d\n", appConfig_Config.permitJoinTime);
        osMemCopy(Status, Buffer, (unsigned8*)&appConfig_Config.permitJoinTime, OutSize);
        break;

      case ZAB_ASK_CHANNEL_TO_CHANGE_TO:
        OutSize = 1;
        Out8 = appConfig_Config.channelToChangeTo;
        printApp(Service, "AppAsk: ZAB_ASK_CHANNEL_TO_CHANGE_TO = 0x%02X (%d)\n", Out8, Out8);
        osMemCopy( Status, Buffer, &Out8, OutSize );
        break;

      case ZAB_ASK_ANTENNA_MODE:
        OutSize = 1;
        Out8 = (unsigned8)appConfig_Config.newAntennaOperatingMode;
        printApp(Service, "AppAsk: ZAB_ASK_ANTENNA_MODE = 0x%02X (%d)\n", Out8, Out8);
        osMemCopy( Status, Buffer, &Out8, OutSize );
        break;

#ifdef APP_CONFIG_ENABLE_GREEN_POWER
      case ZAB_ASK_GREEN_POWER_ENDPOINT:
        OutSize = 1;
        Out8 = appConfig_Config.greenPowerEndpoint;
        printApp(Service, "AppAsk: ZAB_ASK_GREEN_POWER_ENDPOINT = 0x%02X (%d)\n", Out8, Out8);
        osMemCopy( Status, Buffer, &Out8, OutSize );
        break;
#endif

      case ZAB_ASK_NETWORK_MAINTENANCE_SLOW_PING_TIME_MS:
        OutSize = 4;
        Out32 = appConfig_Config.nwkMaintSlowPingTimeMs;
        printApp(Service, "AppAsk: ZAB_ASK_NETWORK_MAINTENANCE_SLOW_PING_TIME_MS = %d\n", Out32);
        osMemCopy( Status, Buffer, (unsigned8*)&Out32, OutSize );
        break;

      case ZAB_ASK_NETWORK_MAINTENANCE_FAST_PING_TIME_MS:
        OutSize = 4;
        Out32 = appConfig_Config.nwkMaintFastPingTimeMs;
        printApp(Service, "AppAsk: ZAB_ASK_NETWORK_MAINTENANCE_FAST_PING_TIME_MS = %d\n", Out32);
        osMemCopy( Status, Buffer, (unsigned8*)&Out32, OutSize );
        break;

      default:
        /* For unsupported items, just leave them as defaults */
        printApp(Service, "WARNING: Unhandled cfg item asked, What = 0x%04X\n", (unsigned16)What);
        break;
    }

  *Size = OutSize;
}


/******************************************************************************
 * Give Handler
 * Accepts configuration values GIVEN by ZAB to the app
 ******************************************************************************/
void appGiveCfg(erStatus* Status, zabService* Service, zabGiveId What, unsigned32 Param1, unsigned32* Size, unsigned8* Buffer)
{
  unsigned32 OutSize = 0;
  unsigned32 len;

  ER_CHECK_STATUS_NULL(Status, Service);
  ER_CHECK_NULL(Status, Buffer);
  len = *Size;

  if (srvCoreGetServiceId(Status, Service) != APP_CONFIG_M_SERVICE_ID)
    {
      return;
    }

  printApp(Service, "AppGive: ");
  switch (What)
    {
      case ZAB_GIVE_CORE_VERSION:
        if (len == 4)
          {
            osMemCopy(Status, (unsigned8*)&appConfig_Info.coreVersion, Buffer, len);
            printApp(Service, "Core Version: %03d.%03d.%03d.%03d",
                   appConfig_Info.coreVersion[0],
                   appConfig_Info.coreVersion[1],
                   appConfig_Info.coreVersion[2] ,
                   appConfig_Info.coreVersion[3]);
          }
        else
          {
              erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;

      case ZAB_GIVE_VENDOR_VERSION:
        if (len == 4)
          {
            osMemCopy(Status, (unsigned8*)&appConfig_Info.vendorVersion, Buffer, len);
            printApp(Service, "Vendor Version: %03d.%03d.%03d.%03d",
                   appConfig_Info.vendorVersion[0],
                   appConfig_Info.vendorVersion[1],
                   appConfig_Info.vendorVersion[2],
                   appConfig_Info.vendorVersion[3]);
          }
        else
          {
            erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;

      case ZAB_GIVE_VENDOR_TYPE:
        if (len == 2)
          {
            appConfig_Info.vendorType = *(unsigned16*)Buffer;
            printApp(Service, "Vendor Type: %s", zabUtility_GetVendorTypeString(appConfig_Info.vendorType));
          }
        else
          {
            erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;

      case ZAB_GIVE_NETWORK_PROCESSOR_MODEL:
        if (len == 1)
          {
            appConfig_Info.hardwareType = (zabNetworkProcessorModel)(*Buffer);
            printApp(Service, "Nwk Proc Model = %s", zabUtility_GetNetworkProcessorModelString(appConfig_Info.hardwareType));
          }
        else
          {
            erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;


      case ZAB_GIVE_NETWORK_PROCESSOR_ACTIVE_APP_TYPE: // 1 byte,
        if (len == 1)
          {
            printApp(Service, "Nwk Proc Active Application Type = %s", zabUtility_GetNetworkProcessorApplicationString((zabNetworkProcessorApplication)(*Buffer)));
          }
        else
          {
            erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;


      case ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_TYPE: // 1 byte,
        if (len == 1)
          {
            printApp(Service, "Nwk Proc ZigBee Application Type = %s", zabUtility_GetNetworkProcessorApplicationString((zabNetworkProcessorApplication)(*Buffer)));
          }
        else
          {
            erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;

      case ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_VERSION:
        if (len == 4)
          {
            printApp(Service, "Nwk Proc ZigBee Application Version = %03d.%03d.%03d.%03d",
                   Buffer[0],
                   Buffer[1],
                   Buffer[2],
                   Buffer[3]);
          }
        else
          {
              erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;

      case ZAB_GIVE_NETWORK_PROCESSOR_BOOTLOADER_VERSION:
        if (len == 4)
          {
            printApp(Service, "Nwk Proc Bootloader Version = %03d.%03d.%03d.%03d",
                   Buffer[0],
                   Buffer[1],
                   Buffer[2],
                   Buffer[3]);
          }
        else
          {
              erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
          }
        break;


    /* Beacons received during network discovery
     * Store in table so one can be selected and returned for Join*/
    case ZAB_GIVE_NWK_BEACON:
      if (appConfig_Config.beaconCount < APP_CONFIG_M_MAX_BEACONS)
        {
          OutSize = sizeof(BeaconFormat_t);
          osMemCopy( Status, (unsigned8*)&appConfig_Config.beaconStorage[appConfig_Config.beaconCount], Buffer, OutSize);

          printApp(Service, "Beacon 0x%02X: Src 0x%04X, PAN 0x%04X, Ch 0x%02X, PJ 0x%02X, EPID 0x%016llX, PD 0x%02X, SP 0x%02X",
                                  appConfig_Config.beaconCount,
                                  appConfig_Config.beaconStorage[appConfig_Config.beaconCount].src,
                                  appConfig_Config.beaconStorage[appConfig_Config.beaconCount].pan,
                                  appConfig_Config.beaconStorage[appConfig_Config.beaconCount].channel,
                                  appConfig_Config.beaconStorage[appConfig_Config.beaconCount].permitJoin,
                                  appConfig_Config.beaconStorage[appConfig_Config.beaconCount].epid,
                                  appConfig_Config.beaconStorage[appConfig_Config.beaconCount].depth,
                                  appConfig_Config.beaconStorage[appConfig_Config.beaconCount].stackProfile);

          appConfig_Config.beaconCount++;
        }
      else
        {
          printApp(Service, "WARNING: Beacon table full");
        }
      break;


    case ZAB_GIVE_IEEE:
      if (len == 8)
        {
          appConfig_Info.ieee = *(unsigned64*)Buffer;
          printApp(Service, "IEEE = 0x%016llX", *(unsigned64*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_NWK_ADDRESS:
      if (len == 2)
        {
          appConfig_Info.nwkAddress = *(unsigned16*)Buffer;
          printApp(Service, "Network Address = 0x%04X", *(unsigned16*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_NWK_CHANNEL_NUMBER:
      if (len == 1)
        {
          appConfig_Info.channel = *Buffer;
          printApp(Service, "Channel = 0x%02X", *Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_NWK_PAN_ID:
      if (len == 2)
        {
          appConfig_Info.panID = *(unsigned16*)Buffer;
          printApp(Service, "PAN ID = 0x%04X", *(unsigned16*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_NWK_EPID:
      if (len == 8)
        {
          appConfig_Info.epid = *(unsigned64*)Buffer;
          printApp(Service, "EPID = 0x%016llX,", *(unsigned64*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_DEVICE_TYPE:
      if (len == sizeof(unsigned8))
        {
          printApp(Service, "ZAB_GIVE_DEVICE_TYPE = %s", zabUtility_GetDeviceTypeString((zabNwkState)*Buffer));
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_PARENT_IEEE:
      if (len == sizeof(unsigned64))
        {
          printApp(Service, "ZAB_GIVE_PARENT_IEEE = 0x%016llX,", *(unsigned64*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_PARENT_NWK_ADDRESS:
      if (len == sizeof(unsigned16))
        {
          printApp(Service, "ZAB_GIVE_PARENT_NWK_ADDRESS = 0x%04X", *(unsigned16*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_NWK_TX_POWER:
      if (len == sizeof(signed8))
        {
          appConfig_Info.actualTxPower = *(signed8*)Buffer;
          printApp(Service, "Actual Tx Power = %ddBm", *(signed8*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_ANTENNA_SUPPORTED_MODES:
      if (len == sizeof(signed8))
        {
          appConfig_Info.antennaSupportedModes = (zabAntennaSupportedModes)*Buffer;
          printApp(Service, "Supported Antenna Modes = 0x%02X: %s %s %s",
                   appConfig_Info.antennaSupportedModes,
                   (appConfig_Info.antennaSupportedModes & ZAB_ANTENNA_SUPPORTED_MODE_ANTENNA1) ? "Antenna1," : "",
                   (appConfig_Info.antennaSupportedModes & ZAB_ANTENNA_SUPPORTED_MODE_ANTENNA2) ? "Antenna2," : "",
                   (appConfig_Info.antennaSupportedModes & ZAB_ANTENNA_SUPPORTED_MODE_DIVERSITY_AUTO) ? "DiveristyAuto" : ""
                  );
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_ANTENNA_CURRENT_MODE:
      if (len == sizeof(signed8))
        {
          appConfig_Info.antennaOperatingMode = (zabAntennaOperatingMode)*Buffer;
          printApp(Service, "Current Antenna Mode = 0x%02X: %s",
                   appConfig_Info.antennaOperatingMode,
                   zabUtility_GetAntennaOperatingModeString(appConfig_Info.antennaOperatingMode));
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_NETWORK_MAINTENANCE_SLOW_PING_TIME_MS:
      if (len == sizeof(unsigned32))
        {
          printApp(Service, "ZAB_GIVE_NETWORK_MAINTENANCE_SLOW_PING_TIME_MS = %d", *(unsigned32*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    case ZAB_GIVE_NETWORK_MAINTENANCE_FAST_PING_TIME_MS:
      if (len == sizeof(unsigned32))
        {
          printApp(Service, "ZAB_GIVE_NETWORK_MAINTENANCE_FAST_PING_TIME_MS = %d", *(unsigned32*)Buffer);
        }
      else
        {
          erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        }
      break;

    default:
      printApp(Service, "WARNING: Unhandled cfg item given, What = 0x%04X", (unsigned16)What);
  }

  printApp(Service, "\n");

  // Pass to appGateway which may wish to catch the item too
  appGateway_GiveCfgHandler(Status, Service, What, Param1, Size, Buffer);
}


/******************************************************************************
 * Open a firmware image so it is ready when pointers and length are asked during the update.
 ******************************************************************************/
zab_bool appZabCoreGlue_OpenFirmwareImage(zabService* Service, char* filename)
{
#if 0
  FILE * fp;
  size_t result;
  long int length;

  printApp(Service, "appZabCoreGlue_OpenFirmwareImage: Opening %s\n", filename);

  // Open update file
  fp=fopen(filename, "rb");
  if (fp==NULL)
    {
      printError(Service, "appZabCoreGlue_OpenFirmwareImage: ERROR - Could not open update file.\n");
      return zab_false;
    }

  /* Position to end of file */
  if (fseek(fp, 0L, SEEK_END) != 0)
    {
      fclose(fp);
      printError(Service, "appZabCoreGlue_OpenFirmwareImage: ERROR - fseek failed\n");
      return zab_false;
    }

  length = ftell(fp);     /* Get file length */
  rewind(fp);             /* Back to start of file */

  if( (length < 0) ||
      (length > M_MAX_FIRMWARE_SIZE) )
    {
      fclose(fp);
      printError(Service, "appZabCoreGlue_OpenFirmwareImage: ERROR - Update file failed or is too big: Length=%d Max=%d\n", length, M_MAX_FIRMWARE_SIZE);
      return zab_false;
    }
  fwUpdateBuffer_Length = (unsigned32)length;

  result = fread(fwUpdateBuffer, 1, fwUpdateBuffer_Length,  fp);
  fclose(fp);

  if (result != fwUpdateBuffer_Length)
    {
      printError(Service, "appZabCoreGlue_OpenFirmwareImage: ERROR - fread failed with %d not %d\n", result, fwUpdateBuffer_Length);
      return zab_false;
    }
#endif
  return zab_true;
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/