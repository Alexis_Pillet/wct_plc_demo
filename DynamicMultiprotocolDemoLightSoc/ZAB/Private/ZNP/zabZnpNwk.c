/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the networking state machine for the ZAB Pro ZNP
 *   vendor.
 *   For details and flow charts see XXX.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 02.00.00.01  31-May-13   MvdB   Original
 * 02.00.00.01                     Add enabling of direct CB during steering so ZDO responses are forwarded up
 *              18-Jul-13   MvdB   Improve Do Not Resume to end in None rather than Uninitialsed
 *              29-Jul-13   MvdB   Support ask and set of EPID
 *              06-Aug-13   MvdB   On form, wait for zdo state change indication to show started as coord before getting config
 * 00.00.02.00  22-Oct-13   MvdB   Support notification of errors
 * 00.00.02.01  03-Dec-13   MvdB   Support transmit power setting
 * 00.00.03.01  18-Mar-14   MvdB   artf53865: Support extended permit join time
 * 00.00.06.00  09-Sep-14   MvdB   artf103725: Fix EPID presentation - REMOVED!!
 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 00.00.06.02  30-Sep-14   MvdB   artf55512: Support network key change action
 * 00.00.06.03  01-Oct-14   MvdB   artf104879: Support energy scans via Mgmt Nwk Update Request
 * 00.00.06.04  06-Oct-14   MvdB   artf56243: Handle local network processor leaves gracefully
 * 00.00.06.06  29-Oct-14   MvdB   artf105443: Ensure new network key in generated when forming new network
 *                                 artf106648: Permit join should only be broadcast to routers and coordinator, not all devices
 * 00.00.07.00  31-Oct-14   MvdB   Update one second tick to handle errors updating PJ time from task
 * 01.00.00.00  05-Dec-14   MvdB   Check serial link is available before trying to set PJ from timer
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 * 01.100.07.00 17-Feb-15   MvdB   ARTF109084: Validate PAN and EPID during form
 * 001.101.001  23-Feb-15   MvdB   ARTF113925: Double leave indication was causing double initialisation to be required during NwkInit with resume=false
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
 * 002.000.003  06-Mar-15   MvdB   ARTF116061: Support run time configuration of Green Power endpoint.
 * 002.000.004  01-Apr-15   MvdB   ARTF56504: Support missed channel change recovery, with actions/notifications/asks/gives to get/set parameters.
 * 002.000.006  08-Apr-15   MvdB   ARTF116255: Review enabling/disabling of GP via SZL_CFG_ENABLE_GREEN_POWER, ZAB_CFG_ENABLE_GREEN_POWER
 *                                 ARTF113873: Set M_DEFAULT_CHANNEL_TO_CHANGE_TO to an invalid value so action fails if ask not correctly supported.
 *                                 ARTF113873: Tidy up action error handling - issue not closed.
 * 002.001.001  29-Apr-15   MvdB   ARTF132254: Clean up type casts in vendor for IAR compiler
 * 002.002.005  11-Sep-15   MvdB   ARTF112436: Complete Get Network Info action
 * 002.002.012  14-Oct-15   MvdB   ARTF104107: Support End Device operation
 * 002.002.013  14-Oct-15   MvdB   ARTF151072: Add service pointer to osTimeGetMilliseconds() and osTimeGetSeconds()
 * 002.002.020  18-Mar-16   MvdB   ARTF164900: Handle channel change to self gracefully
 *                                 ARTF165341: Delay Networked ntf during join until key received (DEV_ROUTER)
 * 002.002.028  13-Oct-16   MvdB   ARTF186964: ZNP Ping should check network action not in process before starting ping
 * 002.002.038  12-Jan-17   MvdB   ARTF113872: Deny key change unless node is Trust Center (it fails if done from other devices)
 *****************************************************************************/

#include "zabZnpService.h"


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

#define M_DEFAULT_CHANNEL_MASK 0x07FFF800
#define M_DEFAULT_SCAN_DURATION 0x02
#define M_DEFAULT_RESUME 0x01
#define M_DEFAULT_NWK_STEER ZAB_NWK_STEER_NONE
#define M_DEFAULT_SECURITY_LEVEL 1
#define M_DEFAULT_TX_POWER 127

#define M_TRUST_CENTER_NETWORK_ADDRESS    ( 0x0000 )

/* Default set to an invalid value so it fails if app does not support the item*/
#define M_DEFAULT_CHANNEL_TO_CHANGE_TO 0


#define M_BEACON_LENGTH 21

/* Default times for Network Maintenance Parameters */
#define NETWORK_MAINTENANCE_SLOW_PING_TIME_MS_DEFAULT (15UL * 60UL * 1000UL)
#define NETWORK_MAINTENANCE_FAST_PING_TIME_MS_DEFAULT (30UL * 1000UL)


#define M_TIMEOUT_DISABLED 0xFFFFFFFF


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/
#define ZAB_SERVICE_ZNP_NWK(_srv)  (ZAB_SERVICE_VENDOR(_srv)->zabNwkInfo)

/* Control parameter for type of getConfiguration() */
typedef enum
{
  GET_CONFIG_TYPE_START_ALL,
  GET_CONFIG_TYPE_START_END_DEVICE_ONLINE,
  GET_CONFIG_TYPE_START_END_DEVICE_OFFLINE,
  GET_CONFIG_TYPE_CONTINUE,
} getConfigType;


/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Set a timeout
 ******************************************************************************/
static void setTimeout(erStatus* Status, zabService* Service, unsigned32 Timeout)
{
  if (erStatusIsOk(Status))
    {
      osTimeGetMilliseconds(Service, &ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs);
      ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs += Timeout;

      /* Handle the (very unlikely) case our timeout works out to the disabled value */
      if (ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs == M_TIMEOUT_DISABLED)
        {
          ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs++;
        }
    }
}

/******************************************************************************
 * Set item is status is good
 * Otherwise graceful harakiri
 ******************************************************************************/
static void checkStatusSetItem(erStatus* Status, zabService* Service, zabNwkCurrentItem item)
{
  if (erStatusIsOk(Status))
    {
      ZAB_SERVICE_ZNP_NWK(Service).currentItem = item;
    }
  else
    {
      printError(Service, "NWK: Error: Command failed at item 0x%02X with error 0x%04X. Resetting state machine\n", item, Status->Error);
      zabZnpNwk_create(Status, Service);
    }
}

/******************************************************************************
 * Set network state and notify the manage sap
 ******************************************************************************/
static void setAndNotifyNwkState(erStatus* Status, zabService* Service, zabNwkState nwkState)
{
  if (ZAB_SERVICE_ZNP_NWK(Service).nwkState != nwkState)
    {
      ZAB_SERVICE_ZNP_NWK(Service).nwkState = nwkState;
      sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, nwkState);
    }
}

/******************************************************************************
 * Run the Get Configuration process
 * This is called once were area Networked, to recall: NwkAddr, Channel, PAN, EPID etc
 ******************************************************************************/
static void getConfiguration(erStatus* Status, zabService* Service, getConfigType getType)
{
  /* Normal start point */
  if (getType == GET_CONFIG_TYPE_START_ALL)
    {
      /* Network Address */
      znpi_sapi_getDeviceInfo(Status, Service, ZAB_ZNP_SAPI_DEV_INFO_TYPE_DEV_SHORT_ADDR);
      checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_GET_CFG_SHORT_ADDR);
    }
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_CFG_SHORT_ADDR)
    {
      /* Device Logical Type */
      znpi_sapi_read_config(Status, Service, ZAB_ZNP_SAPI_NV_CFG_ID_LOGICAL_TYPE);
      checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_GET_CFG_DEV_TYPE);
    }
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_CFG_DEV_TYPE)
    {
      /* Device State */
      znpi_sapi_getDeviceInfo(Status, Service, ZAB_ZNP_SAPI_DEV_INFO_TYPE_DEV_STATE);
      checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_GET_CFG_DEV_STATE);
    }
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_CFG_DEV_STATE)
    {
      /* Channel */
      znpi_sapi_getDeviceInfo(Status, Service, ZAB_ZNP_SAPI_DEV_INFO_TYPE_ZB_CHANNEL);
      checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_GET_CFG_CHANNEL);
    }
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_CFG_CHANNEL)
    {
      /* PAN ID */
      znpi_sapi_getDeviceInfo(Status, Service, ZAB_ZNP_SAPI_DEV_INFO_TYPE_ZB_PAN_ID);
      checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_GET_CFG_PAN);
    }
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_CFG_PAN)
    {
      /* EPID */
      znpi_sapi_getDeviceInfo(Status, Service, ZAB_ZNP_SAPI_DEV_INFO_TYPE_ZB_EXTENDED_PAN_ID);
      checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_GET_CFG_EPID);
    }
  /* Start point if we are just updating the End Device info.  */
  else if ( (getType == GET_CONFIG_TYPE_START_END_DEVICE_ONLINE) ||
            (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_CFG_EPID) )
    {
      /* Parent Network Address */
      znpi_sapi_getDeviceInfo(Status, Service, ZAB_ZNP_SAPI_DEV_INFO_TYPE_PARENT_SHORT_ADDR);
      checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_GET_CFG_PARENT_SHORT_ADDR);
    }
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_CFG_PARENT_SHORT_ADDR)
    {
     /* Parent IEEE Address */
      znpi_sapi_getDeviceInfo(Status, Service, ZAB_ZNP_SAPI_DEV_INFO_TYPE_PARENT_IEEE_ADDR);
      checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_GET_CFG_PARENT_IEEE_ADDR);
    }
  /* Start and end point if we are just updating an end device to offline.
   * End point for all or endDeviceOnline  */
  else if ( (getType == GET_CONFIG_TYPE_START_END_DEVICE_OFFLINE) ||
            (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_CFG_PARENT_IEEE_ADDR) )
    {
      /* Get Config Complete */
      ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;

      /* We can get here via the Init action or the GetNwkInfo action.
       * Use the getNetworkInfoActive flag to determine the source and hence what we should notify */
      if (ZAB_SERVICE_ZNP_NWK(Service).getNetworkInfoActive)
        {
          ZAB_SERVICE_ZNP_NWK(Service).getNetworkInfoActive = 0;
          sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_SUCCESS);
          sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_NONE);
        }
      else
        {
          /* Everything is now finished, so set networked and end state machine.
           * If we are a parentless end device ,then indicate NoComms */
          if ( (ZAB_SERVICE_ZNP_NWK(Service).znpState == DEV_NWK_ORPHAN) ||
               (ZAB_SERVICE_ZNP_NWK(Service).znpState == DEV_NWK_DISC) )
            {
              setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NETWORKED_NO_COMMS);
            }
          else
            {
              setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NETWORKED);
            }
        }
    }
}

/******************************************************************************
 * Ask for then set the transmit power of the network processor
 ******************************************************************************/
static void setTxPower(erStatus* Status, zabService* Service)
{
  signed8 TxPowerDbm;
  TxPowerDbm = (signed8)srvCoreAskBack8( Status, Service, ZAB_ASK_TX_POWER_DBM, M_DEFAULT_TX_POWER);
  zabZnpSysUtility_SetTxPower(Status, Service, TxPowerDbm);
}

/******************************************************************************
 * Ask for GP Loopback Endpoint
 * If valid, set and return true
 * Else return false.
 ******************************************************************************/
static zab_bool getGpLoopback(erStatus* Status, zabService* Service)
{
#ifdef ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER
  ZAB_SERVICE_ZNP_NWK(Service).gpLoopbackEndpoint = srvCoreAskBack8( Status, Service, ZAB_ASK_GREEN_POWER_ENDPOINT, ZAB_ENDPOINT_INVALID);

  if ( (ZAB_SERVICE_ZNP_NWK(Service).gpLoopbackEndpoint >= ZAB_ENDPOINT_MIN_VALID) &&
       (ZAB_SERVICE_ZNP_NWK(Service).gpLoopbackEndpoint <= ZAB_ENDPOINT_MAX_VALID) )
    {
      zabZnpGp_SetParameter(Status, Service, ZNP_GP_PARAM_ID_LOOPBACK_DST_EP, (void*)&(ZAB_SERVICE_ZNP_NWK(Service).gpLoopbackEndpoint));
      return zab_true;
    }
  else
#endif // ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER
    {
      ZAB_SERVICE_ZNP_NWK(Service).gpLoopbackEndpoint = ZAB_ENDPOINT_INVALID;
      return zab_false;
    }
}

/******************************************************************************
 * Get if GP is enabled (GP Loopback Endpoint valid)
 ******************************************************************************/
#ifdef ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER
static zab_bool getGpEnabled(zabService* Service)
{
  if ( (ZAB_SERVICE_ZNP_NWK(Service).gpLoopbackEndpoint >= ZAB_ENDPOINT_MIN_VALID) &&
       (ZAB_SERVICE_ZNP_NWK(Service).gpLoopbackEndpoint <= ZAB_ENDPOINT_MAX_VALID) )
    {
      return zab_true;
    }
  else
    {
      return zab_false;
    }
}
#endif // ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER


/******************************************************************************
 * Validate states for an Action that requires to be opened and Networked
 ******************************************************************************/
static zab_bool validStatesForNetworkedAction(erStatus* Status, zabService* Service)
{
  zab_bool statesValid = zab_true;
  /* Validate various states are correct */
  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem != ZAB_NWK_CURRENT_ITEM_NONE)
    {
      statesValid = zab_false;
      sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_NETWORK_ACTION_IN_PROGRESS);
    }
  else if (zabZnpOpen_GetOpenState(Service) != ZAB_OPEN_STATE_OPENED)
    {
      statesValid = zab_false;
      sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
    }
  else if (ZAB_SERVICE_ZNP_NWK(Service).nwkState != ZAB_NWK_STATE_NETWORKED)
    {
      statesValid = zab_false;
      sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_INVALID_NETWORK_STATE);
    }
  return statesValid;
}
/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Create
 * Initialises networking state / state machine
 ******************************************************************************/
void zabZnpNwk_create(erStatus* Status, zabService* Service)
{
  setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_UNINITIALISED);
  sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_NONE);
  sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
  ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
  ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
  ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining = 0;
  ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining = 0;
}

/******************************************************************************
 * Get Network State
 ******************************************************************************/
zabNwkState zabZnpNwk_GetNwkState(zabService* Service)
{
  return ZAB_SERVICE_ZNP_NWK(Service).nwkState;
}

/******************************************************************************
 * Get if the Network State Machine is currently busy doing something
 ******************************************************************************/
zab_bool zabZnpNwk_GetNwkStateMachineInProgress(zabService* Service)
{
  zab_bool inProgress = zab_true;

  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NONE)
    {
      inProgress = zab_false;
    }

  return inProgress;
}

/******************************************************************************
 * Update timeout
 * Check for timeouts within the networking state machine
 ******************************************************************************/
unsigned32 zabZnpNwk_updateTimeout(erStatus* Status, zabService* Service, unsigned32 Time)
{
  unsigned32 timeoutRemaining = ZAB_WORK_DELAY_MAX_MS;

  if (ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs != M_TIMEOUT_DISABLED)
    {
      /* Check timeout while handling wrapped time*/
      timeoutRemaining = ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs - Time;
      if ( (timeoutRemaining == 0) || (timeoutRemaining > ZAB_ZNP_MS_TIMEOUT_MAX) )
        {
          /* Ask for fast work if timer expired as it may have triggered some other event */
          timeoutRemaining = 0;

          /* Channel change is not critical, so if it fails we just give notification */
          if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE)
            {
              ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
              ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
              sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_FAILED);
              sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_NONE);
            }

          /* If its the delay during network key change then get on with it! */
          else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_KEY_SWITCH_DELAY)
            {
              /* Be careful as the serial link could be locked by another command.
               * Check if the link is idle. If not, delay and count some retries to try to get it done before failing */
              if (ZAB_SERVICE_ZNP_GET_SRSP_TIMEOUT_IDLE(Service))
                {
                  /* Switch network key, then if it worked wait for SRSP */
                  zabZnpZdoUtility_NwkKeySwitchReq(Status, Service, ZAB_BROADCAST_ALL, ZAB_SERVICE_ZNP_NWK(Service).nwkKeySeqNum);
                  if (erStatusIsOk(Status))
                    {
                      ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NWK_KEY_SWITCH_REQ;
                      ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
                    }
                  else
                    {
                      /* It failed, give up */
                      ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
                      ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
                      sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_FAILED);
                      sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
                    }
                }
              /* Link is locked */
              else
                {
                  if (ZAB_SERVICE_ZNP_NWK(Service).retries < ZAB_VND_CFG_ZNP_M_NETWORK_KEY_SWITCH_RETRIES)
                    {
                      ZAB_SERVICE_ZNP_NWK(Service).retries++;
                      ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = Time + ZAB_VND_CFG_ZNP_M_NETWORK_KEY_SWITCH_RETRY_BACKOFF_MS;
                      timeoutRemaining = ZAB_VND_CFG_ZNP_M_NETWORK_KEY_SWITCH_RETRY_BACKOFF_MS;
                    }
                  else
                    {
                      ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
                      ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
                      sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_FAILED);
                      sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
                    }
                }
            }

          /* End Device join never times out, so we need to kill it with a soft reset */
          else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_IN_PROGRESS)
            {
              ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = Time + ZAB_VND_CFG_ZNP_M_OPEN_SOFT_RST_TIMEOUT_MS;
              znpi_sys_reset(Status, Service, SYS_SOFT_RESET);
              checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_FAILED_RESET);
            }

          /* General timeouts. Revert to nothing */
          else
            {
              sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
              zabZnpNwk_create(Status, Service);
            }
        }
    }
  return timeoutRemaining;
}

/******************************************************************************
 * 1 second tick handler
 ******************************************************************************/
void zabZnpNwk_oneSecondTickHandler(erStatus* Status, zabService* Service)
{
  unsigned16 pjTime;

  if (ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining > 0)
    {
      ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining--;
    }

  if (ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining > 0)
    {
      ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining--;
    }

  if ( (ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining < 10) &&
        (ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining > ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining) &&
        (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NONE) &&
        (zabZnpService_GetSerialLinkLocked(Service) == zab_false) )
    {
      if (ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining > ZAB_M_NWK_MAX_PERMIT_JOIN_TIME)
        {
          pjTime = ZAB_M_NWK_MAX_PERMIT_JOIN_TIME;
        }
      else
        {
          pjTime = ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining;
        }

      printVendor(Service, "ZNP NWK: Setting PJ = %d\n", pjTime);
      zabZnpZdoUtility_PermitJoinReq(Status, Service,
                                     ZAB_ADDRESS_MODE_BROADCAST,
                                     ZAB_BROADCAST_ROUTERS_AND_COORDINATOR,
                                     (unsigned8)pjTime,
                                     zab_true,
                                     0);
      if (erStatusIsOk(Status))
        {
          ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_ENABLE_PERMIT_JOIN_UPDATE;
          ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining = (unsigned8)pjTime;
        }
      else
        {
          printError(Service,  "ZNP NWK: WARNING - Failed to set PJ time\n" );
        }
    }
}

/******************************************************************************
 * SRSP Timeout Handler
 * Notifies state machine a command has timed out. It will handle it if
 * it generated the command.
 ******************************************************************************/
void zabZnpNwk_srspTimeoutHandler(erStatus* Status, zabService* Service)
{
  /* Actions where failure is not critical are handled individually */
  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE)
    {
      ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
      sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_FAILED);
      sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_NONE);
    }
  else if ( (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_KEY_UPDATE_REQ) ||
            (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_KEY_SWITCH_REQ) )
    {
      ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
      sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_FAILED);
      sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
    }
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_MGMT_GET)
    {
      ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
      sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_GET_FAILED);
      sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_NONE);
    }
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_MGMT_SET)
    {
      ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
      sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_SET_FAILED);
      sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_NONE);
    }
  /* If we were getting network info a failure is not significant */
  else if (ZAB_SERVICE_ZNP_NWK(Service).getNetworkInfoActive)
    {
      ZAB_SERVICE_ZNP_NWK(Service).getNetworkInfoActive = 0;
      sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_FAILED);
      sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_NONE);
    }
  /* All other actions reset network state */
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem != ZAB_NWK_CURRENT_ITEM_NONE)
    {
      zabZnpNwk_create(Status, Service);
    }
}

/******************************************************************************
 * Reset Indication Handler
 * Re-initialises networking states if dongle is reset
 ******************************************************************************/
void zabZnpNwk_resetHandler(erStatus* Status, zabService* Service)
{
  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_RESTART)
    {
      ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
      /* If we requested the restart, confirm it successfully cleared the old settings */
      znpi_sapi_getDeviceInfo(Status, Service, ZAB_ZNP_SAPI_DEV_INFO_TYPE_DEV_SHORT_ADDR);
      checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_CONFIRM_SHORT_ADDR_INVALID);
    }

  /* We use a soft reset to end an End Device join that does not complete in a reasonable time.
   * Catch it and revert to NetworkNone*/
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_FAILED_RESET)
    {
      ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
      ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
      setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
    }
  else
    {
      // No need to clear timer as it will be done in the re-create
      zabZnpNwk_create(Status, Service);
    }
}

/******************************************************************************
 * Serial Offline Handler
 * Re-initialises networking states if dongle goes offline
 ******************************************************************************/
void zabZnpNwk_serialOfflineHandler(erStatus* Status, zabService* Service)
{
  zabZnpNwk_create(Status, Service);
}

/******************************************************************************
 * Action - Networking
 * Handles Init, Discover and Steer actions for the networking state machines
 ******************************************************************************/
void zabZnpNwk_action(erStatus* Status, zabService* Service, unsigned8 Action)
{
  unsigned32 channelMask;
  unsigned32 nwkMntSlowTime;
  unsigned32 nwkMntFastTime;
  signed8 channel;
  zabNwkSteerOptions nwkSteer;
  BeaconFormat_t beacon;
  unsigned8 lt;
  zabAntennaOperatingMode antennaOperatingMode;

  ER_CHECK_STATUS(Status);

  /* If its an action we are interested in, make sure vendor is open and no other nwk actions are in progress*/
  if ( (Action == ZAB_ACTION_NWK_INIT) ||
        (Action == ZAB_ACTION_NWK_DISCOVER) ||
        (Action == ZAB_ACTION_NWK_STEER) ||
        (Action == ZAB_ACTION_NWK_PERMIT_JOIN_ENABLE) ||
        (Action == ZAB_ACTION_NWK_PERMIT_JOIN_DISABLE) ||
        (Action == ZAB_ACTION_SET_TX_POWER) ||
        (Action == ZAB_ACTION_SELECT_ANTENNA) ||
        (Action == ZAB_ACTION_NWK_GET_INFO) )
    {
      if (ZAB_SERVICE_ZNP_NWK(Service).currentItem != ZAB_NWK_CURRENT_ITEM_NONE)
        {
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_NETWORK_ACTION_IN_PROGRESS);
          sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_SERVICE_ZNP_NWK(Service).nwkState);
          return;
        }
      if (zabZnpOpen_GetOpenState(Service) != ZAB_OPEN_STATE_OPENED)
        {
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
          sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_SERVICE_ZNP_NWK(Service).nwkState);
          return;
        }
    }

  // TODO - Review this, as it is not good as it doesn't notify that the action has been dropped!

  /* Some actions also require us to be networked */
  if ( (Action == ZAB_ACTION_NWK_PERMIT_JOIN_ENABLE) ||
       (Action == ZAB_ACTION_NWK_PERMIT_JOIN_DISABLE) )
    {
      if (ZAB_SERVICE_ZNP_NWK(Service).nwkState != ZAB_NWK_STATE_NETWORKED)
        {
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_INVALID_NETWORK_STATE);
          sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_SERVICE_ZNP_NWK(Service).nwkState);
          return;
        }
    }

  /* Perform actions */
  switch(Action)
    {
      /*
      ** Initialise:
      ** Offers to resume previous network settings if available. Otherwise ends in NetworkNone state,
      ** ready for discovery/steering.
      */
      case ZAB_ACTION_NWK_INIT:
        if (ZAB_SERVICE_ZNP_NWK(Service).nwkState == ZAB_NWK_STATE_UNINITIALISED)
          {
            /* Init Step 1: Get Network address */
            znpi_sapi_getDeviceInfo(Status, Service, ZAB_ZNP_SAPI_DEV_INFO_TYPE_DEV_SHORT_ADDR);
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_GET_SHORT_ADDR);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_INVALID_NETWORK_STATE);
            sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_SERVICE_ZNP_NWK(Service).nwkState);
          }
        break;

      /*
      ** Discover:
      ** Active scan. Indicates beacons which can then be used for Join.
      */
      case ZAB_ACTION_NWK_DISCOVER:
        if (ZAB_SERVICE_ZNP_NWK(Service).nwkState == ZAB_NWK_STATE_NONE)
          {
            /* Discover Step 1: Ask for the channel mask, then run a network discovery request */
            channelMask = srvCoreAskBack32( Status, Service, ZAB_ASK_NWK_CHANNEL_MASK, M_DEFAULT_CHANNEL_MASK);
            zabZnpZdoUtility_NetworkDiscoveryReq(Status, Service, channelMask, M_DEFAULT_SCAN_DURATION /*scanDuration*/);
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_DISCOVER);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_INVALID_NETWORK_STATE);
            sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_SERVICE_ZNP_NWK(Service).nwkState);
          }
        break;

      /*
      ** Steering:
      ** Join or Form a network.
      */
      case ZAB_ACTION_NWK_STEER:
        if (ZAB_SERVICE_ZNP_NWK(Service).nwkState == ZAB_NWK_STATE_NONE)
          {

            /* Steer Step 1: Enable ZDO callbacks before doing anything! */
          /* This has been replaced by a change in the ZNP source to enable cllabcks by default, as the write was unreliable */
/*
            data = 0x01;
            znpi_sapi_write_config(Status, Service, ZAB_ZNP_SAPI_NV_CFG_ID_DIRECT_CB, 1, &data);
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_STEER_SETUP_ZDO_CB);
*/



            /* Steer Step 2: Ask join or Form */
            nwkSteer = (zabNwkSteerOptions)srvCoreAskBack8( Status, Service, ZAB_ASK_NWK_STEER, M_DEFAULT_NWK_STEER);

            switch(nwkSteer)
              {
                /* Steer Step 2a: Join. Ask for the beacon of a node to join, then try it */
                case ZAB_NWK_STEER_JOIN:
                  {
                    setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NETWORKING);

                    // Ask for the beacon
                    osMemSet(Status, (unsigned8*)&beacon, sizeof(BeaconFormat_t), 0xFF);
                    srvCoreAskBackBuffer(Status, Service,ZAB_ASK_NWK_JOIN_BEACON, 0, sizeof(BeaconFormat_t), (unsigned8*)&beacon);

                    // Could do some parameter checking here - just let it try and maybe fail for now
                    zabZnpZdoUtility_JoinReq(Status, Service,
                                              beacon.channel,
                                              beacon.pan,
                                              beacon.epid,
                                              beacon.src,
                                              beacon.depth,
                                              beacon.stackProfile);
                    checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_JOIN_REQ);
                  }

                  break;

                /* Steer Step 2b: Form. Set logical type to coordinator  */
                case ZAB_NWK_STEER_FORM:
                  lt = (unsigned8)ZAB_ZNP_SAPI_LOGICAL_TYPE_COORDINATOR;
                  znpi_sapi_write_config( Status, Service, ZAB_ZNP_SAPI_NV_CFG_ID_LOGICAL_TYPE, 1, &lt);
                  checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_LT);
                  break;

                /* Steer Step 2c: Join as an End Device. Set logical type to ED  */
                case ZAB_NWK_STEER_JOIN_ENDDEVICE:
                  setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NETWORKING);
                  lt = (unsigned8)ZAB_ZNP_SAPI_LOGICAL_TYPE_END_DEVICE;
                  znpi_sapi_write_config( Status, Service, ZAB_ZNP_SAPI_NV_CFG_ID_LOGICAL_TYPE, 1, &lt);
                  checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_SET_LT);
                  break;

                default:
                  printError(Service, "ERROR: Nwk Steer: Invalid nwkSteer %d\n", nwkSteer);
                  sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ASKED_VALUE_INVALID);
                  sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_SERVICE_ZNP_NWK(Service).nwkState);
              }
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_INVALID_NETWORK_STATE);
            sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_SERVICE_ZNP_NWK(Service).nwkState);
          }
        break;

      /*
      ** Enable Permit Join
      */
      case ZAB_ACTION_NWK_PERMIT_JOIN_ENABLE:
        if (ZAB_SERVICE_ZNP_NWK(Service).nwkState == ZAB_NWK_STATE_NETWORKED)
          {

            //ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining = ZAB_M_PERMIT_JOIN_TIME;

            ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining = srvCoreAskBack16( Status, Service, ZAB_ASK_PERMIT_JOIN_TIME, ZAB_M_PERMIT_JOIN_TIME);
            printVendor(Service, "ZNP NWK: Permit Join Time = %d\n", ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining);
            sapMsgPutNotifyNetworkPermitJoinState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining);

            if (ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining > ZAB_M_NWK_MAX_PERMIT_JOIN_TIME)
              {
                printVendor(Service, "ZNP NWK: Setting PJ for = %d, remaining %d\n", ZAB_M_NWK_MAX_PERMIT_JOIN_TIME, ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining);
                ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining = ZAB_M_NWK_MAX_PERMIT_JOIN_TIME;
                zabZnpZdoUtility_PermitJoinReq(Status, Service,
                                               ZAB_ADDRESS_MODE_BROADCAST,
                                               ZAB_BROADCAST_ROUTERS_AND_COORDINATOR,
                                               ZAB_M_NWK_MAX_PERMIT_JOIN_TIME,
                                               zab_true,
                                               0);
              }
            else
              {
                printVendor(Service, "ZNP NWK: Setting PJ for = %d, remaining %d\n", ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining, ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining);
                ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining = ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining;
                zabZnpZdoUtility_PermitJoinReq(Status, Service,
                                               ZAB_ADDRESS_MODE_BROADCAST,
                                               ZAB_BROADCAST_ROUTERS_AND_COORDINATOR,
                                               (unsigned8)ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining,
                                               zab_true,
                                               0);
              }

            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_ENABLE_PERMIT_JOIN);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_INVALID_NETWORK_STATE);
          }
        break;

      /*
      ** Disable Permit Join
      */
      case ZAB_ACTION_NWK_PERMIT_JOIN_DISABLE:
        if (ZAB_SERVICE_ZNP_NWK(Service).nwkState == ZAB_NWK_STATE_NETWORKED)
          {
            ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining = 0;
            ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining = 0;
            zabZnpZdoUtility_PermitJoinReq(Status, Service,
                                           ZAB_ADDRESS_MODE_BROADCAST,
                                           ZAB_BROADCAST_ROUTERS_AND_COORDINATOR,
                                           0,
                                           zab_true,
                                           0);
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_DISABLE_PERMIT_JOIN);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_INVALID_NETWORK_STATE);
          }
        break;


      /*
      ** Update Tx Power
      */
      case ZAB_ACTION_SET_TX_POWER:
        /* No need to set state for this as it is just a single ZNP command
         * and we don't need the response. */
        setTxPower(Status, Service);
        break;


      /*
      ** Channel Change
      */
      case ZAB_ACTION_CHANGE_CHANNEL:
        sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_IN_PROGRESS);

        /* Validate various states are correct */
        if (validStatesForNetworkedAction(Status, Service) == zab_true)
          {
            channel = srvCoreAskBack8( Status, Service, ZAB_ASK_CHANNEL_TO_CHANGE_TO, M_DEFAULT_CHANNEL_TO_CHANGE_TO);

            /* ARTF164900: Handle channel change to self gracefully */
            if (channel == ZAB_SERVICE_ZNP_NWK(Service).channel)
              {
                srvCoreGiveBack8(Status, Service, ZAB_GIVE_NWK_CHANNEL_NUMBER, channel);
                sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_SUCCESS);
                sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_NONE);
                return;
              }
            else if ( (channel >= ZAB_ZNP_NWK_M_CHANNEL_MIN) && (channel <= ZAB_ZNP_NWK_M_CHANNEL_MAX) )
              {
                zabZnpZdoUtility_MgmtNwkUpdateReq(Status, Service,
                                                  ZAB_ADDRESS_MODE_BROADCAST,
                                                  ZAB_BROADCAST_RX_ON_WHEN_IDLE,
                                                  (1 << channel),
                                                  ZAB_ZNP_ZDO_M_SCAN_DURATION_CHANGE_CHANNEL,
                                                  0,
                                                  0,
                                                  0);
                if (erStatusIsOk(Status))
                  {
                    ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE;
                    setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_CHANNEL_CHANGE_TIMEOUT_MS);

                    /* Exit on success so we can clean up failures in one place below */
                    return;
                  }
                else
                  {
                    sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, (zabError)Status->Error);
                  }
              }
            else
              {
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ASKED_VALUE_INVALID);
              }
          }
        /* It failed for some reason, so notify it */
        sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_FAILED);
        sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_NONE);
        break;


      /*
      ** Network Key Change
      */
      case ZAB_ACTION_CHANGE_NETWORK_KEY:
        sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_IN_PROGRESS);

        /* Validate various states are correct */
        if (validStatesForNetworkedAction(Status, Service) == zab_true)
          {
            /* ARTF113872: Only allow key change from TC */
            if (ZAB_SERVICE_ZNP_NWK(Service).nwkAddr == M_TRUST_CENTER_NETWORK_ADDRESS)
              {
                znpi_sapi_getDeviceInfo(Status, Service, ZAB_ZNP_SAPI_DEV_INFO_TYPE_KEY_SEQUENCE_NUMBER);
                if (erStatusIsOk(Status))
                  {
                    /* Set the state and await the SRSP or SRSP timeout */
                    ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NWK_KEY_SEQ_NUM_REQ;

                    /* Exit on success so we can clean up failures in one place below */
                    return;
                  }
                else
                  {
                    sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, (zabError)Status->Error);
                  }
              }
            else
              {
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_CFG_INVALID);
              }
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_INVALID_NETWORK_STATE);
          }
        /* It failed for some reason, so notify it */
        sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_FAILED);
        sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
        break;


      /*
      ** Antenna Selection
      */
      case ZAB_ACTION_SELECT_ANTENNA:
        antennaOperatingMode = (zabAntennaOperatingMode)srvCoreAskBack8( Status, Service, ZAB_ASK_ANTENNA_MODE, (unsigned8)zabAntennaOperatingMode_Antenna1);

        zabZnpSysUtility_SetAntennaMode(Status, Service, antennaOperatingMode);
        if (erStatusIsError(Status))
          {
            /* Give an unknown status to end the action */
            srvCoreGiveBack8(Status, Service, ZAB_GIVE_ANTENNA_CURRENT_MODE, (unsigned8)zabAntennaOperatingMode_Unknown);
          }
        break;


      /*
      ** Get Network Info
      */
      case ZAB_ACTION_NWK_GET_INFO:
        sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_IN_PROGRESS);
        ZAB_SERVICE_ZNP_NWK(Service).getNetworkInfoActive = 1;
        getConfiguration(Status, Service, GET_CONFIG_TYPE_START_ALL);
        break;


      /*
      ** Get Network Maintenance Parameters
      */
      case ZAB_ACTION_NWK_MAINTENANCE_GET_PARAMS:
        /* Notify Process Started*/
        sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_GET);

        /* Validate various states are correct */
        if (validStatesForNetworkedAction(Status, Service) == zab_true)
          {
            /* Everything is good, so proceed with action */
            zabZnpZdoUtility_TcPingTimeGetReq(Status, Service);
            if (erStatusIsOk(Status))
              {
                ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NWK_MGMT_GET;
                /* No need for timeout, we will catch it in the SRSP timeout handler */

                /* Exit on success so we can clean up failures in one place below */
                return;
              }
            else
              {
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, (zabError)Status->Error);
              }
          }
        /* It failed for some reason, so notify it */
        sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_GET_FAILED);
        sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_NONE);
        break;


      /*
      ** Set Network Maintenance Parameters
      */
      case ZAB_ACTION_NWK_MAINTENANCE_SET_PARAMS:
        /* Notify Process Started*/
        sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_SET);

        /* Validate various states are correct */
        if (validStatesForNetworkedAction(Status, Service) == zab_true)
          {
            /* Everything is good, so proceed with action */
            nwkMntSlowTime = srvCoreAskBack32( Status, Service, ZAB_ASK_NETWORK_MAINTENANCE_SLOW_PING_TIME_MS, (unsigned32)NETWORK_MAINTENANCE_SLOW_PING_TIME_MS_DEFAULT);
            nwkMntFastTime = srvCoreAskBack32( Status, Service, ZAB_ASK_NETWORK_MAINTENANCE_FAST_PING_TIME_MS, (unsigned32)NETWORK_MAINTENANCE_FAST_PING_TIME_MS_DEFAULT);

            zabZnpZdoUtility_TcPingTimeSetReq(Status, Service, nwkMntSlowTime, nwkMntFastTime);
            if (erStatusIsOk(Status))
              {
                ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NWK_MGMT_SET;
                /* No need for timeout, we will catch it in the SRSP timeout handler */

                /* Exit on success so we can clean up failures in one place below */
                return;
              }
            else
              {
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, (zabError)Status->Error);
              }
          }
        /* It failed for some reason, so notify it */
        sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_SET_FAILED);
        sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_NONE);
        break;
    }
}

/******************************************************************************
 * Network Key Update SRSP Handler
 ******************************************************************************/
void zabZnpNwk_nwkKeyUpdateSrspHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result)
{
  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_KEY_UPDATE_REQ)
    {
      if ( erStatusIsOk(Status) && (result == ZNPI_API_ERR_SUCCESS) )
        {
          // Start a timer to give some delay before performing the switch
          ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NWK_KEY_SWITCH_DELAY;
          setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_NETWORK_KEY_SWITCH_DELAY_MS);
          ZAB_SERVICE_ZNP_NWK(Service).retries = 0;
        }
      else
        {
          ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
          sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_FAILED);
          sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
        }
    }
}

/******************************************************************************
 * Network Key Switch SRSP Handler
 ******************************************************************************/
void zabZnpNwk_nwkKeySwitchSrspHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result)
{
  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_KEY_SWITCH_REQ)
    {
      ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
      if (result == ZNPI_API_ERR_SUCCESS)
        {
          sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_SUCCESS);
        }
      else
        {
          sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_FAILED);
        }
      sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
    }
}

/******************************************************************************
 * Mgmt Permit Join SRSP Handler
 ******************************************************************************/
void zabZnpNwk_mgmtPermitJoinSrspHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result)
{
  switch(ZAB_SERVICE_ZNP_NWK(Service).currentItem)
    {

      case ZAB_NWK_CURRENT_ITEM_DISABLE_PERMIT_JOIN:
        zabZnpZdoUtility_PermitJoinReq(Status, Service,
                                       ZAB_ADDRESS_MODE_NWK_ADDRESS,
                                       ZAB_SERVICE_ZNP_NWK(Service).nwkAddr,
                                       0,
                                       zab_true,
                                       0);
#ifdef ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER
        if (getGpEnabled(Service) == szl_true)
          {
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_DISABLE_PERMIT_JOIN_GP_NEXT);
          }
        else
          {
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NONE);
          }
#else
        checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NONE);
#endif
        break;

#ifdef ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER
      case ZAB_NWK_CURRENT_ITEM_DISABLE_PERMIT_JOIN_GP_NEXT:
        checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NONE);
        zabZnpGp_SetCommissioningMode(Status, Service,
                                      ZNP_GP_COMMISSIONING_MODE_EXIT,
                                      0x01, //unsigned8 CommissioningModeExit,
                                      0); //unsigned16 CommissioningWindow);
        break;
#endif


      case ZAB_NWK_CURRENT_ITEM_ENABLE_PERMIT_JOIN:
#ifdef ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER
        /* If we were setting permit join and have GP enabled, we need to do the GP commissioning window next */
        if (getGpEnabled(Service) == szl_true)
          {
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_ENABLE_PERMIT_JOIN_GP_NEXT);
          }
        else
          {
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NONE);
          }
#else
        /* If we were setting permit join, mark it done */
        checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NONE);
#endif
        if (ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining > 0)
        {
          printVendor(Service, "ZNP NWK: Setting local PJ for = %d, remaining %d\n", ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining, ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining);
          zabZnpZdoUtility_PermitJoinReq(Status, Service,
                                         ZAB_ADDRESS_MODE_NWK_ADDRESS,
                                         ZAB_SERVICE_ZNP_NWK(Service).nwkAddr,
                                         ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining,
                                         zab_true,
                                         0);
        }
        break;

      case ZAB_NWK_CURRENT_ITEM_ENABLE_PERMIT_JOIN_UPDATE:
        /* If we were setting permit join, mark it done */
        checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NONE);

        if (ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining > 0)
        {
          printVendor(Service, "ZNP NWK: Setting local PJ for = %d, remaining %d\n", ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining, ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining);
          zabZnpZdoUtility_PermitJoinReq(Status, Service,
                                         ZAB_ADDRESS_MODE_NWK_ADDRESS,
                                         ZAB_SERVICE_ZNP_NWK(Service).nwkAddr,
                                         ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining,
                                         zab_true,
                                         0);
        }
        break;

#ifdef ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER
      case ZAB_NWK_CURRENT_ITEM_ENABLE_PERMIT_JOIN_GP_NEXT:
        /* If we were setting permit join, mark it done */
        checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NONE);

        if (getGpEnabled(Service) == szl_true)
          {
            if (ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining > 0)
            {
              printVendor(Service, "ZNP NWK: Setting GP Com Window = %d\n", ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining);
              zabZnpGp_SetCommissioningMode(Status, Service,
                                            ZNP_GP_COMMISSIONING_MODE_RESTRICTIVE,
                                            ZNP_GP_COMMISSIONING_EXIT_MODE_WINDOW_EXPIRATION,
                                            ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining);
            }
          }
        else
          {
            printError(Service, "ERROR: Enable PJ for GP butit's disbaled. Should never happen!");
          }
        break;
#endif

      default:
        break;
    }
}

/******************************************************************************
 * Write configuration response handler
 * Used to confirm success/fail of writes performed by networking state machine
 ******************************************************************************/
void zabZnpNwk_writeCfgRspHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result)
{
  unsigned64 epid;
  unsigned32 channelMask;
  unsigned8 security;
  unsigned16 panId;
  unsigned8 data[8];
  unsigned8 count;

  switch(ZAB_SERVICE_ZNP_NWK(Service).currentItem)
    {
      /* Steer Step 3: Form. Ask and set channel mask  */
      case ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_LT:
        if (result == ZNPI_API_ERR_SUCCESS)
          {
            channelMask = srvCoreAskBack32( Status, Service, ZAB_ASK_NWK_CHANNEL_MASK, M_DEFAULT_CHANNEL_MASK);
            COPY_OUT_32_BITS(data, channelMask);
            znpi_sapi_write_config( Status, Service, ZAB_ZNP_SAPI_NV_CFG_ID_CHANLIST, 4, data);
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_CH_MASK);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
            zabZnpNwk_create(Status, Service);
          }
        break;

      /* Steer Step 4: Form. Ask and set security  */
      case ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_CH_MASK:
        if (result == ZNPI_API_ERR_SUCCESS)
          {
            security = srvCoreAskBack8( Status, Service, ZAB_ASK_NWK_SECURITY_LEVEL, M_DEFAULT_SECURITY_LEVEL);
            znpi_sapi_write_config( Status, Service, ZAB_ZNP_SAPI_NV_CFG_ID_SECURITY_MODE, 1, &security);
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_SECURITY);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
            zabZnpNwk_create(Status, Service);
          }
        break;

      /* Steer Step 5: Form. Ask and set PANID  */
      case ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_SECURITY:
        if (result == ZNPI_API_ERR_SUCCESS)
          {
            /* Generate a non 0 or FFFF PanId with a fair number of tries */
            count = 0;
            do
              {
                panId = ZAB_RAND_16();
                count++;
              } while ( ((panId == 0x0000) || (panId == ZAB_TYPES_M_NETWORK_PAN_ID_UNKNOWN)) && (count < 10) );

            panId = srvCoreAskBack16( Status, Service, ZAB_ASK_NWK_PAN_ID, panId);

            /* Validate value */
            if ( (panId == 0x0000) || (panId == ZAB_TYPES_M_NETWORK_PAN_ID_UNKNOWN) )
              {
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
                zabZnpNwk_create(Status, Service);
                return;
              }

            COPY_OUT_16_BITS(data, panId);
            znpi_sapi_write_config( Status, Service, ZAB_ZNP_SAPI_NV_CFG_ID_PANID, 2, data);
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_PAN_ID);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
            zabZnpNwk_create(Status, Service);
          }
        break;

      /* Steer Step 6: Form. Ask and set EPANID  */
      case ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_PAN_ID:
        if (result == ZNPI_API_ERR_SUCCESS)
          {
            /* Create a Schneider EPID with 16bits of randomness*/
            epid = ZAB_RAND_16();
            epid <<= 48;
            epid += 0x02AB04015E10ULL;
            epid = srvCoreAskBack64( Status, Service, ZAB_ASK_NWK_EPID, epid);

            /* Validate value */
            if ( (epid == 0) || (epid == ZAB_TYPES_M_NETWORK_EPID_UNKNOWN) )
              {
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
                zabZnpNwk_create(Status, Service);
                return;
              }

            znpi_sapi_write_config( Status, Service, ZAB_ZNP_SAPI_NV_CFG_ID_EXTENDED_PAN_ID, 8, &epid);
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_EPID);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
            zabZnpNwk_create(Status, Service);
          }
        break;

      /* Steer Step 7: Form. Startup dongle  */
      case ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_EPID:
        if (result == ZNPI_API_ERR_SUCCESS)
          {
            setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NETWORKING);

            zabZnpZdoUtility_StartupFromApp(Status, Service, 100);
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_FORM_STARTUP);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
            zabZnpNwk_create(Status, Service);
          }
        break;

      case ZAB_NWK_CURRENT_ITEM_NWK_CLEAR:
        /* Init step 4b:
        **   If we succeeded to set the clear config bit, issue a soft reset to clear out network settings.
        **   This will cause a reset indication and reinitialise the network state machine.
        **   If we failed, reinitilise for another try. */
        if (result == ZNPI_API_ERR_SUCCESS)
          {
            znpi_sys_reset(Status, Service, SYS_SOFT_RESET);
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_RESTART);

            // Start the timeout for the confirm
            setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_NWK_CONFIRM_TIMEOUT_MS);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
            zabZnpNwk_create(Status, Service);
          }
        break;


      /* JoinEndDevice: We have set the logical type, now set channel list */
      case ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_SET_LT:
        if (result == ZNPI_API_ERR_SUCCESS)
          {
            /* We just set all channels and let the stack find the network via EPID */
            channelMask = M_DEFAULT_CHANNEL_MASK;
            COPY_OUT_32_BITS(data, channelMask);
            znpi_sapi_write_config( Status, Service, ZAB_ZNP_SAPI_NV_CFG_ID_CHANLIST, 4, data);
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_SET_CH_MASK);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
            zabZnpNwk_create(Status, Service);
          }
        break;

      /* JoinEndDevice: We have set the channel mask, now set the EPID */
      case ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_SET_CH_MASK:
        if (result == ZNPI_API_ERR_SUCCESS)
          {
            BeaconFormat_t beacon;
            /* Ask for the beacon.
             * We only need the EPID, but this keeps it consistent with router join */
            osMemSet(Status, (unsigned8*)&beacon, sizeof(BeaconFormat_t), 0xFF);
            srvCoreAskBackBuffer(Status, Service,ZAB_ASK_NWK_JOIN_BEACON, 0, sizeof(BeaconFormat_t), (unsigned8*)&beacon);

            znpi_sapi_write_config( Status, Service, ZAB_ZNP_SAPI_NV_CFG_ID_EXTENDED_PAN_ID, 8, &beacon.epid);
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_SET_EPID);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
            zabZnpNwk_create(Status, Service);
          }
        break;

      /* JoinEndDevice: Epid is set, so now startup from App  */
      case ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_SET_EPID:
        if (result == ZNPI_API_ERR_SUCCESS)
          {
            zabZnpZdoUtility_StartupFromApp(Status, Service, 100);
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_STARTUP);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
            zabZnpNwk_create(Status, Service);
          }
        break;

      default:
        /* Do nothing - we didn't initiate the write */
        break;
    }
}

/******************************************************************************
 * Network Address Response Handler
 ******************************************************************************/
void zabZnpNwk_setTxPowerRspHandler(erStatus* Status, zabService* Service)
{
  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_SETTING_TX_POWER_BEFORE_RESUME)
    {
      setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NETWORKING);

      // ZDO startup from app
      zabZnpZdoUtility_StartupFromApp(Status, Service, 100); // Try 100ms start delay for now - no idea what it does!
      checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_RESUME_STARTUP);
    }
}

/******************************************************************************
 * GP Set Parameter response handler
 * Used to confirm success/fail of writes performed by networking state machine
 ******************************************************************************/
void zabZnpNwk_gpSetParamRspHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result)
{
  if ( (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_RESUME_SET_LOOPBACK) ||
       (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_LOOPBACK) ||
       (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_JOIN_SET_LOOPBACK) )
    {
      if (result == ZNPI_API_ERR_SUCCESS)
        {
          getConfiguration(Status, Service, GET_CONFIG_TYPE_START_ALL);
        }
      else
        {
          printError(Service, "NWK: Error: SetGpParam failed with result 0x%02X\n", (unsigned8)result);
          zabZnpNwk_create(Status, Service);
        }
    }
}

/******************************************************************************
 * Device Logical Type Response Handler
 ******************************************************************************/
void zabZnpNwk_deviceLogicalTypeHandler(erStatus* Status, zabService* Service, znpSapiLogicalType logicalType)
{
  zabDeviceType deviceType;

  switch (logicalType)
    {
      case ZAB_ZNP_SAPI_LOGICAL_TYPE_COORDINATOR: deviceType = ZAB_DEVICE_TYPE_COORDINATOR; break;
      case ZAB_ZNP_SAPI_LOGICAL_TYPE_ROUTER:      deviceType = ZAB_DEVICE_TYPE_ROUTER; break;
      case ZAB_ZNP_SAPI_LOGICAL_TYPE_END_DEVICE:  deviceType = ZAB_DEVICE_TYPE_END_DEVICE; break;
      default:                                    deviceType = ZAB_DEVICE_TYPE_NONE;
                                                  printError(Service, "NWK: Error: Invalid logical type 0x%02X\n", (unsigned8)logicalType);
    }

  srvCoreGiveBack8(Status, Service, ZAB_GIVE_DEVICE_TYPE, (unsigned8)deviceType);
  getConfiguration(Status, Service, GET_CONFIG_TYPE_CONTINUE);
}

/******************************************************************************
 * Device State Response Handler
 ******************************************************************************/
void zabZnpNwk_deviceStateHandler(erStatus* Status, zabService* Service, ZDO_STATE state)
{
  ZAB_SERVICE_ZNP_NWK(Service).znpState = state;

  getConfiguration(Status, Service, GET_CONFIG_TYPE_CONTINUE);
}

/******************************************************************************
 * Parent Network Address Response Handler
 ******************************************************************************/
void zabZnpNwk_parentNwkAddrHandler(erStatus* Status, zabService* Service, unsigned16 nwkAddr)
{
  srvCoreGiveBack16(Status, Service, ZAB_GIVE_PARENT_NWK_ADDRESS, nwkAddr);
  getConfiguration(Status, Service, GET_CONFIG_TYPE_CONTINUE);
}


/******************************************************************************
 * Parent IEEE Address  Response Handler
 ******************************************************************************/
void zabZnpNwk_parentIeeeAddressHandler(erStatus* Status, zabService* Service, unsigned64 ieeeAddress)
{
  srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_PARENT_IEEE, 8, (unsigned8 *) & ieeeAddress);
  getConfiguration(Status, Service, GET_CONFIG_TYPE_CONTINUE);
}

/******************************************************************************
 * Network Address Response Handler
 ******************************************************************************/
void zabZnpNwk_nwkAddrHandler(erStatus* Status, zabService* Service, unsigned16 nwkAddr)
{
  unsigned8 resume;

  /* ARTF105443: Clear NV config as well as network state, this includes the nwk key! */
  unsigned8 startupOptions = ZNPI_ZB_ZCD_STARTOPT_DEFAULT_CONFIG_STATE | ZNPI_ZB_ZCD_STARTOPT_DEFAULT_NETWORK_STATE;

  // This is the first check to see if we have previous settings or not
  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_SHORT_ADDR)
    {
      /* Init Step 2a: If nwk address is 0xFFFE there are no previous settings. Notify NetworkNone and finish */
      if (nwkAddr == ZAB_ZNP_NWK_M_NWK_ADDR_NO_PREVIOUS_SETTINGS)
        {
          /* No need to set state for this as it is just a single ZNP command
           * and we don't need the response. */
          setTxPower(Status, Service);

          setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
          ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
        }
      /* Init Step 2b: If nwk address is not 0xFFFE there are previous settings, so check if app wants to resume them */
      else
        {

          resume = srvCoreAskBack8( Status, Service, ZAB_ASK_NWK_RESUME, M_DEFAULT_RESUME);

          /*
          ** Init Step 3:
          **    If resuming: Startup from app
          **    Otherwise, write clear config bit into startup options and when it responds trigger a reset to clear out network settings
          */
          if(resume)
            {
              /* No need to set state for this as it is just a single ZNP command
               * and we don't need the response. */
              checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_SETTING_TX_POWER_BEFORE_RESUME);
              setTxPower(Status, Service);
            }
          else
            {
              setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_LEAVING);

              znpi_sapi_write_config( Status, Service, ZAB_ZNP_SAPI_NV_CFG_ID_STARTUP_OPTION, 1, &startupOptions);
              checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_CLEAR);
            }
        }
    }

  /* This state is confirming the short address has become 0xFFFE after the reset and hence the clearing of settings worked */
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_CONFIRM_SHORT_ADDR_INVALID)
    {
      /* It worked, go to network none */
      if (nwkAddr == ZAB_ZNP_NWK_M_NWK_ADDR_NO_PREVIOUS_SETTINGS)
        {

          /* No need to set state for this as it is just a single ZNP command
           * and we don't need the response. */
          setTxPower(Status, Service);

          setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
          ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
        }
      /* It failed, handle error gracefully */
      else
        {
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_ERASE_NETWORK_SETTINGS_FAILED);
          setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_UNINITIALISED);
          ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
        }
    }

  // This is the get config process - kept separate from above to keep it simple
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_CFG_SHORT_ADDR)
    {
      /* Get Config 2: Channel */
      ZAB_SERVICE_ZNP_NWK(Service).nwkAddr = nwkAddr;
      getConfiguration(Status, Service, GET_CONFIG_TYPE_CONTINUE);
    }
}

/******************************************************************************
 * Channel Response Handler
 ******************************************************************************/
void zabZnpNwk_channelHandler(erStatus* Status, zabService* Service, unsigned8 channel)
{
  ZAB_SERVICE_ZNP_NWK(Service).channel = channel;

  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE)
    {
      ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
      ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;

      // Note: It could be more optimal to check the correct channel happened here, but there is no reason it should not be...
      sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_SUCCESS);
      sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_NONE);
    }
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_CFG_CHANNEL)
    {
      /* Get Config 3: PAN ID */
      getConfiguration(Status, Service, GET_CONFIG_TYPE_CONTINUE);
    }
}

/******************************************************************************
 * PAN ID Response Handler
 ******************************************************************************/
void zabZnpNwk_panIdlHandler(erStatus* Status, zabService* Service, unsigned16 panId)
{
  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_CFG_PAN)
    {
      /* Get Config 4: EPID */
      getConfiguration(Status, Service, GET_CONFIG_TYPE_CONTINUE);
    }
}

/******************************************************************************
 * EPID Response Handler
 ******************************************************************************/
void zabZnpNwk_extendedPanIdHandler(erStatus* Status, zabService* Service, unsigned64 extendedPanId)
{
  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_CFG_EPID)
    {
      getConfiguration(Status, Service, GET_CONFIG_TYPE_CONTINUE);
    }
}

/******************************************************************************
 * Network Key Sequence Number Response Handler
 ******************************************************************************/
void zabZnpNwk_nwkKeySequenceNumberHandler(erStatus* Status, zabService* Service, unsigned8 nwkKeySeqNum)
{
  unsigned8 nwkKey[ZAB_ZNP_ZDO_M_KEY_LENGTH];
  unsigned8 index;

  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_KEY_SEQ_NUM_REQ)
    {
      if (nwkKeySeqNum != 0xFF)
        {
          // Increment to the next KSN and save
          ZAB_SERVICE_ZNP_NWK(Service).nwkKeySeqNum = nwkKeySeqNum+1;

          for (index = 0; index < ZAB_ZNP_ZDO_M_KEY_LENGTH; index++)
            {
              nwkKey[index] = (unsigned8)ZAB_RAND_16();
            }

          zabZnpZdoUtility_NwkKeyUpdateReq(Status, Service,
                                           ZAB_BROADCAST_ALL,
                                           ZAB_SERVICE_ZNP_NWK(Service).nwkKeySeqNum,
                                           nwkKey);
          // Clear key off the stack
          osMemSet(Status, nwkKey, ZAB_ZNP_ZDO_M_KEY_LENGTH, 0);

          if (erStatusIsOk(Status))
            {
              /* Set the state and await the SRSP or SRSP timeout */
              ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NWK_KEY_UPDATE_REQ;
            }
          else
            {
              sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
              sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_FAILED);
              sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
            }
        }
      // KSN was invalid, so fail gracefully
      else
        {
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_KEY_SEQ_NUMBER_INVALID);
          sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_FAILED);
          sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
        }
    }
}

/******************************************************************************
 * IEEE Address Handler
 ******************************************************************************/
void zabZnpNwk_ieeeAddressHandler(erStatus* Status, zabService* Service, unsigned64 ieeeAddress)
{
  ZAB_SERVICE_ZNP_NWK(Service).ieeeAddr = ieeeAddress;
}

/******************************************************************************
 * ZDO Startup From App Response Handler
 ******************************************************************************/
void zabZnpNwk_startupFromAppRspHandler(erStatus* Status, zabService* Service, unsigned8 result)
{
  switch (result)
    {
      /* Restored Network State */
      case 0:
          /* Init Step 4a: We are trying to resume and have done so */
        if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_RESUME_STARTUP)
          {
            /* Now ask about GP Endpoint. If needed, set it. Otherwise get configuration */
            if (getGpLoopback(Status, Service) == zab_true)
              {
                checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_RESUME_SET_LOOPBACK);
              }
            else
              {
                getConfiguration(Status, Service, GET_CONFIG_TYPE_START_ALL);
              }
            return;
          }
        // We should never get here unless something other than this process starts the dongle
        printError(Service, "NWK: WARNING: THIS SHOULD NEVER HAPPEN - STARTUP FROM APP A - INVESTIGATE! NwkItem = %d\n", ZAB_SERVICE_ZNP_NWK(Service).currentItem);
        break;

      /* New Network State */
      case 1:
        /* Steer Step 7: Forming in progress, set timer and wait for completion.   */
        if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_FORM_STARTUP)
          {
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_FORM_IN_PROGRESS);

            /* Set a long timeout on the form */
            setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_NWK_FORM_TIMEOUT_MS);
            return;
          }

        /* JoinEndDevice: Joining in progress, set timer and wait for completion.   */
        else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_STARTUP)
          {
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_IN_PROGRESS);

            /* Set a long timeout on the form */
            setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_END_DEVICE_JOIN_TIMEOUT_MS);
            return;
          }


        printError(Service, "NWK: WARNING: THIS SHOULD NEVER HAPPEN - STARTUP FROM APP B - INVESTIGATE\n");
        break;

      /* Leave and not started */
      case 2:
        printError(Service, "NWK: Leave and not started - What is this? How did you get here? This should be fixed under ARTF109790\n");
        break;

      default:
        printError(Service, "NWK: ERROR: Startup From App bad response %d - should not happen!\n", result);
        break;

    }
  zabZnpNwk_create(Status, Service);
}

/******************************************************************************
 * ZDO State Change Handler
 ******************************************************************************/
void zabZnpNwk_zdoStateChangeHandler(erStatus* Status, zabService* Service, ZDO_STATE zdoState)
{
  ZDO_STATE oldZdoState = ZAB_SERVICE_ZNP_NWK(Service).znpState;
  ZAB_SERVICE_ZNP_NWK(Service).znpState = zdoState;

  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem  == ZAB_NWK_CURRENT_ITEM_NWK_FORM_IN_PROGRESS)
    {
      switch (zdoState)
        {
          case DEV_COORD_STARTING:
            // Wait while it starts
            break;

          case DEV_ZB_COORD:
            // Started ok as coordinator. Cancel timeout and continue.
            ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;

            /* Now ask about GP Endpoint. If needed, set it. Otherwise get configuration */
            if (getGpLoopback(Status, Service) == zab_true)
              {
                checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_FORM_SET_LOOPBACK);
              }
            else
              {
                getConfiguration(Status, Service, GET_CONFIG_TYPE_START_ALL);
              }
            break;

          default:
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_FORM_NETWORK_FAILED);
            ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
            setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
            break;
        }
    }

  /* ARTF165341: Router Join is in progress.
   *  - Wait during transitory states.
   *  - End once Router
   *  - Quit if other crazy state */
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem  == ZAB_NWK_CURRENT_ITEM_NWK_JOIN_AWAITING_AUTH)
    {
      switch (zdoState)
        {
          case DEV_NWK_DISC:
          case DEV_NWK_JOINING:
          case DEV_END_DEVICE_UNAUTH:
            // Wait while it starts
            break;

          case DEV_ROUTER:
            // Started ok. Cancel timeout and continue with getting configuration info.
            ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
            /* Now ask about GP Endpoint. If needed, set it. Otherwise get configuration */
            if (getGpLoopback(Status, Service) == zab_true)
              {
                checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_JOIN_SET_LOOPBACK);
              }
            else
              {
                getConfiguration(Status, Service, GET_CONFIG_TYPE_START_ALL);
              }
            break;

          default:
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_JOIN_NETWORK_FAILED);
            ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
            setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
            break;
        }
    }

  /* End Device Join is in progress.
   *  - Wait during transitory states.
   *  - End once End Device
   *  - Quit if other crazy state */
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem  == ZAB_NWK_CURRENT_ITEM_NWK_JOIN_ED_IN_PROGRESS)
    {
      switch (zdoState)
        {
          case DEV_NWK_DISC:
          case DEV_NWK_JOINING:
          case DEV_END_DEVICE_UNAUTH:
            // Wait while it starts
            break;

          case DEV_END_DEVICE:
            // Started ok as ED. Cancel timeout and continue with getting configuration info.
            ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
            getConfiguration(Status, Service, GET_CONFIG_TYPE_START_ALL);
            break;

          default:
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_JOIN_NETWORK_FAILED);
            ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
            setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
            break;
        }
    }

  /* During normal operation, handle changes in end device state*/
  else if ( ( (ZAB_SERVICE_ZNP_NWK(Service).nwkState == ZAB_NWK_STATE_NETWORKED) || (ZAB_SERVICE_ZNP_NWK(Service).nwkState == ZAB_NWK_STATE_NETWORKED_NO_COMMS) ) &&
            (ZAB_SERVICE_ZNP_NWK(Service).currentItem  == ZAB_NWK_CURRENT_ITEM_NONE) &&
            (zdoState != oldZdoState) )
    {

      if (ZAB_SERVICE_ZNP_NWK(Service).znpState == DEV_END_DEVICE)
        {
          getConfiguration(Status, Service, GET_CONFIG_TYPE_START_END_DEVICE_ONLINE);
        }
      else if ( (ZAB_SERVICE_ZNP_NWK(Service).znpState == DEV_NWK_ORPHAN) ||
                (ZAB_SERVICE_ZNP_NWK(Service).znpState == DEV_NWK_DISC) )
        {
          getConfiguration(Status, Service, GET_CONFIG_TYPE_START_END_DEVICE_OFFLINE);
        }
    }
}

/******************************************************************************
 * Network Discovery Response Handler
 ******************************************************************************/
void zabZnpNwk_nwkDiscoveryRspHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result)
{
  /* Discover Step 2: Check response is good and active scan is starting */
  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_DISCOVER)
    {
      if ( erStatusIsOk(Status) && (result == ZNPI_API_ERR_SUCCESS) )
        {
          setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_DISCOVERING);
          ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NWK_DISCOVER_IN_PROGRESS;
          // Start the timeout for the confirm
          setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_NWK_CONFIRM_TIMEOUT_MS);
        }
      else
        {
          printError(Service, "NWK: DiscRsp Error %d\n", result);
          ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
          setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
        }
    }
}

/******************************************************************************
 * Network Discovery Confirm Handler
 ******************************************************************************/
void zabZnpNwk_NwkDiscoveryCnfHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result)
{
  /* Discover Step 4: Active scan complete, return to NetworkNone and await steering or another discovery */
  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_DISCOVER_IN_PROGRESS)
    {
      // Confirm received, so cancel timer
      ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;

      switch (result)
        {
          case ZNPI_API_ERR_SUCCESS:
          case ZNPI_API_ERR_MAC_NO_BEACON:
            setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_DISCOVERY_COMPLETE);
            setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
            ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
            break;

          default:
            //printError(Service, "NWK: Disc Conf Error 0x%02X\n", result);
            ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
            setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
            break;
        }
    }
}

/******************************************************************************
 * Join Response Handler
 ******************************************************************************/
void zabZnpNwk_joinRspHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result)
{
  /* Steer Step 3: Join - check join response is success, then await confirm */
  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_JOIN_REQ)
    {
      if ( erStatusIsOk(Status) && (result == ZNPI_API_ERR_SUCCESS) )
        {
          ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NWK_JOIN_IN_PROGRESS;
          // Start the timeout for the confirm
          setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_NWK_CONFIRM_TIMEOUT_MS);
        }
      else
        {
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_JOIN_NETWORK_FAILED);
          ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
          setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
        }
    }
}

/******************************************************************************
 * Join Confirm Handler
 ******************************************************************************/
void zabZnpNwk_joinCnfHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result)
{
  /* Steer Step 4: Join - Check confirm status is success */
  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_JOIN_IN_PROGRESS)
    {
      // Confirm received, so cancel timer
      ZAB_SERVICE_ZNP_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;

      switch (result)
        {
          case ZNPI_API_ERR_SUCCESS:
            /* ARTF165341: We are now joined, but not yet Authenticated, so wait for ZDO State = ROUTER */
            setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_NWK_CONFIRM_TIMEOUT_MS);
            checkStatusSetItem(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_JOIN_AWAITING_AUTH);
            break;

          default:
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_JOIN_NETWORK_FAILED);
            ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
            setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
            break;
        }
    }
}

/******************************************************************************
 * Handle an incoming Permit Join Indication
 ******************************************************************************/
void zabZnpNwk_permitJoinIndicationHandler(erStatus* Status, zabService* Service, unsigned8 permitJoinTime)
{
  /* We have the PJ indication, but need to work out what/if to indicate upwards.
   * If we are running a PJ window locally and the new time is (zero or longer than the current window, cancel ours and use theirs.
   * If it is shorter, leaves ours running but update the current network time to the value they set for the network. */

  if (ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining > 2)
    {
      if ( (permitJoinTime == 0) ||
           (permitJoinTime > ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining+1) )
        {
          ZAB_SERVICE_ZNP_NWK(Service).permitJoinTimeRemaining = 0;
          ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining = 0;
          sapMsgPutNotifyNetworkPermitJoinState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, permitJoinTime);
        }
      else
        {
          ZAB_SERVICE_ZNP_NWK(Service).currentPermitJoinTimeRemaining = permitJoinTime;
        }
    }
  else
    {
      sapMsgPutNotifyNetworkPermitJoinState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, permitJoinTime);
    }
}

/******************************************************************************
 * Handle an incoming Local Leave Indications
 ******************************************************************************/
void zabZnpNwk_localLeaveHandler(erStatus* Status, zabService* Service, unsigned8 rejoin)
{
  /* For now we don't try to handle rejoins, just let them happen naturally */
  if ( erStatusIsOk(Status) && (!rejoin) )
    {
      /* ARTF113925:
       *   ARTF109999 adds a leave indication when resuming with resume = false, which was appearing hear
       *   and causing double leave indications, so ignore this if already leaving */
      if ( ZAB_SERVICE_ZNP_NWK(Service).nwkState != ZAB_NWK_STATE_LEAVING)
        {
          /* The ZNP is about to reset, so be brutal: Cancel any action in progress and set state to leaving.
           * Run a timer just in case we miss the reset indication which should be following in a few seconds. */
          ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_LEAVING;

          setTimeout(Status, Service, ZAB_VND_CFG_ZNP_M_NWK_LEAVE_TIMEOUT_MS);

          setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_LEAVING);
        }
    }
}

/******************************************************************************
 * SRSP Timeout Handler
 * Notifies state machine a command has timed out. It will handle it if
 * it generated the command.
 ******************************************************************************/
void zabZnpNwk_nwkMaintenaceParamSrspHandler(erStatus* Status, zabService* Service,
                                             ZNPI_API_ERROR_CODES result,
                                             unsigned32 PingTimeSlow,
                                             unsigned32 PingTimeFast)
{
  if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_MGMT_GET)
    {
      ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
      if (result == ZNPI_API_ERR_SUCCESS)
        {
          sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_GET_SUCCESS);
        }
      else
        {
          sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_GET_FAILED);
        }
      sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_NONE);
    }
  else if (ZAB_SERVICE_ZNP_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_MGMT_SET)
    {
      ZAB_SERVICE_ZNP_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
      if (result == ZNPI_API_ERR_SUCCESS)
        {
          sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_SET_SUCCESS);
        }
      else
        {
          sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_SET_FAILED);
        }
      sapMsgPutNotifyNetworkMaintParamState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_MAINTENANCE_PARAM_STATE_NONE);
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
