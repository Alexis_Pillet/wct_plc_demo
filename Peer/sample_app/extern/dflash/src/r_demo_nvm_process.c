/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_main.c
*    @version
*        $Rev: 3384 $
*    @last editor
*        $Author: a5089752 $
*    @date
*        $Date:: 2017-05-31 13:49:31 +0900#$
* Description :
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/

#include "r_typedefs.h"
#include "r_c3sap_api.h"
#include "r_demo_app.h"
#include "r_demo_nvm_process.h"
#include "r_dflash_lib.h"

#include "r_dflash_usage_map.h"
#include "iorx631.h"

/******************************************************************************
Macro definitions
******************************************************************************/
/******************************************************************************
Typedef definitions
******************************************************************************/
/******************************************************************************
Private global variables and functions
******************************************************************************/
/******************************************************************************
Exported global variables
******************************************************************************/
/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/
const uint8_t g_rom_nvm_def_eui64[] = {
    0xFF, 0x01, 0x02, 0xFF, 0xFE, 0x00, 0x00, 0x00
};
const uint8_t g_rom_nvm_def_psk[] = {
    0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF
};
const uint8_t g_rom_nvm_def_coordaddr[] = {
    0x00, 0x00,
};

const uint8_t g_rom_nvm_def_extid_p[] = {
    0x48, 0x45, 0x4D, 0x53
};

const uint8_t g_rom_nvm_def_extid_s[] = {
    0x53, 0x4D,
};
const uint8_t g_rom_nvm_def_gmk[2][16] = {
    {0xAF, 0x4D, 0x6D, 0xCC, 0xF1, 0x4D, 0xE7, 0xC1, 0xC4, 0x23, 0x5E, 0x6F, 0xEF, 0x6C, 0x15, 0x1F},
    {0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0, 0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88}
};

const uint8_t g_rom_nvm_sycnword[] = {
    0xBA, 0xC0, 0x00, 0x00,
};

const uint8_t g_rom_nvm_cert_eui64[] = {
    0xF8, 0x72, 0x6F, 0xBA, 0x00, 0x00, 0x00, 0x01
};
const uint8_t g_rom_nvm_cert_psk[] = {
    0x7F, 0xFE, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};
const uint8_t g_rom_nvm_cert_panid[] = {
    0x78, 0x1D,
};
const uint8_t g_rom_nvm_cert_extid_p[] = {
    0x48, 0x45, 0x4D, 0x53
};
const uint8_t g_rom_nvm_cert_extid_s[] = {
    0x53, 0x4D,
};

/******************************************************************************
Functions
******************************************************************************/

/******************************************************************************
* Function Name: apl_get_port_bit
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint8_t apl_get_port_bit (uint8_t bit)
{
    r_port_switch_t read_port;
    uint8_t         ret = 0;

#ifndef _MSC_VER
    read_port.Byte = ~(PORTE.PIDR.BYTE);
#else
    read_port.Byte = 0x80;
#endif

    switch (bit)
    {
        case 0:
            ret = read_port.bit.b0;
            break;

        case 1:
            ret = read_port.bit.b1;
            break;

        case 2:
            ret = read_port.bit.b2;
            break;

        case 3:
            ret = read_port.bit.b3;
            break;

        case 4:
            ret = read_port.bit.b4;
            break;

        case 5:
            ret = read_port.bit.b5;
            break;

        case 6:
            ret = read_port.bit.b6;
            break;

        case 7:
            ret = read_port.bit.b7;
            break;

        default:
            break;
    } /* switch */

    return ret;
} /* apl_get_port_bit */
/******************************************************************************
   End of function  apl_get_port_bit
******************************************************************************/




/******************************************************************************
* Function Name: r_demo_et_gen_dev_config
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_et_gen_dev_config (uint8_t * param, r_cap_dev_cfg_t * pDev)
{
    uint8_t i;
    R_memset (pDev, 0, sizeof (r_cap_dev_cfg_t));

    /* EUI64 */
    R_memcpy (pDev->extendedAddress, g_rom_nvm_def_eui64, sizeof (g_rom_nvm_def_eui64));
    R_memcpy (&pDev->extendedAddress[6], param, 2);

    /* PSK */
    R_memcpy (pDev->psk, g_rom_nvm_def_psk, sizeof (g_rom_nvm_def_psk));

    /* COORDADDR */
    R_memcpy (pDev->coordAddr, g_rom_nvm_def_coordaddr, sizeof (g_rom_nvm_def_coordaddr));

    /* PANID */
    pDev->panid[0] = (uint8_t)(param[1] << 4);
    if (param[1] >> 4)
    {
        pDev->panid[0] = (uint8_t)(pDev->panid[0] + 0x4u);
    }
    else
    {
        /**/
    }
    pDev->panid[1] = param[0];

    /* GMK */
    R_memcpy (pDev->gmk, g_rom_nvm_def_gmk, sizeof (g_rom_nvm_def_gmk));

    /* EXTID */
    pDev->extID[0].length = APL_EXTID_P_LEN;
    R_memcpy (pDev->extID[0].id, g_rom_nvm_def_extid_p, sizeof (g_rom_nvm_def_extid_p));
    for (i = 0; i < (APL_EXTID_P_LEN - (sizeof (g_rom_nvm_def_extid_p))); i++)
    {
        pDev->extID[0].id[(i + (sizeof (g_rom_nvm_def_extid_p)))] = i;
    }

    pDev->extID[1].length = APL_EXTID_S_LEN;
    R_memcpy (pDev->extID[1].id, g_rom_nvm_def_extid_s, sizeof (g_rom_nvm_def_extid_s));
    for (i = 0; i < (APL_EXTID_S_LEN - (sizeof (g_rom_nvm_def_extid_s))); i++)
    {
        pDev->extID[1].id[i + (sizeof (g_rom_nvm_def_extid_s))] = i;
    }

    return;
} /* r_demo_et_gen_dev_config */
/******************************************************************************
   End of function  r_demo_et_gen_dev_config
******************************************************************************/



/******************************************************************************
* Function Name: r_demo_et_gen_cert_dev_config
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_et_gen_cert_dev_config (r_cap_dev_cfg_t * pDevCfg)
{
    uint8_t i;
    R_memset (pDevCfg, 0, sizeof (r_cap_dev_cfg_t));

    /* EUI64 */
    R_memcpy (pDevCfg->extendedAddress, g_rom_nvm_cert_eui64, sizeof (g_rom_nvm_cert_eui64));

    /* PSK */
    R_memcpy (pDevCfg->psk, g_rom_nvm_cert_psk, sizeof (g_rom_nvm_cert_psk));

    /* COORDADDR */
    R_memcpy (pDevCfg->coordAddr, g_rom_nvm_def_coordaddr, sizeof (g_rom_nvm_def_coordaddr));

    /* PANID */
    R_memcpy (pDevCfg->panid, g_rom_nvm_cert_panid, sizeof (g_rom_nvm_cert_panid));

    /* GMK */
    R_memcpy (pDevCfg->gmk, g_rom_nvm_def_gmk, sizeof (g_rom_nvm_def_gmk));

    /* EXTID */
    pDevCfg->extID[0].length = APL_EXTID_P_LEN;
    R_memcpy (pDevCfg->extID[0].id, g_rom_nvm_cert_extid_p, sizeof (g_rom_nvm_cert_extid_p));
    for (i = 0; i < (APL_EXTID_P_LEN - (sizeof (g_rom_nvm_cert_extid_p))); i++)
    {
        pDevCfg->extID[0].id[i + (sizeof (g_rom_nvm_cert_extid_p))] = i;
    }

    pDevCfg->extID[1].length = APL_EXTID_S_LEN;
    R_memcpy (pDevCfg->extID[1].id, g_rom_nvm_cert_extid_s, sizeof (g_rom_nvm_cert_extid_s));
    for (i = 0; i < (APL_EXTID_S_LEN - (sizeof (g_rom_nvm_cert_extid_s))); i++)
    {
        pDevCfg->extID[1].id[i + (sizeof (g_rom_nvm_cert_extid_s))] = i;
    }

    return;
} /* r_demo_et_gen_cert_dev_config */
/******************************************************************************
   End of function  r_demo_et_gen_cert_dev_config
******************************************************************************/


/******************************************************************************
* Function Name: r_demo_et_read_dev_config
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_et_read_dev_config (uint8_t id, uint8_t * out_ptr)
{
    r_nvm_config_t * pConfig = (r_nvm_config_t *)r_dflash_tmp_buff ();

    r_demo_nvm_read (R_NVM_COMMON_CH, R_NVM_ID_BOOT_MODE, sizeof (r_nvm_config_t), (uint8_t *)pConfig);
    R_memcpy (out_ptr, (uint8_t *)&pConfig->devCfg[id], sizeof (r_cap_dev_cfg_t));
    return;
}
/******************************************************************************
   End of function  r_demo_et_read_dev_config
******************************************************************************/

/******************************************************************************
* Function Name: r_demo_et_read_backup
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint16_t r_demo_et_read_backup (uint8_t id, uint8_t * out_ptr)
{
    r_demo_backup_t           bkup_full;
    r_demo_backup_payload_t * pBkup = (r_demo_backup_payload_t *)out_ptr;

    r_demo_nvm_read (id, R_NVM_ID_BACKUP, sizeof (r_demo_backup_t), (uint8_t *)&bkup_full);

    R_memcpy ((uint8_t *)pBkup, (const uint8_t *)&bkup_full.bkup, sizeof (r_demo_backup_payload_t));

    return sizeof (r_demo_backup_payload_t);
}
/******************************************************************************
   End of function  r_demo_et_read_backup
******************************************************************************/


/******************************************************************************
* Function Name: r_demo_et_gen_backup
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint16_t r_demo_et_gen_backup (r_demo_backup_payload_t * in_bkup, uint8_t * out_ptr)
{
    r_demo_backup_t * pBkup = (r_demo_backup_t *)out_ptr;

    R_memset ((uint8_t *)pBkup, 0, sizeof (r_demo_backup_t));

    R_memcpy (pBkup->SyncWord, g_rom_nvm_sycnword, sizeof (g_rom_nvm_sycnword));
    R_memcpy ((uint8_t *)&pBkup->bkup, (const uint8_t *)in_bkup, sizeof (r_demo_backup_payload_t));

    return sizeof (r_demo_backup_t);
}
/******************************************************************************
   End of function  r_demo_et_gen_backup
******************************************************************************/


/******************************************************************************
* Function Name: r_demo_nvm_read
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_nvm_read (uint8_t id, uint8_t simbol, uint16_t byte_len, uint8_t * out_ptr)
{
    uint32_t  tmp_l = (uint32_t)NULL;
    uint8_t * ptr   = NULL;

    tmp_l = DF_GET_ADDR (id, simbol);
    r_dflash_read (&ptr, tmp_l, byte_len);
    R_memcpy (out_ptr, ptr, byte_len);
}
/******************************************************************************
   End of function  r_demo_nvm_read
******************************************************************************/



/******************************************************************************
* Function Name: r_demo_nvm_write
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_nvm_write (uint8_t id, uint8_t simbol, uint16_t byte_len, uint8_t * in_ptr)
{
    uint32_t tmp_l = (uint32_t)NULL;

    tmp_l = DF_GET_ADDR (id, simbol);
    r_dflash_write (tmp_l, in_ptr, byte_len);
}
/******************************************************************************
   End of function  r_demo_nvm_write
******************************************************************************/



/******************************************************************************
* Function Name: r_demo_nvm_erase
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_nvm_erase (uint8_t id, uint8_t simbol)
{
    uint32_t tmp_l = (uint32_t)NULL;

    tmp_l = DF_GET_ADDR (id, simbol);
    r_dflash_erase (tmp_l, DFLASH_BLOCK_SIZE_LARGE);
}
/******************************************************************************
   End of function  r_demo_nvm_erase
******************************************************************************/


/******************************************************************************
* Function Name: r_demo_nvm_config_init
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_nvm_config_init (void)
{
    r_nvm_config_t * pConfig = (r_nvm_config_t *)r_dflash_tmp_buff ();
    uint8_t          addr[2] = {0x01, 0x01};

    r_demo_nvm_read (R_NVM_COMMON_CH, R_NVM_ID_BOOT_MODE, sizeof (r_nvm_config_t), (uint8_t *)pConfig);

    R_memset (&pConfig->aplMode, 0, sizeof (r_apl_mode_t));
    pConfig->aplMode.bandPlan = R_G3_BANDPLAN_CENELEC_A;
    R_memset (&pConfig->aplMode.tonemask, 0xFF, 9);
    pConfig->aplMode.ch[R_DEMO_G3_USE_PRIMARY_CH].startMode = R_DEMO_MODE_AUTO;
    r_demo_et_gen_dev_config (addr, &pConfig->devCfg[0]);
    addr[1] |= 0x10;
    r_demo_et_gen_dev_config (addr, &pConfig->devCfg[1]);

    r_demo_nvm_write (R_NVM_COMMON_CH, R_NVM_ID_BOOT_MODE, sizeof (r_nvm_config_t), (uint8_t *)pConfig);
}
/******************************************************************************
   End of function  r_demo_nvm_config_init
******************************************************************************/


/******************************************************************************
* Function Name: r_demo_nvm_config_edit
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_nvm_config_edit (uint8_t id, uint8_t * param)
{
    r_nvm_config_t * pConfig = (r_nvm_config_t *)r_dflash_tmp_buff ();
    r_demo_nvm_read (R_NVM_COMMON_CH, R_NVM_ID_BOOT_MODE, sizeof (r_nvm_config_t), (uint8_t *)pConfig);
    r_demo_et_gen_dev_config (param, &pConfig->devCfg[id]);
    r_demo_nvm_write (R_NVM_COMMON_CH, R_NVM_ID_BOOT_MODE, sizeof (r_nvm_config_t), (uint8_t *)pConfig);
    return;
}
/******************************************************************************
   End of function  r_demo_nvm_config_edit
******************************************************************************/


/******************************************************************************
* Function Name: r_demo_nvm_backup_write
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void r_demo_nvm_backup_write (uint8_t id, r_demo_backup_payload_t * bkup)
{
    uint8_t * pBuff = r_dflash_tmp_buff ();
    uint16_t  length;

    length = r_demo_et_gen_backup (bkup, pBuff);
    r_demo_nvm_write (id, R_NVM_ID_BACKUP, length, pBuff);
}
/******************************************************************************
   End of function  r_demo_nvm_backup_write
******************************************************************************/

