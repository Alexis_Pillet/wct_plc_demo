/*******************************************************************************
  Filename    : szl_extended_api.c
  $Date       : 2013-08-28                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : This is the Extended API implementation for the  SZL
 *
 * 002.002.021  21-Apr-16   MvdB   ARTF167807: Support Multi-Cluster Attribute Read/Write for GP
 * 002.002.032  09-Jan-17   MvdB   ARTF172065: Rationalise duplicated private functions into public SZLEXT_ZigBeeDataTypeIsAnalog()
*******************************************************************************/

/**************************************************************************************************
* INCLUDES
**************************************************************************************************/
#include <stdio.h>
#include <string.h>
#include "szl.h"
#include "szl_internal.h"
#include "szl_timer.h"
#include "endianness.h"
#include "zcl_manager_zab.h"
#include "szl_zab.h"
#include "szl_extended_api.h"

/**************************************************************************************************
* DEFINES
**************************************************************************************************/

/* The maximum number of clusters supported in a Multi Cluster request */
#define SZL_EXT_MULTI_CLUSTER_MAX 10

/**************************************************************************************************
* ENUMs, STRUCTs & UNIONs
**************************************************************************************************/

/**************************************************************************************************
* PROTOS
**************************************************************************************************/

static SZL_RESULT_t SendReportOnDemand(zabService* Service, SZL_Addresses_t* DestAddrMode, SZL_EP_t Endpoint, szl_bool ManufacturerSpecific, szl_uint16 ClusterID, szl_uint8 NumberOfAttributes, szl_uint16 Attributes[]);
static SZL_RESULT_t SZLEXT_AttributeReportOnDemandExt(zabService* Service,
                                                      SZL_Addresses_t* DestAddr,
                                                      SZL_EP_t Endpoint,
                                                      szl_bool ManufacturerSpecific,
                                                      szl_uint16 ClusterID,
                                                      szl_uint8 NumberOfAttributes,
                                                      szl_uint16 Attributes[]);

/**************************************************************************************************
* VARIABLES
**************************************************************************************************/

/**************************************************************************************************
* FUNCTIONS
**************************************************************************************************/

/**
 * Send Attribute report on demand
 */
SZL_RESULT_t SZLEXT_AttributeReportOnDemand(zabService* Service, SZLEXT_AttributeReportOnDemandParams_t* Params)
{
   SZL_RESULT_t result = Szl_IsRequestValid( Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED );
   if (result != SZL_RESULT_SUCCESS)
      return result;

   if (Params == NULL)
        return SZL_RESULT_INVALID_DATA;

    return SZLEXT_AttributeReportOnDemandExt(Service, &Params->DestAddr, Params->SourceEndpoint, Params->ManufacturerSpecific, Params->ClusterID, Params->NumberOfAttributes, Params->Attributes);
}

/**
 * Send Attribute report on demand Extended
 */
static
SZL_RESULT_t SZLEXT_AttributeReportOnDemandExt(zabService* Service,
                                               SZL_Addresses_t* DestAddr,
                                               SZL_EP_t Endpoint,
                                               szl_bool ManufacturerSpecific,
                                               szl_uint16 ClusterID,
                                               szl_uint8 NumberOfAttributes,
                                               szl_uint16 Attributes[])

{
    SZL_RESULT_t result = Szl_IsRequestValid( Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED );
    if (result != SZL_RESULT_SUCCESS)
       return result;

    if (DestAddr == NULL || Attributes == NULL || NumberOfAttributes == 0)
    {
        return SZL_RESULT_INVALID_DATA;
    }

    if ((DestAddr->AddressMode != SZL_ADDRESS_MODE_VIA_BIND) && (DestAddr->AddressMode != SZL_ADDRESS_MODE_NWK_ADDRESS))
    {
        //for now the SZL only supports for SZL_ADDRESS_MODE_VIA_BIND and SZL_ADDRESS_MODE_NWK_ADDRESS
        return SZL_RESULT_INVALID_ADDRESS_MODE;
    }

    if (Endpoint == SZL_ADDRESS_EP_ALL)
    {
        // we must have a specific endpoint to report - choose the first on in the init table
        //Endpoint = gLibData.Application.Endpoints[0];
      return SZL_RESULT_INVALID_DATA;
    }

    result = SendReportOnDemand(Service, DestAddr, Endpoint, ManufacturerSpecific, ClusterID, NumberOfAttributes, Attributes);

    return result;
}

/**
 * Send report on demand
 * All attributes has to be in same cluster, endpoint and MS
 */
static SZL_RESULT_t SendReportOnDemand(zabService* Service,
                                       SZL_Addresses_t* DestAddr,
                                       SZL_EP_t Endpoint,
                                       szl_bool ManufacturerSpecific,
                                       szl_uint16 ClusterID,
                                       szl_uint8 NumberOfAttributes,
                                       szl_uint16 Attributes[])
{
    SZL_RESULT_t result = SZL_RESULT_SUCCESS;
    szl_uint8 i;
    SZL_DataPoint_t* dp;
    szl_uint8 length;
    szl_uint8 payload_size = 0;
    szl_uint8 zclCmd[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
    SZL_STATUS_t status;
    ZCL_REPORT_ATTRIBUTE_t* report;

    // fill the ZCL header
    ZCL_PAYLOAD_t* zcl = (ZCL_PAYLOAD_t*)zclCmd;
    zcl->frame_type.frame_control.frame_type = ZCL_FRAME_TYPE_PROFILE_COMMAND;
    zcl->frame_type.frame_control.direction = ZCL_FRAME_DIR_SERVER_CLIENT;
    zcl->frame_type.frame_control.disable_default_rsp = ZCL_FRAME_DEFAULT_RESPONSE_ON_ERROR;
    zcl->frame_type.frame_control.manufacture_specific = ManufacturerSpecific;
    if (zcl->frame_type.frame_control.manufacture_specific)
    {
        zcl->frame_type.manufacture_frame.manufacture_code = UINT16_TO_LE(ZB_SCHNEIDER_MANUFACTURE_ID);
    }

    // fill common header part
    ZCL_COMMON_PART_t* common = ZCL_GET_COMMON_PART_PTR(zcl);
    common->cmd = ZCL_CMD_ID_REPORT_ATTRIBUTES;

    // add all the datapoints to the report (or reports if not fit within one)
    for (i=0; i < NumberOfAttributes; i++)
    {
        dp = Szl_DataPointFind(Service, Endpoint, ManufacturerSpecific, ClusterID, Attributes[i], szl_true);

        // validate the datapoint
        if (dp == NULL)
        {
            result = SZL_RESULT_DP_NOT_FOUND; // report error back after sending
            continue;
        }

        // add to same report or start a new?

        if (ZCL_PAYLOAD_SIZE(ManufacturerSpecific, payload_size + ZCL_REPORT_ATTRIBUTE_SIZE(dp->DataSize)) > SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH)
        {
            // send report
            common->tr_id = szl_zab_GetTransactionId(Service);
            result = ZclM_SendCommand(Service,
                                      Endpoint,
                                      DestAddr,
                                      ClusterID,
                                      common->tr_id,
                                      ZCL_PAYLOAD_SIZE(ManufacturerSpecific, payload_size),
                                      zclCmd);
            if (result != SZL_RESULT_SUCCESS)
              {
                return result;
              }
            payload_size = 0; // start new report
        }

        // fill the attribute to report
        //report = (ZCL_REPORT_ATTRIBUTE_t*) (&common->payload + payload_size);
        report = (ZCL_REPORT_ATTRIBUTE_t*) (common->payload + payload_size);
        report->attribute_id = UINT16_TO_LE(dp->AttributeID);
				length = SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH - (payload_size + ZCL_REPORT_ATTRIBUTE_SIZE(0));
        status = Szl_DataPointReadZB(Service,
                                      dp->Endpoint,
                                      dp->Property.ManufacturerSpecific,
                                      dp->ClusterID,
                                      dp->AttributeID,
                                      &report->data_type,
                                      (void*)report->data,
                                      &length );

        // check status of read
        if (status != SZL_STATUS_SUCCESS)
        {
            return SZL_RESULT_OPERATION_NOT_POSSIBLE;
        }

        // update payload and check for overwrite
        payload_size += ZCL_REPORT_ATTRIBUTE_SIZE(length);
    }

    if (payload_size)
    {
      // send report
      common->tr_id = szl_zab_GetTransactionId(Service);
      result = ZclM_SendCommand(Service,
                                Endpoint,
                                DestAddr,
                                ClusterID,
                                common->tr_id,
                                ZCL_PAYLOAD_SIZE(ManufacturerSpecific, payload_size),
                                zclCmd);
    }

    return result; // report error back if any
}


/**
 * Multi Cluster Attribute Read Request
 *
 * This function is used to read one or more attributes from a specific cluster
 */
SZL_RESULT_t SZLEXT_MultiCluster_AttributeReadReq(zabService* Service,
                                                  SZLEXT_CB_MultiCluster_AttributeReadResp_t Callback,
                                                  SZLEXT_MultiCluster_AttributeReadReqParams_t* Params,
                                                  szl_uint8* TransactionId)
{
    int i;
    int j;
    szl_bool isFirst;
    szl_uint8 tid;
    szl_uint8 uniqueClusterCount;
    szl_uint16 uniqueClusterList[SZL_EXT_MULTI_CLUSTER_MAX];
    SZLEXT_MultiCluster_AttributeReadRespParams_t* errorRspParams;
    szl_uint8 zclBuffer[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
    SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

    if (SZL_RESULT_SUCCESS == result)
    {
        szl_uint16 totalZclLength;
        ZCL_PAYLOAD_t* zcl;

        if (Params->NumberOfAttributes == 0)
        {
            return SZL_RESULT_INVALID_DATA;
        }

        tid = szl_zab_GetTransactionId(Service);
        if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
          {
            return SZL_RESULT_NO_FREE_TID;
          }
        if (TransactionId != NULL)
          {
            *TransactionId = tid;
          }

        /* Count the number of different clusters in the command.
         * The first cluster must be the first instance of it.
         * For later clusters check if they already exist in the unique list, if not add to unique list */
        uniqueClusterCount = 1;
        uniqueClusterList[0] = Params->AttributeIDs[0].ClusterID;
        for (i = 1; i < Params->NumberOfAttributes; i++)
          {
            isFirst = szl_true;
            for (j = 0; j < uniqueClusterCount; j++)
              {
                if (uniqueClusterList[uniqueClusterCount] == Params->AttributeIDs[i].ClusterID)
                  {
                    isFirst = szl_false;
                    break;
                  }
              }
            if (isFirst == szl_true)
              {
                if (uniqueClusterCount < SZL_EXT_MULTI_CLUSTER_MAX)
                  {
                    uniqueClusterList[uniqueClusterCount] = Params->AttributeIDs[i].ClusterID;
                    uniqueClusterCount++;
                  }
                else
                  {
                    return SZL_RESULT_OUT_OF_RANGE;
                  }
              }
          }

        if (Params->DestAddrMode.AddressMode == SZL_ADDRESS_MODE_GP_SOURCE_ID)
          {

            /* Length = GpCmdId(1) + Options(1) + ManId(0/2) + (NumClusters * (Cluster (2) + NumBytes(1))) + Attributes(2n) */
            if (Params->ManufacturerSpecific == szl_true)
              {
                totalZclLength = 4;
              }
            else
              {
                totalZclLength = 2;
              }
            totalZclLength += (uniqueClusterCount * 3) + ((sizeof(ZCL_READ_ATTRIBUTE_ID_t) * Params->NumberOfAttributes));
          }
        else
          {
            // Only GP support this command!
            return SZL_RESULT_INVALID_ADDRESS_MODE;
          }
        if ( totalZclLength >= SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH)
          {
            return SZL_RESULT_DATA_TOO_BIG;
          }


        /* Create the response that will be used in case of no response from the destination */
        errorRspParams = (SZLEXT_MultiCluster_AttributeReadRespParams_t*)szl_mem_alloc(SZLEXT_MultiCluster_AttributeReadRespParams_t_SIZE(Params->NumberOfAttributes),
                                                                                       MALLOC_ID_SZL_MULTI_CLUSTER_READ_ATTR_FAKE_RSP);
        if (errorRspParams == NULL)
          {
            return SZL_RESULT_INTERNAL_LIB_ERROR;
          }

        szl_zab_GetResponseAddrFromRequestDestination(&Params->DestAddrMode, &errorRspParams->SourceAddress);
        errorRspParams->DestinationEndpoint = Params->SourceEndpoint;
        errorRspParams->ManufacturerSpecific = Params->ManufacturerSpecific;
        errorRspParams->NumberOfAttributes = Params->NumberOfAttributes;
        for(i=0; i < Params->NumberOfAttributes; i++)
          {
            errorRspParams->Attributes[i].ClusterID = Params->AttributeIDs[i].ClusterID;
            errorRspParams->Attributes[i].AttributeID = Params->AttributeIDs[i].AttributeID;
            errorRspParams->Attributes[i].Status = SZL_STATUS_NO_RESPONSE;
            errorRspParams->Attributes[i].DataType = SZL_ZB_DATATYPE_NO_DATA;
            errorRspParams->Attributes[i].DataLength = 0;
            errorRspParams->Attributes[i].Data = NULL;
          }

        // fill the ZCL header
        zcl = (ZCL_PAYLOAD_t*)zclBuffer;

        unsigned8 index;

        zcl->frame_type.zgp_frame.cmd = 0xF2;
        if (Params->ManufacturerSpecific)
          {
            zcl->frame_type.zgp_frame.payload[0] = 0x02;
            COPY_OUT_16_BITS(&zcl->frame_type.zgp_frame.payload[1], ZB_SCHNEIDER_MANUFACTURE_ID);
            index = 3;
          }
        else
          {
            zcl->frame_type.zgp_frame.payload[0] = 0x00;
            index = 1;
          }
        /* If multi cluster, then set the bit */
        if (uniqueClusterCount > 1)
          {
            zcl->frame_type.zgp_frame.payload[0] |= 0x01;
          }

        for (i = 0; i < uniqueClusterCount; i++)
          {
            szl_uint8 clusterLengthFieldIndex;
            COPY_OUT_16_BITS(&zcl->frame_type.zgp_frame.payload[index], uniqueClusterList[i]); index += 2;
            clusterLengthFieldIndex = index; index++;

            for (j = 0; j < Params->NumberOfAttributes; j++)
              {
                if (Params->AttributeIDs[j].ClusterID == uniqueClusterList[i])
                  {
                    COPY_OUT_16_BITS(&zcl->frame_type.zgp_frame.payload[index], Params->AttributeIDs[j].AttributeID); index += 2;
                  }
              }
            zcl->frame_type.zgp_frame.payload[clusterLengthFieldIndex] = (index - clusterLengthFieldIndex)-1;
          }

        /* Send command. If it sent ok, then add the transaction. If both ok, return success */
        result = ZclM_SendCommand(Service,
                                  Params->SourceEndpoint,
                                  &Params->DestAddrMode,
                                  0xFFFF,
                                  tid,
                                  totalZclLength,
                                  zclBuffer);
        if (result == SZL_RESULT_SUCCESS)
          {
            syncCallback syncCB;
            syncCB.SZLEXT_CB_MultiCluster_AttributeReadResp = Callback;

            result = SzlZab_AddGreenPowerSynchronousTransaction(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MULTI_CLUSTER_READ_ATTRIBUTE_REQ,
                                                                tid,
                                                                Params->DestAddrMode.Addresses.GpSourceId.SourceId,
                                                                &syncCB,
                                                                errorRspParams);

          }

        if (result != SZL_RESULT_SUCCESS)
          {
            szl_mem_free(errorRspParams);
          }
    }

    return result;
}


/**
 * Multi Cluster Attribute Read Request
 *
 * This function is used to read one or more attributes from a specific cluster
 */
SZL_RESULT_t SZLEXT_MultiCluster_AttributeWriteReq(zabService* Service,
                                                   SZLEXT_CB_MultiCluster_AttributeWriteResp_t Callback,
                                                   SZLEXT_MultiCluster_AttributeWriteReqParams_t* Params,
                                                   szl_uint8* TransactionId)
{
    szl_uint8 tid;
    szl_bool isFirst;
    szl_uint8 uniqueClusterCount;
    szl_uint16 uniqueClusterList[SZL_EXT_MULTI_CLUSTER_MAX];
    int i;
    int j;
    SZLEXT_MultiCluster_AttributeWriteRespParams_t* errorRspParams;
    szl_uint8 zclBuffer[SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH];
    SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED );

    if (SZL_RESULT_SUCCESS == result)
    {
        szl_uint16 data_size = 0;
        szl_uint16 totalZclLength;
        ZCL_PAYLOAD_t* zcl;
        ZCL_WRITE_ATTRIBUTE_t* attr;
        szl_bool greenPowerCmd = szl_false;
        unsigned8 index;

        if (Params->NumberOfAttributes == 0)
        {
            return SZL_RESULT_INVALID_DATA;
        }

        tid = szl_zab_GetTransactionId(Service);
        if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
          {
            return SZL_RESULT_NO_FREE_TID;
          }
        if (TransactionId != NULL)
          {
            *TransactionId = tid;
          }


        /* Count the number of different clusters in the command.
         * The first cluster must be the first instance of it.
         * For later clusters check if they already exist in the unique list, if not add to unique list */
        uniqueClusterCount = 1;
        uniqueClusterList[0] = Params->Attributes[0].ClusterID;
        for (i = 1; i < Params->NumberOfAttributes; i++)
          {
            isFirst = szl_true;
            for (j = 0; j < uniqueClusterCount; j++)
              {
                if (uniqueClusterList[uniqueClusterCount] == Params->Attributes[i].ClusterID)
                  {
                    isFirst = szl_false;
                    break;
                  }
              }
            if (isFirst == szl_true)
              {
                if (uniqueClusterCount < SZL_EXT_MULTI_CLUSTER_MAX)
                  {
                    uniqueClusterList[uniqueClusterCount] = Params->Attributes[i].ClusterID;
                    uniqueClusterCount++;
                  }
                else
                  {
                    return SZL_RESULT_OUT_OF_RANGE;
                  }
              }
          }

        /* Calculate size of Write Attribute items */
        data_size = uniqueClusterCount*3; // Start with cluster block header: id + length
        for(i=0; i < Params->NumberOfAttributes; i++)
        {
            szl_uint8 converted_data_size = ZbDataSize(Params->Attributes[i].DataType, &Params->Attributes[i].DataLength);

            if (converted_data_size == 0)
            {
                return SZL_RESULT_INVALID_DATA;
            }

            // get the size for the converted native to zigbee type
            data_size += ZCL_WRITE_ATTRIBUTE_SIZE(converted_data_size);
        }

        /* Add GP or ZCL header */
        if (Params->DestAddrMode.AddressMode == SZL_ADDRESS_MODE_GP_SOURCE_ID)
          {
            greenPowerCmd = szl_true;

            /* Length = GpCmdId(1) + Options(1) + ManId(0/2) + ClusterAttributesRecords(data_size) */
            if (Params->ManufacturerSpecific == szl_true)
              {
                totalZclLength = 4 + data_size;
              }
            else
              {
                totalZclLength = 2 + data_size;
              }
          }
        else
          {
            // Only GP support this command!
            return SZL_RESULT_INVALID_ADDRESS_MODE;
          }
        if ( totalZclLength >= SZL_ZCL_M_MAX_ZCL_COMMAND_LENGTH)
          {
            return SZL_RESULT_DATA_TOO_BIG;
          }

        /* Create the response that will be used in case of no response from the destination */
        errorRspParams = (SZLEXT_MultiCluster_AttributeWriteRespParams_t*)szl_mem_alloc(SZLEXT_MultiCluster_AttributeWriteRespParams_t_SIZE(Params->NumberOfAttributes),
                                                                                        MALLOC_ID_SZL_MULTI_CLUSTER_WRITE_ATTR_FAKE_RSP);
        if (errorRspParams == NULL)
          {
            return SZL_RESULT_INTERNAL_LIB_ERROR;
          }
        szl_zab_GetResponseAddrFromRequestDestination(&Params->DestAddrMode, &errorRspParams->SourceAddress);
        errorRspParams->DestinationEndpoint = Params->SourceEndpoint;
        errorRspParams->ManufacturerSpecific = Params->ManufacturerSpecific;
        errorRspParams->NumberOfAttributes = Params->NumberOfAttributes;
        for(i=0; i < Params->NumberOfAttributes; i++)
          {
            errorRspParams->Attributes[i].ClusterID = Params->Attributes[i].ClusterID;
            errorRspParams->Attributes[i].AttributeID = Params->Attributes[i].AttributeID;
            errorRspParams->Attributes[i].Status = SZL_STATUS_NO_RESPONSE;
          }

        // fill the ZCL header
        zcl = (ZCL_PAYLOAD_t*)zclBuffer;

        if (greenPowerCmd == szl_true)
          {

            zcl->frame_type.zgp_frame.cmd = 0xF1;
            if (Params->ManufacturerSpecific)
              {
                zcl->frame_type.zgp_frame.payload[0] = 0x02;
                COPY_OUT_16_BITS(&zcl->frame_type.zgp_frame.payload[1], ZB_SCHNEIDER_MANUFACTURE_ID);
                index = 3;
              }
            else
              {
                zcl->frame_type.zgp_frame.payload[0] = 0x00;
                index = 1;
              }

            /* If multi cluster, then set the bit */
            if (uniqueClusterCount > 1)
              {
                zcl->frame_type.zgp_frame.payload[0] |= 0x01;
              }


          }
        else
          {
            return SZL_RESULT_INVALID_ADDRESS_MODE;
          }

        for (i = 0; i < uniqueClusterCount; i++)
          {
            szl_uint8 clusterLengthFieldIndex;
            COPY_OUT_16_BITS(&zcl->frame_type.zgp_frame.payload[index], uniqueClusterList[i]); index += 2;
            clusterLengthFieldIndex = index; index++;
            attr = (ZCL_WRITE_ATTRIBUTE_t*)&zcl->frame_type.zgp_frame.payload[index];
            // fill in the attributes
            for(j=0; j < Params->NumberOfAttributes; j++)
            {
              if (Params->Attributes[j].ClusterID == uniqueClusterList[i])
                {
                  szl_uint8 zb_data_len = 0;
                  attr->attribute_id = UINT16_TO_LE(Params->Attributes[j].AttributeID);
                  attr->data_type = Params->Attributes[j].DataType;

                  switch (attr->data_type)
                  {
                      case SZL_ZB_DATATYPE_CHAR_STR:
                      case SZL_ZB_DATATYPE_OCTET_STR:
                          zb_data_len = Params->Attributes[j].DataLength + 1;
                          attr->data[0] = Params->Attributes[j].DataLength; // the length field is in index[0]
                          szl_memcpy((szl_uint8*)attr->data + 1,Params->Attributes[j].Data, Params->Attributes[j].DataLength);// the real data is in index[1]
                          break;

                      default:
                          if (!DataTypeIsPrimitive( attr->data_type ) )
                          {
                              //SNP_DATA_BUF_FREE(payload);
                              return SZL_RESULT_INVALID_DATA;
                          }
                          // we converts the native stream into LE type
                          zb_data_len = NativeToZb(Params->Attributes[j].Data, Params->Attributes[j].DataLength, Params->Attributes[j].DataType, attr->data);
                          break;
                  }

                  attr = (ZCL_WRITE_ATTRIBUTE_t*)((szl_uint8*)attr + ZCL_WRITE_ATTRIBUTE_SIZE(zb_data_len) );
                }
            }
            zcl->frame_type.zgp_frame.payload[clusterLengthFieldIndex] = (szl_uint8*)attr - &zcl->frame_type.zgp_frame.payload[index];
            index += zcl->frame_type.zgp_frame.payload[clusterLengthFieldIndex];
          }

        /* Send command. If it sent ok, then add the transaction. If both ok, return success */
        result = ZclM_SendCommand(Service,
                                  Params->SourceEndpoint,
                                  &Params->DestAddrMode,
                                  0xFFFF,
                                  tid,
                                  totalZclLength,
                                  zclBuffer);
        if (result == SZL_RESULT_SUCCESS)
          {
            syncCallback syncCB;
            syncCB.SZLEXT_CB_MultiCluster_AttributeWriteResp = Callback;
            result = SzlZab_AddGreenPowerSynchronousTransaction(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MULTI_CLUSTER_WRITE_ATTRIBUTE_REQ,
                                                                tid,
                                                                Params->DestAddrMode.Addresses.GpSourceId.SourceId,
                                                                &syncCB,
                                                                errorRspParams);
          }

        if (result != SZL_RESULT_SUCCESS)
          {
            szl_mem_free(errorRspParams);
          }
    }

    return result;
}


/**
 * Return the true if data type is analog and false if discrete
 */
szl_bool SZLEXT_ZigBeeDataTypeIsAnalog(SZL_ZIGBEE_DATA_TYPE_t DataType)
{
    szl_bool isAnalog = szl_false;

    if ( (DataType >= 0x20 && DataType <= 0x2f) || // uint(8,16,24,32,40,48,56,64) and int(8,16,24,32,40,48,56,64)
         (DataType >= 0x38 && DataType <= 0x3f) || // SEMI_PREC,SINGLE_PREC,DOUBLE_PREC
         (DataType >= 0xe0 && DataType <= 0xe7) )  // TOD, DATE, UTC
      {
        isAnalog = szl_true;
      }
    
    return isAnalog;
}


