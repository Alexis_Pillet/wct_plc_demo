/*******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the core of the ZigBee Primitive Interface
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.03.01  19-Mar-14   MvdB   artf53864: Support mgmt leave request/response
 * 00.00.04.00  19-May-14   MvdB   Fix issue with double mutex locking in checkSynchronousTransactionTimeout()
 * 00.00.06.00  12-Jun-14   MvdB   Add Wireless Test Bench functions
 * 00.00.06.03  01-Oct-14   MvdB   artf104879: Support energy scans via Mgmt Nwk Update Request
 * 00.00.06_GP  15-Oct-14   MvdB   ARTF105853: Complete basic GP features for Smartlink IPZ
 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 * 00.00.06.06  21-Oct-14   MvdB   artf106135: Handle network leave indications
 * 01.00.00.02  29-Jan-15   MvdB   Support GPD De-commissioning
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113134: SZL Callbacks not called if error parsing response
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
 * 002.000.003  06-Mar-15   MvdB   Remove GpAck. Code is no longer used.
 * 002.001.001  15-Jul-15   MvdB   ARTF130228: Add unique address mode for GpSrcId
 * 002.001.004  21-Jul-15   MvdB   ARTF134686: Only accept frames in szl_zab_DataInHandler() if lib is initialised
 * 002.002.004  03-Sep-15   MvdB   ARTF147424: Upgrade Szl_GetDeviceID() macro to include endpoint parameter
 *                                 ARTF148423: Upgrade szl_zab_GetTransactionId() to check TID is not in use already
 * 002.002.009  06-Oct-15   MvdB   ARTF150769: Tweak processDataIn() to request fast work after any command
 * 002.002.010  08-Oct-15   MvdB   Improve freeing in szl_zab_InitSynchronousTransactionTable to avoid linux errors
 *                                 ARTF150980: Support Service ID in memory allocation functions for WTB
 * 002.002.013  14-Oct-15   MvdB   ARTF151072: Add service pointer to osTimeGetMilliseconds() and osTimeGetSeconds()
 * 002.002.021  21-Apr-16   MvdB   ARTF167807: Support Multi-Cluster Attribute Read/Write for GP
 *                                 ARTF167808: Improve Attribute Read/Write/Configure Responses to always list all attributes requested
 * 002.002.031  09-Jan-17   MvdB   ARTF152098: SZL: Improve update of simple descriptors when attributes/commands are registered/deregistered
 *                                 ARTF172881: Avoid work() returning zero due to errors like szl not initialised
 * 002.002.042  20-Jan-17   MvdB   ARTF199306: Release failed synchronous transactions before calling callback
 * 002.002.054  20-Jul-17   SMon   ARTF214292 Manage timeout for default response
 ******************************************************************************/

/*******************************************************************************
 * INCLUDES
 ******************************************************************************/
#include "zabCorePrivate.h"
#include "zabMutexConfigure.h"
#include "szl_timer.h"
#include "szl_timer_types.h"
#include "cb_handler.h"
#include "szl_zab.h"
//#include "szl_gp.h"
#include "szl.h"


/*******************************************************************************
 * TYPEDEFS
 ******************************************************************************/
//#define LIB_DATA( S ) ZAB_SRV( S )->szlService


/*******************************************************************************
 * LOCAL VARIABLES
 ******************************************************************************/


/******************************************************************************
 *                      ******************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Clean up (free) and Error Response Parameters pointer
 ******************************************************************************/
static void cleanupErrorResponseParameters(zabService* Service, szlZabSyncTransType_t syncTransType, void* errorResponseParams)
{
  int i;

  if (errorResponseParams != NULL)
    {
      /* Some Green Power responses may be spread across multiple commands.
       * Those that may have allocated some data but still timeout must be cleaned up here! */
      switch(syncTransType)
        {
          case SZL_ZAB_SYNC_TRANS_TYPE_ZCL_READ_ATTRIBUTE_REQ:
            for (i = 0; i < ((SZL_AttributeReadRespParams_t*)errorResponseParams)->NumberOfAttributes; i++)
              {
                if (((SZL_AttributeReadRespParams_t*)errorResponseParams)->Attributes[i].Data != NULL)
                  {
                    szl_mem_free(((SZL_AttributeReadRespParams_t*)errorResponseParams)->Attributes[i].Data);
                  }
              }
            break;

          case SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MULTI_CLUSTER_READ_ATTRIBUTE_REQ:
            for (i = 0; i < ((SZLEXT_MultiCluster_AttributeReadRespParams_t*)errorResponseParams)->NumberOfAttributes; i++)
              {
                if (((SZLEXT_MultiCluster_AttributeReadRespParams_t*)errorResponseParams)->Attributes[i].Data != NULL)
                  {
                    szl_mem_free(((SZLEXT_MultiCluster_AttributeReadRespParams_t*)errorResponseParams)->Attributes[i].Data);
                  }
              }
            break;

          default:
            break;
        }

      szl_mem_free(errorResponseParams);
    }
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise the low layers of the SZL Service
 * This should be done on ZAB create and befoer app calls SZL_Initialize()
 ******************************************************************************/
void szl_zab_Init(erStatus* Status, zabService* Service)
{
  szlZabServiceType* szl;
  szl_uint8 MutexId;

  ER_CHECK_STATUS(Status);
  ER_CHECK_NULL(Status, Service);
  szl = &ZAB_SRV(Service)->szlService;

  MutexId = ZAB_PROTECT_CREATE(Service);
  qSafeInitialize(Service, &szl->szlDataInQ, MutexId);

  // Allocate a mutex for Synchronous Transactions
  szl->szlServiceDataMutex = ZAB_PROTECT_CREATE(Service);

  // Register for data sap messages
  sapCoreRegisterCallBack(Status, ZAB_SRV(Service)->DataSAP, SAP_MSG_DIRECTION_IN, szl_zab_DataInHandler);

}

/******************************************************************************
 * Reset the SZL Service
 ******************************************************************************/
void szl_zab_Reset(erStatus* Status, zabService* Service)
{
  if (SZL_Destroy(Service) != SZL_RESULT_SUCCESS)
    {
      erStatusSet(Status, SZL_ERROR_PLUGIN_NOT_UNREGISTERED);
    }
}

/******************************************************************************
 * Destroy the SZL Service
 ******************************************************************************/
void szl_zab_Destroy(erStatus* Status, zabService* Service)
{
  szlZabServiceType* szl;

  ER_CHECK_NULL(Status, Service);
  szl = &ZAB_SRV(Service)->szlService;

  // Perform reset of SZL
  szl_zab_Reset(Status, Service);
  ER_CHECK_STATUS(Status);

  // Release mutexes created for SZL
  ZAB_PROTECT_RELEASE(Service, qSafeId(&szl->szlDataInQ));
  ZAB_PROTECT_RELEASE(Service, szl->szlServiceDataMutex);
  (void)szl; // To avoid warnings when mutex macro is null
}

/******************************************************************************
 * Get the Service Id from the Service Pointer
 ******************************************************************************/
szl_uint8 SzlZab_GetServiceId(zabService* Service)
{
  erStatus LocalStatus;
  erStatusClear(&LocalStatus, Service);

  return srvCoreGetServiceId(&LocalStatus, Service );
}

/******************************************************************************
 * Get a Transaction ID
 * Returns SZL_ZAB_M_TRANSACTION_ID_INVALID if none available.
 ******************************************************************************/
szl_uint8 szl_zab_GetTransactionId(zabService* Service)
{
  szl_uint8 index;
  szl_uint8 tid;
  szl_bool tidUsed = szl_false;
  szlZabServiceType* szl;

  szl = &ZAB_SRV(Service)->szlService;

  /* Lock the mutex while we get the TID */
  SZL_LOCK(Service, szl->szlServiceDataMutex);

  tid = szl->LastTransactionId;

  // Loop until we get a good, unused TID
  do
    {
      /* Increment last TID to get the new one, but don't accept SZL_ZAB_M_TRANSACTION_ID_INVALID */
      do
        {
          szl->LastTransactionId++;

          /* If we got back to tid then we have tried all possible TIDs and none were free, so return */
          if (szl->LastTransactionId == tid)
            {
              SZL_UNLOCK(Service, szl->szlServiceDataMutex);
              return SZL_ZAB_M_TRANSACTION_ID_INVALID;
            }
        } while (szl->LastTransactionId == SZL_ZAB_M_TRANSACTION_ID_INVALID);


      /* Confirm TID is not already in use */
      tidUsed = szl_false;
      for (index = 0; index < SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS; index++)
        {
          if (szl->szlZabSyncTransactions[index].tid == szl->LastTransactionId)
            {
              tidUsed = szl_true;
              break;
            }
        }
    } while (tidUsed == szl_true);

  // We got a good TID to return!
  tid = szl->LastTransactionId;
  SZL_UNLOCK(Service, szl->szlServiceDataMutex);



  return tid;
}

/******************************************************************************
 * Convert a destination address of a request into the source address of a (fake) response
 ******************************************************************************/
void szl_zab_GetResponseAddrFromRequestDestination(SZL_Addresses_t* ReqDstAddr, SZL_Addresses_t* RspSrcAddr)
{
  if ( (ReqDstAddr->AddressMode == SZL_ADDRESS_MODE_NWK_ADDRESS) ||
       (ReqDstAddr->AddressMode == SZL_ADDRESS_MODE_GP_SOURCE_ID) ||
       (ReqDstAddr->AddressMode == SZL_ADDRESS_MODE_IEEE_ADDRESS) )
    {
      szl_memcpy(RspSrcAddr, ReqDstAddr, sizeof(SZL_Addresses_t));
    }
  else
    {
      RspSrcAddr->AddressMode = SZL_ADDRESS_MODE_INVALID;
    }
}

/******************************************************************************
 * Initialise the Synchronous Transaction Table
 ******************************************************************************/
void szl_zab_InitSynchronousTransactionTable(zabService* Service)
{
  szl_uint8 index;
  szlZabServiceType* szl;

  szl = &ZAB_SRV(Service)->szlService;

  SZL_LOCK(Service, szl->szlServiceDataMutex);
  for (index = 0; index < SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS; index++)
    {
      cleanupErrorResponseParameters(Service,
                                     szl->szlZabSyncTransactions[index].syncTransType,
                                     szl->szlZabSyncTransactions[index].errorResponseParams);
      szl->szlZabSyncTransactions[index].syncTransType = SZL_ZAB_SYNC_TRANS_TYPE_NONE;
    }
  SZL_UNLOCK(Service, szl->szlServiceDataMutex);
}


/******************************************************************************
 * Modify entry of the Synchronous Transaction Table
 ******************************************************************************/
SZL_RESULT_t SzlZab_ModifySynchronousTransaction(zabService* Service,
                                                szl_uint8 command,
                                                szl_uint8 timout,
                                                szl_uint8 nbRetry,
                                                szl_uint8 tid)
{
    szl_uint8 index;
    szlZabServiceType* szl;
    szl = &ZAB_SRV(Service)->szlService;

    SZL_LOCK(Service, szl->szlServiceDataMutex);
    for (index = 0; index < SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS; index++)
    {
        if ((szl->szlZabSyncTransactions[index].syncTransType == SZL_ZAB_SYNC_TRANS_TYPE_ZCL_CLUSTER_REQ) &&(szl->szlZabSyncTransactions[index].tid == tid))
        {
            szl->szlZabSyncTransactions[index].timeout += (timout-SZL_ZAB_M_SYNCHRONOUS_TRANSACTION_TIMEOUT_S);
            ((SZL_ClusterCmdRespParams_t*) szl->szlZabSyncTransactions[index].errorResponseParams)->Command = command;
            ((SZL_ClusterCmdRespParams_t*) szl->szlZabSyncTransactions[index].errorResponseParams)->PayloadLength = 2;
            ((SZL_ClusterCmdRespParams_t*) szl->szlZabSyncTransactions[index].errorResponseParams)->Payload[0] = nbRetry;
            ((SZL_ClusterCmdRespParams_t*) szl->szlZabSyncTransactions[index].errorResponseParams)->Payload[1] = timout;
        }
    }
    SZL_UNLOCK(Service, szl->szlServiceDataMutex);    
}

/******************************************************************************
 * Add an entry to the Synchronous Transaction Table
 ******************************************************************************/
SZL_RESULT_t SzlZab_AddSynchronousTransaction(zabService* Service,
                                              szlZabSyncTransType_t syncTransType,
                                              szl_uint8 tid,
                                              syncCallback* callback,
                                              void* errorResponseParams)
{
    return SzlZab_AddSynchronousTransactionTimeout(Service,
                                              syncTransType,
                                              tid,
                                              callback,
                                              errorResponseParams,
                                              SZL_ZAB_M_SYNCHRONOUS_TRANSACTION_TIMEOUT_S);
}
/******************************************************************************
 * Add an entry to the Synchronous Transaction Table
 ******************************************************************************/
SZL_RESULT_t SzlZab_AddSynchronousTransactionTimeout(zabService* Service,
                                              szlZabSyncTransType_t syncTransType,
                                              szl_uint8 tid,
                                              syncCallback* callback,
                                              void* errorResponseParams,
                                              szl_uint8 timeout)
{
  szl_uint8 index;
  szlZabServiceType* szl;

  if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
    {
      return SZL_RESULT_INVALID_DATA;
    }

  if (callback != NULL)
    {
      szl = &ZAB_SRV(Service)->szlService;

      SZL_LOCK(Service, szl->szlServiceDataMutex);
      for (index = 0; index < SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS; index++)
        {
          if (szl->szlZabSyncTransactions[index].syncTransType == SZL_ZAB_SYNC_TRANS_TYPE_NONE)
            {
              szl->szlZabSyncTransactions[index].syncTransType = syncTransType;
              szl->szlZabSyncTransactions[index].tid = tid;
              szl->szlZabSyncTransactions[index].gpSrcId = SZL_ZAB_M_SRC_ID_INVALID;
              osTimeGetSeconds(Service, &(szl->szlZabSyncTransactions[index].timeout));
              szl->szlZabSyncTransactions[index].timeout += timeout;
              szl->szlZabSyncTransactions[index].callback = *callback;
              szl->szlZabSyncTransactions[index].errorResponseParams = errorResponseParams;
              SZL_UNLOCK(Service, szl->szlServiceDataMutex);
              return SZL_RESULT_SUCCESS;
            }
        }
      SZL_UNLOCK(Service, szl->szlServiceDataMutex);
      return SZL_RESULT_CB_QUEUE_FULL;
    }
  return SZL_RESULT_MANDATORY_CB_MISSING;
}

/******************************************************************************
 * Add a Green Power entry to the Synchronous Transaction Table by IEEE
 ******************************************************************************/
SZL_RESULT_t SzlZab_AddGreenPowerSynchronousTransaction(zabService* Service,
                                                        szlZabSyncTransType_t syncTransType,
                                                        szl_uint8 tid,
                                                        szl_uint32 srcId,
                                                        syncCallback* callback,
                                                        void* errorResponseParams)
{
  szl_uint8 index;
  szlZabServiceType* szl;

  if (srcId == SZL_ZAB_M_SRC_ID_INVALID)
    {
      return SZL_RESULT_INVALID_DATA;
    }

  SzlZab_GenerateSynchronousTransactionFailureBySrcId(Service, srcId, SZL_STATUS_ZGP_TXQUEUE_OVERWRITEN);

  if (callback != NULL)
    {
      szl = &ZAB_SRV(Service)->szlService;

      SZL_LOCK(Service, szl->szlServiceDataMutex);
      for (index = 0; index < SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS; index++)
        {
          if (szl->szlZabSyncTransactions[index].syncTransType == SZL_ZAB_SYNC_TRANS_TYPE_NONE)
            {
              szl->szlZabSyncTransactions[index].syncTransType = syncTransType;
              szl->szlZabSyncTransactions[index].tid = tid;
              szl->szlZabSyncTransactions[index].gpSrcId = srcId;
              osTimeGetSeconds(Service, &(szl->szlZabSyncTransactions[index].timeout));
              szl->szlZabSyncTransactions[index].timeout += SZL_ZAB_M_GP_SYNCHRONOUS_TRANSACTION_TIMEOUT_S;
              szl->szlZabSyncTransactions[index].callback = *callback;
              szl->szlZabSyncTransactions[index].errorResponseParams = errorResponseParams;
              SZL_UNLOCK(Service, szl->szlServiceDataMutex);
              return SZL_RESULT_SUCCESS;
            }
        }
      SZL_UNLOCK(Service, szl->szlServiceDataMutex);
      return SZL_RESULT_CB_QUEUE_FULL;
    }
  return SZL_RESULT_MANDATORY_CB_MISSING;
}


/******************************************************************************
 * Update the timeout of a Green Power  Synchronous Transaction
 ******************************************************************************/
SZL_RESULT_t SzlZab_UpdateGreenPowerSynchronousTransactiontimeout(zabService* Service,
                                                                  szlZabSyncTransType_t syncTransType,
                                                                  szl_uint32 srcId,
                                                                  szl_uint32 timeoutSeconds)
{
  szl_uint8 index;
  szlZabServiceType* szl;

  if (srcId == SZL_ZAB_M_SRC_ID_INVALID)
    {
      return SZL_RESULT_INVALID_DATA;
    }
  szl = &ZAB_SRV(Service)->szlService;

  SZL_LOCK(Service, szl->szlServiceDataMutex);
  for (index = 0; index < SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS; index++)
    {
      if ( (szl->szlZabSyncTransactions[index].syncTransType == syncTransType) &&
           (szl->szlZabSyncTransactions[index].gpSrcId == srcId) )
        {
          osTimeGetSeconds(Service, &(szl->szlZabSyncTransactions[index].timeout));
          szl->szlZabSyncTransactions[index].timeout += timeoutSeconds;
          SZL_UNLOCK(Service, szl->szlServiceDataMutex);
          return SZL_RESULT_SUCCESS;
        }
    }
  SZL_UNLOCK(Service, szl->szlServiceDataMutex);
  return SZL_RESULT_NOT_FOUND;
}

/******************************************************************************
 * Search the Synchronous Transaction Table for a matching entry by TID
 * Callback will be null if not found.
 ******************************************************************************/
syncCallback SzlZab_GetSynchronousTransactionCallbackAndDestroy(zabService* Service,
                                                                szlZabSyncTransType_t syncTransType,
                                                                szl_uint8 tid)
{
  syncCallback callback;
  void* errorResponseParams;

  callback = SzlZab_GetSynchronousTransactionCallbackAndErrRspThenDestroy(Service,
                                                                          syncTransType,
                                                                          tid,
                                                                          &errorResponseParams);
  cleanupErrorResponseParameters(Service, syncTransType, errorResponseParams);
  return callback;
}

/******************************************************************************
 * Search the Synchronous Transaction Table for a matching entry by TID
 * Callback will be null if not found.
 * Also return errorResponseParams. Caller is responsible for freeing it!
 ******************************************************************************/
syncCallback SzlZab_GetSynchronousTransactionCallbackAndErrRspThenDestroy(zabService* Service,
                                                                          szlZabSyncTransType_t syncTransType,
                                                                          szl_uint8 tid,
                                                                          void** errorResponseParams)
{
  szl_uint8 index;
  szlZabServiceType* szl;
  syncCallback callback = {NULL};
  *errorResponseParams = NULL;

  if (tid != SZL_ZAB_M_TRANSACTION_ID_INVALID)
    {
      szl = &ZAB_SRV(Service)->szlService;

      SZL_LOCK(Service, szl->szlServiceDataMutex);
      for (index = 0; index < SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS; index++)
        {
          if ( (szl->szlZabSyncTransactions[index].syncTransType == syncTransType) &&
               (szl->szlZabSyncTransactions[index].tid == tid) )
            {
              callback = szl->szlZabSyncTransactions[index].callback;
              *errorResponseParams = szl->szlZabSyncTransactions[index].errorResponseParams;
              szl_memset(&(szl->szlZabSyncTransactions[index]), 0, sizeof(szlZabSyncTransactions_t));
              SZL_UNLOCK(Service, szl->szlServiceDataMutex);
              return callback;
            }
        }
      SZL_UNLOCK(Service, szl->szlServiceDataMutex);
    }
  return callback;
}

/******************************************************************************
 * Search the Synchronous Transaction Table for a matching entry by IEEE
 * Callback will be null if not found.
 * Also return errorResponseParams. Caller is responsible for freeing it!
 ******************************************************************************/
syncCallback SzlZab_GetSynchronousTransactionCallbackAndErrRspBySrcIdOptionalDestroy(zabService* Service,
                                                                                     szlZabSyncTransType_t syncTransType,
                                                                                     szl_uint32 gpSrcId,
                                                                                     void** errorResponseParams,
                                                                                     szl_uint8* tid,
                                                                                     szl_bool destroy)
{
  szl_uint8 index;
  szlZabServiceType* szl;
  syncCallback callback = {NULL};
  *errorResponseParams = NULL;

  if (gpSrcId != SZL_ZAB_M_SRC_ID_INVALID)
    {
      szl = &ZAB_SRV(Service)->szlService;

      SZL_LOCK(Service, szl->szlServiceDataMutex);
      for (index = 0; index < SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS; index++)
        {
          if ( (szl->szlZabSyncTransactions[index].syncTransType == syncTransType) &&
               (szl->szlZabSyncTransactions[index].gpSrcId == gpSrcId) )
            {
              callback = szl->szlZabSyncTransactions[index].callback;
              *errorResponseParams = szl->szlZabSyncTransactions[index].errorResponseParams;
              *tid = szl->szlZabSyncTransactions[index].tid;
              if (destroy == szl_true)
                {
                  szl_memset(&(szl->szlZabSyncTransactions[index]), 0, sizeof(szlZabSyncTransactions_t));
                }
              SZL_UNLOCK(Service, szl->szlServiceDataMutex);
              return callback;
            }
        }
      SZL_UNLOCK(Service, szl->szlServiceDataMutex);
    }
  return callback;
}

/******************************************************************************
 * Generate timeout event for an item in the Synchronous Transaction Table
 ******************************************************************************/
static
void generateSynchronousTransactionFailure( erStatus* Status,
                                            zabService* Service,
                                            szlZabSyncTransactions_t* syncTrans,
                                            SZL_STATUS_t szlStatus )
{
  szlZabServiceType* szl;
  szlZabSyncTransactions_t localSyncTrans;

  ER_CHECK_STATUS_NULL( Status, syncTrans);
  szl = &ZAB_SRV(Service)->szlService;
  ER_CHECK_NULL(Status, szl);

  /* Take a local copy of the transaction, then clear the one in the queue.
   * This is necessary so the application  can generate a new GP transaction in the callback if it wants,
   * without being blocked by this one still being there. */
  localSyncTrans = *syncTrans;
  SZL_LOCK(Service, szl->szlServiceDataMutex);
  osMemZero (Status, (szl_uint8*)syncTrans, sizeof(szlZabSyncTransactions_t));
  SZL_UNLOCK(Service, szl->szlServiceDataMutex);

  /* Generate callback with failure parameters */
  if (localSyncTrans.callback.SZLZAB_CB_GenericResp != NULL)
    {
      localSyncTrans.callback.SZLZAB_CB_GenericResp( Service, szlStatus, localSyncTrans.errorResponseParams, localSyncTrans.tid);
    }

  /* Free up slot */
  cleanupErrorResponseParameters(Service, localSyncTrans.syncTransType, localSyncTrans.errorResponseParams);
}

/******************************************************************************
 * An error has occurred with a command (typically a bad data confirm) and the
 * sync transaction failure can be generated now.
 * This is optional, but speeds up timeouts when a failure is detected.
 ******************************************************************************/
void SzlZab_GenerateSynchronousTransactionFailureByTid(zabService* Service,
                                                       szl_uint8 tid)
{
  szl_uint8 index;
  szlZabServiceType* szl;
  erStatus Status;
  erStatusClear(&Status, Service);

  if (tid != SZL_ZAB_M_TRANSACTION_ID_INVALID)
    {
      szl = &ZAB_SRV(Service)->szlService;

      for (index = 0; index < SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS; index++)
        {
          if (szl->szlZabSyncTransactions[index].tid == tid)
            {
              generateSynchronousTransactionFailure(&Status, Service, &szl->szlZabSyncTransactions[index], SZL_STATUS_FAILED);
              return;
            }
        }
    }
}

/******************************************************************************
 * An error has occurred with a command (typically a GpTxQExpiry) and the
 * sync transaction failure can be generated now.
 * This is optional, but speeds up timeouts when a failure is detected.
 ******************************************************************************/
void SzlZab_GenerateSynchronousTransactionFailureBySrcId(zabService* Service,
                                                         szl_uint32 GpSrcId,
                                                         SZL_STATUS_t szlStatus)
{
  szl_uint8 index;
  szlZabServiceType* szl;
  erStatus Status;
  erStatusClear(&Status, Service);

  szl = &ZAB_SRV(Service)->szlService;

  for (index = 0; index < SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS; index++)
    {
      if (szl->szlZabSyncTransactions[index].gpSrcId == GpSrcId)
        {
          generateSynchronousTransactionFailure(&Status, Service, &szl->szlZabSyncTransactions[index], szlStatus);
          return;
        }
    }
}


/******************************************************************************
 * Handle default responses:
 *  - Search the Synchronous Transaction Table for a matching ZCL entry by TID
 *  - If found, call the callback with the provided status
 ******************************************************************************/
void SzlZab_DefaultResponseCallbackAndDestroy(zabService* Service,
                                              szl_uint8 tid,
                                              SZL_STATUS_t status)
{
  szl_uint8 index;
  szlZabServiceType* szl;
  syncCallback callback = {NULL};
  szlZabSyncTransType_t syncTransType;
  void* responseParams;

  szl = &ZAB_SRV(Service)->szlService;

  SZL_LOCK(Service, szl->szlServiceDataMutex);
  for (index = 0; index < SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS; index++)
    {
      if ( (szl->szlZabSyncTransactions[index].syncTransType > SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MIN) &&
           (szl->szlZabSyncTransactions[index].syncTransType < SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MAX) &&
           (szl->szlZabSyncTransactions[index].tid == tid) )
        {
          callback = szl->szlZabSyncTransactions[index].callback;
          syncTransType = szl->szlZabSyncTransactions[index].syncTransType;
          responseParams = szl->szlZabSyncTransactions[index].errorResponseParams;
          szl_memset(&(szl->szlZabSyncTransactions[index]), 0, sizeof(szlZabSyncTransactions_t));
          SZL_UNLOCK(Service, szl->szlServiceDataMutex);
          callback.SZLZAB_CB_GenericResp(Service, status, responseParams, tid);
          cleanupErrorResponseParameters(Service, syncTransType, responseParams);
          return;
        }
    }
  SZL_UNLOCK(Service, szl->szlServiceDataMutex);
}



/******************************************************************************
 * Check for timeout of an entry in the Synchronous Transaction Table
 ******************************************************************************/
static
szl_uint32 checkSynchronousTransactionTimeout(erStatus* Status,
                                            zabService* Service)
{
  szl_uint8 index;
  szlZabServiceType* szl;
  unsigned32 timeNow;
  szl_uint32 timeoutRemainingS = ZAB_WORK_DELAY_MAX_MS;
  szl_uint32 timeoutRemainingMs = ZAB_WORK_DELAY_MAX_MS;

  ER_CHECK_STATUS_VALUE(Status, timeoutRemainingMs);
  szl = &ZAB_SRV(Service)->szlService;
  ER_CHECK_NULL_VALUE(Status, szl, timeoutRemainingMs);

  osTimeGetSeconds(Service, &timeNow);

  /* Search for valid transactions that have exceeded their timeout */
  for (index = 0; index < SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS; index++)
    {
      if (szl->szlZabSyncTransactions[index].syncTransType != SZL_ZAB_SYNC_TRANS_TYPE_NONE)
        {
          /* Check timeout while handling wrapped time */
          timeoutRemainingS = szl->szlZabSyncTransactions[index].timeout - timeNow;
          if ( (timeoutRemainingS == 0) || (timeoutRemainingS > 0x80000000) )
            {
              generateSynchronousTransactionFailure(Status,
                                                    Service,
                                                    &(szl->szlZabSyncTransactions[index]),
                                                    SZL_STATUS_TIMEOUT);
            }
          else
            {
              timeoutRemainingMs = MIN(timeoutRemainingMs, (1000 * timeoutRemainingS));
            }
        }
    }
  return timeoutRemainingMs;
}


/******************************************************************************
 * Register an endpoint with the ZAB
 *
 * WARNING: For now this just registers in the vendor. ZPI level work required toreally make it work!
 *
 * Parameters:
 *   status: Error status
 *   service: ZAB service
 *   simpleDesc: Simple descriptor of the EP to be created
 ******************************************************************************/
static void szl_zab_RegisterEndpointReq(erStatus* Status, zabService* Service, zabMsgProSimpleDescriptor* simpleDesc)
{
  szl_uint8 length = zabMsgProSimpleDescriptor_SIZE(simpleDesc->numInClusters + simpleDesc->numOutClusters);
  sapMsg* Msg = sapMsgAllocateData( Status, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);

  if (Msg != NULL)
    {
      sapMsgSetAppDataMax (Status, Msg, length);
      sapMsgSetAppDataLength(Status, Msg, length);
      sapMsgSetAppType( Status, Msg, ZAB_MSG_APP_REGISTER_REQ);

      osMemCopy(Status, sapMsgGetAppData(Msg), (szl_uint8*)simpleDesc, length);

      sapMsgPutFree( Status, ZAB_SRV( Service )->DataSAP, Msg );
    }
}

/**
 * Register endpoint
 *
 * For each endpoint, find each cluster than has a registered attribute in the table. This is the in cluster list.
 * Register endpoints
 *
 * WARNING: This function requires artf24534 to be fixed before it can work at any time.
 */
SZL_RESULT_t SzlZab_EndpointRegister(zabService* Service)
{

  szl_uint8 ipIndex;
  szl_uint8 endpoint;
  szl_uint8 idx;
  erStatus Status;
  zabMsgProSimpleDescriptor* simpleDesc;
  SZL_RESULT_t result = SZL_RESULT_SUCCESS;

  if (LIB_DATA(Service).Lib.Initialized != szl_true)
    {
      return SZL_RESULT_LIBRARY_NOT_INITIALIZED;
    }

  erStatusClear(&Status, Service);
  simpleDesc = (zabMsgProSimpleDescriptor*)OS_MEM_MALLOC(&Status,
                                                         SzlZab_GetServiceId(Service),
                                                         zabMsgProSimpleDescriptor_SIZE(SZL_CFG_MAX_CLUSTERS),
                                                         MALLOC_ID_SZL_EP_REGISTER);
  if (simpleDesc == NULL)
    {
      return SZL_RESULT_DATA_MISSING;
    }


  simpleDesc->profileId = SZL_CFG_PROFILE_ID;
  simpleDesc->deviceVersion = SZL_CFG_DEVICE_VERSION;

  /* Loop through each endpoint */
  for (ipIndex = 0; ipIndex < LIB_DATA(Service).initData.NumberOfEndpoints; ipIndex++)
    {
     
      endpoint = LIB_DATA(Service).initData.Endpoints[ipIndex];
      simpleDesc->endpoint = endpoint;
      simpleDesc->deviceId = (unsigned16)Szl_GetDeviceID(endpoint);
      simpleDesc->numInClusters = 0;

      printInfo(Service, "SZL: Endpoint Reg: Ep 0x%02X, Prof 0x%04X, DevID 0x%04X, DevVer 0x%02X\n",
                 simpleDesc->endpoint,
                 simpleDesc->profileId,
                 simpleDesc->deviceId,
                 simpleDesc->deviceVersion);


      // get all input clusters
      if (Szl_GetInputClusters(Service,
                               endpoint,
                               SZL_CFG_MAX_CLUSTERS,
                               simpleDesc->clusterList,
                               &simpleDesc->numInClusters) != SZL_STATUS_SUCCESS)
        {
          printError(Service, "SZL: ERROR - EP 0x%02X not registered: Too many input clusters\n", endpoint);
          continue;
        }
      printInfo(Service, "\tInClusters  =");
      for (idx = 0; idx < simpleDesc->numInClusters; idx++)
        {
          printInfo(Service, " %04X", simpleDesc->clusterList[idx]);
        }
      printInfo(Service, "\n");

      // get all output clusters
      if (Szl_GetOutputClusters(Service,
                                endpoint,
                                SZL_CFG_MAX_CLUSTERS - simpleDesc->numInClusters,
                                &simpleDesc->clusterList[simpleDesc->numInClusters],
                                &simpleDesc->numOutClusters ) != SZL_STATUS_SUCCESS)
        {
          printError(Service, "SZL: ERROR - EP 0x%02X not registered: Too many output clusters\n", endpoint);
          continue;
        }
      printInfo(Service, "\tOutClusters =");
      for (idx = 0; idx < simpleDesc->numOutClusters; idx++)
        {
          printInfo(Service, " %04X", simpleDesc->clusterList[simpleDesc->numInClusters + idx]);
        }
      printInfo(Service, "\n");

      erStatusClear(&Status, Service);
      szl_zab_RegisterEndpointReq(&Status, Service, simpleDesc);
      if (erStatusIsError(&Status))
        {
          printError(Service, "SZL: ERROR - Endpoint registration failed!\n");
          result = SZL_RESULT_FAILED;
        }

    }

  OS_MEM_FREE(&Status, SzlZab_GetServiceId(Service), simpleDesc);

  return result;
}

/******************************************************************************
 * Data in handler for the SZL
 * This must be registered as for Data SAP In for the SZL to run
 * Queues data in for later processing
 ******************************************************************************/
void szl_zab_DataInHandler(erStatus* Status, sapHandle Sap, sapMsg* Message)
{
  zabService *Service = (zabService *) sapCoreGetService(Sap);
  szlZabServiceType* szl;

  ER_CHECK_NULL(Status, Message);
  szl = &ZAB_SRV(Service)->szlService;

  /* Only accept frames if we are initialised. If not just drop them. */
  if (Szl_InternalIsLibInitialized(Service) == SZL_RESULT_SUCCESS)
    {
      /* Queue application messages, ignore everything else (they shouldn't be here anyway! */
      switch (sapMsgGetType(Message))
        {
          case SAP_MSG_TYPE_APP:
            {
              sapMsgUse(Status, Message); // must do this because we are storing it on a queue
              printInfo(Service, "SZL: Data In queued\n");
              qSafeAppend(&szl->szlDataInQ, Message);
            }
            break;

          default:
            printError(Service, "SZL: ERROR: Unknown Msg Type 0x%02X received & discarded\n", (szl_uint8)sapMsgGetType(Message));
            break;
        }
    }
}


/******************************************************************************
 * Process the szlDataInQ
 * Picks incoming messages of the queue and feeds them through the SZL handlers
 ******************************************************************************/
static szl_uint32 processDataIn(erStatus* Status, zabService* Service)
{
  sapMsg* Msg;
  szlZabServiceType* szl;
  szl_uint32 timeoutRemainingMs = ZAB_WORK_DELAY_MAX_MS;
  szl_uint8 Count;

  ER_CHECK_NULL_VALUE(Status, Service, timeoutRemainingMs);
  szl = &ZAB_SRV(Service)->szlService;
  ER_CHECK_NULL_VALUE(Status, szl, timeoutRemainingMs);
  Count = qSafeCount(&szl->szlDataInQ);

  /* Process queue until empty or error*/
  while ( (Count > 0) && (erStatusIsOk(Status)) )
    {
      /* We want the next work work as the command *may* generate a response internally, or in a plugin */
      timeoutRemainingMs = 0;

      /* Get the next message */
      Msg = (sapMsg*) qSafeRemove(&szl->szlDataInQ);
      if (Msg == NULL)
        {
          /* There was an error, so break out */
          break;
        }

      /* Send app messages to their correct handlers, ignore everything as it shouldn't be here */
      if (sapMsgGetType(Msg) == SAP_MSG_TYPE_APP)
        {

          switch(sapMsgGetAppType(Msg))
            {
              case ZAB_MSG_APP_ZDO_IEEE_RSP:
              case ZAB_MSG_APP_ZDO_NWK_RSP:
              case ZAB_MSG_APP_ZDO_POWER_DESC_RSP:
              case ZAB_MSG_APP_ZDO_NODE_DESC_RSP:
              case ZAB_MSG_APP_ZDO_ACTIVE_EP_RSP:
              case ZAB_MSG_APP_ZDO_SIMPLE_DESC_RSP:
              case ZAB_MSG_APP_ZDO_MATCH_DESC_RSP:
              case ZAB_MSG_APP_ZDO_MGMT_LQI_RSP:
              case ZAB_MSG_APP_ZDO_MGMT_RTG_RSP:
              case ZAB_MSG_APP_ZDO_MGMT_NWK_UPDATE_RSP:
              case ZAB_MSG_APP_ZDO_MGMT_BIND_RSP:
              case ZAB_MSG_APP_ZDO_MGMT_LEAVE_RSP:
              case ZAB_MSG_APP_ZDO_BIND_RSP:
              case ZAB_MSG_APP_ZDO_UNBIND_RSP:
              case ZAB_MSG_APP_ZDO_DEVICE_ANNOUNCE:
              case ZAB_MSG_APP_ZDO_USER_DESC_RSP:
              case ZAB_MSG_APP_ZDO_USER_DESC_SET_RSP:
              case ZAB_MSG_APP_ZDO_NWK_LEAVE_IND:
                szlZdo_DataInHandler(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_AF_DATA_IN:
                ZclM_HandleMsg(Status, Service, Msg);
                break;

            case ZAB_MSG_APP_CMD_ERROR:
                ZclM_HandleError(Status, Service, Msg);
                break;

#ifdef SZL_CFG_ENABLE_GREEN_POWER
            case ZAB_MSG_APP_GP_COM_IND:
            case ZAB_MSG_APP_GP_COM_NTF_IND:
            case ZAB_MSG_APP_GP_TX_QUEUE_EXPIRY:
                szlGp_DataInHandler(Status, Service, Msg);
                break;
#endif

              default:
                printError(Service, "SZL: WARNING - DATA IN PROCESS: App Msg, Unhandled type = 0x%02X recevied\n", (szl_uint8)sapMsgGetAppType(Msg));
                break;
            }
        }
      else
        {
          printError(Service, "SZL: ERROR - DATA IN PROCESS: Unknwon Msg Type 0x%02X received\n", (szl_uint8)sapMsgGetType(Msg));
        }

      /* Free the message */
      sapMsgFree(Status, Msg);
      Count--;
    }
  return timeoutRemainingMs;
}

/******************************************************************************
 * SZL Work Function
 * This will call any registered SZL handlers
 ******************************************************************************/
szl_uint32 szl_zab_Work(erStatus* Status, zabService* Service, szl_uint32 timeNow)
{
  szl_uint32 msSinceLastExecution;
  szl_uint32 nextWorkRequiredDelayMs = ZAB_WORK_DELAY_MAX_MS;
  szl_uint32 nextWorkRequiredDelayMsTemp;

  szlZabServiceType* szl;

  ER_CHECK_NULL_VALUE(Status, Service, nextWorkRequiredDelayMs);
  szl = &ZAB_SRV(Service)->szlService;
  ER_CHECK_NULL_VALUE(Status, szl, nextWorkRequiredDelayMs);

  /* Calculate time since last execution, handling timer wrap */
  msSinceLastExecution = timeNow - szl->szlTimer.gLastTimeTick;
  if (timeNow < szl->szlTimer.gLastTimeTick)
    {
      msSinceLastExecution += 2^32;
    }

  /* Run the SZL timers */
  Szl_TimerAdvance(Service, msSinceLastExecution);
  szl->szlTimer.gLastTimeTick = timeNow;

  nextWorkRequiredDelayMsTemp = Szl_ExtTimerProcess(Service);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  nextWorkRequiredDelayMsTemp = processDataIn(Status, Service);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  nextWorkRequiredDelayMsTemp = checkSynchronousTransactionTimeout(Status, Service);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  CbHandler_Maintenance(Service);

  /* ARTF152098: Get the endpoint update done if required */
  if (szl->EndpointUpdateRequired == szl_true)
    {
      if (SzlZab_EndpointRegister(Service) == SZL_RESULT_SUCCESS)
        {
          szl->EndpointUpdateRequired = szl_false;
          nextWorkRequiredDelayMs = 0;
        }
    }

  return nextWorkRequiredDelayMs;
}


#ifdef ZAB_CFG_ENABLE_WIRELESS_TEST_BENCH
/******************************************************************************
 * WIRELESS TEST BENCH: Register for Data Request Notification from vendor
 ******************************************************************************/
void szlZab_RegisterDataRequestNotificationHandler(erStatus* Status, zabService* Service, wtbUtility_DataReqNotification_t Callback)
{
  ZAB_SRV(Service)->szlService.DataRequestNotificationCallback = Callback;
}

/******************************************************************************
 * WIRELESS TEST BENCH: Register for Data Confirm Notifications from vendor
 ******************************************************************************/
void szlZab_RegisterDataErrorNotificationHandler(erStatus* Status, zabService* Service, wtbUtility_SzlDataErrorNotification_t Callback)
{
  ZAB_SRV(Service)->szlService.DataConfirmNotificationCallback = Callback;
}

/******************************************************************************
 * WIRELESS TEST BENCH: Register for Data Indication Notification from vendor
 ******************************************************************************/
void szlZab_RegisterDataIndicationNotificationHandler(erStatus* Status, zabService* Service, wtbUtility_DataIndNotification_t Callback)
{
  ZAB_SRV(Service)->szlService.DataIndicationNotificationCallback = Callback;
}
#endif
