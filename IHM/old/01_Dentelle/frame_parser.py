# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to parse the frame received from PLC
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from frame import *


def is_ack_success_frame(data):
    # Define the desired frame
    master_frame = bytearray([Command.ACK.value, Status.ACK_SUCCESS.value])

    # Check frame
    if data == master_frame:
        return True
    else:
        return False


def is_ack_error_frame(data):

        # Check frame
        if int(data[0]) == Command.ACK.value:
            if int(data[1]) != Status.ACK_SUCCESS.value:
                return True, data[1]
            else:
                return False, Status.ACK_SUCCESS.value
        else:
            return False, Status.ACK_SUCCESS.value


def is_uninitialized_state_frame(data):

    # Define the desired frame
    master_frame = bytearray([Command.CONTROL.value, Type.STATE.value, Network.NETWORK.value,
                              NetworkState.UNINITIALIZED.value])

    # Check frame
    if data == master_frame:
        return True
    else:
        return False


def is_none_state_frame(data):

    return _check_frame(data, NetworkState.NONE.value)


def is_networking_state_frame(data):

    return _check_frame(data, NetworkState.NETWORKING.value)


def is_networked_state_frame(data):

    # Define the desired frame
    master_frame = bytearray([Command.CONTROL.value, Type.STATE.value, Network.NETWORK.value,
                              NetworkState.NETWORKED.value])
    # Check frame
    if data[:4] == master_frame:
        return True, int(data[4]), bytearray([data[5], data[6]])
    else:
        return False, None, None


def is_data_frame(data):

    # Check frame
    if int(data[0]) == Command.DATA.value:
        return True, data[1:]
    else:
        return False, None


def _check_frame(data, network_status):

    # Define the desired frame
    master_frame = bytearray([Command.CONTROL.value, Type.STATE.value, Network.NETWORK.value,
                              network_status])
    # Check frame
    if data == master_frame:
        return True
    else:
        return False


def check_crc(data):
    return True
