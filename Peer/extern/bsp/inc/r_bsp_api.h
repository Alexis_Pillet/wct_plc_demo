/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/******************************************************************************
* File Name     : r_bsp_api.c
* Device(s)     : RX631
* Tool-chain    : KPIT GNURX-ELF 16.01 / CC-RX 2.05
* H/W platform  : G-CPX3
* Description   : Sample software
******************************************************************************/
#ifndef R_BSP_API_H
#define R_BSP_API_H

/******************************************************************************
Macro definitions
******************************************************************************/
#define R_BSP_CRC_LSB       (0u)        /* LSB first CRC computation */
#define R_BSP_CRC_MSB       (1u)        /* MSB first CRC computation */

#define R_BSP_CRC_POLY_8    (1u)        /* CRC Generator polynomial x^8 + x^2 + x^1 + x^0 */
#define R_BSP_CRC_POLY_1_16 (2u)        /* CRC Generator polynomial x^16 + x^15 + x^2 + x^1 */
#define R_BSP_CRC_POLY_2_16 (3u)        /* CRC Generator polynomial x^16 + x^12 + x^5 + x^1 */

#define MODE_DOWNLOAD       (0x00u)     /* UART Firmware download mode */
#define MODE_COMMUNICATE    (0x01u)     /* UART communication mode */
#define MODE_RAW            (0x02u)     /* UART communication mode */

#define MODE_GUI            (0x00u)     /* UART GUI mode */
#define MODE_MGMT           (0x01u)     /* UART Serial MGMT connection */
#define MODE_FW_UPDATE      (0x02u)     /* UART Serial FW Update Mode (SCI0) */

#define R_PCLKB_HZ          (48000000u) /* PCLKB Hz */
#define R_UART_CPX_CLOCK    R_PCLKB_HZ  /* UART Clock (Hz) */
#define R_UART_HOST_CLOCK   R_PCLKB_HZ  /* UART Clock (Hz) */
#define R_UART_DBG_CLOCK    R_PCLKB_HZ  /* UART Clock (Hz) */
#define R_UART_RX_IPR       (0x09u)     /* UART RX priority */
#define R_UART_TX_IPR       (0x08u)     /* UART TX priority */
#define R_TIMER_IPR         (0x07u)     /* Free Run Timer priority */
#define R_CMT_CPX_IPR       (0x06u)     /* CMT Timer priority ( CPX SAP Thread ) */
#define R_CMT_APP_IPR       (0x01u)     /* CMT Timer priority ( APP Thread ) */

/******************************************************************************
Typedef definitions
******************************************************************************/
typedef void (*r_bsp_callback_t)(void);             // Callback function type with void parameter list
typedef void (*r_bsp_sci_rx_callback_t)(uint8_t);   // Callback function type with parameter list

/*!
   \enum r_bsp_uart_rx_status_t
   \brief Enumeration for the serial reception status
 */
typedef enum
{
    R_BSP_UART_RX_ON  = 0, // Serial reception on
    R_BSP_UART_RX_OFF = 1  // Serial reception off

} r_bsp_uart_rx_status_t;

/*!
   \enum r_bsp_uart_debug_rx_status_t
   \brief Enumeration for the serial debug reception status
 */
typedef enum
{
    R_BSP_UART_DEBUG_RX_ON  = 0, // Serial reception on
    R_BSP_UART_DEBUG_RX_OFF = 1  // Serial reception off

} r_bsp_uart_debug_rx_status_t;

/*!
   \enum r_bsp_board_type_t
   \brief Enumeration of the different CPX board types
 */
typedef enum
{
    R_BOARD_NOT_SET = 0, // Type not set
    R_BOARD_G_CPX3  = 5, // GLOBAL CPX3 board

} r_bsp_board_type_t;

/*!
   \enum r_bsp_clk_out_t
   \brief Enumeration for the clock output flag (from TMR0 or SCI)
 */
typedef enum
{
    R_BSP_CLK_OUT_OFF = 0, // Clock output off (inactive)
    R_BSP_CLK_OUT_ON  = 1  // Clock output on (active)

} r_bsp_clk_out_t;

/*!
   \enum r_bsp_led_t
   \brief Enumeration for the LEDs
 */
typedef enum
{
    R_BSP_LED_4, // LED 4, available on EU-OFDM-PLC and G-CPX
    R_BSP_LED_5, // LED 5, available on EU-OFDM-PLC and G-CPX
    R_BSP_LED_NUM
} r_bsp_led_t;


/*!
   \enum r_bsp_boot_t
   \brief Enumeration for the CPX BOOT MODE
 */
typedef enum
{
    R_BSP_BOOT_UART, // UART Boot
    R_BSP_BOOT_SROM, // S-ROM boot
} r_bsp_boot_t;

/*!
   \enum r_bsp_uart_t
   \brief Enumeration for different UARTs
 */
typedef enum
{
    R_BSP_RX_CPX_UART   = 0,  // UART between RX and CPX
    R_BSP_RX_HOST_UART  = 1,  // UART between RX and host
    R_BSP_RX_DEBUG_UART = 3   // UART between RX and Debug serial

} r_bsp_uart_t;

/*!
   \enum r_bsp_tx_busy_t
   \brief Enumeration for the serial transmission busy flag
 */
typedef enum
{
    R_BSP_TX_BUSY     = 0, // Serial transmission busy
    R_BSP_TX_NOT_BUSY = 1  // Serial transmission not busy

} r_bsp_tx_busy_t;

/*!
   \enum r_bsp_cmt_timer_id_t
   \brief Enumeration for different CMT TIMER IDs
 */
typedef enum
{
    R_BSP_CMT_TIMER_ID_0 = 0,  // R_BSP_CMT_TIMER_ID_0
    R_BSP_CMT_TIMER_ID_1 = 1   // R_BSP_CMT_TIMER_ID_1

} r_bsp_cmt_timer_id_t;


typedef enum
{
	RX_UNINITIALIZED,
	RX_READY,
	RX_RECEIVING,
	RX_BUSY,
	RX_COMPLETE,
	RX_ERROR,
}t_reception_state;

/*!
   \enum r_bsp_uart_rx_t
   \brief UART reception structure
 */
typedef struct
{
    uint32_t        time_stamp;     // Timestamp of current transmission buffer
    uint8_t*        pdata;          // Pointer to buffer to store the data
    uint16_t        size;           // Number of bytes received
    uint16_t        cnt;            // Counts the received data
    uint16_t        max_buf_size;   // Maximum buffer size
    r_bsp_uart_rx_status_t   on;    // Flag of UART Rx on
    r_boolean_t     receiving;      // Flag indicating if a frame is currenlty being received
    r_boolean_t     time_stamp_set; // Flag indicating if the time_stamp is set or not
    t_reception_state		rx_state;		// Stte of the uart rx

} r_bsp_uart_rx_t;



/*!
   \enum r_bsp_uart_debug_rx_t
   \brief UART debug reception structure
 */
typedef struct
{
    uint32_t        		time_stamp;     // Timestamp of current transmission buffer
    uint8_t*        		pdata;          // Pointer to buffer to store the data
    uint16_t        		size;           // Number of bytes received
    uint16_t        		cnt;            // Counts the received data
    uint16_t        		max_buf_size;   // Maximum buffer size
    r_bsp_uart_rx_status_t 	on;    			// Flag of UART Rx on
    r_boolean_t     		receiving;      // Flag indicating if a frame is currenlty being received
    r_boolean_t     		time_stamp_set; // Flag indicating if the time_stamp is set or not
    t_reception_state		rx_state;		// Stte of the uart rx
} r_bsp_uart_debug_rx_t;


/******************************************************************************
Exported global functions (to be accessed by other files)
******************************************************************************/

/***********************************************************************
* Function Name     : R_BSP_InitLeds
* Description       : Initializes the LEDs and turn all LEDs off
* Argument          : none
* Return Value      : none
***********************************************************************/
/*!
   \fn          void R_BSP_InitLeds (void)
   \brief       Initializes the LEDs and turn all LEDs off
   \return      None
 */
void R_BSP_InitLeds(void);

/***********************************************************************
* Function Name     : R_BSP_ToggleLed
* Description       : Toggles a LED
* Argument          : r_bsp_led_t led The led to be toggled
* Return Value      : none
***********************************************************************/
/*!
   \fn          void R_BSP_ToggleLed(r_bsp_led_t led)
   \brief       Toggles a LED
   \param[in]   led LED enumeration
   \return      None
 */
void R_BSP_ToggleLed(r_bsp_led_t led);

/***********************************************************************
* Function Name     : R_BSP_LedOff
* Description       : Turns a LED off
* Argument          : r_bsp_led_t led The led to be turned off
* Return Value      : none
***********************************************************************/
/*!
   \fn          void R_BSP_LedOff(r_bsp_led_t led)
   \brief       Turns a LED off
   \param[in]   led LED enumeration
   \return      None
 */
void R_BSP_LedOff(r_bsp_led_t led);

/***********************************************************************
* Function Name     : R_BSP_LedOn
* Description       : Turns a LED on
* Argument          : r_bsp_led_t led The led to be turned off
* Return Value      : none
***********************************************************************/
/*!
   \fn          void R_BSP_LedOn(r_bsp_led_t led)
   \brief       Turns a LED on
   \param[in]   led LED enumeration
   \return      None
 */
void R_BSP_LedOn(r_bsp_led_t led);

/***********************************************************************
* Function Name     : R_BSP_SetBootMode
* Description       : Select boot mode
* Argument          : r_bsp_boot_t mode Boot mode
* Return Value      : none
***********************************************************************/
/*!
   \fn          void R_BSP_SetBootMode(r_bsp_boot_t mode)
   \brief       Select boot mode
   \param[in]   Boot mode
   \return      None
 */
void R_BSP_SetBootMode(r_bsp_boot_t mode);


/***********************************************************************
* Function Name     : R_BSP_SetBoardType
* Description       : Configures board type to be used
* Argument          : r_bsp_board_type_t boardType The type of the board to be configured
* Return Value      : R_RESULT_SUCCESS if type known, R_RESULT_FAILED otherwise
***********************************************************************/
/*!
   \fn          r_result_t R_BSP_SetBoardType(r_bsp_board_type_t boardType)
   \brief       Configures board type to be used
   \param[in]   boardType Board type to be used
   \return      R_RESULT_SUCCESS if type known, R_RESULT_FAILED otherwise
 */
r_result_t R_BSP_SetBoardType(r_bsp_board_type_t boardType);

/***********************************************************************
* Function Name     : R_BSP_SetClock
* Description       : This function sets the clocks of the MCU, with ICLK being 8x, BCLK being 4x and BCLK being 1x. Additionally, BCLK is output to P53
* Argument          : none
* Return Value      : none
***********************************************************************/
/*!
   \fn       void R_BSP_SetClock (void)
   \brief    Configure the MCU clocks
   \details  This function sets the clocks of the MCU, with ICLK being 8x, BCLK being 4x and BCLK being 1x. Additionally, BCLK is output to P53
   \return   void
 */
void R_BSP_SetClock(void);

/***********************************************************************
* Function Name     : R_BSP_ConfigureTimer
* Description       : Configure the 8-bit timers 0 and 1 as one 16-bit timer, ticking with a
*                   : period of several milliseconds running from a peripheral clock
*                   : with given frequency. When the timer ticks, a callback function is called
* Argument          : uint32_t pclk Peripheral clock frequency in Hz
*                   : uint32_t tick_ms Timer tick in milliseconds
*                   : uint8_t priority Timer interrupt priority
*                   : r_bsp_clk_out_t timer_out Flag to put the timer match signal to output pin
*                   : r_bsp_callback_t p_callback Pointer to the timer callback function which
*                   : will be caled every tick_ms ms
* Return Value      : Either R_RESULT_SUCCESS or R_RESULT_FAILED
***********************************************************************/
/*!
   \fn     r_result_t R_BSP_ConfigureTimer(uint32_t pclk,
                                    uint32_t tick_ms,
                                    uint8_t priority,
                                    r_bsp_clk_out_t timer_out,
                                    r_bsp_callback_t p_callback);
   \brief    Configure the Timer
   \details  Configure the 8-bit timers 0 and 1 as one 16-bit timer, ticking
   with a period of several milliseconds running from a peripheral clock
   with given frequency. When the timer ticks, a callback function is
   called
   \param[in]  pclk Peripheral clock frequency in Hz
   \param[in]  tick_ms Timer tick in milliseconds
   \param[in]  priority Timer interrupt priority
   \param[in]  timer_out Flag to put the timer match signal to output pin
   \param[in]  p_callback Pointer to the timer callback function which will be caled every tick_ms ms
   \return   Either R_RESULT_SUCCESS or R_RESULT_FAILED
 */
r_result_t R_BSP_ConfigureTimer(uint32_t pclk,
                                    uint32_t tick_ms,
                                    uint8_t priority,
                                    r_bsp_clk_out_t timer_out,
                                    r_bsp_callback_t p_callback);

/***********************************************************************
* Function Name     : R_BSP_TimerOff
* Description       : Sets the compare match timer off
* Argument          : none
* Return Value      : none
***********************************************************************/
/*!
   \fn          void R_BSP_TimerOff(void);
   \brief       Sets the compare match timer off
   \param[in]   void
   \return      void
 */
void R_BSP_TimerOff(void);

/***********************************************************************
* Function Name     : R_BSP_ConfigureCMTimer
* Description       : Configure the Compare match timer 0, ticking with a period of
*                   : several milliseconds running from a peripheral clock with given
*                   : frequency. When the timer ticks, a callback function is called
* Argument          : uint32_t pclk Peripheral clock frequency in Hz
*                   : uint32_t tick_ms Timer tick in milliseconds
*                   : uint8_t priority Compare match timer interrupt priority
*                   : uint32_t timerId Id of the timer to be configured
*                   : r_bsp_callback_t p_callback Pointer to a callback function
* Return Value      : Either R_RESULT_SUCCESS or R_RESULT_FAILED
***********************************************************************/
/*!
   \fn     r_result_t R_BSP_ConfigureCMTimer(uint32_t pclk,
                                  uint32_t tick_ms,
                                  uint8_t priority,
                                  r_bsp_cmt_timer_id_t timerId,
                                  r_bsp_callback_t p_callback);
   \brief    Configure the Compare match timer
   \details  Configure the Compare match timer 0, ticking with a period of
   several milliseconds running from a peripheral clock with given
   frequency. When the timer ticks, a callback function is called
   \param[in]  pclk Peripheral clock frequency in Hz
   \param[in]  priority Compare match timer interrupt priority
   \param[in]  tick_ms Timer tick in milliseconds
   \param[in]  timerId Id of the timer to be configured
   \param[in]  p_callback Pointer to a callback function
   \return   Either R_RESULT_SUCCESS or R_RESULT_FAILED
 */
r_result_t R_BSP_ConfigureCMTimer(uint32_t pclk,
                                  uint32_t tick_ms,
                                  uint8_t priority,
                                  r_bsp_cmt_timer_id_t timerId,
                                  r_bsp_callback_t p_callback);


/***********************************************************************
* Function Name     : R_BSP_CMTimerOn
* Description       : Sets the compare match timer on
* Argument          : r_bsp_cmt_timer_id_t timerId Id of the timer which was configured
* Return Value      : none
***********************************************************************/
/*!
   \fn          void R_BSP_CMTimerOn(r_bsp_cmt_timer_id_t timerId);
   \brief       Sets the compare match timer on
   \param[in]   timerId Id of the timer which was configured
   \return      void
 */
void R_BSP_CMTimerOn(r_bsp_cmt_timer_id_t timerId);

/***********************************************************************
* Function Name     : R_BSP_CMTimerOff
* Description       : Sets the compare match timer off
* Argument          : timerId Id of the timer which was configured
* Return Value      : none
***********************************************************************/
/*!
   \fn          void R_BSP_CMTimerOff(r_bsp_cmt_timer_id_t timerId);
   \brief       Sets the compare match timer off
   \param[in]   timerId Id of the timer which was configured
   \return      void
 */
void R_BSP_CMTimerOff(r_bsp_cmt_timer_id_t timerId);

/***********************************************************************
* Function Name     : R_BSP_ConfigureUart
* Description       : Configure the desired SCI channel as UART given the peripheral clock
*                   : frequency and desired baudrate
* Argument          : uint32_t pclk Peripheral clock frequency in Hz
*                   : uint32_t baudrate Baudrate in bps
*                   : r_bsp_uart_t uartType Enumeration for the UART channel
*                   : uint8_t uartType Enumeration for the UART channel
*                   : uint8_t txPriority UART TX interrupt priority
*                   : uint8_t rxPriority UART RX interrupt priority
*                   : r_bsp_clk_out_t sck_out Flag to put the SCK signal to output pin
*                   : r_bsp_callback_t txFinishedCb callback function pointer called when the data tx finishes
*                   : r_bsp_sci_rx_callback_t rxFinishedCb callback function pointer called when the data rx finishes
*                   : uint8_t uartMode selected mode for the uart
* Return Value      : Either R_RESULT_SUCCESS or R_RESULT_FAILED
***********************************************************************/
/*!
   \fn     r_result_t R_BSP_ConfigureUart(uint32_t pclk,
                                   uint32_t baudrate,
                                   r_bsp_uart_t uartType,
                                   uint8_t txPriority,
                                   uint8_t rxPriority,
                                   r_bsp_clk_out_t sck_out,
                                   r_bsp_callback_t txFinishedCb,
                                   r_bsp_sci_rx_callback_t rxFinishedCb);
   \brief    Configure the UART
   \details  Configure the desired SCI channel as UART given the peripheral clock frequency and desired baudrate
   \param[in]  pclk Peripheral clock frequency in Hz
   \param[in]  baudrate Baudrate in bps
   \param[in]  uartType Enumeration for the UART channel
   \param[in]  txPriority UART TX interrupt priority
   \param[in]  rxPriority UART RX interrupt priority
   \param[in]  sck_out Flag to put the SCK signal to output pin
   \param[in]  txFinishedCb callback function pointer called when the data tx finishes
   \param[in]  rxFinishedCb callback function pointer called when the data rx finishes
   \return   Either R_RESULT_SUCCESS or R_RESULT_FAILED
 */
r_result_t R_BSP_ConfigureUart(uint32_t pclk,
                                   uint32_t baudrate,
                                   r_bsp_uart_t uartType,
                                   uint8_t txPriority,
                                   uint8_t rxPriority,
                                   r_bsp_clk_out_t sck_out,
                                   r_bsp_callback_t txFinishedCb,
                                   r_bsp_sci_rx_callback_t rxFinishedCb);

/***********************************************************************
* Function Name     : R_BSP_SendUart
* Description       : Send a number of bytes via UART, by first checking if the UART
*                   : is not busy with a previous transmission, and if so, blocking
*                   : until the previous transmission finishes before starting a new
*                   : one. Once data tarnsmission starts, no further blocking happens
*                   : and sending continues by using interrupts
* Argument          : const uint8_t* p_data Pointer to buffer storing the data to be sent
*                   : uint16_t size Number of bytes to send
*                   : r_bsp_uart_t channel Number of SCI channel to be used
* Return Value      : Either R_RESULT_SUCCESS or R_RESULT_FAILED
***********************************************************************/
/*!
   \fn     r_result_t R_BSP_SendUart(const uint8_t* p_data,
                              uint16_t size,
                              r_bsp_uart_t channel);
   \brief    Send a number of bytes via UART
   \details  Send a number of bytes via UART, by first checking if the UART
   is not busy with a previous transmission, and if so, blocking
   until the previous transmission finishes before starting a new
   one. Once data tarnsmission starts, no further blocking happens
   and sending continues by using interrupts
   \param[in]  p_data Pointer to buffer storing the data to be sent
   \param[in]  size Number of bytes to send
   \param[in]  channel Number of SCI channel to be used
   \return   Either R_RESULT_SUCCESS or R_RESULT_FAILED
 */
r_result_t R_BSP_SendUart(const uint8_t* p_data,
                              uint32_t size,
                              r_bsp_uart_t channel);

/***********************************************************************
* Function Name     : R_BSP_ConfigureCrc
* Description       : Configure the CRC
* Argument          : uint8_t lms R_BSP_CRC_MSB for MSB first or R_BSP_CRC_LSB for LSB first
*                   : uint8_t gps CRC generator polynomial, R_BSP_CRC_POLY_8, R_BSP_CRC_POLY_1_16 or R_BSP_CRC_POLY_2_16
* Return Value      : Either R_RESULT_SUCCESS or R_RESULT_FAILED
***********************************************************************/
/*!
   \fn          r_result_t R_BSP_ConfigureCrc (uint8_t lms, uint8_t gps);
   \brief       Configure the CRC
   \details     Configure the CRC
   \param[in]   lms R_BSP_CRC_MSB for MSB first or R_BSP_CRC_LSB for LSB first
   \param[in]   gps CRC generator polynomial, R_BSP_CRC_POLY_8, R_BSP_CRC_POLY_1_16 or R_BSP_CRC_POLY_2_16
   \return      Either R_RESULT_SUCCESS or R_RESULT_FAILED
 */
r_result_t R_BSP_ConfigureCrc(uint8_t lms,
                              uint8_t gps);

/***********************************************************************
* Function Name     : R_BSP_EnableInterrupt
* Description       : Enabling of interrupts
* Argument          : none
* Return Value      : none
***********************************************************************/

/*!
   \fn          void R_BSP_EnableInterrupt(void);
   \brief       Enabling of interrupts
   \details     Enabling of all interrupts
   \return      void
 */
void R_BSP_EnableInterrupt(void);

/***********************************************************************
* Function Name     : R_BSP_DisableInterrupt
* Description       : Disabling of interrupts
* Argument          : none
* Return Value      : none
***********************************************************************/

/*!
   \fn          void R_BSP_DisableInterrupt(void);
   \brief       Disabling of interrupts
   \details     Disabling of all interrupts
   \return      void
 */
void R_BSP_DisableInterrupt(void);

/***********************************************************************
* Function Name     : R_BSP_ComputeCrc
* Description       : Compute the CRC and return it in the crcResult
* Argument          : const uint8_t input[] Data input
*                   : uint16_t length Data input length in bytes
*                   : uint8_t lms R_BSP_CRC_MSB for MSB first or R_BSP_CRC_LSB for LSB first
*                   : uint8_t gps CRC generator polynomial, R_BSP_CRC_POLY_8, R_BSP_CRC_POLY_1_16 or R_BSP_CRC_POLY_2_16
*                   : crcResult Pointer to the location where the CRC will be stored
* Return Value      : Either R_RESULT_SUCCESS or R_RESULT_FAILED
***********************************************************************/
/*!
   \fn          r_result_t R_BSP_ComputeCrc(const uint8_t input[], uint16_t length, uint8_t lms, uint8_t gps, uint8_t* crcResult);
   \brief       Compute the CRC
   \details     Compute the CRC and return it in the crcResult
   \param[in]   input Data input
   \param[in]   length Data input length in bytes
   \param[in]   lms R_BSP_CRC_MSB for MSB first or R_BSP_CRC_LSB for LSB first
   \param[in]   gps CRC generator polynomial, R_BSP_CRC_POLY_8, R_BSP_CRC_POLY_1_16 or R_BSP_CRC_POLY_2_16
   \param[in]   crcResult Pointer to the location where the CRC will be stored
   \return      Either R_RESULT_SUCCESS or R_RESULT_FAILED
 */
r_result_t R_BSP_ComputeCrc(const uint8_t input[],
                                uint16_t length,
                                uint8_t lms,
                                uint8_t gps,
                                uint8_t* crcResult);

/***********************************************************************
* Function Name     : R_BSP_ValidateCrc
* Description       : Validate the CRC
* Argument          : const uint8_t input[] Data input
*                   : uint16_t length Data input length in bytes
*                   : uint8_t lms R_BSP_CRC_MSB for MSB first or R_BSP_CRC_LSB for LSB first
*                   : uint8_t gps CRC generator polynomial, R_BSP_CRC_POLY_8, R_BSP_CRC_POLY_1_16 or R_BSP_CRC_POLY_2_16
* Return Value      : Either R_RESULT_SUCCESS or R_RESULT_FAILED
***********************************************************************/
/*!
   \fn          r_result_t R_BSP_ValidateCrc(const uint8_t input[], uint16_t length, uint8_t lms, uint8_t gps);
   \brief       Validate the CRC
   \details     Validate the CRC
   \param[in]   input Data input including appended 2 byte - CRC
   \param[in]   length Data length including CRC in bytes
   \param[in]   lms R_BSP_CRC_MSB for MSB first or R_BSP_CRC_LSB for LSB first
   \param[in]   gps CRC generator polynomial, R_BSP_CRC_POLY_8, R_BSP_CRC_POLY_1_16 or R_BSP_CRC_POLY_2_16
   \return      Either R_RESULT_SUCCESS or R_RESULT_FAILED
 */
r_result_t R_BSP_ValidateCrc(const uint8_t input[],
                                 uint16_t length,
                                 uint8_t lms,
                                 uint8_t gps);

/***********************************************************************
* Function Name     : R_BSP_CheckInStack
* Description       : MCU-dependant check if pointer resides in the stack
* Argument          : p_parameter: Address to check
* Return Value      : Either R_RESULT_SUCCESS or R_RESULT_FAILED
***********************************************************************/
/*!
   \fn          r_result_t R_BSP_CheckInStack(const uint8_t* p_parameter)
   \brief       MCU-dependant check if pointer resides in the stack
   \details     MCU-dependant check if pointer resides in the stack
   \param[in]   p_parameter: Address to check
   \return      Either R_RESULT_SUCCESS or R_RESULT_FAILED
 */
r_result_t R_BSP_CheckInStack(const uint8_t* p_parameter);

/***********************************************************************
* Function Name     : R_BSP_SoftReset
* Description       : Initiate MCU reset
* Argument          : none
* Return Value      : none
***********************************************************************/
/*!
   \fn          void R_BSP_SoftReset(void)
   \brief       Initiate MCU reset
   \return      void
 */
void R_BSP_SoftReset(void);

/***********************************************************************
* Function Name     : R_BSP_Cpx3Reset
* Description       : Reset the CPX2
* Argument          : none
* Return Value      : none
***********************************************************************/
/*!
   \fn          void R_BSP_Cpx3Reset(void)
   \brief       CPX2 reset
   \return      void
 */
void R_BSP_Cpx3Reset(void);

/***********************************************************************
* Function Name     : R_BSP_UartXon
* Description       : XON command
* Argument          : r_bsp_uart_t channel The UART channel
* Return Value      : Either R_RESULT_SUCCESS or R_RESULT_FAILED
***********************************************************************/
/*!
   \fn          r_result_t R_BSP_UartXon(r_bsp_uart_t channel)
   \brief       XON command
   \param[in]   channel the UART channel
   \return      Either R_RESULT_SUCCESS or R_RESULT_FAILED
 */
r_result_t R_BSP_UartXon(r_bsp_uart_t channel);

/***********************************************************************
* Function Name     : R_BSP_UartXoff
* Description       : Continue UART transmission for a speciffic channel
* Argument          : r_bsp_uart_t channel The UART channel
* Return Value      : Either R_RESULT_SUCCESS or R_RESULT_FAILED
***********************************************************************/
/*!
   \fn          r_result_t R_BSP_UartXoff(r_bsp_uart_t channel)
   \brief       XON command
   \param[in]   channel the UART channel
   \return      Either R_RESULT_SUCCESS or R_RESULT_FAILED
 */
r_result_t R_BSP_UartXoff(r_bsp_uart_t channel);

/******************************************************************************
* Function Name     : R_BSP_GetUartSendStatus
* Description       : Get UART transmission status
* Arguments         : r_bsp_uart_t channel The UART channel
*                   : r_bsp_tx_busy_t *status Pointer to the location where transmission status will be stored 
* Return Value      : 
******************************************************************************/
uint8_t R_BSP_GetUartSendStatus(r_bsp_uart_t channel, r_bsp_tx_busy_t *status);

/***********************************************************************
* Function Name     : R_BSP_IwdtInit
* Description       : Independant watch dog timer initialization
* Argument          : void
* Return Value      : void
***********************************************************************/
/*!
   \fn          void R_BSP_IwdtInit(void);
   \brief       Independant watch dog timer initialization
   \return      void
 */
void R_BSP_IwdtInit(void);

/***********************************************************************
* Function Name     : R_BSP_IwdtRefresh
* Description       : Independant watch dog timer refresh function
* Argument          : void
* Return Value      : void
***********************************************************************/
/*!
   \fn          void R_BSP_IwdtRefresh(void);
   \brief       Independant watch dog timer refresh function
   \return      void
 */
void R_BSP_IwdtRefresh(void);

#endif /* R_BSP_API_H */
