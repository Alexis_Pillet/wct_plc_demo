/*
 Name:    Private Message Shared Definitions
 Author:  ZigBee Excellence Center
 Company: Schneider Electric

 Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

 Description:
    Private for implementation that we do not want to expose outside the module.
*/

#ifndef __SAP_MSG_PRIVATE_H__
#define __SAP_MSG_PRIVATE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "osTypes.h"
#include "sapMsgUtility.h"



/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Message Pool
 * Pools of short and long buffers are kept for messages. This tracks which pool they belong too */
typedef enum
{
  SAP_MSG_POOL_SHORT = 0, // short buffer for most messages
  SAP_MSG_POOL_LONG  = 1, // full size buffer able to fit largest possible message
  SAP_MSG_POOL_COUNT
} sapMsgPool;

/* Action Data */
typedef struct
{
  unsigned8 Action;
} sapMsgActionData;

/* Notification Data */
typedef struct
{
  zabNotificationType NotificationType;
  zabNotificationData NotificationData;
} sapMsgNotificationData;

/* Error Data */
typedef struct
{
  erStatus Status;
} sapMsgErrorData;

/* Application Data Message */
typedef struct
{
  unsigned8  Type;                // identifies the data format. This is actually a zabMsgAppType
  unsigned8  TransactionId;
  unsigned16 DataMax;    // size (max) of data buffer
  unsigned16 DataLength;  // length of app data buffer
  unsigned8* DataPointer;        // pointer to app data buffer (after app header)
} sapMsgAppData;

/* Generic message data - union of all possible payloads */
typedef union
{
  sapMsgActionData Action;
  sapMsgNotificationData Notify;
  sapMsgErrorData Error;
  sapMsgAppData App;
} sapMsgData;

/* Sap Message
 * This struct contains all the info about the sap message
 * It is wrapped by sapMsgStructAppDataWrapper which includes the application data if present*/
typedef struct
{
  void* Next;            // for queuing

  sapHandle Sap;         // SAP from creation

  /*
   * MessageType: Action, Notify, Error, App Data
   * MessageDirection: In/Out/Broadcast
   * MessagePool = Long or short message
   */
  struct
  {
    sapMsgType MessageType : 4;
    sapMsgDirection MessageDirection : 2;
    sapMsgPool MessagePool : 2;  // There is an extra bit used here for SAP_MSG_POOL_COUNT. If you need a bit you can get it here.
  } FlagsType;

  /*
   * Number of processes using this message.
   * If a process needs the message it must increment the UseCount.
   * UseCount is decremented by Free(). When UseCount == 0 the message will actually be freed as no more processes are using it
   */
  unsigned8 UseCount;    // non-zero if allocated

  /*
   * Data: Union depending on value of sapMsgType in FlagsType
   * SAP_MSG_TYPE_ERROR  -> Data.Error
   * SAP_MSG_TYPE_APP    -> Data.App
   * SAP_MSG_TYPE_ACTION -> Data.Action
   * SAP_MSG_TYPE_NOTIFY -> Data.Notify
   */
  sapMsgData Data;
} sapMsgStruct;


/* Wrapper for the header and the data buffer */
typedef struct {
  sapMsgStruct Header;
  unsigned8    DataBuffer[VLA_INIT];
} sapMsgStructAppDataWrapper;



/* Sizing of different types of message */
#define SAP_MSG_HEADER_SIZE      (sizeof( sapMsgStruct ))
#define SAP_MSG_EVENT_SIZE       (sizeof( sapMsgStruct ))
#define SAP_MSG_ACTION_SIZE      SAP_MSG_EVENT_SIZE
#define SAP_MSG_NOTIFY_SIZE      SAP_MSG_EVENT_SIZE
#define SAP_MSG_ERROR_SIZE       (sizeof( sapMsgStruct ))

/* Cast the void pointer back to what it really is! */
#define SAP_MSG( msg ) ((sapMsgStruct*)(msg)) // so you can use SAP_MSG( Message )->xxxx to access internal structure

/* Various leftover weird macros - be very very careful! */
#define SAP_MSG_GET_APP_DATA_BUFFER( m )         (((sapMsgStructAppDataWrapper*)(m))->DataBuffer)
#define SAP_MSG_GET_POOL( m )               (SAP_MSG( m )->FlagsType.MessagePool)
#define SAP_MSG_SET_POOL( m, v )              do { SAP_MSG( m )->FlagsType.MessagePool = (v); } while (0)
#define SAP_MSG_SET_APP_DATA( m, v )          do { SAP_MSG( m )->Data.App.DataPointer = (v);       } while (0)
#define SAP_MSG_SET_SAP( m, v )               do { SAP_MSG( m )->Sap      = (v); } while (0)


#ifdef __cplusplus
}
#endif

#endif
