/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_dflash_lib.h
*    @version
*        $Rev: 989 $
*    @last editor
*        $Author: a5089763 $
*    @date  
*        $Date:: 2016-02-16 14:07:27 +0900#$
* Description : 
******************************************************************************/

#ifndef R_DFLASH_LIB_H
#define R_DFLASH_LIB_H

/******************************************************************************
Macro definitions
******************************************************************************/
#define DFLASH_BASE_ADDR            (0x00100000ul)

#define DFLASH_BLOCK_SIZE           (0x00000020ul)
#define DFLASH_BLOCK_SIZE_LARGE     (0x00000800ul)
#define DFLASH_BLOCK_MASK           (0xFFE0u)
#define DFLASH_BLOCK_MASK_LARGE     (0xF800u)
#define DFLASH_WRITE_LINE           (2u)
/******************************************************************************
Typedef definitions
******************************************************************************/
typedef enum {
    DFID_CONFIG = 0,
    DFID_ADP_SETPARAM,
    DFID_MAC_SETPARAM,
    DFID_END_ADDR,
} r_df_usageid_t;

/******************************************************************************
Functions prottype
******************************************************************************/
uint8_t*   r_dflash_tmp_buff();
uint8_t    r_dflash_erase(uint32_t df_dstaddr, uint16_t size_byte);
uint8_t    r_dflash_write(uint32_t df_dstaddr, uint8_t *src_buff, uint16_t size_byte);
void       r_dflash_read_set();
void       r_dflash_read(uint8_t **dst_buff, uint32_t df_srcaddr, uint16_t size_byte);

#endif /* R_DFLASH_LIB_H */
