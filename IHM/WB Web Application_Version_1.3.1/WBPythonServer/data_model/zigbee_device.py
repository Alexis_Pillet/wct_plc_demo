# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to model ZigBee Device
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from abc import ABC, abstractmethod


class ZigBeeDevice(ABC):

    def __init__(self, address, id_wireless_bridge, application_frame_generator):
        self._application_frame_generator = application_frame_generator
        self._address = address
        self._id_wireless_bridge = int(id_wireless_bridge)
        self._product_id = None
        self._model_identifier = ""
        self._manufacturer_name = ""
        self._application_firmware_version = None
        self._application_hardware_version = None
        self._product_serial_number = None
        self._END_POINT_BASIC_CLUSTER = 0x01
        self._BASIC_CLUSTER = "0000"
        self._MODEL_IDENTIFIER = "0005"
        self._MANUFACTURER_NAME = "0004"
        super(ZigBeeDevice, self).__init__()

    @abstractmethod
    def get_attribute(self, destination_ep, cluster_id, attribute_id_list):
        pass

    @abstractmethod
    def set_attribute(self, destination_ep, cluster_id, attribute_id_list, value):
        pass

    @abstractmethod
    def discovery(self):
        pass

    @abstractmethod
    def update_data(self, payload):
        pass

    @abstractmethod
    def get_json_data(self):
        pass

    @abstractmethod
    def get_json_device(self, device_number):
        pass

    @property
    def application_frame_generator(self):
        return self._application_frame_generator

    @application_frame_generator.setter
    def application_frame_generator(self, frame_generator):
        self._application_frame_generator = frame_generator

    @property
    def address(self):
        return self._address

    @address.setter
    def address(self, address):
        self._address = address

    @property
    def id_wireless_bridge(self):
        return self._id_wireless_bridge

    @id_wireless_bridge.setter
    def id_wireless_bridge(self, id_wireless_bridge):
        self._id_wireless_bridge = id_wireless_bridge

    @property
    def product_id(self):
        return self._product_id

    @product_id.setter
    def product_id(self, product_id):
        self._product_id = product_id

    @property
    def model_identifier(self):
        return self._model_identifier

    @model_identifier.setter
    def model_identifier(self, model_identifier):
        self._model_identifier = model_identifier

    @property
    def manufacturer_name(self):
        return self._manufacturer_name

    @manufacturer_name.setter
    def manufacturer_name(self, manufacturer_name):
        self._manufacturer_name = manufacturer_name

    @property
    def application_firmware_version(self):
        return self._application_firmware_version

    @application_firmware_version.setter
    def application_firmware_version(self, application_firmware_version):
        self._application_firmware_version = application_firmware_version

    @property
    def application_hardware_version(self):
        return self._application_hardware_version

    @application_hardware_version.setter
    def application_hardware_version(self, application_hardware_version):
        self._application_hardware_version = application_hardware_version

    @property
    def product_serial_number(self):
        return self._product_serial_number

    @product_serial_number.setter
    def product_serial_number(self, product_serial_number):
        self._product_serial_number = product_serial_number
