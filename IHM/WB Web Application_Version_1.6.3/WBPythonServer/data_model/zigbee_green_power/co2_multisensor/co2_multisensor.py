
"""
data_model.zigbee_green_power.co2_multisensor.co2_multisensor
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to control/model CO2 MultiSensor Device

"""
from data_model.zigbee_green_power.zigbee_green_power_device import ZigBeeGreenPowerDevice


class CO2MultiSensor(ZigBeeGreenPowerDevice):
    """Model IACT device with one End Point"""

    def __init__(self, address, id_wireless_bridge, application_frame_generator):
        """Initialization of the CO2 MultiSensor device"""
        self._temperature = 0
        self._relative_humidity = 0
        self._co2 = 0
        self._TEMPERATURE_CLUSTER = "0402"
        self._RELATIVE_HUMIDITY_CLUSTER = "0405"
        self._CARBON_DIOXIDE_CLUSTER = "040d"
        self._ATTRIBUTE_ID_MEASURED_VALUE = '0000'
        # call super function
        super(CO2MultiSensor, self).__init__(address, id_wireless_bridge, application_frame_generator)

    @property
    def temperature(self):
        return self._temperature

    @temperature.setter
    def temperature(self, temperature):
        self._temperature = temperature

    @property
    def relative_humidity(self):
        return self._relative_humidity

    @relative_humidity.setter
    def relative_humidity(self, relative_humidity):
        self._relative_humidity = relative_humidity

    @property
    def co2(self):
        return self._co2

    @co2.setter
    def co2(self, co2):
        self._co2 = co2

    def update_data(self, data):
        """Update the attributes of the device according to the value specified

        Args:
            data: dict of the value received
        """
        list_attribute = data[self._BASIC_CLUSTER]
        for attribute in list_attribute:
            if attribute.attribute_id == self._MANUFACTURER_NAME:
                self.manufacturer_name = attribute.attribute_value
            elif attribute.attribute_id == self._MODEL_IDENTIFIER:
                self.model_identifier = attribute.attribute_value
            elif attribute.attribute_id == self._APPLICATION_FIRMWARE_VERSION_ID:
                self.application_firmware_version = attribute.attribute_value
            elif attribute.attribute_id == self._APPLICATION_HARDWARE_VERSION_ID:
                self.application_hardware_version = attribute.attribute_value
            elif attribute.attribute_id == self._PRODUCT_SERIAL_NUMBER_ID:
                self.product_serial_number = attribute.attribute_value
            elif attribute.attribute_id == self._PRODUCT_IDENTIFIER_ID:
                self.product_identifier = attribute.attribute_value
            elif attribute.attribute_id == self._PRODUCT_MODEL:
                self.product_model = attribute.attribute_value

        list_attribute = data[self._TEMPERATURE_CLUSTER]
        for attribute in list_attribute:
            if attribute.attribute_id == self._ATTRIBUTE_ID_MEASURED_VALUE:
                self.temperature = attribute.attribute_value

        list_attribute = data[self._RELATIVE_HUMIDITY_CLUSTER]
        for attribute in list_attribute:
            if attribute.attribute_id == self._ATTRIBUTE_ID_MEASURED_VALUE:
                self.relative_humidity = attribute.attribute_value

        list_attribute = data[self._CARBON_DIOXIDE_CLUSTER]
        for attribute in list_attribute:
            if attribute.attribute_id == self._ATTRIBUTE_ID_MEASURED_VALUE:
                self.co2 = attribute.attribute_value[0]

    def update_rssi(self, lqi, rssi):
        """Update the lqi and rssi of the product

        Args:
            lqi: lqi value
            rssi: rssi value
        """
        self.rssi = rssi
        self.lqi = lqi

    def get_json_data(self):
        """Get the json representation of the data of the IACT product"""
        return {"product": {"address": self.address.upper(),
                            "lqi": self.lqi,
                            "rssi": self.rssi,
                            "model_identifier": self.model_identifier,
                            "application_firmware_version": self.application_firmware_version,
                            "application_hardware_version": self.application_hardware_version,
                            "product_serial_number": self.product_serial_number,
                            "product_identifier": self.product_identifier,
                            "product_model": self.product_model,
                            "manufacturer_name": self.manufacturer_name},
                "temperature": self.temperature / 100,
                "relative_humidity": self.relative_humidity / 100,
                "co2": '%.2f' % (float(self.co2) * 1000000.0)}

    def get_json_device(self, device_number):
        """Get the json representation of the IACT product

        Args:
            device_number: number of the device
        """
        return {"text": self.address.upper() + ' - CO2 MultiSensor',
                'id': "WB_" + str(self.id_wireless_bridge + 1) + "_DEVICE_" + self.address,
                'type': 'co2_multisensor',
                'data': self.get_json_data()
                }
