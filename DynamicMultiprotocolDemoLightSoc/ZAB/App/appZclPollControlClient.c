/******************************************************************************
 *                          ZAB TEST APPLICATION
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the ZCL Identify Cluster client functions for the test app.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 002.001.001  24-Jun-15   MvdB   Original
 * 002.002.010  09-Oct-15   MvdB   ARTF150980: Support Service ID in memory allocation functions for WTB
 *****************************************************************************/




#include "zabCoreService.h"

#include "appZcl.h"
#include "appConfig.h"
#include "appMain.h"

#include "szl.h"

#include "poll_control_client.h"

#include "appZclPollControlClient.h"


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/



/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

szl_bool appZclPollControlClient_CheckinNotificationHandler(zabService* Service, szl_uint8 DesintationEndpoint, szl_uint16 SourceNetworkAddress, szl_uint16* FastPollTimeout)
{
  printApp(Service, "appZclPollControlClient_CheckinNotificationHandler: DstEp = 0x%02X, Src = 0x%04X. Pending = %d, Timeout = %d (qs)\n",
           DesintationEndpoint, SourceNetworkAddress, appConfig_Config.pollControlDataPending, appConfig_Config.pollControlFastPollTime);
  
  if (appConfig_Config.pollControlDataPending == zab_true)
    {
      *FastPollTimeout = appConfig_Config.pollControlFastPollTime;
    }
  
  return (szl_bool)appConfig_Config.pollControlDataPending;
}

void appZclPollControlClient_FastPollStopRespHandler(zabService* Service, SZL_STATUS_t Status, szl_uint16 SourceNetworkAddress, szl_uint8 TransactionId)
{
  printApp(Service, "appZclPollControlClient_FastPollStopRespHandler: Status = 0x%02X, Src = 0x%04X, TID = 0x%02X\n", Status, SourceNetworkAddress, TransactionId);
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/