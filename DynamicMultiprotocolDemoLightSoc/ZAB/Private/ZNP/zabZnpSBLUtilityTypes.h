/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the types for Serial Boot Loader state machine for the ZAB Pro ZNP
 *   vendor.
 *   For details and flow charts see XXX.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *  00.00.02.00 18-Oct-13   MvdB   Upgraded from old file
 * 002.001.001  29-Apr-15   MvdB   ARTF132260: Support firmware upgrade of CC2538 network processors
 *****************************************************************************/

#ifndef _ZAB_ZNP_SBL_UTILITY_TYPES_H_
#define _ZAB_ZNP_SBL_UTILITY_TYPES_H_

#include "osTypes.h"
#include "erStatusUtility.h" 
#include "zabTypes.h"

/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

typedef enum
{
  SBL_STATUS_SUCCESS                 = 0,
  SBL_STATUS_FAILURE                 = 1,
  SBL_STATUS_INVALID_FCS             = 2,
  SBL_STATUS_INVALID_FILE            = 3,
  SBL_STATUS_FILESYSTEM_ERROR        = 4,
  SBL_STATUS_ALREADY_STARTED         = 5,
  SBL_STATUS_NO_RESPOSNE             = 6,
  SBL_STATUS_VALIDATE_FAILED         = 7,
  SBL_STATUS_CANCELED                = 8,
  SBL_STATUS_INVALID_COMMAND         = 9,
}sblStatus;

/* Current Item of the state machine */
typedef enum
{
  ZAB_SBL_CURRENT_ITEM_NONE,
  ZAB_SBL_CURRENT_ITEM_HANDSHAKING,
  ZAB_SBL_CURRENT_ITEM_WRITING,
  ZAB_SBL_CURRENT_ITEM_READING,
  ZAB_SBL_CURRENT_ITEM_ENABING,  
} zabSblCurrentItem;

/* Class of SBL we are running. Handles differences between various network processor types */
typedef enum
{
  ZAB_SBL_CLASS_NONE,
  ZAB_SBL_CLASS_CC2530_CC2531,  
  ZAB_SBL_CLASS_CC2538,  
} zabSblClass;


/* State information for this state machine */
typedef struct
{
  zabSblCurrentItem currentItem;    /* Current internal state - PRIVATE*/
  zabSblClass sblClass;             /* Class of network processor we are upgrading */
  unsigned32 timeoutExpiryMs;       /* Timeout for confirms - PRIVATE */
  unsigned32 imageLength;           /* Length of the image to be loaded */
  unsigned32 address;               /* Current address within the image */
} zabSblInfo_t;

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif /* _ZAB_ZNP_SBL_UTILITY_TYPES_H_ */
