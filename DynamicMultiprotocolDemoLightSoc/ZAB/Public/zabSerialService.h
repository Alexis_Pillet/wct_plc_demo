/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the public interface for the ZAB serial service.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.02.01  10-Dec-13   MvdB   Original
 * 002.002.037  11-Jan-17   MvdB   ARTF165759: Upgrade zabSerialService_SerialReceiveHandler() to return zabSerialService_ReceiveStatus_t to indicate when work is required
 *****************************************************************************/

#ifndef __ZAB_SERIAL_SERVICE_H__
#define __ZAB_SERIAL_SERVICE_H__

#include "zabTypes.h"
#include "sapTypes.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 *****         EXTERN TYPES         *****
 *                 *****         VENDOR PUBLIC        *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Serial Receive Status
 * Returned by zabSerialService_SerialReceiveHandler()
 *  - ZAB_SERIAL_RECEIVE_WORK_NOT_REQUIRED:
 *    - Serial data did not complete a command that requires processing.
 *      No further action required.
 *  - ZAB_SERIAL_RECEIVE_WORK_READY
 *    - Serial data completed a command that requires processing.
 *      zabCoreWork() should be called as soon as possible.
 ******************************************************************************/
typedef enum
{
  ZAB_SERIAL_RECEIVE_WORK_NOT_REQUIRED = 0,
  ZAB_SERIAL_RECEIVE_WORK_READY  = 1,
} zabSerialService_ReceiveStatus_t;

/******************************************************************************
 * Action Out Callback
 * This handler will be called with the following actions:
 *  - ZAB_ACTION_OPEN
 *  - ZAB_ACTION_CLOSE
 ******************************************************************************/
typedef void (*zabSerialService_ActionOutCallback_t) (erStatus* Status, zabService* Service, zabAction Action);

/******************************************************************************
 * Serial Out Callback
 * This handler will be called with serial data to be sent to the network processor.
 * If sending fails Status->Error should be set to an error code, which will cause ZAB
 * to close.
 ******************************************************************************/
typedef void (*zabSerialService_SerialOutCallback_t) (erStatus* Status, zabService* Service, unsigned16 DataLength, unsigned8* Data);

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                 *****         VENDOR PUBLIC        *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Serial Receive Handler
 * Application serial glue must feed ZAB serial bytes received from the network
 * processor via this function.
 * There are no restriction on how bytes are fed to ZAB (one at a time, or in blocks):
 * ZAB will handle all queues, checksums, start of frame etc.
 *
 * Returns zabSerialService_ReceiveStatus_t:
 *  - ZAB_SERIAL_RECEIVE_WORK_NOT_REQUIRED:
 *    - Serial data did not complete a command that requires processing.
 *      No further action required.
 *  - ZAB_SERIAL_RECEIVE_WORK_READY
 *    - Serial data completed a command that requires processing.
 *      zabCoreWork() should be called as soon as possible.
 ******************************************************************************/
extern
zabSerialService_ReceiveStatus_t zabSerialService_SerialReceiveHandler(erStatus* Status, zabService* Service,
                                                                       unsigned8 DataLength,
                                                                       unsigned8* DataPointer);

/******************************************************************************
 * Open State Notification
 * Application serial glue must feed ZAB notifications via this function.
 * After receiving ZAB_ACTION_OPEN: Either ZAB_OPEN_STATE_OPENED or ZAB_OPEN_STATE_CLOSED
 * After receiving ZAB_ACTION_CLOSE: ZAB_OPEN_STATE_CLOSED
 ******************************************************************************/
extern
void zabSerialService_NotifyOpenState(erStatus* Status, zabService* Service,
                                      zabOpenState OpenState);

#ifdef __cplusplus
}
#endif

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif //__ZAB_SERIAL_SERVICE_H__