/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_stdio_api.h
*    @version
*        $Rev: 3203 $
*    @last editor
*        $Author: a0202438 $
*    @date
*        $Date:: 2017-05-10 11:25:47 +0900#$
* Description :
******************************************************************************/

/*
 * Prevent nested inclusions
 */
#ifndef R_STDIO_API_H
#define R_STDIO_API_H

/******************************************************************************
Macro definitions
******************************************************************************/
#define ESC_CHARACTER            (0x1B) /* Escape character as HEX value */
#define R_UART_RX_BUFFER_LENGTH  (256)

/* For host operation, use default I/O functions. */

/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Exported global variables
******************************************************************************/

/******************************************************************************
Exported global functions (to be accessed by other files)
******************************************************************************/

/***********************************************************************
* Function Name     : R_STDIO_Init
* Description       : Initiates the STDIO by configuring the UART
* Argument          : none
* Return Value      : none
***********************************************************************/
/*!
   \fn          r_result_t R_STDIO_Init(void);
   \brief       Initiates the STDIO by configuring the UART
   \return      R_RESULT_FAILED when error else R_RESULT_SUCCESS
 */
r_result_t R_STDIO_Init (void);

/***********************************************************************
* Function Name     : R_STDIO_Printf
* Description       : printf implementation using UART Tx
* Argument          : const char* pformat, ... List of arguments
* Return Value      : The number of bytes printed
***********************************************************************/
/*!
   \fn          int32_t R_STDIO_Printf(const char* pformat, ...);
   \brief       printf implementation using UART Tx
   \return      The number of bytes printed
 */
int32_t R_STDIO_Printf (const char * pformat, ...);

/***********************************************************************
* Function Name     : R_STDIO_Gets
* Description       : gets implementation using UART Rx
* Argument          : char* pstr Memory location where
* Return Value      : The number of bytes read
***********************************************************************/
/*!
   \fn          int R_STDIO_Gets(uint8_t* pstr);
   \brief       gets implementation using UART Rx
   \return      The number of bytes read
 */
char R_STDIO_Gets (char * pstr);

/***********************************************************************
* Function Name     : R_STDIO_GetsWithTimer
* Description       : gets implementation using UART Rx (With timeout)
* Argument          : char* pstr Memory location where
*                   : uint32_t time Timeout value
* Return Value      : The number of bytes read
***********************************************************************/
/*!
   \fn          int R_STDIO_GetsWithTimer(char* pstr, uint32_t time);
   \brief       gets implementation using UART Rx
   \return      The number of bytes read
 */
char R_STDIO_GetsWithTimer (char *   pstr,
                            uint32_t time);

/***********************************************************************
* Function Name     : R_STDIO_Get_Busy_Tx
* Description       : Get the busy flag state
* Argument          : None
* Return Value      : Busy flag state
***********************************************************************/
/*!
   \fn          uint8_t R_STDIO_Get_Busy_Tx( void );
   \brief       Get the busy flag state
   \return      Busy flag state
 */
uint8_t R_STDIO_Get_Busy_Tx( void );

/***********************************************************************
* Function Name     : R_STDIO_Set_Busy
* Description       : Get the busy flag state
* Argument          : Flag state
* Return Value      : None
***********************************************************************/
/*!
   \fn          void R_STDIO_Set_Busy( r_boolean_t flag );
   \brief       Set the busy flag state
   \return      Nothing
 */
void R_STDIO_Set_Busy( r_boolean_t flag );

/***********************************************************************
* Function Name     : R_STDIO_Get_Busy_Rx
* Description       : Get the busy flag state
* Argument          : None
* Return Value      : Busy flag state
***********************************************************************/
/*!
   \fn          uint8_t R_STDIO_Get_Busy_Rx( void );
   \brief       Get the busy flag state
   \return      Busy flag state
 */
uint8_t R_STDIO_Get_Busy_Rx( void );


/***********************************************************************
* Function Name     : R_STDIO_Send_Uart5
* Description       : printf implementation using UART Tx
* Argument          : const uint8_t* data: data to send
* 					: const uint8_t size: number of byte to send
* Return Value      : Error or success
***********************************************************************/
/*!
   \fn          void R_STDIO_Send_Uart5( const uint8_t* data, const uint8_t size );
   \brief       printf implementation using UART Tx
   \return      void
 */
r_result_t R_STDIO_Send_Uart5 ( const uint8_t* data, const uint8_t size );

/***********************************************************************
* Function Name     : R_STDIO_Send_String_Uart5
* Description       : printf implementation using UART Tx
* Argument          : const char* pformat, ... List of arguments
* Return Value      : The number of bytes printed
***********************************************************************/
/*!
   \fn          void R_STDIO_Send_String_Uart5(const char* pformat, ...);
   \brief       printf implementation using UART Tx
   \return      void
 */
void R_STDIO_Send_String_Uart5 (const char * pformat, ...);

/***********************************************************************
* Function Name     : R_STDIO_Debug_Gets
* Description       : gets implementation using UART Rx
* Argument          : char* pstr Memory location where
* Return Value      : The number of bytes read
***********************************************************************/
/*!
   \fn          int R_STDIO_Gets(uint8_t* pstr);
   \brief       gets implementation using UART Rx
   \return      The number of bytes read
 */
char R_STDIO_Debug_Gets (char * pstr);


/***********************************************************************
* Function Name     : R_STDIO_Debug_Get_Busy_Tx
* Description       : Get the busy flag state
* Argument          : None
* Return Value      : Busy flag state
***********************************************************************/
/*!
   \fn          uint8_t R_STDIO_Debug_Get_Busy_Tx( void );
   \brief       Get the busy flag state
   \return      Busy flag state
 */
uint8_t R_STDIO_Debug_Get_Busy_Tx( void );

/***********************************************************************
* Function Name     : R_STDIO_Debug_Set_Busy
* Description       : Get the busy flag state
* Argument          : Flag state
* Return Value      : None
***********************************************************************/
/*!
   \fn          void R_STDIO_Debug_Set_Busy( r_boolean_t flag );
   \brief       Set the busy flag state
   \return      Nothing
 */
void R_STDIO_Debug_Set_Busy( r_boolean_t flag );


/***********************************************************************
* Function Name     : R_STDIO_Debug_Get_Busy_Rx
* Description       : Get the busy flag state
* Argument          : None
* Return Value      : Busy flag state
***********************************************************************/
/*!
   \fn          uint8_t R_STDIO_Debug_Get_Busy_Rx( void );
   \brief       Get the busy flag state
   \return      Busy flag state
 */
uint8_t R_STDIO_Debug_Get_Busy_Rx( void );

/***********************************************************************
* Function Name     : R_STDIO_TxFinishedHandle
* Description       : UART TX finish interrupt
* Argument          : none
* Return Value      : none
***********************************************************************/
/*!
   \fn          void R_STDIO_TxFinishedHandle(void);
   \brief       UART TX finish interrupt
 */
void R_STDIO_TxFinishedHandle (void);

/***********************************************************************
* Function Name     : Uart_IHM_Check_Frame
* Description       : Check uart IHM frame
* Argument          : none
* Return Value      : none
***********************************************************************/
/*!
   \fn          void Uart_IHM_Check_Frame(void);
   \brief       Check uart IHM frame
 */
uint8_t Uart_IHM_Check_Frame( uint8_t* rx_data );

/***********************************************************************
* Function Name     : Uart_Check_Frame
* Description       : Check uart frame
* Argument          : none
* Return Value      : none
***********************************************************************/
/*!
   \fn          void Uart_Check_Frame(void);
   \brief       Check uart frame
 */
uint8_t Uart_Check_Frame( uint8_t* rx_data );


/***********************************************************************
* Function Name     : calcFCS
* Description       : Claculate FCS
* Argument          : none
* Return Value      : none
***********************************************************************/
/*!
   \fn          void calcFCS(void);
   \brief       Check uart frame
 */
uint8_t	calcFCS( uint8_t *pMsg, uint8_t len );


#endif /* R_STDIO_API_H */

