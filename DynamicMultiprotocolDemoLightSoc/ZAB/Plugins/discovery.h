/*******************************************************************************
  Filename    : discovery.h
  $Date       : 2013-10-22                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Mark van den Broeke                                           $:

  Description : This is the header for the Discovery plugin for the SZL
 * 
 * 
 * Node Discovery:
 * Node discovery is a process to find information about all nodes in the Pro network.
 * 
 * It aims to report: Network Address, IEEE Address, Node Type and Communication Status
 * for each node. Any of these values may be unknown, in which case they are
 * set to the defined unknown value.
 * 
 * The process uses MGMT Lqi req/Rsp as this command returns information about
 * the Logical Type and RxOnWhenIdle for nodes. This allows us to avoid sending
 * messages to sleepy End Devices, which will timeout, slowing down the scan.
 * 
 * The process accepts a Network Address parameter, which defines which node
 * the scan will start at. Typically this should be set to the address of
 * the local node.
 * 
 * Possible future enhancements include:
 *  - A broadcast attribute read (or similar) to seed the scan with several start addresses
 *    rather than relying on the app to provide one.
 * 
 * 
 * Endpoint Discovery:
 * Endpoint discovery is a process to discover all endpoints on a node.
 * 
 * For each endpoint, it aims to report: 
 *  - Simple Descriptor
 *  - Basic Cluster:
 *    - Model Id
 *    - Product Id (Schneider MS)
 * 
 * The process uses Active Endpoint Req/Rsp to find the list of endpoint on the node.
 * Then for each endpoint it sends Simple Descriptor Req and Read Attribute().
 * Each endpoint will be reported individually once discovered.
 * The confirm will be called at the completion of the process.
 * 
 * Possible future enhancements:
 *  - More attributes or data as desired by the applications.
 * 
*******************************************************************************/
#ifndef _DISCOVERY_H_
#define _DISCOVERY_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "szl.h"
#include "szl_zdo.h"

  
/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/
  
/* Maximum number of endpoint discoveries that can run at the same time */  
#define SZL_PLUGIN_DISCOVERY_M_MAX_CONCURRENT_DISCOVERIES 10  
  
  
  
/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Unknown values for IEEE and Network Addresses */
#define SZL_PLUGIN_DISCOVERY_IEEE_ADDR_UNKNOWN SZL_ZDO_MGMT_LQI_IEEE_ADDR_UNKNOWN 
#define SZL_PLUGIN_DISCOVERY_NWK_ADDR_UNKNOWN SZL_ZDO_MGMT_LQI_NWK_ADDR_UNKNOWN 

/* Undefined value for ProductIdentifer. Either it is not a Schneider device, or it does not support the MS attribute */  
#define SZL_PLUGIN_DISCOVERY_PRODUCT_IDENTIFIER_UNDEFINED 0xFFFF
  
/* Node Function */
typedef szl_uint8 SZLPLUGIN_DISCOVERY_NODEFUNCTION_t;
typedef enum
{
  SZLPLUGIN_DISCOVERY_NODEFUNCTION_UNKNOWN    = 0,
  SZLPLUGIN_DISCOVERY_NODEFUNCTION_FULL       = 1,
  SZLPLUGIN_DISCOVERY_NODEFUNCTION_REDUCED    = 2,
}ENUM_SZLPLUGIN_DISCOVERY_NODEFUNCTION_t;

/* Node Status */
typedef szl_uint8 SZLPLUGIN_DISCOVERY_NODESTATUS_t;
typedef enum
{
  SZLPLUGIN_DISCOVERY_NODESTATUS_UNKNOWN      = 0,
  SZLPLUGIN_DISCOVERY_NODESTATUS_COMMS_OK     = 1,
  SZLPLUGIN_DISCOVERY_NODESTATUS_COMMS_FAILED = 2,
}ENUM_SZLPLUGIN_DISCOVERY_NODESTATUS_t;

/* Information discovered about a Node */
typedef struct
{
  SZLPLUGIN_DISCOVERY_NODEFUNCTION_t NodeFunction;
  SZLPLUGIN_DISCOVERY_NODESTATUS_t NodeStatus;
  szl_uint16 NetworkAddress;
  
  /* IEEE Address of the Node.
   * For a GPD this will be 0xFFFFFFFFFFFFFFFF and further discovery of the node SHOULD NOT be run */
  szl_uint64 IeeeAddress;
}SzlPlugin_Discovery_NodeInfo_t;

/* Information discovered about an Endpoint */
#define M_MAX_MODEL_ID_LENGTH 32
typedef struct
{
  SZL_ZdoSimpleDescriptorRespParams_t SimpleDesc;
  char ModelId[M_MAX_MODEL_ID_LENGTH + 1]; // Allow one extra byte for terminator
  szl_uint16 ProductIdentifier;
  
  // Private - Applications access this via SimpleDesc.InClusterList/OutClusterList
  szl_uint16 ClusterLists[VLA_INIT];
}SzlPlugin_Discovery_EndpointInfo_t;
#define SzlPlugin_Discovery_EndpointInfo_t_SIZE(_num_clusters) (sizeof(SzlPlugin_Discovery_EndpointInfo_t) - (VLA_INIT*sizeof(szl_uint16)) + (sizeof(szl_uint16) * (_num_clusters)))

/**
 * Node Detected Callback
 */
typedef void (*SzlPlugin_Discovery_NodeDetected_CB_t) (zabService* Service, SzlPlugin_Discovery_NodeInfo_t* NodeInfo);

/**
 * Node Discovery Confirm Callback
 */
typedef void (*SzlPlugin_Discovery_NodeConfirm_CB_t) (zabService* Service, SZL_STATUS_t Status, szl_uint8 NumberOfNodes, SzlPlugin_Discovery_NodeInfo_t* NodeInfo);

/**
 * Endpoint Discovered Callback
 */
typedef void (*SzlPlugin_Discovery_EndpointDiscovered_CB_t) (zabService* Service, SzlPlugin_Discovery_EndpointInfo_t* EndpointInfo);

/**
 * Endpoint Discovery Confirm Callback
 */
typedef void (*SzlPlugin_Discovery_EndpointConfirm_CB_t) (zabService* Service, SZL_STATUS_t Status, szl_uint16 NetworkAddress);

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/  



/**
 * Discover Nodes in the Pro Network, starting at StartNetworkAddress.
 * Callback will be called upon completion with a status and a list of nodes discovered
 */
extern 
SZL_RESULT_t SzlPlugin_Discovery_DiscoverNodes(zabService* Service, 
                                               SzlPlugin_Discovery_NodeConfirm_CB_t Callback, 
                                               szl_uint16 StartNetworkAddress);

/**
 * Discover Endpoints on a Node in the Pro Network
 * 
 * Parameters:
 *  NetworkAddress: network address of node to be discovered
 *  EndpointCallback: Callback to be called with info about each endpoint. 
 *                    Callback will be called once per endpoint on the node.
 *  ConfirmCallback: Callback to be called on completion of discovery, with status
 *                   indicating success/failure of the process.
 *  Retries: Number of times any discovery command will be retried if the first attempt fails.
 *           Total number of attempts to send the command = (Retries+1)
 *           A minimum value of 1 is recommended to ensure reliable discovery.
 *           Each retry will add up to SZL_ZAB_M_SYNCHRONOUS_TRANSACTION_TIMEOUT_S for each command,
 *            so total process can become significantly slower if retries are required.
 */
extern 
SZL_RESULT_t SzlPlugin_Discovery_DiscoverEndpoints(zabService* Service, 
                                                   SzlPlugin_Discovery_EndpointConfirm_CB_t ConfirmCallback, 
                                                   SzlPlugin_Discovery_EndpointDiscovered_CB_t EndpointCallback,
                                                   szl_uint16 NetworkAddress,
                                                   szl_uint8 Retries);

/**
 * Plugin Initialize
 * 
 * SourceEndpoint: This must be a previously registered endpoint. It is used
 *                 as the source for ZCL read attributes commands.
 */
extern 
SZL_RESULT_t SzlPlugin_Discovery_Init(zabService* Service, 
                                      szl_uint8 SourceEndpoint, 
                                      SzlPlugin_Discovery_NodeDetected_CB_t NodeDetectedCallback);

/**
 * Plugin Destroy
 */
extern 
SZL_RESULT_t SzlPlugin_Discovery_Destroy(zabService* Service);


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /*_DISCOVERY_H_*/
