
"""
data_model.zigbee_green_power_zigbee_green_power_device
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to model ZigBee Green Power Device

"""
from abc import abstractmethod
from data_model.zigbee_device import ZigBeeDevice
from application import Application, Attribute
from collections import namedtuple


class ZigBeeGreenPowerDevice(ZigBeeDevice):
    """Model ZigBee Green Power device"""

    def __init__(self, address, id_wireless_bridge, application_frame_generator):
        """Initialization"""
        self._attributeType = namedtuple('AttributeType', ['attribute_id', 'attribute_type', 'attribute_value'])
        super(ZigBeeGreenPowerDevice, self).__init__(address, id_wireless_bridge, application_frame_generator)

    def get_attribute(self, destination_ep, cluster_id, attribute_id_list):
        """Generate a get attribute command to get value of the specified attributes

        Args:
            destination_ep: destination of the cluster
            cluster_id: cluster id containing the attributes
            attribute_id_list: list of the attributes
        """
        option = self.application_frame_generator.default_option
        option = self.application_frame_generator.set_option(option,
                                                             Application.OptionDownMask.DESTINATION_ADDRESS_TYPE,
                                                             Application.OptionAddressType.SOURCE_ID_ADDRESS)
        option = self.application_frame_generator.set_option(option,
                                                             Application.OptionDownMask.DESTINATION_EP_PRESENT,
                                                             Application.OptionEP.NOT_PRESENT)
        option = self.application_frame_generator.set_option(option,
                                                             Application.OptionDownMask.MS_FLAG,
                                                             Application.OptionMS.SCHNEIDER)

        frame = bytearray.fromhex(self.address)
        # Transform in little endian
        frame.reverse()
        cluster_id = bytearray.fromhex(cluster_id)
        # Transform in little endian
        cluster_id.reverse()
        for byte in cluster_id:
            frame.append(byte)
        for value in attribute_id_list:
            attribute_id = bytearray.fromhex(value)
            # Transform in little endian
            attribute_id.reverse()
            for byte in attribute_id:
                frame.append(byte)
        self.application_frame_generator.send_get_attribute_command_frame(self.id_wireless_bridge, frame, option=option)

    def set_attribute(self, destination_ep, cluster_id, attribute_list):
        """Generate a set attribute command to change the value of the specified attributes

        Args:
            destination_ep: destination of the cluster
            cluster_id: cluster id containing the attributes
            attribute_list: list of the attributes
            Named tuple
            >>> AttributeType = namedtuple('Attribute', ['attribute_id', 'attribute_type', 'attribute_value'])
        """
        option = self.application_frame_generator.default_option
        option = self.application_frame_generator.set_option(option,
                                                             Application.OptionDownMask.DESTINATION_ADDRESS_TYPE,
                                                             Application.OptionAddressType.SOURCE_ID_ADDRESS)
        option = self.application_frame_generator.set_option(option,
                                                             Application.OptionDownMask.DESTINATION_EP_PRESENT,
                                                             Application.OptionEP.NOT_PRESENT)
        option = self.application_frame_generator.set_option(option,
                                                             Application.OptionDownMask.MS_FLAG,
                                                             Application.OptionMS.SCHNEIDER)

        frame = bytearray.fromhex(self.address)
        # Transform in little endian
        frame.reverse()
        cluster_id = bytearray.fromhex(cluster_id)
        # Transform in little endian
        cluster_id.reverse()
        for byte in cluster_id:
            frame.append(byte)
        for attribute in attribute_list:
            attribute_id = bytearray.fromhex(attribute.attribute_id)
            # Transform in little endian
            attribute_id.reverse()
            for byte in attribute_id:
                frame.append(byte)

            frame.append(attribute.attribute_type)

            if attribute.attribute_type == Attribute.Type.UINT_48:
                # length of the attribute
                frame.append(0x06)
                attribute_value = bytearray(attribute.attribute_value)
                # Transform in little endian
                attribute_value.reverse()
                for byte in attribute_value:
                    frame.append(byte)
            elif attribute.attribute_type == Attribute.Type.ENUM_8:
                # length of the attribute
                frame.append(0x01)
                attribute_value = bytearray(attribute.attribute_value)
                for byte in attribute_value:
                    frame.append(byte)

        self.application_frame_generator.send_set_attribute_command_frame(self.id_wireless_bridge, frame, option=option)

    def discovery(self):
        """Generate a get attribute command to discover data of the product"""
        self.get_attribute(self._END_POINT_BASIC_CLUSTER, self._BASIC_CLUSTER, [self._MANUFACTURER_NAME,
                                                                                self._MODEL_IDENTIFIER,
                                                                                self._APPLICATION_FIRMWARE_VERSION_ID,
                                                                                self._APPLICATION_HARDWARE_VERSION_ID,
                                                                                self._PRODUCT_SERIAL_NUMBER_ID,
                                                                                self._PRODUCT_IDENTIFIER_ID,
                                                                                self._PRODUCT_MODEL])

    @abstractmethod
    def update_data(self, payload):
        pass

    @abstractmethod
    def update_rssi(self, lqi, rssi):
        pass

    @abstractmethod
    def get_json_data(self):
        pass

    @abstractmethod
    def get_json_device(self, device_number):
        pass
