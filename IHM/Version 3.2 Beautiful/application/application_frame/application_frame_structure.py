# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to store the structure of applicative frames
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from enum import Enum


class ApplicationInterface(Enum):
    RESERVED = 0x00
    ZIGBEE = 0x01


class ApplicationCommandID(Enum):
    DEFAULT_RESPONSE = 0x00
    UPDATE_DEVICE_DATA = 0x01
    UPDATE_DEVICE_LINK_INDICATOR = 0x02
    CLUSTER_COMMAND = 0x10


class ApplicationOptionMask(Enum):
    MASK_RESPONSE_ENABLE = 0x01
    MASK_SOURCE_ADDRESS_TYPE = 0x06
    MASK_SOURCE_EP_PRESENT = 0x08
    MASK_RESERVED = 0xF0


class ApplicationCommandOptionMask(Enum):
    MASK_RESPONSE_ENABLE = 0x01
    MASK_SOURCE_EP_PRESENT = 0x02
    MASK_DESTINATION_ADDRESS_TYPE = 0x0C
    MASK_DESTINATION_EP_PRESENT = 0x10
    MASK_DIRECTION = 0x20
    MASK_RESERVED = 0xC0


class ApplicationOptionResponseEnable(Enum):
    RESPONSE_DISABLE = 0x00
    RESPONSE_ENABLE = 0x01


class ApplicationOptionAddressType(Enum):
    SOURCE_ID_ADDRESS = 0x00
    IEEE_ADDRESS = 0x01


class ApplicationOptionEPPresent(Enum):
    EP_PRESENT = 0x00
    EP_NOT_PRESENT = 0x01


class ApplicationOptionDirection(Enum):
    CLIENT_TO_SERVER = 0x00
    SERVER_TO_CLIENT = 0x01







