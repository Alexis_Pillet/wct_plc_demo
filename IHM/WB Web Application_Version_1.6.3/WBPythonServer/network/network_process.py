
"""
network.network_process
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to manage the network layout

"""
import types
from queue import Queue, Empty
from threading import Thread
from apscheduler.schedulers.background import BackgroundScheduler
from .network_frame import *
from datetime import datetime, timedelta
from logger import LoggerManager
log = LoggerManager.get_logger(__name__)


class NetworkProcess(object):
    """Network process to send/receive network frames"""

    def __init__(self):
        """Initialization of the network process"""
        # Thread
        self._receiver_alive = None
        self._receiver = None
        # Read/Write management
        self._local_network_sequence_number = 0
        self._external_network_sequence_number = -1
        # Connection to mac process
        self._queue = Queue()
        self._mac_frame_generator = None
        # Send command/data management
        self._application_queue = None
        self._network_frame_generator = None
        self._network_scheduler = BackgroundScheduler()
        self._network_frame_in_progress = []

    @property
    def queue(self):
        return self._queue

    @queue.setter
    def queue(self, queue):
        self._queue = queue

    @property
    def application_queue(self):
        return self._application_queue

    @application_queue.setter
    def application_queue(self, application_queue):
        self._application_queue = application_queue

    @property
    def mac_frame_generator(self):
        return self._mac_frame_generator

    @mac_frame_generator.setter
    def mac_frame_generator(self, mac_frame_generator):
        self._mac_frame_generator = mac_frame_generator

    @property
    def network_frame_generator(self):
        return self._network_frame_generator

    @network_frame_generator.setter
    def network_frame_generator(self, frame_generator):
        self._network_frame_generator = frame_generator

    def _start_receiver(self):
        self._receiver_alive = True
        self._receiver = Thread(target=self._receiver_loop, name='network_rx', args=())
        self._receiver.daemon = True
        self._receiver.start()

    def _stop_receiver(self):
        self._receiver_alive = False

    def start(self):
        """Start receiver thread/scheduler"""
        self._start_receiver()
        self._network_frame_generator = NetworkFrameGenerator(self)
        self._network_scheduler.start()

    def stop(self):
        """Stop receiver thread/scheduler"""
        self._stop_receiver()
        self._receiver.join()
        self._network_scheduler.remove_all_jobs()
        self._network_scheduler.shutdown()

    def _receiver_loop(self):
        """Receiver Thread process"""

        while self._receiver_alive:

            # We are waiting for data coming from mac process
            try:
                data = self._queue.get(True, 0.1)
            except Empty:
                continue

            result = self._check_ack_frame(data["frame"])
            if result["is_ack"]:

                # Check if the ack is not corresponding to a command/data frame sent
                if self._network_scheduler.get_job(str(result["network_sequence_number"])) is None:
                    continue

                # Check this is the good device who sent us the network ack
                if self._network_frame_in_progress[0].device_number != data["device_number"]:
                    continue

                # Remove retry_jobs/metadata since we received the network ack
                self._network_scheduler.remove_job(str(result["network_sequence_number"]))
                self._network_frame_in_progress.pop(0)
                # Check if there are retry_jobs pending => if yes start it
                if len(self._network_frame_in_progress) >= 1:
                    self._network_scheduler.resume_job(self._network_frame_in_progress[0].id_job)
                    # We are adding 10 milliseconds to permit to to PLC concentrator to see the frame
                    self._network_scheduler.modify_job(self._network_frame_in_progress[0].id_job,
                                                       next_run_time=(datetime.now() + timedelta(milliseconds=10)))

                if result["ack_status"] != Network.AckStatus.SUCCESS:
                    log.error('ACK_RECEIVED - {}'.format(Network.display_ack()[result["ack_status"]]))
            else:
                result = self._check_data_or_control_frame(data["frame"])
                if result["is_data_control_frame"]:
                    log.debug('DATA - {}'.format(data["frame"].hex()))
                    self._application_queue.put({"device_number": data["device_number"], "frame": data["frame"][3:]})
                self._send_ack(data["device_number"], result["ack_status_to_send"])
                if result["ack_status_to_send"] == Network.AckStatus.SUCCESS_NO_ACK:
                    log.debug('ACK_SEND - {}'.format(Network.display_ack()[result["ack_status_to_send"]]))
                elif result["ack_status_to_send"] != Network.AckStatus.SUCCESS:
                    log.error('ACK_SEND - {}'.format(Network.display_ack()[result["ack_status_to_send"]]))

            self._queue.task_done()

    def _check_ack_frame(self, frame):
        """Check if the frame is a network ack

        Args:
            frame: data of the frame

        Returns:
            A dict with the result of the control
            "is_ack": If the frame is a network ack or not
            "network_sequence_number": network sequence number of the frame
            "ack_status": Status of the network ack
            example:
                >>> {"is_ack": True, "network_sequence_number": 0, "ack_status": 0}
        """
        result = {"is_ack": False, "network_sequence_number": -1, "ack_status": Network.AckStatus.ERROR_NO_ACK}

        if int(frame[0]) != Network.FrameControl.ACK:
            return result

        # Check if we are waiting an acknowledgment
        if len(self._network_frame_in_progress) == 0:
            return result

        if int(frame[1]) != self._network_frame_in_progress[0].network_sequence_number:
            return result

        # Good frame
        result["is_ack"] = True
        result["network_sequence_number"] = int(frame[1])
        result["ack_status"] = int(frame[2])
        return result

    def _check_data_or_control_frame(self, frame):
        """Check if the data is a data or control frame

        Args:
            frame: data of the frame

        Returns:
            A dict with the result of the control
            "is_data_control_frame": If the frame is a control or data frame
            "ack_status_to_send": status of the ack to send back
            example:
                >>> {"is_data_control_frame": True, "ack_status_to_send": 0x00}
        """
        result = {"is_data_control_frame": False, "ack_status_to_send": Network.AckStatus.ERROR_NO_ACK}

        if int(frame[0]) != Network.FrameControl.DATA:
            if int(frame[0]) == Network.FrameControl.ACK:
                result["ack_status_to_send"] = Network.AckStatus.ERROR_NO_ACK
            else:
                result["ack_status_to_send"] = Network.AckStatus.INVALID_COMMAND
            return result

        # Sequence number must be different of the previous one
        if int(frame[1]) == self._external_network_sequence_number:
            result["ack_status_to_send"] = Network.AckStatus.ALREADY_RX
            return result

        # Reserved bit of the option field must be set to 0
        if (int(frame[2]) & Network.OptionMask.RESERVED) != 0x00:
            result["ack_status_to_send"] = Network.AckStatus.ERROR_NO_ACK
            return result

        # Good frame
        self._external_network_sequence_number = int(frame[1])
        result["is_data_control_frame"] = True
        # Check if the bit "Response Enable" is set or not
        # If this is set, we send a network acknowledgement
        if (int(frame[2]) & Network.OptionMask.RESPONSE) == Network.OptionMask.RESPONSE:
            result["ack_status_to_send"] = Network.AckStatus.SUCCESS
        else:
            result["ack_status_to_send"] = Network.AckStatus.SUCCESS_NO_ACK
        return result

    def _send_ack(self, device_number, result):
        """Send data to mac process to send a network ack

        Args:
            device_number: device number of the WB
            result: ack status to send
        """
        # In some cases we don't want to send a ack
        # Wrong frame or "Response Enable" Option
        if (result != Network.AckStatus.ERROR_NO_ACK) & (result != Network.AckStatus.SUCCESS_NO_ACK):

            frame = bytearray([Network.FrameControl.ACK, self._external_network_sequence_number])
            frame.append(result)
            self._mac_frame_generator.send_data_frame(device_number, frame)

    def send_network_frame(self, frame_control, option, device_number, payload):
        """Send network frame to mac process

        Args:
            frame_control: frame control field
            option: option field
            device_number: device number of the WB
            payload: payload of the frame
        """
        if self._local_network_sequence_number == 255:
            self._local_network_sequence_number = 0
        else:
            self._local_network_sequence_number += 1

        frame = bytearray([frame_control, self._local_network_sequence_number, option])
        for element in payload:
            frame.append(element)

        # Send network frame to mac layout without retry system because we don't want an network acknowledgment
        if (option & Network.OptionMask.RESPONSE) == Network.OptionResponse.DISABLE:
            self._mac_frame_generator.send_data_frame(device_number, frame)
        else:

            # Save metadata of the frame to be able to manage the retry process of it
            metadata_frame = types.SimpleNamespace()
            metadata_frame.network_sequence_number = self._local_network_sequence_number
            metadata_frame.id_job = str(self._local_network_sequence_number)
            metadata_frame.device_number = device_number
            metadata_frame.number_retry = 0
            self._network_frame_in_progress.append(metadata_frame)

            self._network_scheduler.add_job(self._sender, trigger='interval', args=(frame, device_number,), seconds=1.0,
                                            id=str(self._local_network_sequence_number))
            self._network_scheduler.pause_job(metadata_frame.id_job)

            # If there are no retry ob running => start it immediately
            if len(self._network_frame_in_progress) == 1:
                self._network_scheduler.resume_job(self._network_frame_in_progress[0].id_job)
                # We are adding 10 milliseconds to permit to to PLC concentrator to see the frame
                self._network_scheduler.modify_job(self._network_frame_in_progress[0].id_job,
                                                   next_run_time=(datetime.now() + timedelta(milliseconds=10)))

    def _sender(self, frame, device_number):
        """retry job function

        We are waiting 1 second to receive an network ack
        If you don't receive one, we retry to send the frame
        After 3 retry, we raise an error

        Args:
            frame: frame to send to mac process
            device_number: device number of the WB
        """
        try:

            if self._network_frame_in_progress[0].number_retry < 3:
                self._network_frame_in_progress[0].number_retry += 1
                self._mac_frame_generator.send_data_frame(device_number, frame)
            else:
                # Stop the retry process for this frame
                self._network_scheduler.remove_job(self._network_frame_in_progress[0].id_job)
                self._network_frame_in_progress.pop(0)
                # Check if there are retry_jobs pending => if yes start it
                if len(self._network_frame_in_progress) >= 1:
                    self._network_scheduler.resume_job(self._network_frame_in_progress[0].id_job)
                    # We are adding 10 milliseconds to permit to to PLC concentrator to see the frame
                    self._network_scheduler.modify_job(self._network_frame_in_progress[0].id_job,
                                                       next_run_time=(datetime.now() + timedelta(milliseconds=10)))
        except Exception:
            raise
