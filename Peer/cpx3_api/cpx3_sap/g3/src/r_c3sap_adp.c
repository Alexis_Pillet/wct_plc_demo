/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name : r_c3sap_adp.c
 * Description : G3 ADP layer API
 ******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_typedefs.h"
#include "r_c3sap_config.h"
#include "r_c3sap_g3_if.h"
#include "r_c3sap_g3_convert.h"
#include "r_g3_cmd.h"
#include "r_adp_binstruct.h"
#include "r_c3sap_plc_buffsize.h"

/******************************************************************************
Macro definitions
******************************************************************************/
#define R_ADP_CB_BUFF_SIZE  (R_MEMORY_POOL_SIZE_CPX_SAP - R_C3SAP_MAX_REQBIN_SIZE)

/******************************************************************************
Typedef definitions
******************************************************************************/
typedef struct
{
    r_g3_adp_callback_t cb[R_G3_CH_MAX];
    uint8_t *           preq_bin;
    r_adp_cb_str_t *    pcb_str;
    uint16_t            req_buff_size;
    uint16_t            cb_buff_size;
} r_adp_info_t;

/******************************************************************************
Private global variables and functions
******************************************************************************/
static void adp_base_cb_adp_cnf (uint8_t * pbuff);
static void adp_base_cb_adp_ind (uint8_t * pbuff);
static void adp_base_cb_mac_cnf (uint8_t * pbuff);
static void adp_base_cb_mac_ind (uint8_t * pbuff);
static r_result_t adp_check_param (uint8_t ch, uint16_t func_id, uint8_t * preq_buff);
static r_result_t adp_process_request (uint8_t ch, uint16_t func_id, void * preq);
static r_result_t adp_process_request_without_payload (uint8_t ch, uint16_t func_id);

/******************************************************************************
Private global variables and functions
******************************************************************************/
static r_adp_info_t adp_info;

/*===========================================================================*/
/* Functions                                                                 */
/*===========================================================================*/

/******************************************************************************
* Function Name:R_ADP_AdpdDataReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_ADP_AdpdDataReq (uint8_t ch, r_adp_adpd_data_req_t * preq)
{
    r_result_t status;

    status = adp_process_request (ch, R_G3_FUNCID_ADPD_DATA_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_ADP_AdpdDataReq
******************************************************************************/

/******************************************************************************
* Function Name:R_ADP_AdpmResetReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_ADP_AdpmResetReq (uint8_t ch)
{
    r_result_t status;

    status = adp_process_request_without_payload (ch, R_G3_FUNCID_ADPM_RESET_REQ);

    return status;
}
/******************************************************************************
   End of function  R_ADP_AdpmResetReq
******************************************************************************/

/******************************************************************************
* Function Name:R_ADP_AdpmDiscoveryReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_ADP_AdpmDiscoveryReq (uint8_t ch, r_adp_adpm_discovery_req_t * preq)
{
    r_result_t status;

    status = adp_process_request (ch, R_G3_FUNCID_ADPM_DISCOVERY_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_ADP_AdpmDiscoveryReq
******************************************************************************/

/******************************************************************************
* Function Name:R_ADP_AdpmNetworkStartReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_ADP_AdpmNetworkStartReq (uint8_t ch, r_adp_adpm_network_start_req_t * preq)
{
    r_result_t status;

    status = adp_process_request (ch, R_G3_FUNCID_ADPM_NETWORK_START_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_ADP_AdpmNetworkStartReq
******************************************************************************/

/******************************************************************************
* Function Name:R_ADP_AdpmNetworkJoinReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_ADP_AdpmNetworkJoinReq (uint8_t ch, r_adp_adpm_network_join_req_t * preq)
{
    r_result_t status;

    status = adp_process_request (ch, R_G3_FUNCID_ADPM_NETWORK_JOIN_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_ADP_AdpmNetworkJoinReq
******************************************************************************/

/******************************************************************************
* Function Name:R_ADP_AdpmNetworkLeaveReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_ADP_AdpmNetworkLeaveReq (uint8_t ch)
{
    r_result_t status;

    status = adp_process_request_without_payload (ch, R_G3_FUNCID_ADPM_NETWORK_LEAVE_REQ);

    return status;
}
/******************************************************************************
   End of function  R_ADP_AdpmNetworkLeaveReq
******************************************************************************/

/******************************************************************************
* Function Name:R_ADP_AdpmGetReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_ADP_AdpmGetReq (uint8_t ch, r_adp_adpm_get_req_t * preq)
{
    r_result_t status;

    status = adp_process_request (ch, R_G3_FUNCID_ADPM_GET_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_ADP_AdpmGetReq
******************************************************************************/

/******************************************************************************
* Function Name:R_ADP_AdpmSetReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_ADP_AdpmSetReq (uint8_t ch, r_adp_adpm_set_req_t * preq)
{
    r_result_t status;

    status = adp_process_request (ch, R_G3_FUNCID_ADPM_SET_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_ADP_AdpmSetReq
******************************************************************************/

/******************************************************************************
* Function Name:R_ADP_AdpmRouteDiscoveryReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_ADP_AdpmRouteDiscoveryReq (uint8_t ch, r_adp_adpm_route_disc_req_t * preq)
{
    r_result_t status;

    status = adp_process_request (ch, R_G3_FUNCID_ADPM_ROUTE_DISCOVERY_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_ADP_AdpmRouteDiscoveryReq
******************************************************************************/

/******************************************************************************
* Function Name:R_ADP_AdpmPathDiscoveryReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_ADP_AdpmPathDiscoveryReq (uint8_t ch, r_adp_adpm_path_discovery_req_t * preq)
{
    r_result_t status;

    status = adp_process_request (ch, R_G3_FUNCID_ADPM_PATH_DISCOVERY_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_ADP_AdpmPathDiscoveryReq
******************************************************************************/

/******************************************************************************
* Function Name:R_ADP_AdpmLbpReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_ADP_AdpmLbpReq (uint8_t ch, r_adp_adpm_lbp_req_t * preq)
{
    r_result_t status;

    status = adp_process_request (ch, R_G3_FUNCID_ADPM_LBP_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_ADP_AdpmLbpReq
******************************************************************************/

/******************************************************************************
* Function Name:R_ADP_RegistCb
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_ADP_RegistCb (uint8_t ch, r_g3_adp_callback_t * pcallbacks)
{
    if (NULL == pcallbacks)
    {
        return R_RESULT_BAD_INPUT_ARGUMENTS;
    }

    adp_info.cb[ch] = *pcallbacks;

    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_ADP_RegistCb
******************************************************************************/

/******************************************************************************
* Function Name:R_ADP_ClearCb
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_ADP_ClearCb (uint8_t ch)
{
    R_memset (&adp_info.cb[ch], 0, sizeof (r_g3_adp_callback_t));
    
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_ADP_ClearCb
******************************************************************************/

/******************************************************************************
* Function Name:R_ADP_BaseCb
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_ADP_BaseCb (uint8_t * pbuff)
{
    uint8_t ida;
    uint8_t idp;

    if (NULL != pbuff)
    {
        ida = R_G3_GET_PKT_IDA (pbuff[0]);
        idp = R_G3_GET_PKT_IDP (pbuff[0]);

        if (R_G3_UNITID_G3ADP == idp)
        {
            if (R_G3_CMDTYPE_CNF == ida)
            {
                adp_base_cb_adp_cnf (pbuff);
            }
            else if (R_G3_CMDTYPE_IND == ida)
            {
                adp_base_cb_adp_ind (pbuff);
            }
            else
            {
                /* Do Nothing */
            }
        }
        else if (R_G3_UNITID_G3MAC == idp)
        {
            if (R_G3_CMDTYPE_CNF == ida)
            {
                adp_base_cb_mac_cnf (pbuff);
            }
            else if (R_G3_CMDTYPE_IND == ida)
            {
                adp_base_cb_mac_ind (pbuff);
            }
            else
            {
                /* Do Nothing */
            }
        }
        else
        {
            /* Do Nothing */
        }
    }

} /* R_ADP_BaseCb */
/******************************************************************************
   End of function  R_ADP_BaseCb
******************************************************************************/

/******************************************************************************
* Function Name:R_ADP_SapInit
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_ADP_SapInit (uint8_t * preq_bin, uint16_t req_buff_size, void * pcb_str, uint16_t cb_buff_size)
{
    R_memset ((void *)&adp_info, 0, sizeof (r_adp_info_t));

    if ((NULL != preq_bin) && (NULL != pcb_str))
    {
        adp_info.preq_bin      = preq_bin;
        adp_info.pcb_str       = (r_adp_cb_str_t *)pcb_str;
        adp_info.req_buff_size = req_buff_size;
        adp_info.cb_buff_size  = cb_buff_size;
    }
}
/******************************************************************************
   End of function  R_ADP_SapInit
******************************************************************************/

/******************************************************************************
* Function Name:adp_base_cb_adp_cnf
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void adp_base_cb_adp_cnf (uint8_t * pbuff)
{
    uint8_t    idc;
    uint8_t    cmd;
    uint16_t   length;
    uint16_t   max_len = adp_info.cb_buff_size;
    r_result_t status;

    if ((NULL == pbuff) || (NULL == adp_info.pcb_str))
    {
        return;
    }
    idc = R_G3_GET_PKT_IDC (pbuff[0]);
    cmd = pbuff[1];


    switch (cmd)
    {
        case R_G3_CMDID_ADPD_DATA:
            if (NULL != adp_info.cb[idc].R_ADP_AdpdDataCnf)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPD_DATA_CNF, &pbuff[2], max_len, &adp_info.pcb_str->adpd_data_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpdDataCnf (&adp_info.pcb_str->adpd_data_cnf);
                }
            }
            break;

        case R_G3_CMDID_ADPM_RESET:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmResetCnf)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_RESET_CNF, &pbuff[2], max_len, &adp_info.pcb_str->adpm_reset_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmResetCnf (&adp_info.pcb_str->adpm_reset_cnf);
                }
            }
            break;

        case R_G3_CMDID_ADPM_DISCOVERY:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmDiscoveryCnf)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_DISCOVERY_CNF, &pbuff[2], max_len, &adp_info.pcb_str->adpm_discovery_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmDiscoveryCnf (&adp_info.pcb_str->adpm_discovery_cnf);
                }
            }
            break;

        case R_G3_CMDID_ADPM_NETWORK_START:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmNetworkStartCnf)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_NETWORK_START_CNF, &pbuff[2], max_len, &adp_info.pcb_str->adpm_network_start_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmNetworkStartCnf (&adp_info.pcb_str->adpm_network_start_cnf);
                }
            }
            break;

        case R_G3_CMDID_ADPM_NETWORK_JOIN:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmNetworkJoinCnf)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_NETWORK_JOIN_CNF, &pbuff[2], max_len, &adp_info.pcb_str->adpm_network_join_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmNetworkJoinCnf (&adp_info.pcb_str->adpm_network_join_cnf);
                }
            }
            break;

        case R_G3_CMDID_ADPM_NETWORK_LEAVE:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmNetworkLeaveCnf)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_NETWORK_LEAVE_CNF, &pbuff[2], max_len, &adp_info.pcb_str->adpm_network_leave_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmNetworkLeaveCnf (&adp_info.pcb_str->adpm_network_leave_cnf);
                }
            }
            break;

        case R_G3_CMDID_ADPM_GET:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmGetCnf)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_GET_CNF, &pbuff[2], max_len, &adp_info.pcb_str->adpm_get_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmGetCnf (&adp_info.pcb_str->adpm_get_cnf);
                }
            }
            break;

        case R_G3_CMDID_ADPM_SET:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmSetCnf)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_SET_CNF, &pbuff[2], max_len, &adp_info.pcb_str->adpm_set_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmSetCnf (&adp_info.pcb_str->adpm_set_cnf);
                }
            }
            break;

        case R_G3_CMDID_ADPM_ROUTE_DISCOVERY:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmRouteDiscoveryCnf)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_ROUTE_DISCOVERY_CNF, &pbuff[2], max_len, &adp_info.pcb_str->adpm_route_discovery_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmRouteDiscoveryCnf (&adp_info.pcb_str->adpm_route_discovery_cnf);
                }
            }
            break;

        case R_G3_CMDID_ADPM_PATH_DISCOVERY:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmPathDiscoveryCnf)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_PATH_DISCOVERY_CNF, &pbuff[2], max_len, &adp_info.pcb_str->adpm_path_discovery_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmPathDiscoveryCnf (&adp_info.pcb_str->adpm_path_discovery_cnf);
                }
            }
            break;

        case R_G3_CMDID_ADPM_LBP:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmLbpCnf)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_LBP_CNF, &pbuff[2], max_len, &adp_info.pcb_str->adpm_lbp_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmLbpCnf (&adp_info.pcb_str->adpm_lbp_cnf);
                }
            }
            break;

        default:
            break;
    } /* switch */
} /* adp_base_cb_adp_cnf */
/******************************************************************************
   End of function  adp_base_cb_adp_cnf
******************************************************************************/

/******************************************************************************
* Function Name:adp_base_cb_adp_ind
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void adp_base_cb_adp_ind (uint8_t * pbuff)
{
    uint8_t    idc;
    uint8_t    cmd;
    uint16_t   length;
    uint16_t   max_len = adp_info.cb_buff_size;
    r_result_t status;

    if ((NULL == pbuff) || (NULL == adp_info.pcb_str))
    {
        return;
    }

    idc = R_G3_GET_PKT_IDC (pbuff[0]);
    cmd = pbuff[1];

    switch (cmd)
    {
        case R_G3_CMDID_ADPD_DATA:
            if (NULL != adp_info.cb[idc].R_ADP_AdpdDataInd)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPD_DATA_IND, &pbuff[2], max_len, &adp_info.pcb_str->adpd_data_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpdDataInd (&adp_info.pcb_str->adpd_data_ind);
                }
            }
            break;

        case R_G3_CMDID_ADPM_NETWORK_LEAVE:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmNetworkLeaveInd)
            {
                adp_info.cb[idc].R_ADP_AdpmNetworkLeaveInd ();
            }
            break;

        case R_G3_CMDID_ADPM_PATH_DISCOVERY:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmPathDiscoveryInd)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_PATH_DISCOVERY_IND, &pbuff[2], max_len, &adp_info.pcb_str->adpm_path_discovery_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmPathDiscoveryInd (&adp_info.pcb_str->adpm_path_discovery_ind);
                }
            }
            break;

        case R_G3_CMDID_ADPM_LBP:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmLbpInd)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_LBP_IND, &pbuff[2], max_len, &adp_info.pcb_str->adpm_lbp_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmLbpInd (&adp_info.pcb_str->adpm_lbp_ind);
                }
            }
            break;

        case R_G3_CMDID_ADPM_NETWORK_STATUS:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmNetworkStatusInd)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_NETWORK_STATUS_IND, &pbuff[2], max_len, &adp_info.pcb_str->adpm_network_status_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmNetworkStatusInd (&adp_info.pcb_str->adpm_network_status_ind);
                }
            }
            break;

        case R_G3_CMDID_ADPM_BUFFER:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmBufferInd)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_BUFFER_IND, &pbuff[2], max_len, &adp_info.pcb_str->adpm_buffer_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmBufferInd (&adp_info.pcb_str->adpm_buffer_ind);
                }
            }
            break;

        case R_G3_CMDID_ADPM_KEY_STATE:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmKeyStateInd)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_KEY_STATE_IND, &pbuff[2], max_len, &adp_info.pcb_str->adpm_key_state_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmKeyStateInd (&adp_info.pcb_str->adpm_key_state_ind);
                }
            }
            break;

        case R_G3_CMDID_ADPM_ROUTE_ERROR:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmRouteErrorInd)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_ROUTE_ERROR_IND, &pbuff[2], max_len, &adp_info.pcb_str->adpm_route_error_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmRouteErrorInd (&adp_info.pcb_str->adpm_route_error_ind);
                }
            }
            break;

        case R_G3_CMDID_ADPM_EAP_KEY:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmEapKeyInd)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_EAP_KEY_IND, &pbuff[2], max_len, &adp_info.pcb_str->adpm_eap_key_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmEapKeyInd (&adp_info.pcb_str->adpm_eap_key_ind);
                }
            }
            break;

        case R_G3_CMDID_ADPM_FRAME_COUNTER:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmFrameCounterInd)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_FRAME_COUNTER_IND, &pbuff[2], max_len, &adp_info.pcb_str->adpm_frame_counter_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmFrameCounterInd (&adp_info.pcb_str->adpm_frame_counter_ind);
                }
            }
            break;

        case R_G3_CMDID_ADPM_ROUTE_UPDATE:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmRouteUpdateInd)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_ROUTE_UPDATE_IND, &pbuff[2], max_len, &adp_info.pcb_str->adpm_route_update_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmRouteUpdateInd (&adp_info.pcb_str->adpm_route_update_ind);
                }
            }
            break;

        case R_G3_CMDID_ADPM_LOAD_SEQ_NUM:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmLoadSeqNumInd)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_LOAD_SEQ_NUM_IND, &pbuff[2], max_len, &adp_info.pcb_str->adpm_load_seq_num_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmLoadSeqNumInd (&adp_info.pcb_str->adpm_load_seq_num_ind);
                }
            }
            break;

        case R_G3_CMDID_ADPM_RREP:
            if (NULL != adp_info.cb[idc].R_ADP_AdpmRrepInd)
            {
                status = R_ADP_CbBin2Str (R_G3_FUNCID_ADPM_RREP_IND, &pbuff[2], max_len, &adp_info.pcb_str->adpm_rrep_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_ADP_AdpmRrepInd (&adp_info.pcb_str->adpm_rrep_ind);
                }
            }
            break;

        default:
            break;
    } /* switch */
} /* adp_base_cb_adp_ind */
/******************************************************************************
   End of function  adp_base_cb_adp_ind
******************************************************************************/

/******************************************************************************
* Function Name:adp_base_cb_mac_cnf
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void adp_base_cb_mac_cnf (uint8_t * pbuff)
{
    uint8_t    idc;
    uint8_t    cmd;
    uint16_t   length;
    uint16_t   max_len = adp_info.cb_buff_size;
    r_result_t status;

    if ((NULL == pbuff) || (NULL == adp_info.pcb_str))
    {
        return;
    }

    idc = R_G3_GET_PKT_IDC (pbuff[0]);
    cmd = pbuff[1];

    switch (cmd)
    {
        case R_G3_CMDID_MLME_GET:
            if (NULL != adp_info.cb[idc].R_G3MAC_MlmeGetCnf)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_GET_CNF, &pbuff[2], max_len, &adp_info.pcb_str->mlme_get_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_G3MAC_MlmeGetCnf (&adp_info.pcb_str->mlme_get_cnf);
                }
            }
            break;

        case R_G3_CMDID_MLME_SET:
            if (NULL != adp_info.cb[idc].R_G3MAC_MlmeSetCnf)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_SET_CNF, &pbuff[2], max_len, &adp_info.pcb_str->mlme_set_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_G3MAC_MlmeSetCnf (&adp_info.pcb_str->mlme_set_cnf);
                }
            }
            break;

        default:
            break;
    } /* switch */
} /* adp_base_cb_mac_cnf */
/******************************************************************************
   End of function  adp_base_cb_mac_cnf
******************************************************************************/

/******************************************************************************
* Function Name:adp_base_cb_mac_ind
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void adp_base_cb_mac_ind (uint8_t * pbuff)
{
    uint8_t    idc;
    uint8_t    cmd;
    uint16_t   length;
    uint16_t   max_len = adp_info.cb_buff_size;
    r_result_t status;

    if ((NULL == pbuff) || (NULL == adp_info.pcb_str))
    {
        return;
    }
    idc = R_G3_GET_PKT_IDC (pbuff[0]);
    cmd = pbuff[1];

    switch (cmd)
    {
        case R_G3_CMDID_MLME_TMR_RECEIVE:
        {
            if (NULL != adp_info.cb[idc].R_G3MAC_MlmeTmrReceiveInd)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_TMR_RECEIVE_IND, &pbuff[2], max_len, &adp_info.pcb_str->mlme_tmr_receve_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_G3MAC_MlmeTmrReceiveInd (&adp_info.pcb_str->mlme_tmr_receve_ind);
                }
            }
            break;
        }

        case R_G3_CMDID_MLME_TMR_TRANSMIT:
        {
            if (NULL != adp_info.cb[idc].R_G3MAC_MlmeTmrTransmitInd)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_TMR_TRANSMIT_IND, &pbuff[2], max_len, &adp_info.pcb_str->mlme_tmr_transmit_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_G3MAC_MlmeTmrTransmitInd (&adp_info.pcb_str->mlme_tmr_transmit_ind);
                }
            }
            break;
        }

        case R_G3_CMDID_MLME_BEACON_NOTIFY:
        {
            if (NULL != adp_info.cb[idc].R_G3MAC_MlmeBeaconNotifyInd)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_BEACON_NOTIFY_IND, &pbuff[2], max_len, &adp_info.pcb_str->mlme_beacon_notify_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    adp_info.cb[idc].R_G3MAC_MlmeBeaconNotifyInd (&adp_info.pcb_str->mlme_beacon_notify_ind);
                }
            }
            break;
        }

        default:
            break;
    } /* switch */
} /* adp_base_cb_mac_ind */
/******************************************************************************
   End of function  adp_base_cb_mac_ind
******************************************************************************/

/******************************************************************************
* Function Name:adp_check_param
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t adp_check_param (uint8_t ch, uint16_t func_id, uint8_t * preq_buff)
{
    uint8_t g3mode = R_G3CTRL_GetG3Mode (ch);

    if (R_G3_CH_MAX  <= ch)
    {
        return R_RESULT_BAD_INPUT_ARGUMENTS;
    }

    if (R_G3_FUNCID_ADPM_LBP_REQ == func_id)
    {
        if (R_G3_UNITID_G3ADP != g3mode)
        {
            return R_RESULT_INVALID_REQUEST;
        }
    }
    else
    {
        if ((R_G3_UNITID_G3EAP != g3mode) && (R_G3_UNITID_G3ADP != g3mode))
        {
            return R_RESULT_INVALID_REQUEST;
        }
    }
    if (NULL == preq_buff)
    {
        return R_RESULT_INVALID_REQUEST;
    }

    return R_RESULT_SUCCESS;
} /* adp_check_param */
/******************************************************************************
   End of function  adp_check_param
******************************************************************************/

/******************************************************************************
* Function Name:adp_process_request
* Description : Main function for the processing of ADP requests. All commands
*               and functions are processed in the same way.
* Arguments : uint8_t ch: The channel used for communication
*             uint16_t func_id: The function ID indicating the request (e.g. R_G3_FUNCID_ADPD_DATA_REQ)
*             void *preq: A generic pointer containing information about the specific request
* Return Value : - R_RESULT_BAD_INPUT_ARGUMENTS if the arguments are wrong or NULL
*                - R_RESULT_SUCCESS if the conversion performed by R_ADP_ReqStr2Bin and
*                  sending of the command succeeds.
*                - R_RESULT_FAILED otherwise.
******************************************************************************/
static r_result_t adp_process_request (uint8_t ch, uint16_t func_id,  void * preq)
{
    uint8_t *  preq_buff = adp_info.preq_bin;
    uint16_t   length    = 0u;
    uint16_t   max_len   = adp_info.req_buff_size - 2u;
    uint8_t    cmd       = (uint8_t)(func_id & 0xFFu);
    r_result_t status;

    status = adp_check_param (ch, func_id, preq_buff);
    if (R_RESULT_SUCCESS != status)
    {
        return status;
    }
    if (NULL == preq)
    {
        return R_RESULT_BAD_INPUT_ARGUMENTS;
    }

    status = R_G3CTRL_SemaphoreWait ();
    if (R_RESULT_SUCCESS == status)
    {
        status = R_ADP_ReqStr2Bin (func_id, preq, max_len, &preq_buff[2], &length);

        if (R_RESULT_SUCCESS == status)
        {
            preq_buff[0] = R_G3_SET_PKT_HEAD (ch, R_G3_CMDTYPE_REQ, R_G3_UNITID_G3ADP);
            preq_buff[1] = cmd;

            status       = R_G3CTRL_SendCmd (preq_buff, length + 2u);
        }

        R_G3CTRL_SemaphoreRelease ();
    }

    return status;
} /* adp_process_request */
/******************************************************************************
   End of function  adp_process_request
******************************************************************************/

/******************************************************************************
* Function Name:adp_process_request_without_payload
* Description : Main function for the processing of ADP requests. All commands
*               and functions are processed in the same way.
* Arguments : uint8_t ch: The channel used for communication
*             uint16_t func_id: The function ID indicating the request (e.g. R_G3_FUNCID_ADPD_DATA_REQ)
* Return Value : - R_RESULT_SUCCESS if the sending of the command succeeds.
*                - R_RESULT_FAILED otherwise.
******************************************************************************/
static r_result_t adp_process_request_without_payload (uint8_t ch, uint16_t func_id)
{
    uint8_t *  preq_buff = adp_info.preq_bin;
    uint8_t    cmd       = (uint8_t)(func_id & 0xFFu);
    r_result_t status;

    /* This function shall only be called for reset and leave. */
    if ((R_G3_FUNCID_ADPM_RESET_REQ != func_id) &&
        (R_G3_FUNCID_ADPM_NETWORK_LEAVE_REQ != func_id))
    {
        return R_RESULT_BAD_INPUT_ARGUMENTS;
    }

    status = adp_check_param (ch, func_id, preq_buff);
    if (R_RESULT_SUCCESS != status)
    {
        return status;
    }

    status = R_G3CTRL_SemaphoreWait ();
    if (R_RESULT_SUCCESS == status)
    {
        preq_buff[0] = R_G3_SET_PKT_HEAD (ch, R_G3_CMDTYPE_REQ, R_G3_UNITID_G3ADP);
        preq_buff[1] = cmd;

        status       = R_G3CTRL_SendCmd (preq_buff, 2u);

        R_G3CTRL_SemaphoreRelease ();
    }

    return status;
} /* adp_process_request_without_payload */
/******************************************************************************
   End of function  adp_process_request_without_payload
******************************************************************************/

