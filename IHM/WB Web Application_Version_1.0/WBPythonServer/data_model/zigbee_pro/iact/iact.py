
"""
network.network_process
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to control/model iACT Device with one ENd Point

"""
from data_model.zigbee_pro.zigbee_pro_device import ZigBeeProDevice


class IACT(ZigBeeProDevice):
    """Model IACT device with one End Point"""

    def __init__(self, address, id_wireless_bridge, application_frame_generator):
        """Initialization of the IACT device"""
        self.STATUS_OPEN = True
        self.STATUS_CLOSE = False
        self._ON_OFF_CLUSTER = ([0x00, 0x06])
        self._END_POINT = 0x01
        self._COMMAND_ID_OFF = 0x00
        self._COMMAND_ID_ON = 0x01
        self._status = self.STATUS_OPEN
        # call super function
        super(IACT, self).__init__(address, id_wireless_bridge, application_frame_generator)

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        self._status = status

    def update_data(self, data):
        """Update the attributes of the device according to the value specified

        Args:
            data: dict of the value received
        """
        pass

    def open(self):
        """Send a Open cluster command"""
        frame = bytearray.fromhex(self.address)
        frame.append(self._END_POINT)
        for byte in self._ON_OFF_CLUSTER:
            frame.append(byte)
        frame.append(self._COMMAND_ID_ON)
        self.application_frame_generator.send_cluster_command_frame(self.id_wireless_bridge, frame)

    def close(self):
        """Send a Close cluster command"""
        frame = bytearray.fromhex(self.address)
        frame.append(self._END_POINT)
        for byte in self._ON_OFF_CLUSTER:
            frame.append(byte)
        frame.append(self._COMMAND_ID_OFF)
        self.application_frame_generator.send_cluster_command_frame(self.id_wireless_bridge, frame)
