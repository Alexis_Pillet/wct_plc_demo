# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to model ZigBee Pro Device
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from abc import abstractmethod
from data_model.zigbee_device import ZigBeeDevice


class ZigBeeProDevice(ZigBeeDevice):

    def __init__(self, address, id_wireless_bridge, application_frame_generator):
        # call super function
        super(ZigBeeProDevice, self).__init__(address, id_wireless_bridge, application_frame_generator)

    def read_attribute(self, cluster_id, attribute_id):
        pass

    def write_attribute(self, cluster_id, attribute_id, value):
        pass

    def discovery(self):
        pass

    @abstractmethod
    def update_data(self, payload):
        pass

    @abstractmethod
    def get_json_data(self):
        pass

    @abstractmethod
    def get_json_device(self, device_number):
        pass
