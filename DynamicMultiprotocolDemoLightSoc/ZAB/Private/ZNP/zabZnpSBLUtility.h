/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the header for Serial Boot Loader state machine for the ZAB Pro ZNP
 *   vendor.
 *   For details and flow charts see XXX.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.02.00  18-Oct-13   MvdB   Upgraded from old file
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
 * 002.002.024  28-Jun-16   MvdB   ARTF172101: Implement ZNP ping to handle lost Reset indication when hidden inside a serial command that was not completed before reset
 *****************************************************************************/

#ifndef _ZAB_ZNP_SBL_UTILITY_H_
#define _ZAB_ZNP_SBL_UTILITY_H_

#include "zabZnpSBLUtilityTypes.h"
#include "sapTypes.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 * Create
 * Initialises SBL state machine
 ******************************************************************************/
extern 
void zabZnpSblUtility_Create(erStatus* Status, zabService* Service);

/******************************************************************************
 * Update timeout
 * Check for timeouts within the state machine
 ******************************************************************************/
extern 
unsigned32 zabZnpSblUtility_UpdateTimeout(erStatus* Status, zabService* Service, unsigned32 Time);
  
/******************************************************************************
 * Action Handler
 * Handles Update action for the SBL state machines
 ******************************************************************************/
extern 
void zabZnpSblUtility_Action(erStatus* Status, zabService* Service, unsigned8 Action);

/******************************************************************************
 * Process all incoming messages of subsystem SBL
 ******************************************************************************/
extern 
void zabZnpSBLUtility_ProcessMsg(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * Send an SBL Application Version Request
 ******************************************************************************/
extern 
void zabZnpSblUtility_AppVersionReq(erStatus* Status, zabService* Service);

/******************************************************************************
 * Send an SBL Application Start Request
 ******************************************************************************/
extern 
void zabZnpSblUtility_AppStartReq(erStatus* Status, zabService* Service);

/******************************************************************************
 * Confirm if a version of bootloader is supported by this utility
 * Note: 0.0.0 indicates no response, which is a TI standard bootloader if it has
 *       previously responded to a handshake
 ******************************************************************************/
extern
zab_bool zabZnpSblUtility_CompatibilityCheck(unsigned8 MajorVersion,
                                            unsigned8 MinorVersion,
                                            unsigned8 ReleaseVersion);                               

/******************************************************************************
 * Confirm if an update is in progress
 ******************************************************************************/
extern
zab_bool zabZnpSblUtility_UpdateActive(zabService* Service);  

#ifdef __cplusplus
}
#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif /* _ZAB_ZNP_SBL_UTILITY_H_ */
