/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the Thermostat Cluster.
 *
 *   It supports:
 *    - Multiple, application specified endpoints
 *    - Attributes required for WiserOne testing
 *
 *   WARNING: It does not (currently) support:
 *    - All mandatory attributes
 *    - Optional Attributes
 *    - Commands
 *    - Non-volatile backing of any data.
 *
 * USAGE:
 *   Usage of this plugin is very simple:
 *    1. Initialise it with SzlPlugin_ThermostatClusterInit(), specifying the endpoints and reead/write function pointers
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 * 002.002.038  16-Jan-17   MvdB   Original
 *****************************************************************************/

#include <stdio.h>
#include <stddef.h>
#include <stddef.h>
#include <string.h>
#include "zabCoreService.h"
#include "zabCorePrivate.h"
#include "szl.h"
#include "szl_plugin.h"
#include "szl_report.h"

#include "thermostat_cluster.h"

/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Cluster ID */
#define ZCL_CLUSTER_ID_THERMOSTAT     0x0201


/* Parameters for the service */
typedef struct
{
  szl_uint8 numEndpoints;
  szl_uint8 Endpoints[VLA_INIT];
}SzlPlugin_Thermostat_Params_t;

#define SzlPlugin_Thermostat_Params_t_SIZE(numEndpoints) (sizeof(SzlPlugin_Thermostat_Params_t) - (VLA_INIT * sizeof(szl_uint8))+ (sizeof(szl_uint8) * (numEndpoints)))


/*
 * The set of attributes for the cluster.
 * This const data is used to initialise the majority of the attribute table.
 * Once it is copied into RAM, Endpoint and Data Access function pointers are set for each attribute.
 *
 * New attributes must be added here.
 */
const SZL_DataPoint_t thermostatDatapointConfig[] = {
  {ZCL_CLUSTER_ID_THERMOSTAT, ATTRID_THERMOSTAT_LOCAL_TEMP,               0, SZL_ZB_DATATYPE_INT16, 2,  {DP_METHOD_DIRECT, DP_ACCESS_READ,  szl_false, DP_DIRECTION_SERVER}, {.Callback = {NULL, NULL}}},
  {ZCL_CLUSTER_ID_THERMOSTAT, ATTRID_THERMOSTAT_OCCUPIED_HEATING_SETPOINT,0, SZL_ZB_DATATYPE_INT16, 2,  {DP_METHOD_DIRECT, DP_ACCESS_RW,    szl_false, DP_DIRECTION_SERVER}, {.Callback = {NULL, NULL}}}
};
#define THERMOSTAT_M_NUM_ATTRIBUTES_PER_ENDPOINT ( sizeof(thermostatDatapointConfig) / sizeof(SZL_DataPoint_t))




/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/




/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Plugin Initialisation
 * This function must be called from the application after the Library has been initialized.
 *
 * Parameters:
 *  Service: Instance of library
 *  numEndpoints: The length of endpointList
 *  endpointList: The endpoints to be initialised with this cluster
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_ThermostatClusterInit(zabService* Service,
                                             szl_uint8 numEndpoints,
                                             szl_uint8 * endpointList,
                                             SZL_CB_DataPointRead_t ReadCallback,
                                             SZL_CB_DataPointWrite_t WriteCallback)
{
    SZL_RESULT_t result;
    szl_uint8 epIndex;
    SzlPlugin_Thermostat_Params_t* pluginParams = NULL;
    szl_uint8 attr;
    SZL_DataPoint_t serverAttributes[THERMOSTAT_M_NUM_ATTRIBUTES_PER_ENDPOINT];
    szl_bool anyRegisterWorked;

    /* Validate inputs */
    if ( (Service == NULL) || (numEndpoints == 0) || (endpointList == NULL)
          || (ReadCallback == NULL) || (WriteCallback == NULL) )
      {
        return SZL_RESULT_INVALID_DATA;
      }

    /* Malloc parameters the plugin needs to store and link from the SZL */
    result = SZL_PluginRegister(Service, SZL_PLUGIN_ID_THERMOSTAT, SzlPlugin_Thermostat_Params_t_SIZE(numEndpoints), (void**)&pluginParams);
    if ( (result == SZL_RESULT_SUCCESS) && (pluginParams != NULL) )
      {
        /* Set parameters that are not per endpoint */
        pluginParams->numEndpoints = numEndpoints;

        /* Init table of attributes to be registered for each endpoint
         * This is mostly the same for each EP, but we set the endpoint number and data pointers differently */
        anyRegisterWorked = szl_false;
        result = SZL_RESULT_SUCCESS;
        for (epIndex = 0;
             (epIndex < numEndpoints) && (result == SZL_RESULT_SUCCESS || result == SZL_RESULT_ALREADY_EXISTS);
             epIndex++)
          {

            /* Init table of attributes to be registered for each endpoint
             * This is mostly the same for each EP, but we set the endpoint number and data pointers differently */
            szl_memcpy(serverAttributes,
                       thermostatDatapointConfig,
                       sizeof(thermostatDatapointConfig));

            /* Initialise endpoint number in data */
            pluginParams->Endpoints[epIndex] = endpointList[epIndex];

            /* Set the data read/write function pointers */
            for (attr = 0; attr < THERMOSTAT_M_NUM_ATTRIBUTES_PER_ENDPOINT; attr++)
              {
                serverAttributes[attr].Endpoint = endpointList[epIndex];

                serverAttributes[attr].Property.Method = DP_METHOD_CALLBACK;
                serverAttributes[attr].AccessType.Callback.Read = ReadCallback;
                serverAttributes[attr].AccessType.Callback.Write = WriteCallback;
              }

            /* Register the cluster attributes */
            result = SZL_AttributesRegister(Service, serverAttributes, THERMOSTAT_M_NUM_ATTRIBUTES_PER_ENDPOINT);

            /* Keep boolean to show if any register worked, as if it did we must ensure the data it points to is valid */
            if(result == SZL_RESULT_SUCCESS || result == SZL_RESULT_ALREADY_EXISTS)
              {
                anyRegisterWorked = szl_true;
              }
          }

        /* If any register worked, we must keep Params to ensure data pointers are valid. If all failed we can free */
        if(anyRegisterWorked == szl_true)
          {
            printInfo(Service, "PLUGIN THERMOSTAT: Init Successful\n");
            return SZL_RESULT_SUCCESS;
          }
        else
          {
            /* Failed. Cleanup */
            SZL_PluginUnregister(Service, SZL_PLUGIN_ID_THERMOSTAT);
            result = SZL_RESULT_FAILED;
          }
      }

    return result;
}

/******************************************************************************
 * Plugin Destruction
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_ThermostatCluster_Destroy(zabService* Service)
{
  SZL_RESULT_t result;
  szl_uint8 epIndex;
  szl_uint8 attr;
  SZL_EP_t endpoint;
  SzlPlugin_Thermostat_Params_t* pluginParams = NULL;
  SZL_DataPoint_t serverAttributes[THERMOSTAT_M_NUM_ATTRIBUTES_PER_ENDPOINT];

  /* Validate inputs */
  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_THERMOSTAT, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }

  for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
    {
      /* Get the endpoint number */
      endpoint = pluginParams->Endpoints[epIndex];

      /* Init table of attributes to be de-registered for each endpoint
       * This is mostly the same for each EP, but we set the endpoint number differently */
      szl_memcpy(serverAttributes,
                 thermostatDatapointConfig,
                 sizeof(thermostatDatapointConfig));

      /* Set the endpoint number. For deregister we don't care about the access types etc. */
      for (attr = 0; attr < THERMOSTAT_M_NUM_ATTRIBUTES_PER_ENDPOINT; attr++)
        {
          serverAttributes[attr].Endpoint = endpoint;
        }

      /* De-Register the cluster attributes.
       * Don't care too much about the result as we will make best effort only */
      result = SZL_AttributesDeregister(Service, serverAttributes, THERMOSTAT_M_NUM_ATTRIBUTES_PER_ENDPOINT);
      if (result != SZL_RESULT_SUCCESS)
        {
          printError(Service, "PLUGIN THERMOSTAT: WARNING: Deregister failed for EP 0x%02X with Result 0x%02X\n",
                     endpoint,
                     result);
        }
    }

  /* Now unregister the plugin */
  return SZL_PluginUnregister(Service, SZL_PLUGIN_ID_THERMOSTAT);
}


/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/