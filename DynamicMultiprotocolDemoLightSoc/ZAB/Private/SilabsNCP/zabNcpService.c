/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the main interface for the ZAB EZSP Vendor - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.005  01-Jul-16   MvdB   Support mandatory ZDO client commands for MiGenie:
 *                                  - SZL_ZDO_MgmtLqiReq, SZL_ZDO_MgmtBindReq, SZL_ZDO_BindReq, SZL_ZDO_UnBindReq,
 *                                  - SZL_NwkLeaveReq, SZL_ZDO_NwkAddrReq, SZL_ZDO_PowerDescriptorReq ,SZL_ZDO_NodeDescriptorReq
 * 000.000.013  21-Jul-16   MvdB   Support Network Discover and Join
 * 000.000.014  22-Jul-16   MvdB   Support ZAB_ACTION_SET_TX_POWER
 * 000.000.015  25-Jul-16   MvdB   Support multiple endpoints
 * 000.000.016  25-Jul-16   MvdB   Support ZAB_ACTION_CHANGE_CHANNEL and other a-synchronous network parameter changes
 * 000.000.020  27-Jul-16   MvdB   Add network processor ping
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 * 002.002.040  17-Jan-17   MvdB   ARTF175875: Support retaining message in output queue for via bind
 *****************************************************************************/

#include <stdint.h>

#include "ember-types.h"
#include "zabCoreService.h"
#include "zabNcpPrivate.h"
#include "zabNcpAsh.h"
#include "zabNcpEzsp.h"
#include "zabNcpEzspConfiguration.h"
#include "zabNcpNwk.h"
#include "zabNcpOpen.h"
#include "zabNcpZdo.h"
#include "zabNcpAf.h"
#include "zabNcpXModem.h"
#include "zabNcpSbl.h"

#include "zabNcpService.h"
#include "zabSerialServicePrivate.h"


#include "ezsp-protocol.h"
#include "ezsp-enum-decode.h"


/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 * Give the Vendor Version to the application
 ******************************************************************************/
static void giveBackVendorVersionInfo(erStatus* Status, zabService* Service)
{
  unsigned8 vendorVersion[4] = {ZAB_VENDOR_VERSION_MAJOR, ZAB_VENDOR_VERSION_MINOR, ZAB_VENDOR_VERSION_RELEASE, ZAB_VENDOR_VERSION_BUILD};
  ER_CHECK_STATUS(Status);

  srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_VENDOR_VERSION, sizeof(vendorVersion), vendorVersion);
  srvCoreGiveBack16(Status, Service, ZAB_GIVE_VENDOR_TYPE, ZAB_VENDOR_TYPE_SILABS_EZSP);
}

/**
 *  @brief Calculates 16-bit cyclic redundancy code (CITT CRC 16).
 *
 * Applies the standard CITT CRC 16 polynomial to a
 * single byte. It should support being called first with an initial
 * value, then repeatedly until all data is processed.
 *
 * @param newByte     The new byte to be run through CRC.
 *
 * @param prevResult  The previous CRC result.
 *
 * @return The new CRC result.
 */
static unsigned16 zabNcpService_Crc16(unsigned8 newByte, unsigned16 prevResult)
{
  prevResult = (prevResult >> 8) | (prevResult << 8);
  prevResult ^= newByte;
  prevResult ^= (prevResult & 0xff) >> 4;
  prevResult ^= (prevResult << 8) << 4;

  prevResult ^= ( (unsigned8) ( (unsigned8) ( (unsigned8) (prevResult & 0xff) ) << 5)) |
    ((unsigned16) ( (unsigned8) ( (unsigned8) (prevResult & 0xff)) >> 3) << 8);

  return prevResult;
}

/******************************************************************************
 * Calculate a CRC16 across a buffer with supplied seed
 ******************************************************************************/
unsigned16 zabNcpService_CalculateCrc16(unsigned16 seed, unsigned8 Length, unsigned8* Buffer)
{
  unsigned16 crc = seed;
  unsigned8 index;

  if (Buffer != NULL)
    {
      for (index = 0; index < Length; index++)
        {
          crc = zabNcpService_Crc16(Buffer[index], crc);
        }
    }
  return crc;
}


/******************************************************************************
 * Vendor In Work
 * Checks the VendorInQ from the serial SAP.
 * Process messages and notifications and forwards them to application and state machines
 ******************************************************************************/
static unsigned32 LocalWorkIn(erStatus* Status, zabService* Service)
{
  sapMsg* Msg;
  sapMsgType Type;
  zabMsgAppType App;
  unsigned8 Count = qSafeCount(&ZAB_SRV(Service)->VendorInQ);

  while ( (Count > 0) && (erStatusIsOk(Status)) )
    {

      Msg = (sapMsg*) qSafeRemove(&ZAB_SRV(Service)->VendorInQ);
      if (Msg == NULL)
        {
          erStatusSet( Status, SAP_ERROR_MSG_NOT_AVAILABLE );
          return 0;
        }

      Type = sapMsgGetType(Msg);
      if (Type == SAP_MSG_TYPE_APP)
        {
          App = sapMsgGetAppType(Msg);
          if (App == ZAB_MSG_APP_EZSP_ASH_CRC)
            {
              /* Process incoming ASH messages */
              zabNcpAsh_ProcessMessageIn(Status, Service, Msg);
            }
          else if (App == ZAB_MSG_APP_EZSP_XMODEM)
            {
              /* Process incoming ASH messages */
              zabNcpXModem_ProcessMessageIn(Status, Service, Msg);
            }
          else
            {
              printError(Service, "LocalWorkIn: WARNING: Unknown IN application type = 0x%X\n", (unsigned8)App);
              erStatusSet(Status, ZAB_ERROR_MSG_APP_INVALID);
            }
        }
      else if (Type == SAP_MSG_TYPE_NOTIFY)
        {
          if (sapMsgGetNotifyType(Msg) == ZAB_NOTIFICATION_OPEN_STATE)
            {
              zabNcpOpen_InNotificationHandler(Status, Service, sapMsgGetNotifyData(Msg).openState);
            }
          else
            {
              printError(Service, "LocalWorkIn: WARNING: Unknown IN notification type = 0x%X\n", (unsigned8)sapMsgGetNotifyType(Msg));
            }
        }
      else
        {
          printError(Service, "LocalWorkIn: WARNING: Unknown IN message type = 0x%X\n", (unsigned8)Type);
          erStatusSet(Status, ZAB_ERROR_MSG_TYPE_INVALID);
        }

      sapMsgFree(Status, Msg);
      Count--;
    }

  /* If we successfully cleared all messages then return max delay.
   * If not, request more work ASAP */
  if (Count == 0)
    {
      return ZAB_WORK_DELAY_MAX_MS;
    }
  return 0;
}

/******************************************************************************
 * Vendor Out Work
 * Process messages on the VendorOutQ and send commands and action to the
 * network processor and serial glue.
 ******************************************************************************/
static unsigned32 LocalWorkOut(erStatus* Status, zabService* Service)
{
  unsigned32 timeoutRemaining = ZAB_WORK_DELAY_MAX_MS;
  sapMsg *Msg;
  sapMsgType Type;
  zabAction Action;
  zab_bool retainMessage;

  unsigned8 Count = qSafeCount(&ZAB_SRV(Service)->VendorOutQ);

  /* Process outwards messages until queue is empty or ZNP is busy */
  while ( (Count > 0) &&
          erStatusIsOk(Status) &&
          (zabNcpEzsp_GetSerialLinkLocked(Service) == zab_false) )
    {
      // These actions or commands will have done something, so get the next work done quickly to correctly calculate real next timeout
      timeoutRemaining = 0;

      Msg = (sapMsg*) qSafeRemove(&ZAB_SRV(Service)->VendorOutQ);
      if (Msg == NULL )
        {
          printError(Service, "LocalWorkOut: ERROR: Null message\n");
          break;
        }

      /* Normally we will not retain the message on the queue unless in a particular case for DataReq via multiple binds*/
      retainMessage = zab_false;

      Type = sapMsgGetType(Msg);
      if (Type == SAP_MSG_TYPE_ACTION)
        {
          Action = sapMsgGetAction(Msg);

          switch(Action)
            {
              case ZAB_ACTION_OPEN:
              case ZAB_ACTION_CLOSE:
              case ZAB_ACTION_GO_TO_FIRMWARE_UPDATER:
                zabNcpOpen_Action(Status, Service, Action);
                break;

              case ZAB_ACTION_NWK_INIT:
              case ZAB_ACTION_NWK_DISCOVER:
              case ZAB_ACTION_NWK_STEER:
              case ZAB_ACTION_NWK_GET_INFO:
              case ZAB_ACTION_NWK_PERMIT_JOIN_ENABLE:
              case ZAB_ACTION_NWK_PERMIT_JOIN_DISABLE:
              case ZAB_ACTION_SET_TX_POWER:
              case ZAB_ACTION_CHANGE_CHANNEL:
              case ZAB_ACTION_CHANGE_NETWORK_KEY:
                zabNcpNwk_Action(Status, Service, Action);
                break;

              case ZAB_ACTION_EXIT_FIRMWARE_UPDATER:
              case ZAB_ACTION_UPDATE_FIRMWARE:
                zabNcpSbl_Action(Status, Service, Action);
                break;

              /* Do nothing actions */
              case ZAB_ACTION_EZ_MODE_NWK_STEER:
                break;

              default:
                printError(Service, "LocalWorkOut: ERROR: Unhandled action 0x%02X\n", (unsigned8)Action);
            }
        }
      else if (Type == SAP_MSG_TYPE_APP)
        {
          /* Application messages need to be converted from the ZAB internal format into the vendor format */
          switch (sapMsgGetAppType(Msg))
            {
              case ZAB_MSG_APP_AF_DATA_OUT:                 retainMessage = zabNcpAf_DataRequest(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_IEEE_REQ:                zabNcpZdo_IeeeAddressReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_NWK_REQ:                 zabNcpZdo_NwkAddressReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_NODE_DESC_REQ:           zabNcpZdo_NodeDescReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_POWER_DESC_REQ:          zabNcpZdo_PowerDescReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_ACTIVE_EP_REQ:           zabNcpZdo_ActiveEndpointReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_SIMPLE_DESC_REQ:         zabNcpZdo_SimpleDescReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_MATCH_DESC_REQ:          zabNcpZdo_MatchDescReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_BIND_REQ:                zabNcpZdo_BindReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_UNBIND_REQ:              zabNcpZdo_UnbindReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_MGMT_LQI_REQ:            zabNcpZdo_MgmtLqiReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_MGMT_BIND_REQ:           zabNcpZdo_MgmtBindReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_MGMT_RTG_REQ:            zabNcpZdo_MgmtRtgReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_MGMT_LEAVE_REQ:          zabNcpZdo_MgmtLeaveReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_MGMT_NWK_UPDATE_REQ:     zabNcpZdo_MgmtNwkUpdateReqConversion(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_USER_DESC_REQ:           zabNcpZdo_UserDescReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_ZDO_USER_DESC_SET_REQ:       zabNcpZdo_UserDescSetReq(Status, Service, Msg); break;
              case ZAB_MSG_APP_REGISTER_REQ:                zabNcpEzspConfiguration_AddEndpoint(Status, Service, Msg); break;
              default:
                printError(Service, "LocalWorkOut: ERROR - Unknown app message type 0x%02X\n", sapMsgGetAppType(Msg));
            }
        }

      if (retainMessage == zab_true)
        {
          /* Push the item back into the front of the queue */
          qSafePush(&ZAB_SRV(Service)->VendorOutQ, (void*)Msg);
        }
      else
        {
          /* Free the message and decrement count before moving onto next item*/
          sapMsgFree(Status, Msg);
          Count--;
        }
    }
  return timeoutRemaining;
}

/******************************************************************************
 * This function receives serial port data.
 * It is registered as the IN handler with the serial SAP.
 ******************************************************************************/
static void SerialToVendorIn(erStatus* Status, sapHandle Sap, sapMsg* Message)
{
  erStatus localStatus;
  zabServiceStruct* This = ZAB_SRV( sapCoreGetService( Sap ) );

  erStatusClear(&localStatus, This);

  ER_CHECK_NULL(Status, Message);

  sapMsgUse(&localStatus, Message); // must do this because we are storing it on a queue

  if (erStatusIsError(&localStatus))
    {
      erStatusSet(Status, erStatusGetError(&localStatus));
    }
  else
    {
      qSafeAppend(&This->VendorInQ, Message);           // just put it on the queue
    }
}


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                 *****         VENDOR PUBLIC        *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Create an instance of ZAB Vendor
 ******************************************************************************/
void zabVendorService_Create(erStatus* Status, zabService* Service)
{
  zabServiceStruct* This = ZAB_SRV( Service );
  unsigned8 MutexId;

  This->Vendor = OS_MEM_MALLOC(Status,
                               srvCoreGetServiceId(Status, Service),
                               sizeof(zabVendorServiceStruct),
                               MALLOC_ID_CFG_VENDOR);
  if (This->Vendor == NULL)
    {
      return;
    }

  MutexId = ZAB_PROTECT_CREATE(Service);
  This->SerialSAP = sapCoreCreate(Status,
                                  Service,
                                  srvCoreAskBack8(Status, Service, ZAB_ASK_SAP_SERIAL_MAX, 0) + 2 ,
                                  MutexId);
  sapCoreRegisterCallBack(Status, zabCoreSapSerial(This), SAP_MSG_DIRECTION_IN, SerialToVendorIn);
  sapCoreRegisterCallBack(Status, zabCoreSapSerial(This), SAP_MSG_DIRECTION_OUT, zabSerialService_OutHandler);
  sapMsgRegAllocate(Status, zabCoreSapSerial(This), zabCoreAllocateApp, zabCoreFreeApp);

  zabVendorService_Reset(Status, Service);

  giveBackVendorVersionInfo(Status, Service);
}

/******************************************************************************
 * Reset an instance of ZAB Vendor
 ******************************************************************************/
void zabVendorService_Reset(erStatus* Status, zabService* Service)
{
  printVendor(Service, "zabVendorService_Reset\n");
  zabNcpAsh_InitService(Status, Service);
  zabNcpEzsp_InitService(Status, Service);
  ZAB_SERVICE_VENDOR( Service )->SerialReceiveBuffer.Length = 0; // dirty

  osTimeGetMilliseconds(Service, &ZAB_SERVICE_VENDOR(Service)->lastOneSecondTickMs);
  zabCoreFreeQueue(Status, &ZAB_SRV( Service )->VendorInQ);

  /* Initialise state machines */
  zabNcpOpen_Create(Status, Service);
  zabNcpNwk_Create(Status, Service);
  zabNcpSbl_Create(Status, Service);
  zabNcpRft_Create(Status, Service);
}

/******************************************************************************
 * Destroy an instance of ZAB Vendor
 ******************************************************************************/
void zabVendorService_Destroy(erStatus* Status, zabService* Service)
{
  unsigned8 index;

  if (Service != NULL)
    {
      for (index = 0; index < ZAB_VND_CFG_MAX_ENDPOINTS; index++)
        {
          if (ZAB_SERVICE_VENDOR(Service)->simpleDesc[index] != NULL)
            {
              OS_MEM_FREE(Status, srvCoreGetServiceId(Status, Service), ZAB_SERVICE_VENDOR(Service)->simpleDesc[index]);
              ZAB_SERVICE_VENDOR(Service)->simpleDesc[index] = NULL;
            }
        }

      zabCoreFreeQueue(Status, &ZAB_SRV( Service )->VendorInQ);
      ZAB_PROTECT_RELEASE(Service, qSafeId(&ZAB_SRV( Service )->VendorInQ));

      ZAB_PROTECT_RELEASE(Service, sapCoreGetMutexId(ZAB_SRV(Service)->SerialSAP));
      OS_MEM_FREE(Status, srvCoreGetServiceId(Status, Service), ZAB_SRV(Service)->SerialSAP);
      OS_MEM_FREE(Status, srvCoreGetServiceId(Status, Service), ZAB_SRV(Service)->Vendor);
    }
}

/******************************************************************************
 * Vendor Work
 *   Process VendorOutQ and send commands to network processor
 *   Process VendorInQ and indicate messages up to the core
 *   Check for SRSP timeouts
 *   Update state machines
 ******************************************************************************/
unsigned32 zabVendorService_Work(erStatus* Status, zabService* Service)
{
  unsigned32 time;
  unsigned32 nextWorkRequiredDelayMs = ZAB_WORK_DELAY_MAX_MS;
  unsigned32 nextWorkRequiredDelayMsTemp;

  /* Local work in first, which may generate commands out from the state machines.
   * This gives them first go at the serial link before it is locked */
  nextWorkRequiredDelayMsTemp = LocalWorkIn(Status, Service);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  /* Get time now and check all timeouts. Much more efficient than getting time several times over */
  osTimeGetMilliseconds(Service, &time);

  nextWorkRequiredDelayMsTemp = zabNcpEzsp_UpdateTimeOut(Status, Service, time);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  nextWorkRequiredDelayMsTemp = zabNcpOpen_UpdateTimeout(Status, Service, time);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  nextWorkRequiredDelayMsTemp = zabNcpNwk_UpdateTimeout(Status, Service, time);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  nextWorkRequiredDelayMsTemp = zabNcpSbl_UpdateTimeout(Status, Service, time);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  /* One second tick. */
  if ( (time+1000) > ZAB_SERVICE_VENDOR(Service)->lastOneSecondTickMs)
    {
      if (time >= (ZAB_SERVICE_VENDOR(Service)->lastOneSecondTickMs +1000))
        {
          ZAB_SERVICE_VENDOR(Service)->lastOneSecondTickMs+=1000;
          nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, 1000);

          zabNcpNwk_OneSecondTickHandler(Status, Service);
        }
      else
        {
          nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, (ZAB_SERVICE_VENDOR(Service)->lastOneSecondTickMs +1000) - time);
        }
    }
  /* Time is within a second of wrapping, just update the last tick time to timeNow until timeNow gets to zero */
  else
    {
      ZAB_SERVICE_VENDOR(Service)->lastOneSecondTickMs = time;
      nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, 1000);
    }

  /* Ping network processor: Lower priority than everything except work out */
  nextWorkRequiredDelayMsTemp = zabNcpOpen_UpdateNetworkProcessorPing(Status, Service, time);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);


  nextWorkRequiredDelayMsTemp = LocalWorkOut(Status, Service);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  return nextWorkRequiredDelayMs;
}


/******************************************************************************
 * Vendor is ready for local comms from the App
 ******************************************************************************/
void zabVendorService_GetLocalCommsReady(erStatus* Status, zabService* Service)
{
  ER_CHECK_STATUS(Status);
  if (zabNcpOpen_GetOpenState(Service) != ZAB_OPEN_STATE_OPENED)
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
    }
}

/******************************************************************************
 * Vendor is ready for network comms from the App
 ******************************************************************************/
void zabVendorService_GetNetworkCommsReady(erStatus* Status, zabService* Service)
{
  ER_CHECK_STATUS(Status);
  if (zabNcpOpen_GetOpenState(Service) != ZAB_OPEN_STATE_OPENED)
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
    }
  else if (zabNcpNwk_GetNwkState(Service) != ZAB_NWK_STATE_NETWORKED)
    {
      erStatusSet(Status, ZAB_ERROR_INVALID_NETWORK_STATE);
    }
}

/******************************************************************************
 * This function receives DATA and MANAGE SAP messages from the core.
 * It queues them for processing by Vendor Work
 ******************************************************************************/
void zabVendorService_CoreToVendorOut(erStatus* Status, sapHandle Sap, sapMsg* Message)
{
  zabServiceStruct* This = ZAB_SRV( sapCoreGetService( Sap ) );
  sapMsgType Type = sapMsgGetType(Message);

  ER_CHECK_STATUS(Status);

  if ((Type == SAP_MSG_TYPE_ACTION) || (Type == SAP_MSG_TYPE_APP))
    {
      sapMsgUse(Status, Message); // must do this because we are storing it on a queue

      ER_CHECK_STATUS(Status);

      qSafeAppend(&This->VendorOutQ, Message); // store app messages to send to network
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_MSG_TYPE_INVALID);
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/