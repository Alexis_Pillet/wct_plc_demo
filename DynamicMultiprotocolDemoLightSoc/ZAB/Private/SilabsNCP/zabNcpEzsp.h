/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Service - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/

#ifndef __ZAB_NCP_EZSP_H__
#define __ZAB_NCP_EZSP_H__

#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************
 *                      ******************************
 *                 *****          CONSTANTS           *****
 *                      ******************************
 ******************************************************************************/

/* Default sequence number for commands initiated form within vendor */
#define ZAB_NCP_EZSP_DEFAULT_SEQ_NUM    ( 0 )


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise the EZSP service
 ******************************************************************************/
void zabNcpEzsp_InitService(erStatus* Status, zabService* Service);

/******************************************************************************
 * Get status of serial link
 ******************************************************************************/
zab_bool zabNcpEzsp_GetSerialLinkLocked(zabService* Service);

/******************************************************************************
 * Check for timeouts on EZSP responses
 ******************************************************************************/
unsigned32 zabNcpEzsp_UpdateTimeOut(erStatus* Status, zabService* Service, unsigned32 Time);

/******************************************************************************
 * Send an EZSP message
 *  BufferData must include EZSP header (EZSP_MIN_FRAME_LENGTH) plus any payload.
 *  EZSP Sequence number will be added internally
 ******************************************************************************/
void zabNcpEzsp_Send(erStatus* Status, zabService* Service, unsigned8 SequenceNumber, unsigned8 BufferLength, unsigned8* BufferData);

/******************************************************************************
 * Process an incoming EZSP frame
 ******************************************************************************/
void zabNcpEzsp_ProcessIncomingFrame(erStatus* Status, zabService* Service, unsigned8 FrameLength, unsigned8* FrameData);

#ifdef __cplusplus
}
#endif

#endif // __ZAB_NCP_EZSP_H__
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/