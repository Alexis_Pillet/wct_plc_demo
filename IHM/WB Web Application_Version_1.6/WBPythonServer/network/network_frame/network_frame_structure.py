
"""
network.network_frame.network_frame_structure
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to store the structure of network frames

"""
from enum import IntEnum, IntFlag


class Network:
    """Contains all possible values of all fields of network frames"""

    @staticmethod
    def display_ack():
        """Return a string to display an user friendly log"""
        return {Network.AckStatus.SUCCESS: 'SUCCESS',
                Network.AckStatus.BAD_NETWORK_SEQUENCE_NUMBER: 'BAD_NETWORK_SEQUENCE_NUMBER',
                Network.AckStatus.ALREADY_RX: 'ALREADY_RX',
                Network.AckStatus.INVALID_COMMAND: 'INVALID_COMMAND',
                Network.AckStatus.ERROR_NO_ACK: 'ERROR_NO_ACK',
                Network.AckStatus.SUCCESS_NO_ACK: 'SUCCESS_NO_ACK'}

    class FrameControl(IntEnum):
        """Frame Control field values"""
        CONTROL = 0x00
        DATA = 0x01
        ACK = 0xFF

    class OptionMask(IntFlag):
        """Masks to obtain specific option of option field"""
        RESPONSE = 0x01
        RESERVED = 0xFE

    class OptionResponse(IntEnum):
        """Option Response field values"""
        DISABLE = 0x00
        ENABLE = 0x01

    class AckStatus(IntEnum):
        """Ack Status field values"""
        SUCCESS = 0x00
        BAD_NETWORK_SEQUENCE_NUMBER = 0x01
        ALREADY_RX = 0x02
        INVALID_COMMAND = 0x03
        ERROR_NO_ACK = 0x04
        SUCCESS_NO_ACK = 0x05
