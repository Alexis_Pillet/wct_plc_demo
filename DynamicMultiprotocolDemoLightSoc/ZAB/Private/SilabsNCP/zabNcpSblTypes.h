/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the NCP Serial Boot Loader manager - HEADER TYPES
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/
#ifndef _ZAB_NCP_SBL_TYPES_H_
#define _ZAB_NCP_SBL_TYPES_H_

#include "osTypes.h"
#include "erStatusUtility.h"
#include "zabTypes.h"

/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

typedef enum
{
  SBL_STATUS_SUCCESS                 = 0,
  SBL_STATUS_FAILURE                 = 1,
  SBL_STATUS_INVALID_FCS             = 2,
  SBL_STATUS_INVALID_FILE            = 3,
  SBL_STATUS_FILESYSTEM_ERROR        = 4,
  SBL_STATUS_ALREADY_STARTED         = 5,
  SBL_STATUS_NO_RESPOSNE             = 6,
  SBL_STATUS_VALIDATE_FAILED         = 7,
  SBL_STATUS_CANCELED                = 8,
  SBL_STATUS_INVALID_COMMAND         = 9,
}sblStatus;

/* Current Item of the state machine */
typedef enum
{
  ZAB_SBL_CURRENT_ITEM_NONE,
  ZAB_SBL_CURRENT_ITEM_WAITING_FOR_READY,
  ZAB_SBL_CURRENT_ITEM_HANDSHAKING,
  ZAB_SBL_CURRENT_ITEM_WRITING,
  ZAB_SBL_CURRENT_ITEM_WRITE_COMPLETE,
  ZAB_SBL_CURRENT_ITEM_WRITE_AWAIT_PROMPT,
  ZAB_SBL_CURRENT_ITEM_WRITE_RUN_WAIT,

  ZAB_SBL_CURRENT_ITEM_EXIT_RUN_WAIT,

  ZAB_SBL_CURRENT_ITEM_WRITE_FAILED_EXIT_XMODEM,
  ZAB_SBL_CURRENT_ITEM_FAILED_GET_PROMPT
} zabSblCurrentItem;

/* Class of SBL we are running. Handles differences between various network processor types */
typedef enum
{
  ZAB_SBL_CLASS_NONE,
  ZAB_SBL_CLASS_CC2530_CC2531,
  ZAB_SBL_CLASS_CC2538,
} zabSblClass;


typedef enum
{
  ZAB_SBL_RESPONSE_ACK,
  ZAB_SBL_RESPONSE_NACK,
  ZAB_SBL_RESPONSE_CLEAR,
  ZAB_SBL_RESPONSE_PROMPT_READY,
} zabSblResponse_t;

/* State information for this state machine */
typedef struct
{
  zabSblCurrentItem currentItem;    /* Current internal state - PRIVATE*/
  unsigned32 timeoutExpiryMs;       /* Timeout for confirms - PRIVATE */
  unsigned32 imageLength;           /* Length of the image to be loaded */
  unsigned32 address;               /* Current address within the image */
} zabSblInfo_t;

#endif /* _ZAB_NCP_SBL_TYPES_H_ */
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/