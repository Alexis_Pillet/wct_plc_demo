/******************************************************************************
 *                          ZAB TEST APPLICATION
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the Green Power callbacks.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.06.00  24-Jul-14   MvdB   Original
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 * 002.001.001  15-Jul-15   MvdB   ARTF136585: Add transaction IDs to over the air commands/responses
 * 002.002.002  01-Sep-15   MvdB   ARTF147441: Support Key Mode in appGp_GpdCommissioningNtf_Handler()
 * 002.002.019  24-Nov-15   MvdB   Support ZGP TransTable Req & Update for testing
 *****************************************************************************/
#ifndef _APP_GP_H_
#define _APP_GP_H_


#include "zabCoreService.h"
#include "appConfig.h"
#include "szl_gp.h"

#ifdef APP_CONFIG_ENABLE_GREEN_POWER
/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

extern 
void appGp_Init(void);

/******************************************************************************
 * Process a Commissioning Notification
 * This is called when a GPD wishes to join the network.
 * The application should analyse the Params then decide to accept or reject
 * the GPD.
 * Return:
 *  - szl_true to accept
 *  - szl_false to reject
 ******************************************************************************/
extern 
szl_bool appGp_GpdCommissioningNtf_Handler(zabService* Service, SZL_GP_GpdCommissioningNtfParams_t* Params, SZL_GP_COMMISSIONING_KEY_MODE_t* KeyMode);

/******************************************************************************
 * Process a Commissioned Notification
 * This is called when a GPD has successfully been commissioned in to the network.
 ******************************************************************************/
extern 
void appGp_GpdCommissionedNtf_Handler(zabService* Service, struct _SZL_GP_GpdCommissionedNtfParams_t* Params);

#endif // APP_CONFIG_ENABLE_GREEN_POWER
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif /* _APP_GP_H_ */
