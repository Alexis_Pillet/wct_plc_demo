/**************************************************************************************************
* INCLUDES
**************************************************************************************************/
#ifndef _CB_HANDLER_H_
#define _CB_HANDLER_H_
#include "szl_types.h"
#include "szl.h"

/**************************************************************************************************
* DEFINES
**************************************************************************************************/
/*
 * Global defines
 */


#ifdef __cplusplus
extern "C" {
#endif

/*
 * Application data defines
 */
// MvdB: Imported here as i dont use CB queue
typedef void (*CBQueue_CallbackProto)(void);

/**************************************************************************************************
* ENUMs, STRUCTs & UNIONs
**************************************************************************************************/
typedef enum
{
    CB_FUNC_BRICK_INIT_NTF,
    CB_FUNC_ATTRIBUTE_CHANGED_NTF,
    CB_FUNC_ATTRIBUTE_REPORT_NTF,
    CB_FUNC_NWK_JOIN_NTF,
    CB_FUNC_NWK_LEAVE_NTF,
    CB_FUNC_NWK_STATE_CHANGE_NTF,
    CB_FUNC_NWK_RX_SIG_STR_NTF,
    CB_FUNC_COORDINATOR_STATE_NTF,
    CB_FUNC_ZDO_DEVICE_ANNOUNCE,
    CB_FUNC_GPD_COMMISSIONING_NTF,
    CB_FUNC_GPD_COMMISSIONED_NTF,
            
            
    CB_FUNC_REMOVED = 0xFE,
    CB_FUNC_EMPTY = 0xFF,
} ENUM_CB_FUNCTION_t;

typedef szl_uint8 CB_FUNCTION_t;

typedef struct
{
    CB_FUNCTION_t cbFunction;
    CBQueue_CallbackProto callback;
} CB_HANDLER_ENTRY_t;


/* ZAB Specific Data Structure */
typedef struct
{
  CB_HANDLER_ENTRY_t cbHandlerTable[SZL_CFG_CB_HANDLER_SIZE];
  szl_bool cdHandlerTableWorkRequired; // Make a set of flags if these become common!
} CBInfo;



/**************************************************************************************************
* PROTOS
**************************************************************************************************/
void CbHandler_Initialize(zabService* Service);
SZL_RESULT_t CbHandler_Add(zabService* Service, CB_FUNCTION_t func, CBQueue_CallbackProto cb);
SZL_RESULT_t CbHandler_Remove(zabService* Service, CB_FUNCTION_t func, CBQueue_CallbackProto cb);
CBQueue_CallbackProto CbHandler_Find(zabService* Service, int* startIdx, CB_FUNCTION_t func);
szl_bool CbHandler_HasCallback(zabService* Service, CB_FUNCTION_t func);
void CbHandler_Maintenance(zabService* Service);


#ifdef __cplusplus
}
#endif

#endif /*_CB_HANDLER_H_*/
