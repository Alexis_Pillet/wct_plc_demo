/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the cloning engine for ZNP network processors.
 * 
 *   Full clone reads and write are triggered by actions, but incremental updates
 *   are notified to the application a-synchronously.
 * 
 *   Handlers for the clone data are currently required to be registered here
 *   in the vendor, as the format is likely to be different for different vendors.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.02.01  29-Nov-13   MvdB   Original
 * 00.00.06.00  11-Sep-14   MvdB   Add NV Change Notification
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
 * 002.000.007  14-Apr-15   MvdB   ARTF130628: Replace Clone Timeout with SRSP timeout
 * 002.002.007  28-Sep-15   MvdB   ARTF148934: Clone Get: Support reading of selected items
 * 002.002.011  12-Oct-15   MvdB   ARTF148934: Upgrade clone get to allow app to read only changed items
 *                                 ARTF104108: Add notification of progress for Clone actions
 *****************************************************************************/
#ifndef __ZAB_ZNP_CLONE_H__
#define __ZAB_ZNP_CLONE_H__

#include "osTypes.h"
#include "erStatusUtility.h" 
#include "sapTypes.h"
#include "zabTypes.h"
#include "zabZnpFrame.h"

#ifdef __cplusplus
extern "C" {
#endif
  
  
/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/  
  
/* Network State */
typedef enum
{
  ZAB_CLONE_ACTION_NONE,
  ZAB_CLONE_ACTION_GET_START,
  ZAB_CLONE_ACTION_GET_LENGTH,
  ZAB_CLONE_ACTION_GET_READ,
  ZAB_CLONE_ACTION_SET_START,
  ZAB_CLONE_ACTION_SET_WRITE,
} zabCloneAction;  


#define ZAB_ZNP_CLONE_M_ID_ALL 0xFFFF
  
/* Data Store Callback: Called by ZAB to give the app an NV item to store */
typedef void (*zabZnpClone_CB_DataStore_t) (zabService* Service, unsigned16 CloneDataId, unsigned16 TotalLength, unsigned16 Offset, unsigned8 DataLength, unsigned8* DataPointer);

/* Data Retrieve Callback: Called by ZAB to ask the app to give back an NV item */
typedef void (*zabZnpClone_CB_DataRetrieve_t) (zabService* Service, unsigned16 CloneDataId, unsigned16* TotalLength, unsigned16 Offset, unsigned8 *DataLength, unsigned8* CloneDataPointer);

/* Data Change Notification Callback: Called by ZAB to notify the app that NV item(s) have changed.
 * If CloneDataIds[0] == ZAB_ZNP_CLONE_M_ID_ALL then more IDs have changed than we can manage, so all data must be reloaded. */
typedef void (*zabZnpClone_CB_DataChangeNotification_t) (zabService* Service, unsigned8 NumberOfIds, unsigned16* CloneDataIds);

/* Selective Get: Number of Ids Callback.
 * This allows the app to specify a number of selected items to be read during a ZAB_ACTION_CLONE_GET.
 * The handler may return:
 *  1-0xFFFE: Specified number of items to be read. Each item ID will be requested form the app via zabZnpClone_CB_SelectiveGetDataId_t
 *  0 or 0xFFFF (ZAB_ZNP_CLONE_M_ID_ALL): Read all items. zabZnpClone_CB_SelectiveGetDataId_t will not be called. */
typedef unsigned16 (*zabZnpClone_CB_SelectiveGetNumberOfIds_t) (zabService* Service);

/* Selective Get: Data Id Callback.
 * This allows the app to register a handler that can be used to specify selected items only to be read during a ZAB_ACTION_CLONE_GET.
 * Handler will be called with index starting from 0 up to the value returned by zabZnpClone_CB_SelectiveGetNumberOfIds_t - 1 */
typedef unsigned16 (*zabZnpClone_CB_SelectiveGetDataId_t) (zabService* Service, unsigned16 Index);


/* State information for this state machine */
typedef struct
{
  zabZnpClone_CB_DataStore_t DataStoreCallback;
  zabZnpClone_CB_DataRetrieve_t DataRetrieveCallback;
  zabZnpClone_CB_DataChangeNotification_t DataChangeNotificationCallback;
  zabZnpClone_CB_SelectiveGetNumberOfIds_t SelectiveGetNumberOfIdsCallback;
  zabZnpClone_CB_SelectiveGetDataId_t SelectiveGetDataIdCallback;
  zabCloneAction cloneAction;            
  unsigned16 cloneBlockIndex;         
  unsigned16 cloneItemId;   
  unsigned16 currentItemLength;
  unsigned16 currentOffset;

  unsigned16 progressItemCountTotal;   /*< Total nubmer of items to be get/set */
  unsigned16 progressItemCountCurrent; /*< Current count of items already got/set */
} zabCloneInfo_t;  

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/  

/******************************************************************************
 * Register Clone Data Store Callback
 * 
 * This callback will be called with data from the network processor after a 
 * ZAB_ACTION_CLONE_GET, or on change of data if a callback is registered.
 * 
 * Applications that do not support cloning should leave this callback unregistered.
 ******************************************************************************/
extern 
void zabZnpClone_RegisterDataStoreCallback(zabService* Service, 
                                           zabZnpClone_CB_DataStore_t Callback);

/******************************************************************************
 * Register Clone Data Retrieve Callback
 * 
 * This callback will be called to request stored data to be written back into
 * a network processor after a ZAB_ACTION_CLONE_SET.
 * 
 * Applications that do not support cloning should leave this callback unregistered.
 ******************************************************************************/
extern 
void zabZnpClone_RegisterDataRetrieveCallback(zabService* Service, 
                                              zabZnpClone_CB_DataRetrieve_t Callback);

/******************************************************************************
 * Register Clone Data Change Notification Callback
 * 
 * This callback will be called to notify the application that NV item(s) have changed.
 * Data should then be read again to update the backup.
 * 
 * Applications that do not support cloning should leave this callback unregistered.
 ******************************************************************************/
extern 
void zabZnpClone_RegisterChangeNotificationCallback(zabService* Service, 
                                                    zabZnpClone_CB_DataChangeNotification_t Callback);


/******************************************************************************
 * Register Selective Get Callback
 * 
 * These callbacks will be called to ask the application if it wishes to specify
 * the items read during a ZAB_ACTION_CLONE_GET.
 * Typically this would be items that have been notified to have changed via zabZnpClone_CB_DataChangeNotification_t
 * 
 * Application may:
 *  - Not register these callbacks, in which case the action will get all items.
 *  - Register the callbacks and return:
 *   - The number of items to be read via NumberOfIdsCallback
 *   - The ID of items to be read via DataIdCallback
 * 
 * Applications that do not support cloning should leave these callbacks unregistered.
 ******************************************************************************/
extern 
void zabZnpClone_RegisterSelectiveGetCallback(zabService* Service, 
                                              zabZnpClone_CB_SelectiveGetNumberOfIds_t NumberOfIdsCallback,
                                              zabZnpClone_CB_SelectiveGetDataId_t DataIdCallback);


/******************************************************************************
 *                      ******************************
 *                ***** INTERNAL FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/ 

/******************************************************************************
 * Create
 ******************************************************************************/
extern 
void zabZnpClone_create(erStatus* Status, zabService* Service);


/******************************************************************************
 * Reset the service
 ******************************************************************************/
extern 
void zabZnpClone_Reset(erStatus* Status, zabService* Service);

/******************************************************************************
 * Action
 ******************************************************************************/
extern 
void zabZnpClone_action(erStatus* Status, zabService* Service, unsigned8 Action);


/******************************************************************************
 * Handle SRSP timeouts
 ******************************************************************************/
extern 
void zabZnpClone_srspTimeoutHandler(erStatus* Status, zabService* Service);

/******************************************************************************
 * NV Length Response Handler
 ******************************************************************************/
extern 
void zabZnpClone_NvLengthResponseHandler(erStatus* Status, 
                                         zabService* Service, 
                                         unsigned16 Length);

/******************************************************************************
 * NV Read Response Handler
 ******************************************************************************/
extern 
void zabZnpClone_NvReadResponseHandler(erStatus* Status, 
                                       zabService* Service, 
                                       ZNPI_API_ERROR_CODES znpStatus, 
                                       unsigned8 Length, 
                                       unsigned8* DataPointer);


/******************************************************************************
 * NV Write Response Handler
 ******************************************************************************/
extern 
void zabZnpClone_NvWriteResponseHandler(erStatus* Status, 
                                        zabService* Service, 
                                        ZNPI_API_ERROR_CODES znpStatus);

/******************************************************************************
 * NV Change Notification Handler
 ******************************************************************************/
extern 
void zabZnpClone_NvChangeNotificationHandler(erStatus* Status, 
                                             zabService* Service, 
                                             unsigned8 NumberOfIds,
                                             unsigned16* CloneDataIds);

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/

#ifdef __cplusplus
}
#endif


#endif
