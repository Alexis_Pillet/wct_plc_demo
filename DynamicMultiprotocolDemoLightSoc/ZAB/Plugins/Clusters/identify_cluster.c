/*******************************************************************************
  Filename    : identify_cluster.c
  $Date       : 2013-08-20                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : This is the Identify Cluster plugin for the SZL
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    7         20-Jul-15   MvdB   ARTF134686: Update allocation method to be generic and protect against multiple initialisation
 * 002.001.006  27-Jul-15   MvdB   ARTF134686: Standardise on SZL_RESULT_t for plugin init return value
 *                                 ARTF138318: Improve plugin destroy
 * 002.002.012  14-Oct-15   MvdB   ARTF151335: Identify Plugin: Endpoint parameter in callback is wrong on timer expiry
*******************************************************************************/
/*
*/
#include <stdio.h>
#include <string.h>
#include "zabCoreService.h"
#include "szl_timer.h"
#include "zabCorePrivate.h"
#include "identify_cluster.h"


#define ZCL_CLUSTER_ID_GEN_IDENTIFY     0x0003

typedef struct
{
  szl_uint8 endpoint; // This is needed so when we decrement IdentifyTimer to zero, we can know which EP it was for
  szl_uint16 IdentifyTimer;
}identifyEndpointParams;

typedef struct
{
  SzlPlugin_IdentifyClusterNotification_CB_t IdentifyCallback;
  szl_uint8 numEndpoints;
  szl_uint16 identifyTimerId;
  identifyEndpointParams identifyEndpoints[VLA_INIT]; // Variable length array
}SzlPlugin_Identify_Params_t;
#define SzlPlugin_Identify_Params_t_SIZE(numEndpoints) (sizeof(SzlPlugin_Identify_Params_t) - (VLA_INIT*sizeof(identifyEndpointParams))+ (sizeof(identifyEndpointParams) * (numEndpoints)))




szl_bool SzlIC_ClusterCommandHandler(zabService* Service,
                                     szl_uint8 DestinationEndpoint,
                                     szl_bool manufacturerSpecific,
                                     szl_uint16 ClusterID,
                                     szl_uint8 CmdID,
                                     szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize,
                                     szl_uint8* CmdOutID,
                                     szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize,
                                     szl_uint8 TransactionId,
                                     SZL_Addresses_t SourceAddress,
                                     SZL_ADDRESS_MODE_t DestAddressMode);
void SzlIC_TimerExpired(zabService* Service, szl_uint8 ID, szl_uint32 duration);

/**
 * Identify cluster attributes used
 */
typedef enum
{
    ATTRID_IDENTIFY_TIME                = 0x0000,
} IDENTIFY_CLUSTER_ATTRIBUTES;

/**
 * Identify cluster commands
 */
typedef enum
{
  COMMAND_IDENTIFY                      = 0x00,
  COMMAND_IDENTIFY_QUERY                = 0x01
} IDENTIFY_CLUSTER_CMD_ATTRIBS;


// Set the timer to 1 second
#define TIMER_DURATION (1)

// Cluster Commands
const SZL_ClusterCmd_t SzlIC_clusterCommands[] ={
  {szl_false, SZL_ADDRESS_EP_ALL, ZCL_CLUSTER_ID_GEN_IDENTIFY, COMMAND_IDENTIFY,       SZL_CLU_OP_TYPE_CUSTOM_CLIENT_TO_SERVER, {.Callback = SzlIC_ClusterCommandHandler}},
  {szl_false, SZL_ADDRESS_EP_ALL, ZCL_CLUSTER_ID_GEN_IDENTIFY, COMMAND_IDENTIFY_QUERY, SZL_CLU_OP_TYPE_CUSTOM_CLIENT_TO_SERVER, {.Callback = SzlIC_ClusterCommandHandler}},
};
#define CLUSTER_COMMANDS_COUNT    ( sizeof(SzlIC_clusterCommands) / sizeof(SZL_ClusterCmd_t))


/******************************************************************************
 * Attribute Changed Notification Handler
 ******************************************************************************/
static void AttributeChangedNotificationHandler(zabService* Service, struct _SZL_AttributeChangedNtfParams_t* Params)
{
  szl_uint8 data_type;
  szl_uint16 idTime;
  szl_uint8 length = sizeof(szl_uint16);
  SzlPlugin_Identify_Params_t * pluginParams = NULL;

  /* Validate inputs */
  if ( (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_IDENTIFY, (void**)&pluginParams) == SZL_RESULT_SUCCESS) &&
       (pluginParams != NULL) &&
       (pluginParams->IdentifyCallback != NULL) )
    {
      if (Szl_DataPointReadNative(Service,
                                  Params->Endpoint,
                                  szl_false,
                                  ZCL_CLUSTER_ID_GEN_IDENTIFY,
                                  ATTRID_IDENTIFY_TIME,
                                  &data_type, &idTime, &length) == SZL_STATUS_SUCCESS)
        {
          if (idTime > 0)
            {
              pluginParams->IdentifyCallback(Service, Params->Endpoint, szl_true);
            }
          else
            {
              pluginParams->IdentifyCallback(Service, Params->Endpoint, szl_false);
            }
        }
    }
}




/**
 * Identify Cluster Initialize
 *
 * This function will register the Identify cluster and it's attributes
 *
 * If the plugin isn't able to register the attributes the brick will be reset
 *
 */
SZL_RESULT_t SzlPlugin_IdentifyClusterInit(zabService* Service, szl_uint8 numEndpoints, szl_uint8 * endpointList, SzlPlugin_IdentifyClusterNotification_CB_t callback)
{
    SZL_RESULT_t result;
    szl_uint8 index;
    szl_uint8 commandIndex;
    SzlPlugin_Identify_Params_t * pluginParams = NULL;
    SZL_DataPoint_t serverAttributes[SZL_CFG_ENDPOINT_CNT];
    SZL_ClusterCmd_t clusterCommands[CLUSTER_COMMANDS_COUNT];

    printInfo(Service, "PLUGIN Identify: Init\n");

    /* Validate inputs */
    if ( (Service == NULL) || (callback == NULL) || (endpointList == NULL) || (numEndpoints > SZL_CFG_ENDPOINT_CNT))
      {
        return SZL_RESULT_INVALID_DATA;
      }

    /* Malloc then initialise parameters the plugin needs to store and link from the SZL */
    result = SZL_PluginRegister(Service, SZL_PLUGIN_ID_IDENTIFY, SzlPlugin_Identify_Params_t_SIZE(numEndpoints), (void**)&pluginParams);
    if ( (result == SZL_RESULT_SUCCESS) && (pluginParams != NULL) )
      {

        pluginParams->IdentifyCallback = callback;
        if (callback != NULL)
          {
            if (SZL_AttributeChangedNtfRegisterCB(Service, AttributeChangedNotificationHandler) == SZL_RESULT_TABLE_FULL)
              {
                printError(Service, "PLUGIN ID: Init failed - callback table full\n");
                SZL_PluginUnregister(Service, SZL_PLUGIN_ID_IDENTIFY);
                return SZL_RESULT_TABLE_FULL;
              }
          }

        pluginParams->numEndpoints = numEndpoints;
        for (index = 0; index < numEndpoints; index++)
          {
            pluginParams->identifyEndpoints[index].endpoint = endpointList[index];
            pluginParams->identifyEndpoints[index].IdentifyTimer = 0;
          }

        for (index = 0; index < numEndpoints; index++)
          {
            serverAttributes[index].ClusterID = ZCL_CLUSTER_ID_GEN_IDENTIFY;
            serverAttributes[index].AttributeID = ATTRID_IDENTIFY_TIME;
            serverAttributes[index].Endpoint = endpointList[index];
            serverAttributes[index].DataType = SZL_ZB_DATATYPE_UINT16;
            serverAttributes[index].DataSize = sizeof(szl_uint16);
            //serverAttributes[index].AccessType = DP_ACCESS_DA_RW; // Note: We should use handlers, not Direct Access so wee can notify app if ID time is written

            serverAttributes[index].Property.Access = DP_ACCESS_RW;
            serverAttributes[index].Property.Direction = DP_DIRECTION_SERVER;
            serverAttributes[index].Property.Method = DP_METHOD_DIRECT;
            serverAttributes[index].Property.ManufacturerSpecific = szl_false;

            serverAttributes[index].AccessType.DirectAccess.Data = &(pluginParams->identifyEndpoints[index].IdentifyTimer);
          }

        // register the cluster attributes
        result = SZL_AttributesRegister(Service, serverAttributes, numEndpoints);
        if( (result == SZL_RESULT_SUCCESS) || (result == SZL_RESULT_ALREADY_EXISTS) )
          {

            // Register the clusters commands for each endpoint
            szl_memcpy(clusterCommands, SzlIC_clusterCommands, sizeof(clusterCommands));
            for (index = 0; index < numEndpoints; index++)
              {
                for (commandIndex = 0; commandIndex < CLUSTER_COMMANDS_COUNT; commandIndex++)
                  {
                    clusterCommands[commandIndex].Endpoint = endpointList[index];
                  }
                result = SZL_ClusterCmdRegister(Service, clusterCommands, CLUSTER_COMMANDS_COUNT);
                if( (result != SZL_RESULT_SUCCESS) && (result != SZL_RESULT_ALREADY_EXISTS) )
                  {
                    break;
                  }
              }

            if( (result == SZL_RESULT_SUCCESS) || (result == SZL_RESULT_ALREADY_EXISTS) )
              {

                pluginParams->identifyTimerId = Szl_ExtTimerRegister(Service, szl_true, TIMER_DURATION, SzlIC_TimerExpired);
                Szl_ExtTimerStart(Service, pluginParams->identifyTimerId);
                if(pluginParams->identifyTimerId > 0)
                  {
                    /* All good - assign Id parameters into SZL and cleanup*/
                    printInfo(Service, "PLUGIN Identify: Init Successful\n");
                    return SZL_RESULT_SUCCESS;
                  }
                else
                  {
                    result = SZL_RESULT_TABLE_FULL;
                  }
              }
          }

        /* Failed. Cleanup */
        printError(Service, "PLUGIN Identify: Init failed\n");
        SZL_PluginUnregister(Service, SZL_PLUGIN_ID_IDENTIFY);
      }
    return result;
}

/**
 * Identify Cluster Destroy
 */
SZL_RESULT_t SzlPlugin_IdentifyCluster_Destroy(zabService* Service)
{
  SZL_RESULT_t result;
  szl_uint8 epIndex;
  SZL_EP_t endpoint;
  SzlPlugin_Identify_Params_t* pluginParams = NULL;
  SZL_DataPoint_t serverAttribute;

  /* Validate inputs */
  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_IDENTIFY, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /* Stop and remove timer */
  Szl_ExtTimerStop(Service, pluginParams->identifyTimerId);
  result = Szl_ExtTimerUnregister(Service, pluginParams->identifyTimerId);
  if (result != SZL_RESULT_SUCCESS)
    {
      printError(Service, "PLUGIN IDENTIFY: WARNING: Deregister timer 0x%02X failed with Result 0x%02X\n",
                 pluginParams->identifyTimerId,
                 result);
    }

  /* Remove commands */
  result = SZL_ClusterCmdDeregister(Service, SzlIC_clusterCommands, CLUSTER_COMMANDS_COUNT);
  if (result != SZL_RESULT_SUCCESS)
    {
      printError(Service, "PLUGIN IDENTIFY: WARNING: Deregister commands failed with Result 0x%02X\n",
                 result);
    }

  /* Remove Attributes */
  for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
    {
      /* Get the endpoint number */
      endpoint = pluginParams->identifyEndpoints[epIndex].endpoint;

      serverAttribute.ClusterID = ZCL_CLUSTER_ID_GEN_IDENTIFY;
      serverAttribute.AttributeID = ATTRID_IDENTIFY_TIME;
      serverAttribute.Endpoint = endpoint;
      serverAttribute.DataType = SZL_ZB_DATATYPE_UINT16;
      serverAttribute.Property.ManufacturerSpecific = szl_false;

      /* De-Register the cluster attributes.
       * Don't care too much about the result as we will make best effort only */
      result = SZL_AttributesDeregister(Service, &serverAttribute, 1);
      if (result != SZL_RESULT_SUCCESS)
        {
          printError(Service, "PLUGIN IDENTIFY: WARNING: Deregister failed for EP 0x%02X with Result 0x%02X\n",
                     endpoint,
                     result);
        }
    }

  /* Now unregister the plugin */
  return SZL_PluginUnregister(Service, SZL_PLUGIN_ID_IDENTIFY);
}



// -----------------------------------------------------------------------------
// Internal functions
// -----------------------------------------------------------------------------
/*
 * Callback function for the identify cluster command handler
 */
szl_bool SzlIC_ClusterCommandHandler(zabService* Service,
                                     szl_uint8 DestinationEndpoint,
                                     szl_bool manufacturerSpecific,
                                     szl_uint16 ClusterID,
                                     szl_uint8 CmdID,
                                     szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize,
                                     szl_uint8* CmdOutID,
                                     szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize,
                                     szl_uint8 TransactionId,
                                     SZL_Addresses_t SourceAddress,
                                     SZL_ADDRESS_MODE_t DestAddressMode)
{
  szl_uint16 idTime;
  szl_uint8 length = sizeof(szl_uint16);;
  szl_uint8 data_type;
  SzlPlugin_Identify_Params_t * pluginParams = NULL;

  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_IDENTIFY, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return szl_false;
    }

  switch (CmdID)
    {
        case COMMAND_IDENTIFY:
            idTime = COPY_IN_16_BITS(ZCLPayloadIn);
            if (Szl_DataPointWriteNative(Service, DestinationEndpoint, manufacturerSpecific, ClusterID, ATTRID_IDENTIFY_TIME, SZL_ZB_DATATYPE_UINT16, &idTime, 2) == SZL_STATUS_SUCCESS)
              {
                *PayloadOutSize = 0;
                return szl_true;
              }
            break;

        case COMMAND_IDENTIFY_QUERY:
            if (Szl_DataPointReadNative(Service, DestinationEndpoint, manufacturerSpecific, ClusterID, ATTRID_IDENTIFY_TIME, &data_type, &idTime, &length) == SZL_STATUS_SUCCESS)
              {
                *CmdOutID = 0x00; //query response
                *PayloadOutSize = sizeof(szl_int16);
                COPY_OUT_16_BITS(ZCLPayloadOut, idTime);
                return szl_true;
              }
            break;
    }
    return szl_false;
}

/*
 * Callback function for timer expiration
 */
void SzlIC_TimerExpired(zabService* Service, szl_uint8 ID, szl_uint32 duration)
{
  szl_uint8 index;

  SzlPlugin_Identify_Params_t * pluginParams = NULL;

  if ( (Service == NULL) ||
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_IDENTIFY, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return;
    }

  for (index = 0; index < pluginParams->numEndpoints; index++)
    {
      if (pluginParams->identifyEndpoints[index].IdentifyTimer > 0)
        {
          pluginParams->identifyEndpoints[index].IdentifyTimer--;

          if (pluginParams->identifyEndpoints[index].IdentifyTimer == 0)
            {
              if (pluginParams->IdentifyCallback != NULL)
                {
                  pluginParams->IdentifyCallback(Service,
                                                 pluginParams->identifyEndpoints[index].endpoint,
                                                 szl_false);
                }
            }
        }
    }
}