/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Networking Commands/Responses - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.014  22-Jul-16   MvdB   Support ZAB_ACTION_SET_TX_POWER
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 * 000.000.023  02-Aug-16   MvdB   Support EZSP_SET_MANUFACTURER_CODE
 *****************************************************************************/

#ifndef __ZAB_NCP_EZSP_NETWORKING_H__
#define __ZAB_NCP_EZSP_NETWORKING_H__

#include "ember-types.h"
#include "ezsp-enum.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Network Init Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_NetworkInit(erStatus* Status, zabService* Service);
void zabNcpEzspNetworking_NetworkInitResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Energy or Active Scan: Request, Response, Callbacks and Confirm
 ******************************************************************************/
void zabNcpEzspNetworking_StartScan(erStatus* Status, zabService* Service, EzspNetworkScanType ScanType, unsigned32 ChannelMask, unsigned8 Duration);
void zabNcpEzspNetworking_StartScanResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);
void zabNcpEzspNetworking_EnergyScanResultHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);
void zabNcpEzspNetworking_NetworkFoundResultHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);
void zabNcpEzspNetworking_ScanCompleteHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Network Form Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_NetworkForm(erStatus* Status, zabService* Service, EmberNetworkParameters* NetworkParameters);
void zabNcpEzspNetworking_NetworkFormResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Network Join Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_NetworkJoin(erStatus* Status, zabService* Service, EmberNodeType NodeType, EmberNetworkParameters* NetworkParameters);
void zabNcpEzspNetworking_NetworkJoinResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Stack Status Handler
 ******************************************************************************/
void zabNcpEzspNetworking_StackStatusHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Get Network Parameters Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_GetNetworkParameters(erStatus* Status, zabService* Service);
void zabNcpEzspNetworking_GetNetworkParametersResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Set Radio Power Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_SetRadioPower(erStatus* Status, zabService* Service, signed8 TxPower);
void zabNcpEzspNetworking_SetRadioPowerResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Local Permit Join Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_PermitJoin(erStatus* Status, zabService* Service, unsigned8 PermitJoinTime);
void zabNcpEzspNetworking_PermitJoinResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Local Leave Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_Leave(erStatus* Status, zabService* Service);
void zabNcpEzspNetworking_LeaveResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Set Manufacturer Code Request + Response Handler
 ******************************************************************************/
void zabNcpEzspNetworking_SetManufacturerCode(erStatus* Status, zabService* Service, unsigned16 ManufacturerCode);
void zabNcpEzspNetworking_SetManufacturerCodeResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/