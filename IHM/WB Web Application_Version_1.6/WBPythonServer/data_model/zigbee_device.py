
"""
data_model.zigbee_device
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to model ZigBee Device

"""
from abc import ABC, abstractmethod
from collections import namedtuple


class ZigBeeDevice(ABC):
    """Model ZigBee device"""

    def __init__(self, address, id_wireless_bridge, application_frame_generator):
        """Initialization"""
        self._application_frame_generator = application_frame_generator
        self._address = address
        self._rssi = 0
        self._lqi = 0
        self._id_wireless_bridge = int(id_wireless_bridge)
        self._product_id = None
        self._manufacturer_name = ""
        self._model_identifier = ""
        self._application_firmware_version = ""
        self._application_hardware_version = ""
        self._product_serial_number = ""
        self._product_identifier = 0
        self._product_model = ""
        self._END_POINT_BASIC_CLUSTER = 0x01
        self._BASIC_CLUSTER = "0000"
        self._MANUFACTURER_NAME = "0004"
        self._MODEL_IDENTIFIER = "0005"
        self._APPLICATION_FIRMWARE_VERSION_ID = "e001"
        self._APPLICATION_HARDWARE_VERSION_ID = "e002"
        self._PRODUCT_SERIAL_NUMBER_ID = "e004"
        self._PRODUCT_IDENTIFIER_ID = "e007"
        self._PRODUCT_MODEL = "e009"
        super(ZigBeeDevice, self).__init__()

    @abstractmethod
    def get_attribute(self, destination_ep, cluster_id, attribute_id_list):
        """Generate a get attribute command to get value of the specified attributes

        Args:
            destination_ep: destination of the cluster
            cluster_id: cluster id containing the attributes
            attribute_id_list: list of the attributes
        """
        pass

    @abstractmethod
    def set_attribute(self, destination_ep, cluster_id, attribute_list):
        """Generate a set attribute command to change the value of the specified attributes

        Args:
            destination_ep: destination of the cluster
            cluster_id: cluster id containing the attributes
            attribute_list: list of the attributes
            Named tuple
            >>> AttributeType = namedtuple('Attribute', ['attribute_id', 'attribute_type', 'attribute_value'])
        """
        pass

    @abstractmethod
    def discovery(self):
        """Generate commands to discover the ZigBee device"""
        pass

    @abstractmethod
    def update_data(self, data):
        """Update the data of the ZigBee device with the specified value

        Args:
            data: dict of the value received
        """
        pass

    @abstractmethod
    def update_rssi(self, lqi, rssi):
        """Update the lqi and rssi of the ZigBee product

        Args:
            lqi: lqi value
            rssi: rssi value
        """
        pass

    @abstractmethod
    def get_json_data(self):
        """Get the json representation of the data of the product"""
        pass

    @abstractmethod
    def get_json_device(self, device_number):
        """Get the json representation of the ZigBee device

        Args:
            device_number: device number of the device
        """
        pass

    @property
    def application_frame_generator(self):
        return self._application_frame_generator

    @application_frame_generator.setter
    def application_frame_generator(self, frame_generator):
        self._application_frame_generator = frame_generator

    @property
    def address(self):
        return self._address

    @address.setter
    def address(self, address):
        self._address = address

    @property
    def lqi(self):
        return self._lqi

    @lqi.setter
    def lqi(self, lqi):
        self._lqi = lqi

    @property
    def rssi(self):
        return self._rssi

    @rssi.setter
    def rssi(self, rssi):
        self._rssi = rssi

    @property
    def id_wireless_bridge(self):
        return self._id_wireless_bridge

    @id_wireless_bridge.setter
    def id_wireless_bridge(self, id_wireless_bridge):
        self._id_wireless_bridge = id_wireless_bridge

    @property
    def product_id(self):
        return self._product_id

    @product_id.setter
    def product_id(self, product_id):
        self._product_id = product_id

    @property
    def model_identifier(self):
        return self._model_identifier

    @model_identifier.setter
    def model_identifier(self, model_identifier):
        self._model_identifier = model_identifier

    @property
    def manufacturer_name(self):
        return self._manufacturer_name

    @manufacturer_name.setter
    def manufacturer_name(self, manufacturer_name):
        self._manufacturer_name = manufacturer_name

    @property
    def application_firmware_version(self):
        return self._application_firmware_version

    @application_firmware_version.setter
    def application_firmware_version(self, application_firmware_version):
        self._application_firmware_version = application_firmware_version

    @property
    def application_hardware_version(self):
        return self._application_hardware_version

    @application_hardware_version.setter
    def application_hardware_version(self, application_hardware_version):
        self._application_hardware_version = application_hardware_version

    @property
    def product_serial_number(self):
        return self._product_serial_number

    @product_serial_number.setter
    def product_serial_number(self, product_serial_number):
        self._product_serial_number = product_serial_number

    @property
    def product_identifier(self):
        return self._product_identifier

    @product_identifier.setter
    def product_identifier(self, product_identifier):
        self._product_identifier = product_identifier

    @property
    def product_model(self):
        return self._product_model

    @product_model.setter
    def product_model(self, product_model):
        self._product_model = product_model
