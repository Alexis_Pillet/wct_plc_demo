
"""
core_engine
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Core module starting and stopping the processes of the programe

"""
import logging
from mac import MacProcess
from network import NetworkProcess
from application import ApplicationProcess
from logger import LoggerManager
from json_parser import JsonLogs


class CoreEngine:
    """Start and stop the layout processes"""

    def __init__(self):
        """Create instance of layout processes"""
        self._mac = MacProcess()
        self._network = NetworkProcess()
        self._application = ApplicationProcess()
        LoggerManager.set_log_level(logging.DEBUG)
        # logs => json
        self._logs_to_json = JsonLogs("../logs.json")

    @property
    def zigbee_device_container(self):
        return self._application.zigbee_device_container

    @property
    def logs_to_json(self):
        return self._logs_to_json

    def start(self):

        """Start the layout processes"""
        # Initialization/Start mac process
        self._mac.network_queue = self._network.queue
        self._mac.start()

        # Initialization/Start network process
        self._network.mac_frame_generator = self._mac.mac_frame_generator
        self._network.application_queue = self._application.queue
        self._network.start()

        # Initialization/Start application process
        self._application.network_frame_generator = self._network.network_frame_generator
        self._application.start()

    def stop(self):
        """Stop the layout processes"""
        self._mac.stop()
        self._network.stop()
        self._application.stop()

    def check_network_init(self):
        return self._mac.current_state == MacProcess.NETWORKED_STATE
