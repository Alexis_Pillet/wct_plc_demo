# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to model ZigBee Device
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from abc import ABC, abstractmethod


class ZigBeeDevice(ABC):

    def __init__(self, address, id_wireless_bridge):
        self._address = address
        self._id_wireless_bridge = id_wireless_bridge
        self._product_id = None
        self._model_identifier = None
        self._application_firmware_version = None
        self._application_hardware_version = None
        self._product_serial_number = None
        super(ZigBeeDevice, self).__init__()

    @abstractmethod
    def read_attribute(self, cluster_id, attribute_id):
        pass

    @abstractmethod
    def write_attribute(self, cluster_id, attribute_id, value):
        pass

    @abstractmethod
    def discovery(self):
        pass

    @abstractmethod
    def update_data(self, payload):
        pass

    @property
    def address(self):
        return self._address

    @address.setter
    def address(self, address):
        self._address = address

    @property
    def id_wireless_bridge(self):
        return self._id_wireless_bridge

    @id_wireless_bridge.setter
    def id_wireless_bridge(self, id_wireless_bridge):
        self._id_wireless_bridge = id_wireless_bridge

    @property
    def product_id(self):
        return self._product_id

    @product_id.setter
    def product_id(self, product_id):
        self._product_id = product_id

    @property
    def model_identifier(self):
        return self._model_identifier

    @model_identifier.setter
    def model_identifier(self, model_identifier):
        self._model_identifier = model_identifier

    @property
    def application_firmware_version(self):
        return self._application_firmware_version

    @application_firmware_version.setter
    def application_firmware_version(self, application_firmware_version):
        self._application_firmware_version = application_firmware_version

    @property
    def application_hardware_version(self):
        return self._application_hardware_version

    @application_hardware_version.setter
    def application_hardware_version(self, application_hardware_version):
        self._application_hardware_version = application_hardware_version

    @property
    def product_serial_number(self):
        return self._product_serial_number

    @product_serial_number.setter
    def product_serial_number(self, product_serial_number):
        self._product_serial_number = product_serial_number
