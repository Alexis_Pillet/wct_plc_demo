/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the ZDO primitives for the ZAB Pro ZNP vendor.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.01.00  27-Aug-13   MvdB   Alpha 1, migrated from old code.
 * 00.00.01.01  29-Aug-13   MvdB   artf24849: Fix problem with only one beacon getting reported
 * 00.00.02.01  27-Nov-13   MvdB   Support MGMT Routing Req/Rsp
 *                                 Update MGMT permit Join Req to use address mode
 * 00.00.03.01  19-Mar-14   MvdB   artf53864: Support mgmt leave request/response
 * 00.00.06.00  27-Jun-14   MvdB   ARTF60318: Add Power Descriptor Request
 *                                 ARTF69774: Fix user descriptor get/set response handling when not supported
 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 00.00.06.02  30-Sep-14   MvdB   artf55512: Support network key change action
 * 00.00.06.03  01-Oct-14   MvdB   artf104879: Support energy scans via Mgmt Nwk Update Request
 * 00.00.06.04  06-Oct-14   MvdB   artf56243: Handle local network processor leaves gracefully
 * 00.00.06_GP  15-Oct-14   MvdB   ARTF105860: Upgrade to TI permit Join indication, replacing =S=
 * 00.00.06.06  21-Oct-14   MvdB   artf106135: Handle network leave indications
 * 01.00.00.01  19-Jan-15   MvdB   Add support for TI's PJ Indication
 * 01.100.07.00 17-Feb-15   MvdB   ARTF70280: Fix zabZnp_ProcessMgmtBindRsp() MT processing for binds to groups
 * 002.000.001  03-Feb-15   MvdB   ARTF115770: Support ZDO Node Descriptor Request
 * 002.000.004  xx-Mar-15   MvdB   ARTF113871: IeeeAddrRsp/NwkAddrRsp: Clear start address if no associated device list.
 *                                 ARTF56504: Support missed channel change recovery, with actions/notifications/asks/gives to get/set parameters.
 * 002.000.008  15-Apr-15   MvdB   ARTF130755: Fix handling of SimpleDescRsp from invalid endpoint. Requires SZNP 2.0.5.
 * 002.001.001  29-Apr-15   MvdB   ARTF132254: Clean up type casts in vendor for IAR compiler
 * 002.002.015  21-Oct-15   MvdB   ARTF104106: Support SZL_ZDO_MatchDescriptorReq()
 * 002.002.044  23-Jan-17   MvdB   ARTF67562: ZNP: Speed up ZDO failure callbacks by reporting failure codes in ZDO SRSPs to SZL
 *****************************************************************************/

#include "zabZnpService.h"

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

#define ZNPI_ZDO_JOIN_REQ_DATALENGTH()  (sizeof(zabznpZdo_ZDO_JOIN_REQ))



/* Bit masks for Flags of Device Announce */
#define DEVIC_ANNCE_MASK_ALT_PAN_COORD 0x01
#define DEVIC_ANNCE_MASK_DEV_TYPE_FFD 0x02
#define DEVIC_ANNCE_MASK_POWER_SRC_MAINS 0x04
#define DEVIC_ANNCE_MASK_RX_ON_WHEN_IDLE 0x08
#define DEVIC_ANNCE_MASK_SECURITY_CAP 0x40
#define DEVIC_ANNCE_MASK_ALLOCATE_ADDR 0x80


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/


typedef enum
{
    /*****************/
    /* ZDO Interface */
    /*****************/

    ZNPI_CMD_ZDO_NWK_ADDR               = 0x00, /* ZDO_NWK_ADDR */
    ZNPI_CMD_ZDO_IEEE_ADDR              = 0x01, /* ZDO_IEEE_ADDR */
    ZNPI_CMD_ZDO_NODE_DESC              = 0x02, /* ZDO_NODE_DESC */
    ZNPI_CMD_ZDO_POWER_DESC             = 0x03, /* ZDO_POWER_DESC */
    ZNPI_CMD_ZDO_SIMPLE_DESC            = 0x04, /* ZDO_SIMPLE_DESC */
    ZNPI_CMD_ZDO_ACTIVE_EP              = 0x05, /* ZDO_ACTIVE_EP */
    ZNPI_CMD_ZDO_MATCH_DESC             = 0x06, /* ZDO_MATCH_DESC */
    ZNPI_CMD_ZDO_COMPLEX_DESC           = 0x07, /* ZDO_COMPLEX_DESC */
    ZNPI_CMD_ZDO_USER_DESC              = 0x08, /* ZDO_USER_DESC */
    ZNPI_CMD_ZDO_DEVICE_ANNCE           = 0x0A, /* ZDO_DEVICE_ANNCE */
    ZNPI_CMD_ZDO_USER_DESC_SET          = 0x0B, /* ZDO_USER_DESC_SET */
    ZNPI_CMD_ZDO_SERVER_DISC            = 0x0C, /* ZDO_SERVER_DISC */

    ZNPI_CMD_ZDO_END_DEVICE_BIND        = 0x20, /* ZDO_END_DEVICE_BIND */
    ZNPI_CMD_ZDO_BIND                   = 0x21, /* ZDO_BIND */
    ZNPI_CMD_ZDO_UNBIND                 = 0x22, /* ZDO_UNBIND */
    ZNPI_CMD_ZDO_SET_LINK_KEY           = 0x23, /* ZDO_SET_LINK_KEY */
    ZNPI_CMD_ZDO_REMOVE_LINK_KEY        = 0x24, /* ZDO_REMOVE_LINK_KEY */
    ZNPI_CMD_ZDO_GET_LINK_KEY           = 0x25, /* ZDO_GET_LINK_KEY */
    ZNPI_CMD_ZDO_NWK_DISC_REQ           = 0x26,
    ZNPI_CMD_ZDO_JOIN_REQ               = 0x27,

    ZNPI_CMD_ZDO_GET_PING_TIMES         = 0x2E,
    ZNPI_CMD_ZDO_SET_PING_TIMES         = 0x2F,

    ZNPI_CMD_ZDO_MGMT_NWK_DISC          = 0x30, /* ZDO_MGMT_NWK_DISC */
    ZNPI_CMD_ZDO_MGMT_LQI               = 0x31, /* ZDO_MGMT_LQI */
    ZNPI_CMD_ZDO_MGMT_RTG               = 0x32, /* ZDO_MGMT_RTG */
    ZNPI_CMD_ZDO_MGMT_BIND              = 0x33, /* ZDO_MGMT_BIND */
    ZNPI_CMD_ZDO_MGMT_LEAVE             = 0x34, /* ZDO_MGMT_LEAVE */
    ZNPI_CMD_ZDO_MGMT_DIRECT_JOIN       = 0x35, /* ZDO_MGMT_DIRECT_JOIN */
    ZNPI_CMD_ZDO_MGMT_PERMIT_JOIN       = 0x36, /* ZDO_MGMT_PERMIT_JOIN */
    ZNPI_CMD_ZDO_MGMT_NWK_UPDATE        = 0x37, /* ZDO_MGMT_NWK_UPDATE */





    /* TEMP TEMP TEMP EXPERIMENTAL CODE!!! */
    ZNPI_CMD_ZDO_NWK_DISC2_CLEANUP_REQ = 0x2B,
    ZNPI_CMD_ZDO_NWK_REJOIN_REQ        = 0x2C,
    ZNPI_CMD_ZDO_NWK_DISC2_REQ        = 0x2D,
    ZNPI_CMD_ZDO_TCLK_REQ        = 0x3D,
    /* END EXPERIMENTAL CODE!!! */





    ZNPI_CMD_ZDO_MSG_CB_REGISTER        = 0x3E, /* ZDO_MSG_CB_REGISTER */
    ZNPI_CMD_ZDO_MSG_CB_REMOVE          = 0x3F, /* ZDO_MSG_CB_REMOVE */
    ZNPI_CMD_ZDO_STARTUP_FROM_APP       = 0x40, /* ZDO_STARTUP_FROM_APP */
    ZNPI_CMD_ZDO_AUTO_FIND_DESTINATION  = 0x41, /* ZDO_AUTO_FIND_DESTINATION */

    ZNPI_CMD_ZDO_UPDATE_NWK_KEY        = 0x4E,
    ZNPI_CMD_ZDO_SWITCH_NWK_KEY        = 0x4F,


    ZNPI_CMD_ZDO_NWK_ADDR_IND           = 0x80, /* ZDO_NWK_ADDR */
    ZNPI_CMD_ZDO_IEEE_ADDR_IND          = 0x81, /* ZDO_IEEE_ADDR */
    ZNPI_CMD_ZDO_NODE_DESC_IND          = 0x82, /* ZDO_NODE_DESC */
    ZNPI_CMD_ZDO_POWER_DESC_IND         = 0x83, /* ZDO_POWER_DESC */
    ZNPI_CMD_ZDO_SIMPLE_DESC_IND        = 0x84, /* ZDO_SIMPLE_DESC */
    ZNPI_CMD_ZDO_ACTIVE_EP_IND          = 0x85, /* ZDO_ACTIVE_EP */
    ZNPI_CMD_ZDO_MATCH_DESC_IND         = 0x86, /* ZDO_MATCH_DESC */
    ZNPI_CMD_ZDO_COMPLEX_DESC_IND       = 0x87, /* ZDO_COMPLEX_DESC */
    ZNPI_CMD_ZDO_USER_DESC_IND          = 0x91, /* ZDO_USER_DESC */
    ZNPI_CMD_ZDO_USER_DESC_CONF_IND     = 0x94, /* ZDO_USER_DESC_CONF */
    ZNPI_CMD_ZDO_SERVER_DISC_IND        = 0x8A, /* ZDO_SERVER_DISC */

    ZNPI_CMD_ZDO_END_DEVICE_BIND_IND    = 0xA0, /* ZDO_END_DEVICE_BIND */
    ZNPI_CMD_ZDO_BIND_IND               = 0xA1, /* ZDO_BIND */
    ZNPI_CMD_ZDO_UNBIND_IND             = 0xA2, /* ZDO_UNBIND */

    ZNPI_CMD_ZDO_MGMT_NWK_DISC_IND      = 0xB0, /* ZDO_MGMT_NWK_DISC */
    ZNPI_CMD_ZDO_MGMT_LQI_IND           = 0xB1, /* ZDO_MGMT_LQI */
    ZNPI_CMD_ZDO_MGMT_RTG_IND           = 0xB2, /* ZDO_MGMT_RTG */
    ZNPI_CMD_ZDO_MGMT_BIND_IND          = 0xB3, /* ZDO_MGMT_BIND */
    ZNPI_CMD_ZDO_MGMT_LEAVE_IND         = 0xB4, /* ZDO_MGMT_LEAVE */
    ZNPI_CMD_ZDO_MGMT_DIRECT_JOIN_IND   = 0xB5, /* ZDO_MGMT_DIRECT_JOIN */
    ZNPI_CMD_ZDO_MGMT_PERMIT_JOIN_IND   = 0xB6, /* ZDO_MGMT_PERMIT_JOIN */
    ZNPI_CMD_ZDO_MGMT_NWK_UPDATE_IND    = 0xB8,

    ZNPI_CMD_ZDO_STATE_CHANGE_IND       = 0xC0, /* ZDO_STATE_CHANGE_IND */
    ZNPI_CMD_ZDO_END_DEVICE_ANNCE_IND   = 0xC1, /* ZDO_END_DEVICE_ANNCE_IND */
    ZNPI_CMD_ZDO_MATCH_DESC_RSP_SENT    = 0xC2, /* ZDO_MATCH_DESC_RSP_SENT */
    ZNPI_CMD_ZDO_STATUS_ERROR           = 0xC3, /* ZDO_STATUS_ERROR */
    ZNPI_CMD_ZDO_SRC_RTG_IND            = 0xC4, /* ZDO_SRC_RTG_IND */

    ZNPI_CMD_ZDO_BEACON_NOTIFY_IND      = 0xC5,
    ZNPI_CMD_ZDO_JOIN_CNF               = 0xC6,
    ZNPI_CMD_ZDO_NWK_DISCOVERY_CNF      = 0xC7,
    ZNPI_CMD_ZDO_NWK_LEAVE_IND          = 0xC9,
    ZNPI_CMD_ZDO_PERMIT_JOIN_IND        = 0xCB, /* Schneider command added under ARTF 24746 */
    ZNPI_CMD_ZDO_LEAVE_LOCAL_IND        = 0xCE, /* Schneider command added under ARTF 56243 */
    ZNPI_CMD_ZDO_PERMIT_JOIN_IND_SCHNEIDER = 0xCF, /* Schneider command added under ARTF 24746 - Kept for backwards compatibility with early ZNP's, can be removed at some time*/

    ZNPI_CMD_ZDO_MSG_CB_INCOMING_IND    = 0xFF, /* ZDO_MSG_CB_INCOMING */

} ZNPI_CMD_ZDO_COMMANDCODES;





//------------------------------------------------------------------------------
// Struct Status For All ZNP RESPONSE
// - ZDO_...
typedef struct s_znpZdoStatus
{
    unsigned8 status;       // Status
} zabznpZdo_StatusResp;

//------------------------------------------------------------------------------
// Struct for ZNP MESSAGE :
//  - ZDO_STATE_CHANGE_IND
/*
 * Struct ZNP of ZDO state change
 */
typedef struct s_znpZdoStateChange
{
    unsigned8 state; //  ZDO State
} zabznpZdo_ZDO_STATE_CHANGE_IND;

//------------------------------------------------------------------------------
// Struct for ZNP MESSAGE :
//  - ZDO_IEEE_ADDR_REQ
/*
 * Struct ZNP of ZDO request a device IEEE 64-bit address.
 */
typedef struct s_znpZdoIEEEAddrReq
{
    unsigned8 shortAddr[2]; //  Short address of the device
    unsigned8 reqType;      //
    unsigned8 startIndex;   // Starting index indto the list of chidren
    unsigned8 tid;
} zabznpZdo_ZDO_IEEE_ADDR_REQ;

//------------------------------------------------------------------------------
// Struct for ZNP MESSAGE :
//  - ZDO_JOIN_REQ
/*
 * Struct ZNP of ZDO request the device to join itsetf to a parent device on a network
 */
typedef struct s_znpZdoJoinReq
{
    unsigned8 logicalChannel;
    unsigned8 panId[2];
    unsigned8 ExPanId[8];
    unsigned8 chosenParent[2];
    unsigned8 parentDepth;
    unsigned8 stackProfile;
} zabznpZdo_ZDO_JOIN_REQ;

//------------------------------------------------------------------------------
// Struct for ZNP MESSAGE :
//  - ZDO_STARTUP_FROM_APP
/*
 * Struct ZNP of ZDO Starts the device in the network
 */
typedef struct s_znpZdoStartupFromApp
{
    unsigned8 startDelay[2];
} zabznpZdo_ZDO_STARTUP_FROM_APP;

//------------------------------------------------------------------------------
// Struct for ZNP MESSAGE :
//  - ZDO_NWK_DISC_REQ
/*
 * Struct ZNP of ZDO initiate a network discovery (action scan)
 */
typedef struct s_znpZdoNwkDiscReq
{
    unsigned8 scanChannel[4];
    unsigned8 scanDuration;
} zabznpZdo_ZDO_NWK_DISC_REQ;

/*
 * Struct ZNP of ZDO MGMT Permit Join Request
 */
typedef struct
{
    unsigned8 AddressMode;
    unsigned8 DesinationAddress[2];
    unsigned8 Duration;
    unsigned8 TCSignificance;
    unsigned8 Tid;
} zabznpZdo_MGMT_PERMIT_JOIN_REQ;

/* Device Announce ZNP Command Format*/
typedef struct
{
    unsigned8 nwkAddr[2];
    unsigned8 ieeeAddr[8];
    unsigned8 capabilities;
} znpDeviceAnnounceFormat;

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

static void zabZnp_ProcessChangeStateIndication(erStatus* Status, zabService* Service, sapMsg* Message)
{
    ZDO_STATE ZDO_State = DEV_HOLD;
    zabznpZdo_ZDO_STATE_CHANGE_IND *msgData;
    unsigned8 msgLen;

    zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
    ER_CHECK_STATUS(Status);

    if (msgLen >= sizeof(zabznpZdo_ZDO_STATE_CHANGE_IND))
      {
        ZDO_State = (ZDO_STATE)msgData->state;
        printVendor(Service, "\n**********ZDO_State changed!!! ZDO_State=%d = %s************\n\n", (unsigned8)ZDO_State, zabZnpZdoUtility_GetZdoStateString( ZDO_State ) );
        zabZnpNwk_zdoStateChangeHandler(Status, Service, ZDO_State);
      }
}


static void getZdoStatus( erStatus* Status, sapMsg* Message, unsigned8* status )
{
    zabznpZdo_StatusResp *data;
    unsigned8 len;

    ER_CHECK_STATUS_NULL(Status, status);

    zabParseZNPGetData(Status, Message, &len, (unsigned8 **)&data);

    ER_CHECK_STATUS(Status);

    if (len >= sizeof(*data))
    {
        *status = data->status;
    }
    else
    {
        erStatusSet( Status, ZAB_ERROR_VENDOR_PARSE);
    }
}




/******************************************************************************
 * ZDO IEEE Address Request
 ******************************************************************************/
static
void zabZnpZdoUtility_IeeeAddrReq( erStatus* Status, zabService* Service, unsigned16 nwkAddr, unsigned8 reqType, unsigned8 startIndex, unsigned8 tid)
{
  sapMsg* Msg = NULL;
  zabznpZdo_ZDO_IEEE_ADDR_REQ *msgData;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, sizeof(zabznpZdo_ZDO_IEEE_ADDR_REQ));
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_IEEE_ADDR);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */

  /* These functions are a waste of time if they are only used once:
   *  - They make many more lines of code to maintain
   *  - They are inefficeint - 3 status checks, pointer look ups and settings of the length
  zabParseZNPZdo_SetShortAddr(Status, Msg, nwkAddr);
  zabParseZNPZdo_SetReqType(Status, Msg, reqType);
  zabParseZNPZdo_SetStartIndex(Status, Msg, startIndex);*/

  msgData = (zabznpZdo_ZDO_IEEE_ADDR_REQ*)ZAB_MSG_ZNP_DATA(Msg); // (zabznpZdo_ZDO_IEEE_ADDR_REQ*)(ZAB_MSG_ZNP(Msg)->Data);
  zabParseZNPSetLength(Status, Msg, sizeof(zabznpZdo_ZDO_IEEE_ADDR_REQ));
  COPY_OUT_16_BITS(msgData->shortAddr, nwkAddr);
  msgData->reqType = reqType;
  msgData->startIndex = startIndex;
  msgData->tid = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO IEEE Address Request
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_IeeeMsgConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoIeeeAddrReqParams_t* dp;

  dp = (SZL_ZdoIeeeAddrReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  zabZnpZdoUtility_IeeeAddrReq(Status, Service, dp->NetworkAddress, (unsigned8)dp->RequestType, dp->StartIndex, sapMsgGetAppTransactionId(Message));
}

/******************************************************************************
 * ZDO IEEE Address Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
#define M_IEEE_ADDR_RSP_MAX_DEVICES 35
static void zabZnp_ProcessIeeeAddrRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  unsigned8 device;
  SZL_ZdoAddrRespParams_t* ieeeRsp;
  unsigned8 tid = 0;
  unsigned8 offset;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

  /* Check there is atleast the minimum frame length in the data*/
  if (znpDataLength >= 11)
    {
      // Allocate New Message
      length = SZL_ZdoAddrRespParams_t_SIZE(M_IEEE_ADDR_RSP_MAX_DEVICES);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_IEEE_RSP);

      /* Extract from the ZNP command into the data sap structure */

      ieeeRsp = (SZL_ZdoAddrRespParams_t*)sapMsgGetAppData(dataMessage);
      ieeeRsp->Status = (SZL_ZDO_STATUS_t)dp[0];
      ieeeRsp->IeeeAddr = COPY_IN_64_BITS(&dp[1]);
      ieeeRsp->NetworkAddress = COPY_IN_16_BITS(&dp[9]);

      /* Associated device info is only included if status == success */
      if ( (ieeeRsp->Status == SZL_ZDO_STATUS_SUCCESS) && (znpDataLength >= 13) )
        {
          ieeeRsp->StartIndex = dp[11];
          ieeeRsp->NumAssocDev = dp[12];
          /* ARTF113871: Clear start address if no associated device list. As the value provided by stack is uninitialised */
          if (ieeeRsp->NumAssocDev == 0)
            {
              ieeeRsp->StartIndex = 0;
            }

          if (ieeeRsp->NumAssocDev > M_IEEE_ADDR_RSP_MAX_DEVICES)
            {
              printError(Service, "ZNP ZDO: Warning: Max num devices %d exceeds %d, truncating\n", ieeeRsp->NumAssocDev, M_IEEE_ADDR_RSP_MAX_DEVICES);
              ieeeRsp->NumAssocDev = M_IEEE_ADDR_RSP_MAX_DEVICES;
            }

          offset=13;
          for (device = 0; ( (device < ieeeRsp->NumAssocDev) && (znpDataLength >= (15 + (2*device))) ); device++)
            {
              ieeeRsp->AssocDevNetworkAddresses[device] = COPY_IN_16_BITS(&dp[offset]); offset+=2;
            }
          /* Set back to the actual number of devices we managed to add - handles the case where data is shorter then indicated by numAssociatedDevices*/
          ieeeRsp->NumAssocDev = device;

          if (offset < znpDataLength)
            {
              tid = dp[offset];
            }
        }
      else
        {
          ieeeRsp->StartIndex = 0;
          ieeeRsp->NumAssocDev = 0;
        }

      sapMsgSetAppDataLength(Status, dataMessage, SZL_ZdoAddrRespParams_t_SIZE(ieeeRsp->NumAssocDev));
      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}


/******************************************************************************
 * ZDO Network Rejoin Request
 * TEMP TEMP TEMP - EXPERIMENTAL CODE
 ******************************************************************************/
void zabZnpZdoUtility_NwkRejoinReq( erStatus* Status, zabService* Service, unsigned8 Secure, unsigned8 Channel)
{
  sapMsg* Msg = NULL;
  unsigned16 dataLength = 2;
  unsigned8* msgData;

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, dataLength);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, (unsigned8)dataLength);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_NWK_REJOIN_REQ);

  /* load the data area */
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  *msgData++ = Secure;
  *msgData++ = Channel;

  zabZnpService_SendMsg(Status, Service, Msg);
}
/******************************************************************************
 * ZDO Network Discovery Request 2
 * TEMP TEMP TEMP - EXPERIMENTAL CODE
 ******************************************************************************/
void zabZnpZdoUtility_NwkDisc2Req( erStatus* Status, zabService* Service)
{
  sapMsg* Msg = NULL;
  unsigned16 dataLength = 0;

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, dataLength);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, (unsigned8)dataLength);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_NWK_DISC2_REQ);

  /* load the data area */
/*
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  *msgData++ = Secure;
  *msgData++ = Channel;
*/

  zabZnpService_SendMsg(Status, Service, Msg);
}
/******************************************************************************
 * ZDO Network Discovery Request 2 Cleanup
 * TEMP TEMP TEMP - EXPERIMENTAL CODE
 ******************************************************************************/
void zabZnpZdoUtility_NwkDisc2CleanupReq( erStatus* Status, zabService* Service)
{
  sapMsg* Msg = NULL;
  unsigned16 dataLength = 0;

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, dataLength);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, (unsigned8)dataLength);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_NWK_DISC2_CLEANUP_REQ);

  /* load the data area */
/*
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  *msgData++ = Secure;
  *msgData++ = Channel;
*/

  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * ZDO Network Key Update Request
 * This sends a APS Transport Key command with a new network key.
 ******************************************************************************/
void zabZnpZdoUtility_NwkKeyUpdateReq(erStatus* Status, zabService* Service,
                                      unsigned16 DestinationAddress,
                                      unsigned8 KeySequenceNumber,
                                      unsigned8* NetworkKey)
{
  sapMsg* Msg = NULL;
  unsigned8 *msgData;
  #define M_NWK_KEY_UPDATE_REQ_DATA_LENGTH (ZAB_ZNP_ZDO_M_KEY_LENGTH + 3)

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_NWK_KEY_UPDATE_REQ_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_NWK_KEY_UPDATE_REQ_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_UPDATE_NWK_KEY);

  /* load the data area */
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(msgData, DestinationAddress); msgData += MACRO_DATA_SIZE_WORD;
  *msgData++ = KeySequenceNumber;
  osMemCopy( Status, msgData, NetworkKey, ZAB_ZNP_ZDO_M_KEY_LENGTH);

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO Network Key Switch Request
 * This sends a APS Switch Key command with the parametised Key Sequence Number
 ******************************************************************************/
void zabZnpZdoUtility_NwkKeySwitchReq(erStatus* Status, zabService* Service,
                                      unsigned16 DestinationAddress,
                                      unsigned8 KeySequenceNumber)
{
  sapMsg* Msg = NULL;
  unsigned8 *msgData;
  #define M_NWK_KEY_SWITCH_REQ_DATA_LENGTH 3

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_NWK_KEY_SWITCH_REQ_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_NWK_KEY_SWITCH_REQ_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_SWITCH_NWK_KEY);

  /* load the data area */
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(msgData, DestinationAddress); msgData += MACRO_DATA_SIZE_WORD;
  *msgData++ = KeySequenceNumber;

  zabZnpService_SendMsg(Status, Service, Msg);
}



/******************************************************************************
 * ZDO TCLK Request
 * TEMP TEMP TEMP - EXPERIMENTAL CODE
 ******************************************************************************/
void zabZnpZdoUtility_TclkReq( erStatus* Status, zabService* Service)
{
  sapMsg* Msg = NULL;
  #define M_TCLK_REQ_DATA_LENGTH 0

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_TCLK_REQ_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_TCLK_REQ_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_TCLK_REQ);

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO Network Address Request
 ******************************************************************************/
static
void zabZnpZdoUtility_NwkAddrReq( erStatus* Status, zabService* Service, unsigned64 ieeeAddr, unsigned8 reqType, unsigned8 startIndex, unsigned8 tid)
{
  sapMsg* Msg = NULL;
  unsigned8 *msgData;
  #define M_NWK_ADDR_REQ_DATA_LENGTH 11

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_NWK_ADDR_REQ_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_NWK_ADDR_REQ_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_NWK_ADDR);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_64_BITS(msgData, ieeeAddr); msgData+=8;
  *msgData++ = reqType;
  *msgData++ = startIndex;
  *msgData++ = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO Network Address Request
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_NwkMsgConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoNwkAddrReqParams_t* dp;

  dp = (SZL_ZdoNwkAddrReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  zabZnpZdoUtility_NwkAddrReq(Status, Service, dp->IeeeAddress, dp->RequestType, dp->StartIndex, sapMsgGetAppTransactionId(Message));
}


/******************************************************************************
 * ZDO Nwk Address Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
#define M_IEEE_ADDR_RSP_MAX_DEVICES 35
static void zabZnp_ProcessNwkAddrRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  unsigned8 device;
  unsigned8 tid = 0;
  unsigned8 offset;
  SZL_ZdoAddrRespParams_t* rsp;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

  /* Check there is at least the minimum frame length in the data*/
  if (znpDataLength >= 11)
    {
      // Allocate New Message
      length = SZL_ZdoAddrRespParams_t_SIZE(M_IEEE_ADDR_RSP_MAX_DEVICES);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_NWK_RSP);

      /* Extract from the ZNP command into the data sap structure */

      rsp = (SZL_ZdoAddrRespParams_t*)sapMsgGetAppData(dataMessage);
      rsp->Status = dp[0];
      rsp->IeeeAddr = COPY_IN_64_BITS(&dp[1]);
      rsp->NetworkAddress = COPY_IN_16_BITS(&dp[9]);

      /* Associated device info is only included if status == success */
      if ( (rsp->Status == ZNPI_API_ERR_SUCCESS) && (znpDataLength >= 13) )
        {
          rsp->StartIndex = dp[11];
          rsp->NumAssocDev = dp[12];
          /* ARTF113871: Clear start address if no associated device list. As the value provided by stack is uninitialised */
          if (rsp->NumAssocDev == 0)
            {
              rsp->StartIndex = 0;
            }

          if (rsp->NumAssocDev > M_IEEE_ADDR_RSP_MAX_DEVICES)
            {
              printError(Service, "ZNP ZDO: Warning: Max num devices %d exceeds %d, truncating\n", rsp->NumAssocDev, M_IEEE_ADDR_RSP_MAX_DEVICES);
              rsp->NumAssocDev = M_IEEE_ADDR_RSP_MAX_DEVICES;
            }

          offset=13;
          for (device = 0; ( (device < rsp->NumAssocDev) && (znpDataLength >= (15 + (2*device))) ); device++)
            {
              rsp->AssocDevNetworkAddresses[device] = COPY_IN_16_BITS(&dp[offset]); offset+=2;
            }
          /* Set back to the actual number of devices we managed to add - handles the case where data is shorter then indicated by numAssociatedDevices*/
          rsp->NumAssocDev = device;

          if (offset < znpDataLength)
            {
            tid = dp[offset];
            }
        }
      else
        {
          rsp->StartIndex = 0;
          rsp->NumAssocDev = 0;
        }

      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      sapMsgSetAppDataLength(Status, dataMessage,  SZL_ZdoAddrRespParams_t_SIZE(rsp->NumAssocDev));
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}


/******************************************************************************
 * ZDO Power Descriptor Request
 ******************************************************************************/
static
void zabZnpZdoUtility_PowerDescReq( erStatus* Status, zabService* Service, unsigned16 nwkAddr, unsigned16 nwkAddrOfInterest, unsigned8 tid)
{
  #define M_POWER_DESC_DATA_LENGTH 5
  sapMsg* Msg = NULL;
  unsigned8* buffer;
  unsigned16 sendCount;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_POWER_DESC_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_POWER_DESC_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_POWER_DESC);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  sendCount = 0;
  buffer = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddr); sendCount += MACRO_DATA_SIZE_WORD;
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddrOfInterest); sendCount += MACRO_DATA_SIZE_WORD;
  buffer[sendCount] = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO Power Descriptor Request
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_PowerDescReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoPowerDescriptorReqParams_t* dp;

  dp = (SZL_ZdoPowerDescriptorReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  zabZnpZdoUtility_PowerDescReq(Status, Service, dp->NetworkAddress, dp->NetworkAddressOfInterest, sapMsgGetAppTransactionId(Message));
}


/******************************************************************************
 * ZDO Power Descriptor Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
static void zabZnp_ProcessPowerDescRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 length;
  unsigned8 tid = 0;
  sapMsg* dataMessage = NULL;
  SZL_ZdoPowerDescriptorRespParams_t* rsp;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

  /* Check there is at least the minimum frame length in the data*/
  if (znpDataLength >= 5)
    {

      // Allocate New Message
      length = SZL_ZdoPowerDescriptorRespParams_t_SIZE;
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_POWER_DESC_RSP);

      /* Extract from the ZNP command into the data sap structure */

      rsp = (SZL_ZdoPowerDescriptorRespParams_t*)sapMsgGetAppData(dataMessage);
      rsp->SourceAddress = COPY_IN_16_BITS(&dp[0]);
      rsp->Status = dp[2];
      rsp->NetworkAddressOfInterest = COPY_IN_16_BITS(&dp[3]);

      /* Power Descriptor is only included if status == success */
      if ( (rsp->Status == ZNPI_API_ERR_SUCCESS) && (znpDataLength >= 7) )
        {
          rsp->CurrentPowerMode = dp[5] & 0x0F;
          rsp->AvailablePowerSources = (dp[5]>>4) & 0x0F;
          rsp->CurrentPowerSource = dp[6] & 0x0F;
          rsp->CurrentPowerSourceLevel = (dp[6]>>4) & 0x0F;

          if (znpDataLength >= 8)
            {
              tid = dp[7];
            }
        }
      else
        {
          /* Something went wrong with the length - setup defaults */
          rsp->AvailablePowerSources = SZL_AVAILABLE_POWER_SOURCE_UNKNOWN;
          rsp->CurrentPowerMode = SZL_ZDO_CURRENT_POWER_MODE_RX_ON_WHEN_IDLE;
          rsp->CurrentPowerSourceLevel = SZL_CURRENT_POWER_SOURCE_LEVEL_100_PERCENT;
          rsp->CurrentPowerSource = SZL_CURRENT_POWER_SOURCE_UNKNWON;
        }

      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      sapMsgSetAppDataLength(Status, dataMessage,  length);
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}



/******************************************************************************
 * ZDO Node Descriptor Request
 ******************************************************************************/
static
void zabZnpZdoUtility_NodeDescReq( erStatus* Status, zabService* Service, unsigned16 nwkAddr, unsigned16 nwkAddrOfInterest, unsigned8 tid)
{
  #define M_NODE_DESC_DATA_LENGTH 5
  sapMsg* Msg = NULL;
  unsigned8* buffer;
  unsigned16 sendCount;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_NODE_DESC_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_NODE_DESC_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_NODE_DESC);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  sendCount = 0;
  buffer = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddr); sendCount += MACRO_DATA_SIZE_WORD;
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddrOfInterest); sendCount += MACRO_DATA_SIZE_WORD;
  buffer[sendCount] = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO Node Descriptor Request
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_NodeDescReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoNodeDescriptorReqParams_t* dp;

  dp = (SZL_ZdoNodeDescriptorReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  zabZnpZdoUtility_NodeDescReq(Status, Service, dp->NetworkAddress, dp->NetworkAddressOfInterest, sapMsgGetAppTransactionId(Message));
}


/******************************************************************************
 * ZDO Node Descriptor Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
static void zabZnp_ProcessNodeDescRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 length;
  unsigned8 tid = 0;
  sapMsg* dataMessage = NULL;
  SZL_ZdoNodeDescriptorRespParams_t* rsp;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

  /* Check there is at least the minimum frame length in the data*/
  if (znpDataLength >= 5)
    {

      // Allocate New Message
      length = SZL_ZdoNodeDescriptorRespParams_t_SIZE;
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      ER_CHECK_NULL( Status, dataMessage );
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_NODE_DESC_RSP);

      /* Extract from the ZNP command into the data sap structure */

      rsp = (SZL_ZdoNodeDescriptorRespParams_t*)sapMsgGetAppData(dataMessage);
      rsp->SourceAddress = COPY_IN_16_BITS(&dp[0]);
      rsp->Status = dp[2];
      rsp->NetworkAddressOfInterest = COPY_IN_16_BITS(&dp[3]);

      /* Power Descriptor is only included if status == success */
      if ( (rsp->Status == ZNPI_API_ERR_SUCCESS) && (znpDataLength >= 18) )
        {
          rsp->Option.optWord = COPY_IN_16_BITS(&dp[5]);
          rsp->MACCapabilityFlag.optByte = dp[7];
          rsp->ManufacturerCode = COPY_IN_16_BITS(&dp[8]);
          rsp->MaximumBufferSize = dp[10];
          rsp->MaximumIncomingTransferSize = COPY_IN_16_BITS(&dp[11]);
          rsp->ServerMask.optWord = COPY_IN_16_BITS(&dp[13]);
          rsp->MaximumOutgoingTransferSize = COPY_IN_16_BITS(&dp[15]);
          rsp->DescriptorCapability.optByte = dp[17];

          if (znpDataLength >= 19)
            {
              tid = dp[18];
            }
        }
      else
        {
           /* Something went wrong with the length - setup defaults */
            rsp->Option.optBits.LogicalType = SZL_ZDO_MGMT_LQI_DEVICE_UNKNOWN;
            rsp->Option.optBits.ComplexDescriptorAvailable = NOT_SUPPORTED;
            rsp->Option.optBits.UserDescriptorAvailable = NOT_SUPPORTED;
            rsp->Option.optBits.FrequencyBand = SZL_ZDO_FEQUENCY_BAND_UNKNOWN;
            rsp->MACCapabilityFlag.optBits.AllocateAddress = NOT_SUPPORTED;
            rsp->MACCapabilityFlag.optBits.AlternatePANCoordinator = NOT_SUPPORTED;
            rsp->MACCapabilityFlag.optBits.PowerSource = NOT_SUPPORTED;
            rsp->MACCapabilityFlag.optBits.ReceiverOnWhenIdle = NOT_SUPPORTED;
            rsp->MACCapabilityFlag.optBits.SecurityCapability = NOT_SUPPORTED;
            rsp->MACCapabilityFlag.optBits.DeviceType = 0;
            rsp->ManufacturerCode = DEFAULT_MANUFACTURER_CODE;
            rsp->MaximumBufferSize = DEFAULT_SIZE;
            rsp->MaximumIncomingTransferSize = DEFAULT_SIZE;
            rsp->ServerMask.optBits.BackupBindingTableCache = NOT_SUPPORTED;
            rsp->ServerMask.optBits.BackupDiscoveryCache = NOT_SUPPORTED;
            rsp->ServerMask.optBits.BackupTrustCenter = NOT_SUPPORTED;
            rsp->ServerMask.optBits.NetworkManager = NOT_SUPPORTED;
            rsp->ServerMask.optBits.PrimaryBindingTableCache = NOT_SUPPORTED;
            rsp->ServerMask.optBits.PrimaryDiscoveryCache = NOT_SUPPORTED;
            rsp->ServerMask.optBits.PrimaryTrustCenter = NOT_SUPPORTED;
            rsp->MaximumOutgoingTransferSize = DEFAULT_SIZE;
            rsp->DescriptorCapability.optBits.ExtendedActiveEndpointListAvailable = NOT_SUPPORTED;
            rsp->DescriptorCapability.optBits.ExtendedSimpleDescriptorListAvailable = NOT_SUPPORTED;
        }

      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      sapMsgSetAppDataLength(Status, dataMessage,  length);
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}


/******************************************************************************
 * ZDO Active Endpoint Request
 ******************************************************************************/
static
void zabZnpZdoUtility_ActiveEndpointReq( erStatus* Status, zabService* Service, unsigned16 nwkAddr, unsigned16 nwkAddrOfInterest, unsigned8 tid)
{
  #define M_ACTIVE_EP_DATA_LENGTH 5
  sapMsg* Msg = NULL;
  unsigned8* buffer;
  unsigned16 sendCount;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_ACTIVE_EP_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_ACTIVE_EP_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_ACTIVE_EP);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  sendCount = 0;
  buffer = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddr); sendCount += MACRO_DATA_SIZE_WORD;
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddrOfInterest); sendCount += MACRO_DATA_SIZE_WORD;
  buffer[sendCount] = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO Active Endpoint Request
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_ActiveEndpointReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned16 length;
  SZL_ZdoActiveEndpointReqParams_t* dp;

  /* Confirm data length is correct before using it */
  length = sapMsgGetAppDataLength(Message);
  ER_CHECK_EQUAL(Status, length, sizeof(SZL_ZdoActiveEndpointReqParams_t));

  dp = (SZL_ZdoActiveEndpointReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  zabZnpZdoUtility_ActiveEndpointReq(Status, Service, dp->NetworkAddress, dp->NetworkAddressOfInterest, sapMsgGetAppTransactionId(Message));
}

/******************************************************************************
 * ZDO Active Endpoint Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
#define M_MAX_ACTIVE_EPS 77
static void zabZnp_ProcessActiveEndpointRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  sapMsg* dataMessage = NULL;
  unsigned8 endpointCount;
  unsigned8 tid = 0;
  SZL_ZdoActiveEndpointRespParams_t* activeEpRsp;
  unsigned8 length;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

  /* Validate length before using data */
  if (znpDataLength >= 6)
    {
      endpointCount = dp[5];
      if (endpointCount > M_MAX_ACTIVE_EPS)
        {
          printError(Service, "ZNP ZDO: Warning: Max num endpoints %d exceeds %d, truncating\n", endpointCount, M_MAX_ACTIVE_EPS);
          endpointCount = M_MAX_ACTIVE_EPS;
        }

      // Allocate New Message
      length = SZL_ZdoActiveEndpointRespParams_t_SIZE(endpointCount);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_ACTIVE_EP_RSP);

      /* Extract from the ZNP command into the data sap structure */
      activeEpRsp = (SZL_ZdoActiveEndpointRespParams_t*)sapMsgGetAppData(dataMessage);
      activeEpRsp->Status = (SZL_ZDO_STATUS_t)dp[2];
      // activeEpRsp->nwkAddress = COPY_IN_16_BITS(&dp[0]);
      activeEpRsp->NetworkAddressOfInterest = COPY_IN_16_BITS(&dp[3]);

      if (znpDataLength >= (6 + endpointCount))
        {
          activeEpRsp->ActiveEndpointCount = endpointCount;
          osMemCopy( Status, activeEpRsp->ActiveEndpointList, &dp[6], endpointCount);

          if (znpDataLength > (6 + endpointCount))
            {
              tid = dp[6+endpointCount];
            }
        }
      else
        {
          activeEpRsp->ActiveEndpointCount = 0;
          length = SZL_ZdoActiveEndpointRespParams_t_SIZE(activeEpRsp->ActiveEndpointCount);
        }

      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      sapMsgSetAppDataLength(Status, dataMessage, length);
      sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}



/******************************************************************************
 * ZDO Simple Descriptor Request
 ******************************************************************************/
static
void zabZnpZdoUtility_SimpleDescriptorReq( erStatus* Status, zabService* Service, unsigned16 nwkAddr, unsigned16 nwkAddrOfInterest, unsigned8 endpoint, unsigned8 tid)
{
  #define M_SIMPLE_DESC_DATA_LENGTH 6
  sapMsg* Msg = NULL;
  unsigned8* buffer;
  unsigned16 sendCount;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_SIMPLE_DESC_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_SIMPLE_DESC_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_SIMPLE_DESC);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  sendCount = 0;
  buffer = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddr); sendCount += MACRO_DATA_SIZE_WORD;
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddrOfInterest); sendCount += MACRO_DATA_SIZE_WORD;
  buffer[sendCount++] = endpoint;
  buffer[sendCount++] = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * ZDO Simple Descriptor Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_SimpleDescriptorReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned16 length;
  SZL_ZdoSimpleDescriptorReqParams_t* req;

  /* Confirm data length is correct before using it */
  length = sapMsgGetAppDataLength(Message);
  ER_CHECK_EQUAL(Status, length, sizeof(SZL_ZdoSimpleDescriptorReqParams_t));

  req = (SZL_ZdoSimpleDescriptorReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, req);

  zabZnpZdoUtility_SimpleDescriptorReq(Status, Service, req->NetworkAddress, req->NetworkAddressOfInterest, req->Endpoint, sapMsgGetAppTransactionId(Message));
}


/******************************************************************************
 * ZDO Simple Descriptor Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
#define M_MAX_NUMBER_OF_CLUSTERS 32
static void zabZnp_ProcessSimpleDescriptorRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  unsigned8 cluster;
  unsigned8 tid = 0;
  unsigned8 descriptorLength;
  SZL_ZdoSimpleDescriptorRespParams_t* simpleDescRsp;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

  /* Validate length before using data */
  if (znpDataLength >= 6)
    {
      // Allocate New Message - allocating space for M_MAX_NUMBER_OF_CLUSTERS is slightly inefficient, but simpler
      length = SZL_ZdoSimpleDescriptorRespParams_t_SIZE(M_MAX_NUMBER_OF_CLUSTERS, 0);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_SIMPLE_DESC_RSP);

      simpleDescRsp = (SZL_ZdoSimpleDescriptorRespParams_t*)sapMsgGetAppData(dataMessage);

      /* Extract from the ZNP command into the data sap structure */
      simpleDescRsp->NetworkAddress = COPY_IN_16_BITS(&dp[0]);
      simpleDescRsp->Status = (SZL_ZDO_STATUS_t)dp[2];
      simpleDescRsp->NetworkAddressOfInterest = COPY_IN_16_BITS(&dp[3]);
      descriptorLength = dp[5];

      // Pick our the Transaction Id Extension if it is there
      if (znpDataLength > (6 + descriptorLength))
        {
          tid = dp[6 + descriptorLength];
        }

      simpleDescRsp->NumInClusters= 0;
      simpleDescRsp->NumOutClusters = 0;

      /* Descriptor is only included if status is success */
      if (simpleDescRsp->Status == SZL_ZDO_STATUS_SUCCESS)
        {
          if ( (znpDataLength >= 14) && (descriptorLength >= 8) )
            {
              simpleDescRsp->Endpoint = dp[6];
              simpleDescRsp->ProfileId = COPY_IN_16_BITS(&dp[7]);
              simpleDescRsp->DeviceId = COPY_IN_16_BITS(&dp[9]);
              simpleDescRsp->DeviceVersion = dp[11];
              simpleDescRsp->NumInClusters = dp[12];
              simpleDescRsp->InClusterList = simpleDescRsp->ClusterList;
              simpleDescRsp->NumOutClusters = dp[13+(2*simpleDescRsp->NumInClusters)];
              simpleDescRsp->OutClusterList = &simpleDescRsp->ClusterList[simpleDescRsp->NumInClusters];

              /* Check there are not too many clusters for the memory allocated, and that we have enough data for however many there are meant to be */
              if ( ((simpleDescRsp->NumInClusters + simpleDescRsp->NumOutClusters) > M_MAX_NUMBER_OF_CLUSTERS) ||
                   (znpDataLength < (14 + (2 *(simpleDescRsp->NumInClusters + simpleDescRsp->NumOutClusters)))) )
                {
                  printError(Service, "ZNP ZDO: ERROR: Number of clusters exceeds %d, truncating\n", M_MAX_NUMBER_OF_CLUSTERS);
                  /* Keep it simple for now - can deliver some cluster later if we want */
                  simpleDescRsp->NumInClusters = 0;
                  simpleDescRsp->NumOutClusters = 0;
                }

              /* Add the cluster lists */
              for (cluster = 0; cluster < simpleDescRsp->NumInClusters; cluster++)
                {
                  simpleDescRsp->ClusterList[cluster] = COPY_IN_16_BITS(&dp[13+(2*cluster)]);
                }
              for (cluster = 0; cluster < simpleDescRsp->NumOutClusters; cluster++)
                {
                  simpleDescRsp->ClusterList[simpleDescRsp->NumInClusters + cluster] = COPY_IN_16_BITS(&dp[14+(2*(simpleDescRsp->NumInClusters + cluster))]);
                }
            }
          else
            {
              simpleDescRsp->Status = SZL_ZDO_STATUS_INSUFFICIENT_SPACE;
            }
        }

      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      // Set actual data length and send upwards
      sapMsgSetAppDataLength(Status, dataMessage,  SZL_ZdoSimpleDescriptorRespParams_t_SIZE(simpleDescRsp->NumInClusters, simpleDescRsp->NumOutClusters));
      sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}


/******************************************************************************
 * ZDO Match Descriptor Request
 ******************************************************************************/
static
void zabZnpZdoUtility_MatchDescriptorReq(erStatus* Status, zabService* Service,
                                         unsigned16 nwkAddr,
                                         unsigned16 nwkAddrOfInterest,
                                         unsigned16 profileId,
                                         unsigned8 numInClusters,
                                         unsigned16* inClusterList,
                                         unsigned8 numOutClusters,
                                         unsigned16* outClusterList,
                                         unsigned8 tid)
{
  #define M_MATCH_DESC_DATA_LENGTH(_num_clusters) (9 + ((_num_clusters)*sizeof(unsigned16)))
  sapMsg* Msg = NULL;
  unsigned8* buffer;
  unsigned16 sendCount;
  unsigned8 index;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_MATCH_DESC_DATA_LENGTH(numInClusters+numOutClusters));
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_MATCH_DESC_DATA_LENGTH(numInClusters+numOutClusters));
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_MATCH_DESC);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  sendCount = 0;
  buffer = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddr); sendCount += MACRO_DATA_SIZE_WORD;
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddrOfInterest); sendCount += MACRO_DATA_SIZE_WORD;
  COPY_OUT_16_BITS(&buffer[sendCount], profileId); sendCount += MACRO_DATA_SIZE_WORD;
  buffer[sendCount++] = numInClusters;
  for (index = 0; index < numInClusters; index++)
    {
      COPY_OUT_16_BITS(&buffer[sendCount], inClusterList[index]); sendCount += MACRO_DATA_SIZE_WORD;
    }
  buffer[sendCount++] = numOutClusters;
  for (index = 0; index < numOutClusters; index++)
    {
      COPY_OUT_16_BITS(&buffer[sendCount], outClusterList[index]); sendCount += MACRO_DATA_SIZE_WORD;
    }
  buffer[sendCount++] = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * ZDO Match Descriptor Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_MatchDescriptorReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned16 length;
  SZL_ZdoMatchDescriptorReqParams_t* req;

  /* Confirm data length is correct before using it */
  length = sapMsgGetAppDataLength(Message);
  ER_CHECK_EQUAL(Status, length, sizeof(SZL_ZdoMatchDescriptorReqParams_t));

  req = (SZL_ZdoMatchDescriptorReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, req);

  zabZnpZdoUtility_MatchDescriptorReq(Status, Service,
                                      req->NetworkAddress,
                                      req->NetworkAddressOfInterest,
                                      req->ProfileId,
                                      req->NumInClusters,
                                      req->InClusterList,
                                      req->NumOutClusters,
                                      req->OutClusterList,
                                      sapMsgGetAppTransactionId(Message));
}



/******************************************************************************
 * ZDO Match Descriptor Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
#define M_MAX_NUMBER_OF_ENDPOINTS 128
static void zabZnp_ProcessMatchDescriptorRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  #define M_MATCH_DESC_RSP_DATA_LENGTH(_num_endpoint) (6 + ((_num_endpoint)*sizeof(unsigned8)))
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  unsigned8 tid = 0;
  SZL_ZdoMatchDescriptorRespParams_t* matchDescRsp;
  unsigned8 numberofEndpoints;
  unsigned8 index;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

  /* Validate length before using data.
   * This message will always include network address of interest and match length even if status is bad. */
  if (znpDataLength >= M_MATCH_DESC_RSP_DATA_LENGTH(0))
    {
      // Get number of endpoints so we can calculate size to malloc. Apply some reasonable limits.
      numberofEndpoints = dp[5];
      if (numberofEndpoints > M_MAX_NUMBER_OF_ENDPOINTS)
        {
          numberofEndpoints = M_MAX_NUMBER_OF_ENDPOINTS;
        }

      // Allocate New Message
      length = SZL_ZdoMatchDescriptorRespParams_t_SIZE(numberofEndpoints);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_MATCH_DESC_RSP);

      matchDescRsp = (SZL_ZdoMatchDescriptorRespParams_t*)sapMsgGetAppData(dataMessage);

      /* Extract from the ZNP command into the data sap structure */
      matchDescRsp->NetworkAddress = COPY_IN_16_BITS(&dp[0]);
      matchDescRsp->Status = (SZL_ZDO_STATUS_t)dp[2];
      matchDescRsp->NetworkAddressOfInterest = COPY_IN_16_BITS(&dp[3]);
      matchDescRsp->NumberOfEndpoints = numberofEndpoints;

      for (index = 0; index < numberofEndpoints; index++)
        {
          matchDescRsp->Endpoints[index] = dp[6+index];
        }

      // Pick our the Transaction Id Extension if it is there
      if (znpDataLength > M_MATCH_DESC_RSP_DATA_LENGTH(numberofEndpoints))
        {
          tid = dp[M_MATCH_DESC_RSP_DATA_LENGTH(numberofEndpoints)];
        }

      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      // Set actual data length and send upwards
      sapMsgSetAppDataLength(Status, dataMessage,  SZL_ZdoMatchDescriptorRespParams_t_SIZE(numberofEndpoints));
      sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * ZDO MGMT LQI Request
 ******************************************************************************/
static
void zabZnpZdoUtility_MgmtLqiReq( erStatus* Status, zabService* Service, unsigned16 nwkAddr, unsigned8 startIndex, unsigned8 tid)
{
  #define M_MGMT_LQI_REQ_DATA_LENGTH 4
  sapMsg* Msg = NULL;
  unsigned8* buffer;
  unsigned16 sendCount = 0;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_MGMT_LQI_REQ_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_MGMT_LQI_REQ_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_MGMT_LQI);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  buffer = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddr); sendCount += MACRO_DATA_SIZE_WORD;
  buffer[sendCount++] = startIndex;
  buffer[sendCount++] = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO Mgmt LWI Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_MgmtLqiReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned16 length;
  SZL_ZdoMgmtLqiReqParams_t* req;

  /* Confirm data length is correct before using it */
  length = sapMsgGetAppDataLength(Message);
  ER_CHECK_EQUAL(Status, length, sizeof(SZL_ZdoMgmtLqiReqParams_t));

  req = (SZL_ZdoMgmtLqiReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, req);

  zabZnpZdoUtility_MgmtLqiReq(Status, Service, req->NetworkAddress, req->StartIndex, sapMsgGetAppTransactionId(Message));
}

/******************************************************************************
 * ZDO MGMT LQI Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
#define M_MGMT_LQI_RSP_MAX_NEIGHBOR_TABLE_ITEMS 3
static void zabZnp_ProcessMgmtLqiRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoMgmtLqiRespParams_t* lqiRsp;
  unsigned8 znpIndex;
  unsigned8 lqiItem;
  unsigned8 tid = 0;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

   /* Validate length before using data */
  if (znpDataLength >= 3)
    {
      // Allocate New Message - allocating space for M_MAX_NUMBER_OF_CLUSTERS is slightly inefficent, but simpler
      length = SZL_ZdoMgmtLqiRespParams_t_SIZE(M_MGMT_LQI_RSP_MAX_NEIGHBOR_TABLE_ITEMS);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_MGMT_LQI_RSP);
      lqiRsp = (SZL_ZdoMgmtLqiRespParams_t*)sapMsgGetAppData(dataMessage);

      /* Extract from the ZNP command into the data sap structure */
      znpIndex = 0;
      lqiRsp->NetworkAddress = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;

      lqiRsp->Status = dp[znpIndex++];

      /* Everything after status is only included if success. If not, null out everything nicely */
      if ( (lqiRsp->Status == SZL_ZDO_STATUS_SUCCESS) && (znpDataLength >= 6) )
        {
          lqiRsp->NeighborTableEntries = dp[znpIndex++];
          lqiRsp->StartIndex = dp[znpIndex++];
          lqiRsp->NeighborTableListCount = dp[znpIndex++];

          /* Try to grab the TID before modifying parameters */
          if (znpDataLength > (6 + (lqiRsp->NeighborTableListCount * 22)))
            {
              tid = dp[6 + (lqiRsp->NeighborTableListCount * 22)];
            }

          /* Limit count to the ammount of data allocated */
          if (lqiRsp->NeighborTableListCount > M_MGMT_LQI_RSP_MAX_NEIGHBOR_TABLE_ITEMS)
            {
              lqiRsp->NeighborTableListCount = M_MGMT_LQI_RSP_MAX_NEIGHBOR_TABLE_ITEMS;
            }
          /* If we dont have enough data ion the ZNP frame, tidy up */
          if (znpDataLength < (6 + (lqiRsp->NeighborTableListCount * 22)))
            {
              lqiRsp->NeighborTableListCount = 0;
            }

          for (lqiItem = 0; lqiItem < lqiRsp->NeighborTableListCount; lqiItem++)
            {
              lqiRsp->NeighborTableList[lqiItem].Epid = COPY_IN_64_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_QUAD_WORD;
              lqiRsp->NeighborTableList[lqiItem].Ieee = COPY_IN_64_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_QUAD_WORD;
              lqiRsp->NeighborTableList[lqiItem].NetworkAddress = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;

              lqiRsp->NeighborTableList[lqiItem].DeviceType = dp[znpIndex] & 0x03;
              lqiRsp->NeighborTableList[lqiItem].RxOnWhenIdle = (ENUM_SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_TYPE_t)((dp[znpIndex] >> 2) & 0x03);
              lqiRsp->NeighborTableList[lqiItem].Relationship = (ENUM_SZL_ZDO_MGMT_LQI_REALATIONSHIP_TYPE)((dp[znpIndex] >> 4) & 0x07);
              znpIndex++;

              lqiRsp->NeighborTableList[lqiItem].PermitJoin = dp[znpIndex++];
              lqiRsp->NeighborTableList[lqiItem].Depth = dp[znpIndex++];
              lqiRsp->NeighborTableList[lqiItem].Lqi = dp[znpIndex++];
            }
        }
      else
        {
          lqiRsp->NeighborTableEntries = 0;
          lqiRsp->StartIndex = 0;
          lqiRsp->NeighborTableListCount = 0;
        }

      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      // Set actual data length and send upwards
      sapMsgSetAppDataLength(Status, dataMessage, SZL_ZdoMgmtLqiRespParams_t_SIZE(lqiRsp->NeighborTableListCount));
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
}


/******************************************************************************
 * ZDO MGMT RTG Request
 ******************************************************************************/
static
void zabZnpZdoUtility_MgmtRtgReq( erStatus* Status, zabService* Service, unsigned16 nwkAddr, unsigned8 startIndex, unsigned8 tid)
{
  #define M_MGMT_RTG_REQ_DATA_LENGTH 4
  sapMsg* Msg = NULL;
  unsigned8* buffer;
  unsigned16 sendCount = 0;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_MGMT_RTG_REQ_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_MGMT_RTG_REQ_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_MGMT_RTG);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  buffer = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddr); sendCount += MACRO_DATA_SIZE_WORD;
  buffer[sendCount++] = startIndex;
  buffer[sendCount++] = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * ZDO Mgmt RTG Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_MgmtRtgReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned16 length;
  SZL_ZdoMgmtRtgReqParams_t* req;

  /* Confirm data length is correct before using it */
  length = sapMsgGetAppDataLength(Message);
  ER_CHECK_EQUAL(Status, length, sizeof(SZL_ZdoMgmtRtgReqParams_t));

  req = (SZL_ZdoMgmtRtgReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, req);

  zabZnpZdoUtility_MgmtRtgReq(Status, Service, req->NetworkAddress, req->StartIndex, sapMsgGetAppTransactionId(Message));
}

/******************************************************************************
 * ZDO MGMT RTG Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
#define M_MGMT_RTG_RSP_MAX_NEIGHBOR_TABLE_ITEMS 10
#define M_SIZE_OF_MGMT_RTG_ITEM 5
static void zabZnp_ProcessMgmtRtgRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoMgmtRtgRespParams_t* rtgRsp;
  unsigned8 znpIndex;
  unsigned8 rtgItem;
  unsigned8 tid = 0;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

   /* Validate length before using data */
  if (znpDataLength >= 3)
    {
      // Allocate New Message - allocating space for M_MAX_NUMBER_OF_CLUSTERS is slightly inefficient, but simpler
      length = SZL_ZdoMgmtRtgRespParams_t_SIZE(M_MGMT_LQI_RSP_MAX_NEIGHBOR_TABLE_ITEMS);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_MGMT_RTG_RSP);
      rtgRsp = (SZL_ZdoMgmtRtgRespParams_t*)sapMsgGetAppData(dataMessage);

      /* Extract from the ZNP command into the data sap structure */
      znpIndex = 0;
      rtgRsp->SourceNetworkAddress = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;

      rtgRsp->Status = dp[znpIndex++];

      /* Everything after status is only included if success. If not, null out everything nicely */
      if ( (rtgRsp->Status == SZL_ZDO_STATUS_SUCCESS) && (znpDataLength >= 6) )
        {

          rtgRsp->RoutingTableEntries = dp[znpIndex++];
          rtgRsp->StartIndex = dp[znpIndex++];
          rtgRsp->RoutingTableListCount = dp[znpIndex++];

          /* Try to grab the TID before modifying parameters */
          if (znpDataLength > (6 + (rtgRsp->RoutingTableListCount * M_SIZE_OF_MGMT_RTG_ITEM)))
            {
              tid = dp[6 + (rtgRsp->RoutingTableListCount * M_SIZE_OF_MGMT_RTG_ITEM)];
            }

          /* Limit count to the amount of data allocated */
          if (rtgRsp->RoutingTableListCount > M_MGMT_LQI_RSP_MAX_NEIGHBOR_TABLE_ITEMS)
            {
              rtgRsp->RoutingTableListCount = M_MGMT_LQI_RSP_MAX_NEIGHBOR_TABLE_ITEMS;
            }
          /* If we don't have enough data ion the ZNP frame, tidy up */
          if (znpDataLength < (6 + (rtgRsp->RoutingTableListCount * M_SIZE_OF_MGMT_RTG_ITEM)))
            {
              rtgRsp->RoutingTableListCount = 0;
            }

          for (rtgItem = 0; rtgItem < rtgRsp->RoutingTableListCount; rtgItem++)
            {
              rtgRsp->RoutingTableList[rtgItem].DestinationNetworkAddress = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;
              rtgRsp->RoutingTableList[rtgItem].RtgStatus = dp[znpIndex++];
              rtgRsp->RoutingTableList[rtgItem].RtgStatus &= 0x07; // Mask it to be sure we dont have extra bits that exist OTA but we dont care about
              rtgRsp->RoutingTableList[rtgItem].NextHopNetworkAddress = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;
            }
        }
      else
        {
          rtgRsp->RoutingTableListCount = 0;
          rtgRsp->StartIndex = 0;
          rtgRsp->RoutingTableListCount = 0;
        }

      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      // Set actual data length and send upwards
      sapMsgSetAppDataLength(Status, dataMessage, SZL_ZdoMgmtRtgRespParams_t_SIZE(rtgRsp->RoutingTableListCount));
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
}






/******************************************************************************
 * ZDO MGMT Bind Request
 ******************************************************************************/
static
void zabZnpZdoUtility_MgmtBindReq( erStatus* Status, zabService* Service, unsigned16 nwkAddr, unsigned8 startIndex, unsigned8 tid)
{
  #define M_MGMT_BIND_REQ_DATA_LENGTH 4
  sapMsg* Msg = NULL;
  unsigned8* buffer;
  unsigned16 sendCount = 0;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_MGMT_BIND_REQ_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_MGMT_BIND_REQ_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_MGMT_BIND);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  buffer = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddr); sendCount += MACRO_DATA_SIZE_WORD;
  buffer[sendCount++] = startIndex;
  buffer[sendCount++] = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO Mgmt Bind Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_MgmtBindReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned16 length;
  SZL_ZdoMgmtBindReqParams_t* req;

  /* Confirm data length is correct before using it */
  length = sapMsgGetAppDataLength(Message);
  ER_CHECK_EQUAL(Status, length, sizeof(SZL_ZdoMgmtBindReqParams_t));

  req = (SZL_ZdoMgmtBindReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, req);

  zabZnpZdoUtility_MgmtBindReq(Status, Service, req->NetworkAddress, req->StartIndex, sapMsgGetAppTransactionId(Message));
}

/******************************************************************************
 * ZDO MGMT Bind Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
#define M_MGMT_BIND_RSP_MAX_BINDING_TABLE_ITEMS 4
#define M_MIN_SIZE_OF_MGMT_BIND_ITEM 14
static void zabZnp_ProcessMgmtBindRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoMgmtBindRespParams_t* bindRsp;
  unsigned8 znpIndex;
  unsigned8 bindItem;
  unsigned8 tid = 0;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

   /* Validate length before using data */
  if (znpDataLength >= 3)
    {
      // Allocate New Message - allocating space for M_MAX_NUMBER_OF_CLUSTERS is slightly inefficient, but simpler
      length = SZL_ZdoMgmtBindRespParams_t_SIZE(M_MGMT_BIND_RSP_MAX_BINDING_TABLE_ITEMS);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_MGMT_BIND_RSP);
      bindRsp = (SZL_ZdoMgmtBindRespParams_t*)sapMsgGetAppData(dataMessage);

      /* Extract from the ZNP command into the data sap structure */
      znpIndex = 0;
      bindRsp->SourceNetworkAddress = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;

      bindRsp->Status = dp[znpIndex++];

      /* Everything after status is only included if success. If not, null out everything nicely */
      if ( (bindRsp->Status == SZL_ZDO_STATUS_SUCCESS) && (znpDataLength >= 6) )
        {

          bindRsp->BindingTableEntries = dp[znpIndex++];
          bindRsp->StartIndex = dp[znpIndex++];
          bindRsp->BindingTableListCount = dp[znpIndex++];


          /* Limit count to the amount of data allocated */
          if (bindRsp->BindingTableListCount > M_MGMT_BIND_RSP_MAX_BINDING_TABLE_ITEMS)
            {
              bindRsp->BindingTableListCount = M_MGMT_BIND_RSP_MAX_BINDING_TABLE_ITEMS;
            }

          for (bindItem = 0; bindItem < bindRsp->BindingTableListCount; bindItem++)
            {
              if (znpDataLength < (znpIndex + M_MIN_SIZE_OF_MGMT_BIND_ITEM))
                {
                  // Command is corrupt, so throw it away
                  sapMsgFree(Status, dataMessage);
                  return;
                }

              bindRsp->BindingTableList[bindItem].SourceIeeeAddress = COPY_IN_64_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_QUAD_WORD;
              bindRsp->BindingTableList[bindItem].SourceEndpoint = dp[znpIndex++];
              bindRsp->BindingTableList[bindItem].ClusterID = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;

              /* Set nice defaults, then load real value based on the mode */
              bindRsp->BindingTableList[bindItem].DestinationGroupAddress = 0xFFFF;
              bindRsp->BindingTableList[bindItem].DestinationIeeeAddress = 0xFFFFFFFFFFFFFFFFULL;
              bindRsp->BindingTableList[bindItem].DestinationEndpoint = 0xFF;
              switch((zabAddressMode)dp[znpIndex++])
                {
                  case ZAB_ADDRESS_MODE_GROUP:
                    bindRsp->BindingTableList[bindItem].DestinationAddressMode = SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_GROUP;
                    bindRsp->BindingTableList[bindItem].DestinationGroupAddress = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;
                    break;

                  case ZAB_ADDRESS_MODE_IEEE_ADDRESS:
                    if (znpDataLength < (znpIndex + MACRO_DATA_SIZE_QUAD_WORD + 1))
                      {
                        // Command is corrupt, so throw it away
                        sapMsgFree(Status, dataMessage);
                        return;
                      }
                    bindRsp->BindingTableList[bindItem].DestinationAddressMode = SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_UNICAST;
                    bindRsp->BindingTableList[bindItem].DestinationIeeeAddress = COPY_IN_64_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_QUAD_WORD;
                    bindRsp->BindingTableList[bindItem].DestinationEndpoint = dp[znpIndex++];
                    break;

                  default:
                    if (znpDataLength < (znpIndex + MACRO_DATA_SIZE_QUAD_WORD + 1))
                      {
                        // Command is corrupt, so throw it away
                        sapMsgFree(Status, dataMessage);
                        return;
                      }
                    break;
                }
            }
          // Tidy up in case we had less valid entries than expected
          bindRsp->BindingTableListCount = bindItem;

          /* Try to grab the TID if it exists  */
          if (znpDataLength > znpIndex)
            {
              tid = dp[znpIndex];
            }
        }
      else
        {
          bindRsp->BindingTableListCount = 0;
          bindRsp->StartIndex = 0;
          bindRsp->BindingTableListCount = 0;
        }

      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      // Set actual data length and send upwards
      sapMsgSetAppDataLength(Status, dataMessage, SZL_ZdoMgmtBindRespParams_t_SIZE(bindRsp->BindingTableListCount));
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
}


/******************************************************************************
 * ZDO MGMT Leave Request
 ******************************************************************************/
#define M_LEAVE_OPTIONS_BIT_REJOIN 0x01
#define M_LEAVE_OPTIONS_BIT_REMOVE_CHILDREN 0x02
static
void zabZnpZdoUtility_MgmtLeaveReq(erStatus* Status, zabService* Service, unsigned16 nwkAddr, unsigned64 ieeeAddr, zab_bool rejoin, zab_bool removeChildren, unsigned8 tid)
{
  #define M_MGMT_LEAVE_REQ_DATA_LENGTH 12
  sapMsg* Msg = NULL;
  unsigned8* buffer;
  unsigned16 sendCount = 0;
  unsigned8 options;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Create the options field */
  options = 0;
  if (rejoin)
    {
      options |= M_LEAVE_OPTIONS_BIT_REJOIN;
    }
  if (removeChildren)
    {
      options |= M_LEAVE_OPTIONS_BIT_REMOVE_CHILDREN;
    }

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_MGMT_LEAVE_REQ_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_MGMT_LEAVE_REQ_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_MGMT_LEAVE);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  buffer = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddr); sendCount += MACRO_DATA_SIZE_WORD;
  COPY_OUT_64_BITS(&buffer[sendCount], ieeeAddr); sendCount += MACRO_DATA_SIZE_QUAD_WORD;
  buffer[sendCount++] = options;
  buffer[sendCount++] = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO Mgmt Leave Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_MgmtLeaveReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned16 length;
  SZL_NwkLeaveReqParams_t* req;

  /* Confirm data length is correct before using it */
  length = sapMsgGetAppDataLength(Message);
  ER_CHECK_EQUAL(Status, length, SZL_NwkLeaveReqParams_t_SIZE);

  req = (SZL_NwkLeaveReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, req);

  zabZnpZdoUtility_MgmtLeaveReq(Status, Service, req->DeviceNwkAddress, req->DeviceIEEEAddress, req->Rejoin, zab_false, sapMsgGetAppTransactionId(Message));
}

/******************************************************************************
 * ZDO MGMT Leave Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
static void zabZnp_ProcessMgmtLeaveRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  zabMsgProMgmtLeaveRsp* leaveRsp;
  unsigned8 znpIndex;
  unsigned8 tid = 0;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

   /* Validate length before using data */
  if (znpDataLength >= 3)
    {
      /* Allocate message to send upwards */
      length = zabMsgProMgmtLeaveRsp_SIZE;
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_MGMT_LEAVE_RSP);
      leaveRsp = (zabMsgProMgmtLeaveRsp*)sapMsgGetAppData(dataMessage);

      /* Extract from the ZNP command into the data sap structure */
      znpIndex = 0;
      leaveRsp->srcNwkAddress = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;
      leaveRsp->status = dp[znpIndex++];

      /* Get the TID if it is included */
      if (znpDataLength >= 4)
        {
          tid = dp[znpIndex++];
        }

      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
}




/******************************************************************************
 * ZDO MGMT Nwk Update Notification
 ******************************************************************************/
static void zabZnp_ProcessMgmtNwkUpdateNtf(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t* rsp;
  unsigned8 znpIndex;
  unsigned8 tid = 0;
  unsigned32 chMask;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

  /* Validate length before using data */
  if (znpDataLength >= 3)
    {
      /* Allocate message to send upwards */
      length = SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t_SIZE;
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_MGMT_NWK_UPDATE_RSP);
      rsp = (SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t*)sapMsgGetAppData(dataMessage);

      /* Extract from the ZNP command into the data sap structure */
      znpIndex = 0;
      rsp->NetworkAddress = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;
      rsp->Status = dp[znpIndex++];

      if ( (rsp->Status == SZL_ZDO_STATUS_SUCCESS) && (znpDataLength >= 12))
        {
          chMask = COPY_IN_32_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_DOUBLE_WORD;
          rsp->ScannedChannelsMask = (chMask >> 11); // WARNING - this is SZL magic and should not really be done here
          rsp->TotalTransmisisons = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;
          rsp->TransmisisonFailures = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;
          rsp->ScannedChannelListCount = dp[znpIndex++];

          if (znpDataLength >= (12 + rsp->ScannedChannelListCount))
            {
              osMemCopy( Status, rsp->EnergyValues, &dp[znpIndex], rsp->ScannedChannelListCount);
              znpIndex += rsp->ScannedChannelListCount;

              /* Get the TID if it is included */
              if (znpDataLength > (12 + rsp->ScannedChannelListCount))
                {
                  tid = dp[znpIndex++];
                }
            }
        }

      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
}

/******************************************************************************
 * MGMT Network Update Request
 ******************************************************************************/
void zabZnpZdoUtility_MgmtNwkUpdateReq(erStatus* Status, zabService* Service,
                                       zabAddressMode DestinationAddressMode,
                                       unsigned16 DestinationAddress,
                                       unsigned32 ChannelMask,
                                       unsigned8 ScanDuration,
                                       unsigned8 ScanCount,
                                       unsigned16 NetworkManagerAddress,
                                       unsigned8 tid)
{
  #define M_MGMT_NWK_UPDATE_REQ_DATA_LENGTH 12
  sapMsg* Msg = NULL;
  unsigned8* buffer;
  unsigned16 sendCount = 0;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_MGMT_NWK_UPDATE_REQ_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_MGMT_NWK_UPDATE_REQ_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_MGMT_NWK_UPDATE);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  buffer = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(&buffer[sendCount], DestinationAddress); sendCount += MACRO_DATA_SIZE_WORD;
  buffer[sendCount++] = (unsigned8)DestinationAddressMode;
  COPY_OUT_32_BITS(&buffer[sendCount], ChannelMask); sendCount += MACRO_DATA_SIZE_DOUBLE_WORD;
  buffer[sendCount++] = ScanDuration;
  buffer[sendCount++] = ScanCount;
  COPY_OUT_16_BITS(&buffer[sendCount], NetworkManagerAddress); sendCount += MACRO_DATA_SIZE_WORD;
  buffer[sendCount++] = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * MGMT Network Update Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_MgmtNwkUpdateReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned16 length;
  zabMsgProMgmtNwkUpdateReq* req;
  zabProAddress dstAddr;
  osMemZero(Status, (unsigned8*)&dstAddr, sizeof(zabProAddress));

  /* Confirm data length is correct before using it */
  length = sapMsgGetAppDataLength(Message);
  ER_CHECK_EQUAL(Status, length, zabMsgProMgmtNwkUpdateReq_SIZE);

  req = (zabMsgProMgmtNwkUpdateReq*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, req);

  zabZnpZdoUtility_MgmtNwkUpdateReq(Status, Service,
                                    req->DestinationAddressMode,
                                    req->DestinationAddress,
                                    req->ChannelMask,
                                    req->ScanDuration,
                                    req->ScanCount,
                                    req->NetworkManagerAddress,
                                    sapMsgGetAppTransactionId(Message));
}

/******************************************************************************
 * ZDO Bind Request
 ******************************************************************************/
static
void zabZnpZdoUtility_BindReq( erStatus* Status, zabService* Service,
                               unsigned16 nwkAddr,
                               unsigned64 srcIeeeAddr, unsigned8 srcEndpoint,
                               unsigned16 cluster,
                               zabProAddress* dstAddrPtr,
                               unsigned8 tid)
{
  #define M_BIND_REQ_DATA_LENGTH 24
  sapMsg* Msg = NULL;
  unsigned8* buffer;
  unsigned16 sendCount;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_BIND_REQ_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_BIND_REQ_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_BIND);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  sendCount = 0;
  buffer = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddr); sendCount += MACRO_DATA_SIZE_WORD;
  COPY_OUT_64_BITS(&buffer[sendCount], srcIeeeAddr); sendCount += MACRO_DATA_SIZE_QUAD_WORD;
  buffer[sendCount++] = srcEndpoint;
  COPY_OUT_16_BITS(&buffer[sendCount], cluster); sendCount += MACRO_DATA_SIZE_WORD;
  buffer[sendCount++] = (unsigned8)dstAddrPtr->addressMode;

  /* Load destination address - it is always 8 bytes, but has different contents based on addressMode */
  if (dstAddrPtr->addressMode == ZAB_ADDRESS_MODE_IEEE_ADDRESS)
    {
      COPY_OUT_64_BITS(&buffer[sendCount], dstAddrPtr->address.ieeeAddress); sendCount += MACRO_DATA_SIZE_QUAD_WORD;
    }
  else if (dstAddrPtr->addressMode == ZAB_ADDRESS_MODE_GROUP)
    {
      COPY_OUT_16_BITS(&buffer[sendCount], dstAddrPtr->address.shortAddress); sendCount += MACRO_DATA_SIZE_WORD;
      sendCount+=6;
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_MSG_PARAM_INVALID);
    }
  buffer[sendCount++] = dstAddrPtr->endpoint;
  buffer[sendCount++] = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO Bind Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_BindReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned16 length;
  SZL_ZdoBindReqParams_t* req;
  zabProAddress dstAddr;
  osMemZero(Status, (unsigned8*)&dstAddr, sizeof(zabProAddress));

  /* Confirm data length is correct before using it */
  length = sapMsgGetAppDataLength(Message);
  ER_CHECK_EQUAL(Status, length, sizeof(SZL_ZdoBindReqParams_t));

  req = (SZL_ZdoBindReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, req);

  /* Convert address into vendor format */
  switch (req->DestinationAddressMode)
    {
      case SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_GROUP:
        dstAddr.addressMode = ZAB_ADDRESS_MODE_GROUP;
        dstAddr.address.shortAddress = req->DestinationGroupAddress;
        break;

      case SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_UNICAST:
        dstAddr.addressMode = ZAB_ADDRESS_MODE_IEEE_ADDRESS;
        dstAddr.address.ieeeAddress = req->DestinationIeeeAddress;
        dstAddr.endpoint = req->DestinationEndpoint;
        break;

      default:
        dstAddr.addressMode = ZAB_ADDRESS_MODE_UNDEFINED;
        break;
    }

  zabZnpZdoUtility_BindReq(Status, Service,
                           req->NetworkAddress,
                           req->SourceIeeeAddress, req->SourceEndpoint,
                           req->ClusterID,
                           &dstAddr,
                           sapMsgGetAppTransactionId(Message));
}


/******************************************************************************
 * ZDO Bind Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
static void zabZnp_ProcessBindRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 sapDataLength;
  sapMsg* dataMessage = NULL;
  SZL_ZdoBindRespParams_t* bindRsp;
  unsigned8 znpIndex;
  unsigned8 tid = 0;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  ER_CHECK_NULL(Status, dp);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);

  /* Validate length before using data */
  if (znpDataLength >= 3)
    {
      // Allocate New Message
      sapDataLength = sizeof(SZL_ZdoBindRespParams_t);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, sapDataLength);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_BIND_RSP);
      sapMsgSetAppDataLength(Status, dataMessage, sapDataLength);
      bindRsp = (SZL_ZdoBindRespParams_t*)sapMsgGetAppData(dataMessage);

      /* Extract from the ZNP command into the data sap structure */
      znpIndex = 0;
      bindRsp->NetworkAddress = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;
      bindRsp->Status = dp[znpIndex++];

      /* Grab TID if it is there */
      if (znpDataLength > 3)
        {
          tid = dp[znpIndex++];
        }
      sapMsgSetAppTransactionId(Status, dataMessage, tid);

      /* Send up through data sap */
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
}




/******************************************************************************
 * ZDO UnBind Request
 ******************************************************************************/
static
void zabZnpZdoUtility_UnBindReq( erStatus* Status, zabService* Service,
                               unsigned16 nwkAddr,
                               unsigned64 srcIeeeAddr, unsigned8 srcEndpoint,
                               unsigned16 cluster,
                               zabProAddress* dstAddr,
                               unsigned8 tid)
{
  #define M_BIND_REQ_DATA_LENGTH 24
  sapMsg* Msg = NULL;
  unsigned8* buffer;
  unsigned16 sendCount;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_BIND_REQ_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_BIND_REQ_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_UNBIND);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  sendCount = 0;
  buffer = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddr); sendCount += MACRO_DATA_SIZE_WORD;
  COPY_OUT_64_BITS(&buffer[sendCount], srcIeeeAddr); sendCount += MACRO_DATA_SIZE_QUAD_WORD;
  buffer[sendCount++] = srcEndpoint;
  COPY_OUT_16_BITS(&buffer[sendCount], cluster); sendCount += MACRO_DATA_SIZE_WORD;
  buffer[sendCount++] = (unsigned8)dstAddr->addressMode;

  /* Load destination address - it is always 8 bytes, but has different contents based on addressMode */
  if (dstAddr->addressMode == ZAB_ADDRESS_MODE_IEEE_ADDRESS)
    {
      COPY_OUT_64_BITS(&buffer[sendCount], dstAddr->address.ieeeAddress); sendCount += MACRO_DATA_SIZE_QUAD_WORD;
    }
  else if (dstAddr->addressMode == ZAB_ADDRESS_MODE_GROUP)
    {
      COPY_OUT_16_BITS(&buffer[sendCount], dstAddr->address.shortAddress); sendCount += MACRO_DATA_SIZE_WORD;
      sendCount+=6;
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_MSG_PARAM_INVALID);
    }
  buffer[sendCount++] = dstAddr->endpoint;
  buffer[sendCount++] = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO UnBind Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_UnBindReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned16 length;
  SZL_ZdoBindReqParams_t* req;
  zabProAddress dstAddr;
  osMemZero(Status, (unsigned8*)&dstAddr, sizeof(zabProAddress));

  /* Confirm data length is correct before using it */
  length = sapMsgGetAppDataLength(Message);
  ER_CHECK_EQUAL(Status, length, sizeof(SZL_ZdoBindReqParams_t));

  req = (SZL_ZdoBindReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, req);

  /* Convert address into vendor format */
  switch (req->DestinationAddressMode)
    {
      case SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_GROUP:
        dstAddr.addressMode = ZAB_ADDRESS_MODE_GROUP;
        dstAddr.address.shortAddress = req->DestinationGroupAddress;
        break;

      case SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_UNICAST:
        dstAddr.addressMode = ZAB_ADDRESS_MODE_IEEE_ADDRESS;
        dstAddr.address.ieeeAddress = req->DestinationIeeeAddress;
        dstAddr.endpoint = req->DestinationEndpoint;
        break;

      default:
        dstAddr.addressMode = ZAB_ADDRESS_MODE_UNDEFINED;
        break;
    }

  zabZnpZdoUtility_UnBindReq(Status, Service,
                            req->NetworkAddress,
                            req->SourceIeeeAddress, req->SourceEndpoint,
                            req->ClusterID,
                            &dstAddr,
                             sapMsgGetAppTransactionId(Message));
}

/******************************************************************************
 * ZDO UnBind Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
static void zabZnp_ProcessUnBindRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 sapDataLength;
  sapMsg* dataMessage = NULL;
  SZL_ZdoBindRespParams_t* bindRsp;
  unsigned8 znpIndex;
  unsigned8 tid = 0;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  ER_CHECK_NULL(Status, dp);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);

  /* Validate length before using data */
  if (znpDataLength >= 3)
    {
      // Allocate New Message
      sapDataLength = sizeof(SZL_ZdoBindRespParams_t);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, sapDataLength);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_UNBIND_RSP);
      sapMsgSetAppDataLength(Status, dataMessage, sapDataLength);
      bindRsp = (SZL_ZdoBindRespParams_t*)sapMsgGetAppData(dataMessage);

      /* Extract from the ZNP command into the data sap structure */
      znpIndex = 0;
      bindRsp->NetworkAddress = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;
      bindRsp->Status = dp[znpIndex++];

      /* Grab TID if it is there */
      if (znpDataLength > 3)
        {
          tid = dp[znpIndex++];
        }
      sapMsgSetAppTransactionId(Status, dataMessage, tid);

      /* Send up through data sap */
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
}















/******************************************************************************
 * Join Request
 ******************************************************************************/
void zabZnpZdoUtility_JoinReq(erStatus* Status, zabService* Service,
                              unsigned8 Channel,
                              unsigned16 PanId,
                              unsigned64 Epid,
                              unsigned16 ParentNwkAddr,
                              unsigned8 ParentDepth,
                              unsigned8 StackProfile)
{
  zabznpZdo_ZDO_JOIN_REQ* joinReq;
  sapMsg* Msg = NULL;

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, ZNPI_ZDO_JOIN_REQ_DATALENGTH());
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_JOIN_REQ);
  zabParseZNPSetLength(Status, Msg, ZNPI_ZDO_JOIN_REQ_DATALENGTH());

  joinReq = (zabznpZdo_ZDO_JOIN_REQ*)ZAB_MSG_ZNP_DATA(Msg);
  joinReq->logicalChannel = Channel;
  COPY_OUT_16_BITS(joinReq->panId, PanId);
  COPY_OUT_64_BITS(joinReq->ExPanId, Epid);
  COPY_OUT_16_BITS(joinReq->chosenParent, ParentNwkAddr);
  joinReq->parentDepth = ParentDepth;
  joinReq->stackProfile = StackProfile;

  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * Startup From App Request
 ******************************************************************************/
void zabZnpZdoUtility_StartupFromApp( erStatus* Status, zabService* Service, unsigned16 startDelay)
{
  sapMsg* Msg = NULL;
  zabznpZdo_ZDO_STARTUP_FROM_APP *msgData;
  unsigned8 payloadLength = sizeof(zabznpZdo_ZDO_STARTUP_FROM_APP);
  unsigned8 msgLen;

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, payloadLength);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_STARTUP_FROM_APP);

  /* map ZNP data to Zdo Data struct and update length */
  zabParseZNPGetData(Status, Msg, &msgLen, (unsigned8 **)&msgData);
  zabParseZNPSetLength(Status, Msg, payloadLength);

  /* If status is ok, write data fields and send. Otherwise free and exit */
  if (erStatusIsOk(Status))
    {
      /* Set ZNP ZDO Field */
      COPY_OUT_16_BITS(msgData->startDelay, startDelay);
      zabZnpService_SendMsg(Status, Service, Msg);
    }
  else
    {
      sapMsgFree(Status, Msg);
    }
}


/******************************************************************************
 * Network Discovery Request
 * Starts an Active Scan to seach for networks to join
 ******************************************************************************/
void zabZnpZdoUtility_NetworkDiscoveryReq( erStatus* Status, zabService* Service, unsigned32 channelMask, unsigned8 scanDuration)
{
  sapMsg* Msg = NULL;
  zabznpZdo_ZDO_NWK_DISC_REQ *msgData;
  unsigned8 payloadLength = sizeof(zabznpZdo_ZDO_NWK_DISC_REQ);
  unsigned8 msgLen;

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, payloadLength);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_NWK_DISC_REQ);

  /* map ZNP data to Zdo Data struct and set length*/
  zabParseZNPGetData(Status, Msg, &msgLen, (unsigned8 **)&msgData);
  zabParseZNPSetLength(Status, Msg, payloadLength);

  /* If status is ok, write data fields and send. Otherwise free and exit */
  if (erStatusIsOk(Status))
    {
      /* Set ZNP ZDO Field */
      COPY_OUT_32_BITS(msgData->scanChannel, channelMask);
      msgData->scanDuration = scanDuration;
      zabZnpService_SendMsg(Status, Service, Msg);
    }
  else
    {
      sapMsgFree(Status, Msg);
    }
}


/******************************************************************************
 * Device Announce Handler
 ******************************************************************************/
static
void processDeviceAnnounce(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  #define M_DEVICE_ANNOUNCE_LENGTH 13
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 sapDataLength;
  sapMsg* dataMessage = NULL;
  SZL_ZdoDeviceAnnounceIndParams_t* DeviceAnnounce;
  unsigned8 znpIndex;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  ER_CHECK_NULL(Status, dp);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);

  /* Validate length before using data */
  if (znpDataLength >= M_DEVICE_ANNOUNCE_LENGTH)
    {
      // Allocate New Message
      sapDataLength = sizeof(SZL_ZdoDeviceAnnounceIndParams_t);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, sapDataLength);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_DEVICE_ANNOUNCE);
      sapMsgSetAppDataLength(Status, dataMessage, sapDataLength);
      DeviceAnnounce = (SZL_ZdoDeviceAnnounceIndParams_t*)sapMsgGetAppData(dataMessage);

      /* Extract from the ZNP command into the data sap structure */
      znpIndex = 2; // Skip source address, we only caer about device address
      DeviceAnnounce->NwkAddress = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;
      DeviceAnnounce->IeeeAddress = COPY_IN_64_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_QUAD_WORD;

      if ( (dp[znpIndex] & DEVIC_ANNCE_MASK_ALT_PAN_COORD) > 0)
        {
          DeviceAnnounce->AltPanCoordinator = zab_true;
        }
      else
        {
          DeviceAnnounce->AltPanCoordinator = zab_false;
        }
      if ( (dp[znpIndex] & DEVIC_ANNCE_MASK_DEV_TYPE_FFD) > 0)
        {
          DeviceAnnounce->DeviceType_Ffd = zab_true;
        }
      else
        {
          DeviceAnnounce->DeviceType_Ffd = zab_false;
        }
      if ( (dp[znpIndex] & DEVIC_ANNCE_MASK_POWER_SRC_MAINS) > 0)
        {
          DeviceAnnounce->PowerSource_Mains = zab_true;
        }
      else
        {
          DeviceAnnounce->PowerSource_Mains = zab_false;
        }
      if ( (dp[znpIndex] & DEVIC_ANNCE_MASK_RX_ON_WHEN_IDLE) > 0)
        {
          DeviceAnnounce->RxOnWhenIdle = zab_true;
        }
      else
        {
          DeviceAnnounce->RxOnWhenIdle = zab_false;
        }
      if ( (dp[znpIndex] & DEVIC_ANNCE_MASK_SECURITY_CAP) > 0)
        {
          DeviceAnnounce->SecurityCapability = zab_true;
        }
      else
        {
          DeviceAnnounce->SecurityCapability = zab_false;
        }
      if ( (dp[znpIndex] & DEVIC_ANNCE_MASK_ALLOCATE_ADDR) > 0)
        {
          DeviceAnnounce->AllocateAddress = zab_true;
        }
      else
        {
          DeviceAnnounce->AllocateAddress = zab_false;
        }

      /* Send up through data sap */
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
}


/******************************************************************************
 * Beacon Indication Handler
 ******************************************************************************/
static
void processBeacon(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  #define M_BEACON_LENGTH 21
  unsigned8 beaconCount = 0;
  unsigned8 beaconNumber;
  unsigned8* dp;
  unsigned8 znpDataLength;
  BeaconFormat_t beacon;
  unsigned8 znpIndex;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

  /* When beacons are received, Give them up to the core */
  znpIndex = 0;
  beaconCount = dp[znpIndex++];

  for ( beaconNumber = 0;
        (beaconNumber < beaconCount) && ((znpIndex + M_BEACON_LENGTH) <= znpDataLength);
        beaconNumber++)
    {
      beacon.src = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;
      beacon.pan = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_WORD;
      beacon.channel = dp[znpIndex++];
      beacon.permitJoin = dp[znpIndex++];
      beacon.routerCapacity = dp[znpIndex++];
      beacon.deviceCapacity = dp[znpIndex++];
      beacon.protocolVersion = dp[znpIndex++];
      beacon.stackProfile = dp[znpIndex++];
      beacon.lqi = dp[znpIndex++];
      beacon.depth = dp[znpIndex++];
      beacon.updateID = dp[znpIndex++];
      beacon.epid = COPY_IN_64_BITS(&dp[znpIndex]); znpIndex += MACRO_DATA_SIZE_QUAD_WORD;

      srvCoreGiveBackBuffer (Status, Service, ZAB_GIVE_NWK_BEACON, sizeof(BeaconFormat_t), (unsigned8*)&beacon);
    }
}



/******************************************************************************
 * MGMT Permit Join Request
 ******************************************************************************/
void zabZnpZdoUtility_PermitJoinReq(erStatus* Status, zabService* Service,
                                    zabAddressMode AddressMode,
                                    unsigned16 DestinationAddress,
                                    unsigned8 Duration,
                                    zab_bool TCSignificance,
                                    unsigned8 tid)
{
  zabznpZdo_MGMT_PERMIT_JOIN_REQ* pjReq;
  unsigned8 payloadLength = sizeof(zabznpZdo_MGMT_PERMIT_JOIN_REQ);
  sapMsg* Msg = NULL;

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, payloadLength);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_MGMT_PERMIT_JOIN);
  sapMsgSetAppTransactionId(Status, Msg, tid);
  zabParseZNPSetLength(Status, Msg, payloadLength);
  pjReq = (zabznpZdo_MGMT_PERMIT_JOIN_REQ*)ZAB_MSG_ZNP_DATA(Msg);

  /* Populate command data */
  pjReq->AddressMode = AddressMode;
  COPY_OUT_16_BITS(pjReq->DesinationAddress, DestinationAddress);
  pjReq->Duration = Duration;
  pjReq->TCSignificance = TCSignificance;
  pjReq->Tid = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * ZDO Device Announce Request
 ******************************************************************************/
void zabZnpZdoUtility_DeviceAnnounceReq(erStatus* Status, zabService* Service,
                                        unsigned16 nwkAddr,
                                        unsigned64 ieeeAddr,
                                        unsigned8 capabilities)
{
  sapMsg* Msg = NULL;
  unsigned8 payloadLength = sizeof(znpDeviceAnnounceFormat);
  znpDeviceAnnounceFormat *msgData;

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, payloadLength);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_DEVICE_ANNCE);

  /* load the data area */
  msgData = (znpDeviceAnnounceFormat*)ZAB_MSG_ZNP_DATA(Msg);
  zabParseZNPSetLength(Status, Msg, payloadLength);
  COPY_OUT_16_BITS(msgData->nwkAddr, nwkAddr);
  COPY_OUT_64_BITS(msgData->ieeeAddr, ieeeAddr);
  msgData->capabilities = capabilities;

  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * ZDO User Descriptor Request
 ******************************************************************************/
static
void zabZnpZdoUtility_UserDescriptorReq( erStatus* Status, zabService* Service, unsigned16 nwkAddr, unsigned16 nwkAddrOfInterest, unsigned8 tid)
{
  #define M_USER_DESC_DATA_LENGTH 5
  sapMsg* Msg = NULL;
  unsigned8* buffer;
  unsigned16 sendCount;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, M_USER_DESC_DATA_LENGTH);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, M_USER_DESC_DATA_LENGTH);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_USER_DESC);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  sendCount = 0;
  buffer = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddr); sendCount += MACRO_DATA_SIZE_WORD;
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddrOfInterest); sendCount += MACRO_DATA_SIZE_WORD;
  buffer[sendCount] = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO User Descriptor Request
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_UserDescriptorReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned16 length;
  SZL_ZdoUserDescriptorReqParams_t* dp;

  /* Confirm data length is correct before using it */
  length = sapMsgGetAppDataLength(Message);
  ER_CHECK_EQUAL(Status, length, sizeof(SZL_ZdoUserDescriptorReqParams_t));

  dp = (SZL_ZdoUserDescriptorReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  zabZnpZdoUtility_UserDescriptorReq(Status, Service, dp->NetworkAddress, dp->NetworkAddressOfInterest, sapMsgGetAppTransactionId(Message));
}

/******************************************************************************
 * ZDO User Descriptor Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
static void zabZnp_ProcessUserDescriptorRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  sapMsg* dataMessage = NULL;
  unsigned8 stringLength;
  unsigned8 tid = 0;
  SZL_ZdoUserDescriptorRespParams_t* userDescRsp;
  unsigned8 length;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

  /* Validate length before using data */
  // WARNING - ZNP doc says there will be at least 6 bytes of data, but if User Desc is not supported we only get 4: NekAddr, Sts, TID
  if (znpDataLength >= 3)
    {
      if (znpDataLength >= 6)
        {
          stringLength = dp[5];
        }
      else
        {
          stringLength = 0;
        }

      // Allocate New Message
      length = SZL_ZdoUserDescriptorRespParams_t_SIZE(stringLength+1); // Add 1 for the null terminator
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_USER_DESC_RSP);

      /* Extract from the ZNP command into the data sap structure */
      userDescRsp = (SZL_ZdoUserDescriptorRespParams_t*)sapMsgGetAppData(dataMessage);
      userDescRsp->Status = (SZL_ZDO_STATUS_t)dp[2];
      userDescRsp->SourceAddress = COPY_IN_16_BITS(&dp[0]);

      if ( (userDescRsp->Status == SZL_ZDO_STATUS_SUCCESS) && (znpDataLength >= (6 + stringLength)) )
        {
          userDescRsp->NetworkAddressOfInterest = COPY_IN_16_BITS(&dp[3]);
          userDescRsp->UserDescriptorLength = stringLength;
          osMemCopy( Status, userDescRsp->UserDescriptor, &dp[6], stringLength);
          userDescRsp->UserDescriptor[stringLength] = 0; // null terminate

          if (znpDataLength > (6 + stringLength))
            {
              tid = dp[6+stringLength];
            }
        }
      else
        {
          // WARNING: This breaks if Destination Address was not equal to Dest Address of interest
          userDescRsp->NetworkAddressOfInterest = userDescRsp->SourceAddress;

          userDescRsp->UserDescriptorLength = 0;
          userDescRsp->UserDescriptor[0] = 0; // null terminate
          length = SZL_ZdoUserDescriptorRespParams_t_SIZE(1);

          if (znpDataLength >= 4)
            {
              tid = dp[3];
            }
        }

      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      sapMsgSetAppDataLength(Status, dataMessage, length);
      sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}









/******************************************************************************
 * ZDO User Descriptor Set Request
 ******************************************************************************/
static
void zabZnpZdoUtility_UserDescriptorSetReq( erStatus* Status, zabService* Service,
                                           unsigned16 nwkAddr, unsigned16 nwkAddrOfInterest,
                                           unsigned8 DescriptorLength, unsigned8* Descriptor,
                                           unsigned8 tid)
{
  #define M_USER_DESC_SET_HEADER_LENGTH 6
  sapMsg* Msg = NULL;
  unsigned8* buffer;
  unsigned16 sendCount;
  unsigned16 znpLength = DescriptorLength + M_USER_DESC_SET_HEADER_LENGTH;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, znpLength);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, (unsigned8)znpLength);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_USER_DESC_SET);
  sapMsgSetAppTransactionId(Status, Msg, tid);

  /* load the data area */
  sendCount = 0;
  buffer = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddr); sendCount += MACRO_DATA_SIZE_WORD;
  COPY_OUT_16_BITS(&buffer[sendCount], nwkAddrOfInterest); sendCount += MACRO_DATA_SIZE_WORD;
  buffer[sendCount++] = DescriptorLength;
  osMemCopy(Status, &buffer[sendCount], Descriptor, DescriptorLength); sendCount += DescriptorLength;
  buffer[sendCount] = tid;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO User Descriptor Set Request
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpZdoUtility_UserDescriptorSetReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned16 length;
  SZL_ZdoUserDescriptorSetReqParams_t* dp;

  /* Confirm data length is correct before using it */
  length = sapMsgGetAppDataLength(Message);
  if (length < SZL_ZdoUserDescriptorSetReqParams_t_SIZE(0))
    {
      erStatusSet(Status, SAP_ERROR_MSG_INVALID_LENGTH ); return;
    }

  dp = (SZL_ZdoUserDescriptorSetReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  zabZnpZdoUtility_UserDescriptorSetReq(Status, Service,
                                        dp->NetworkAddress, dp->NetworkAddressOfInterest,
                                        dp->UserDescriptorLength, dp->UserDescriptor,
                                        sapMsgGetAppTransactionId(Message));
}

/******************************************************************************
 * ZDO User Descriptor Set Response
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
static void zabZnp_ProcessUserDescriptorSetRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  sapMsg* dataMessage = NULL;
  unsigned8 tid = 0;
  SZL_ZdoUserDescriptorSetRespParams_t* userDescSetRsp;
  unsigned8 length;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

  /* Validate length before using data */
  if (znpDataLength >= 3)
    {

      // Allocate New Message
      length = SZL_ZdoUserDescriptorSetRespParams_t_SIZE;
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_USER_DESC_SET_RSP);

      /* Extract from the ZNP command into the data sap structure */
      userDescSetRsp = (SZL_ZdoUserDescriptorSetRespParams_t*)sapMsgGetAppData(dataMessage);
      userDescSetRsp->Status = (SZL_ZDO_STATUS_t)dp[2];
      userDescSetRsp->SourceAddress = COPY_IN_16_BITS(&dp[0]);

      if ( (userDescSetRsp->Status == SZL_ZDO_STATUS_SUCCESS) && (znpDataLength >= 5))
        {
          userDescSetRsp->NetworkAddressOfInterest = COPY_IN_16_BITS(&dp[3]);

          if (znpDataLength >= 6)
            {
              tid = dp[5];
            }
        }
      else if (znpDataLength >= 4)
        {
          tid = dp[3];
        }

      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      sapMsgSetAppDataLength(Status, dataMessage, length);
      sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Network Leave Indication
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
static void processNwkLeaveInd(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  zabMsgPro_NwkLeaveInd* leaveInd;
  unsigned8 tid = 0;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  ER_CHECK_NULL(Status, dp);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);

  /* Check there is at least the minimum frame length in the data*/
  if (znpDataLength >= 13)
    {
      // Allocate New Message
      length = zabMsgPro_NwkLeaveInd_SIZE;
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_NWK_LEAVE_IND);

      /* Extract from the ZNP command into the data sap structure */

      leaveInd = (zabMsgPro_NwkLeaveInd*)sapMsgGetAppData(dataMessage);
      leaveInd->srcNwkAddress = COPY_IN_16_BITS(&dp[0]);
      leaveInd->srcIeeeAddress = COPY_IN_64_BITS(&dp[2]);
      leaveInd->request = dp[10];
      leaveInd->removeChildren = dp[11];
      leaveInd->rejoin = dp[12];

      sapMsgSetAppDataLength(Status, dataMessage, length);
      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * ZDO Get Trust Center Ping Time Request
 ******************************************************************************/
void zabZnpZdoUtility_TcPingTimeGetReq( erStatus* Status, zabService* Service)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;

  data_length = 0;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_GET_PING_TIMES);

  zabParseZNPSetLength(Status, Msg, data_length);

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * ZDO Set Trust Center Ping Time Request
 ******************************************************************************/
void zabZnpZdoUtility_TcPingTimeSetReq( erStatus* Status, zabService* Service,
                                        unsigned32 PingTimeSlowMs, unsigned32 PingTimeFastMs)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;
  unsigned8* msgData;

  data_length = 8;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_ZDO);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_ZDO_SET_PING_TIMES);

  zabParseZNPSetLength(Status, Msg, data_length);
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_32_BITS(msgData, PingTimeSlowMs); msgData += MACRO_DATA_SIZE_DOUBLE_WORD;
  COPY_OUT_32_BITS(msgData, PingTimeFastMs); msgData += MACRO_DATA_SIZE_DOUBLE_WORD;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Process a Get/Set Trust Center Ping Time Response
 ******************************************************************************/
static void processPingTimeRsp(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 status;
//  unsigned8 version;
  unsigned32 PingTimeSlowMs;
  unsigned32 PingTimeFastMs;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  ER_CHECK_NULL(Status, dp);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);

  /* Check there is at least the minimum frame length in the data*/
  if (znpDataLength >= 10)
    {
      /* Extract from the ZNP command into the data sap structure */
      status = *dp++;
      dp++; // version = *dp++;
      PingTimeSlowMs = COPY_IN_32_BITS(dp); dp += MACRO_DATA_SIZE_DOUBLE_WORD;
      PingTimeFastMs = COPY_IN_32_BITS(dp); dp += MACRO_DATA_SIZE_DOUBLE_WORD;

      srvCoreGiveBack32(Status, Service, ZAB_GIVE_NETWORK_MAINTENANCE_SLOW_PING_TIME_MS, PingTimeSlowMs);
      srvCoreGiveBack32(Status, Service, ZAB_GIVE_NETWORK_MAINTENANCE_FAST_PING_TIME_MS, PingTimeFastMs);

      zabZnpNwk_nwkMaintenaceParamSrspHandler(Status, Service, (ZNPI_API_ERROR_CODES)status, PingTimeSlowMs, PingTimeFastMs);
    }
  else
    {
      zabZnpNwk_nwkMaintenaceParamSrspHandler(Status, Service, ZNPI_API_ERR_FAILURE, 0, 0);
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process incoming ZDO messages
 ******************************************************************************/
void zabZnpZdoUtility_ProcessIncomingMessage(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8 test;
  ZNPI_CMD_ZDO_COMMANDCODES cmd;
  unsigned8 result;

  zabParseZNPGetCommandID(Status, Message, &test);
  cmd = (ZNPI_CMD_ZDO_COMMANDCODES)test;

  switch (cmd)
    {
      case ZNPI_CMD_ZDO_IEEE_ADDR:
      case ZNPI_CMD_ZDO_NWK_ADDR:
      case ZNPI_CMD_ZDO_POWER_DESC:
      case ZNPI_CMD_ZDO_NODE_DESC:
      case ZNPI_CMD_ZDO_ACTIVE_EP:
      case ZNPI_CMD_ZDO_SIMPLE_DESC:
      case ZNPI_CMD_ZDO_MATCH_DESC:
      case ZNPI_CMD_ZDO_BIND:
      case ZNPI_CMD_ZDO_UNBIND:
      case ZNPI_CMD_ZDO_DEVICE_ANNCE:
      case ZNPI_CMD_ZDO_MGMT_BIND:
      case ZNPI_CMD_ZDO_MGMT_LQI:
      case ZNPI_CMD_ZDO_MGMT_RTG:
      case ZNPI_CMD_ZDO_MGMT_LEAVE:
      case ZNPI_CMD_ZDO_MGMT_NWK_UPDATE:
      case ZNPI_CMD_ZDO_USER_DESC:
      case ZNPI_CMD_ZDO_USER_DESC_SET:
        getZdoStatus(Status, Message, &result);

        printVendor(Service, "ZNP ZDO: SRSP: Status = 0x%02X\n", result);

        /* ARTF67562: Speed up failure notification by reporting SRSP failure to SZL */
        if (result != ZNPI_API_ERR_SUCCESS)
          {
            printError(Service, "ERROR: zabZnpZdoUtility_ProcessIncomingMessage SRSP: TID = 0x%02X, Status : 0x%02X\n", sapMsgGetAppTransactionId(Message), result);

            /* Notify error up to App data service */
            zabParseZNP_AppErrorIn(Service, sapMsgGetAppTransactionId(Message), ZAB_ERROR_VENDOR_SENDING);
          }
        break;

      case ZNPI_CMD_ZDO_UPDATE_NWK_KEY:
        getZdoStatus(Status, Message, &result);
        zabZnpNwk_nwkKeyUpdateSrspHandler(Status, Service, (ZNPI_API_ERROR_CODES)result);
        break;

      case ZNPI_CMD_ZDO_SWITCH_NWK_KEY:
        getZdoStatus(Status, Message, &result);
        zabZnpNwk_nwkKeySwitchSrspHandler(Status, Service, (ZNPI_API_ERROR_CODES)result);
        break;

      case ZNPI_CMD_ZDO_IEEE_ADDR_IND:
        printVendor(Service, "ZNP ZDO: IEEE Addr Rsp\n");
        zabZnp_ProcessIeeeAddrRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_NWK_ADDR_IND:
        printVendor(Service, "ZNP ZDO: Nwk Addr Rsp\n");
        zabZnp_ProcessNwkAddrRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_POWER_DESC_IND:
        printVendor(Service, "ZNP ZDO: Power Descriptor Rsp\n");
        zabZnp_ProcessPowerDescRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_NODE_DESC_IND:
        printVendor(Service, "ZNP ZDO: Node Descriptor Rsp\n");
        zabZnp_ProcessNodeDescRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_ACTIVE_EP_IND:
        printVendor(Service, "ZNP ZDO: Active Endpoint Rsp\n");
        zabZnp_ProcessActiveEndpointRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_SIMPLE_DESC_IND:
        printVendor(Service, "ZNP ZDO: Simple Descriptor Rsp\n");
        zabZnp_ProcessSimpleDescriptorRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_MATCH_DESC_IND:
        printVendor(Service, "ZNP ZDO: Match Descriptor Rsp\n");
        zabZnp_ProcessMatchDescriptorRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_USER_DESC_IND:
        printVendor(Service, "ZNP ZDO: User Descriptor Rsp\n");
        zabZnp_ProcessUserDescriptorRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_USER_DESC_CONF_IND:
        printVendor(Service, "ZNP ZDO: User Descriptor Set Rsp\n");
        zabZnp_ProcessUserDescriptorSetRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_BIND_IND:
        printVendor(Service, "ZNP ZDO: Bind Rsp\n");
        zabZnp_ProcessBindRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_UNBIND_IND:
        printVendor(Service, "ZNP ZDO: UnBind Rsp\n");
        zabZnp_ProcessUnBindRsp(Status, Service, Message);
        break;


      case ZNPI_CMD_ZDO_MGMT_LQI_IND:
        printVendor(Service, "ZNP ZDO: Mgmt Lqi Rsp\n");
        zabZnp_ProcessMgmtLqiRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_MGMT_RTG_IND:
        printVendor(Service, "ZNP ZDO: Mgmt Rtg Rsp\n");
        zabZnp_ProcessMgmtRtgRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_MGMT_BIND_IND:
        printVendor(Service, "ZNP ZDO: Mgmt Bind Rsp\n");
        zabZnp_ProcessMgmtBindRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_MGMT_LEAVE_IND:
        printVendor(Service, "ZNP ZDO: Mgmt Leave Rsp\n");
        zabZnp_ProcessMgmtLeaveRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_MGMT_PERMIT_JOIN_IND:
        printVendor(Service, "ZNP ZDO: Mgmt PJ Rsp - (not used)\n");
        break;

      case ZNPI_CMD_ZDO_MGMT_NWK_UPDATE_IND:
        zabZnp_ProcessMgmtNwkUpdateNtf(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_STATE_CHANGE_IND:
        printVendor(Service, "ZNP MESSAGE ZDO_STATE_CHANGE_IND :\n");
        zabZnp_ProcessChangeStateIndication(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_STARTUP_FROM_APP:
        getZdoStatus(Status, Message, &result);
        zabZnpNwk_startupFromAppRspHandler(Status, Service, result);
        break;

      case ZNPI_CMD_ZDO_NWK_DISC_REQ:
        getZdoStatus(Status, Message, &result);
        zabZnpNwk_nwkDiscoveryRspHandler(Status, Service, (ZNPI_API_ERROR_CODES)result);
        break;

      case ZNPI_CMD_ZDO_JOIN_REQ:
        getZdoStatus(Status, Message, &result);
        zabZnpNwk_joinRspHandler(Status, Service, (ZNPI_API_ERROR_CODES)result);
        break;



      /* This stuffs sync and async in the same handler - not beautiful... */
      case ZNPI_CMD_ZDO_END_DEVICE_ANNCE_IND:
        processDeviceAnnounce(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_BEACON_NOTIFY_IND:
        processBeacon(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_JOIN_CNF:
        getZdoStatus(Status, Message, &result);
        zabZnpNwk_joinCnfHandler(Status, Service, (ZNPI_API_ERROR_CODES)result);
        break;
      case ZNPI_CMD_ZDO_NWK_DISCOVERY_CNF:
        getZdoStatus(Status, Message, &result);
        zabZnpNwk_NwkDiscoveryCnfHandler(Status, Service, (ZNPI_API_ERROR_CODES)result);
        break;


      case ZNPI_CMD_ZDO_NWK_LEAVE_IND:
        processNwkLeaveInd(Status, Service, Message);
        break;

      case ZNPI_CMD_ZDO_MGMT_PERMIT_JOIN:
        getZdoStatus(Status, Message, &result);
        zabZnpNwk_mgmtPermitJoinSrspHandler(Status, Service, (ZNPI_API_ERROR_CODES)result);
        break;


      /* Warning: Custom Schneider Command!
       * Only data is a single byte containing the Rejoin boolean*/
      case ZNPI_CMD_ZDO_LEAVE_LOCAL_IND:
        getZdoStatus(Status, Message, &result);
        zabZnpNwk_localLeaveHandler(Status, Service, result);
        break;

      /* Only data is a single byte containing the PJ time */
      case ZNPI_CMD_ZDO_PERMIT_JOIN_IND:
      case ZNPI_CMD_ZDO_PERMIT_JOIN_IND_SCHNEIDER: /* Warning: Custom Schneider Command! */
        getZdoStatus(Status, Message, &result);
        zabZnpNwk_permitJoinIndicationHandler(Status, Service, result);
        //sapMsgPutNotifyNetworkPermitJoinState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, result);
        break;



      case ZNPI_CMD_ZDO_GET_PING_TIMES:
      case ZNPI_CMD_ZDO_SET_PING_TIMES:
        processPingTimeRsp(Status, Service, Message);
        break;

      default:
        printError(Service, "<<ATTENTION>> Unhandled ZNP message for ZDO Substring [%#02x]\n", (unsigned8)cmd);
        break;
    }
}


/******************************************************************************
 * This function returns string of ZDO State
 ******************************************************************************/
char* zabZnpZdoUtility_GetZdoStateString(ZDO_STATE zdoStatus)
{
  switch( zdoStatus )
    {
      case DEV_HOLD: return "DEV_HOLD";
      case DEV_INIT: return "DEV_INIT";
      case DEV_NWK_DISC: return "DEV_NWK_DISC";
      case DEV_NWK_JOINING: return "DEV_NWK_JOINING";
      case DEV_NWK_REJOIN: return "DEV_NWK_REJOIN";
      case DEV_END_DEVICE_UNAUTH: return "DEV_END_DEVICE_UNAUTH";
      case DEV_END_DEVICE: return "DEV_END_DEVICE";
      case DEV_ROUTER: return "DEV_ROUTER";
      case DEV_COORD_STARTING: return "DEV_COORD_STARTING";
      case DEV_ZB_COORD: return "DEV_ZB_COORD";
      case DEV_NWK_ORPHAN: return "DEV_NWK_ORPHAN";
      default: return "DEV_UNKOWN";
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
