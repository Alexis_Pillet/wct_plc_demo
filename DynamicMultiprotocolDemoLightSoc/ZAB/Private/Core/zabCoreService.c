/*
 Name:    ZigBee Application Brick Interface
 Author:  ZigBee Excellence Center
 Company: Schneider Electric

 Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

 Description:
    ZigBee Application Brick Interface
 *
 * MODIFICATION HISTORY:
 * Core Rev     Date       Author  Change Description
 * 00.00.06.00  10-Jul-14   MvdB   ARTF67933: Fix alignment issue for ComX
 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 00.00.06.02  30-Sep-14   MvdB   artf55512: Support network key change action
 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 * 00.00.06.06  29-Oct-14   MvdB   artf106645: Fix length check issue in zabCoreAllocateApp
 * 01.00.00.02  28-Jan-15   MvdB   ARTF111653: Split in/out buffer ask into separate items for short and long
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
 * 002.000.007  14-Apr-15   MvdB   ARTF130627: Tidy up any active state machines upon CLOSE from glue/vendor
 * 002.000.009  16-Apr-15   MvdB   Return timeout from zabCoreEzMode_Work()
 * 002.001.004  21-Jul-15   MvdB   ARTF134686: Rework zabCoreDestroy to detect and return as early as possible if there is an error.
 * 002.002.010  09-Oct-15   MvdB   ARTF150980: Support Service ID in memory allocation functions for WTB
 * 002.002.013  14-Oct-15   MvdB   ARTF151072: Add service pointer to osTimeGetMilliseconds() and osTimeGetSeconds()
 * 002.002.021  21-Apr-16   MvdB   ARTF167736: Add ZAB_ACTION_INTERNAL_SERIAL_FLUSH, only allow app actions < ZAB_ACTION_MAX
 * 002.002.031  09-Jan-17   MvdB   ARTF172881: Avoid work() returning zero due to errors like szl not initialised
*/
#include "zabCorePrivate.h"
#include "zabSerialServicePrivate.h"


/******************************************************************************
 *                      *****************************
 *                 *****      QUEUE MANAGEMENT       *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Free all items in a queue, but leave the queue intact
 ******************************************************************************/
void zabCoreFreeQueue(erStatus* Status, qSafeType* Queue)
{
  sapMsg* Msg;

  /*
   * This free is super dodgy!
   * It frees messages using a sap free function, but these queues are SAP independent!
   */

  ER_CHECK_NULL(Status, Queue);
  Msg = qSafeRemove(Queue);
  while (Msg != NULL)
    {
      sapMsgFree(Status, Msg); // puts on free queue
      Msg = qSafeRemove(Queue);
    }
}

/******************************************************************************
 * Allocate a queue of Count items of size Size
 ******************************************************************************/
static qSafeType* zabCoreAllocateQueue(erStatus* Status, zabServiceStruct* This, unsigned32 Size, unsigned32 Count)
{
    unsigned8* Buffer;
    qSafeType* Queue;
    size_t Length;
    unsigned8 MutexId;

    /* ARTF67933: We need to make sure size is compatible with the alignment, as one big malloc is done, then divided up by size.
     *            If we let an item be unaligned it can cause seg fault if cast to a structure on alignment critical machines. */
    while (Size & ZAB_M_ALIGNMENT_MASK)
      {
        Size++;
      }

    Length = sizeof(qSafeType) + (Size * Count);

    ER_CHECK_STATUS_ZERO(Status);

    Buffer = (unsigned8 *) OS_MEM_MALLOC(Status,
                                         srvCoreGetServiceId(Status, This),
                                         Length,
                                         MALLOC_ID_CFG_QUEUE);
    if (Buffer == NULL)
      {
        erStatusSet(Status, OS_ERROR_MEM_NONE);
        return 0;
      }

    osMemZero(Status, (unsigned8*)Buffer, Length); // zero the memory

    Queue = (qSafeType*) Buffer;

    MutexId = ZAB_PROTECT_CREATE(This);
    qSafeInitialize(This, Queue, MutexId);

    /* ARTF67933: This is dangerous on machines that require memory alignment.
     *            We must ensure Buffer is always correctly alligned!!!!
     *            sizeof(qSafeType) and Size are no carefully managed to do this. Be careful if you change anything!*/
    Buffer += sizeof (qSafeType); // move buffer pointer to Msg data area
    while (Count-- && erStatusIsOk(Status))
    {
        sapMsg* Msg = (sapMsg *) Buffer;
        qSafeAppend(Queue, Msg);
        Buffer += Size;
    }

    return Queue;
}

/******************************************************************************
 * Destroy a queue
 ******************************************************************************/
static void zabCoreDestroyQueue (erStatus* Status, qSafeType* Queue)
{
  if (Queue != NULL)
    {
      ZAB_PROTECT_RELEASE(Queue->Service, qSafeId(Queue));
      OS_MEM_FREE(Status, srvCoreGetServiceId(Status, Queue->Service),(void*) Queue);
    }
}


/******************************************************************************
 * Notification Handler for the core
 * This allows the core to capture notifications and store state internally
 ******************************************************************************/
static void zabCoreNotificationHandler (erStatus* Status, sapHandle Sap, sapMsg* Message)
{

  zabServiceStruct* Service = ZAB_SRV(sapCoreGetService(Sap));

  ER_CHECK_NULL(Status, Message);

  if (sapMsgGetType(Message) == SAP_MSG_TYPE_NOTIFY)
    {
      switch(sapMsgGetNotifyType(Message))
        {
          case ZAB_NOTIFICATION_OPEN_STATE:
            Service->openState = sapMsgGetNotifyData(Message).openState;

            /* Kill core state machines if we are closing or closed */
            if ( (Service->openState == ZAB_OPEN_STATE_CLOSED) ||
                 (Service->openState == ZAB_OPEN_STATE_CLOSING) )
              {
                zabCoreEzMode_Init(Status, Service);
              }

            break;

          case ZAB_NOTIFICATION_NETWORK_STATE:

            /* In the case of zab closed and re-opened with SZL still running, re-register SZL endpoints */
            if ( (Service->nwkState != sapMsgGetNotifyData(Message).nwkState) &&
                 (sapMsgGetNotifyData(Message).nwkState == ZAB_NWK_STATE_NETWORKED) )
              {
                SzlZab_EndpointRegister(Service);
              }

            Service->nwkState = sapMsgGetNotifyData(Message).nwkState;

#ifdef ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING
            zabCoreEzMode_NwkStateNotificationHandler(Status, Service, Service->nwkState);
#endif
            break;

          case ZAB_NOTIFICATION_NETWORK_PERMIT_JOIN:
          case ZAB_NOTIFICATION_FIRMWARE_UPDATE_STATE:
          case ZAB_NOTIFICATION_FIRMWARE_UPDATE_PROGRESS:
          case ZAB_NOTIFICATION_CLONE_STATE:
          case ZAB_NOTIFICATION_ERROR:
          case ZAB_NOTIFICATION_CHANNEL_CHANGE_STATE:
          case ZAB_NOTIFICATION_NETWORK_KEY_CHANGE_STATE:
          case ZAB_NOTIFICATION_CLONE_PROGRESS:
          case ZAB_NOTIFICATION_NETWORK_MAINTENANCE_PARAM_STATE:
          case ZAB_NOTIFICATION_NETWORK_INFO_STATE:
            // Do nothing for now
            break;

          default:
            printError(Service, "ZAB Core: WARNING: Unknown notification type 0x%02X\n", (unsigned8)sapMsgGetNotifyType(Message));
            break;
        }
    }

}

/******************************************************************************
 * Give the app version info from the core
 ******************************************************************************/
static void giveBackVersionInfo(erStatus* Status, zabService* Service)
{
    unsigned8 coreVersion[4] = {ZAB_VERSION_MAJOR, ZAB_VERSION_MINOR, ZAB_VERSION_RELEASE, ZAB_VERSION_BUILD};

    ER_CHECK_STATUS(Status);

    srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_CORE_VERSION, sizeof(coreVersion), coreVersion);
}

// INPUT FUNCTIONS FOR SAPS //////////////////////////////////////////////////////////////
// MESSAGE ALLOCATE/FREE  ////////////////////////////////////////////////////////

unsigned16 zabMsgSizeLong (void)
{
    return (unsigned16) (SAP_MSG_HEADER_SIZE + ZAB_VENDOR_MAX_DATA);
}

unsigned16 zabMsgSizeShort (void)
{
    return (unsigned16) (SAP_MSG_HEADER_SIZE + ZAB_VENDOR_MIN_DATA);
}

static void* AllocateCore (erStatus* Status, sapHandle Sap, sapMsgType Type, sapMsgDirection Direction, unsigned32 DataLength)
{
    zabServiceStruct*   This            = ZAB_SRV(SAP_SERVICE(Sap));
    void*               Msg             = NULL;
    unsigned16          sapMsgLength    = SAP_MSG_EVENT_SIZE;
    erStatus            localStatus;

    erStatusClear(&localStatus, This);

    if (DataLength)
    {
        erStatusSet(Status, SAP_ERROR_MSG_INVALID_LENGTH); //event has no data, data length is 0 as default
        return NULL;
    }

    printSap(This, "AllocateCore::Event Message Allocation in FreeCoreQ\n");
    Msg = qSafeRemove(This->FreeCoreQ);
    if (Msg == NULL)
    {
        erStatusSet(Status, SAP_ERROR_MSG_NOT_AVAILABLE);
        return NULL;
    }

    osMemZero(&localStatus, (unsigned8*)Msg, sapMsgLength);

    if (erStatusIsError(&localStatus))
    {
        erStatusSet(Status, erStatusGetError(&localStatus));
        return NULL;
    }

    sapMsgSetType(Status, Msg, Type);

    sapMsgSetDirection(Status, Msg, Direction);

    SAP_MSG_SET_SAP(Msg, Sap);

    return (void*) Msg;
}

static void FreeCore (erStatus* Status, sapHandle Sap, sapMsg* Message)
{
    zabServiceStruct* This = ZAB_SRV(SAP_SERVICE(Sap));

    ER_CHECK_NULL(Status, Message);

    qSafeAppend(This->FreeCoreQ, Message);
}

/******************************************************************************
 * Allocate a message for the Application.
 * Used by data and serial SAPs.
 * Gets a spare buffer from one of the free queues
 ******************************************************************************/
void* zabCoreAllocateApp (erStatus* Status, sapHandle Sap, sapMsgType Type, sapMsgDirection Direction, unsigned32 DataLength)
{
    zabServiceStruct* This = ZAB_SRV(SAP_SERVICE(Sap));
    sapMsgPool Pool = SAP_MSG_POOL_SHORT;
    unsigned16 totalBufferSize = zabMsgSizeShort();
    unsigned16 maxDataLength = ZAB_VENDOR_MIN_DATA; //zabMsgSizeShort();
    void* Msg;
#ifdef ZAB_CORE_M_ENABLE_QUEUE_PROFILING
    unsigned8 qCount;
#endif

    printSap(This, "zabCoreAllocateApp:: App Message Allocation in FreeMsgQ. Type = 0x%X, Dir=0x%X, Len=0x%X, status = 0x%04X\n", Type, Direction,DataLength, Status->Error);
    ER_CHECK_STATUS_ZERO(Status);

    if (Type != SAP_MSG_TYPE_APP) // only app messages have data and datalength
    {
        return AllocateCore(Status, Sap, Type, Direction, 0);
    }


    if (DataLength > ZAB_VENDOR_MAX_DATA)
    {
        erStatusSet(Status, ER_ERROR_BUFFER_OVERFLOW);
        return NULL;
    }
    else if (DataLength > ZAB_VENDOR_MIN_DATA)
    {
        Pool = SAP_MSG_POOL_LONG;
        totalBufferSize = zabMsgSizeLong();
        maxDataLength = ZAB_VENDOR_MAX_DATA;
    }

    Msg = qSafeRemove(This->FreeMsgQ[ ZAB_FREE_DIRECTION(Direction) ][ Pool ]);

    if (Msg == NULL)
      {
#ifdef ZAB_CORE_M_ENABLE_QUEUE_PROFILING
        This->FreeMsgQFullCount[ ZAB_FREE_DIRECTION(Direction) ][ Pool ]++;
#endif

        printError(This, "zabCoreService: ERROR: zabCoreAllocateApp() failed to allocate a app message from pool: %s, %s\n",
                   zabUtility_GetSapMsgDirectionString(Direction),
                   (Pool == SAP_MSG_POOL_SHORT) ? "SHORT" : "LONG");
      }
#ifdef ZAB_CORE_M_ENABLE_QUEUE_PROFILING
    // keep track of the smallest number of free messages
    qCount = qSafeCount(This->FreeMsgQ[ ZAB_FREE_DIRECTION(Direction) ][ Pool ]);
    if (qCount < This->FreeMsgQMinFreeCount[ ZAB_FREE_DIRECTION(Direction) ][ Pool ])
      {
        This->FreeMsgQMinFreeCount[ ZAB_FREE_DIRECTION(Direction) ][ Pool ] = qCount;
      }
#endif

    if (Msg != NULL)
      {
        osMemZero(Status, (unsigned8*)Msg, totalBufferSize); // attention: all message data gets initialized here
        sapMsgSetAppDataMax(Status, Msg, maxDataLength); //set Message Data Size firstly
        sapMsgSetAppDataLength(Status, Msg, 0); // zero data length
        sapMsgSetType(Status, Msg, (sapMsgType) Type); // need to set the type for other message functions to work
        sapMsgSetDirection(Status, Msg, Direction); // set direction here so we know where to free the message
        SAP_MSG_SET_SAP(Msg, Sap);
        SAP_MSG_SET_POOL(Msg, Pool); // set the size option (long or short)
        SAP_MSG_SET_APP_DATA(Msg, SAP_MSG_GET_APP_DATA_BUFFER(Msg)); //    sapMsgGetAppHeader(Msg)); // move app data pointer to SAP message
      }
    return (void*) Msg;
}

/******************************************************************************
 * Free a message from the Application.
 * Used by data and serial SAPs.
 * Puts buffer back onto the free queues
 ******************************************************************************/
void zabCoreFreeApp (erStatus* Status, sapHandle Sap, sapMsg* Message)
{
    zabServiceStruct* This = ZAB_SRV(SAP_SERVICE(Sap));
    ER_CHECK_NULL(Status, Message);

    printSap(This, "zabCoreFreeApp::Free App Message\n");

    if (sapMsgGetType(Message) != SAP_MSG_TYPE_APP)    // only app messages have data and datalength
    {
        FreeCore(Status, Sap, Message);
    }
    else
    {
        sapMsg* Msg = Message;
        unsigned8 Direction = sapMsgGetDirection(Msg);

        qSafeAppend(This->FreeMsgQ[ ZAB_FREE_DIRECTION(Direction) ][ SAP_MSG_GET_POOL(Msg) ], Message);

    }
}


#ifdef ZAB_CORE_M_ENABLE_QUEUE_PROFILING
/******************************************************************************
 * Print info about Queue status for queue profiling
 ******************************************************************************/
void zabCore_PrintQInfo(zabService* Service)
{
  zabServiceStruct* This = ZAB_SRV(Service);

  printApp(Service, "Message Queue Info:\n");

  printApp(Service, " In Short: Size = %d, Min Free Count = %d, Full Count = %d\n",
           This->FreeMsgQSize[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_SHORT ],
           This->FreeMsgQMinFreeCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_SHORT ],
           This->FreeMsgQFullCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_SHORT ]);

  printApp(Service, " In Long: Size = %d, Min Free Count = %d, Full Count = %d\n",
           This->FreeMsgQSize[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_LONG ],
           This->FreeMsgQMinFreeCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_LONG ],
           This->FreeMsgQFullCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_LONG ]);

  printApp(Service, " Out Short: Size = %d, Min Free Count = %d, Full Count = %d\n",
           This->FreeMsgQSize[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_SHORT ],
           This->FreeMsgQMinFreeCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_SHORT ],
           This->FreeMsgQFullCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_SHORT ]);

  printApp(Service, " Out Long: Size = %d, Min Free Count = %d, Full Count = %d\n",
           This->FreeMsgQSize[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_LONG ],
           This->FreeMsgQMinFreeCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_LONG ],
           This->FreeMsgQFullCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_LONG ]);
}
#endif

/******************************************************************************
 * Create ZAB
 * At creation all memory is allocated and configuration data is asked back to the app. If configuration
 * data is needed my the app, it is also given back to the app depending on compile time options.
 *
 * Parameters:
 *  Status: Pointer to ZAB Status. Error must be ZAB_SUCCESS for function to execute.
 *  Id: Service ID of ZAB. Used for multiple instances.
 *  ConfigureBack: Function pointer by which ZAB instance will ASK for configuration values
 *  GiveBack: Function pointer by which ZAB instance will GIVE data
 *
 * Returns:
 *  Status: Pointer to ZAB Status. Error indicates success/failure of the function
 *  zabService*: Pointer to instance of ZAB.
 *               Application should store this is the Service pointer for accessing this instance of ZAB.
 ******************************************************************************/



zabService* zabCoreCreate(erStatus* Status,
                          unsigned8 Id,
                          srvConfigureBack ConfigureBack,
                          srvGiveBack GiveBack,
                          zabSerialService_ActionOutCallback_t ActionOutCallback,
                          zabSerialService_SerialOutCallback_t SerialOutCallback)
{
    zabServiceStruct* This;
    zabService* Service;
    unsigned16 ReceiveBufferCount;
    unsigned16 SendBufferCount;
    unsigned16 BufferCount[ ZAB_FREE_DIRECTION_COUNT ][ SAP_MSG_POOL_COUNT ];
    unsigned8 MutexId;

    ER_CHECK_STATUS_ZERO(Status);

    /* Validate Service Id */
    if (Id == ZAB_CORE_SERVICE_ID_INVALID)
      {
        erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
        return NULL;
      }

    Service = (zabService*)OS_MEM_MALLOC(Status,
                                         Id,
                                         sizeof(zabServiceStruct),
                                         MALLOC_ID_CFG_CORE);

    // Check it malloced ok, if not return null
    ER_CHECK_STATUS_NULL_ZERO(Status, Service);

    osMemZero(Status, (unsigned8*)Service, sizeof ( zabServiceStruct));

    // must do these first for ask back function to work
    srvCoreSetServiceId(Status, Service, Id);
    srvCoreSetConfigureBack(Status, Service, ConfigureBack);
    srvCoreSetGiveBack(Status, Service, GiveBack);

    This = ZAB_SRV(Service);

    This->openState = ZAB_OPEN_STATE_CLOSED;
    This->nwkState = ZAB_NWK_STATE_UNINITIALISED;
    This->channel = ZAB_TYPES_M_NETWORK_CHANNEL_UNKNOWN;
    This->nwkAddress = ZAB_TYPES_M_NETWORK_ADDRESS_UNKNOWN;
    This->panID = ZAB_TYPES_M_NETWORK_PAN_ID_UNKNOWN;
    This->epid = ZAB_TYPES_M_NETWORK_EPID_UNKNOWN;
    This->ieee = ZAB_TYPES_M_IEEE_UNKNOWN;
    This->workActive = zab_false;

    /* EZ Mode - Must be done before the Asks start as it intercepts the ask handler */
#ifdef ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING
    zabCoreEzMode_Init(Status, Service);
#endif // ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING

    This->MutexId = ZAB_PROTECT_CREATE(Service);

    /* Create Manage SAP.
     * Sized for the number of handlers requested by the app + 2 handlers for internal use: core in, vendor out.
     * Register the two handlers.
     * Allocate the FreeCoreQ which is used for storage of buffers for Manage SAP events.
     * Buffers sit in FreeCoreQ when unused.
     * AllocateCore() grabs a buffer of this queue and FreeCore() puts them back - register these with the SAP */
    MutexId = ZAB_PROTECT_CREATE(Service);
    This->ManageSAP = sapCoreCreate(Status, (void*) This, srvCoreAskBack8(Status, Service, ZAB_ASK_SAP_MANAGE_MAX, 1) + 2, MutexId);
    if (erStatusIsError(Status) || (This->ManageSAP == NULL) )
      {
        zabCoreDestroy(Status, Service);
        return NULL;
      }

    sapCoreRegisterCallBack(Status, This->ManageSAP, SAP_MSG_DIRECTION_OUT, zabVendorService_CoreToVendorOut);
    sapCoreRegisterCallBack(Status, This->ManageSAP, SAP_MSG_DIRECTION_IN, zabCoreNotificationHandler);
    This->FreeCoreQ = zabCoreAllocateQueue(Status, This, SAP_MSG_EVENT_SIZE, srvCoreAskBack16(Status, Service, ZAB_ASK_BUFFER_COUNT_EVENT, 8));
    if (erStatusIsError(Status) || (This->FreeCoreQ == NULL) )
      {
        zabCoreDestroy(Status, Service);
        return NULL;
      }
    sapMsgRegAllocate(Status, This->ManageSAP, AllocateCore, FreeCore);

    /* Create Data SAP.
     * Sized for the number of handlers requested by the app + 2 handlers for internal use: core to vendor out, SZL in.
     * Register core to vendor out. Leave SZL in registration to szl init, in case it is not used.
     * Use zabCoreAllocateApp() and zabCoreFreeApp(), which grab buffer from FreeCoreQ (messy!) or the FreeMsgQs
     */
    MutexId = ZAB_PROTECT_CREATE(Service);
    This->DataSAP = sapCoreCreate(Status, (void*) This, srvCoreAskBack8(Status, Service, ZAB_ASK_SAP_DATA_MAX, 0) + 2, MutexId);
    if (erStatusIsError(Status) || (This->DataSAP == NULL) )
      {
        zabCoreDestroy(Status, Service);
        return NULL;
      }
    sapCoreRegisterCallBack(Status, This->DataSAP, SAP_MSG_DIRECTION_OUT, zabVendorService_CoreToVendorOut);
    sapMsgRegAllocate(Status, This->DataSAP, zabCoreAllocateApp, zabCoreFreeApp);

    MutexId = ZAB_PROTECT_CREATE(Service);
    qSafeInitialize(This, &This->VendorOutQ, MutexId);
    MutexId = ZAB_PROTECT_CREATE(Service);
    qSafeInitialize(This, &This->VendorInQ, MutexId);

    /* Create the FreeMsgQs
     * These are 4 queues for small/large buffers for in/out messages.
     * These are used by the data and serial SAPs for general data messages. */

    /* Originally we only had ZAB_ASK_BUFFER_COUNT_IN, then the value was divided between short and long.
     * This is not good as different products require different ratios.
     * ZAB_ASK_BUFFER_COUNT_IN_LONG has now been added in a backwards compatible way. If it does not return a good value we go back to the old default division.*/
    #define M_LONG_BUFFER_UNSUPPORTED_VALUE 0xFFFF
    ReceiveBufferCount = srvCoreAskBack16(Status, Service, ZAB_ASK_BUFFER_COUNT_IN, 8);
    BufferCount[ ZAB_FREE_DIRECTION_IN  ][ SAP_MSG_POOL_LONG  ] = srvCoreAskBack16(Status, Service, ZAB_ASK_BUFFER_COUNT_IN_LONG, M_LONG_BUFFER_UNSUPPORTED_VALUE);
    if (BufferCount[ ZAB_FREE_DIRECTION_IN  ][ SAP_MSG_POOL_LONG  ] == M_LONG_BUFFER_UNSUPPORTED_VALUE)
      {
        BufferCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_SHORT ] = ReceiveBufferCount / 2;
        BufferCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_LONG ] = (unsigned16) (ReceiveBufferCount - BufferCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_SHORT ]);
      }
    else
      {
        BufferCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_SHORT ] = ReceiveBufferCount;
      }

    /* Same backwards compatible stuff for our buffers */
    SendBufferCount = srvCoreAskBack16(Status, Service, ZAB_ASK_BUFFER_COUNT_OUT, 8);
    BufferCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_LONG  ] = srvCoreAskBack16(Status, Service, ZAB_ASK_BUFFER_COUNT_OUT_LONG, M_LONG_BUFFER_UNSUPPORTED_VALUE);
    if (BufferCount[ ZAB_FREE_DIRECTION_OUT  ][ SAP_MSG_POOL_LONG  ] == M_LONG_BUFFER_UNSUPPORTED_VALUE)
      {
        BufferCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_LONG ] = SendBufferCount / 4;
        BufferCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_SHORT ] = (unsigned16) (SendBufferCount - BufferCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_LONG ]);
      }
    else
      {
        BufferCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_SHORT ] = SendBufferCount;
      }
    if (erStatusIsError(Status))
      {
        zabCoreDestroy(Status, Service);
        return NULL;
      }


    This->FreeMsgQ[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_LONG ] = zabCoreAllocateQueue(Status, This, zabMsgSizeLong(), BufferCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_LONG ]);
    This->FreeMsgQ[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_SHORT ] = zabCoreAllocateQueue(Status, This, zabMsgSizeShort(), BufferCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_SHORT ]);
    This->FreeMsgQ[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_LONG ] = zabCoreAllocateQueue(Status, This, zabMsgSizeLong(), BufferCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_LONG ]);
    This->FreeMsgQ[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_SHORT ] = zabCoreAllocateQueue(Status, This, zabMsgSizeShort(), BufferCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_SHORT ]);

#ifdef ZAB_CORE_M_ENABLE_QUEUE_PROFILING
    This->FreeMsgQSize[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_LONG ] = BufferCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_LONG ];
    This->FreeMsgQSize[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_SHORT ] = BufferCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_SHORT ];
    This->FreeMsgQSize[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_LONG ] = BufferCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_LONG ];
    This->FreeMsgQSize[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_SHORT ] = BufferCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_SHORT ];

    This->FreeMsgQMinFreeCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_LONG ] = BufferCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_LONG ];
    This->FreeMsgQMinFreeCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_SHORT ] = BufferCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_SHORT ];
    This->FreeMsgQMinFreeCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_LONG ] = BufferCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_LONG ];
    This->FreeMsgQMinFreeCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_SHORT ] = BufferCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_SHORT ];

    This->FreeMsgQFullCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_LONG ] = 0;
    This->FreeMsgQFullCount[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_SHORT ] = 0;
    This->FreeMsgQFullCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_LONG ] = 0;
    This->FreeMsgQFullCount[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_SHORT ] = 0;
#endif

    if (erStatusIsError(Status) ||
        (This->FreeMsgQ[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_LONG ] == NULL) ||
        (This->FreeMsgQ[ ZAB_FREE_DIRECTION_IN ][ SAP_MSG_POOL_SHORT ] == NULL) ||
        (This->FreeMsgQ[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_LONG ] == NULL) ||
        (This->FreeMsgQ[ ZAB_FREE_DIRECTION_OUT ][ SAP_MSG_POOL_SHORT ] == NULL) )
      {
        zabCoreDestroy(Status, Service);
        return NULL;
      }


    /* Initialise services */
#ifdef ZAB_CFG_ENABLE_SZL
    szl_zab_Init(Status, Service);
#endif // ZAB_CFG_ENABLE_SZL

    /* Initialise Vendor */
    zabVendorService_Create(Status, This);

    /* Initialise Serial Callback*/
    zabSerialService_Init(Status, Service, ActionOutCallback, SerialOutCallback);

    /* Give Version information */
    giveBackVersionInfo(Status, Service);

    if (erStatusIsError(Status))
      {
        zabCoreDestroy(Status, Service);
        return NULL;
      }

    return This;
}


/******************************************************************************
 * Reset an instance of ZAB
 * Re-initialises data but keeps resources allocated
 ******************************************************************************/
void zabCoreReset(erStatus* Status, zabService* Service)
{

    ER_CHECK_STATUS(Status);
    ER_CHECK_NULL(Status, Service);

    zabCoreFreeQueue(Status, &ZAB_SRV(Service)->VendorOutQ);

#ifdef ZAB_CFG_ENABLE_SZL
    szl_zab_Reset(Status, Service);
#endif

#ifdef ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING
    zabCoreEzMode_Init(Status, Service);
#endif

    zabVendorService_Reset(Status, Service);
    printInfo(Service, "\n!!!!!!!!!!!!!zabCoreReset is done!!!!!!!!!!!!!\n\n");
}


/******************************************************************************
 * Destroy an instance of ZAB
 * Closes port and deallocates resources
 ******************************************************************************/
void zabCoreDestroy(erStatus* Status, zabService* Service)
{
    zabServiceStruct* This = ZAB_SRV(Service);
    unsigned8 direction;
    unsigned8 pool;

    ER_CHECK_STATUS(Status);
    ER_CHECK_NULL(Status, Service);

    // Start with the resets and make sure they work.
    // If they fail we can return the error and app can fix & try again
    zabCoreReset(Status, Service);
    ER_CHECK_STATUS(Status);

#ifdef ZAB_CFG_ENABLE_SZL
    // SZL destroy can fail if plugins are still registered.
    // Make sure we return if it not successful so everything stays intact and can be fixed!
    szl_zab_Destroy(Status, Service);
    ER_CHECK_STATUS(Status);
#endif

    // Now we start the real destruction.
    // After this point failures will probably not be fixable.
    ZAB_PROTECT_RELEASE(Service, qSafeId(&This->VendorOutQ));

    ZAB_PROTECT_RELEASE(Service, sapCoreGetMutexId(ZAB_SRV(Service)->ManageSAP));
    OS_MEM_FREE(Status, srvCoreGetServiceId(Status, Service), ZAB_SRV(Service)->ManageSAP);
    ZAB_PROTECT_RELEASE(Service, sapCoreGetMutexId(ZAB_SRV(Service)->DataSAP));
    OS_MEM_FREE(Status, srvCoreGetServiceId(Status, Service), ZAB_SRV(Service)->DataSAP);

    zabVendorService_Destroy(Status, Service);

    /* Destroy the free queues last - THIS IS CRITICAL - ALWAYS DO THIS LAST BEFORE FREEING THE CORE!
     * Buffers for messages are actually allocated here, other queues just use them for a while.
     * If you destroy these before freeing other queues, the pointers in those queues will be de-referenced. */
    zabCoreDestroyQueue(Status, This->FreeCoreQ);
    for (direction = 0; direction < ZAB_FREE_DIRECTION_COUNT; direction++)
      {
        for (pool = 0; pool < SAP_MSG_POOL_COUNT; pool++)
          {
            zabCoreDestroyQueue(Status, This->FreeMsgQ[direction][pool]);
          }
      }

    ZAB_PROTECT_RELEASE(Service, This->MutexId);

    OS_MEM_FREE(Status, srvCoreGetServiceId(Status, Service), (void*) Service);
}

/******************************************************************************
 * Get the Open State of ZAB, as returned in last notification
 ******************************************************************************/
zabOpenState zabCoreGetOpenState(zabService* Service)
{
  zabOpenState openState = ZAB_OPEN_STATE_NULL;

  if (Service != NULL)
    {
    ZAB_MUTEX_ENTER(Service, ZAB_SRV(Service)->MutexId);
    openState = ZAB_SRV(Service)->openState;
    ZAB_MUTEX_EXIT(Service, ZAB_SRV(Service)->MutexId);
    }
    return openState;
}

/******************************************************************************
 * Get the Network State of ZAB, as returned in last notification
 ******************************************************************************/
zabNwkState zabCoreGetNetworkState(zabService* Service)
{
  zabNwkState nwkState = ZAB_NWK_STATE_UNINITIALISED;

  if (Service != NULL)
    {
    ZAB_MUTEX_ENTER(Service, ZAB_SRV(Service)->MutexId);
    nwkState = ZAB_SRV(Service)->nwkState;
    ZAB_MUTEX_EXIT(Service, ZAB_SRV(Service)->MutexId);
    }

    return nwkState;
}


/******************************************************************************
 * Get if ZAB if ready to send command across the network (Open and Networked)
 ******************************************************************************/
zab_bool zabCoreService_GetZabNetworkReady(zabService* Service)
{
  zabOpenState openState;
  zabNwkState nwkState;

  openState = zabCoreGetOpenState(Service);
  nwkState = zabCoreGetNetworkState(Service);

  if ( (openState == ZAB_OPEN_STATE_OPENED) &&
       (nwkState == ZAB_NWK_STATE_NETWORKED) )
    {
      return zab_true;
    }

  return zab_false;
}

/******************************************************************************
 * Get the version of ZAB Core
 ******************************************************************************/
void zabCoreGetVersion (erStatus* Status, unsigned char* Version)
{
    ER_CHECK_STATUS_NULL(Status, Version); // debug check for null

    Version[ 0 ] = ZAB_VERSION_MAJOR;
    Version[ 1 ] = ZAB_VERSION_MINOR;
    Version[ 2 ] = ZAB_VERSION_RELEASE;
    Version[ 3 ] = ZAB_VERSION_BUILD;
}

/******************************************************************************
 * This does the work for the service, processing queues, state machines and timeouts.
 *
 * Return Value:
 * The longest delay in ms before work SHOULD be called again.
 *  - Zero is returned to request work be re-called immediately.
 *  - Calling work faster than the returned value is good.
 *  - Calling work slower than the returned value is ok but not recommended, as timeouts
 *    we be slow to occur.
 *  - It is ok to ignore this value and  just call work regularly on a timed tick or in a super loop.
 *  - Work MUST be called after calling an API function or feeding serial bytes into ZAB to update the returned value.
 ******************************************************************************/
unsigned32 zabCoreWork( erStatus* Status, zabService* Service )
{
  zab_bool workActive;
  unsigned32 timeNow;
  unsigned32 nextWorkRequiredDelayMs = ZAB_WORK_DELAY_MAX_MS;
  unsigned32 nextWorkRequiredDelayMsTemp;
  ER_CHECK_STATUS_VALUE(Status, nextWorkRequiredDelayMs);
  ER_CHECK_NULL_VALUE(Status, Service, nextWorkRequiredDelayMs);

  /* Use a flag to check if work is already active for the service.
   * If so, print big nasty errors and don't allow it! */
  ZAB_MUTEX_ENTER(Service, ZAB_SRV(Service)->MutexId);
  workActive = ZAB_SRV(Service)->workActive;
  ZAB_MUTEX_EXIT(Service, ZAB_SRV(Service)->MutexId);

  if (workActive == zab_false)
    {
      /* Flag work as active - THIS MUST BE FIRST IN THE IF!!! */
      ZAB_MUTEX_ENTER(Service, ZAB_SRV(Service)->MutexId);
      ZAB_SRV(Service)->workActive = zab_true;
      ZAB_MUTEX_EXIT(Service, ZAB_SRV(Service)->MutexId);

      osTimeGetMilliseconds(Service, &timeNow);

      nextWorkRequiredDelayMsTemp = zabVendorService_Work( Status, Service );   // process vendor in and app send queues
      nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

#ifdef ZAB_CFG_ENABLE_SZL
      nextWorkRequiredDelayMsTemp = szl_zab_Work(Status, Service, timeNow);
      nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);
#endif

#ifdef ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING
      nextWorkRequiredDelayMsTemp = zabCoreEzMode_Work(Status, Service);
      nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);
#endif

      /* Flag work as inactive - THIS MSUT BE THE LAST THING IN THE IF!!! */
      ZAB_MUTEX_ENTER(Service, ZAB_SRV(Service)->MutexId);
      ZAB_SRV(Service)->workActive = zab_false;
      ZAB_MUTEX_EXIT(Service, ZAB_SRV(Service)->MutexId);
    }
  else
    {
      printError(Service, "zabCoreService: CRITICAL ERROR - ZAB Work called when already active!!!\n"
                          "                                 This must not be done and has been disallowed, otherwise bad things happen\n");
    }

  // Tidy up range on delay
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, ZAB_WORK_DELAY_MAX_MS);
  return nextWorkRequiredDelayMs;
}

/******************************************************************************
 * Perform an Action on the ZAB
 *
 * Parameters:
 *  Status: Pointer to ZAB Status. Error must be ZAB_SUCCESS for function to execute.
 *  Service: Pointer to instance of ZAB.
 *  Action: Action to be performed. See zabAction for supported actions.
 ******************************************************************************/
void zabCoreAction(erStatus* Status, zabService* Service, zabAction Action)
{
  ER_CHECK_STATUS(Status);
  ER_CHECK_NULL(Status, Service);

  printInfo(Service, "zabCoreAction: %d = %s\n", (unsigned8)Action, zabUtility_GetZabActionString(Action));

  if (Action < ZAB_ACTION_MAX)
    {
#ifdef ZAB_CFG_ENABLE_EZ_MODE_NETWORK_STEERING
      zabCoreEzMode_ActionOutHandler(Status, Service, Action);
#endif

      sapMsgPutAction(Status, ZAB_SRV(Service)->ManageSAP, SAP_MSG_DIRECTION_OUT, Action);
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_ACTION_INVALID);
    }
}


/******************************************************************************
 * Get SAPs (Manage/Data/Serial)from an instance of ZAB
 ******************************************************************************/
sapHandle zabCoreSapSerial (zabService* Service)
{
    return ZAB_SRV(Service)->SerialSAP;
}
sapHandle zabCoreSapData (zabService* Service)
{
    return ZAB_SRV(Service)->DataSAP;
}
sapHandle zabCoreSapManage (zabService* Service)
{
  if (Service != NULL)
    {
      return ZAB_SRV(Service)->ManageSAP;
    }
  return NULL;
}
