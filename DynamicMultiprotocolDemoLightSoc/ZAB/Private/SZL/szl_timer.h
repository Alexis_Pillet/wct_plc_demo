#ifndef _SZL_TIMER_H_
#define _SZL_TIMER_H_

#include "szl.h"
#include "szl_zab_types.h"
#include "szl_timer_types.h"


void Szl_TimersInitialize(zabService* Service);

szl_uint8 Szl_ExtTimerRegister(zabService* Service, szl_bool repeatable, szl_uint16 seconds, SZL_CB_ExtTimer_t callback);
SZL_RESULT_t Szl_ExtTimerUnregister(zabService* Service, szl_uint8 ID);
SZL_RESULT_t Szl_ExtTimerStart(zabService* Service, szl_uint8 ID);
SZL_RESULT_t Szl_ExtTimerStartWithDuration(zabService* Service, szl_uint8 ID, szl_uint16 Duration);
SZL_RESULT_t Szl_ExtTimerStop(zabService* Service, szl_uint8 ID);
szl_uint16 Szl_ExtTimerRemaining(zabService* Service, szl_uint8 ID);
szl_uint16 Szl_ExtTimerNextExpiration(zabService* Service);
szl_bool Szl_ExtTimerIsRunning(zabService* Service, szl_uint8 ID);

// MvdB: Added return value for ARTF104110: Support next work time return from zabCoreWork
szl_uint32 Szl_ExtTimerProcess(zabService* Service);




void Szl_TimerAdvance(zabService* Service, szl_uint32 MillisecondsToAdvance);
szl_bool Szl_TimerIsActive(SZL_Timer_t* timer);
void Szl_TimerActivate(zabService* Service, SZL_Timer_t* timer, szl_uint16 interval);
void Szl_TimerDeactivate(SZL_Timer_t* timer);
szl_bool Szl_TimerExpired(zabService* Service, SZL_Timer_t* timer);
szl_uint32 Szl_TimerElapsed(zabService* Service, SZL_Timer_t* timer);


// Time functions
szl_uint32 Szl_TimeElapsed(zabService* Service, szl_uint32 startTime);
szl_uint32 Szl_TimeNow(zabService* Service);




#endif /*_SZL_TIMER_H_*/
