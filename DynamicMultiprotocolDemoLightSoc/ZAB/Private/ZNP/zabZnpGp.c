/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the Green Power functions for the ZNP.
 *   This is as developed for the GPZNP.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.06.00  28-Apr-14   MvdB   Original
 * 00.00.06_GP  15-Oct-14   MvdB   ARTF105853: Complete basic GP features for Smartlink IPZ
 * 01.00.00.02  29-Jan-15   MvdB   Support GPD De-commissioning
 * 002.000.003  06-Mar-15   MvdB   ARTF116061: Support run time configuration of Green Power endpoint.
 * 002.000.009  17-Apr-15   MvdB   ARTF131022: Remove SZL_GP_Initialize() and associated functions as it is now obsolete
 * 002.001.001  15-Jul-15   MvdB   ARTF130228: Add unique address mode for GpSrcId
 * 002.002.002  01-Sep-15   MvdB   ARTF147441: Support Key Mode in zabZnpGp_CommissioningReplyReq() and extended options in processGpCommissGpdfInd()
 * 002.002.030  09-Jan-17   MvdB   ARTF198434: Upgrade SZL_GP_GpdCommissioningNtfParams_t to include command and cluster lists
 *****************************************************************************/

#include "zabZnpService.h"
#include "szl_gp_types.h"


#ifdef ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* ME Extension Id for Schneider GP extensions */
#define SCHNEIDER_GP_EXT_ID 0x80

/* Max size of a GP param in bytes */
#define M_MAX_GP_PARAM_DATA_SIZE 2

#define ZNP_GP_M_SECURITY_KEY_LENGTH              ( 16 )
#define ZNP_GP_M_SECURITY_FRAME_COUNTER_LENGTH    ( 4 )

/* Flags in the GP Commissioning Options field */
#define ZNP_GP_M_COM_IND_OPT_MSN_CAP_FLAG         ( 0x01 )
#define ZNP_GP_M_COM_IND_OPT_RXON_CAP_FLAG        ( 0x02 )
#define ZNP_GP_M_COM_IND_OPT_APP_INFO_PRES_FLAG   ( 0x04 )
#define ZNP_GP_M_COM_IND_OPT_PAN_ID_REQ_FLAG      ( 0x10 )
#define ZNP_GP_M_COM_IND_OPT_SEC_KEY_REQ_FLAG     ( 0x20 )
#define ZNP_GP_M_COM_IND_OPT_FIXED_LOC_FLAG       ( 0x40 )
#define ZNP_GP_M_COM_IND_OPT_EXT_OPT_PRES_FLAG    ( 0x80 )

/* Flags in the GP Commissioning Extended Options field */
#define ZNP_GP_M_COM_IND_EXT_OPT_SEC_LVL_MASK           ( 0x03 )
#define ZNP_GP_M_COM_IND_EXT_OPT_KEY_TYPE_MASK          ( 0x1C )
#define ZNP_GP_M_COM_IND_EXT_OPT_KEY_TYPE_SHIFT         ( 2 )
#define ZNP_GP_M_COM_IND_EXT_OPT_GPD_KEY_PRES_FLAG      ( 0x20 )
#define ZNP_GP_M_COM_IND_EXT_OPT_GPD_KEY_ENCRYPTED_FLAG ( 0x40 )
#define ZNP_GP_M_COM_IND_EXT_OPT_OUT_FC_PRES_FLAG       ( 0x80 )

/* Flags in the GP Commissioning Application Info field */
#define ZNP_GP_M_COM_IND_APP_INFO_MAN_ID_PRES_FLAG      ( 0x01 )
#define ZNP_GP_M_COM_IND_APP_INFO_MOD_ID_PRES_FLAG      ( 0x02 )
#define ZNP_GP_M_COM_IND_APP_INFO_GP_CMDS_PRES_FLAG     ( 0x04 )
#define ZNP_GP_M_COM_IND_APP_INFO_CLUSTERS_PRES_FLAG    ( 0x08 )

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/* MT GP Commands */
typedef enum
{
  ZNP_CMD_GP_CLEAR_ALL             = 0x00,
  ZNP_CMD_GP_SET_PARAM             = 0x01,
  ZNP_CMD_GP_COMMISS_MODE          = 0x02,
  ZNP_CMD_GP_COMMISS_REPLY_REQ     = 0x03,

  ZNP_CMD_GP_COMMISS_GPDF_IND      = 0x80,
  ZNP_CMD_GP_COMMISS_FINAL_IND     = 0x81,
  ZNP_CMD_GP_EXPIRE_TXQ_IND        = 0x82,
} ZNP_CMD_GP;




/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 * Process status from an SRSP
 ******************************************************************************/
static void getGpStatus( erStatus* Status, sapMsg* Message, unsigned8* status )
{
  unsigned8* data;
  unsigned8 len;
  ER_CHECK_STATUS_NULL(Status, status);

  zabParseZNPGetData(Status, Message, &len, &data);
  ER_CHECK_STATUS(Status);

  if (len >= sizeof(unsigned8))
    {
      *status = *data;
    }
  else
    {
      erStatusSet( Status, ZAB_ERROR_VENDOR_PARSE);
    }
}

/******************************************************************************
 * Process incoming Gp Commissioning GPDF
 ******************************************************************************/
#define M_MIN_COM_GPDF_LENGTH 10    // IEEE(8) + DeviceIf(1) + Options(1)
static void processGpCommissGpdfInd(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  unsigned8 tid = 0;
  unsigned8 znpIndex;
  unsigned8 tempIndex;
  unsigned8 clusterIndex;
  SZL_GP_GpdCommissioningNtfParams_t* ind;
  unsigned8 frameLengthCheck;
  unsigned64 ieee;

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

  /* Check there is at least the minimum frame length in the data*/
  frameLengthCheck = M_MIN_COM_GPDF_LENGTH;
  if (znpDataLength >= frameLengthCheck)
    {
      // Allocate New Message
      length = SZL_GP_GpdCommissioningNtfParams_t_SIZE;
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if (dataMessage == NULL)
        {
          return;
        }

      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_GP_COM_IND);

      /* Extract from the ZNP command into the data sap structure */

      ind = (SZL_GP_GpdCommissioningNtfParams_t*)sapMsgGetAppData(dataMessage);

      znpIndex = 0;
      ieee = COPY_IN_64_BITS(&dp[znpIndex]); znpIndex += sizeof(unsigned64);

      /* Today only Source Id is supported */
      if ( (ieee & ZNP_M_GP_SRC_ID_IEEE_EXTENSION_MASK) == ZNP_M_GP_SRC_ID_IEEE_EXTENSION )
        {
          ind->Address.AddressMode = SZL_ADDRESS_MODE_GP_SOURCE_ID;
          ind->Address.Addresses.GpSourceId.SourceId = (unsigned32)ieee;
        }
      else
        {
          sapMsgFree(Status, dataMessage);
          return;
        }

      ind->DeviceId = dp[znpIndex++];

      /* Options field */
      if (dp[znpIndex] & ZNP_GP_M_COM_IND_OPT_MSN_CAP_FLAG)
        {
          ind->Options.MacSeqNumCap = 1;
        }
      if (dp[znpIndex] & ZNP_GP_M_COM_IND_OPT_RXON_CAP_FLAG)
        {
          ind->Options.RxOnCap = 1;
        }
      if (dp[znpIndex] & ZNP_GP_M_COM_IND_OPT_APP_INFO_PRES_FLAG)
        {
          ind->Options.AppInfoPresent = 1;
        }
      if (dp[znpIndex] & ZNP_GP_M_COM_IND_OPT_PAN_ID_REQ_FLAG)
        {
          ind->Options.PanIdRequest = 1;
        }
      if (dp[znpIndex] & ZNP_GP_M_COM_IND_OPT_SEC_KEY_REQ_FLAG)
        {
          ind->Options.GpSecurityKeyReq = 1;
        }
      if (dp[znpIndex] & ZNP_GP_M_COM_IND_OPT_FIXED_LOC_FLAG)
        {
          ind->Options.FixedLocation = 1;
        }
      if (dp[znpIndex] & ZNP_GP_M_COM_IND_OPT_EXT_OPT_PRES_FLAG)
        {
          ind->Options.ExtOptionsPresent = 1;
        }
      znpIndex++;

      /* Extended Options - If present */
      if (ind->Options.ExtOptionsPresent)
        {
          frameLengthCheck++;
          if (znpDataLength >= frameLengthCheck)
            {
              ind->ExtendedOptions.SecurityLevelCapabilities = (SZL_GP_SECURITY_LEVEL_CAP_t)(dp[znpIndex] & ZNP_GP_M_COM_IND_EXT_OPT_SEC_LVL_MASK);
              ind->ExtendedOptions.KeyType = (SZL_GP_SECURITY_KEY_TYPE_t)((dp[znpIndex] & ZNP_GP_M_COM_IND_EXT_OPT_KEY_TYPE_MASK) >> ZNP_GP_M_COM_IND_EXT_OPT_KEY_TYPE_SHIFT);

              if (dp[znpIndex] & ZNP_GP_M_COM_IND_EXT_OPT_GPD_KEY_PRES_FLAG)
                {
                  ind->ExtendedOptions.GpdKeyPresent = 1;
                }
              if (dp[znpIndex] & ZNP_GP_M_COM_IND_EXT_OPT_GPD_KEY_ENCRYPTED_FLAG)
                {
                  ind->ExtendedOptions.GpdKeyEncryption = 1;
                }
              if (dp[znpIndex] & ZNP_GP_M_COM_IND_EXT_OPT_OUT_FC_PRES_FLAG)
                {
                  ind->ExtendedOptions.GpdOutgoingCounterPresent = 1;
                }
              znpIndex++;
            }
          if (ind->ExtendedOptions.GpdKeyPresent)
            {
              frameLengthCheck+=ZNP_GP_M_SECURITY_KEY_LENGTH;
              if (znpDataLength >= frameLengthCheck)
                {
                  znpIndex+=ZNP_GP_M_SECURITY_KEY_LENGTH;

                  if (ind->ExtendedOptions.GpdKeyEncryption)
                    {
                      frameLengthCheck+=ZNP_GP_M_SECURITY_FRAME_COUNTER_LENGTH;
                      if (znpDataLength >= frameLengthCheck)
                        {
                          znpIndex+=ZNP_GP_M_SECURITY_FRAME_COUNTER_LENGTH;
                        }
                    }
                }
            }
          if (ind->ExtendedOptions.GpdOutgoingCounterPresent)
            {
              frameLengthCheck+=ZNP_GP_M_SECURITY_FRAME_COUNTER_LENGTH;
              if (znpDataLength >= frameLengthCheck)
                {
                  znpIndex+=ZNP_GP_M_SECURITY_FRAME_COUNTER_LENGTH;
                }
            }
        }

      /* Application Info - If present */
      if (ind->Options.AppInfoPresent)
        {
          frameLengthCheck++;
          if (znpDataLength >= frameLengthCheck)
            {
              if (dp[znpIndex] & ZNP_GP_M_COM_IND_APP_INFO_MAN_ID_PRES_FLAG)
                {
                  ind->AppInfo.ManufacturerIdPresent = 1;
                }
              if (dp[znpIndex] & ZNP_GP_M_COM_IND_APP_INFO_MOD_ID_PRES_FLAG)
                {
                  ind->AppInfo.ModelIdPresent = 1;
                }
              if (dp[znpIndex] & ZNP_GP_M_COM_IND_APP_INFO_GP_CMDS_PRES_FLAG)
                {
                  ind->AppInfo.GpdCommandsPresent = 1;
                }
              if (dp[znpIndex] & ZNP_GP_M_COM_IND_APP_INFO_CLUSTERS_PRES_FLAG)
                {
                  ind->AppInfo.ClusterListPresent = 1;
                }
              znpIndex++;
            }
        }

      ind->ManufacturerId = SZL_GP_COM_IND_ID_NOT_DEFINED;
      if (ind->AppInfo.ManufacturerIdPresent)
        {
          frameLengthCheck+=sizeof(unsigned16);
          if (znpDataLength >= frameLengthCheck)
            {
              ind->ManufacturerId = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += sizeof(unsigned16);
            }
        }

      ind->ManufacturerModelId = SZL_GP_COM_IND_ID_NOT_DEFINED;
      if (ind->AppInfo.ModelIdPresent)
        {
          frameLengthCheck+=sizeof(unsigned16);
          if (znpDataLength >= frameLengthCheck)
            {
              ind->ManufacturerModelId = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += sizeof(unsigned16);
            }
        }

      ind->GpdCommandIdListLength = 0;
      if (ind->AppInfo.GpdCommandsPresent)
        {
          /* Confirm we have a valid list length */
          frameLengthCheck++;
          if (znpDataLength >= frameLengthCheck)
            {
              /* Confirm we have a valid list of specified length */
              frameLengthCheck+=dp[znpIndex];
              if (znpDataLength >= frameLengthCheck)
                {
                  /* Get, length, store copy of real length list for modifying index, then limit to our max */
                  ind->GpdCommandIdListLength = dp[znpIndex++];
                  tempIndex = ind->GpdCommandIdListLength;
                  if (ind->GpdCommandIdListLength > SZL_GP_COM_IND_MAX_LIST_LENGTH)
                    {
                      ind->GpdCommandIdListLength = SZL_GP_COM_IND_MAX_LIST_LENGTH;
                    }
                  osMemCopy(Status, ind->GpdCommandIdList, &dp[znpIndex], ind->GpdCommandIdListLength);
                  znpIndex += tempIndex;
                }
            }
        }

      ind->ClientClusterListLength = 0;
      ind->ServerClusterListLength = 0;
      if (ind->AppInfo.ClusterListPresent)
        {
          /* Confirm we have a valid list length */
          frameLengthCheck++;
          if (znpDataLength >= frameLengthCheck)
            {
              /* Confirm we have a valid list of specified length */
              tempIndex = (dp[znpIndex] & 0x0F) + ((dp[znpIndex] >> 4) & 0x0F);
              frameLengthCheck+= (tempIndex * sizeof(unsigned16));
              if (znpDataLength >= frameLengthCheck)
                {
                  /* Get list lengths */
                  ind->ServerClusterListLength = dp[znpIndex] & 0x0F;
                  ind->ClientClusterListLength = (dp[znpIndex] >> 4) & 0x0F;
                  znpIndex++;

                  /* Limit lengths to our maximums. Save real length of server list in case it is longer than we can support */
                  tempIndex = ind->ServerClusterListLength;
                  if (ind->ServerClusterListLength > SZL_GP_COM_IND_MAX_LIST_LENGTH)
                    {
                      ind->ServerClusterListLength = SZL_GP_COM_IND_MAX_LIST_LENGTH;
                    }
                  if (ind->ClientClusterListLength > SZL_GP_COM_IND_MAX_LIST_LENGTH)
                    {
                      ind->ClientClusterListLength = SZL_GP_COM_IND_MAX_LIST_LENGTH;
                    }

                  /* Load out server clusters, handle any server cluster that exist in the command but do not fit in our structure, then get client clusters */
                  for (clusterIndex = 0; clusterIndex < ind->ServerClusterListLength; clusterIndex++)
                    {
                      ind->ServerClusterList[clusterIndex] = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += sizeof(unsigned16);
                    }
                  if (tempIndex > ind->ServerClusterListLength)
                    {
                      znpIndex += (tempIndex - ind->ServerClusterListLength) * sizeof(unsigned16);
                    }
                  for (clusterIndex = 0; clusterIndex < ind->ClientClusterListLength; clusterIndex++)
                    {
                      ind->ClientClusterList[clusterIndex] = COPY_IN_16_BITS(&dp[znpIndex]); znpIndex += sizeof(unsigned16);
                    }
                }
            }
        }

      sapMsgSetAppTransactionId(Status, dataMessage, tid);
      sapMsgSetAppDataLength(Status, dataMessage,  length);
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process incoming Gp Commissioning Final Indication
 * This happen when a GPD is commissioned, or (for now at least) with status
 * of fail when the commissioning window times out.
 ******************************************************************************/
static void processGpCommissFinalInd(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  unsigned8 tid = 0;
  SZL_GP_GpdCommissionedNtfParams_t* ind;
  unsigned64 ieee;

  ER_CHECK_STATUS_NULL(Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);
  ER_CHECK_NULL(Status, dp);

   /* Validate length before using data */
  if (znpDataLength >= 10)
    {
      if ( (dp[0] == ZNPI_API_ERR_SUCCESS) ||
           (dp[0] == ZNPI_API_ERR_NWK_LEAVE_UNCONFIRMED) )
        {
          // Allocate New Message
          length = SZL_GP_GpdCommissionedNtfParams_t_SIZE;
          dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
          if (dataMessage == NULL)
            {
              return;
            }

          sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_GP_COM_NTF_IND);

          /* Extract from the ZNP command into the data sap structure */

          ind = (SZL_GP_GpdCommissionedNtfParams_t*)sapMsgGetAppData(dataMessage);

          if (dp[0] == ZNPI_API_ERR_SUCCESS)
            {
              ind->Event = ZGP_COMMISSIONED_EVENT_COMMISSIONED;
            }
          else
            {
              ind->Event = ZGP_COMMISSIONED_EVENT_DECOMMISSIONED;
            }

          ieee = COPY_IN_64_BITS(&dp[1]);
          /* Today only Source Id is supported */
          if ( (ieee & ZNP_M_GP_SRC_ID_IEEE_EXTENSION_MASK) == ZNP_M_GP_SRC_ID_IEEE_EXTENSION )
            {
              ind->Address.AddressMode = SZL_ADDRESS_MODE_GP_SOURCE_ID;
              ind->Address.Addresses.GpSourceId.SourceId = (unsigned32)ieee;
            }
          else
            {
              sapMsgFree(Status, dataMessage);
              return;
            }

          ind->DeviceId = dp[9];

          sapMsgSetAppTransactionId(Status, dataMessage, tid);
          sapMsgSetAppDataLength(Status, dataMessage,  length);
          sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
        }
      else
        {
          printVendor(Service, "zabZnpGp: CommFinalInd: Sts = 0x%02X, GpdId = 0x%016llX, DeviceId = 0x%02X\n",
                   dp[0],
                   COPY_IN_64_BITS(&dp[1]),
                   dp[9]);
        }
    }
}


/******************************************************************************
 * Report a GP Tx Queue Expiry to the higher layers
 *
 * Note: This function deliberately does not use a status parameter, as it
 * is typically called upon detection of bad status, so using that status will
 * cause the error report to fail!
 * Error reports are "best effort" only. If this function fails then there
 * is no status returned and hence there will never be the opportunity to retry.
 ******************************************************************************/
static void gpTxQueueExpiry(zabService *Service, unsigned64 GpdId)
{
  erStatus errorStatus;
  sapMsg* Msg = NULL;

  /* For now we only work for Source Ids */
  if ( (GpdId & ZNP_M_GP_SRC_ID_IEEE_EXTENSION_MASK) == ZNP_M_GP_SRC_ID_IEEE_EXTENSION )
  {
      erStatusClear(&errorStatus, Service);

      Msg = sapMsgAllocateData( &errorStatus, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, sizeof(unsigned32));
      ER_CHECK_STATUS_NULL( &errorStatus, Msg );

      sapMsgSetAppType(&errorStatus, Msg, ZAB_MSG_APP_GP_TX_QUEUE_EXPIRY);
      sapMsgSetAppTransactionId(&errorStatus, Msg, 0x00);
      sapMsgCopyAppDataTo(&errorStatus, Msg, (unsigned8*)&GpdId, sizeof(unsigned32));
      sapMsgPutFree(&errorStatus, zabCoreSapData(Service), Msg);
  }
}

/******************************************************************************
 * Notify the host application about dropping of GPDF in the gpTxQueue due to
 * expiration of its gpTxQueueEntryLifetime
 ******************************************************************************/
static void processGpTxQueueExpireInd(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  unsigned8* dp;
  unsigned8 znpDataLength;
  unsigned64 GpdId;

  ER_CHECK_STATUS_NULL(Status, vendorMessage);
  dp = ZAB_MSG_ZNP_DATA(vendorMessage);
  ER_CHECK_NULL(Status, dp);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);

   /* Validate length before using data */
  if (znpDataLength >= 8)
    {
      GpdId = COPY_IN_64_BITS(dp);
      gpTxQueueExpiry(Service, GpdId);
    }
}


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Clear all GP-related tables on the ZigBee network device, including the
 * Sink Table and the Translation Table
 ******************************************************************************/
void zabZnpGp_ClearAll(erStatus* Status, zabService* Service)
{
  unsigned16 dataLength = 0;
  sapMsg* Msg = NULL;

  /* Validate the vendor is open */
  zabZnpService_GetLocalCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, dataLength);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, (unsigned8)dataLength);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_GP);
  zabParseZNPSetCommandID(Status, Msg, ZNP_CMD_GP_CLEAR_ALL);

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Set a GP-related parameter on the ZigBee stack that is not accessible through
 * a specified GPEP attribute, e.g. Pre-commissioned group ID
 ******************************************************************************/
void zabZnpGp_SetParameter(erStatus* Status, zabService* Service, ZNP_GP_PARAM_ID ParamId, void* Data)
{
  unsigned16 dataLength = 1;
  sapMsg* Msg = NULL;
  unsigned8* leData;

  /* Validate the vendor is open */
  zabZnpService_GetLocalCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, dataLength+M_MAX_GP_PARAM_DATA_SIZE);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_GP);
  zabParseZNPSetCommandID(Status, Msg, ZNP_CMD_GP_SET_PARAM);

  /* Load little endian data*/
  leData = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  *leData++ = (unsigned8)ParamId;

  switch (ParamId)
    {
      /* Unsigned8 */
      case ZNP_GP_PARAM_ID_LOOPBACK_DST_EP:
      case ZNP_GP_PARAM_ID_TX_Q_EXPIRY_IND:
        dataLength += sizeof(unsigned8);
        *leData = *(unsigned8*)Data;
        break;

      /* Unsigned16 */
      case ZNP_GP_PARAM_ID_PRE_COM_GROUP_ID:
      case ZNP_GP_PARAM_ID_TX_Q_ENTRY_LIFETIME:
        dataLength += sizeof(unsigned16);
        COPY_OUT_16_BITS(leData, *(unsigned16*)Data);
        break;

    default:
        erStatusSet(Status, ZAB_ERROR_VENDOR_REQ_INVALID);
        break;
    }

  /* Set data length with real actual paylaod length */
  zabParseZNPSetLength(Status, Msg, (unsigned8)dataLength);

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Instruct the GPS to enter or exit commissioning mode
 *  - Normal Mode = Stack will accept any GPD when commissioning window open
 *  - Restrictive = Stack will callback to host t confirm acceptance of GPD
 ******************************************************************************/
void zabZnpGp_SetCommissioningMode(erStatus* Status, zabService* Service, ZNP_GP_COMMISSIONING_MODE CommissioningMode, unsigned8 CommissioningModeExit, unsigned16 CommissioningWindow)
{
  unsigned16 dataLength;
  sapMsg* Msg = NULL;
  unsigned8* leData;

  /* Validate the vendor is open */
  zabZnpService_GetLocalCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Use the extended command only when entering commissioning mode. We don't want to mess up the attributes */
  switch (CommissioningMode)
    {
      case ZNP_GP_COMMISSIONING_MODE_NORMAL:
      case ZNP_GP_COMMISSIONING_MODE_RESTRICTIVE:
      case ZNP_GP_COMMISSIONING_MODE_NORMAL_UNICAST:
      case ZNP_GP_COMMISSIONING_MODE_RESTRICTIVE_UNICAST:
        dataLength = 5;
        break;

      default:
        dataLength = 1;
        break;
    }


  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, dataLength);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, (unsigned8)dataLength);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_GP);
  zabParseZNPSetCommandID(Status, Msg, ZNP_CMD_GP_COMMISS_MODE);

  /* Load little endian data*/
  leData = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  *leData++ = (unsigned8)CommissioningMode;

  /* Load Schneider Extension */
  *leData++ = SCHNEIDER_GP_EXT_ID;
  *leData++ = CommissioningModeExit;
  COPY_OUT_16_BITS(leData, CommissioningWindow);


  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Instruct the GPS to inject a Commissioning Reply command to the
 * TempMaster’s gpTxQueue, while in restrictive commissioning mode
 ******************************************************************************/
void zabZnpGp_CommissioningReplyReq(erStatus* Status, zabService* Service, unsigned64 GpdId, unsigned8 Options, unsigned8 KeyMode)
{
  unsigned16 dataLength = 10;
  sapMsg* Msg = NULL;
  unsigned8* leData;

  /* Validate the vendor is open */
  zabZnpService_GetLocalCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  /* Allocate Msg ZNP */
  Msg = zabParseZNPCreateRequest(Status, Service, dataLength);
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Set Header ZNP */
  zabParseZNPSetLength(Status, Msg, (unsigned8)dataLength);
  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_GP);
  zabParseZNPSetCommandID(Status, Msg, ZNP_CMD_GP_COMMISS_REPLY_REQ);

  /* Load little endian data*/
  leData = (unsigned8*)ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_64_BITS(leData, GpdId); leData+=8;
  *leData++ = Options;
  *leData++ = (KeyMode & 0x03); // Take care to make sure bits 2-7 are reserved and zero

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * GP Commissioning Reply Request Conversion
 * Convert from ZAB format to vendor format
 ******************************************************************************/
void zabZnpGp_CommissioningReplyReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned16 length;
  SZL_GpComReplyReqParams_t* req;
  unsigned8 options;
  unsigned64 ieee;

  /* Confirm data length is correct before using it */
  length = sapMsgGetAppDataLength(Message);
  ER_CHECK_EQUAL(Status, length, SZL_GpComReplyReqParams_t_SIZE);

  req = (SZL_GpComReplyReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, req);

  /* Convert from SZL address mode into ZNP IEEE address */
  if (req->Address.AddressMode == SZL_ADDRESS_MODE_GP_SOURCE_ID)
    {
      ieee = ZNP_M_GP_SRC_ID_IEEE_EXTENSION | req->Address.Addresses.GpSourceId.SourceId;
    }
  else
    {
      return;
    }

  /* Convert options from struct into unsigned8 */
  options = 0;
  if (req->Options.MacSeqNumCap)
    {
      options |= 0x01;
    }
  if (req->Options.RxOnCap)
    {
      options |= 0x02;
    }
  if (req->Options.AppInfoPresent)
    {
      options |= 0x04;
    }
  if (req->Options.PanIdRequest)
    {
      options |= 0x10;
    }
  if (req->Options.GpSecurityKeyReq)
    {
      options |= 0x20;
    }
  if (req->Options.FixedLocation)
    {
      options |= 0x40;
    }
  if (req->Options.ExtOptionsPresent)
    {
      options |= 0x80;
    }

  zabZnpGp_CommissioningReplyReq(Status, Service,
                                 ieee,
                                 options,
                                 (unsigned8)req->KeyMode);
}

/******************************************************************************
 * Process incoming GP messages
 ******************************************************************************/
void zabZnpGp_ProcessIncomingMessage(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8 test;
  ZNP_CMD_GP cmd;
  unsigned8 result;

  zabParseZNPGetCommandID(Status, Message, &test);
  cmd = (ZNP_CMD_GP)test;

  switch (cmd)
    {
      /* SRSP's */

      case ZNP_CMD_GP_SET_PARAM:
        getGpStatus(Status, Message, &result);
        zabZnpNwk_gpSetParamRspHandler(Status, Service, (ZNPI_API_ERROR_CODES)result);
        break;

      case ZNP_CMD_GP_CLEAR_ALL:
      case ZNP_CMD_GP_COMMISS_MODE:
      case ZNP_CMD_GP_COMMISS_REPLY_REQ:
        getGpStatus(Status, Message, &result);
        if ((ZNPI_API_ERROR_CODES)result != ZNPI_API_ERR_SUCCESS)
          {
            printError(Service, "ZNP GP: ERROR! CMD: 0x%02X, SRSP: Status = 0x%02X\n", (unsigned8)cmd, result);
          }
        else
          {
            printVendor(Service, "ZNP GP: SRSP: Status = 0x%02X\n", result);
          }
        break;

      /* Indications */
      case ZNP_CMD_GP_COMMISS_GPDF_IND:
        processGpCommissGpdfInd(Status, Service, Message);
        break;

      case ZNP_CMD_GP_COMMISS_FINAL_IND:
        processGpCommissFinalInd(Status, Service, Message);
        break;

      case ZNP_CMD_GP_EXPIRE_TXQ_IND:
        processGpTxQueueExpireInd(Status, Service, Message);
        break;

      /* Unhandled */
      default:
        printError(Service, "<<ATTENTION>> Unhandled ZNP message for GP Subsystem [0x%02X]\n", (unsigned8)cmd);
        break;
    }
}


#endif // ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
