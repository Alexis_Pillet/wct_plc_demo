/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/******************************************************************************
* File Name     : CCRXmachine.h
* Device(s)     : RX631
* Tool-chain    : KPIT GNURX-ELF 15.01
* H/W platform  : G-CPX / EU-CPX2 / G-CPX3
* Description   : Sample software
******************************************************************************/

#ifndef __CCRX_MACHINE_H__
#define __CCRX_MACHINE_H__

/***********************************************************************
* Function definitions
***********************************************************************/
signed long max(signed long data1, signed long data2);
signed long min(signed long data1, signed long data2);
void xchg(signed long *data1, signed long *data2);
long long rmpab(long long init, unsigned long count, signed char *addr1, signed char *addr2);
long long rmpaw(long long init, unsigned long count, short *addr1, short *addr2);
long long rmpal(long long init, unsigned long count, long *addr1, long *addr2);
unsigned long rolc(unsigned long data);
unsigned long rorc(unsigned long data);
unsigned long rotl(unsigned long data, unsigned long num);
unsigned long rotr(unsigned long data, unsigned long num);
void set_ipl(signed long level);
unsigned char get_ipl(void);
signed long long emul(signed long data1, signed long data2);
unsigned long long emulu(unsigned long data1, unsigned long data2);
void chg_pmusr(void);
void set_acc(signed long long data);
signed long long get_acc(void);
void setpsw_i(void);
void clrpsw_i(void);
long macl(short* data1, short* data2, unsigned long count);
short macw1(short* data1, short* data2, unsigned long count);
short macw2(short* data1, short* data2, unsigned long count);
void _CALL_INIT(void);
void _CALL_END(void);
void _INITSCT(void);
void nop(void);

#endif //__CCRX_MACHINE_H__
