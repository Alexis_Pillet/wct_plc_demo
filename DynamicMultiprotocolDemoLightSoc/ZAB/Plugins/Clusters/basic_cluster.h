/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the Basic Cluster.
 *   This plugin is originally designed for Nova (partner).
 * 
 *   It supports:
 *    - Multiple, application specified endpoints
 *    - Standard attributes
 * 
 *   It does not (currently) support:
 *    - Non-volatile backing of any data.
 *
 * CONFIGURATION:
 *   BASIC_M_USE_DIRECT_ACCESS:
 *    - Define to have plugin malloc data storage for attributes and access the data directly.
 *    - Undefine to have plugin use callbacks to request attribute read/write via callbacks
 *
 * USAGE:
 *   Usage of this plugin is very simple:
 *   If using BASIC_M_USE_DIRECT_ACCESS:
 *    1. Initialise it with SzlPlugin_BasicClusterInit(), specify the endpoints and a write notification callback
 *    2. Use SzlPlugin_BasicCluster_GetAttributePointer() to get access to the data for each endpoint.
 *    3. Populate the data
 *    4. Call SzlPlugin_BasicCluster_ConfigureReporting() to start attribute reporting
 *    5. Update data as it changes, the plugin will do the rest.
 *   If not using BASIC_M_USE_DIRECT_ACCESS:
 *    1. Initialise it with SzlPlugin_BasicClusterInit(), specify the endpoints and read/write attribute callbacks
 *    2. Call SzlPlugin_BasicCluster_ConfigureReporting() to start attribute reporting
 * 
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         20-Feb-14   MvdB   Original
 *    2         19-May-14   MvdB   Remove endpoint number from public attribute data
 *    3         17-Sep-14   MvdB   #56501: Add generation of Node Communications Status notification
 *    4         29-Oct-14   MvdB   artf74202: Support use of callbacks instead of direct access
 *    5         27-Nov-14   MvdB   Update URL length to match latest SZCL
 *    6         19-Feb-15   MvdB   Remove obsoleted attributes: ATTRID_BASIC_DEVICE_NAME_STRING
 * 002.001.006  27-Jul-15   MvdB   ARTF134686: Standardise on SZL_RESULT_t for plugin init return value
 *****************************************************************************/
#ifndef _BASIC_CLUSTER_H_
#define _BASIC_CLUSTER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "zabTypes.h"
#include "szl.h"

  
/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/

/* If defined, the plugin will use Direct Access to attribute data. This means:
 *  - Plugin will malloc space to store all attribute data for the cluster.
 *  - Plugin will directly access this data via pointers.
 *  - Application can get a pointer to the structure, then set values in it.
 *  - If the application is not single threaded it must handle mutexing before accessing the data.
 * If not defined, the plugin will use callbacks to access attribute data. This means:
 *  - The plugin will read/write data by calling an application supplied callback.
 *  - The application can then manage all data access itself.
 */
//#define BASIC_M_USE_DIRECT_ACCESS

/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Attributes Identifiers */
typedef enum
{
  ATTRID_BASIC_ZCL_VERSION                        = 0x0000,
  ATTRID_BASIC_MANUFACTURER_NAME                  = 0x0004,
  ATTRID_BASIC_MODEL_IDENTIFIER                   = 0x0005,
  ATTRID_BASIC_DATE_CODE                          = 0x0006,
  ATTRID_BASIC_POWER_SOURCE                       = 0x0007,

  /* =S= MS Attributes */
  ATTRID_BASIC_NETWORK_PROCESSOR_FIRMWARE_VERSION = 0xE000,
  ATTRID_BASIC_APPLICATION_FIRMWARE_VERSION       = 0xE001,
  ATTRID_BASIC_APPLICATION_HARDWARE_VERSION       = 0xE002,
  ATTRID_BASIC_PRODUCT_SERIAL_NUMBER              = 0xE004,
  ATTRID_BASIC_NETWORK_PROCESSOR_HARDWARE_VERSION = 0xE006,
  ATTRID_BASIC_PRODUCT_IDENTIFIER                 = 0xE007,
  ATTRID_BASIC_PRODUCT_RANGE                      = 0xE008,
  ATTRID_BASIC_PRODUCT_MODEL                      = 0xE009,
  ATTRID_BASIC_PRODUCT_FAMILY                     = 0xE00A,
  ATTRID_BASIC_VENDOR_URL                         = 0xE00B,
  ATTRID_BASIC_PRODUCT_CAPABILITY_1               = 0xE040,
  ATTRID_BASIC_SYSTEM_MODULE_NUMBER               = 0xE100,
} BASIC_CLUSTER_ATTRIBUTES;
  
/* Length limits for strings. ZAB will allocate an extra byte for the terminator */
#define BASIC_M_MANUFACTURER_NAME_LENGTH                  32
#define BASIC_M_MODEL_IDENTIFIER_LENGTH                   32
#define BASIC_M_DATE_CODE_LENGTH                          16
#define BASIC_M_DEVICE_NAME_LENGTH                        16
#define BASIC_M_NETWORK_PROCESSOR_FIRMWARE_VERSION_LENGTH 20
#define BASIC_M_APPLICATION_FIRMWARE_VERSION_LENGTH       20
#define BASIC_M_APPLICATION_HARDWARE_VERSION_LENGTH       20
#define BASIC_M_PRODUCT_SERIAL_NUMBER_LENGTH              16
#define BASIC_M_NETWORK_PROCESSOR_HARDWARE_VERSION_LENGTH 20
#define BASIC_M_PRODUCT_RANGE_LENGTH                      16
#define BASIC_M_PRODUCT_MODEL_LENGTH                      16
#define BASIC_M_PRODUCT_FAMILY_LENGTH                     16
#define BASIC_M_VENDOR_URL_LENGTH                         65
#define BASIC_M_PRODUCT_CAPABILITY_1_LENGTH               20


/* Attribute Data storage
 * An instance of this is created per endpoint.
 * Applications may access it via SzlPlugin_BasicCluster_GetAttributePointer() 
 * All strings are allocated an extra byte for a zero terminator.*/
typedef struct
{
  /* ZigBee Standard Attributes */
  szl_uint8 ZclVersion;
  char ManufacturerName[BASIC_M_MANUFACTURER_NAME_LENGTH+1];
  char ModelIdentifier[BASIC_M_MODEL_IDENTIFIER_LENGTH+1];
  char DateCode[BASIC_M_DATE_CODE_LENGTH+1];
  szl_uint8 PowerSource;
  
  /* =S= MS Attributes */
  char DeviceName[BASIC_M_DEVICE_NAME_LENGTH+1];
  char NetworkProcessorFirmwareVersion[BASIC_M_NETWORK_PROCESSOR_FIRMWARE_VERSION_LENGTH+1];
  char ApplicationFirmwareVersion[BASIC_M_APPLICATION_FIRMWARE_VERSION_LENGTH+1];
  char ApplicationHardwareVersion[BASIC_M_APPLICATION_HARDWARE_VERSION_LENGTH+1];
  char ProductSerialNumber[BASIC_M_PRODUCT_SERIAL_NUMBER_LENGTH+1];
  char NetworkProcessorHardwareVersion[BASIC_M_NETWORK_PROCESSOR_HARDWARE_VERSION_LENGTH+1];
  szl_uint16 ProductIdentifier;
  char ProductRange[BASIC_M_PRODUCT_RANGE_LENGTH+1];
  char ProductModel[BASIC_M_PRODUCT_MODEL_LENGTH+1];
  char ProductFamily[BASIC_M_PRODUCT_FAMILY_LENGTH+1];
  char VendorUrl[BASIC_M_VENDOR_URL_LENGTH+1];
  char ProductCapability1[BASIC_M_PRODUCT_CAPABILITY_1_LENGTH+1];
  szl_uint8 SystemModuleNumber;
}SzlPlugin_Basic_Attributes_t;


/* Node Communication Status Enumeration - See SZCL for details */
typedef enum
{
  BASIC_NODE_COMMUNICATION_STATUS_DISABLED     = 0x00,
  BASIC_NODE_COMMUNICATION_STATUS_ENABLED      = 0x01,
} ENUM_BASIC_NODE_COMMUNICATION_STATUS_t;

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Plugin Initialisation
 * This function must be called from the application after the Library has been initialized.
 * 
 * Parameters:
 *  Service: Instance of library
 *  numEndpoints: The length of endpointList
 *  endpointList: The endpoints to be initialised with this cluster
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_BasicClusterInit(zabService* Service, 
                                        szl_uint8 numEndpoints, 
                                        szl_uint8 * endpointList
#ifndef BASIC_M_USE_DIRECT_ACCESS
                                        ,
                                        SZL_CB_DataPointRead_t ReadCallback,
                                        SZL_CB_DataPointWrite_t WriteCallback
#endif
                                        );

/******************************************************************************
 * Plugin Destruction
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_BasicCluster_Destroy(zabService* Service);

/******************************************************************************
 * Get pointer to Basic Cluster Attributes for an endpoint
 * This can be used to get access to the endpoints parameters for the local 
 *  application to read or write them
 * Return NULL if not found
 ******************************************************************************/
#ifdef BASIC_M_USE_DIRECT_ACCESS
extern 
SzlPlugin_Basic_Attributes_t* SzlPlugin_BasicCluster_GetAttributePointer(zabService* Service, szl_uint8 endpoint);
#endif

/******************************************************************************
 * Broadcast a Schneider Manufacturer Specific Node Communication Notification command
 * See the Schneider ZCL for details.
 ******************************************************************************/
extern 
SZL_RESULT_t SzlPlugin_BasicCluster_SendNodeCommunicationNotification(zabService* Service, 
                                                                      szl_uint8 endpoint, 
                                                                      ENUM_BASIC_NODE_COMMUNICATION_STATUS_t NodeCommsStatus);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /*_BASIC_CLUSTER_H_*/
