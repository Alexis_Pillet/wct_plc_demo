/*******************************************************************************
  Filename    : cb_handler.c
  $Date       : 2013-07-08                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : This is the callback handler functions for the SZL

 * MODIFICATION HISTORY:
 *  Core Rev      Date     Author  Change Description
 * 002.001.004  21-Jul-15   MvdB   ARTF134686: Use Szl_InternalIsLibInitialized() before allowing registration
*******************************************************************************/

/**************************************************************************************************
* INCLUDES
**************************************************************************************************/
#include <stdio.h>
#include <string.h>
#include "szl_types.h"
#include "szl.h"
#include "szl_internal.h"
#include "sort.h"
#include "zabCorePrivate.h"
#include "szl_zab_types.h"

#include "cb_handler.h"

/**************************************************************************************************
* DEFINES
**************************************************************************************************/
/*
 * Global defines
 */
#define CB_HANDLERS_SIZE SZL_CFG_CB_HANDLER_SIZE


/*
 * Application data defines
 */

#define ZAB_SZL_CB( s ) (ZAB_SRV( s )->szlService.CB)
#define ZAB_SZL_CB_HANDLER_TABLE_WORK_REQUIRED( s ) (ZAB_SRV( s )->szlService.CB.cdHandlerTableWorkRequired)
#define ZAB_SZL_CB_HANDLER_TABLE( s ) (ZAB_SRV( s )->szlService.CB.cbHandlerTable)
#define ZAB_SZL_CB_HANDLER_TABLE_SIZE (sizeof(CB_HANDLER_ENTRY_t) * CB_HANDLERS_SIZE)

/**************************************************************************************************
* ENUMs, STRUCTs & UNIONs
**************************************************************************************************/

/**************************************************************************************************
* PROTOS
**************************************************************************************************/

/**************************************************************************************************
* VARIABLES
**************************************************************************************************/

/**************************************************************************************************
* FUNCTIONS
**************************************************************************************************/


/* Set work required */
static void Szl_LT_CBMaintenance(zabService* Service)
{
  // Move this into a set of flags if we start getting lots of them!
  ZAB_SZL_CB_HANDLER_TABLE_WORK_REQUIRED(Service) = szl_true;
}

/**
 * @brief   Initializes the callback queue
 *
 * @param   Service - type of service
 *
 * @return  N/A
 */
void CbHandler_Initialize(zabService* Service)
{
  szl_memset(&ZAB_SZL_CB(Service).cbHandlerTable, 0xFF, ZAB_SZL_CB_HANDLER_TABLE_SIZE);
  ZAB_SZL_CB(Service).cdHandlerTableWorkRequired = szl_false;
}

/**
 * @brief   This function compares the keys of two data points
 *
 * @param   a - data point to be compared
 * @param   b - data point to be compared
 *
 * @return   -1 a < b
 *            0 a = b
 *           +1 a > b
 */
int CbHandler_Compare(const void* a, const void* b)
{
    CB_HANDLER_ENTRY_t* da = (CB_HANDLER_ENTRY_t*) a;
    CB_HANDLER_ENTRY_t* db = (CB_HANDLER_ENTRY_t*) b;

/*    if (da->cbFunction == db->cbFunction)
        return ((szl_uint32)da->callback > (szl_uint32)db->callback) - ((szl_uint32)da->callback < (szl_uint32)db->callback);*/

    return (da->cbFunction > db->cbFunction) - (da->cbFunction < db->cbFunction);
}

/**
 * @brief   Adds a specific callback for a specific function
 *
 * @param   Service - Type of service
 * @param   func    - function used in the callback
 * @param   cb      - callback
 *
 * @return   True, if the function is added for the callback
 */
SZL_RESULT_t CbHandler_Add(zabService* Service, CB_FUNCTION_t func, CBQueue_CallbackProto cb)
{
    SZL_RESULT_t result = Szl_InternalIsLibInitialized(Service);
    if (result == SZL_RESULT_SUCCESS)
    {
        int i = 0;

        while (i < CB_HANDLERS_SIZE &&
               !(ZAB_SZL_CB_HANDLER_TABLE(Service)[i].cbFunction == CB_FUNC_EMPTY ||
                 ZAB_SZL_CB_HANDLER_TABLE(Service)[i].cbFunction == CB_FUNC_REMOVED) )
        {
            if (ZAB_SZL_CB_HANDLER_TABLE(Service)[i].cbFunction == func &&
                ZAB_SZL_CB_HANDLER_TABLE(Service)[i].callback == cb )
            {
                return SZL_RESULT_ALREADY_EXISTS;
            }
            i++;
        }

        if (i >= CB_HANDLERS_SIZE)
        {
            return SZL_RESULT_TABLE_FULL;
        }

        ZAB_SZL_CB_HANDLER_TABLE(Service)[i].cbFunction = func;
        ZAB_SZL_CB_HANDLER_TABLE(Service)[i].callback = cb;

        Szl_LT_CBMaintenance(Service);

        return SZL_RESULT_SUCCESS;
    }
    return result;
}

/**
 * @brief   Removes a specific callback for a specific function
 *
 * @param   Service - Type of service
 * @param   func    - function used in the callback
 * @param   cb      - callback
 *
 * @return   True, if the function is successfully removed
 */
SZL_RESULT_t CbHandler_Remove(zabService* Service, CB_FUNCTION_t func, CBQueue_CallbackProto cb)
{
    SZL_RESULT_t result = Szl_InternalIsLibInitialized(Service);
    if (result == SZL_RESULT_SUCCESS)
    {
        int i = 0;

        while (i < CB_HANDLERS_SIZE)
        {
            if (ZAB_SZL_CB_HANDLER_TABLE(Service)[i].cbFunction == CB_FUNC_EMPTY)
            {
                return SZL_RESULT_NOT_FOUND;
            }

            if (ZAB_SZL_CB_HANDLER_TABLE(Service)[i].cbFunction == func &&
                ZAB_SZL_CB_HANDLER_TABLE(Service)[i].callback == cb )
            {
                break;
            }
            i++;
        }

        if (i >= CB_HANDLERS_SIZE)
        {
            return SZL_RESULT_NOT_FOUND;
        }

        szl_memset(&ZAB_SZL_CB_HANDLER_TABLE(Service)[i], 0xFF, sizeof( CB_HANDLER_ENTRY_t ));
        ZAB_SZL_CB_HANDLER_TABLE(Service)[i].cbFunction = CB_FUNC_REMOVED;
        Szl_LT_CBMaintenance(Service);

        return SZL_RESULT_SUCCESS;
    }
    return result;
}

/**
 * @brief   Find the next callback registered for this function
 *          If startIdx Is NULL, search will start from beginning of list
 * If startIdx is provided it should be initialized to -1 and will
 * be self maintained for multiple calls
 *
 * @param   Service   - Type of service
 * @param   startIdx  - Index to start the search
 * @param   func      - function to be searched
 *
 * @return   the searched callback function if found, otherwise NULL
 */
CBQueue_CallbackProto CbHandler_Find(zabService* Service, int* startIdx, CB_FUNCTION_t func)
{
    int i = -1;

    if (startIdx != NULL)
        i = *startIdx;

    while (++i < CB_HANDLERS_SIZE)
    {
        if (ZAB_SZL_CB_HANDLER_TABLE(Service)[i].cbFunction == CB_FUNC_EMPTY)
            return NULL;

        if (ZAB_SZL_CB_HANDLER_TABLE(Service)[i].cbFunction == func)
        {
            if (startIdx != NULL)
                *startIdx = i;

            return ZAB_SZL_CB_HANDLER_TABLE(Service)[i].callback;
        }
    }

    return NULL;
}

/**
 * @brief   Check if atleast one callback has been registered for this function
 *
 * @param   Service   - Type of service
 * @param   func      - function to be searched
 *
 * @return   True if found otherwise False
 */
szl_bool CbHandler_HasCallback(zabService* Service, CB_FUNCTION_t func)
{
    int i;

	for (i = 0; i < CB_HANDLERS_SIZE; i++)
    {
        if (ZAB_SZL_CB_HANDLER_TABLE(Service)[i].cbFunction == func &&
            ZAB_SZL_CB_HANDLER_TABLE(Service)[i].callback != NULL)
            return szl_true;
    }

    return szl_false;
}

/**
 * @brief   This function is called from the mainloop so we dont risk
 *          sorting the table while we are using it!
 *
 * @param   Service   - Type of service
 *
 * @return   N/A
 */
void CbHandler_Maintenance(zabService* Service)
{
    int i;
  
  if (ZAB_SZL_CB_HANDLER_TABLE_WORK_REQUIRED(Service) != szl_false)
    {
    for (i=0; i < CB_HANDLERS_SIZE; i++)
    {
        if (ZAB_SZL_CB_HANDLER_TABLE(Service)[i].cbFunction == CB_FUNC_REMOVED)
            ZAB_SZL_CB_HANDLER_TABLE(Service)[i].cbFunction = CB_FUNC_EMPTY;
    }

    bubble_sort(ZAB_SZL_CB_HANDLER_TABLE(Service), CB_HANDLERS_SIZE, sizeof( CB_HANDLER_ENTRY_t ), CbHandler_Compare);
    }
}