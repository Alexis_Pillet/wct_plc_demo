/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the profile wide client commands for the ZAB implementation
 *   of the SZL ZDO interface
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *              23-Aug-13   MvdB   Original
 * 00.00.02.01  27-Nov-13   MvdB   Support MGMT Routing Req/Rsp
 * 00.00.03.01  19-Mar-14   MvdB   artf53864: Support mgmt leave request/response
 *              27-Jun-14   MvdB   Add User Descriptor support
 * 00.00.06.00  11-Jul-14   MvdB   ARTF60318: Add Power Descriptor Request
 * 00.00.06.03  01-Oct-14   MvdB   artf104879: Support energy scans via Mgmt Nwk Update Request
 * 00.00.06.06  21-Oct-14   MvdB   artf106135: Handle network leave indications
 * 002.000.001  03-Feb-15   MvdB   ARTF115770: Support ZDO Node Descriptor Request
 * 002.001.002  15-Jul-15   MvdB   ARTF136585: Add transaction IDs to over the air ZDO commands/responses
 * 002.002.010  09-Oct-15   MvdB   ARTF150980: Support Service ID in memory allocation functions for WTB
 * 002.002.012  14-Oct-15   MvdB   ARTF104107: Check network status before sending ZDO commands (important for NoComms end device)
 * 002.002.015  21-Oct-15   MvdB   ARTF104106: Support SZL_ZDO_MatchDescriptorReq()
 * 002.002.047  31-Jan-17   MvdB   Add and enforce SZL_ZDO_USER_DESCRIPTOR_LENGTH_MAX
 *****************************************************************************/

/******************************************************************************
 * INCLUDES
 ******************************************************************************/
#include "szl_zab.h"
#include "zabCorePrivate.h"
#include "cb_handler.h"

#include "szl_zdo.h"


/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 * ZDO IEEE Address Response Handler
 ******************************************************************************/
static void ieeeAddrRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoAddrRespParams_t* szlRsp;

  szlRsp = (SZL_ZdoAddrRespParams_t*)sapMsgGetAppData(Message);

  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_IEEE_ADDRESS_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoAddrResp != NULL)
    {
      Callback.SZL_CB_ZdoAddrResp(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}

/******************************************************************************
 * ZDO Nwk Address Response Handler
 ******************************************************************************/
static void nwkAddrRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoAddrRespParams_t* szlRsp;

  szlRsp = (SZL_ZdoAddrRespParams_t*)sapMsgGetAppData(Message);

  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_NWK_ADDRESS_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoAddrResp != NULL)
    {
      Callback.SZL_CB_ZdoAddrResp(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}

/******************************************************************************
 * ZDO Power Descriptor Response Handler
 ******************************************************************************/
static void powerDescriptorRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoPowerDescriptorRespParams_t* szlRsp;

  szlRsp = (SZL_ZdoPowerDescriptorRespParams_t*)sapMsgGetAppData(Message);

  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_POWER_DESCRIPTOR_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoPowerDescriptorResp != NULL)
    {
      Callback.SZL_CB_ZdoPowerDescriptorResp(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}

/******************************************************************************
 * ZDO Node Descriptor Response Handler
 ******************************************************************************/
static void nodeDescriptorRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoNodeDescriptorRespParams_t* szlRsp;

  szlRsp = (SZL_ZdoNodeDescriptorRespParams_t*)sapMsgGetAppData(Message);

  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_NODE_DESCRIPTOR_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoNodeDescriptorResp != NULL)
    {
      Callback.SZL_CB_ZdoNodeDescriptorResp(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}

/******************************************************************************
 * ZDO Active Endpoint Response Handler
 ******************************************************************************/
static void activeEndpointRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoActiveEndpointRespParams_t* szlRsp;

  szlRsp = (SZL_ZdoActiveEndpointRespParams_t*)sapMsgGetAppData(Message);

  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_ACTIVE_ENDPOINT_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoActiveEndpointResp != NULL)
    {
      Callback.SZL_CB_ZdoActiveEndpointResp(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}


/******************************************************************************
 * ZDO Simple Descriptor Response Handler
 ******************************************************************************/
static void simpleDescriptorRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoSimpleDescriptorRespParams_t* szlRsp;

  szlRsp = (SZL_ZdoSimpleDescriptorRespParams_t*)sapMsgGetAppData(Message);

  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_SIMPLE_DESCRIPTOR_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoSimpleDescriptorResp != NULL)
    {
      Callback.SZL_CB_ZdoSimpleDescriptorResp(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}

/******************************************************************************
 * ZDO Match Descriptor Response Handler
 ******************************************************************************/
static void matchDescriptorRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoMatchDescriptorRespParams_t* szlRsp;

  szlRsp = (SZL_ZdoMatchDescriptorRespParams_t*)sapMsgGetAppData(Message);

  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MATCH_DESCRIPTOR_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoMatchDescriptorResp != NULL)
    {
      Callback.SZL_CB_ZdoMatchDescriptorResp(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}

/******************************************************************************
 * ZDO MGMT LQI Response Handler
 ******************************************************************************/
static void mgmtLqiRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoMgmtLqiRespParams_t* szlRsp;

  szlRsp = (SZL_ZdoMgmtLqiRespParams_t*)sapMsgGetAppData(Message);


  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_LQI_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoMgmtLqiResp != NULL)
    {
      Callback.SZL_CB_ZdoMgmtLqiResp(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}

/******************************************************************************
 * ZDO MGMT RTG Response Handler
 ******************************************************************************/
static void mgmtRtgRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoMgmtRtgRespParams_t* szlRsp;

  szlRsp = (SZL_ZdoMgmtRtgRespParams_t*)sapMsgGetAppData(Message);


  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_RTG_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoMgmtRtgResp != NULL)
    {
      Callback.SZL_CB_ZdoMgmtRtgResp(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}

/******************************************************************************
 * ZDO MGMT Network Update Response Handler
 ******************************************************************************/
static void mgmtNwkUpdateRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t* szlRsp;

  szlRsp = (SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t*)sapMsgGetAppData(Message);


  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_NWK_UPDATE_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoMgmtNwkUpdateResp_EnergyScan != NULL)
    {
      Callback.SZL_CB_ZdoMgmtNwkUpdateResp_EnergyScan(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}


/******************************************************************************
 * ZDO Bind Response Handler
 ******************************************************************************/
static void bindRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoBindRespParams_t* szlRsp;

  szlRsp = (SZL_ZdoBindRespParams_t*)sapMsgGetAppData(Message);


  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_BIND_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoBindResp != NULL)
    {
      Callback.SZL_CB_ZdoBindResp(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}

/******************************************************************************
 * ZDO UnBind Response Handler
 ******************************************************************************/
static void unBindRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoBindRespParams_t* szlRsp;

  szlRsp = (SZL_ZdoBindRespParams_t*)sapMsgGetAppData(Message);


  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_UNBIND_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoBindResp != NULL)
    {
      Callback.SZL_CB_ZdoBindResp(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}


/******************************************************************************
 * ZDO MGMT Bind Response Handler
 ******************************************************************************/
static void mgmtBindRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoMgmtBindRespParams_t* szlRsp;

  szlRsp = (SZL_ZdoMgmtBindRespParams_t*)sapMsgGetAppData(Message);


  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_BIND_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoMgmtBindResp != NULL)
    {
      Callback.SZL_CB_ZdoMgmtBindResp(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}

/******************************************************************************
 * ZDO MGMT Leave Response Handler
 ******************************************************************************/
static void mgmtLeaveRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  zabMsgProMgmtLeaveRsp* leaveInd;
  SZL_NwkLeaveRespParams_t szlRsp;
  leaveInd = (zabMsgProMgmtLeaveRsp*)sapMsgGetAppData(Message);


  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_LEAVE_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_NwkLeaveResp != NULL)
    {
      szlRsp.DeviceNwkAddress = leaveInd->srcNwkAddress;
      szlRsp.Status = leaveInd->status;
      if (leaveInd->status == SZL_ZDO_STATUS_SUCCESS)
        {
          Callback.SZL_CB_NwkLeaveResp(Service, SZL_STATUS_SUCCESS, &szlRsp, sapMsgGetAppTransactionId(Message));
        }
      else
        {
          Callback.SZL_CB_NwkLeaveResp(Service, SZL_STATUS_FAILED, &szlRsp, sapMsgGetAppTransactionId(Message));
        }
    }
}

/******************************************************************************
 * ZDO Device Announce Handler
 ******************************************************************************/
static void deviceAnnounceHandler(zabService* Service, sapMsg* Message)
{
  SZL_ZdoDeviceAnnounceIndParams_t* szlRsp;
  int searchIdx = -1; // mark first search before index 0
  SZL_CB_ZdoDeviceAnnounceInd_t cbFunction = NULL;

  szlRsp = (SZL_ZdoDeviceAnnounceIndParams_t*)sapMsgGetAppData(Message);

  while ( (cbFunction = (SZL_CB_ZdoDeviceAnnounceInd_t)CbHandler_Find(Service, &searchIdx, CB_FUNC_ZDO_DEVICE_ANNOUNCE)) != NULL)
    {
      cbFunction(Service, szlRsp);
    }
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * ZDO IEEE Address Request / Response
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_IeeeAddrReq(zabService* Service,
                                 SZL_CB_ZdoAddrResp_t Callback,
                                 SZL_ZdoIeeeAddrReqParams_t* Params,
                                 szl_uint8* TransactionId)
{
  syncCallback syncCB;
  szl_uint8 length;
  szl_uint8 tid;
  SZL_ZdoAddrRespParams_t* errorRspParams;
  sapMsg* Msg;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoAddrRespParams_t*)szl_mem_alloc(SZL_ZdoAddrRespParams_t_SIZE(0),
                                                               MALLOC_ID_ZDO_IEEE_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->NetworkAddress = Params->NetworkAddress;
      errorRspParams->IeeeAddr = SZL_ZDO_MGMT_LQI_IEEE_ADDR_UNKNOWN;
      errorRspParams->NumAssocDev = 0;
      errorRspParams->StartIndex = 0;

      /* Allocate SAP message, with enough data for the Params */
      length = sizeof(SZL_ZdoIeeeAddrReqParams_t);
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_IEEE_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      /* Copy in message specific parameters */
      osMemCopy(&localStatus, sapMsgGetAppData(Msg), (szl_uint8*)Params, length);
      sapMsgPutFree(&localStatus, ZAB_SRV(Service)->DataSAP, Msg);

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoAddrResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                        SZL_ZAB_SYNC_TRANS_TYPE_ZDO_IEEE_ADDRESS_REQ,
                                        tid,
                                        &syncCB,
                                        errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}

/******************************************************************************
 * ZDO IEEE Address Request / Response
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_NwkAddrReq(zabService* Service,
                                SZL_CB_ZdoAddrResp_t Callback,
                                SZL_ZdoNwkAddrReqParams_t* Params,
                                szl_uint8* TransactionId)
{
  syncCallback syncCB;
  szl_uint8 length;
  szl_uint8 tid;
  SZL_ZdoAddrRespParams_t* errorRspParams;
  sapMsg* Msg;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoAddrRespParams_t*)szl_mem_alloc(SZL_ZdoAddrRespParams_t_SIZE(0),
                                                               MALLOC_ID_ZDO_NWK_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->NetworkAddress = SZL_ADDR_NWK_NONE;
      errorRspParams->IeeeAddr = Params->IeeeAddress;
      errorRspParams->NumAssocDev = 0;
      errorRspParams->StartIndex = 0;

      /* Allocate SAP message, with enough data for the Params */
      length = sizeof(SZL_ZdoNwkAddrReqParams_t);
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_NWK_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      osMemCopy(&localStatus, sapMsgGetAppData(Msg), (szl_uint8*)Params, length);

      sapMsgPutFree(&localStatus, ZAB_SRV(Service)->DataSAP, Msg);

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoAddrResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                                  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_NWK_ADDRESS_REQ,
                                                  tid,
                                                  &syncCB,
                                                  errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}

/******************************************************************************
 * ZDO Active Endpoint Request / Response
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_ActiveEndpointReq(zabService* Service,
                                       SZL_CB_ZdoActiveEndpointResp_t Callback,
                                       SZL_ZdoActiveEndpointReqParams_t* Params,
                                       szl_uint8* TransactionId)
{
  szl_uint8 length;
  syncCallback syncCB;
  szl_uint8 tid;
  SZL_ZdoActiveEndpointRespParams_t* errorRspParams;
  sapMsg* Msg;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoActiveEndpointRespParams_t*)szl_mem_alloc(SZL_ZdoActiveEndpointRespParams_t_SIZE(0),
                                                                         MALLOC_ID_ZDO_ACTIVE_EP_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->NetworkAddressOfInterest = Params->NetworkAddressOfInterest;
      errorRspParams->ActiveEndpointCount = 0;

      /* Allocate SAP message, with enough data for the Params */
      length = sizeof(SZL_ZdoActiveEndpointReqParams_t);
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_ACTIVE_EP_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      osMemCopy(&localStatus, sapMsgGetAppData(Msg), (szl_uint8*)Params, length);
      sapMsgPutFree(&localStatus, ZAB_SRV( Service )->DataSAP, Msg );

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoActiveEndpointResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                                    SZL_ZAB_SYNC_TRANS_TYPE_ZDO_ACTIVE_ENDPOINT_REQ,
                                                    tid,
                                                    &syncCB,
                                                    (void*)errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}

/******************************************************************************
 * ZDO Simple Descriptor Request / Response
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_SimpleDescriptorReq(zabService* Service,
                                         SZL_CB_ZdoSimpleDescriptorResp_t Callback,
                                         SZL_ZdoSimpleDescriptorReqParams_t* Params,
                                         szl_uint8* TransactionId)
{
  szl_uint8 length;
  syncCallback syncCB;
  szl_uint8 tid;
  SZL_ZdoSimpleDescriptorRespParams_t* errorRspParams;
  sapMsg* Msg;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoSimpleDescriptorRespParams_t*)szl_mem_alloc(SZL_ZdoSimpleDescriptorRespParams_t_SIZE(0, 0),
                                                                           MALLOC_ID_ZDO_SIMPLE_DESC_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->NetworkAddress = Params->NetworkAddress;
      errorRspParams->NetworkAddressOfInterest = Params->NetworkAddressOfInterest;
      errorRspParams->Endpoint = Params->Endpoint;
      errorRspParams->DeviceId = 0xFFFF;
      errorRspParams->DeviceVersion = 0xFF;
      errorRspParams->NumInClusters = 0;
      errorRspParams->NumOutClusters = 0;

      /* Allocate SAP message, with enough data for the Params */
      length = sizeof(SZL_ZdoSimpleDescriptorReqParams_t);
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_SIMPLE_DESC_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      osMemCopy(&localStatus, sapMsgGetAppData(Msg), (szl_uint8*)Params, length);
      sapMsgPutFree(&localStatus, ZAB_SRV( Service )->DataSAP, Msg );

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoSimpleDescriptorResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                                  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_SIMPLE_DESCRIPTOR_REQ,
                                                  tid,
                                                  &syncCB,
                                                  errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}

/******************************************************************************
 * ZDO Match Descriptor Request / Response
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_MatchDescriptorReq(zabService* Service,
                                        SZL_CB_ZdoMatchDescriptorResp_t Callback,
                                        SZL_ZdoMatchDescriptorReqParams_t* Params,
                                        szl_uint8* TransactionId)
{
  szl_uint8 length;
  syncCallback syncCB;
  szl_uint8 tid;
  SZL_ZdoMatchDescriptorRespParams_t* errorRspParams;
  sapMsg* Msg;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoMatchDescriptorRespParams_t*)szl_mem_alloc(SZL_ZdoMatchDescriptorRespParams_t_SIZE(0),
                                                                          MALLOC_ID_ZDO_MATCH_DESC_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->NetworkAddress = Params->NetworkAddress;
      errorRspParams->NumberOfEndpoints = 0;

      /* Allocate SAP message, with enough data for the Params */
      length = sizeof(SZL_ZdoMatchDescriptorReqParams_t);
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_MATCH_DESC_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      osMemCopy(&localStatus, sapMsgGetAppData(Msg), (szl_uint8*)Params, length);
      sapMsgPutFree(&localStatus, ZAB_SRV( Service )->DataSAP, Msg );

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoMatchDescriptorResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                                  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MATCH_DESCRIPTOR_REQ,
                                                  tid,
                                                  &syncCB,
                                                  errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}

/******************************************************************************
 * ZDO MGMT Network Update Request - Energy Scan
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_MgmtNwkUpdateReq_EnergyScan(zabService* Service,
                                                 SZL_CB_ZdoMgmtNwkUpdateResp_EnergyScan_t Callback,
                                                 SZL_ZdoMgmtNwkUpdateReq_EnergyScan_Params_t* Params,
                                                 szl_uint8* TransactionId)
{
  szl_uint8 length;
  syncCallback syncCB;
  szl_uint8 tid;
  SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t* errorRspParams;
  zabMsgProMgmtNwkUpdateReq* internalReqParams;
  sapMsg* Msg;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t*)szl_mem_alloc(SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t_SIZE,
                                                                                    MALLOC_ID_ZDO_MGMT_NWK_UPDATE_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->NetworkAddress = Params->NetworkAddress;
      errorRspParams->ScannedChannelsMask = CHANNEL_DEFAULT;
      errorRspParams->TotalTransmisisons = 0;
      errorRspParams->TransmisisonFailures = 0;
      errorRspParams->ScannedChannelListCount = 0;

      /* Allocate SAP message, with enough data for the full internal set of Params */
      length = zabMsgProMgmtNwkUpdateReq_SIZE;
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_MGMT_NWK_UPDATE_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      /* Copy parameters into message and send */
      internalReqParams = (zabMsgProMgmtNwkUpdateReq*)sapMsgGetAppData(Msg);
      internalReqParams->DestinationAddressMode = ZAB_ADDRESS_MODE_NWK_ADDRESS;
      internalReqParams->DestinationAddress = Params->NetworkAddress;
      internalReqParams->ChannelMask = Params->ScanChannelsMask;
      internalReqParams->ChannelMask <<= 11; // Convert from SZL mask to normal mask
      internalReqParams->ScanDuration = 5; // Longest scan for better averaging
      internalReqParams->ScanCount = 1; // Only a single scan so we get a single response.
      internalReqParams->NetworkManagerAddress = 0;


      sapMsgPutFree(&localStatus, ZAB_SRV( Service )->DataSAP, Msg );

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoMgmtNwkUpdateResp_EnergyScan = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                                  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_NWK_UPDATE_REQ,
                                                  tid,
                                                  &syncCB,
                                                  errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}


/******************************************************************************
 * ZDO MGMT LQI Request / Response
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_MgmtLqiReq(zabService* Service,
                                SZL_CB_ZdoMgmtLqiResp_t Callback,
                                SZL_ZdoMgmtLqiReqParams_t* Params,
                                szl_uint8* TransactionId)
{
  szl_uint8 length;
  syncCallback syncCB;
  szl_uint8 tid;
  SZL_ZdoMgmtLqiRespParams_t* errorRspParams;
  sapMsg* Msg;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoMgmtLqiRespParams_t*)szl_mem_alloc(SZL_ZdoMgmtLqiRespParams_t_SIZE(0),
                                                                  MALLOC_ID_ZDO_MGMT_LQI_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->NetworkAddress = Params->NetworkAddress;
      errorRspParams->NeighborTableEntries = 0;
      errorRspParams->NeighborTableListCount = 0;
      errorRspParams->StartIndex = 0;

      /* Allocate SAP message, with enough data for the Params */
      length = sizeof(SZL_ZdoMgmtLqiReqParams_t);
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_MGMT_LQI_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      /* Copy parameters into message and send */
      osMemCopy(&localStatus, sapMsgGetAppData(Msg), (szl_uint8*)Params, sizeof(SZL_ZdoMgmtLqiReqParams_t));
      sapMsgPutFree(&localStatus, ZAB_SRV( Service )->DataSAP, Msg );

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoMgmtLqiResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                                  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_LQI_REQ,
                                                  tid,
                                                  &syncCB,
                                                  errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}


/******************************************************************************
 * ZDO MGMT RTG Request / Response
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_MgmtRtgReq(zabService* Service,
                                SZL_CB_ZdoMgmtRtgResp_t Callback,
                                SZL_ZdoMgmtRtgReqParams_t* Params,
                                szl_uint8* TransactionId)
{
  szl_uint8 length;
  syncCallback syncCB;
  szl_uint8 tid;
  SZL_ZdoMgmtRtgRespParams_t* errorRspParams;
  sapMsg* Msg;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoMgmtRtgRespParams_t*)szl_mem_alloc(SZL_ZdoMgmtRtgRespParams_t_SIZE(0),
                                                                  MALLOC_ID_ZDO_MGMT_RTG_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->SourceNetworkAddress = Params->NetworkAddress;
      errorRspParams->RoutingTableEntries = 0;
      errorRspParams->RoutingTableListCount = 0;
      errorRspParams->StartIndex = 0;

      /* Allocate SAP message, with enough data for the Params */
      length = sizeof(SZL_ZdoMgmtRtgReqParams_t);
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_MGMT_RTG_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      /* Copy parameters into message and send */
      osMemCopy(&localStatus, sapMsgGetAppData(Msg), (szl_uint8*)Params, sizeof(SZL_ZdoMgmtRtgReqParams_t));
      sapMsgPutFree(&localStatus, ZAB_SRV( Service )->DataSAP, Msg );

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoMgmtRtgResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                                  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_RTG_REQ,
                                                  tid,
                                                  &syncCB,
                                                  errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}


/******************************************************************************
 * ZDO MGMT Bind Request / Response
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_MgmtBindReq(zabService* Service,
                                 SZL_CB_ZdoMgmtBindResp_t Callback,
                                 SZL_ZdoMgmtBindReqParams_t* Params,
                                 szl_uint8* TransactionId)
{
  szl_uint8 length;
  syncCallback syncCB;
  szl_uint8 tid;
  SZL_ZdoMgmtBindRespParams_t* errorRspParams;
  sapMsg* Msg;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoMgmtBindRespParams_t*)szl_mem_alloc(SZL_ZdoMgmtBindRespParams_t_SIZE(0),
                                                                   MALLOC_ID_ZDO_MGMT_BIND_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->SourceNetworkAddress = Params->NetworkAddress;
      errorRspParams->BindingTableEntries = 0;
      errorRspParams->BindingTableListCount = 0;
      errorRspParams->StartIndex = 0;

      /* Allocate SAP message, with enough data for the Params */
      length = sizeof(SZL_ZdoMgmtBindReqParams_t);
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_MGMT_BIND_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      /* Copy parameters into message and send */
      osMemCopy(&localStatus, sapMsgGetAppData(Msg), (szl_uint8*)Params, sizeof(SZL_ZdoMgmtBindReqParams_t));
      sapMsgPutFree(&localStatus, ZAB_SRV( Service )->DataSAP, Msg );

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoMgmtBindResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                                  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_BIND_REQ,
                                                  tid,
                                                  &syncCB,
                                                  errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}


/******************************************************************************
 * ZDO Bind Request / Response
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_BindReq(zabService* Service,
                             SZL_CB_ZdoBindResp_t Callback,
                             SZL_ZdoBindReqParams_t* Params,
                             szl_uint8* TransactionId)
{
  syncCallback syncCB;
  szl_uint8 tid;
  SZL_ZdoBindRespParams_t* errorRspParams;
  sapMsg* Msg;
  szl_uint8 length;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoBindRespParams_t*)szl_mem_alloc(SZL_ZdoBindRespParams_t_SIZE,
                                                               MALLOC_ID_ZDO_BIND_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->NetworkAddress = Params->NetworkAddress;

      /* Allocate SAP message, with enough data for the Params */
      length = sizeof(SZL_ZdoBindReqParams_t);
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_BIND_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      /* Copy parameters into message and send */
      osMemCopy(&localStatus, sapMsgGetAppData(Msg), (szl_uint8*)Params, length);
      sapMsgPutFree(&localStatus, ZAB_SRV( Service )->DataSAP, Msg );

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoBindResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                                  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_BIND_REQ,
                                                  tid,
                                                  &syncCB,
                                                  errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}

/******************************************************************************
 * ZDO UnBind Request / Response
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_UnBindReq(zabService* Service,
                               SZL_CB_ZdoBindResp_t Callback,
                               SZL_ZdoBindReqParams_t* Params,
                               szl_uint8* TransactionId)
{
  syncCallback syncCB;
  szl_uint8 tid;
  SZL_ZdoBindRespParams_t* errorRspParams;
  sapMsg* Msg;
  szl_uint8 length;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoBindRespParams_t*)szl_mem_alloc(SZL_ZdoBindRespParams_t_SIZE,
                                                               MALLOC_ID_ZDO_UNBIND_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->NetworkAddress = Params->NetworkAddress;

      /* Allocate SAP message, with enough data for the Params */
      length = sizeof(SZL_ZdoBindReqParams_t);
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_UNBIND_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      /* Copy parameters into message and send */
      osMemCopy(&localStatus, sapMsgGetAppData(Msg), (szl_uint8*)Params, length);
      sapMsgPutFree(&localStatus, ZAB_SRV( Service )->DataSAP, Msg );

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoBindResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                                  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_UNBIND_REQ,
                                                  tid,
                                                  &syncCB,
                                                  errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}

/******************************************************************************
 * ZDO Device Announce - Register Indication Handler
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_DeviceAnnounceNtfRegisterCB(zabService* Service,
                                                 SZL_CB_ZdoDeviceAnnounceInd_t Callback)
{
  return CbHandler_Add(Service, CB_FUNC_ZDO_DEVICE_ANNOUNCE, (CBQueue_CallbackProto)Callback);
}

/******************************************************************************
 * ZDO Device Announce - Unregister Indication Handler
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_DeviceAnnounceNtfUnregisterCB(zabService* Service,
                                                   SZL_CB_ZdoDeviceAnnounceInd_t Callback)
{
    return CbHandler_Remove(Service, CB_FUNC_ZDO_DEVICE_ANNOUNCE, (CBQueue_CallbackProto)Callback);
}

/******************************************************************************
 * ZDO User Descriptor Request / Response
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_UserDescriptorReq(zabService* Service,
                                       SZL_CB_ZdoUserDescriptorResp_t Callback,
                                       SZL_ZdoUserDescriptorReqParams_t* Params,
                                       szl_uint8* TransactionId)
{
  szl_uint8 length;
  syncCallback syncCB;
  szl_uint8 tid;
  SZL_ZdoUserDescriptorRespParams_t* errorRspParams;
  sapMsg* Msg;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoUserDescriptorRespParams_t*)szl_mem_alloc(SZL_ZdoUserDescriptorRespParams_t_SIZE(1),
                                                                         MALLOC_ID_ZDO_USER_DESC_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->SourceAddress = Params->NetworkAddress;
      errorRspParams->NetworkAddressOfInterest = Params->NetworkAddressOfInterest;

      /* Set length to zero and also null terminate the string to be safe */
      errorRspParams->UserDescriptorLength = 0;
      errorRspParams->UserDescriptor[0] = 0;

      /* Allocate SAP message, with enough data for the Params */
      length = sizeof(SZL_ZdoUserDescriptorReqParams_t);
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_USER_DESC_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      osMemCopy(&localStatus, sapMsgGetAppData(Msg), (szl_uint8*)Params, length);
      sapMsgPutFree(&localStatus, ZAB_SRV( Service )->DataSAP, Msg );

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoUserDescriptorResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                                    SZL_ZAB_SYNC_TRANS_TYPE_ZDO_USER_DESCRIPTOR_REQ,
                                                    tid,
                                                    &syncCB,
                                                    (void*)errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}


/******************************************************************************
 * ZDO User Descriptor Response Handler
 ******************************************************************************/
static void userDescriptorRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoUserDescriptorRespParams_t* szlRsp;

  szlRsp = (SZL_ZdoUserDescriptorRespParams_t*)sapMsgGetAppData(Message);

  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_USER_DESCRIPTOR_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoUserDescriptorResp != NULL)
    {
      Callback.SZL_CB_ZdoUserDescriptorResp(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}


/******************************************************************************
 * ZDO User Descriptor Set Request / Response
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_UserDescriptorSetReq(zabService* Service,
                                          SZL_CB_ZdoUserDescriptorSetResp_t Callback,
                                          SZL_ZdoUserDescriptorSetReqParams_t* Params,
                                          szl_uint8* TransactionId)
{
  szl_uint8 length;
  syncCallback syncCB;
  szl_uint8 tid;
  SZL_ZdoUserDescriptorSetRespParams_t* errorRspParams;
  sapMsg* Msg;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) || (Params->UserDescriptorLength > SZL_ZDO_USER_DESCRIPTOR_LENGTH_MAX) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoUserDescriptorSetRespParams_t*)szl_mem_alloc(SZL_ZdoUserDescriptorSetRespParams_t_SIZE,
                                                                            MALLOC_ID_ZDO_USER_DESC_SET_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->SourceAddress = Params->NetworkAddress;
      errorRspParams->NetworkAddressOfInterest = Params->NetworkAddressOfInterest;

      /* Allocate SAP message, with enough data for the Params */
      length = SZL_ZdoUserDescriptorSetReqParams_t_SIZE(Params->UserDescriptorLength);
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_USER_DESC_SET_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      osMemCopy(&localStatus, sapMsgGetAppData(Msg), (szl_uint8*)Params, length);
      sapMsgPutFree(&localStatus, ZAB_SRV( Service )->DataSAP, Msg );

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoUserDescriptorSetResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                                    SZL_ZAB_SYNC_TRANS_TYPE_ZDO_USER_DESCRIPTOR_SET_REQ,
                                                    tid,
                                                    &syncCB,
                                                    (void*)errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}

/******************************************************************************
 * ZDO USer Descriptor Set Response Handler
 ******************************************************************************/
static void userDescriptorSetRespHandler(zabService* Service, sapMsg* Message)
{
  syncCallback Callback;
  SZL_ZdoUserDescriptorSetRespParams_t* szlRsp;

  szlRsp = (SZL_ZdoUserDescriptorSetRespParams_t*)sapMsgGetAppData(Message);

  /* Get the callback from the synchronous transaction table. If none, request has probably been timed out, so just exit */
  Callback = SzlZab_GetSynchronousTransactionCallbackAndDestroy(Service,
                                                                SZL_ZAB_SYNC_TRANS_TYPE_ZDO_USER_DESCRIPTOR_SET_REQ,
                                                                sapMsgGetAppTransactionId(Message));

  if (Callback.SZL_CB_ZdoUserDescriptorSetResp != NULL)
    {
      Callback.SZL_CB_ZdoUserDescriptorSetResp(Service, SZL_STATUS_SUCCESS, szlRsp, sapMsgGetAppTransactionId(Message));
    }
}


/******************************************************************************
 * ZDO Power Descriptor Request / Response
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_PowerDescriptorReq(zabService* Service,
                                        SZL_CB_ZdoPowerDescriptorResp_t Callback,
                                        SZL_ZdoPowerDescriptorReqParams_t* Params,
                                        szl_uint8* TransactionId)
{
  syncCallback syncCB;
  szl_uint8 length;
  szl_uint8 tid;
  SZL_ZdoPowerDescriptorRespParams_t* errorRspParams;
  sapMsg* Msg;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoPowerDescriptorRespParams_t*)szl_mem_alloc(SZL_ZdoPowerDescriptorRespParams_t_SIZE,
                                                                          MALLOC_ID_ZDO_POWER_DESC_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }
      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->SourceAddress = Params->NetworkAddress;
      errorRspParams->NetworkAddressOfInterest = Params->NetworkAddressOfInterest;
      errorRspParams->AvailablePowerSources = SZL_AVAILABLE_POWER_SOURCE_UNKNOWN;
      errorRspParams->CurrentPowerMode = SZL_ZDO_CURRENT_POWER_MODE_RX_ON_WHEN_IDLE;
      errorRspParams->CurrentPowerSourceLevel = SZL_CURRENT_POWER_SOURCE_LEVEL_100_PERCENT;
      errorRspParams->CurrentPowerSource = SZL_CURRENT_POWER_SOURCE_UNKNWON;

      /* Allocate SAP message, with enough data for the Params */
      length = sizeof(SZL_ZdoPowerDescriptorReqParams_t);
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_POWER_DESC_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      osMemCopy(&localStatus, sapMsgGetAppData(Msg), (szl_uint8*)Params, length);

      sapMsgPutFree(&localStatus, ZAB_SRV(Service)->DataSAP, Msg);

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoPowerDescriptorResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                                    SZL_ZAB_SYNC_TRANS_TYPE_ZDO_POWER_DESCRIPTOR_REQ,
                                                    tid,
                                                    &syncCB,
                                                    errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}



/******************************************************************************
 * Network Leave Indication Handler
 ******************************************************************************/
static void nwkLeaveIndHandler(zabService* Service, sapMsg* Message)
{
  SZL_NwkLeaveNtfParams_t szlInd;
  zabMsgPro_NwkLeaveInd* zabInd;

  /* Get pointer to ZAB structure, then convert what is required into the SZL struct and call any registered callbacks */
  zabInd = (zabMsgPro_NwkLeaveInd*)sapMsgGetAppData(Message);

  /* Only notify if it is a permanent leave (not rejoining) and a notification (not a request)*/
  if ( (zabInd->rejoin == zab_false) && (zabInd->request == zab_false) )
    {
      szlInd.NwkAddress = zabInd->srcNwkAddress;
      Szl_NwkLeaveNtfCB(Service, &szlInd);
    }
}

/******************************************************************************
 * ZDO Node Descriptor Request / Response
 ******************************************************************************/
SZL_RESULT_t SZL_ZDO_NodeDescriptorReq(zabService* Service,
                                       SZL_CB_ZdoNodeDescriptorResp_t Callback,
                                       SZL_ZdoNodeDescriptorReqParams_t* Params,
                                       szl_uint8* TransactionId)
{
  syncCallback syncCB;
  unsigned8 length;
  szl_uint8 tid;
  SZL_ZdoNodeDescriptorRespParams_t* errorRspParams;
  sapMsg* Msg;
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  SZL_RESULT_t result = Szl_IsRequestValid(Service, SZL_APP_COORDINATOR | SZL_APP_ROUTER | SZL_APP_SED);

  if (SZL_RESULT_SUCCESS == result)
    {
      /* Validate inputs */
      if ( (Callback == NULL) || (Params == NULL) )
        {
          return SZL_RESULT_INVALID_DATA;
        }

      /* Make sure we can have a transaction Id */
      tid = szl_zab_GetTransactionId(Service);
      if (tid == SZL_ZAB_M_TRANSACTION_ID_INVALID)
        {
          return SZL_RESULT_NO_FREE_TID;
        }
      if (TransactionId != NULL)
        {
          *TransactionId = tid;
        }

      /* Create the response that will be used in case of no response from the destination */
      errorRspParams = (SZL_ZdoNodeDescriptorRespParams_t*)szl_mem_alloc(SZL_ZdoNodeDescriptorRespParams_t_SIZE,
                                                                         MALLOC_ID_ZDO_NODE_DESC_FAKE_RSP);
      if (errorRspParams == NULL)
        {
          return SZL_RESULT_INTERNAL_LIB_ERROR;
        }

      errorRspParams->Status = SZL_ZDO_STATUS_TIMEOUT;
      errorRspParams->SourceAddress = Params->NetworkAddress;
      errorRspParams->NetworkAddressOfInterest = Params->NetworkAddressOfInterest;
      errorRspParams->Option.optBits.LogicalType = SZL_ZDO_MGMT_LQI_DEVICE_UNKNOWN;
      errorRspParams->Option.optBits.ComplexDescriptorAvailable = NOT_SUPPORTED;
      errorRspParams->Option.optBits.UserDescriptorAvailable = NOT_SUPPORTED;
      errorRspParams->Option.optBits.FrequencyBand = SZL_ZDO_FEQUENCY_BAND_UNKNOWN;
      errorRspParams->MACCapabilityFlag.optBits.AllocateAddress = NOT_SUPPORTED;
      errorRspParams->MACCapabilityFlag.optBits.AlternatePANCoordinator = NOT_SUPPORTED;
      errorRspParams->MACCapabilityFlag.optBits.PowerSource = NOT_SUPPORTED;
      errorRspParams->MACCapabilityFlag.optBits.ReceiverOnWhenIdle = NOT_SUPPORTED;
      errorRspParams->MACCapabilityFlag.optBits.SecurityCapability = NOT_SUPPORTED;
      errorRspParams->MACCapabilityFlag.optBits.DeviceType = 0;
      errorRspParams->ManufacturerCode = DEFAULT_MANUFACTURER_CODE;
      errorRspParams->MaximumBufferSize = DEFAULT_SIZE;
      errorRspParams->MaximumIncomingTransferSize = DEFAULT_SIZE;
      errorRspParams->ServerMask.optBits.BackupBindingTableCache = NOT_SUPPORTED;
      errorRspParams->ServerMask.optBits.BackupDiscoveryCache = NOT_SUPPORTED;
      errorRspParams->ServerMask.optBits.BackupTrustCenter = NOT_SUPPORTED;
      errorRspParams->ServerMask.optBits.NetworkManager = NOT_SUPPORTED;
      errorRspParams->ServerMask.optBits.PrimaryBindingTableCache = NOT_SUPPORTED;
      errorRspParams->ServerMask.optBits.PrimaryDiscoveryCache = NOT_SUPPORTED;
      errorRspParams->ServerMask.optBits.PrimaryTrustCenter = NOT_SUPPORTED;
      errorRspParams->MaximumOutgoingTransferSize = DEFAULT_SIZE;
      errorRspParams->DescriptorCapability.optBits.ExtendedActiveEndpointListAvailable = NOT_SUPPORTED;
      errorRspParams->DescriptorCapability.optBits.ExtendedSimpleDescriptorListAvailable = NOT_SUPPORTED;

      /* Allocate SAP message, with enough data for the Params */
      length = sizeof(SZL_ZdoNodeDescriptorReqParams_t);
      Msg = sapMsgAllocateData( &localStatus, ZAB_SRV(Service)->DataSAP, SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, length);
      if (erStatusIsError(&localStatus) || (Msg == NULL))
        {
          szl_mem_free(errorRspParams);
          return SZL_RESULT_QUEUE_FULL;
        }

      /* Set message parameters */
      sapMsgSetAppDataMax (&localStatus, Msg, length);
      sapMsgSetAppDataLength(&localStatus, Msg, length);
      sapMsgSetAppType(&localStatus, Msg, ZAB_MSG_APP_ZDO_NODE_DESC_REQ);
      sapMsgSetAppTransactionId(&localStatus, Msg, tid);

      osMemCopy(&localStatus, sapMsgGetAppData(Msg), (unsigned8*)Params, length);

      sapMsgPutFree(&localStatus, ZAB_SRV(Service)->DataSAP, Msg);

      /* If it sent ok, then add the transaction. If both ok, return success */
      result = SZL_RESULT_FAILED;
      if (erStatusIsOk(&localStatus))
        {
          syncCB.SZL_CB_ZdoNodeDescriptorResp = Callback;
          result = SzlZab_AddSynchronousTransaction(Service,
                                                    SZL_ZAB_SYNC_TRANS_TYPE_ZDO_NODE_DESCRIPTOR_REQ,
                                                    tid,
                                                    &syncCB,
                                                    errorRspParams);
        }
      if (result != SZL_RESULT_SUCCESS)
        {
          szl_mem_free(errorRspParams);
        }
    }
  return result;
}


/******************************************************************************
 *                      ******************************
 *                ***** INTERNAL FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/


/*******************************************************************************
 * Data in handler for the SZL ZDO
 ******************************************************************************/
void szlZdo_DataInHandler(erStatus* Status, zabService* Service, sapMsg* Message)
{
  ER_CHECK_NULL(Status, Message);

  switch(sapMsgGetAppType(Message))
    {
      case ZAB_MSG_APP_ZDO_IEEE_RSP:
        ieeeAddrRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_NWK_RSP:
        nwkAddrRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_POWER_DESC_RSP:
        powerDescriptorRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_NODE_DESC_RSP:
        nodeDescriptorRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_ACTIVE_EP_RSP:
        activeEndpointRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_SIMPLE_DESC_RSP:
        simpleDescriptorRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_MATCH_DESC_RSP:
        matchDescriptorRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_MGMT_LQI_RSP:
        mgmtLqiRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_MGMT_RTG_RSP:
        mgmtRtgRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_MGMT_NWK_UPDATE_RSP:
        mgmtNwkUpdateRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_MGMT_BIND_RSP:
        mgmtBindRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_MGMT_LEAVE_RSP:
        mgmtLeaveRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_BIND_RSP:
        bindRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_UNBIND_RSP:
        unBindRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_DEVICE_ANNOUNCE:
        deviceAnnounceHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_USER_DESC_RSP:
        userDescriptorRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_USER_DESC_SET_RSP:
        userDescriptorSetRespHandler(Service, Message);
        break;

      case ZAB_MSG_APP_ZDO_NWK_LEAVE_IND:
        nwkLeaveIndHandler(Service, Message);
        break;


      default:
        printError(Service, "SZL ZDO: Warning - Unhandled message type 0x%02X received\n", sapMsgGetAppType(Message));
        break;
    }
}