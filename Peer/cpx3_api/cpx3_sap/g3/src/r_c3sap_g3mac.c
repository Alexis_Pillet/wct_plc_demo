/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name : r_c3sap_g3mac.c
 * Description : G3 MAC layer API
 ******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_typedefs.h"
#include "r_c3sap_config.h"
#include "r_c3sap_g3_if.h"
#include "r_c3sap_g3_convert.h"
#include "r_g3_cmd.h"
#include "r_g3mac_binstruct.h"
#include "r_c3sap_plc_buffsize.h"

/******************************************************************************
Macro definitions
******************************************************************************/

/******************************************************************************
Typedef definitions
******************************************************************************/
typedef struct
{
    r_g3_mac_callback_t cb[R_G3_CH_MAX];
    uint8_t *           preq_bin;
    r_g3mac_cb_str_t *  pcb_str;
    uint16_t            req_buff_size;
    uint16_t            cb_buff_size;
} r_g3mac_info_t;

/******************************************************************************
Private global variables and functions
******************************************************************************/
static void g3mac_base_cb_cnf (uint8_t * pbuff);
static void g3mac_base_cb_ind (uint8_t * pbuff);
static r_result_t g3mac_check_param (uint8_t ch, uint16_t func_id, void * preq, uint8_t * preq_buff);
static r_result_t g3mac_process_request (uint8_t ch, uint16_t func_id, void * preq);

static r_g3mac_info_t g3mac_info;

/*===========================================================================*/
/* Functions                                                                 */
/*===========================================================================*/


/******************************************************************************
* Function Name:R_G3MAC_McpsDataReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_G3MAC_McpsDataReq (uint8_t ch, r_g3mac_mcps_data_req_t * preq)
{
    r_result_t status;

    status = g3mac_process_request (ch, R_G3_FUNCID_MCPS_DATA_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_G3MAC_McpsDataReq
******************************************************************************/

/******************************************************************************
* Function Name:R_G3MAC_MlmeResetReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_G3MAC_MlmeResetReq (uint8_t ch, r_g3mac_mlme_reset_req_t * preq)
{
    r_result_t status;

    status = g3mac_process_request (ch, R_G3_FUNCID_MLME_RESET_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_G3MAC_MlmeResetReq
******************************************************************************/

/******************************************************************************
* Function Name:R_G3MAC_MlmeGetReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_G3MAC_MlmeGetReq (uint8_t ch, r_g3mac_mlme_get_req_t * preq)
{
    r_result_t status;

    status = g3mac_process_request (ch, R_G3_FUNCID_MLME_GET_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_G3MAC_MlmeGetReq
******************************************************************************/

/******************************************************************************
* Function Name:R_G3MAC_MlmeSetReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_G3MAC_MlmeSetReq (uint8_t ch, r_g3mac_mlme_set_req_t * preq)
{
    r_result_t status;

    status = g3mac_process_request (ch, R_G3_FUNCID_MLME_SET_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_G3MAC_MlmeSetReq
******************************************************************************/

/******************************************************************************
* Function Name:R_G3MAC_MlmeScanReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_G3MAC_MlmeScanReq (uint8_t ch, r_g3mac_mlme_scan_req_t * preq)
{
    r_result_t status;

    status = g3mac_process_request (ch, R_G3_FUNCID_MLME_SCAN_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_G3MAC_MlmeScanReq
******************************************************************************/

/******************************************************************************
* Function Name:R_G3MAC_MlmeStartReq
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_G3MAC_MlmeStartReq (uint8_t ch, r_g3mac_mlme_start_req_t * preq)
{
    r_result_t status;

    status = g3mac_process_request (ch, R_G3_FUNCID_MLME_START_REQ, preq);

    return status;
}
/******************************************************************************
   End of function  R_G3MAC_MlmeStartReq
******************************************************************************/

/******************************************************************************
* Function Name:R_G3MAC_RegistCb
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_G3MAC_RegistCb (uint8_t ch, r_g3_mac_callback_t * pcallbacks)
{
    if (NULL == pcallbacks)
    {
        return R_RESULT_BAD_INPUT_ARGUMENTS;
    }

    g3mac_info.cb[ch] = *pcallbacks;

    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_G3MAC_RegistCb
******************************************************************************/

/******************************************************************************
* Function Name:R_G3MAC_ClearCb
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_G3MAC_ClearCb (uint8_t ch)
{
    R_memset (&g3mac_info.cb[ch], 0, sizeof (r_g3_mac_callback_t));
    
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_G3MAC_ClearCb
******************************************************************************/

/******************************************************************************
* Function Name:R_G3MAC_BaseCb
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_G3MAC_BaseCb (uint8_t * pbuff)
{
    uint8_t ida;
    uint8_t idp;

    if (NULL != pbuff)
    {
        ida = R_G3_GET_PKT_IDA (pbuff[0]);
        idp = R_G3_GET_PKT_IDP (pbuff[0]);

        if (R_G3_UNITID_G3MAC == idp)
        {
            if (R_G3_CMDTYPE_CNF == ida)
            {
                g3mac_base_cb_cnf (pbuff);
            }
            else if (R_G3_CMDTYPE_IND == ida)
            {
                g3mac_base_cb_ind (pbuff);
            }
            else
            {
                /* Do Nothing */
            }
        }
    }
} /* R_G3MAC_BaseCb */
/******************************************************************************
   End of function  R_G3MAC_BaseCb
******************************************************************************/

/******************************************************************************
* Function Name:R_G3MAC_SapInit
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_G3MAC_SapInit (uint8_t * preq_bin, uint16_t req_buff_size, void * pcb_str, uint16_t cb_buff_size)
{
    R_memset ((void *)&g3mac_info, 0, sizeof (r_g3mac_info_t));

    if ((NULL != preq_bin) && (NULL != pcb_str))
    {
        g3mac_info.preq_bin      = preq_bin;
        g3mac_info.pcb_str       = (r_g3mac_cb_str_t *)pcb_str;
        g3mac_info.req_buff_size = req_buff_size;
        g3mac_info.cb_buff_size  = cb_buff_size;
    }
}
/******************************************************************************
   End of function  R_G3MAC_SapInit
******************************************************************************/

/******************************************************************************
* Function Name:g3mac_base_cb_cnf
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void g3mac_base_cb_cnf (uint8_t * pbuff)
{
    uint8_t    idc;
    uint8_t    cmd;
    uint16_t   length;
    uint16_t   max_len = g3mac_info.cb_buff_size;
    r_result_t status;

    if ((NULL == pbuff) || (NULL == g3mac_info.pcb_str))
    {
        return;
    }
    idc = R_G3_GET_PKT_IDC (pbuff[0]);
    cmd = pbuff[1];

    switch (cmd)
    {
        case R_G3_CMDID_MCPS_DATA:
        {
            if (NULL != g3mac_info.cb[idc].R_G3MAC_McpsDataCnf)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MCPS_DATA_CNF, &pbuff[2], max_len, &g3mac_info.pcb_str->mcps_data_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    g3mac_info.cb[idc].R_G3MAC_McpsDataCnf (&g3mac_info.pcb_str->mcps_data_cnf);
                }
            }
            break;
        }

        case R_G3_CMDID_MLME_RESET:
        {
            if (NULL != g3mac_info.cb[idc].R_G3MAC_MlmeResetCnf)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_RESET_CNF, &pbuff[2], max_len, &g3mac_info.pcb_str->mlme_reset_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    g3mac_info.cb[idc].R_G3MAC_MlmeResetCnf (&g3mac_info.pcb_str->mlme_reset_cnf);
                }
            }
            break;
        }

        case R_G3_CMDID_MLME_GET:
        {
            if (NULL != g3mac_info.cb[idc].R_G3MAC_MlmeGetCnf)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_GET_CNF, &pbuff[2], max_len, &g3mac_info.pcb_str->mlme_get_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    g3mac_info.cb[idc].R_G3MAC_MlmeGetCnf (&g3mac_info.pcb_str->mlme_get_cnf);
                }
            }
            break;
        }

        case R_G3_CMDID_MLME_SET:
        {
            if (NULL != g3mac_info.cb[idc].R_G3MAC_MlmeSetCnf)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_SET_CNF, &pbuff[2], max_len, &g3mac_info.pcb_str->mlme_set_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    g3mac_info.cb[idc].R_G3MAC_MlmeSetCnf (&g3mac_info.pcb_str->mlme_set_cnf);
                }
            }
            break;
        }

        case R_G3_CMDID_MLME_SCAN:
        {
            if (NULL != g3mac_info.cb[idc].R_G3MAC_MlmeScanCnf)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_SCAN_CNF, &pbuff[2], max_len, &g3mac_info.pcb_str->mlme_scan_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    g3mac_info.cb[idc].R_G3MAC_MlmeScanCnf (&g3mac_info.pcb_str->mlme_scan_cnf);
                }
            }
            break;
        }

        case R_G3_CMDID_MLME_START:
        {
            if (NULL != g3mac_info.cb[idc].R_G3MAC_MlmeStartCnf)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_START_CNF, &pbuff[2], max_len, &g3mac_info.pcb_str->mlme_start_cnf, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    g3mac_info.cb[idc].R_G3MAC_MlmeStartCnf (&g3mac_info.pcb_str->mlme_start_cnf);
                }
            }
            break;
        }

        default:
            break;
    } /* switch */
} /* g3mac_base_cb_cnf */
/******************************************************************************
   End of function  g3mac_base_cb_cnf
******************************************************************************/

/******************************************************************************
* Function Name:g3mac_base_cb_ind
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void g3mac_base_cb_ind (uint8_t * pbuff)
{
    uint8_t    idc;
    uint8_t    cmd;
    uint16_t   length;
    uint16_t   max_len = g3mac_info.cb_buff_size;
    r_result_t status;

    if ((NULL == pbuff) || (NULL == g3mac_info.pcb_str))
    {
        return;
    }
    idc = R_G3_GET_PKT_IDC (pbuff[0]);
    cmd = pbuff[1];

    switch (cmd)
    {
        case R_G3_CMDID_MCPS_DATA:
        {
            if (NULL != g3mac_info.cb[idc].R_G3MAC_McpsDataInd)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MCPS_DATA_IND, &pbuff[2], max_len, &g3mac_info.pcb_str->mcps_data_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    g3mac_info.cb[idc].R_G3MAC_McpsDataInd (&g3mac_info.pcb_str->mcps_data_ind);
                }
            }
            else
            {
                /* Do Nothing */
            }
            break;
        }

        case R_G3_CMDID_MLME_BEACON_NOTIFY:
        {
            if (NULL != g3mac_info.cb[idc].R_G3MAC_MlmeBeaconNotifyInd)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_BEACON_NOTIFY_IND, &pbuff[2], max_len, &g3mac_info.pcb_str->mlme_beacon_notify_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    g3mac_info.cb[idc].R_G3MAC_MlmeBeaconNotifyInd (&g3mac_info.pcb_str->mlme_beacon_notify_ind);
                }
            }
            break;
        }

        case R_G3_CMDID_MLME_COMM_STATUS:
        {
            if (NULL != g3mac_info.cb[idc].R_G3MAC_MlmeCommStatusInd)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_COMM_STATUS_IND, &pbuff[2], max_len, &g3mac_info.pcb_str->mlme_comm_status_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    g3mac_info.cb[idc].R_G3MAC_MlmeCommStatusInd (&g3mac_info.pcb_str->mlme_comm_status_ind);
                }
            }
            break;
        }

        case R_G3_CMDID_MLME_FRAMECOUNT:
        {
            if (NULL != g3mac_info.cb[idc].R_G3MAC_MlmeFrameCountInd)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_FRAMECOUNT_IND, &pbuff[2], max_len, &g3mac_info.pcb_str->mlme_frame_count_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    g3mac_info.cb[idc].R_G3MAC_MlmeFrameCountInd (&g3mac_info.pcb_str->mlme_frame_count_ind);
                }
            }
            break;
        }

        case R_G3_CMDID_MLME_TMR_RECEIVE:
        {
            if (NULL != g3mac_info.cb[idc].R_G3MAC_MlmeTmrReceiveInd)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_TMR_RECEIVE_IND, &pbuff[2], max_len, &g3mac_info.pcb_str->mlme_tmr_receve_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    g3mac_info.cb[idc].R_G3MAC_MlmeTmrReceiveInd (&g3mac_info.pcb_str->mlme_tmr_receve_ind);
                }
            }
            break;
        }

        case R_G3_CMDID_MLME_TMR_TRANSMIT:
        {
            if (NULL != g3mac_info.cb[idc].R_G3MAC_MlmeTmrTransmitInd)
            {
                status = R_G3MAC_CbBin2Str (R_G3_FUNCID_MLME_TMR_TRANSMIT_IND, &pbuff[2], max_len, &g3mac_info.pcb_str->mlme_tmr_transmit_ind, &length);
                if ((R_RESULT_SUCCESS == status) && (0u != length))
                {
                    g3mac_info.cb[idc].R_G3MAC_MlmeTmrTransmitInd (&g3mac_info.pcb_str->mlme_tmr_transmit_ind);
                }
            }
            break;
        }

        default:
            break;
    } /* switch */
} /* g3mac_base_cb_ind */
/******************************************************************************
   End of function  g3mac_base_cb_ind
******************************************************************************/

/******************************************************************************
* Function Name:g3mac_check_param
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t g3mac_check_param (uint8_t ch, uint16_t func_id, void * preq, uint8_t * preq_buff)
{
    uint8_t g3mode = R_G3CTRL_GetG3Mode (ch);

    if ((R_G3_CH_MAX  <= ch) || (NULL == preq))
    {
        return R_RESULT_BAD_INPUT_ARGUMENTS;
    }
    if (NULL == preq_buff)
    {
        return R_RESULT_INVALID_REQUEST;
    }

    if ((R_G3_FUNCID_MLME_GET_REQ == func_id) ||
        (R_G3_FUNCID_MLME_SET_REQ == func_id))
    {
        if ((R_G3_MODE_MAC > g3mode) || (R_G3_MODE_EAP < g3mode))
        {
            return R_RESULT_INVALID_REQUEST;
        }
    }
    else
    {
        if (R_G3_MODE_MAC != g3mode)
        {
            return R_RESULT_INVALID_REQUEST;
        }
    }

    return R_RESULT_SUCCESS;
} /* g3mac_check_param */
/******************************************************************************
   End of function  g3mac_check_param
******************************************************************************/

/******************************************************************************
* Function Name:g3mac_process_request
* Description : Main function for the processing of G3MAC requests. All commands
*               and functions are processed in the same way.
* Arguments : uint8_t ch: The channel used for communication
*             uint16_t func_id: The function ID indicating the request (e.g. R_G3_FUNCID_MCPS_DATA_REQ)
*             void *preq: A generic pointer containing information about the specific request
* Return Value : - R_RESULT_INVALID_REQUEST if the request is not performed in g3mode
*                - R_RESULT_SUCCESS if the conversion performed by R_G3MAC_ReqStr2Bin and
*                  sending of the command succeeds.
*                - R_RESULT_FAILED otherwise.
******************************************************************************/
static r_result_t g3mac_process_request (uint8_t ch, uint16_t func_id, void * preq)
{
    uint8_t *  preq_buff = g3mac_info.preq_bin;
    uint16_t   length    = 0u;
    uint16_t   max_len   = g3mac_info.req_buff_size - 2u;
    uint8_t    cmd       = (uint8_t)(func_id & 0xFFu);
    r_result_t status;

    status = g3mac_check_param (ch, func_id, preq, preq_buff);
    if (R_RESULT_SUCCESS != status)
    {
        return status;
    }

    status = R_G3CTRL_SemaphoreWait ();
    if (R_RESULT_SUCCESS == status)
    {
        status = R_G3MAC_ReqStr2Bin (func_id, preq, max_len, &preq_buff[2], &length);

        if (R_RESULT_SUCCESS == status)
        {
            preq_buff[0] = R_G3_SET_PKT_HEAD (ch, R_G3_CMDTYPE_REQ, R_G3_UNITID_G3MAC);
            preq_buff[1] = cmd;

            status       = R_G3CTRL_SendCmd (preq_buff, length + 2u);
        }

        R_G3CTRL_SemaphoreRelease ();
    }

    return status;
} /* g3mac_process_request */
/******************************************************************************
   End of function  g3mac_process_request
******************************************************************************/

