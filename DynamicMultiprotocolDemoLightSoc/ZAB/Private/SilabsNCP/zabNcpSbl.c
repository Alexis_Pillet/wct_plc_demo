/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the NCP Serial Boot Loader manager
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.011  19-Jul-16   MvdB   SBL Upgrades: Version checking, Failure management, Detect bootloader active during open
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 * 002.002.031  09-Jan-17   MvdB   ARTF172881: Avoid work() returning zero due to errors like szl not initialised
 * 002.002.039  16-Jan-17   MvdB   Support bootloading of SiLabs NCP 5.8.x (Changed to EMBER_PHY_RAIL)
 * 002.002.048  06-Feb-17   MvdB   Speed up OPENING notification on EFR32 ZAB_ACTION_EXIT_FIRMWARE_UPDATER
 *****************************************************************************/

#include "zabNcpPrivate.h"
#include "zabNcpService.h"
#include "zabNcpOpen.h"
#include "zabNcpXModem.h"
#include "zabNcpSbl.h"


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* Default value for Asks */
#define M_DEFAULT_UPDATE_LENGTH             ( 0 )
#define M_DEFAULT_UPDATE_POINTER            ( NULL )

/* Length of data in a write frame */
#define SILABS_WRITE_LENGTH                 ( 128 )

/* Timeout constants */
#define M_TIMEOUT_DISABLED                  ( 0xFFFFFFFF )

/* For format of EBL header item see ebl.h: eblHdr3xx_t */
#define EBLTAG_HEADER                       ( 0x0000 )
#define IMAGE_SIGNATURE                     ( 0xE350 )
#define EBL_HEADER_OFFSET                   ( 0 )
#define EBL_HEADER_TLV_SIZE                 ( 16 )
#define EBL_HEADER_OFFSET_TAG               ( 0 )
#define EBL_HEADER_OFFSET_LENGTH            ( 2 )
#define EBL_HEADER_OFFSET_VERSION           ( 4 )
#define EBL_HEADER_OFFSET_SIGNATURE         ( 6 )

/* For format of table, see \hal\micro\cortexm3\memmap-tables.h*/
#define IMAGE_STAMP_SIZE                    ( 8 )
#define IMAGE_INFO_MAXLEN                   ( 32 )
#define EBL_HEADER_TLV_SIZE                 ( 16 )
#define AAT_BASE_TABLE_SIZE                 ( 24 )
#define AAT_OFFSET_PLAT_INFO                ( EBL_HEADER_TLV_SIZE + AAT_BASE_TABLE_SIZE )
#define AAT_OFFSET_MICRO_INFO               ( AAT_OFFSET_PLAT_INFO + 1)
#define AAT_OFFSET_PHY_INFO                 ( AAT_OFFSET_MICRO_INFO + 1)
#define AAT_OFFSET_SW_VERSION               ( AAT_OFFSET_PHY_INFO + 2)
#define AAT_OFFSET_SW_BUILD                 ( AAT_OFFSET_SW_VERSION + 2)
#define AAT_OFFSET_IMAGE_INFO               ( AAT_OFFSET_SW_BUILD + 2 + 4)
#define AAT_OFFSET_CUST_APP_VER             ( AAT_OFFSET_IMAGE_INFO + IMAGE_INFO_MAXLEN + 4 + 12 + 4)
#define AAT_OFFSET_FAMILY_INFO              ( AAT_OFFSET_CUST_APP_VER + 4 + 4 + IMAGE_STAMP_SIZE)

/* Minimum size of header block required */
#define EBL_HEADER_MIN_SIZE                 ( AAT_OFFSET_FAMILY_INFO + 1 )

/* Minimum iamge length required */
#define EFR32_MINIMUM_IMAGE_LENGTH          ( EBL_HEADER_MIN_SIZE )

/* For defined values see \platform\base\hal\micro\micro-types.h */
#define EMBER_PLATFORM_CORTEXM3             ( 4 )
#define EMBER_MICRO_CORTEXM3_EFR32          ( 24 )
#define EMBER_PHY_EFR32                     ( 9 )
#define EMBER_PHY_RAIL                      ( 15 )
#define EMBER_FAMILY_EFR32MG1P              ( 16 )


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      *****************************
 *                 *****           MACROS            *****
 *                      *****************************
 ******************************************************************************/

/* Macros for quick access to SBL info */
#define ZAB_SERVICE_ZNP_SBL( _srv )  (ZAB_SERVICE_VENDOR( (_srv) )->zabSblInfo)
#define SBL_CURRENT_ITEM( _srv )     (ZAB_SERVICE_ZNP_SBL( (_srv) ).currentItem)
#define SBL_TIMEOUT( _srv )          (ZAB_SERVICE_ZNP_SBL( (_srv) ).timeoutExpiryMs)
#define SBL_IMAGE_LENGTH( _srv )     (ZAB_SERVICE_ZNP_SBL( (_srv) ).imageLength)
#define SBL_ADDRESS( _srv )          (ZAB_SERVICE_ZNP_SBL( (_srv) ).address)

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/* Prototypes */
static void checkStatusSetItem(erStatus* Status, zabService* Service, zabSblCurrentItem item, unsigned32 Timeout);

/******************************************************************************
 * Set the state machine inactive
 ******************************************************************************/
static void updateInactive(erStatus* Status, zabService* Service)
{
  SBL_CURRENT_ITEM(Service) = ZAB_SBL_CURRENT_ITEM_NONE;
  SBL_TIMEOUT(Service) = M_TIMEOUT_DISABLED;
  SBL_IMAGE_LENGTH(Service) = M_DEFAULT_UPDATE_LENGTH;
  SBL_ADDRESS(Service) = 0;
}

/******************************************************************************
 * Update completed successfully
 ******************************************************************************/
static void updateComplete(erStatus* Status, zabService* Service)
{
  updateInactive(Status, Service);
  sapMsgPutNotifyFwUpdateState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_FW_STATE_COMPLETE);
}

/******************************************************************************
 * Update failed
 ******************************************************************************/
static void updateFailed(erStatus* Status, zabService* Service)
{
  printVendor(Service, "updateFailed: Item %d\n", SBL_CURRENT_ITEM(Service));

  if (SBL_CURRENT_ITEM(Service) == ZAB_SBL_CURRENT_ITEM_WRITING)
    {
      zabNcpXModem_EndFileTransfer(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_SBL_CURRENT_ITEM_WRITE_FAILED_EXIT_XMODEM, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
    }
  else if (SBL_CURRENT_ITEM(Service) != ZAB_SBL_CURRENT_ITEM_FAILED_GET_PROMPT)
    {
      zabNcpXModem_Reset(Status, Service);
      zabNcpXModem_SendCarriageReturn(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_SBL_CURRENT_ITEM_FAILED_GET_PROMPT, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
    }
  else
    {
      updateInactive(Status, Service);
      sapMsgPutNotifyFwUpdateState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_FW_STATE_FAILED);
      sapMsgPutNotifyFwUpdateState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_FW_STATE_NONE);
    }
}

/******************************************************************************
 * Set item if status is good
 * Otherwise graceful harakiri
 ******************************************************************************/
static void checkStatusSetItem(erStatus* Status, zabService* Service, zabSblCurrentItem item, unsigned32 Timeout)
{
  if (erStatusIsOk(Status))
    {
      SBL_CURRENT_ITEM(Service) = item;

      if (Timeout == M_TIMEOUT_DISABLED)
        {
          SBL_TIMEOUT(Service) = M_TIMEOUT_DISABLED;
        }
      else
        {
          osTimeGetMilliseconds(Service, &SBL_TIMEOUT(Service));
          SBL_TIMEOUT(Service) += Timeout;

          /* Handle the (very unlikely) case our timeout works out to the disabled value */
          if (SBL_TIMEOUT(Service) == M_TIMEOUT_DISABLED)
            {
              SBL_TIMEOUT(Service)++;
            }
        }
    }
  else
    {
      sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
      updateFailed(Status, Service);
    }
}

/******************************************************************************
 * Perform the next chunk of the write
 ******************************************************************************/
static void updateNextChunk(erStatus* Status, zabService* Service)
{
  unsigned8 imageBuffer[SILABS_WRITE_LENGTH];
  unsigned32 bufferSize;
  unsigned16 progress;

  if (SBL_ADDRESS(Service) < SBL_IMAGE_LENGTH(Service))
    {
      printVendor(Service, "updateNextChunk: Addr %d of %d\n", SBL_ADDRESS(Service), SBL_IMAGE_LENGTH(Service));
      /* Get the next chunk of data to be written at offset = SBL_ADDRESS */
      bufferSize = srvCoreAskBackBuffer(Status, Service, ZAB_ASK_FIRMWARE_IMAGE_DATA,
                                        SBL_ADDRESS(Service),
                                        SILABS_WRITE_LENGTH,
                                        imageBuffer);
      if (bufferSize < SILABS_WRITE_LENGTH)
        {
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ASKED_VALUE_INVALID);
          updateFailed(Status, Service);
        }

      zabNcpXModem_SendBlock(Status, Service, imageBuffer);
      SBL_ADDRESS(Service) += SILABS_WRITE_LENGTH;

      progress = ((unsigned64)SBL_ADDRESS(Service) << 8) / SBL_IMAGE_LENGTH(Service);
      if (progress > 255)
        {
          progress = 255;
        }
      sapMsgPutNotifyFwUpdateProgress(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, (unsigned8)progress);

      checkStatusSetItem(Status, Service, ZAB_SBL_CURRENT_ITEM_WRITING, ZAB_VND_CFG_M_SBL_BLOCK_ACK_TIMEOUT_MS);
    }
  else
    {
      printVendor(Service, "updateNextChunk: Finish\n");
      // Finished
      zabNcpXModem_EndFileTransfer(Status, Service);
      checkStatusSetItem(Status, Service, ZAB_SBL_CURRENT_ITEM_WRITE_COMPLETE, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
    }
}

/******************************************************************************
 * Start the firmware update process
 ******************************************************************************/
static void updateFirmware(erStatus* Status, zabService* Service)
{
  unsigned32 imageLength;
  unsigned8 imageBuffer[EBL_HEADER_MIN_SIZE];
  unsigned32 bufferSize;
  unsigned16 tag;            /* = EBLTAG_HEADER              */
  unsigned16 len;
  unsigned16 version;        /* Version of the ebl format    */
  unsigned16 signature;      /* Magic signature: 0xE350      */
  ER_CHECK_STATUS(Status);

  imageLength = srvCoreAskBack32(Status, Service, ZAB_ASK_FIRMWARE_IMAGE_LENGTH, M_DEFAULT_UPDATE_LENGTH);
  if (imageLength > EFR32_MINIMUM_IMAGE_LENGTH)
    {
      bufferSize = srvCoreAskBackBuffer(Status, Service, ZAB_ASK_FIRMWARE_IMAGE_DATA,
                                        EBL_HEADER_OFFSET,
                                        EBL_HEADER_MIN_SIZE,
                                        imageBuffer);

      if (bufferSize >= EBL_HEADER_MIN_SIZE)
        {
          /* Direct tag data is big endian */
          tag = (imageBuffer[EBL_HEADER_OFFSET_TAG] << 8) | imageBuffer[EBL_HEADER_OFFSET_TAG+1];
          len = (imageBuffer[EBL_HEADER_OFFSET_LENGTH] << 8) | imageBuffer[EBL_HEADER_OFFSET_LENGTH+1];
          version = (imageBuffer[EBL_HEADER_OFFSET_VERSION] << 8) | imageBuffer[EBL_HEADER_OFFSET_VERSION+1];
          signature = (imageBuffer[EBL_HEADER_OFFSET_SIGNATURE] << 8) | imageBuffer[EBL_HEADER_OFFSET_SIGNATURE+1];
          printVendor(Service, "tag = 0x%04X\n", tag);
          printVendor(Service, "len = %d\n", len);
          printVendor(Service, "version = 0x%04X\n", version); // So far this is 0x0201
          printVendor(Service, "signature = 0x%04X\n", signature);

          /* AAT data is little endian */
          printVendor(Service, "platInfo = %d (4 = CORTEXM3)\n", imageBuffer[AAT_OFFSET_PLAT_INFO]);
          printVendor(Service, "microInfo = %d (24 = CORTEXM3_EFR32)\n", imageBuffer[AAT_OFFSET_MICRO_INFO]);
          printVendor(Service, "phyInfo = %d (9 = EFR32, 15 = RAIL)\n", imageBuffer[AAT_OFFSET_PHY_INFO]);
          printVendor(Service, "familyInfo = %d (16 = EFR32MG1P)\n", imageBuffer[AAT_OFFSET_FAMILY_INFO]);

          /* Validate Image */
          if ( (tag == EBLTAG_HEADER) &&
               (len >= EBL_HEADER_MIN_SIZE) &&
               (signature == IMAGE_SIGNATURE) &&
               (imageBuffer[AAT_OFFSET_PLAT_INFO] == EMBER_PLATFORM_CORTEXM3) &&
               (imageBuffer[AAT_OFFSET_MICRO_INFO] == EMBER_MICRO_CORTEXM3_EFR32) &&
               ( (imageBuffer[AAT_OFFSET_PHY_INFO] == EMBER_PHY_EFR32) || (imageBuffer[AAT_OFFSET_PHY_INFO] == EMBER_PHY_RAIL) ) &&
               (imageBuffer[AAT_OFFSET_FAMILY_INFO] == EMBER_FAMILY_EFR32MG1P) )
            {
              /* At this point we still have possible issues with:
               *  Is the image an NCP?
               *  Is it for the correct FLASH size?
               *  Is it for the correct package?
               *  Is it a firefly part with integrated serial FLASH?
               * We live with this for now. I don't want to modify the bootloader or standard images until it becomes necessary and requirements more clear */
              printVendor(Service, "softwareVersion = %04X\n", COPY_IN_16_BITS(&imageBuffer[AAT_OFFSET_SW_VERSION]));
              printVendor(Service, "softwareBuild = %d\n", COPY_IN_16_BITS(&imageBuffer[AAT_OFFSET_SW_BUILD]));
              printVendor(Service, "imageInfo = %s\n", &imageBuffer[AAT_OFFSET_IMAGE_INFO]);
              printVendor(Service, "customerApplicationVersion = %08X\n", COPY_IN_32_BITS(&imageBuffer[AAT_OFFSET_CUST_APP_VER]));

              SBL_IMAGE_LENGTH(Service) = imageLength;
              SBL_ADDRESS(Service) = 0;
              sapMsgPutNotifyFwUpdateProgress(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, 0);
              sapMsgPutNotifyFwUpdateState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_FW_STATE_UPDATING);

              zabNcpXModem_InitialiseUpload(Status, Service);
              checkStatusSetItem(Status, Service, ZAB_SBL_CURRENT_ITEM_HANDSHAKING, ZAB_VND_CFG_M_SBL_READY_TIMEOUT_MS);
            }
          else
            {
              sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_BOOTLOADER_IMAGE_INVALID);
              updateFailed(Status, Service);
            }
        }
    }
  else
    {
      sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ASKED_VALUE_INVALID);
      updateFailed(Status, Service);
    }
}


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Create
 * Initialises SBL state machine
 ******************************************************************************/
void zabNcpSbl_Create(erStatus* Status, zabService* Service)
{
  updateInactive(Status, Service);
  sapMsgPutNotifyFwUpdateProgress(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, 0);
  sapMsgPutNotifyFwUpdateState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_FW_STATE_NONE);
}

/******************************************************************************
 * Update timeout
 * Check for timeouts within the state machine
 ******************************************************************************/
unsigned32 zabNcpSbl_UpdateTimeout(erStatus* Status, zabService* Service, unsigned32 Time)
{
  unsigned32 timeoutRemaining = ZAB_WORK_DELAY_MAX_MS;
  ER_CHECK_STATUS_VALUE(Status, timeoutRemaining);
  ER_CHECK_NULL_VALUE(Status, Service, timeoutRemaining);

  if (SBL_TIMEOUT(Service) != M_TIMEOUT_DISABLED)
    {
      /* Check timeout while handling wrapped time*/
      timeoutRemaining = SBL_TIMEOUT(Service) - Time;
      if ( (timeoutRemaining == 0) || (timeoutRemaining > ZAB_VND_MS_TIMEOUT_MAX) )
        {
          timeoutRemaining = 0;

          if (SBL_CURRENT_ITEM(Service) == ZAB_SBL_CURRENT_ITEM_WRITE_RUN_WAIT)
            {
              zabNcpOpen_Action(Status, Service, ZAB_ACTION_OPEN);
              updateComplete(Status, Service);
            }
          else if (SBL_CURRENT_ITEM(Service) == ZAB_SBL_CURRENT_ITEM_EXIT_RUN_WAIT)
            {
              updateInactive(Status, Service);
              zabNcpOpen_Action(Status, Service, ZAB_ACTION_OPEN);
            }
          else
            {
              updateFailed(Status, Service);
            }
        }
    }
  return timeoutRemaining;
}

/******************************************************************************
 * Action Handler
 * Handles Update action for the SBL state machines
 ******************************************************************************/
void zabNcpSbl_Action(erStatus* Status, zabService* Service, unsigned8 Action)
{
  ER_CHECK_STATUS(Status);
  ER_CHECK_NULL(Status, Service);

  switch(Action)
    {
      /*
      ** Firmware Update
      */
      case ZAB_ACTION_UPDATE_FIRMWARE:
        if (SBL_CURRENT_ITEM(Service) == ZAB_SBL_CURRENT_ITEM_NONE)
          {
            if (zabNcpOpen_GetOpenState(Service) == ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE)
              {
                updateFirmware(Status, Service);
              }
            else
              {
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
                sapMsgPutNotifyFwUpdateState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_FW_STATE_NONE);
              }
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_BOOTLOADER_ACTION_IN_PROGRESS);
          }
        break;

      /*
      ** Exit Firmware Updater
      */
      case ZAB_ACTION_EXIT_FIRMWARE_UPDATER:
        if (SBL_CURRENT_ITEM(Service) == ZAB_SBL_CURRENT_ITEM_NONE)
          {
            if (zabNcpOpen_GetOpenState(Service) == ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE)
              {
                /* Start the app */
                zabNcpXModem_Run(Status, Service);

                /* Give a short timeout, then get moving with open */
                checkStatusSetItem(Status, Service, ZAB_SBL_CURRENT_ITEM_EXIT_RUN_WAIT, 500);
              }
            else
              {
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
                sapMsgPutNotifyFwUpdateState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_FW_STATE_NONE);
              }
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_BOOTLOADER_ACTION_IN_PROGRESS);
          }
        break;
    }
}

/******************************************************************************
 * Process an incoming XModem response
 ******************************************************************************/
void zabNcpSbl_XModemResponseHandler(erStatus* Status, zabService* Service, zabSblResponse_t SblResponse)
{
  printVendor(Service, "zabNcpSbl_XModemResponseHandler: Item %d Resp %d\n", SBL_CURRENT_ITEM(Service), (unsigned8)SblResponse);

  if ( (SBL_CURRENT_ITEM(Service) == ZAB_SBL_CURRENT_ITEM_HANDSHAKING) &&
       (SblResponse == ZAB_SBL_RESPONSE_CLEAR) )
    {
      updateNextChunk(Status, Service);
    }
  else if (SBL_CURRENT_ITEM(Service) == ZAB_SBL_CURRENT_ITEM_WRITING)
    {
      if (SblResponse == ZAB_SBL_RESPONSE_ACK)
        {
          updateNextChunk(Status, Service);
        }
      else
        {
          updateFailed(Status, Service);
        }
    }
  else if (SBL_CURRENT_ITEM(Service) ==  ZAB_SBL_CURRENT_ITEM_WRITE_FAILED_EXIT_XMODEM)
    {
      updateFailed(Status, Service);
    }
  else if (SBL_CURRENT_ITEM(Service) == ZAB_SBL_CURRENT_ITEM_WRITE_COMPLETE)
    {
      if (SblResponse == ZAB_SBL_RESPONSE_ACK)
        {
          zabNcpXModem_SendCarriageReturn(Status, Service);
          checkStatusSetItem(Status, Service, ZAB_SBL_CURRENT_ITEM_WRITE_AWAIT_PROMPT, ZAB_VND_CFG_M_SBL_READY_TIMEOUT_MS);
        }
      else
        {
          updateFailed(Status, Service);
        }
    }
  else if (SBL_CURRENT_ITEM(Service) == ZAB_SBL_CURRENT_ITEM_WRITE_AWAIT_PROMPT)
    {
      if (SblResponse == ZAB_SBL_RESPONSE_PROMPT_READY)
        {
          zabNcpXModem_Run(Status, Service);
          checkStatusSetItem(Status, Service, ZAB_SBL_CURRENT_ITEM_WRITE_RUN_WAIT, ZAB_VND_CFG_M_SBL_READY_TIMEOUT_MS);
        }
      else
        {
          updateFailed(Status, Service);
        }
    }

  else if (SBL_CURRENT_ITEM(Service) == ZAB_SBL_CURRENT_ITEM_FAILED_GET_PROMPT)
    {
      if (SblResponse == ZAB_SBL_RESPONSE_PROMPT_READY)
        {
          updateFailed(Status, Service);
        }
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
