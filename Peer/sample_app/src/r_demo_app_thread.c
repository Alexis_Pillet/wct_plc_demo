/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_app_thread.c
*    @version
*        $Rev: 3384 $
*    @last editor
*        $Author: a5089752 $
*    @date  
*        $Date:: 2017-05-31 13:49:31 +0900#$
* Description : 
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include "r_typedefs.h"
#include "r_config.h"
#include "r_bsp_api.h"
#include "r_byte_swap.h"
#include "r_memory_api.h"
#include "r_queue_api.h"

#include "r_timer_api.h"

/* g3 part */
#include "r_c3sap_api.h"

/* app part */
#include "r_demo_app.h"
#include "r_demo_app_eap.h"
#include "r_demo_app_thread.h"
#include "r_demo_api.h"


/******************************************************************************
Macro definitions
******************************************************************************/
#define R_DEMO_APP_QUEUE_SIZE           (8)
#define R_DEMO_APP_MSG_SIZE             (1500)

/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Private global variables and functions
******************************************************************************/
static r_queue_t            demoAppQueue;                                                       //!< Queue for demo application
static r_queue_element_t    demoAppQueueArray[R_DEMO_APP_QUEUE_SIZE];                           //!< Array for demo application queue
static uint8_t              demoAppMsgsMemory[R_DEMO_APP_MSG_SIZE * R_DEMO_APP_QUEUE_SIZE];     //!< Memory for demo application queue



static void app_process_thread(void);
static void thread_app_timeout(uint8_t handle);

/* Timer related static variables */
static r_timer_oneshot_table_t app_timer_table[MAX_USED_APP_TIMER_HANDLES];
static uint8_t                 app_active_check[(MAX_USED_APP_TIMER_HANDLES / 8) + 1];

/******************************************************************************
Exported global variables
******************************************************************************/

/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/
volatile    r_boolean_t     g_app_timeout_detect;

/******************************************************************************
Functions
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AppThreadInit
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AppThreadInit(void)
{
    /* Clear timer table */
    R_TIMER_TimerOneShotOffRange(R_TIMER_ID_APP, HANDLE_APP_FIRST, HANDLE_APP_LAST);

    if (R_TIMER_AssignOneShotTimer(R_TIMER_ID_APP, app_timer_table, MAX_USED_APP_TIMER_HANDLES, app_active_check, sizeof(app_active_check), &thread_app_timeout) != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }

    /* Initialize demo application queue. */
    if (R_QUEUE_Create(&demoAppQueue,
                       demoAppQueueArray,
                       R_DEMO_APP_QUEUE_SIZE) != R_QUEUE_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }
        
    /* Initialize a memory pool for the UART rx */
    if (R_MEMORY_Init(R_MEM_INSTANCE_DEMO, demoAppMsgsMemory, R_DEMO_APP_MSG_SIZE * R_DEMO_APP_QUEUE_SIZE)  != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }

    /* Configure application processing thread. */
    if (R_BSP_ConfigureCMTimer(R_PCLKB_HZ, R_APP_THREAD_PERIOD, R_CMT_APP_IPR, R_BSP_CMT_TIMER_ID_1, &app_process_thread) != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }
    
    /* Start application processing thread. */ 
    R_BSP_CMTimerOn(R_BSP_CMT_TIMER_ID_1);
    
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_DEMO_AppThreadInit
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppThreadEnqueDataInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppThreadEnqueDataInd(const r_adp_adpd_data_ind_t* ind)
{
    uint8_t*            pAddr;


    r_adp_adpd_data_ind_t dataIndLocal;

    pAddr = R_MEMORY_Malloc(R_MEM_INSTANCE_DEMO, (uint16_t)(sizeof(r_adp_adpd_data_ind_t) + ind->nsduLength));

    /* Copy data indication structure. */
    dataIndLocal.linkQualityIndicator = ind->linkQualityIndicator;
    dataIndLocal.nsduLength           = ind->nsduLength;
    dataIndLocal.pNsdu                = pAddr + sizeof(r_adp_adpd_data_ind_t);

    /* Obtain the next receive buffer */
    if (NULL != pAddr)
    {
        R_memcpy(pAddr, (uint8_t*) &dataIndLocal, sizeof(r_adp_adpd_data_ind_t));
        R_memcpy(pAddr + sizeof(r_adp_adpd_data_ind_t), ind->pNsdu, ind->nsduLength);

        R_QUEUE_Enqueue(&demoAppQueue, pAddr, (uint16_t)(sizeof(r_adp_adpd_data_ind_t) + ind->nsduLength), R_DEMO_APP_HANDLE_DATA_IND);

    }
}
/******************************************************************************
   End of function  R_DEMO_AppThreadEnqueDataInd
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AppThreadEnqueMacDataInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppThreadEnqueMacDataInd(const r_g3mac_mcps_data_ind_t* ind)
{
    uint8_t*            pAddr;

    r_g3mac_mcps_data_ind_t dataIndLocal;


    pAddr = R_MEMORY_Malloc(R_MEM_INSTANCE_DEMO, (uint16_t)(sizeof(r_g3mac_mcps_data_ind_t) + ind->msduLength));

    /* Copy data indication structure. */
    dataIndLocal = *ind;
    dataIndLocal.pMsdu                 = pAddr + sizeof(r_g3mac_mcps_data_ind_t);

    /* Obtain the next receive buffer */
    if (NULL != pAddr)
    {
        R_memcpy(pAddr, (uint8_t*) &dataIndLocal, sizeof(r_g3mac_mcps_data_ind_t));
        R_memcpy(pAddr + sizeof(r_g3mac_mcps_data_ind_t), ind->pMsdu, ind->msduLength);

        R_QUEUE_Enqueue(&demoAppQueue, pAddr, (uint16_t)(sizeof(r_g3mac_mcps_data_ind_t) + ind->msduLength), R_DEMO_APP_HANDLE_MAC_DATA_IND);

    }
}
/******************************************************************************
   End of function  R_DEMO_AppThreadEnqueMacDataInd
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AppThreadEnqueInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppThreadEnqueInd(const uint8_t* indPtr,
                                         const uint8_t handle,
                                         const uint16_t size)
{
    uint8_t*            pAddr;


    if ((size > 0) &&
        (NULL == indPtr))
    {
        return;
    }

    if (size > 0)
    {
        pAddr = R_MEMORY_Malloc(R_MEM_INSTANCE_DEMO, size);

        /* Obtain the next receive buffer */
        if (NULL != pAddr)
        {
            R_memcpy(pAddr, indPtr, size);
        }

        R_QUEUE_Enqueue(&demoAppQueue, pAddr, size, handle);

    }
    else
    {
        R_QUEUE_Enqueue(&demoAppQueue, NULL, 0, handle);

    }
}
/******************************************************************************
   End of function  R_DEMO_AppThreadEnqueInd
******************************************************************************/


/******************************************************************************
* Function Name: app_process_thread
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void app_process_thread(void)
{ 
    r_queue_element_t deque;
    
    r_queue_result_t  queueReturn;

    /* Stop application processing thread. */
    R_BSP_CMTimerOff(R_BSP_CMT_TIMER_ID_1);

    /* Enable interrupts. */
    R_BSP_EnableInterrupt();

    /* Check if something has been dequeued */
    do
    {
        /* Try to dequeue a new message. */
        queueReturn = R_QUEUE_Dequeue(&demoAppQueue, &deque);
        if (R_QUEUE_RESULT_SUCCESS == queueReturn)

        {
            switch (deque.handle)
            {
                case R_DEMO_APP_HANDLE_DATA_IND:
                    R_DEMO_AppHandleDataIndication((const r_adp_adpd_data_ind_t*) deque.pdata);
                    break;
                case R_DEMO_APP_HANDLE_LEAVE_IND:
                    R_DEMO_AppHandleLeaveIndication();
                    break;
                case R_DEMO_APP_HANDLE_BUFFER_IND:
                    R_DEMO_AppHandleBufferInd((const r_adp_adpm_buffer_ind_t*) deque.pdata);
                    break;
                case R_DEMO_APP_HANDLE_STATUS_IND:
                    R_DEMO_AppHandleStatusInd((const r_adp_adpm_network_status_ind_t*) deque.pdata);
                    break;
                case R_DEMO_APP_HANDLE_LBP_IND:
                    break;
                case R_DEMO_APP_HANDLE_PATH_DIS_IND:
                    R_DEMO_AppHandlePathDiscInd((r_adp_adpm_path_discovery_ind_t*) deque.pdata);
                    break;
                case R_DEMO_APP_HANDLE_REBOOT_REQUEST_IND:
                    R_DEMO_AppHandleSysRebootReqInd((r_sys_rebootreq_ind_t *) deque.pdata);
                    break;
                case R_DEMO_APP_HANDLE_FRAMECOUNT_IND:
                    R_DEMO_AppHandleFrameCntInd((r_adp_adpm_framecounter_ind_t*) deque.pdata);
                    break;
                case R_DEMO_APP_HANDLE_EAP_NETWORKJOIN_IND:
                    R_DEMO_AppHandleEapNwkJoinInd((r_eap_eapm_network_join_ind_t*) deque.pdata);
                    break;
                case R_DEMO_APP_HANDLE_EAP_NETWORKLEAVE_IND:
                    R_DEMO_AppHandleEapNwkLeaveInd((r_eap_eapm_network_leave_ind_t*) deque.pdata);
                    break;
                case R_DEMO_APP_HANDLE_EAP_NEWDEVICE_IND:
                    R_DEMO_AppHandleEapNewDeviceInd((r_eap_eapm_newdevice_ind_t*) deque.pdata);
                    break;
                case R_DEMO_APP_HANDLE_MAC_DATA_IND:
                    R_DEMO_AppHandleMcpsDataInd((r_g3mac_mcps_data_ind_t*) deque.pdata);
                    break;
                case R_DEMO_APP_HANDLE_ADP_ROUTE_UPDATE_IND:
                    R_DEMO_AppHandleRouteInd((const r_adp_adpm_route_update_ind_t*) deque.pdata);
                    break;
                case R_DEMO_APP_HANDLE_ADP_LOAD_SEQ_NUM_IND:
                    R_DEMO_AppHandleLoadInd((const r_adp_adpm_load_seq_num_ind_t*) deque.pdata);
                    break;
                case R_DEMO_APP_HANDLE_MAC_TMR_RCV_IND:
                    R_DEMO_AppHandleMacTmrRcvInd((const r_g3mac_mlme_tmr_receive_ind_t*) deque.pdata);
                    break;
                case R_DEMO_APP_HANDLE_MAC_TMR_TRANSMIT_IND:
                    R_DEMO_AppHandleMacTmrTransmitInd((const r_g3mac_mlme_tmr_transmit_ind_t*) deque.pdata);
                    break;
                case R_DEMO_APP_HANDLE_ADP_RREP_IND:
                    R_DEMO_AppHandleRrepInd((const r_adp_adpm_rrep_ind_t*) deque.pdata);
                    break;
                default:
                    break;
            }

            /* Free memory. */
            R_MEMORY_Free(R_MEM_INSTANCE_DEMO, deque.pdata);
        }
    }
    while (R_QUEUE_RESULT_SUCCESS == queueReturn);

    R_BSP_CMTimerOn(R_BSP_CMT_TIMER_ID_1);

}
/******************************************************************************
   End of function  app_process_thread
******************************************************************************/

/******************************************************************************
* Function Name: thread_app_timeout
* Description :Function for enqueuing a timeout event
* Arguments : 
* Return Value : 
******************************************************************************/
static void thread_app_timeout(uint8_t handle)
{
    /* Enqueue timer event if not HANDLE_DONT_CARE */
    if (HANDLE_APP_DONT_CARE != handle)
    {
        g_app_timeout_detect = R_TRUE;
    }
}
/******************************************************************************
   End of function  thread_app_timeout
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_WaitcnfTimerOn
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_WaitcnfTimerOn()
{
    R_TIMER_TimerOneShotOn( R_TIMER_ID_APP,R_APP_WAIT_CNF_TIMEOUT*1000, HANDLE_APP_CMN_REQUEST );
    g_app_timeout_detect = R_FALSE;
}
/******************************************************************************
   End of function  R_DEMO_WaitcnfTimerOn
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_WaitcnfTimerOff
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_WaitcnfTimerOff()
{
    R_TIMER_TimerOneShotOff( R_TIMER_ID_APP, HANDLE_APP_CMN_REQUEST );
}
/******************************************************************************
   End of function  R_DEMO_WaitcnfTimerOff
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_WaitcnfTimeout
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_boolean_t R_DEMO_WaitcnfTimeout()
{
    return (r_boolean_t)g_app_timeout_detect;
}
/******************************************************************************
   End of function  R_DEMO_WaitcnfTimeout
******************************************************************************/
