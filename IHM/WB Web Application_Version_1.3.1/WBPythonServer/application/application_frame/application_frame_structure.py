
"""
application.application_frame.application_frame_structure
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to store the structure of applicative frames

"""
from enum import IntEnum, IntFlag


class Application:
    """Contains all possible values of all fields of applicative frames"""

    @staticmethod
    def display_ack():
        """Return a string to display an user friendly log"""
        return {Application.AckStatus.SUCCESS: 'SUCCESS',
                Application.AckStatus.ALREADY_RX: 'ALREADY_RX',
                Application.AckStatus.INVALID_COMMAND: 'INVALID_COMMAND',
                Application.AckStatus.ERROR_NO_ACK: 'ERROR_NO_ACK',
                Application.AckStatus.SUCCESS_NO_ACK: 'SUCCESS_NO_ACK'}

    class Interface(IntEnum):
        """Interface field values"""
        RESERVED = 0x00
        ZIGBEE = 0x01
        GREEN_POWER = 0x02

    class CommandID(IntEnum):
        """Command ID field values"""
        DEFAULT_RESPONSE = 0x00
        GET_RESPONSE = 0x01
        SET_RESPONSE = 0x02
        UPDATE_DEVICE_DATA = 0x10
        UPDATE_DEVICE_LINK_INDICATOR = 0x11
        CLUSTER_COMMAND = 0x20
        GET_COMMAND = 0x21
        SET_COMMAND = 0x22

    class OptionAckMask(IntFlag):
        """Masks to obtain specific option of option field"""
        SOURCE_ADDRESS_PRESENT = 0x01
        SOURCE_ADDRESS_TYPE = 0x06
        SOURCE_EP_PRESENT = 0x08
        PAYLOAD_PRESENT = 0x10
        RESERVED = 0xE0

    class OptionUpStreamMask(IntFlag):
        """Masks to obtain specific option of option field"""
        RESPONSE_ENABLE = 0x01
        SOURCE_ADDRESS_TYPE = 0x06
        SOURCE_EP_PRESENT = 0x08
        RESERVED = 0xF0

    class OptionDownStreamMask(IntFlag):
        """Masks to obtain specific option of option field"""
        RESPONSE_ENABLE = 0x01
        SOURCE_EP_PRESENT = 0x02
        DESTINATION_ADDRESS_TYPE = 0x0C
        DESTINATION_EP_PRESENT = 0x10
        DIRECTION = 0x20
        MS_FLAG = 0x40
        RESERVED = 0x80

    class OptionResponse(IntEnum):
        """Option Response field values"""
        DISABLE = 0x00
        ENABLE = 0x01

    class OptionAddressType(IntEnum):
        """Option Address type field values"""
        SOURCE_ID_ADDRESS = 0x00
        IEEE_ADDRESS = 0x02

    class OptionAddress(IntEnum):
        """Option Address Present field values"""
        NOT_PRESENT = 0x00
        PRESENT = 0x01

    class OptionPayload(IntEnum):
        """Option Payload Present field values"""
        NOT_PRESENT = 0x00
        PRESENT = 0x01

    class OptionEP(IntEnum):
        """Option End Point Present field values"""
        NOT_PRESENT = 0x00
        PRESENT = 0x01

    class OptionDirection(IntEnum):
        """Option Direction field values"""
        CLIENT_TO_SERVER = 0x00
        SERVER_TO_CLIENT = 0x01

    class OptionMS(IntEnum):
        """Option MS Flag field values"""
        NORMALIZED = 0x00
        SCHNEIDER = 0x01

    class AckStatus(IntEnum):
        """Ack Status field values"""
        SUCCESS = 0x00
        ALREADY_RX = 0x01
        INVALID_COMMAND = 0x02
        SUCCESS_NO_ACK = 0x03
        ERROR_NO_ACK = 0x04
