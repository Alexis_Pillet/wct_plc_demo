# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to manage the network layout
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from queue import Queue, Empty
from threading import Lock, Thread
from apscheduler.schedulers.background import BackgroundScheduler
from .network_frame import *
from datetime import datetime, timedelta
from logger import get_logger
import types


class NetworkProcess(object):

    def __init__(self):
        self._debug = False
        # Thread
        self._receiver_alive = None
        self._receiver_thread = None
        # Read/Write management
        self._local_network_sequence_number = 0
        self._external_network_sequence_number = -1
        # Connection to mac process
        self._queue = Queue()
        self._mac_frame_generator = None
        # Send command/data management
        self._application_queue = None
        self._network_frame_generator = None
        self._mutex = Lock()
        self._mutex_no_multiple_send = Lock()
        self._network_scheduler = BackgroundScheduler()
        self._dict_network_frame_in_progress = []

    @property
    def queue(self):
        return self._queue

    @queue.setter
    def queue(self, queue):
        self._queue = queue

    @property
    def application_queue(self):
        return self._application_queue

    @application_queue.setter
    def application_queue(self, application_queue):
        self._application_queue = application_queue

    @property
    def mac_frame_generator(self):
        return self._mac_frame_generator

    @mac_frame_generator.setter
    def mac_frame_generator(self, mac_frame_generator):
        self._mac_frame_generator = mac_frame_generator

    @property
    def network_frame_generator(self):
        return self._network_frame_generator

    @network_frame_generator.setter
    def network_frame_generator(self, frame_generator):
        self._network_frame_generator = frame_generator

    # Activate/Deactivate debug
    def set_debug(self, value):
        self._debug = value

    def _start_receiver(self):
        """Start reader thread"""
        self._receiver_alive = True
        self._receiver_thread = Thread(target=self._receiver, name='network_rx', args=())
        self._receiver_thread.daemon = True
        self._receiver_thread.start()

    def _stop_receiver(self):
        """Stop reader thread only, wait for clean exit of thread"""
        self._receiver_alive = False

    def start(self):
        """start worker thread"""
        self._start_receiver()
        # Start network frame generator
        self._network_frame_generator = NetworkFrameGenerator(self)
        # Start the scheduler
        self._network_scheduler.start()

    def stop(self):
        """set flag to stop worker threads"""
        self._stop_receiver()
        # remove jobs and stop scheduler
        self._network_scheduler.remove_all_jobs()
        self._network_scheduler.shutdown()

    def join(self):
        """wait for worker thread to terminate"""
        self._receiver_thread.join()

    def _receiver(self):
        """loop and copy serial->console"""

        while self._receiver_alive:

            try:
                data = self._queue.get(True, 0.1)
            except Empty:
                continue

            # We sent a command we are waiting for an acknowledgment
            # Check if this is an acknowledgment
            result = self._parse_ack_frame(data["frame"])
            # Report to applicative
            if result["is_ack"]:
                # Check if the ack corresponding to a command/data frames sent
                if self._network_scheduler.get_job(str(result["network_sequence_number"])) is not None:
                    # Remove process of sending the command/data frame
                    self._network_scheduler.remove_job(str(result["network_sequence_number"]))
                    # remove current frame
                    self._dict_network_frame_in_progress.pop(0)
                    # Check if there are job pending => if yes start it
                    if len(self._dict_network_frame_in_progress) >= 1:
                        self._network_scheduler.resume_job(self._dict_network_frame_in_progress[0].id_job)
                        self._network_scheduler.modify_job(self._dict_network_frame_in_progress[0].id_job,
                                                           next_run_time=(datetime.now() + timedelta(milliseconds=10)))
                    # logger
                    if self._debug:
                        if result["ack_status"] != NetworkAckStatus.ACK_SUCCESS.value:
                                get_logger().error('NETWORK_ACK_RECEIVED - {}'
                                                   .format(NetworkAckStatusDisplay()
                                                           .status_string_display[result["ack_status"]]))
            # Check if this is a data frame
            # We can receive a command while we are waiting for an acknowledgment
            # We need still need to treat it
            else:
                result = self._parse_data_frame(data["frame"])
                if result["is_data_control_frame"]:
                    # Send data in applicative queue
                    self._application_queue.put({"device_number": data["device_number"], "frame": data["frame"][3:]})
                    if self._debug:
                        get_logger().debug('NETWORK : DATA - {}'.format(data["frame"].hex()))
                # Send acknowledgment
                self._send_ack(data["device_number"], result["ack_status_to_send"])
                # logger
                if result["ack_status_to_send"] == NetworkAckStatus.SEND_NO_ACK.value:
                    get_logger().debug('NETWORK_ACK_TO_SEND - {}'
                                       .format(NetworkAckStatusDisplay()
                                               .status_string_display[result["ack_status_to_send"]]))
                elif result["ack_status_to_send"] != NetworkAckStatus.ACK_SUCCESS.value:
                    get_logger().error('NETWORK_ACK_TO_SEND - {}'
                                       .format(NetworkAckStatusDisplay()
                                               .status_string_display[result["ack_status_to_send"]]))

            self._queue.task_done()

    # Return (is_ack, network_sequence_number, result)
    def _parse_ack_frame(self, data):

        is_ack = False
        network_sequence_number = -1
        ack_status = NetworkAckStatus.SEND_NO_ACK.value

        # Frame is an ACk ?
        if int(data[0]) == NetworkFrameControl.ACK.value:
            # Check if we are waiting an acknowledgment
            if len(self._dict_network_frame_in_progress) >= 1:
                # Check network sequence number  => Same as previous
                if int(data[1]) == self._dict_network_frame_in_progress[0].local_network_sequence_number:
                    is_ack = True
                    network_sequence_number = int(data[1])
                    ack_status = int(data[2])

        return {"is_ack": is_ack, "network_sequence_number": network_sequence_number, "ack_status": ack_status}

    # Return (Report_To_Applicative, ACK Status To Send)
    def _parse_data_frame(self, data):

        is_data_control_frame = False

        # Compare two frames
        if int(data[0]) == NetworkFrameControl.DATA.value:
            # Check network sequence number
            if int(data[1]) != self._external_network_sequence_number:

                if (int(data[2]) & NetworkOptionMask.MASK_RESERVED.value) == 0x00:
                    # Save network sequence number
                    self._external_network_sequence_number = int(data[1])
                    # Good frame
                    is_data_control_frame = True
                    # Check if the bit "ACK Response" is set or not
                    # If this is set, we don't send an acknowledgement
                    if (int(data[2]) & NetworkOptionMask.MASK_OPTION_ACK.value) == NetworkOptionAck.ACK_RESPONSE.value:
                        ack_status_to_send = NetworkAckStatus.ACK_SUCCESS.value
                    else:
                        ack_status_to_send = NetworkAckStatus.SEND_NO_ACK.value
                # Wrong option field => drop fame
                else:
                    ack_status_to_send = NetworkAckStatus.SEND_NO_ACK.value
            # Wrong network sequence number
            else:
                ack_status_to_send = NetworkAckStatus.ACK_ALREADY_RX.value
        # ACK
        elif int(data[1]) == NetworkFrameControl.ACK.value:
            ack_status_to_send = NetworkAckStatus.SEND_NO_ACK.value
        # Wrong Frame
        else:
            ack_status_to_send = NetworkAckStatus.ACK_INVALID_COMMAND.value

        return {"is_data_control_frame": is_data_control_frame, "ack_status_to_send": ack_status_to_send}

    def _send_ack(self, device_number, result):

        # Don't need to send an acknowledgment when received an other acknowledgment
        if result != NetworkAckStatus.SEND_NO_ACK.value:

            # Create frame
            frame = bytearray([NetworkFrameControl.ACK.value, self._external_network_sequence_number])

            # Add payload of the frame
            frame.append(result)

            # Send network frame to mac layout
            self._mac_frame_generator.send_data_frame(device_number, frame)

    def send_frame(self, frame_control, option, device_number, payload):

        # Increment frame counter
        if self._local_network_sequence_number == 255:
            self._local_network_sequence_number = 0
        else:
            self._local_network_sequence_number += 1

        # Create frame
        frame = bytearray([frame_control, self._local_network_sequence_number, option])

        # Add payload of the frame
        for element in payload:
            frame.append(element)

        # Save number_retry for this transaction
        metadata_frame = types.SimpleNamespace()
        metadata_frame.local_network_sequence_number = self._local_network_sequence_number
        metadata_frame.id_job = str(self._local_network_sequence_number)
        metadata_frame.number_retry = 0
        self._dict_network_frame_in_progress.append(metadata_frame)

        # Schedule the jobs to send the command/data frame
        self._network_scheduler.add_job(self._sender, trigger='interval',
                                        args=(frame, device_number,), seconds=1.0,
                                        id=str(self._local_network_sequence_number))
        # Store the job
        self._network_scheduler.pause_job(metadata_frame.id_job)

        # If no job are running => start it
        if len(self._dict_network_frame_in_progress) == 1:
            self._network_scheduler.resume_job(self._dict_network_frame_in_progress[0].id_job)
            self._network_scheduler.modify_job(self._dict_network_frame_in_progress[0].id_job,
                                               next_run_time=(datetime.now() + timedelta(milliseconds=10)))

    def _sender(self, frame, device_number):

        # We don't send multiple frame at one
        self._mutex_no_multiple_send.acquire()

        try:

            # We are trying to send the frame 3 times
            if self._dict_network_frame_in_progress[0].number_retry < 3:

                # increase retry counter
                self._dict_network_frame_in_progress[0].number_retry += 1
                # Send network frame to mac layout
                self._mac_frame_generator.send_data_frame(device_number, frame)

            else:
                # Send error to applicative
                # Remove process of sending the command/data frame
                self._network_scheduler.remove_job(self._dict_network_frame_in_progress[0].id_job)
                self._dict_network_frame_in_progress.pop(0)
                # Check if there are job pending => if yes start it
                if len(self._dict_network_frame_in_progress) >= 1:
                    self._network_scheduler.resume_job(self._dict_network_frame_in_progress[0].id_job)
                    self._network_scheduler.modify_job(self._dict_network_frame_in_progress[0].id_job,
                                                       next_run_time=datetime.now())
                pass

        except Exception:
            self._receiver_alive = False
            raise

        # We finish to send the current frame
        self._mutex_no_multiple_send.release()
