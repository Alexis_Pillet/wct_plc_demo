/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_status2text.c
*    @version
*        $Rev: 3282 $
*    @last editor
*        $Author: a0202438 $
*    @date
*        $Date:: 2017-05-18 15:50:39 +0900#$
* Description :
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include "r_typedefs.h"
#include "r_stdio_api.h"

/* g3 part */
#include "r_c3sap_api.h"
#include "r_demo_status2text.h"

#include "r_lml_statistics.h"
#include "r_g3mac_statistics.h"
#include "r_adp_statistics.h"
#include "r_eap_statistics.h"

#include "r_uif_statistics.h"

/******************************************************************************
Macro definitions
******************************************************************************/
#define STATUS_CASE(id, msg)  case R_##id##_STATUS_##msg: \
        pRet = #msg; break;

#define STATUS_CASE2(id)      case id: \
        ret = #id; break;

#define IBID_GEN(id, size)    case R_##id: \
        * txt = #id; * len = size; break;

#define STATS_TXT(id, msg)    case R_##id##_STATS_##msg: \
        pRet = #msg; break;

/******************************************************************************
Typedef definitions
******************************************************************************/
/******************************************************************************
Private global variables and functions
******************************************************************************/
/******************************************************************************
Exported global variables
******************************************************************************/
/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/
/******************************************************************************
Functions
******************************************************************************/



/******************************************************************************
* Function Name: status_to_text_adp
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static const char *status_to_text_adp (uint32_t status)
{
    const char * pRet;

    switch (status)
    {
        STATUS_CASE (ADP, SUCCESS)
        STATUS_CASE (ADP, COUNTER_ERROR)
        STATUS_CASE (ADP, IMPROPER_KEY_TYPE)
        STATUS_CASE (ADP, IMPROPER_SECURITY_LEVEL)
        STATUS_CASE (ADP, UNSUPPORTED_LEGACY)
        STATUS_CASE (ADP, UNSUPPORTED_SECURITY)
        STATUS_CASE (ADP, CHANNEL_ACCESS_FAILURE)
        STATUS_CASE (ADP, SECURITY_ERROR)
        STATUS_CASE (ADP, FRAME_TOO_LONG)
        STATUS_CASE (ADP, INVALID_HANDLE)
        STATUS_CASE (ADP, INVALID_PARAMETER)
        STATUS_CASE (ADP, NO_ACK)
        STATUS_CASE (ADP, NO_BEACON)
        STATUS_CASE (ADP, NO_DATA)
        STATUS_CASE (ADP, NO_SHORT_ADDRESS)
        STATUS_CASE (ADP, OUT_OF_CAP)
        STATUS_CASE (ADP, ALTERNATE_PANID_DETECTION)
        STATUS_CASE (ADP, UNAVAILABLE_KEY)
        STATUS_CASE (ADP, UNSUPPORTED_ATTRIBUTE)
        STATUS_CASE (ADP, INVALID_ADDRESS)
        STATUS_CASE (ADP, INVALID_INDEX)
        STATUS_CASE (ADP, LIMIT_REACHED)
        STATUS_CASE (ADP, READ_ONLY)
        STATUS_CASE (ADP, SCAN_IN_PROGRESS)
        STATUS_CASE (ADP, MAC_INVALID_STATE)
        STATUS_CASE (ADP, MAC_NO_RESPONSE)
        STATUS_CASE (ADP, INVALID_REQUEST)
        STATUS_CASE (ADP, NOT_PERMITTED)
        STATUS_CASE (ADP, INVALID_IPV6_FRAME)
        STATUS_CASE (ADP, ROUTE_ERROR)
        STATUS_CASE (ADP, INCOMPLETE_PATH)
        STATUS_CASE (ADP, ALREADY_IN_PROGRESS)
        STATUS_CASE (ADP, FAILED)
        STATUS_CASE (ADP, TIMEOUT)
        STATUS_CASE (ADP, REQ_QUEUE_FULL)
        STATUS_CASE (ADP, INSUFFICIENT_MEMSIZE)
        default:
            pRet = "FATAL UNDEFINED ERROR";
            break;
    } /* switch */

    return pRet;
} /* status_to_text_adp */
/******************************************************************************
   End of function  status_to_text_adp
******************************************************************************/


/******************************************************************************
* Function Name: status_to_text_eap
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static const char *status_to_text_eap (uint32_t status)
{
    const char * pRet;

    switch (status)
    {
        STATUS_CASE (EAP, SUCCESS)
        STATUS_CASE (EAP, INVALID_REQUEST)
        STATUS_CASE (EAP, CONFIG_ERROR)
        STATUS_CASE (EAP, TIMEOUT)
        STATUS_CASE (EAP, REQ_QUEUE_FULL)
        STATUS_CASE (EAP, UNSUPPORTED_ATTRIBUTE)
        STATUS_CASE (EAP, INVALID_INDEX)
        STATUS_CASE (EAP, READ_ONLY)
        STATUS_CASE (EAP, EAP_PSK_IN_PROGRESS)
        STATUS_CASE (EAP, BLACKLISTED_DEVICE)
        STATUS_CASE (EAP, EAP_PSK_FAILURE)
        STATUS_CASE (EAP, JOIN_DISCARD)
        STATUS_CASE (EAP, INSUFFICIENT_MEMSIZE)
        default:
            pRet = "FATAL UNDEFINED ERROR";
            break;
    } /* switch */

    return pRet;
} /* status_to_text_eap */
/******************************************************************************
   End of function  status_to_text_eap
******************************************************************************/



/******************************************************************************
* Function Name: status_to_text_mac
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static const char *status_to_text_mac (uint8_t status)
{
    const char * pRet;
    switch (status)
    {
        STATUS_CASE (G3MAC, SUCCESS)
        STATUS_CASE (G3MAC, COUNTER_ERROR)
        STATUS_CASE (G3MAC, IMPROPER_KEY_TYPE)
        STATUS_CASE (G3MAC, IMPROPER_SECURITY_LEVEL)
        STATUS_CASE (G3MAC, UNSUPPORTED_LEGACY)
        STATUS_CASE (G3MAC, UNSUPPORTED_SECURITY)
        STATUS_CASE (G3MAC, CHANNEL_ACCESS_FAILURE)
        STATUS_CASE (G3MAC, SECURITY_ERROR)
        STATUS_CASE (G3MAC, FRAME_TOO_LONG)
        STATUS_CASE (G3MAC, INVALID_HANDLE)
        STATUS_CASE (G3MAC, INVALID_PARAMETER)
        STATUS_CASE (G3MAC, NO_ACK)
        STATUS_CASE (G3MAC, NO_BEACON)
        STATUS_CASE (G3MAC, NO_DATA)
        STATUS_CASE (G3MAC, NO_SHORT_ADDRESS)
        STATUS_CASE (G3MAC, OUT_OF_CAP)
        STATUS_CASE (G3MAC, ALTERNATE_PANID_DETECTION)
        STATUS_CASE (G3MAC, UNAVAILABLE_KEY)
        STATUS_CASE (G3MAC, UNSUPPORTED_ATTRIBUTE)
        STATUS_CASE (G3MAC, INVALID_ADDRESS)
        STATUS_CASE (G3MAC, INVALID_INDEX)
        STATUS_CASE (G3MAC, LIMIT_REACHED)
        STATUS_CASE (G3MAC, READ_ONLY)
        STATUS_CASE (G3MAC, SCAN_IN_PROGRESS)
        STATUS_CASE (G3MAC, INVALID_STATE)
        STATUS_CASE (G3MAC, NO_RESPONSE)
        default:
            pRet = "MAC UNDEFINED STATUS";
            break;
    } /* switch */

    return pRet;
} /* status_to_text_mac */
/******************************************************************************
   End of function  status_to_text_mac
******************************************************************************/


/******************************************************************************
* Function Name: status_to_text_ctrl
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static const char *status_to_text_ctrl (uint8_t status)
{
    const char * pRet;
    switch (status)
    {
        STATUS_CASE (G3, SUCCESS)
        STATUS_CASE (G3, INVALID_REQUEST)
        STATUS_CASE (G3, INSUFFICIENT_MEMSIZE)
        STATUS_CASE (G3, NO_RESPONSE)
        STATUS_CASE (G3, IF_FATAL_ERROR)
        STATUS_CASE (G3, IF_TIMEOUT)
        STATUS_CASE (G3, IF_QUEUE_FULL)
        STATUS_CASE (G3, IF_INVALID_STATE)
        STATUS_CASE (G3, IF_INVALID_PARAMETER)
        STATUS_CASE (G3, IF_INVALID_RESPONSE)
        STATUS_CASE (G3, IF_NO_RESPONSE)
        STATUS_CASE (G3, INVALID_PARAMETER)
        STATUS_CASE (G3, INVALID_STATE)
        default:
            pRet = "MAC UNDEFINED STATUS";
            break;
    } /* switch */

    return pRet;
} /* status_to_text_ctrl */
/******************************************************************************
   End of function  status_to_text_ctrl
******************************************************************************/




/******************************************************************************
* Function Name: status_to_text
* Description :
* Arguments :
* Return Value :
******************************************************************************/
const char *status_to_text (uint8_t id, uint8_t status)
{

    if (R_G3_MODE_MAC == id)
    {
        return status_to_text_mac (status);
    }
    else if (R_G3_MODE_ADP == id)
    {
        return status_to_text_adp (status);
    }
    else if (R_G3_MODE_EAP == id)
    {
        return status_to_text_eap (status);
    }
    else if (0 == id)
    {
        return status_to_text_ctrl (status);
    }
    else
    {
        return "UNDEFINED ID or below layer status";
    }
} /* status_to_text */
/******************************************************************************
   End of function  status_to_text
******************************************************************************/




/******************************************************************************
* Function Name: ibid_to_text_mac
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static uint32_t ibid_to_text_mac (uint16_t id, const char ** txt, uint8_t * len)
{
    uint32_t ret = 0;
    switch (id)
    {
        IBID_GEN (G3MAC_IB_ACKWAIT_DURATION, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_MAX_BE, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_BSN, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_DSN, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_MAX_CSMABACKOFFS, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_MIN_BE, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_PANID, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_PROMISCUOUS_MODE, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_SHORTADDRESS, APL_LENGTH_16)
        IBID_GEN (G3MAC_IB_MAX_FRAME_RETRIES, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_TIMESTAMP_SUPPORTED, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_SECURITY_ENABLED, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_KEY_TABLE, APL_LENGTH_TABLE)
        IBID_GEN (G3MAC_IB_DEVICE_TABLE, APL_LENGTH_TABLE)
        IBID_GEN (G3MAC_IB_FRAME_COUNTER, APL_LENGTH_32)
        IBID_GEN (G3MAC_IB_HIGHPRIORITY_WINDOWSIZE, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_TXDATAPACKET_COUNT, APL_LENGTH_32)
        IBID_GEN (G3MAC_IB_RXDATAPACKET_COUNT, APL_LENGTH_32)
        IBID_GEN (G3MAC_IB_TXCMDPACKET_COUNT, APL_LENGTH_32)
        IBID_GEN (G3MAC_IB_RXCMDPACKET_COUNT, APL_LENGTH_32)
        IBID_GEN (G3MAC_IB_CSMAFAIL_COUNT, APL_LENGTH_32)
        IBID_GEN (G3MAC_IB_CSMANOACK_COUNT, APL_LENGTH_32)
        IBID_GEN (G3MAC_IB_RXDATABROADCAST_COUNT, APL_LENGTH_32)
        IBID_GEN (G3MAC_IB_TXDATABROADCAST_COUNT, APL_LENGTH_32)
        IBID_GEN (G3MAC_IB_BADCRC_COUNT, APL_LENGTH_32)
        IBID_GEN (G3MAC_IB_NEIGHBOUR_TABLE, APL_LENGTH_TABLE)
        IBID_GEN (G3MAC_IB_CSMA_FAIRNESS_LIMIT, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_TMR_TTL, APL_LENGTH_32)
        IBID_GEN (G3MAC_IB_POS_TABLE_ENTRY_TTL, APL_LENGTH_32)
        IBID_GEN (G3MAC_IB_RCCOORD, APL_LENGTH_16)
        IBID_GEN (G3MAC_IB_TONEMASK, APL_LENGTH_TABLE)
        IBID_GEN (G3MAC_IB_BEACON_RAND_WIN_LENGTH, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_A, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_K, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_MINCWATTEMPTS, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_CENELEC_LEGACY_MODE, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_FCC_LEGACY_MODE, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_BROADCAST_MAX_CW_EANBLE, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_TRANSMIT_ATTEN, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_POS_TABLE, APL_LENGTH_TABLE)

        IBID_GEN (G3MAC_IB_COHERENT_TRANSMISSION, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_NEIGHBOUR_TABLE_SIZE, APL_LENGTH_16)
        IBID_GEN (G3MAC_IB_DEVICE_TABLE_SIZE, APL_LENGTH_16)
        IBID_GEN (G3MAC_IB_VALID_NEITABLE_ENTRIES, APL_LENGTH_16)
        IBID_GEN (G3MAC_IB_VALID_DEVTABLE_ENTRIES, APL_LENGTH_16)
        IBID_GEN (G3MAC_IB_NEIGHBOUR_TABLE_BY_SHORT_ADDR, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_KEY_VALIDATE, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_EXTADDRESS, APL_LENGTH_TABLE)
        IBID_GEN (G3MAC_IB_SOFT_VERSION, APL_LENGTH_TABLE)
        IBID_GEN (G3MAC_IB_UNICAST_DATA_TX_TIMEOUT, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_BROADCAST_DATA_TX_TIMEOUT, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_BEACON_REQUEST_TX_TIMEOUT, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_TX_GAIN, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_FRAME_CNT_IND_INTERVAL, APL_LENGTH_32)
        IBID_GEN (G3MAC_IB_DEVICE_TABLE_BY_SHORT_ADDR, APL_LENGTH_TABLE)
        IBID_GEN (G3MAC_IB_NEIGHBOUR_INDEX_BY_SHORT_ADDR, APL_LENGTH_16)
        IBID_GEN (G3MAC_IB_DEVICE_INDEX_BY_SHORT_ADDR, APL_LENGTH_16)
        IBID_GEN (G3MAC_IB_VALID_POSTABLE_ENTRIES, APL_LENGTH_16)
        IBID_GEN (G3MAC_IB_POS_TABLE_BY_SHORT_ADDR, APL_LENGTH_16)
        IBID_GEN (G3MAC_IB_TMR_IND_ENABLE, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_KEEP_MOD_RETRY_NUM, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_NEI_UPDATE_AFTER_RETRANSMIT, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_COMM_STATUS_IND_MASK, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_TMR_REQ_LEAD_TIME_BEF_TTL, APL_LENGTH_8)

        IBID_GEN (G3MAC_IB_OFFSET_SNR, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_THRESH_CARRIER_NUM, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_THRESH_TONENUM_PER_MAP, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_THRESH_SNR_DIFFERENTIAL, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_THRESH_SNR_COHERENT, APL_LENGTH_8)

        IBID_GEN (G3MAC_IB_STATISTICS, APL_LENGTH_TABLE)

        IBID_GEN (G3MAC_IB_PHY_TX_POWER, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_PHY_TX_FILTER_SCALE, APL_LENGTH_16)
        IBID_GEN (G3MAC_IB_PHY_TX_DIGITAL_PREAMBLE_GAIN, APL_LENGTH_16)
        IBID_GEN (G3MAC_IB_PHY_TX_DIGITAL_GAIN, APL_LENGTH_16)
        IBID_GEN (G3MAC_IB_PHY_TXENB_POLARITY, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_PHY_TX_WAIT_TIME, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_PHY_TX_BREAK, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_PHY_TX_ACK_GAIN, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_PHY_SATT_CTRL_DISABLE, APL_LENGTH_8)
        IBID_GEN (G3MAC_IB_PHY_AC_PHASE_OFFSET, APL_LENGTH_16)
        IBID_GEN (G3MAC_IB_PHY_STATISTICS, APL_LENGTH_TABLE)


        default:
            ret = 1;
            break;
    } /* switch */

    return ret;
} /* ibid_to_text_mac */
/******************************************************************************
   End of function  ibid_to_text_mac
******************************************************************************/


/******************************************************************************
* Function Name: ibid_to_text_adp
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static uint32_t ibid_to_text_adp (uint16_t id, const char ** txt, uint8_t * len)
{
    uint32_t ret = 0;
    switch (id)
    {
        IBID_GEN (ADP_IB_SECURITY_LEVEL, APL_LENGTH_8)
        IBID_GEN (ADP_IB_PREFIX_TABLE, APL_LENGTH_TABLE)
        IBID_GEN (ADP_IB_BROADCAST_LOG_TABLE_ENTRY_TTL, APL_LENGTH_16)
        IBID_GEN (ADP_IB_METRIC_TYPE, APL_LENGTH_8)
        IBID_GEN (ADP_IB_LOW_LQI_VALUE, APL_LENGTH_8)
        IBID_GEN (ADP_IB_HIGH_LQI_VALUE, APL_LENGTH_8)
        IBID_GEN (ADP_IB_RREP_WAIT, APL_LENGTH_8)
        IBID_GEN (ADP_IB_CONTEXT_INFORMATION_TABLE, APL_LENGTH_TABLE)
        IBID_GEN (ADP_IB_COORD_SHORT_ADDRESS, APL_LENGTH_16)
        IBID_GEN (ADP_IB_RLC_ENABLED, APL_LENGTH_8)
        IBID_GEN (ADP_IB_ADD_REV_LINK_COST, APL_LENGTH_8)
        IBID_GEN (ADP_IB_BROADCAST_LOG_TABLE, APL_LENGTH_TABLE)
        IBID_GEN (ADP_IB_ROUTING_TABLE, APL_LENGTH_TABLE)
        IBID_GEN (ADP_IB_UNICAST_RREQ_GEN_ENABLE, APL_LENGTH_8)
        IBID_GEN (ADP_IB_GROUP_TABLE, APL_LENGTH_TABLE)
        IBID_GEN (ADP_IB_MAX_HOPS, APL_LENGTH_8)
        IBID_GEN (ADP_IB_DEVICE_TYPE, APL_LENGTH_8)
        IBID_GEN (ADP_IB_NET_TRAVERSAL_TIME, APL_LENGTH_16)
        IBID_GEN (ADP_IB_ROUTING_TABLE_ENTRY_TTL, APL_LENGTH_16)
        IBID_GEN (ADP_IB_KR, APL_LENGTH_8)
        IBID_GEN (ADP_IB_KM, APL_LENGTH_8)
        IBID_GEN (ADP_IB_KC, APL_LENGTH_8)
        IBID_GEN (ADP_IB_KQ, APL_LENGTH_8)
        IBID_GEN (ADP_IB_KH, APL_LENGTH_8)
        IBID_GEN (ADP_IB_RREQ_RETRIES, APL_LENGTH_8)
        IBID_GEN (ADP_IB_RREQ_WAIT, APL_LENGTH_8)
        IBID_GEN (ADP_IB_WEAK_LQI_VALUE, APL_LENGTH_8)
        IBID_GEN (ADP_IB_KRT, APL_LENGTH_8)
        IBID_GEN (ADP_IB_SOFT_VERSION, APL_LENGTH_TABLE)
        IBID_GEN (ADP_IB_BLACKLIST_TABLE, APL_LENGTH_TABLE)
        IBID_GEN (ADP_IB_BLACKLIST_TABLE_ENTRY_TTL, APL_LENGTH_16)
        IBID_GEN (ADP_IB_MAX_JOIN_WAIT_TIME, APL_LENGTH_16)
        IBID_GEN (ADP_IB_PATH_DISCOVERY_TIME, APL_LENGTH_16)
        IBID_GEN (ADP_IB_ACTIVE_KEY_INDEX, APL_LENGTH_8)
        IBID_GEN (ADP_IB_DESTINATION_ADDRESS_SET, APL_LENGTH_TABLE)
        IBID_GEN (ADP_IB_DEFAULT_COORD_ROUTE_ENABLED, APL_LENGTH_8)
        IBID_GEN (ADP_IB_DISABLE_DEFAULT_ROUTING, APL_LENGTH_8)

        /* RENESAS ORIGINAL */
        IBID_GEN (ADP_IB_LOAD_SEQ_NUMBER, APL_LENGTH_16)
        IBID_GEN (ADP_IB_ROUTE_TABLE_BY_ADDR, APL_LENGTH_TABLE)
        IBID_GEN (ADP_IB_ROUTE_TABLE_SIZE, APL_LENGTH_16)
        IBID_GEN (ADP_IB_VALID_RTABLE_ENTRIES, APL_LENGTH_16)
        IBID_GEN (ADP_IB_ROUTE_INDEX_BY_ADDR, APL_LENGTH_16)
        IBID_GEN (ADP_IB_ROUTE_IND_ENABLE, APL_LENGTH_8)
        IBID_GEN (ADP_IB_LOAD_SEQ_NUM_IND_INTERVAL, APL_LENGTH_16)
        IBID_GEN (ADP_IB_BEACON_IND_ENABLE, APL_LENGTH_8)
        IBID_GEN (ADP_IB_BUFF_IND_DISABLE, APL_LENGTH_8)
        IBID_GEN (ADP_IB_RREP_IND_ENABLE, APL_LENGTH_8)

        IBID_GEN (ADP_IB_DATATYPE, APL_LENGTH_8)
        IBID_GEN (ADP_IB_ENABLE_DATATRANS, APL_LENGTH_8)
        IBID_GEN (ADP_IB_DISABLE_RELAY, APL_LENGTH_8)

        IBID_GEN (ADP_IB_STATISTICS, APL_LENGTH_TABLE)

        default:
            ret = 1;
            break;
    } /* switch */

    return ret;
} /* ibid_to_text_adp */
/******************************************************************************
   End of function  ibid_to_text_adp
******************************************************************************/



/******************************************************************************
* Function Name: ibid_to_text_eap
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static uint32_t ibid_to_text_eap (uint16_t id, const char ** txt, uint8_t * len)
{
    uint32_t ret = 0;
    switch (id)
    {
        IBID_GEN (EAP_IB_GMK, 16)
        IBID_GEN (EAP_IB_ACTIVEKEYINDEX, APL_LENGTH_8)
        IBID_GEN (EAP_IB_COORDSHORTADDRESS, APL_LENGTH_16)
        IBID_GEN (EAP_IB_CINFOTABLEENTRIES, APL_LENGTH_16)
        IBID_GEN (EAP_IB_CLIENTINFOTABLE, APL_LENGTH_TABLE)
        IBID_GEN (EAP_IB_JOIN_WAITTIMESEC, APL_LENGTH_16)
        IBID_GEN (EAP_IB_JOINGMKID, APL_LENGTH_8)
        IBID_GEN (EAP_IB_DISABLE_JOIN, APL_LENGTH_8)
        IBID_GEN (EAP_IB_EAPPSK_RETRYNUM, APL_LENGTH_8)
        IBID_GEN (EAP_IB_NWK_WAITTIMESEC, APL_LENGTH_16)
        IBID_GEN (EAP_IB_ONETIME_CLIENTINFO, APL_LENGTH_8)
        IBID_GEN (EAP_IB_STATISTICS, APL_LENGTH_TABLE)

        default:
            ret = 1;
            break;
    } /* switch */

    return ret;
} /* ibid_to_text_eap */
/******************************************************************************
   End of function  ibid_to_text_eap
******************************************************************************/


/******************************************************************************
* Function Name: ibid_to_text
* Description :
* Arguments :
* Return Value :
******************************************************************************/
const char *ibid_to_text (uint8_t Layer, uint16_t id, uint8_t * len)
{
    const char * pTxt = NULL;
    uint32_t     ret  = 1;

    if (R_G3_MODE_MAC == Layer)
    {
        ret = ibid_to_text_mac (id, &pTxt, len);
    }
    else if (R_G3_MODE_ADP == Layer)
    {
        ret = ibid_to_text_adp (id, &pTxt, len);
    }
    else if (R_G3_MODE_EAP == Layer)
    {
        ret = ibid_to_text_eap (id, &pTxt, len);
    }
    else
    {
        /**/
    }

    if (ret)
    {
        *len = 0xFF;
        pTxt = "Unknown Symbol";
    }
    return pTxt;
} /* ibid_to_text */
/******************************************************************************
   End of function  ibid_to_text
******************************************************************************/



/******************************************************************************
* Function Name: statsindex_to_text_lml
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static const char *statsindex_to_text_lml (uint32_t index)
{
    const char * pRet;

    switch (index)
    {
        STATS_TXT (LML, IDX_NUM_FRAMES_SND)
        STATS_TXT (LML, IDX_NUM_FRAMES_RCV)
        STATS_TXT (LML, IDX_NUM_SYNCM)
        STATS_TXT (LML, IDX_NUM_FCH_OK)
        STATS_TXT (LML, IDX_NUM_FCH_ERROR)
        STATS_TXT (LML, IDX_NUM_MACHDR_ERROR)
        STATS_TXT (LML, IDX_NUM_FCS_ERROR)
        STATS_TXT (LML, IDX_NUM_OVERWRITE)
        STATS_TXT (LML, IDX_NUM_ACK_SND)
        STATS_TXT (LML, IDX_NUM_NACK_SND)
        STATS_TXT (LML, IDX_NUM_ACK_RCV)
        STATS_TXT (LML, IDX_NUM_NACK_RCV)
        default:
            pRet = NULL;
            break;
    } /* switch */

    return pRet;
} /* statsindex_to_text_lml */
/******************************************************************************
   End of function  statsindex_to_text_lml
******************************************************************************/


/******************************************************************************
* Function Name: statsindex_to_text_mac
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static const char *statsindex_to_text_mac (uint32_t index)
{
    const char * pRet;

    switch (index)
    {
        STATS_TXT (G3MAC, TXDATAPACKETCOUNT)
        STATS_TXT (G3MAC, RXDATAPACKETCOUNT)
        STATS_TXT (G3MAC, TXCMDPACKETCOUNT)
        STATS_TXT (G3MAC, RXCMDPACKETCOUNT)
        STATS_TXT (G3MAC, CSMAFAILCOUNT)
        STATS_TXT (G3MAC, CSMANOACKCOUNT)
        STATS_TXT (G3MAC, TXDATABROADCASTCOUNT)
        STATS_TXT (G3MAC, RXDATABROADCASTCOUNT)
        STATS_TXT (G3MAC, BADCRCCOUNT)
        STATS_TXT (G3MAC, ALL_IN_RECEIVES)
        STATS_TXT (G3MAC, ALL_IN_DISCARDS)
        STATS_TXT (G3MAC, ALL_OUT_TRANSMITS)
        STATS_TXT (G3MAC, ALL_OUT_SUCCESS)
        STATS_TXT (G3MAC, ALL_OUT_FAILURE)
        STATS_TXT (G3MAC, ALL_OUT_FAILURE_LR)
        STATS_TXT (G3MAC, DATA_IN_RECEIVES)
        STATS_TXT (G3MAC, DATA_IN_RECEIVES_BCAST)
        STATS_TXT (G3MAC, DATA_IN_REASM_REQDS)
        STATS_TXT (G3MAC, DATA_IN_REASM_DISCARDS)
        STATS_TXT (G3MAC, DATA_IN_REASM_OKS)
        STATS_TXT (G3MAC, DATA_IN_DISCARDS_DEVICETABLE)
        STATS_TXT (G3MAC, DATA_IN_DISCARDS_SECURITY)
        STATS_TXT (G3MAC, DATA_IN_DELIVERS_UNICAST)
        STATS_TXT (G3MAC, DATA_IN_DELIVERS_BCAST)
        STATS_TXT (G3MAC, DATA_OUT_TRANSMITS)
        STATS_TXT (G3MAC, DATA_OUT_TRANSMITS_BCAST)
        STATS_TXT (G3MAC, DATA_OUT_RETRANSMISSION)
        STATS_TXT (G3MAC, DATA_OUT_SUCCESS)
        STATS_TXT (G3MAC, DATA_OUT_FAILURE)
        STATS_TXT (G3MAC, DATA_OUT_BCAST_SUCCESS)
        STATS_TXT (G3MAC, DATA_OUT_BCAST_FAILURE)
        STATS_TXT (G3MAC, CMD_IN_BCNREQ)
        STATS_TXT (G3MAC, CMD_IN_BCN)
        STATS_TXT (G3MAC, CMD_IN_TMREQ)
        STATS_TXT (G3MAC, CMD_IN_TMRES)
        STATS_TXT (G3MAC, CMD_OUT_BCNREQ_TRANSMITS)
        STATS_TXT (G3MAC, CMD_OUT_BCNREQ_SUCCESS)
        STATS_TXT (G3MAC, CMD_OUT_BCNREQ_FAILLURE)
        STATS_TXT (G3MAC, CMD_OUT_BCN_TRANSMITS)
        STATS_TXT (G3MAC, CMD_OUT_BCN_SUCCESS)
        STATS_TXT (G3MAC, CMD_OUT_BCN_FAILLURE)
        STATS_TXT (G3MAC, CMD_OUT_TMRES_TRANSMITS)
        STATS_TXT (G3MAC, CMD_OUT_TMRES_SUCCESS)
        STATS_TXT (G3MAC, CMD_OUT_TMRES_FAILLURE)
        STATS_TXT (G3MAC, MCPS_DATA_REQD)
        STATS_TXT (G3MAC, MCPS_DATA_SUCCESS)
        STATS_TXT (G3MAC, MCPS_DATA_FAILURE)
        STATS_TXT (G3MAC, MCPS_DATA_FAILURE_NOACK)
        STATS_TXT (G3MAC, MCPS_DATA_FAILURE_CSMAFAILURE)
        STATS_TXT (G3MAC, MCPS_DATA_FAILURE_LMLABORT)
        STATS_TXT (G3MAC, MCPS_DATA_IND)
        STATS_TXT (G3MAC, MCPS_TMR_RECEIVE_IND)
        STATS_TXT (G3MAC, MCPS_TMR_TRANSMIT_IND)
        STATS_TXT (G3MAC, MCPS_SUCCESS_UC_ROBUST)
        STATS_TXT (G3MAC, MCPS_SUCCESS_UC_BPSK)
        STATS_TXT (G3MAC, MCPS_SUCCESS_UC_QPSK)
        STATS_TXT (G3MAC, MCPS_SUCCESS_UC_8PSK)
        STATS_TXT (G3MAC, MCPS_FAILURE_UC_ROBUST)
        STATS_TXT (G3MAC, MCPS_FAILURE_UC_BPSK)
        STATS_TXT (G3MAC, MCPS_FAILURE_UC_QPSK)
        STATS_TXT (G3MAC, MCPS_FAILURE_UC_8PSK)
        STATS_TXT (G3MAC, MCPS_SUCCESS_CSMA_NCW3)
        STATS_TXT (G3MAC, MCPS_SUCCESS_CSMA_NCW4)
        STATS_TXT (G3MAC, MCPS_SUCCESS_CSMA_NCW5)
        STATS_TXT (G3MAC, MCPS_SUCCESS_CSMA_NCW6)
        STATS_TXT (G3MAC, MCPS_SUCCESS_CSMA_NCW7)
        STATS_TXT (G3MAC, MCPS_SUCCESS_CSMA_NCW8)
        STATS_TXT (G3MAC, MCPS_SUCCESS_CSMA_NCW9)
        STATS_TXT (G3MAC, MCPS_SUCCESS_CSMA_NCW10)
        STATS_TXT (G3MAC, MCPS_FAILURE_CSMA_NCW3)
        STATS_TXT (G3MAC, MCPS_FAILURE_CSMA_NCW4)
        STATS_TXT (G3MAC, MCPS_FAILURE_CSMA_NCW5)
        STATS_TXT (G3MAC, MCPS_FAILURE_CSMA_NCW6)
        STATS_TXT (G3MAC, MCPS_FAILURE_CSMA_NCW7)
        STATS_TXT (G3MAC, MCPS_FAILURE_CSMA_NCW8)
        STATS_TXT (G3MAC, MCPS_FAILURE_CSMA_NCW9)
        STATS_TXT (G3MAC, MCPS_FAILURE_CSMA_NCW10)
        default:
            pRet = NULL;
            break;
    } /* switch */

    return pRet;
} /* statsindex_to_text_mac */
/******************************************************************************
   End of function  statsindex_to_text_mac
******************************************************************************/


/******************************************************************************
* Function Name: statsindex_to_text_adp
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static const char *statsindex_to_text_adp (uint32_t index)
{
    const char * pRet;

    switch (index)
    {
        STATS_TXT (ADP, LOWPAN_IN_RECEIVES)
        STATS_TXT (ADP, LOWPAN_IN_HDR_ERRORS)
        STATS_TXT (ADP, LOWPAN_IN_MESH_RECEIVES)
        STATS_TXT (ADP, LOWPAN_IN_MESH_FORWDS)
        STATS_TXT (ADP, LOWPAN_IN_MESH_DELIVERS)
        STATS_TXT (ADP, LOWPAN_IN_REASM_REQDS)
        STATS_TXT (ADP, LOWPAN_IN_REASM_FAILS)
        STATS_TXT (ADP, LOWPAN_IN_REASM_OKS)
        STATS_TXT (ADP, LOWPAN_IN_COMP_REQDS)
        STATS_TXT (ADP, LOWPAN_IN_COMP_FAILS)
        STATS_TXT (ADP, LOWPAN_IN_COMP_OKS)
        STATS_TXT (ADP, LOWPAN_IN_DISCARDS)
        STATS_TXT (ADP, LOWPAN_IN_DISCARDS_SECURITY)
        STATS_TXT (ADP, LOWPAN_IN_DISCARDS_BCAST_TBL)
        STATS_TXT (ADP, LOWPAN_IN_DISCARDS_GROUP_TBL)
        STATS_TXT (ADP, LOWPAN_IN_DISCARDS_IPV6_BFULL)
        STATS_TXT (ADP, LOWPAN_IN_DISCARDS_LBP_BFULL)
        STATS_TXT (ADP, LOWPAN_IN_DISCARDS_RELAY_BFULL)
        STATS_TXT (ADP, LOWPAN_IN_DELIVERS)
        STATS_TXT (ADP, LOWPAN_IN_DELIVERS_UNICAST)
        STATS_TXT (ADP, LOWPAN_IN_DELIVERS_MULTICAST)
        STATS_TXT (ADP, LOWPAN_OUT_REQUESTS)
        STATS_TXT (ADP, LOWPAN_OUT_COMP_REQDS)
        STATS_TXT (ADP, LOWPAN_OUT_COMP_FAILS)
        STATS_TXT (ADP, LOWPAN_OUT_COMP_OKS)
        STATS_TXT (ADP, LOWPAN_OUT_FRAG_REQDS)
        STATS_TXT (ADP, LOWPAN_OUT_FRAG_FAILS)
        STATS_TXT (ADP, LOWPAN_OUT_FRAG_OKS)
        STATS_TXT (ADP, LOWPAN_OUT_FRAG_CREATES)
        STATS_TXT (ADP, LOWPAN_OUT_MESH_HOP_LIMIT_EXCEEDS)
        STATS_TXT (ADP, LOWPAN_OUT_MESH_NO_ROUTES)
        STATS_TXT (ADP, LOWPAN_OUT_MESH_REQUESTS)
        STATS_TXT (ADP, LOWPAN_OUT_MESH_FORWDS)
        STATS_TXT (ADP, LOWPAN_OUT_MESH_TRANSMITS)
        STATS_TXT (ADP, LOWPAN_OUT_DISCARDS)
        STATS_TXT (ADP, LOWPAN_OUT_TRANSMITS)
        STATS_TXT (ADP, LOWPAN_OUT_SUCCESS)
        STATS_TXT (ADP, LOWPAN_OUT_FAILURE)
        STATS_TXT (ADP, LOWPAN_OUT_FAILURE_NO_ACK)
        STATS_TXT (ADP, LOWPAN_OUT_IPV6_TRANSMITS)
        STATS_TXT (ADP, LOWPAN_OUT_IPV6_SUCCESS)
        STATS_TXT (ADP, LOWPAN_OUT_IPV6_FAILURE)
        STATS_TXT (ADP, LOWPAN_OUT_RELAY_TRANSMITS)
        STATS_TXT (ADP, LOWPAN_OUT_RELAY_SUCCESS)
        STATS_TXT (ADP, LOWPAN_OUT_RELAY_FAILURE)
        STATS_TXT (ADP, LOWPAN_IN_LBP_RECEIVES)
        STATS_TXT (ADP, LOWPAN_IN_LBP_DISCARDS)
        STATS_TXT (ADP, LOWPAN_OUT_LBP_DISCARDS)
        STATS_TXT (ADP, LOWPAN_OUT_LBP_REQUESTS)
        STATS_TXT (ADP, LOWPAN_OUT_LBP_TRANSMITS)
        STATS_TXT (ADP, LOWPAN_OUT_LBP_SUCCESS)
        STATS_TXT (ADP, LOWPAN_OUT_LBP_FAILURE)
        STATS_TXT (ADP, EAPPSK_SUCCESS)
        STATS_TXT (ADP, EAPPSK_FAILURE_MSG3_MACS)
        STATS_TXT (ADP, EAPPSK_FAILURE_MSG3_EAX)
        STATS_TXT (ADP, EAPPSK_FAILURE_MSG3_PCH)
        STATS_TXT (ADP, LOWPAN_IN_LOADNG_RECEIVES)
        STATS_TXT (ADP, LOWPAN_IN_LOADNG_RECEIVES_RREQ)
        STATS_TXT (ADP, LOWPAN_IN_LOADNG_RECEIVES_RREP)
        STATS_TXT (ADP, LOWPAN_IN_LOADNG_RECEIVES_RERR)
        STATS_TXT (ADP, LOWPAN_IN_LOADNG_RECEIVES_PREQ)
        STATS_TXT (ADP, LOWPAN_IN_LOADNG_RECEIVES_PREP)
        STATS_TXT (ADP, LOWPAN_IN_LOADNG_DISCARDS)
        STATS_TXT (ADP, LOWPAN_IN_LOADNG_DISCARDS_BY_BLACKLIST)
        STATS_TXT (ADP, LOWPAN_OUT_LOADNG_DISCARDS)
        STATS_TXT (ADP, LOWPAN_OUT_LOADNG_REQUESTS_RREQ)
        STATS_TXT (ADP, LOWPAN_OUT_LOADNG_REQUESTS_RREQ_REPAIR)
        STATS_TXT (ADP, LOWPAN_OUT_LOADNG_REQUESTS_PREQ)
        STATS_TXT (ADP, LOWPAN_OUT_LOADNG_TRANSMITS)
        STATS_TXT (ADP, LOWPAN_OUT_LOADNG_SUCCESS)
        STATS_TXT (ADP, LOWPAN_OUT_LOADNG_FAILURE)
        STATS_TXT (ADP, LOWPAN_OUT_LOADNG_FAILURE_NOACK)
        STATS_TXT (ADP, LOWPAN_OUT_LOADNG_FAILURE_NOACK_RREQ)
        default:
            pRet = NULL;
            break;
    } /* switch */

    return pRet;
} /* statsindex_to_text_adp */
/******************************************************************************
   End of function  statsindex_to_text_adp
******************************************************************************/


/******************************************************************************
* Function Name: statsindex_to_text_eap
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static const char *statsindex_to_text_eap (uint32_t index)
{
    const char * pRet;

    switch (index)
    {
        STATS_TXT (EAP, LBP_IN_RECEIVES)
        STATS_TXT (EAP, LBP_IN_RECEIVES_JOINRERQ)
        STATS_TXT (EAP, LBP_IN_RECEIVES_LEAVE)
        STATS_TXT (EAP, LBP_IN_DISCARD)
        STATS_TXT (EAP, LBP_OUT_TRANSMITS)
        STATS_TXT (EAP, LBP_OUT_SUCCESS)
        STATS_TXT (EAP, LBP_OUT_FAILURE)
        STATS_TXT (EAP, EAPPSK_SUCCESS)
        STATS_TXT (EAP, EAPPSK_FAILURE_MSG2_MACP)
        STATS_TXT (EAP, EAPPSK_FAILURE_MSG4_EAX)
        STATS_TXT (EAP, EAPPSK_FAILURE_MSG4_PCH)
        STATS_TXT (EAP, NEWDEVICE)
        STATS_TXT (EAP, JOIN_SUCCESS)
        STATS_TXT (EAP, JOIN_FAILURE)
        STATS_TXT (EAP, KICK_SUCCESS)
        STATS_TXT (EAP, KICK_FAILURE)
        STATS_TXT (EAP, GMKTRANS_SUCCESS)
        STATS_TXT (EAP, GMKTRANS_FAILURE)
        STATS_TXT (EAP, GMKACTIVATE_SUCCESS)
        STATS_TXT (EAP, GMKACTIVATE_FAILURE)
        default:
            pRet = NULL;
            break;
    } /* switch */

    return pRet;
} /* statsindex_to_text_eap */
/******************************************************************************
   End of function  statsindex_to_text_eap
******************************************************************************/


/******************************************************************************
* Function Name: statsindex_to_text
* Description :
* Arguments :
* Return Value :
******************************************************************************/
const char *statsindex_to_text (uint8_t id, uint32_t index)
{

    if (R_G3_INFO_LAYER_LMAC_DSP == id)
    {
        return statsindex_to_text_lml (index);
    }
    else if (R_G3_INFO_LAYER_UMAC == id)
    {
        return statsindex_to_text_mac (index);
    }
    else if (R_G3_INFO_LAYER_ADP == id)
    {
        return statsindex_to_text_adp (index);
    }
    else if (R_G3_INFO_LAYER_EAP == id)
    {
        return statsindex_to_text_eap (index);
    }
    else
    {
        return "UNDEFINED ID or below layer status";
    }
} /* statsindex_to_text */
/******************************************************************************
   End of function  statsindex_to_text
******************************************************************************/



/******************************************************************************
* Function Name: R_LOG_Stats2TextUif
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static const char *R_LOG_Stats2TextUif (uint32_t index)
{
    const char * pRet;

    switch (index)
    {
        STATS_TXT (UIF, RX_IN_CRC_ERROR)
        STATS_TXT (UIF, RX_IN_SYNC_BREAK)
        STATS_TXT (UIF, RX_IN_TIMEOUT)
        STATS_TXT (UIF, RX_IN_LENGTH_ERROR)
        STATS_TXT (UIF, RX_IN_BUFF_OVERFLOW)
        STATS_TXT (UIF, RX_IN_SECURITY_SUCCESS)
        STATS_TXT (UIF, RX_IN_SECURITY_ERROR)
        STATS_TXT (UIF, RX_IN_DISCARD)
        STATS_TXT (UIF, RX_OUT_COMMAND_REQD)
        STATS_TXT (UIF, RX_OUT_COMMAND_REQD_ERROR)
        STATS_TXT (UIF, RX_OUT_COMMAND_SUCCESS)
        STATS_TXT (UIF, TX_IN_RCV_COMMAND)
        STATS_TXT (UIF, TX_IN_RCV_ERROR)
        STATS_TXT (UIF, TX_IN_SECURITY_SUCCESS)
        STATS_TXT (UIF, TX_IN_SECURITY_ERROR)
        STATS_TXT (UIF, TX_OUT_COMMAND_REQD)
        STATS_TXT (UIF, TX_OUT_COMMAND_SUCCESS)
        STATS_TXT (UIF, TX_OUT_TIMEOUT)
        STATS_TXT (UIF, TX_DRV_ERROR)
        default:
            pRet = NULL;
            break;
    } /* switch */

    return pRet;
} /* R_LOG_Stats2TextUif */
/******************************************************************************
   End of function  R_LOG_Stats2TextUif
******************************************************************************/

/******************************************************************************
* Function Name: R_LOG_Stats2TextSys
* Description :
* Arguments :
* Return Value :
******************************************************************************/
const char *R_LOG_Stats2TextSys (uint8_t id, uint32_t index)
{

    if (R_SYS_INFO_BLOCK_UARTIF_0 == id)
    {
        return R_LOG_Stats2TextUif (index);
    }
    else
    {
        return "UNSUPPORT ID or below layer status";
    }
}
/******************************************************************************
   End of function  R_LOG_Stats2TextSys
******************************************************************************/

