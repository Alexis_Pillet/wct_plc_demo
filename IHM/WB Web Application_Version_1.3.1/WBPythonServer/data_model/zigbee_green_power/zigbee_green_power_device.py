# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to model ZigBee Green Power Device
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from abc import abstractmethod
from data_model.zigbee_device import ZigBeeDevice
from application import Application


class ZigBeeGreenPowerDevice(ZigBeeDevice):

    def __init__(self, address, id_wireless_bridge, application_frame_generator):
        # call super function
        super(ZigBeeGreenPowerDevice, self).__init__(address, id_wireless_bridge, application_frame_generator)

    def get_attribute(self, destination_ep, cluster_id, attribute_id_list):

        option = self.application_frame_generator.default_option
        option = self.application_frame_generator.set_option(option,
                                                             Application.OptionDownStreamMask.DESTINATION_ADDRESS_TYPE,
                                                             Application.OptionAddressType.SOURCE_ID_ADDRESS)
        frame = bytearray.fromhex(self.address)
        # Transform in little endian
        frame.reverse()
        frame.append(destination_ep)
        cluster_id = bytearray.fromhex(cluster_id)
        # Transform in little endian
        cluster_id.reverse()
        for byte in cluster_id:
            frame.append(byte)
        for value in attribute_id_list:
            attribute_id = bytearray.fromhex(value)
            # Transform in little endian
            attribute_id.reverse()
            for byte in attribute_id:
                frame.append(byte)
        self.application_frame_generator.send_get_attribute_command_frame(self.id_wireless_bridge, frame, option=option)

    def set_attribute(self, destination_ep, cluster_id, attribute_id_list, value):
        pass

    def discovery(self):

        self.get_attribute(self._END_POINT_BASIC_CLUSTER, self._BASIC_CLUSTER, [self._MODEL_IDENTIFIER,
                                                                                self._MANUFACTURER_NAME])

    @abstractmethod
    def update_data(self, payload):
        pass

    @abstractmethod
    def get_json_data(self):
        pass

    @abstractmethod
    def get_json_device(self, device_number):
        pass
