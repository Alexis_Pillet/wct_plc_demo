/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Configuration Commands/Responses - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.012  19-Jul-16   MvdB   Move utilities functions into zabNcpEzspUtilities
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/

#ifndef __ZAB_NCP_EZSP_CONFIGURATION_H__
#define __ZAB_NCP_EZSP_CONFIGURATION_H__

#include "ezsp-enum.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Version Request + Response Handler
 ******************************************************************************/
void zabNcpEzspConfiguration_Version(erStatus* Status, zabService* Service);
void zabNcpEzspConfiguration_VersionResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Get Config Value Request + Response Handler
 ******************************************************************************/
void zabNcpEzspConfiguration_GetConfigValue(erStatus* Status, zabService* Service, EzspConfigId ConfigId);
void zabNcpEzspConfiguration_GetConfigValueResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Set Config Value Request + Response Handler
 ******************************************************************************/
void zabNcpEzspConfiguration_SetConfigValue(erStatus* Status, zabService* Service, EzspConfigId ConfigId, unsigned16 ConfigValue);
void zabNcpEzspConfiguration_SetConfigValueResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Get Value / Extended Value Request + Response Handler
 ******************************************************************************/
void zabNcpEzspConfiguration_GetValue(erStatus* Status, zabService* Service, EzspValueId ValueId);
void zabNcpEzspConfiguration_GetExtendedValue(erStatus* Status, zabService* Service, EzspValueId ValueId, unsigned32 Characteristics);
void zabNcpEzspConfiguration_GetValueResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Add Endpoint Request + Response Handler
 * Note: We are handling the endpoints in ZAB now, so this function does not send a command.
 * It just registers the endpoint with the ZAB vendor.
 ******************************************************************************/
void zabNcpEzspConfiguration_AddEndpoint(erStatus* Status, zabService* Service, sapMsg *appMsg);

/******************************************************************************
 * Set Policy Request + Response Handler
 ******************************************************************************/
void zabNcpEzspConfiguration_SetPolicy(erStatus* Status, zabService* Service, EzspPolicyId PolicyId, EzspDecisionId DecisionId);
void zabNcpEzspConfiguration_SetPolicyResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Launch Bootloader Request + Response Handler
 ******************************************************************************/
void zabNcpEzspConfiguration_LaunchStandaloneBootloader(erStatus* Status, zabService* Service);
void zabNcpEzspConfiguration_LaunchStandaloneBootloaderResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/