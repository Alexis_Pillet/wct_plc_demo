/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the main interface for the ZAB ZNP Vendor - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.01.00  27-Aug-13   MvdB   Rework. Rebase version numbers to 0.0.1.0
 * 00.00.06.00  12-Jun-14   MvdB   Add Wireless Test Bench functions
 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 *                                 ARTF104098: Move mutex config to zabMutexConfigure.h
 * 00.00.06.06  30-Oct-14   MvdB   Lots of changes!
 * 00.00.07.00  31-Oct-14   MvdB   Release
 * 00.00.08.00  14-Nov-14   MvdB   Release
 * 01.00.00.00  05-Dec-14   MvdB   Release for ComX
 * 002.000.000  27-Feb-15   MvdB   Roll version number for release
 * 002.001.000  20-Apr-15   MvdB   Version for release
 * 002.002.000  29-Jul-15   MvdB   Release
 *****************************************************************************/

#ifndef __ZAB_ZNP_SERVICE_H__
#define __ZAB_ZNP_SERVICE_H__

#include "zabCoreService.h"
#include "zabCorePrivate.h"
#include "zabVendorService.h"


#include "zabZnpFrame.h"

#include "zabZnpAfUtility.h"
#include "zabZnpGp.h"
#include "zabZnpRftUtility.h"
#include "zabZnpRpcUtility.h"
#include "zabZnpSapiUtility.h"
#include "zabZnpSBLUtility.h"
#include "zabZnpSysUtility.h"
#include "zabZnpZdoUtility.h"


#include "zabZnpClone.h"
#include "zabZnpNwk.h"
#include "zabZnpOpen.h"

#include "zabZnpPrivate.h"

#include "sapMsgPrivate.h"
#include "srvCorePrivate.h"

#include "zabParseZnpUtility.h"

#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************
 *                      ******************************
 *                 *****  VENDOR SERVICE VERSIONING   *****
 *                      ******************************
 ******************************************************************************/
#define ZAB_VENDOR_VERSION_MAJOR   2   // non-backward compatible changes
#define ZAB_VENDOR_VERSION_MINOR   2   // backward compatible changes/enhancements
#define ZAB_VENDOR_VERSION_RELEASE 49  // given to QA for testing new version or fixes
#define ZAB_VENDOR_VERSION_BUILD   0   // internal developer build, not shown




/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 * Vendor is ready for local comms from the App
 ******************************************************************************/
extern
void zabZnpService_GetLocalCommsReady(erStatus* Status, zabService* Service);

/******************************************************************************
 * Vendor is ready for network comms from the App
 ******************************************************************************/
extern
void zabZnpService_GetNetworkCommsReady(erStatus* Status, zabService* Service);

/******************************************************************************
 * Get status of serial link
 ******************************************************************************/
extern
zab_bool zabZnpService_GetSerialLinkLocked(zabService* Service);

/******************************************************************************
 * Send a ZNP message.
 * Message will be discarded if ZNP link is locked, so caller should verify first.
 ******************************************************************************/
extern
void zabZnpService_SendMsg(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * Send a raw message to the network processor.
 * Message will be discarded if ZNP link is locked, so caller should verify first.
 ******************************************************************************/
#ifdef M_ENABLE_RAW
extern
void zabZnpService_SendMsgRaw(erStatus* Status, zabService* Service, sapMsg* Message);
#endif


#ifdef ZAB_VND_CFG_ENABLE_WIRELESS_TEST_BENCH
/******************************************************************************
 * WIRELESS TEST BENCH: Register for Data Request Notification from vendor
 ******************************************************************************/
extern
void zabZnpService_RegisterDataRequestNotificationHandler(erStatus* Status, zabService* Service, wtbUtility_DataReqNotification_t Callback);

/******************************************************************************
 * WIRELESS TEST BENCH: Register for Data Confirm Notifications from vendor
 ******************************************************************************/
extern
void zabZnpService_RegisterDataConfirmNotificationHandler(erStatus* Status, zabService* Service, wtbUtility_ZnpDataCnfNotification_t Callback);

/******************************************************************************
 * WIRELESS TEST BENCH: Register for Data Indication Notification from vendor
 ******************************************************************************/
extern
void zabZnpService_RegisterDataIndicationNotificationHandler(erStatus* Status, zabService* Service, wtbUtility_DataIndNotification_t Callback);
#endif

#ifdef __cplusplus
}
#endif

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif
