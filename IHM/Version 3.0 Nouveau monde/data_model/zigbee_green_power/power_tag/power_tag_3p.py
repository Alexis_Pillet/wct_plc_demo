# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to model Power Tag 3P Device
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from data_model.zigbee_green_power.power_tag.power_tag import PowerTag


class PowerTag3P(PowerTag):

    def __init__(self, address, _id_wireless_bridge):
        # call super function
        super(PowerTag3P, self).__init__(address, _id_wireless_bridge)
        self._voltage_pha_phb = None
        self._voltage_phb_phc = None
        self._voltage_phc_pha = None
        self._current_pha = None
        self._current_phb = None
        self._current_phc = None
        self._active_power_phb = None
        self._active_power_phc = None

    def update_data(self, payload):
        pass

    @property
    def voltage_pha_phb(self):
        return self._voltage_pha_phb

    @voltage_pha_phb.setter
    def voltage_pha_phb(self, voltage_pha_phb):
        self._voltage_pha_phb = voltage_pha_phb

    @property
    def voltage_phb_phc(self):
        return self._voltage_phb_phc

    @voltage_phb_phc.setter
    def voltage_phb_phc(self, voltage_phb_phc):
        self._voltage_phb_phc = voltage_phb_phc

    @property
    def voltage_phc_pha(self):
        return self._voltage_phc_pha

    @voltage_phc_pha.setter
    def voltage_phc_pha(self, voltage_phc_pha):
        self._voltage_phc_pha = voltage_phc_pha

    @property
    def current_pha(self):
        return self._current_pha

    @current_pha.setter
    def current_pha(self, current_pha):
        self._current_pha = current_pha

    @property
    def current_phb(self):
        return self._current_phb

    @current_phb.setter
    def current_phb(self, current_phb):
        self._current_phb = current_phb

    @property
    def current_phc(self):
        return self._current_phc

    @current_phc.setter
    def current_phc(self, current_phc):
        self._current_phc = current_phc

    @property
    def active_power_phb(self):
        return self._active_power_phb

    @active_power_phb.setter
    def active_power_phb(self, active_power_phb):
        self._active_power_phb = active_power_phb

    @property
    def active_power_phc(self):
        return self._active_power_phc

    @active_power_phc.setter
    def active_power_phc(self, active_power_phc):
        self._active_power_phc = active_power_phc
