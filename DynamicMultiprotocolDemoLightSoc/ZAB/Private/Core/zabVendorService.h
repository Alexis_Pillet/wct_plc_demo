/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the public vendor interfae for the ZAB ZNP Vendor - HEADER
 *   It is intended for internal zAB use only and should not be accessed by applications.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.02.00  31-Oct-13   MvdB   Tidy up from original
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
 *****************************************************************************/
#ifndef __ZAB_VENDOR_INTERFACE_H__
#define __ZAB_VENDOR_INTERFACE_H__

#include "zabVendorConfigure.h"


#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                 *****         VENDOR PUBLIC        *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Create an instance of ZAB Vendor
 ******************************************************************************/
extern
void zabVendorService_Create( erStatus* Status, zabService* Service );

/******************************************************************************
 * Reset an instance of ZAB Vendor
 ******************************************************************************/
extern
void zabVendorService_Reset( erStatus* Status, zabService* Service );

/******************************************************************************
 * Destroy an instance of ZAB Vendor
 ******************************************************************************/
extern
void zabVendorService_Destroy(erStatus* Status, zabService* Service);

/******************************************************************************
 * Vendor Work
 *   Process VendorOutQ and send commands to network processor
 *   Process VendorInQ and indicate messages up to the core
 *   Check for SRSP timeouts
 *   Update state machines
 ******************************************************************************/
extern
unsigned32 zabVendorService_Work( erStatus* Status, zabService* Service );


/******************************************************************************
 * Vendor is ready for local comms from the App
 ******************************************************************************/
void zabVendorService_GetLocalCommsReady(erStatus* Status, zabService* Service);

/******************************************************************************
 * Vendor is ready for network comms from the App
 ******************************************************************************/
void zabVendorService_GetNetworkCommsReady(erStatus* Status, zabService* Service);

/******************************************************************************
 * This function receives DATA and MANAGE SAP messages from the core.
 * It queues them for processing by Vendor Work
 ******************************************************************************/
extern
void zabVendorService_CoreToVendorOut(erStatus* Status, sapHandle Sap, sapMsg* Message);


/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#ifdef __cplusplus
}
#endif
#endif
