/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_app.h
*    @version
*        $Rev: 3384 $
*    @last editor
*        $Author: a5089752 $
*    @date
*        $Date:: 2017-05-31 13:49:31 +0900#$
* Description :
******************************************************************************/

#ifndef R_DEMO_APP_H
#define R_DEMO_APP_H

/******************************************************************************
Macro definitions
******************************************************************************/
#define R_CAP_ICMP_IDENTIFIER             (0x0102u)
#define R_CAP_SYNCWORD                    (0xF8726FBAul)
#define R_CAP_CAPSET_MAX_LEN              (32u)

#define R_DEMO_APP_MODE_NORMAL            (0u)
#define R_DEMO_APP_MODE_DEMO              (1u)
#define R_DEMO_APP_MODE_CERT              (2u)

#define R_DEMO_APP_UART_BOOT              (0u)
#define R_DEMO_APP_SROM_BOOT              (1u)

#define R_DEMO_APP_STRING_BUFFER_SIZE     (72u)
#define R_DEMO_APP_NSDU_BUFFER_SIZE       (1280u)
#define R_DEMO_APP_MSDU_PRINT_MAXLEN      (128u)
#define R_DEMO_APP_STATS_BUFF_MAXLEN      (1024u)
#define R_DEMO_APP_PAN_MAXNUM             (8u)

#define R_UNICAST_TX_TIMEOUT              (30u)         /*!< Timeout for MAC unicast data transmissions in seconds */
#define R_BROADCAST_TX_TIMEOUT            (30u)         /*!< Timeout for MAC broadcast data transmissions in seconds */

#define R_DEMO_G3_USE_PRIMARY_CH          (0u)          /*!< CPX3 G3 FW can use dual adp, but wrapper fixed single ch */
#define R_DEMO_G3_USE_SECONDARY_CH        (1u)          /*!< CPX3 G3 FW can use dual adp, but wrapper fixed single ch */

#define R_DEMO_G3MAC_NEIGBOUR_TABLE_SIZE  (150u)        /*!< Neighbour table size */
#define R_DEMO_G3MAC_DEVICE_TABLE_SIZE    (150u)        /*!< Device table size */
/* table configuration. */
#define R_DEMO_ADP_ADPD_DATA_QUEUE_SIZE   (2u)          /*!< Adpd-data queue size range 1~4 */
#define R_DEMO_ADP_ROUTING_TABLE_SIZE     (150u)        /*!< Routing table size range 1~2000 */
#define R_DEMO_ADP_MAX_PAN_DESCRIPTORS    (64u)         /*!< Maximum number of PAN descriptors stored during discovery  range 1~128 */


/* for Coordinator */
#define R_DEMO_EAP_LBP_BUFF_SIZE          (8u)          /*!< LBP buffer size range 1-128 */
#define R_DEMO_EAP_CINFO_TABLE_SIZE       (64u)         /*!< Client ifnromation table size range 1~1200 */


#define R_DEMO_G3_STATUS_NOT_SET          (0x01u)       /*!< Default value for confirm structures */
#define R_DEMO_G3_STATUS_FAILED           (0x02u)       /*!< Error Status when api call return failure */


#define R_APP_WAIT_CNF_TIMEOUT            (300u)                 /*!< Timeout for wait confirm in seconds */
#define HANDLE_APP_DONT_CARE              (0x00u)                /*!< HANDLE_DONT_CARE */
#define HANDLE_APP_FIRST                  (0x01u)                /*!< HANDLE_APP_FIRST */
#define HANDLE_APP_CMN_REQUEST            (0x01u)                /*!< HANDLE_APP_CMN_REQUEST */
#define HANDLE_APP_LAST                   (0x02u)                /*!< HANDLE_APP_LAST */
#define MAX_USED_APP_TIMER_HANDLES        (HANDLE_APP_LAST + 1u) /*!< + 1 due to starting from 0 */


/******************************************************************************
Typedef definitions
******************************************************************************/
/*!
    \enum r_status_flag_type_t
    \brief Type of Status Flag
 */
typedef enum
{
    R_FLAG_GET_INFO           =   0x00000001,
    R_FLAG_CLEAR_INFO         =   0x00000002,
    R_FLAG_GET_CONFIG         =   0x00000004,
    R_FLAG_SET_CONFIG         =   0x00000008,
    R_FLAG_MCPS_DATA          =   0x00000010,
    R_FLAG_MLME_RESET         =   0x00000020,
    R_FLAG_MLME_SET           =   0x00000040,
    R_FLAG_MLME_GET           =   0x00000080,
    R_FLAG_MLME_SCAN          =   0x00000100,
    R_FLAG_MLME_START         =   0x00000200,
    R_FLAG_ADPM_LBP           =   0x00000400,
    R_FLAG_ADPM_LEAVE         =   0x00000800,
    R_FLAG_ADPM_JOIN          =   0x00001000,
    R_FLAG_ADPM_DISCOVERY     =   0x00002000,
    R_FLAG_ADPD_DATA          =   0x00004000,
    R_FLAG_ADPM_START         =   0x00008000,
    R_FLAG_ADPM_SET           =   0x00010000,
    R_FLAG_ADPM_GET           =   0x00020000,
    R_FLAG_ADPM_ROUTE         =   0x00040000,
    R_FLAG_ADPM_PATH          =   0x00080000,
    R_FLAG_ADPM_RESET         =   0x00100000,
    R_FLAG_EAPM_START         =   0x00200000,
    R_FLAG_EAPM_GET           =   0x00400000,
    R_FLAG_EAPM_SET           =   0x00800000,
    R_FLAG_EAPM_NETWORK       =   0x01000000,
    R_FLAG_EAPM_SETCLIENTINFO =   0x02000000,
    R_FLAG_EAPM_RESET         =   0x04000000
} r_status_flag_type_t;

/*!
    \enum r_modem_platform_type_t
    \brief Type of CPX modem platform
 */
typedef enum
{
    R_PLATFORM_TYPE_CPX1 =   0x01,               /*!< R_PLATFORM_TYPE_CPX1 */
    R_PLATFORM_TYPE_CPX2 =   0x02,               /*!< R_PLATFORM_TYPE_CPX2 */
    R_PLATFORM_TYPE_CPX3 =   0x03                /*!< R_PLATFORM_TYPE_CPX3 */

} r_modem_platform_type_t;

/*!
    \enum r_modem_board_type_t
    \brief Type of CPX modem board
 */
typedef enum
{
    R_BOARD_TYPE_EU_OFDM =   0x00,             /*!< R_BOARD_TYPE_EU_OFDM */
    R_BOARD_TYPE_G_CPX   =   0x01,             /*!< R_BOARD_TYPE_G_CPX */
    R_BOARD_TYPE_G_CPX3  =   0x02              /*!< R_BOARD_TYPE_G_CPX */

} r_modem_board_type_t;


/*!
    \struct r_cap_icmp_header_t
    \brief ICMP CAP header
 */
typedef struct
{
    uint8_t type;
    uint8_t code;
    uint8_t checksum[2];
    uint8_t identifier[2];
    uint8_t sequenceNumber[2];

} r_cap_icmp_header_t;

/*!
    \struct r_cap_command_header_t
    \brief CAP command header
 */
typedef struct
{
    uint8_t Syncword[4];
    uint8_t CheckSum[2];            /* include command */
    uint8_t AckReq;
    uint8_t Type;
    uint8_t CmdID[2];
    uint8_t SequenceNumber[2];
    uint8_t Reserve[2];
    uint8_t length[2];              /* byte length after here (payload length only) */

} r_cap_command_header_t;

/*!
    \struct r_cap_cmd_capm_set_req_t
    \brief CAP set request
 */
typedef struct
{
    uint8_t type;
    uint8_t id[2];
    uint8_t index[2];
    uint8_t payload[R_CAP_CAPSET_MAX_LEN];

} r_cap_cmd_capm_set_req_t;

/*!
    \struct r_cap_cmd_capm_get_req_t
    \brief CAP get request
 */
typedef struct
{
    uint8_t type;
    uint8_t id[2];
    uint8_t index[2];

} r_cap_cmd_capm_get_req_t;

/*!
    \struct r_cap_cmd_capm_set_cnf_t
    \brief CAP set confirm
 */
typedef struct
{
    uint8_t Status[4];
    uint8_t type;
    uint8_t id[2];
    uint8_t index[2];

} r_cap_cmd_capm_set_cnf_t;

/*!
    \struct r_cap_cmd_capm_get_cnf_t
    \brief CAP get confirm
 */
typedef struct
{
    uint8_t Status[4];
    uint8_t type;
    uint8_t id[2];
    uint8_t index[2];
    uint8_t payload[R_CAP_CAPSET_MAX_LEN];

} r_cap_cmd_capm_get_cnf_t;

/*!
    \enum r_cap_type_id_t
    \brief CAP type ID
 */
typedef enum
{
    R_CAP_TYPE_ID_GET_SET_ADP   =   0x02,        /*!< R_CAP_TYPE_ID_GET_SET_ADP */
    R_CAP_TYPE_ID_GET_SET_MAC   =   0x03,        /*!< R_CAP_TYPE_ID_GET_SET_MAC */
    R_CAP_TYPE_ID_GET_SET_STATS =   0x80,        /*!< R_CAP_TYPE_ID_GET_SET_STATS */
    R_CAP_TYPE_ID_GET_SET_LOGS  =   0x81         /*!< R_CAP_TYPE_ID_GET_SET_LOGS */

} r_cap_type_id_t;


/*!
\enum r_cap_type_stats_index_t
\brief CAP statistices type
*/
typedef enum
{
    R_CAP_STATS_INDEX_MAC_SND = 0x00,
    R_CAP_STATS_INDEX_MAC_MOD,
    R_CAP_STATS_INDEX_MAC_CSMA1,
    R_CAP_STATS_INDEX_MAC_CSMA2,
} r_cap_type_stats_index_t;


/*!
    \enum r_cap_type_id_t
    \brief CAP type ID
 */
typedef enum
{
    CAP_SUCCESS                 = 0x00,
    CAP_INVALID_REQUEST         = 0x01,
    CAP_UNKNOWN_DEVICE          = 0x02,
    CAP_FAILURE                 = 0x80,
    CAP_NON_SUPPORTED_ATTRIBUTE = 0x90,
    CAP_INVALID_INDEX           = 0x91,
    CAP_OUT_OF_RANGE            = 0x92,
    CAP_READ_ONLY               = 0x93

} r_cap_sap_status_t;

typedef enum
{
    R_DEMO_MODE_SIMPLE = 0,
    R_DEMO_MODE_AUTO,
    R_DEMO_MODE_CERT,
} r_demo_operation_mode_t;


typedef struct
{
    r_modem_platform_type_t modemPlatformType;
    r_modem_board_type_t    modemBoardType;
    r_g3_bandplan_t         bandPlan;
    r_adp_device_type_t     devType;
    r_g3_route_type_t       routeType;
    r_boolean_t             verboseEnabled;
    r_boolean_t             macPromiscuousEnabled;
    r_demo_operation_mode_t appMode;
    r_g3mac_qos_t           qualityOfService;
    r_boolean_t             discoverRoute;
    uint64_t                deviceEUI64;                /*!< EUI64 of the device */
    uint16_t                panId;
    uint16_t                coordShortAddress;
    uint8_t                 pskKey[16];                 /*!< Pre-shared key */
    uint8_t                 gmk0[16];                   /*!< GMK0 for coordinator */
    uint8_t                 gmk1[16];                   /*!< GMK1 for coordinator */
    uint8_t                 activeKeyIndex;             /*!< active key index for coordinator */
    uint8_t                 wait[4];
    uint8_t                 tonemask[9];
    r_g3_config_extid_t     extId;
} r_demo_config_t;


typedef struct
{
    uint16_t panId;                                     /*!< Assigned PAN ID */
    uint16_t shortAddress;                              /*!< Assigned short address */
    uint8_t  nsduHandle;                                /*!< NSDU handle for data transmissions */
} r_demo_entity_t;


typedef struct
{
    uint8_t                panCount;
    r_adp_pan_descriptor_t pan[R_DEMO_APP_PAN_MAXNUM];
} r_demo_paninfo_t;

typedef struct
{
    uint8_t          getStringBuffer[R_DEMO_APP_STRING_BUFFER_SIZE];                    /*!< String buffer used for console interaction */
    uint8_t          Nsdu[R_DEMO_APP_NSDU_BUFFER_SIZE];                                 /*!< Buffer for IPv6 data frame */
    r_demo_paninfo_t panInfo;
} r_demo_buff_t;

typedef struct
{
    uint8_t g3mode;
    uint8_t startMode;
    uint8_t routeType;
} r_apl_mode_ch_t;

typedef struct
{
    uint8_t         bandPlan;
    uint8_t         tonemask[9];
    r_apl_mode_ch_t ch[2];
} r_apl_mode_t;

typedef struct
{
/*===========================================================================*/
/*    G3CTRL callbacks                                                       */
/*===========================================================================*/

    r_g3_set_config_cnf_t    setConfig;
    r_g3_get_config_cnf_t    getConfig;
    r_g3_get_info_cnf_t      getInfo;
    r_g3_clear_info_cnf_t    clrInfo;
    r_g3_event_ind_t         eventInd;
    uint32_t                 cbBuffU32[R_DEMO_APP_STATS_BUFF_MAXLEN / 4];


/*===========================================================================*/
/*    G3MAC callbacks                                                        */
/*===========================================================================*/
    r_g3mac_mcps_data_cnf_t         mcpsDataCnf;
    r_g3mac_mlme_reset_cnf_t        mlmeResetCnf;
    r_g3mac_mlme_get_cnf_t          mlmeGetCnf;
    r_g3mac_mlme_set_cnf_t          mlmeSetCnf;
    r_g3mac_mlme_scan_cnf_t         mlmeScanCnf;
    r_g3mac_mlme_start_cnf_t        mlmeStartCnf;

/*    r_g3mac_mcps_data_ind_t             mcpsDataInd; */
    r_g3mac_mlme_bcn_notify_ind_t   mlmeBeaconNotifyInd;
    r_g3mac_mlme_comm_status_ind_t  mlmeCommStatusInd;
    r_g3mac_mlme_framecount_ind_t   mlmeFrameCountInd;
    r_g3mac_mlme_tmr_receive_ind_t  mlmeTmrReceivInd;
    r_g3mac_mlme_tmr_transmit_ind_t mlmeTmrTransmitInd;

/*===========================================================================*/
/*    ADP callbacks                                                        */
/*===========================================================================*/
    r_adp_adpd_data_cnf_t           adpdDataCnf;
    r_adp_adpm_reset_cnf_t          adpmResetCnf;
    r_adp_adpm_discovery_cnf_t      adpmDiscoveryCnf;
    r_adp_adpm_network_start_cnf_t  adpmNetworkStartCnf;
    r_adp_adpm_network_join_cnf_t   adpmNetworkJoinCnf;
    r_adp_adpm_network_leave_cnf_t  adpmNetworkLeaveCnf;
    r_adp_adpm_get_cnf_t            adpmGetCnf;
    r_adp_adpm_set_cnf_t            adpmSetCnf;
    r_adp_adpm_route_disc_cnf_t     adpmRouteDiscoveryCnf;
    r_adp_adpm_path_discovery_cnf_t adpmPathDiscoveryCnf;
    r_adp_adpm_lbp_cnf_t            adpmLbpCnf;

/*    r_adp_adpd_data_ind_t               adpdDataInd; */
    r_adp_adpm_network_status_ind_t adpmNetworkStatusInd;
    r_adp_adpm_path_discovery_ind_t adpmPathDiscoveryInd;
    r_adp_adpm_lbp_ind_t            adpmLbpInd;

/*    r_adp_adpm_buffer_ind_t             adpmBufferInd; */
    r_adp_adpm_key_state_ind_t      adpmKeyStateInd;
    r_adp_adpm_route_error_ind_t    adpmRouteErrorInd;
    r_adp_adpm_eap_key_ind_t        adpmEapKeyInd;
    r_adp_adpm_framecounter_ind_t   adpmFrameCounterInd;
    r_adp_adpm_route_update_ind_t   adpmRouteUpdateInd;
    r_adp_adpm_rrep_ind_t           adpmRrepInd;

/*===========================================================================*/
/*    EAP callbacks                                                        */
/*===========================================================================*/
    r_eap_eapm_reset_cnf_t          eapmResetCnf;
    r_eap_eapm_start_cnf_t          eapmStartCnf;
    r_eap_eapm_get_cnf_t            eapmGetCnf;
    r_eap_eapm_set_cnf_t            eapmSetCnf;
    r_eap_eapm_network_cnf_t        eapmNetworkCnf;
    r_eap_eapm_set_clientinfo_cnf_t eapmSetClientInfoCnf;

/*    r_eap_eapm_network_join_ind_t       eapmNetworkJoinInd; */
    r_eap_eapm_network_leave_ind_t  eapmNetworkLeaveInd;

/*    r_eap_eapm_newdevice_ind_t          eapmNewDeviceInd; */
} r_demo_g3_cb_str_t;


/******************************************************************************
Functions prototype
******************************************************************************/

/***********************************************************************
* Function Name     : R_DEMO_AppHandleMacTmrRcvInd
* Description       : Handling function for MAC TMR Receive indications
* Argument          : None
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppHandleMacTmrRcvInd(const r_g3mac_mlme_tmr_receive_ind_t* ind);
   \brief Handling function for MAC TMR Receive indications
 */
void R_DEMO_AppHandleMacTmrRcvInd (const r_g3mac_mlme_tmr_receive_ind_t * ind);

/***********************************************************************
* Function Name     : R_DEMO_AppHandleMacTmrTransmitInd
* Description       : Handling function for MAC TMR Transmit indications
* Argument          : None
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppHandleMacTmrTransmitInd(const r_g3mac_mlme_tmr_transmit_ind_t* ind);
   \brief Handling function for MAC TMR Transmit indications
 */
void R_DEMO_AppHandleMacTmrTransmitInd (const r_g3mac_mlme_tmr_transmit_ind_t * ind);


/***********************************************************************
* Function Name     : R_DEMO_AppMainMenu
* Description       : Demo application main menu
* Argument          : None
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppMainMenu(void);
   \brief Demo application main menu
 */
void R_DEMO_AppMainMenu (void);

/***********************************************************************
* Function Name     : R_DEMO_AppHandleDataIndication
* Description       : Handling function for data indications
* Argument          : ind : Pointer to indication structure
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppHandleDataIndication(const r_adp_adpd_data_ind_t* ind);
   \brief Handling function for data indications
 */
void R_DEMO_AppHandleDataIndication (const r_adp_adpd_data_ind_t * ind);

/***********************************************************************
* Function Name     : R_DEMO_AppHandleLeaveIndication
* Description       : Handling function for network leave indications
* Argument          : None
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppHandleLeaveIndication(void);
   \brief Handling function for network leave indications
 */
void R_DEMO_AppHandleLeaveIndication (void);

/***********************************************************************
* Function Name     : R_DEMO_AppHandleBufferInd
* Description       : Handling function for buffer indications
* Argument          : ind : Pointer to indication structure
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppHandleBufferInd(const r_adp_adpm_buffer_ind_t* ind);
   \brief Handling function for buffer indications
 */
void R_DEMO_AppHandleBufferInd (const r_adp_adpm_buffer_ind_t * ind);

/***********************************************************************
* Function Name     : R_DEMO_AppHandleStatusInd
* Description       : Handling function for network status indications
* Argument          : ind : Pointer to indication structure
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppHandleStatusInd(const r_adp_adpm_network_status_ind_t* ind);
   \brief Handling function for network status indications
 */
void R_DEMO_AppHandleStatusInd (const r_adp_adpm_network_status_ind_t * ind);

/***********************************************************************
* Function Name     : R_DEMO_AppHandlePathDiscInd
* Description       : Handling function for path discovery indications
* Argument          : ind : Pointer to indication structure
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppHandlePathDiscInd(const r_adp_adpm_path_discovery_ind_t* ind);
   \brief Handling function for path discovery indications
 */
void R_DEMO_AppHandlePathDiscInd (const r_adp_adpm_path_discovery_ind_t * ind);

/***********************************************************************
* Function Name     : R_DEMO_AppHandleFrameCntInd
* Description       : Handling function for reboot request indications
* Argument          : None
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppHandleFrameCntInd(const r_adp_adpm_framecounter_ind_t* ind);
   \brief Handling function for reboot request indications
 */
void R_DEMO_AppHandleFrameCntInd (const r_adp_adpm_framecounter_ind_t * ind);

/***********************************************************************
* Function Name     : R_DEMO_AppHandleRouteInd
* Description       : Handling function for route update indications
* Argument          : None
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppHandleRouteInd(const r_adp_adpm_route_update_ind_t* ind);
   \brief Handling function for route update indications
 */
void R_DEMO_AppHandleRouteInd (const r_adp_adpm_route_update_ind_t * ind);

/***********************************************************************
* Function Name     : R_DEMO_AppHandleLoadInd
* Description       : Handling function for load sequence number indications
* Argument          : None
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppHandleLoadInd(const r_adp_adpm_load_seq_num_ind_t* ind);
   \brief Handling function for load sequence number indications
 */
void R_DEMO_AppHandleLoadInd (const r_adp_adpm_load_seq_num_ind_t * ind);

/***********************************************************************
* Function Name     : R_DEMO_AppHandleRrepInd
* Description       : Handling function for rrep indications
* Argument          : None
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppHandleRrepInd(const r_adp_adpm_rrep_ind_t* ind);
   \brief Handling function for rrep indications
 */
void R_DEMO_AppHandleRrepInd (const r_adp_adpm_rrep_ind_t * ind);

/***********************************************************************
* Function Name     : R_DEMO_AppHandleSysRebootReqInd
* Description       : Handling function for reboot request indications
* Argument          : None
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppHandleSysRebootReqInd(void);
   \brief Handling function for reboot request indications
 */
void R_DEMO_AppHandleSysRebootReqInd (const r_sys_rebootreq_ind_t * ind);

/***********************************************************************
* Function Name     : R_DEMO_AppPreserveProcess
* Description       : Handling function for preserved info
* Argument          : None
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppPreserveProcess(uint32_t framecounter);
   \brief Handling function for device resetting
 */
r_result_t R_DEMO_AppPreserveProcess (uint32_t framecounter);

/***********************************************************************
* Function Name     : R_DEMO_AppPresetProcess
* Description       : Handling function for preserved info
* Argument          : None
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppPresetProcess(uint32_t framecounter);
   \brief Handling function for device resetting
 */
r_result_t R_DEMO_AppPresetProcess (void);

/***********************************************************************
* Function Name     : R_DEMO_AppResetDevice
* Description       : Handling function for device resetting
* Argument          : None
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppResetDevice(void);
   \brief Handling function for device resetting
 */
r_result_t R_DEMO_AppResetDevice (void);

/***********************************************************************
* Function Name     : R_DEMO_AppJoinNetwork
* Description       : Starts network join procedure
* Argument          : None
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppJoinNetwork(void);
   \brief Starts network join procedure
 */
void R_DEMO_AppJoinNetwork (void);


/*!
   \fn void R_DEMO_DeviceStartup(void)
   \brief Function used for device reboot(defined externally)
 */
r_result_t R_DEMO_ModemBoot (void);
void R_DEMO_ModemReboot (void);
void R_DEMO_AppCert (void);
void R_DEMO_AppMainMenuProc (void);
void R_DEMO_AppMainMenuProcLbs (void);
r_result_t R_DEMO_AppClearInfo (uint8_t chId, uint8_t type);
r_result_t R_DEMO_AppGetLog (uint8_t chId, r_g3_info_layer_t layer);
r_result_t R_DEMO_AppGetStatistics (uint8_t chId, r_g3_info_layer_t layer);
r_result_t R_DEMO_SetDeviceType (void);
r_result_t R_DEMO_AppNetworkStart (uint16_t panId);
uint16_t R_DEMO_AppNetworkDiscovery (void);

void R_DEMO_AppHandleMcpsDataInd (const r_g3mac_mcps_data_ind_t * ind);

r_result_t R_DEMO_AppToggleMacPromiscuous (uint8_t onoff);
r_result_t R_DEMO_CheckModeBandPlan (uint8_t bandPlan, r_apl_mode_ch_t * pModeCh);

/*!
   \fn flags_manipulation
   \brief Status flag manipulation
 */

#endif /* R_DEMO_APP_H */

