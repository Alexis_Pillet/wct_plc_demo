#include "inter_task.h"

 /******************************************************************************
 *                      ******************************
 *                 *****       LOCAL VARIABLES        *****
 *                      ******************************
 ******************************************************************************/
 static uint8_t msqOK;
 /******************************************************************************
 *                      ******************************
 *                 *****       EXTERN VARIABLES       *****
 *                      ******************************
 ******************************************************************************/
 OS_Q ZigbeeToPlcMsgQ;
OS_Q PlcToZigbeeMsgQ;


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/
/******************************************************************************
 * Create all the messages Queues
 ******************************************************************************/
uint8_t createMsgQ(void)
{
	msqOK = 0;
	RTOS_ERR       p_err;
	OSQCreate(&ZigbeeToPlcMsgQ, "ZigbeeToPlcMsgQ", 20, &p_err);
	if (RTOS_ERR_CODE_GET(p_err) == RTOS_ERR_NONE) 
	{
		msqOK = 1;
	}
 
	OSQCreate(&PlcToZigbeeMsgQ, "PlcToZigbeeMsgQ", 20, &p_err);
	if (RTOS_ERR_CODE_GET(p_err) == RTOS_ERR_NONE) 
	{
		msqOK += 1;
	}
	
	return msqOK;
}	
 
/******************************************************************************
 * wait a msg from a specified msgQ
 ******************************************************************************/ 
uint8_t* waitMsgQ(OS_Q *MsgQ, uint16_t* msgsize, uint32_t timeToWait)
{
	OS_RATE_HZ  tick_rate;
    OS_TICK     ticks;
	RTOS_ERR err;
	CPU_TS ts;
	uint8_t* msgRcv;
   

	tick_rate = OSCfg_TickRate_Hz;
    ticks     = (tick_rate * ((OS_TICK)timeToWait + (OS_TICK)500u / tick_rate)) / (OS_TICK)1000u;
    *msgsize = 0;
	msgRcv =  (uint8_t*) OSQPend (MsgQ, ticks, OS_OPT_PEND_BLOCKING, msgsize, &ts, &err);
  
	if((RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE) || (msgsize==0))
	{
		*msgsize = 0;
	}

	return msgRcv;
}

/******************************************************************************
 * send a msg to a specified msgQ
 ******************************************************************************/
uint8_t sendMsgQ(OS_Q *MsgQ, uint8_t* msg, uint16_t msgSize)
{
	RTOS_ERR       err; 
	uint8_t status = 0;
	
	OSQPost (MsgQ,msg, msgSize, OS_OPT_POST_FIFO,&err);	
	
	if (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) 
	{
		status = 1;
	}
	return status;
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/