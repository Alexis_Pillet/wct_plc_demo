/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the ZAB Test Cluster.
 *   This plugin is designed for ZAB testing only.
 * 
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 * 002.002.015  22-Oct-15   MvdB   Original
 *****************************************************************************/

#include <stdio.h>
#include <stddef.h>
#include <stddef.h>
#include <string.h>
#include "zabCoreService.h"
#include "appConsoleMain.h"
#include "appZcl.h"
#include "szl.h"
#include "szl_plugin.h"
#include "szl_report.h"

#include "zab_test_cluster.h"

/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/
#define M_NUM_ATTRIBUTES_PER_REGISTRATION 10
#define M_NUM_ATTRIBUTES_PER_READ 4

/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Cluster ID */
#define ZCL_CLUSTER_ID_ZAB_TEST     0xFFFE


/* Limits */
#define UINT24_MAX (0xFFFFFF)
#define INT24_MAX  (0x7FFFFF)
#define INT24_MIN  (-INT24_MAX-1)

#define UINT40_MAX (0xFFFFFFFFFF)
#define INT40_MAX  (0x7FFFFFFFFF)
#define INT40_MIN  (-INT40_MAX-1)

#define UINT48_MAX (0xFFFFFFFFFFFF)
#define INT48_MAX  (0x7FFFFFFFFFFF)
#define INT48_MIN  (-INT48_MAX-1)

#define UINT56_MAX (0xFFFFFFFFFFFFFF)
#define INT56_MAX  (0x7FFFFFFFFFFFFF)
#define INT56_MIN  (-INT56_MAX-1)


typedef struct
{
  SzlPlugin_ZabTest_Attributes_t AttributeData;
  szl_uint8 EndpointNumber;
}SzlPlugin_ZabTest_Endpoints_t;

/* Parameters for the service */
typedef struct
{
  szl_uint8 numEndpoints;
  SzlPlugin_ZabTest_Endpoints_t ZabTestEndpoints[VLA_INIT];
}SzlPlugin_ZabTest_Params_t;

#define SzlPlugin_ZabTest_Params_t_SIZE(numEndpoints) (sizeof(SzlPlugin_ZabTest_Params_t) - (VLA_INIT * sizeof(SzlPlugin_ZabTest_Endpoints_t))+ (sizeof(SzlPlugin_ZabTest_Endpoints_t) * (numEndpoints)))

/* 
 * The set of attributes for the cluster.
 * This const data is used to initialise the majority of the attribute table.
 * Once it is copied into RAM, Endpoint and Data Access pointers are set for each attribute.
 * 
 * Read only attributes use direct access by SZL.
 * Writable attributes use Read/Write callbacks, as the need the write callback to notify the plugin that
 *   a write is happening, so it can notify the app.
 * 
 * New attributes must be added here.
 */
const SZL_DataPoint_t zabTestDatapointConfig[] = {
//{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_NO_DATA        , 0, SZL_ZB_DATATYPE_NO_DATA        ,  0, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtNoData)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_DATA8          , 0, SZL_ZB_DATATYPE_DATA8          ,  1, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtData8)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_DATA16         , 0, SZL_ZB_DATATYPE_DATA16         ,  2, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtData16)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_DATA24         , 0, SZL_ZB_DATATYPE_DATA24         ,  4, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtData24)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_DATA32         , 0, SZL_ZB_DATATYPE_DATA32         ,  4, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtData32)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_DATA40         , 0, SZL_ZB_DATATYPE_DATA40         ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtData40)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_DATA48         , 0, SZL_ZB_DATATYPE_DATA48         ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtData48)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_DATA56         , 0, SZL_ZB_DATATYPE_DATA56         ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtData56)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_DATA64         , 0, SZL_ZB_DATATYPE_DATA64         ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtData64)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_BOOLEAN        , 0, SZL_ZB_DATATYPE_BOOLEAN        ,  1, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtBoolean)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_BITMAP8        , 0, SZL_ZB_DATATYPE_BITMAP8        ,  1, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtBitmap8)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_BITMAP16       , 0, SZL_ZB_DATATYPE_BITMAP16       ,  2, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtBitmap16)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_BITMAP24       , 0, SZL_ZB_DATATYPE_BITMAP24       ,  4, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtBitmap24)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_BITMAP32       , 0, SZL_ZB_DATATYPE_BITMAP32       ,  4, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtBitmap32)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_BITMAP40       , 0, SZL_ZB_DATATYPE_BITMAP40       ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtBitmap40)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_BITMAP48       , 0, SZL_ZB_DATATYPE_BITMAP48       ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtBitmap48)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_BITMAP56       , 0, SZL_ZB_DATATYPE_BITMAP56       ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtBitmap56)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_BITMAP64       , 0, SZL_ZB_DATATYPE_BITMAP64       ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtBitmap64)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_UINT8          , 0, SZL_ZB_DATATYPE_UINT8          ,  1, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtUInt8)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_UINT16         , 0, SZL_ZB_DATATYPE_UINT16         ,  2, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtUInt16)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_UINT24         , 0, SZL_ZB_DATATYPE_UINT24         ,  4, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtUInt24)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_UINT32         , 0, SZL_ZB_DATATYPE_UINT32         ,  4, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtUInt32)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_UINT40         , 0, SZL_ZB_DATATYPE_UINT40         ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtUInt40)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_UINT48         , 0, SZL_ZB_DATATYPE_UINT48         ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtUInt48)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_UINT56         , 0, SZL_ZB_DATATYPE_UINT56         ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtUInt56)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_UINT64         , 0, SZL_ZB_DATATYPE_UINT64         ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtUInt64)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_INT8           , 0, SZL_ZB_DATATYPE_INT8           ,  1, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtInt8)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_INT16          , 0, SZL_ZB_DATATYPE_INT16          ,  2, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtInt16)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_INT24          , 0, SZL_ZB_DATATYPE_INT24          ,  4, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtInt24)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_INT32          , 0, SZL_ZB_DATATYPE_INT32          ,  4, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtInt32)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_INT40          , 0, SZL_ZB_DATATYPE_INT40          ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtInt40)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_INT48          , 0, SZL_ZB_DATATYPE_INT48          ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtInt48)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_INT56          , 0, SZL_ZB_DATATYPE_INT56          ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtInt56)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_INT64          , 0, SZL_ZB_DATATYPE_INT64          ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtInt64)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_ENUM8          , 0, SZL_ZB_DATATYPE_ENUM8          ,  1, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtEnum8)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_ENUM16         , 0, SZL_ZB_DATATYPE_ENUM16         ,  2, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtEnum16)}}},
//{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_SEMI_PREC      , 0, SZL_ZB_DATATYPE_SEMI_PREC      ,  2, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtSemiPrec)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_SINGLE_PREC    , 0, SZL_ZB_DATATYPE_SINGLE_PREC    ,  4, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtSinglePrec)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_DOUBLE_PREC    , 0, SZL_ZB_DATATYPE_DOUBLE_PREC    ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtDoublePrec)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_OCTET_STR      , 0, SZL_ZB_DATATYPE_OCTET_STR      , ZABTEST_STRING_LENGTH, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtOctetStr)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_CHAR_STR       , 0, SZL_ZB_DATATYPE_CHAR_STR       , ZABTEST_STRING_LENGTH, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtCharStr)}}},
//{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_LONG_OCTET_STR , 0, SZL_ZB_DATATYPE_LONG_OCTET_STR ,   , {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtLongOctetStr)}}},
//{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_LONG_CHAR_STR  , 0, SZL_ZB_DATATYPE_LONG_CHAR_STR  ,   , {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtLongCharStr)}}},
//{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_ARRAY          , 0, SZL_ZB_DATATYPE_ARRAY          ,   , {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtArray)}}},
//{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_STRUCT         , 0, SZL_ZB_DATATYPE_STRUCT         ,   , {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtStruct)}}},
//{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_SET            , 0, SZL_ZB_DATATYPE_SET            ,   , {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtSet)}}},
//{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_BAG            , 0, SZL_ZB_DATATYPE_BAG            ,   , {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtBag)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_TOD            , 0, SZL_ZB_DATATYPE_TOD            ,  4, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtTOD)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_DATE           , 0, SZL_ZB_DATATYPE_DATE           ,  4, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtDate)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_UTC            , 0, SZL_ZB_DATATYPE_UTC            ,  4, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtUTC)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_CLUSTER_ID     , 0, SZL_ZB_DATATYPE_CLUSTER_ID     ,  2, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtClusterId)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_ATTR_ID        , 0, SZL_ZB_DATATYPE_ATTR_ID        ,  2, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtAttrId)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_BAC_OID        , 0, SZL_ZB_DATATYPE_BAC_OID        ,  4, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtBACOID)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_IEEE_ADDR      , 0, SZL_ZB_DATATYPE_IEEE_ADDR      ,  8, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtIeeeAddr)}}},
{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_128_BIT_SEC_KEY, 0, SZL_ZB_DATATYPE_128_BIT_SEC_KEY, 16, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, Zt128BitSecKey)}}},
//{ZCL_CLUSTER_ID_ZAB_TEST, ATTRID_ZABTEST_UNKNOWN        , 0, SZL_ZB_DATATYPE_UNKNOWN        ,  0, {DP_METHOD_DIRECT, DP_ACCESS_RW, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ZabTest_Attributes_t, ZtUnknown)}}},
};
#define ZAB_TEST_M_NUM_ATTRIBUTES_PER_ENDPOINT ( sizeof(zabTestDatapointConfig) / sizeof(SZL_DataPoint_t))




/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/




/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Plugin Initialisation
 * This function must be called from the application after the Library has been initialized.
 * 
 * Parameters:
 *  Service: Instance of library
 *  numEndpoints: The length of endpointList
 *  endpointList: The endpoints to be initialised with this cluster
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_ZabTestCluster_Init(zabService* Service, 
                                           szl_uint8 numEndpoints, 
                                           szl_uint8 * endpointList)
{
    SZL_RESULT_t result;
    szl_uint8 epIndex;
    szl_uint8 attrIndex;
    szl_uint8 attrChunkIndex;
    SzlPlugin_ZabTest_Params_t * pluginParams = NULL;
    SZL_DataPoint_t serverAttributes[M_NUM_ATTRIBUTES_PER_REGISTRATION];
    szl_bool anyRegisterWorked;
    
    /* Validate inputs */
    if ( (Service == NULL) || (numEndpoints == 0) || (endpointList == NULL) )
      {
        return SZL_RESULT_INVALID_DATA;
      }
    
    /* Malloc parameters the plugin needs to store and link from the SZL */
    result = SZL_PluginRegister(Service, SZL_PLUGIN_ID_APP_F, SzlPlugin_ZabTest_Params_t_SIZE(numEndpoints), (void**)&pluginParams);
    if ( (result == SZL_RESULT_SUCCESS) && (pluginParams != NULL) )
      {
  
        /* Set parameters that are not per endpoint */
        pluginParams->numEndpoints = numEndpoints;

        anyRegisterWorked = szl_false;
        result = SZL_RESULT_SUCCESS;
        for (epIndex = 0; 
             (epIndex < numEndpoints) && (result == SZL_RESULT_SUCCESS || result == SZL_RESULT_ALREADY_EXISTS);  
             epIndex++)
          {

            /* Initialise endpoint number in data
             * All other data is un-initialised - this is up to the app unless we choose to add NVM support later */
            pluginParams->ZabTestEndpoints[epIndex].EndpointNumber = endpointList[epIndex];

            /* Register attributes in chunks to keep the stack usage under control */
            for (attrIndex = 0; attrIndex < ZAB_TEST_M_NUM_ATTRIBUTES_PER_ENDPOINT; attrIndex += M_NUM_ATTRIBUTES_PER_REGISTRATION)
              {

                /* Copy the next chunk of attributes to be registered to RAM
                 * This is mostly the same for each EP, but we set the endpoint number and data pointers differently */
                szl_memcpy(serverAttributes,
                           &zabTestDatapointConfig[attrIndex],
                           sizeof(serverAttributes));

                /* Set the data pointers in to params correctly, based on the endpoint number and attribute ID*/
                for (attrChunkIndex = 0; 
                     (attrChunkIndex < M_NUM_ATTRIBUTES_PER_REGISTRATION) && ((attrIndex + attrChunkIndex) < ZAB_TEST_M_NUM_ATTRIBUTES_PER_ENDPOINT);
                     attrChunkIndex++)
                  {
                    serverAttributes[attrChunkIndex].Endpoint = endpointList[epIndex];
                    serverAttributes[attrChunkIndex].AccessType.DirectAccess.Data = (void*)((szl_uint8*)serverAttributes[attrChunkIndex].AccessType.DirectAccess.Data + (size_t)&pluginParams->ZabTestEndpoints[epIndex].AttributeData);
                  }   

                /* Register the cluster attributes */
                result = SZL_AttributesRegister(Service, serverAttributes, attrChunkIndex);

                /* Keep boolean to show if any register worked, as if it did we must ensure the data it points to is valid */
                if(result == SZL_RESULT_SUCCESS || result == SZL_RESULT_ALREADY_EXISTS)
                  {
                    anyRegisterWorked = szl_true;
                  }
                else
                  {
            printApp(Service, "REg error %x %d\n", result, attrIndex);
                  }
              } 
          } 

        /* If any register worked, we must keep Params to ensure data pointers are valid. If all failed we can free */
        if(anyRegisterWorked == szl_true)
          {
            printInfo(Service, "PLUGIN ZT: Init Successful\n");
            return SZL_STATUS_SUCCESS;
          }
        else
          {
            /* Failed. Cleanup */
            SZL_PluginUnregister(Service, SZL_PLUGIN_ID_ELEC_MEAS);
            result = SZL_RESULT_FAILED;
          }
      }
    
    return result;
}

/******************************************************************************
 * Plugin Destruction
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_ZabTestCluster_Destroy(zabService* Service)
{
  SZL_RESULT_t result;
  szl_uint8 epIndex;
  SZL_EP_t endpoint;
  szl_uint8 attrIndex;
  szl_uint8 attrChunkIndex;
  SzlPlugin_ZabTest_Params_t * pluginParams = NULL;
  SZL_DataPoint_t serverAttributes[M_NUM_ATTRIBUTES_PER_REGISTRATION];
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_APP_F, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
    
  
  for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
    {
      /* Get the endpoint number */
      endpoint = pluginParams->ZabTestEndpoints[epIndex].EndpointNumber;

      /* Register attributes in chunks to keep the stack usage under control */
      for (attrIndex = 0; attrIndex < ZAB_TEST_M_NUM_ATTRIBUTES_PER_ENDPOINT; attrIndex += M_NUM_ATTRIBUTES_PER_REGISTRATION)
        {
          /* Copy the next chunk of attributes to be registered to RAM
           * This is mostly the same for each EP, but we set the endpoint number differently */
          szl_memcpy(serverAttributes,
                     &zabTestDatapointConfig[attrIndex],
                     sizeof(serverAttributes));

          /* Set the data pointers in to params correctly, based on the endpoint number and attribute ID*/
          for (attrChunkIndex = 0; 
               (attrChunkIndex < M_NUM_ATTRIBUTES_PER_REGISTRATION) && ((attrIndex + attrChunkIndex) < ZAB_TEST_M_NUM_ATTRIBUTES_PER_ENDPOINT);
               attrChunkIndex++)
            {
              serverAttributes[attrChunkIndex].Endpoint = endpoint;
            }   

          /* De-Register the cluster attributes.
           * Don't care too much about the result as we will make best effort only */
          result = SZL_AttributesDeregister(Service, serverAttributes, attrChunkIndex);
          if (result != SZL_RESULT_SUCCESS)
            {
              printError(Service, "PLUGIN ZT: WARNING: Deregister failed for EP 0x%02X with Result 0x%02X\n",
                         endpoint,
                         result);
            }
        }
    }
  
  /* Now unregister the plugin */
  return SZL_PluginUnregister(Service, SZL_PLUGIN_ID_APP_F);
}

/******************************************************************************
 * Get pointer to ZabTest Cluster Attributes for an endpoint
 * This can be used to get access to the endpoints parameters for the local 
 *  application to read or write them
 * Return NULL if not found
 ******************************************************************************/
SzlPlugin_ZabTest_Attributes_t* SzlPlugin_ZabTestCluster_GetAttributePointer(zabService* Service, szl_uint8 endpoint)
{
  szl_uint8 epIndex;
  SzlPlugin_ZabTest_Params_t* pluginParams = NULL;
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_APP_F, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return NULL;
    }
  
  for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
    {
      if (pluginParams->ZabTestEndpoints[epIndex].EndpointNumber == endpoint)
        {
          return &pluginParams->ZabTestEndpoints[epIndex].AttributeData;
        }
    }
  return NULL;
}


/******************************************************************************
 * Set values to min, max or typical values
 ******************************************************************************/
void SzlPlugin_ZabTestCluster_SetData(zabService* Service, szl_uint8 endpoint, SzlPlugin_ZabTestSetData_t SetData)
{
  szl_uint8 epIndex;
  SzlPlugin_ZabTest_Params_t* pluginParams = NULL;
  SzlPlugin_ZabTest_Attributes_t* ptr;
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_APP_F, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return;
    }
  
  for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
    {
      if (pluginParams->ZabTestEndpoints[epIndex].EndpointNumber == endpoint)
        {
          ptr = &pluginParams->ZabTestEndpoints[epIndex].AttributeData;
          
          if (SetData == SzlPlugin_ZabTestSetData_Min)
            {
              // ptr->ZtNoData = ;
              ptr->ZtData8 = 0;
              ptr->ZtData16 = 0;
              ptr->ZtData24 = 0;
              ptr->ZtData32 = 0;
              ptr->ZtData40 = 0;
              ptr->ZtData48 = 0;
              ptr->ZtData56 = 0;
              ptr->ZtData64 = 0;
              ptr->ZtBoolean = 0;
              ptr->ZtBitmap8 = 0;
              ptr->ZtBitmap16 = 0;
              ptr->ZtBitmap24 = 0;
              ptr->ZtBitmap32 = 0;
              ptr->ZtBitmap40 = 0;
              ptr->ZtBitmap48 = 0;
              ptr->ZtBitmap56 = 0;
              ptr->ZtBitmap64 = 0;
              ptr->ZtUInt8 = 0;
              ptr->ZtUInt16 = 0;
              ptr->ZtUInt24 = 0;
              ptr->ZtUInt32 = 0;
              ptr->ZtUInt40 = 0;
              ptr->ZtUInt48 = 0;
              ptr->ZtUInt56 = 0;
              ptr->ZtUInt64 = 0;
              ptr->ZtInt8 = INT8_MIN;
              ptr->ZtInt16 = INT16_MIN;
              ptr->ZtInt24 = INT24_MIN;
              ptr->ZtInt32 = INT32_MIN;
              ptr->ZtInt40 = INT40_MIN;
              ptr->ZtInt48 = INT48_MIN;
              ptr->ZtInt56 = INT56_MIN;
              ptr->ZtInt64 = INT64_MIN;
              ptr->ZtEnum8 = 0;
              ptr->ZtEnum16 = 0;
              // ptr->ZtSemiPrec = ;
              ptr->ZtSinglePrec = -2.5e-3;
              ptr->ZtDoublePrec = -2.5e-3;
              ptr->ZtOctetStr[0] = 0;
              ptr->ZtCharStr[0] = 0;
              // ptr->ZtLongOctetStr = ;
              // ptr->ZtLongCharStr = ;
              // ptr->ZtArray = ;
              // ptr->ZtStruct = ;
              // ptr->ZtSet = ;
              // ptr->ZtBag = ;
              ptr->ZtTOD = 0;
              ptr->ZtDate = 0;
              ptr->ZtUTC = 0;
              ptr->ZtClusterId = 0;
              ptr->ZtAttrId = 0;
              ptr->ZtBACOID = 0;
              ptr->ZtIeeeAddr = 0;
              memset(ptr->Zt128BitSecKey, 0, 16);
              //ptr->ZtUnknown = ;
            }
          else if (SetData == SzlPlugin_ZabTestSetData_Max)
            {
              // HIGH
              // ptr->ZtNoData = ;
              ptr->ZtData8 = __UINT8_MAX__;
              ptr->ZtData16 = __UINT16_MAX__;
              ptr->ZtData24 = UINT24_MAX;
              ptr->ZtData32 = __UINT32_MAX__;
              ptr->ZtData40 = UINT40_MAX;
              ptr->ZtData48 = UINT48_MAX;
              ptr->ZtData56 = UINT56_MAX;
              ptr->ZtData64 = __UINT64_MAX__;
              ptr->ZtBoolean = szl_true;
              ptr->ZtBitmap8 = __UINT8_MAX__;
              ptr->ZtBitmap16 = __UINT16_MAX__;
              ptr->ZtBitmap24 = UINT24_MAX;
              ptr->ZtBitmap32 = __UINT32_MAX__;
              ptr->ZtBitmap40 = UINT40_MAX;
              ptr->ZtBitmap48 = UINT48_MAX;
              ptr->ZtBitmap56 = UINT56_MAX;
              ptr->ZtBitmap64 = __UINT64_MAX__;
              ptr->ZtUInt8 = __UINT8_MAX__;
              ptr->ZtUInt16 = __UINT16_MAX__;
              ptr->ZtUInt24 = UINT24_MAX;
              ptr->ZtUInt32 = __UINT32_MAX__;
              ptr->ZtUInt40 = UINT40_MAX;
              ptr->ZtUInt48 = UINT48_MAX;
              ptr->ZtUInt56 = UINT56_MAX;
              ptr->ZtUInt64 = __UINT64_MAX__;
              ptr->ZtInt8 = __INT8_MAX__;
              ptr->ZtInt16 = __INT16_MAX__;
              ptr->ZtInt24 = INT24_MAX;
              ptr->ZtInt32 = __INT32_MAX__;
              ptr->ZtInt40 = INT40_MAX;
              ptr->ZtInt48 = INT48_MAX;
              ptr->ZtInt56 = INT56_MAX;
              ptr->ZtInt64 = __INT64_MAX__;
              ptr->ZtEnum8 = __UINT8_MAX__;
              ptr->ZtEnum16 = __UINT16_MAX__;
              // ptr->ZtSemiPrec = ;
              ptr->ZtSinglePrec = 1575e-2;
              ptr->ZtDoublePrec = 1575e-2;
              snprintf(ptr->ZtOctetStr, ZABTEST_STRING_LENGTH+1, "0123456789ABCDEF0123456789ABCDEF");
              snprintf(ptr->ZtCharStr, ZABTEST_STRING_LENGTH+1, "0123456789ABCDEF0123456789ABCDEF");
              // ptr->ZtLongOctetStr = ;
              // ptr->ZtLongCharStr = ;
              // ptr->ZtArray = ;
              // ptr->ZtStruct = ;
              // ptr->ZtSet = ;
              // ptr->ZtBag = ;
              ptr->ZtTOD = __UINT32_MAX__;
              ptr->ZtDate = __UINT32_MAX__;
              ptr->ZtUTC = __UINT32_MAX__;
              ptr->ZtClusterId = __UINT16_MAX__;
              ptr->ZtAttrId = __UINT16_MAX__;
              ptr->ZtBACOID = __UINT32_MAX__;
              ptr->ZtIeeeAddr = __UINT64_MAX__;
              memset(ptr->Zt128BitSecKey, 0xFF, 16);
              //ptr->ZtUnknown = ;
            }
          else // SzlPlugin_ZabTestSetData_Typical
            {
              // ptr->ZtNoData = ;
              ptr->ZtData8 = 8;
              ptr->ZtData16 = 16;
              ptr->ZtData24 = 24;
              ptr->ZtData32 = 32;
              ptr->ZtData40 = 40;
              ptr->ZtData48 = 48;
              ptr->ZtData56 = 56;
              ptr->ZtData64 = 64;
              ptr->ZtBoolean = szl_true;
              ptr->ZtBitmap8 = 0x81;
              ptr->ZtBitmap16 = 0x8A51;
              ptr->ZtBitmap24 = 0x8A5A51;
              ptr->ZtBitmap32 = 0x8A5A5A51;
              ptr->ZtBitmap40 = 0x8A5A5A5A51;
              ptr->ZtBitmap48 = 0x8A5A5A5A5A51;
              ptr->ZtBitmap56 = 0x8A5A5A5A5A5A51;
              ptr->ZtBitmap64 = 0x8A5A5A5A5A5A5A51;
              ptr->ZtUInt8 =  0x12;
              ptr->ZtUInt16 = 0x1234;
              ptr->ZtUInt24 = 0x123456;
              ptr->ZtUInt32 = 0x12345678;
              ptr->ZtUInt40 = 0x1234567890;
              ptr->ZtUInt48 = 0x1234567890AB;
              ptr->ZtUInt56 = 0x1234567890ABCD;
              ptr->ZtUInt64 = 0x1234567890ABCDEF;
              ptr->ZtInt8 =  -8;
              ptr->ZtInt16 =  1617;
              ptr->ZtInt24 = -242526;
              ptr->ZtInt32 =  32333435;
              ptr->ZtInt40 = -4041424344;
              ptr->ZtInt48 =  484950515253;
              ptr->ZtInt56 = -56575859606162;
              ptr->ZtInt64 =  6465666768697071;
              ptr->ZtEnum8 = 0xA5;
              ptr->ZtEnum16 = 0xA5A5;
              // ptr->ZtSemiPrec = ;
              ptr->ZtSinglePrec = 1234567.8;
              ptr->ZtDoublePrec = 1234567.89123456789;
              snprintf(ptr->ZtOctetStr, ZABTEST_STRING_LENGTH+1, "Hello World");
              snprintf(ptr->ZtCharStr, ZABTEST_STRING_LENGTH+1, "Schneider Electric");
              // ptr->ZtLongOctetStr = ;
              // ptr->ZtLongCharStr = ;
              // ptr->ZtArray = ;
              // ptr->ZtStruct = ;
              // ptr->ZtSet = ;
              // ptr->ZtBag = ;
              ptr->ZtTOD = __UINT32_MAX__;
              ptr->ZtDate = __UINT32_MAX__;
              ptr->ZtUTC = __UINT32_MAX__;
              ptr->ZtClusterId = __UINT16_MAX__;
              ptr->ZtAttrId = __UINT16_MAX__;
              ptr->ZtBACOID = __UINT32_MAX__;
              ptr->ZtIeeeAddr = __UINT64_MAX__;
              memset(ptr->Zt128BitSecKey, 0xFF, 16);
              //ptr->ZtUnknown = ;
            }
          return;
        }
    }
}

/******************************************************************************
 * CLIENT: Read all attributes from a ZabTestCluster server
 ******************************************************************************/
void SzlPlugin_ZabTestCluster_ReadAll(zabService* Service, SZL_EP_t SourceEndpoint, SZL_Addresses_t DestAddrMode)
{
  szl_uint8 attrIndex;
  szl_uint8 attrChunkIndex;
  szl_uint8 tid;
  SZL_RESULT_t res;
  SZL_AttributeReadReqParams_t* readReq;
  readReq = (SZL_AttributeReadReqParams_t *)APP_MEM_GLUE_MALLOC(SZL_AttributeReadReqParams_t_SIZE(M_NUM_ATTRIBUTES_PER_READ), 
                                                                appConsoleMain_GetServiceId(Service),
                                                                MALLOC_ID_APP_GENERAL);
  if (readReq == NULL)
    {
      printApp(Service, "Mem Error\n");
      return;
    }           
  
  readReq->SourceEndpoint = SourceEndpoint;
  readReq->DestAddrMode = DestAddrMode;
  readReq->ManufacturerSpecific = szl_false;
  readReq->ClusterID = ZCL_CLUSTER_ID_ZAB_TEST;

  for (attrIndex = 0; attrIndex < ZAB_TEST_M_NUM_ATTRIBUTES_PER_ENDPOINT; attrIndex += M_NUM_ATTRIBUTES_PER_READ)
    {
    
      for (attrChunkIndex = 0; 
           (attrChunkIndex < M_NUM_ATTRIBUTES_PER_READ) && ((attrIndex + attrChunkIndex) < ZAB_TEST_M_NUM_ATTRIBUTES_PER_ENDPOINT);
           attrChunkIndex++)
        {
          readReq->AttributeIDs[attrChunkIndex] = zabTestDatapointConfig[attrIndex+attrChunkIndex].AttributeID;
        }
      readReq->NumberOfAttributes = attrChunkIndex;
      
      res = SZL_AttributeReadReq(Service, appZcl_ReadAttrRspHandler,readReq, &tid);
      appZcl_PrintSzlResult(Service, "SzlPlugin_ZabTestCluster_ReadAll", res, tid);
    }
  APP_MEM_GLUE_FREE(readReq, appConsoleMain_GetServiceId(Service));
}


/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/