/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the debug print interface for ZAB
 * 
 *   This file is to be copied and modified for a particular implementation of the 
 *   ZigBee Application Brick.
 * 
 *   Applications may define an appropriate printf compatible output for:
 *   printApp: General debug output used by ZAB and should also be used by App.
 *   printError: Used for errors and warnings.
 *   printInfo: Used for general info.
 *   printVendor/printSap: Used for internal debugging.
 * 
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *  00.00.00.02 30-Oct-13   MvdB   Tidy up
 *  00.00.00.03 21-Jan-14   MvdB   Support time stamping on output
 *  00.00.04.00 14-Apr-14   MvdB   Move details into platDebug
 * 002.000.001  03-Feb-15   MvdB   ARTF104099: Add service pointer to print function for multi instance apps.
 *****************************************************************************/

#ifndef __ZAB_DEBUG_H__
#define __ZAB_DEBUG_H__


#include "platDebug.h"

#define printApp( srv, format, ... )     platPrintApp( srv, format, ##__VA_ARGS__ )
#define printError( srv, format, ... )   platPrintError( srv, __FILE__, __FUNCTION__, __LINE__, format, ##__VA_ARGS__ )
#define printInfo( srv, format, ... )    platPrintInfo( srv, format, ##__VA_ARGS__ )
#define printVendor( srv, format, ... )  platPrintVendor( srv, format, ##__VA_ARGS__ )
#define printSap( srv, format, ... )     platPrintSap( srv, format, ##__VA_ARGS__ )


/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif
