
"""
application.application_frame_management
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to treat the received applicative frames (update data model)

"""
from .application_frame import get_address, get_attributes
from json_parser import JsonParser
from .application_frame import Application


class ApplicationFrameManagement(object):
    """Start and stop the layout processes"""

    def __init__(self, application_process):
        """Save application process instance"""
        self._application_process = application_process
        self._json_parser = JsonParser("../topology.json")

    def default_response(self, device_number, payload):
        """Parse the default response frame and treat it

        Args:
            device_number: device number of the wireless bridge
            payload: data of the default response
        """
        if (int(payload[0])
                & Application.OptionAckMask.SOURCE_ADDRESS_PRESENT) != Application.OptionUpAddress.PRESENT:
            return

        if (int(payload[0]) & Application.OptionAckMask.PAYLOAD_PRESENT) != Application.OptionUpPayload.PRESENT:
            return

        if int(payload[2]) != Application.ResponseType.GET_RESPONSE:
            return

        result = get_address(payload, 3)
        # Looking for the right device
        if self._application_process.zigbee_device_container.is_device_saved(id_wireless_bridge=device_number,
                                                                             address=result["address"]):

            device = self._application_process.zigbee_device_container.get_device(id_wireless_bridge=device_number,
                                                                                  address=result["address"])
            # Parse the data of frame and update the data of the device
            data = get_attributes(result["data"])
            device.update_data(data)
            self._json_parser.update_device(device)

    def update_device_data(self, device_number, payload):
        """Parse the update device data frame and treat it

        Args:
            device_number: device number of the wireless bridge
            payload: data of the default response
        """
        result = get_address(payload, 1)
        # Looking for the right device
        if self._application_process.zigbee_device_container.is_device_saved(id_wireless_bridge=device_number,
                                                                             address=result["address"]):

            device = self._application_process.zigbee_device_container.get_device(id_wireless_bridge=device_number,
                                                                                  address=result["address"])
            # Parse the data of the frame and update the data of the device
            data = get_attributes(result["data"])
            device.update_data(data)
            self._json_parser.update_device(device)

    def update_device_link_indicator(self, device_number, payload):
        """Parse the update device link indicator frame and treat it

        Args:
            device_number: device number of the wireless bridge
            payload: data of the default response
        """
        result = get_address(payload, 1)
        # Looking for the right device
        if self._application_process.zigbee_device_container.is_device_saved(id_wireless_bridge=device_number,
                                                                             address=result["address"]):

            device = self._application_process.zigbee_device_container.get_device(id_wireless_bridge=device_number,
                                                                                  address=result["address"])
            # Parse the data of the frame and update the data of the device
            last_hop_lqi = int.from_bytes(result["data"][0:1], byteorder='little', signed=False)
            last_hop_rssi = int.from_bytes(result["data"][1:2], byteorder='little', signed=True)
            device.update_rssi(last_hop_lqi, last_hop_rssi)
            self._json_parser.update_device(device)

    def cluster_command(self, device_number, payload):
        pass
