# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to generate application frames
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from .application_frame_structure import *


class ApplicationFrameGenerator:

    def __init__(self, application_process):
        self._application_process = application_process

    def send_data_frame(self, device_number, payload):

        # Send frame
        self._application_process.send_frame('0x42', '0x00', device_number, payload)
