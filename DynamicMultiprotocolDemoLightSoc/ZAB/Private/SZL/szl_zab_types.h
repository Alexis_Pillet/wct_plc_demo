/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the types for the ZigBee Primitive Interface
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.06.03  01-Oct-14   MvdB   artf104879: Support energy scans via Mgmt Nwk Update Request
 * 002.000.003  06-Mar-15   MvdB   Remove GpAck. Code is no longer used.
 * 002.001.001  15-Jul-15   MvdB   ARTF130228: Add unique address mode for GpSrcId
 * 002.001.004  21-Jul-15   MvdB   ARTF134686: Update plugin allocation method to be generic and protect against multiple initialisation
 * 002.002.015  21-Oct-15   MvdB   ARTF104106: Support SZL_ZDO_MatchDescriptorReq()
 * 002.002.021  21-Apr-16   MvdB   ARTF167807: Support Multi-Cluster Attribute Read/Write for GP
 *                                 Add generic callback type for tidier default code
 * 002.002.031  09-Jan-17   MvdB   ARTF152098: SZL: Improve update of simple descriptors when attributes/commands are registered/deregistered
 *****************************************************************************/
#ifndef _SZL_ZAB_TYPES_H_
#define _SZL_ZAB_TYPES_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "zabCoreService.h"
#include "qSafeUtility.h"
#include "zabParseProPrivate.h"

#include "szl_config.h"
#include "szl_internal.h"
#include "szl_types.h"
#include "szl_zdo.h"
#include "szl_timer_types.h"
#include "szl_plugin.h"
#include "szl_report.h"
#include "cb_handler.h"

#include "wtbUtility.h"


/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/


#define SZL_ZAB_M_TRANSACTION_ID_INVALID 0
#define SZL_ZAB_M_IEEE_INVALID 0
#define SZL_ZAB_M_SRC_ID_INVALID 0

/* ZDO response type - this tells the handler how to cast the void* of the response */
typedef enum
{                                                         /* Cast data* to: */
  SZL_ZAB_ZDO_MESSAGE_TYPE_IEEE_ADDRESS_RESPONSE,             /* SZL_ZdoAddrRespParams_t* */
  SZL_ZAB_ZDO_MESSAGE_TYPE_ACTIVE_ENDPOINT_RESPONSE,          /* sapMsgAppZdoActiveEndpointRsp* */
  SZL_ZAB_ZDO_MESSAGE_TYPE_SIMPLE_DESCRIPTOR_RESPONSE,        /* sapMsgAppZdoSimpleDescriptorRsp* */


  /* All the other ZDO responses... */
}szlZabZdoMessageType_t;

/* The generic response callback, used when generating timeouts etc.
 * Any callback that does not conform to this type will need to be handled specifically */
typedef void (*SZLZAB_CB_GenericResp_t) (zabService* Service, SZL_STATUS_t Status, void* Params, szl_uint8 TransactionId);

/******************************************************************************
 * Synchronous Transaction Type
 * Type of synchronous transaction that is waiting for a response
 ******************************************************************************/
typedef enum
{
  SZL_ZAB_SYNC_TRANS_TYPE_NONE = 0, /* Must be zero or else memset 0 will break */

  /* ZCL Types must be within min and max as this is the range checked for default response*/
  SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MIN,
  SZL_ZAB_SYNC_TRANS_TYPE_ZCL_CLUSTER_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZCL_READ_ATTRIBUTE_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZCL_WRITE_ATTRIBUTE_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZCL_DISCOVER_ATTRIBUTE_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZCL_CONFIGURE_REPORT_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZCL_READ_REPORT_CONFIG_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MAX,

  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_IEEE_ADDRESS_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_NWK_ADDRESS_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_POWER_DESCRIPTOR_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_NODE_DESCRIPTOR_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_ACTIVE_ENDPOINT_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_SIMPLE_DESCRIPTOR_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MATCH_DESCRIPTOR_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_LQI_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_RTG_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_NWK_UPDATE_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_BIND_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_MGMT_LEAVE_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_BIND_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_UNBIND_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_USER_DESCRIPTOR_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZDO_USER_DESCRIPTOR_SET_REQ,

  SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MULTI_CLUSTER_READ_ATTRIBUTE_REQ,
  SZL_ZAB_SYNC_TRANS_TYPE_ZCL_MULTI_CLUSTER_WRITE_ATTRIBUTE_REQ,

}szlZabSyncTransType_t;



/******************************************************************************
 * Synchronous Transaction Callback
 * Union of function pointers for callbacks.
 * Which from to use is defined by szlZabSyncTransType_t
 ******************************************************************************/
typedef union
{
  SZLZAB_CB_GenericResp_t SZLZAB_CB_GenericResp;
  SZL_CB_ClusterCmdResp_t SZL_CB_ClusterCmdResp;
  SZL_CB_AttributeReadResp_t SZL_CB_AttributeReadResp;
  SZL_CB_AttributeWriteResp_t SZL_CB_AttributeWriteResp;
  SZL_CB_AttributeDiscoverResp_t SZL_CB_AttributeDiscoverResp;
  SZL_CB_AttributeReportCfgResp_t SZL_CB_AttributeReportCfgResp;
  SZL_CB_AttributeReadReportCfgResp_t SZL_CB_AttributeReadReportCfgResp;
  SZL_CB_ZdoAddrResp_t SZL_CB_ZdoAddrResp;
  SZL_CB_ZdoPowerDescriptorResp_t SZL_CB_ZdoPowerDescriptorResp;
  SZL_CB_ZdoNodeDescriptorResp_t SZL_CB_ZdoNodeDescriptorResp;
  SZL_CB_ZdoActiveEndpointResp_t SZL_CB_ZdoActiveEndpointResp;
  SZL_CB_ZdoSimpleDescriptorResp_t SZL_CB_ZdoSimpleDescriptorResp;
  SZL_CB_ZdoMatchDescriptorResp_t SZL_CB_ZdoMatchDescriptorResp;
  SZL_CB_ZdoMgmtLqiResp_t SZL_CB_ZdoMgmtLqiResp;
  SZL_CB_ZdoMgmtRtgResp_t SZL_CB_ZdoMgmtRtgResp;
  SZL_CB_ZdoMgmtNwkUpdateResp_EnergyScan_t SZL_CB_ZdoMgmtNwkUpdateResp_EnergyScan;
  SZL_CB_ZdoMgmtBindResp_t SZL_CB_ZdoMgmtBindResp;
  SZL_CB_ZdoUserDescriptorResp_t SZL_CB_ZdoUserDescriptorResp;
  SZL_CB_ZdoUserDescriptorSetResp_t SZL_CB_ZdoUserDescriptorSetResp;
  SZL_CB_NwkLeaveResp_t SZL_CB_NwkLeaveResp;
  SZL_CB_ZdoBindResp_t SZL_CB_ZdoBindResp;
  SZLEXT_CB_MultiCluster_AttributeReadResp_t SZLEXT_CB_MultiCluster_AttributeReadResp;
  SZLEXT_CB_MultiCluster_AttributeWriteResp_t SZLEXT_CB_MultiCluster_AttributeWriteResp;
} syncCallback;

/******************************************************************************
 * Synchronous Transaction
 * Stores callback to be called when response is received or timed out
 ******************************************************************************/
typedef struct
{
  szlZabSyncTransType_t syncTransType;    /* Transaction type */
  szl_uint8 tid;                          /* Transaction ID - Used for Pro requests/responses*/
  szl_uint32 gpSrcId;                     /* Source Id - Used for GP requests/responses */
  szl_uint32 timeout;                     /* Time in seconds at which the event expires (time from osTimeGetSeconds()) */
  syncCallback callback;                  /* Callback for response */
  void* errorResponseParams;              /* Response params to be given in case of error */
}szlZabSyncTransactions_t;

/******************************************************************************
 * Plugin Data
 * REference to an allocated plugin
 ******************************************************************************/
typedef struct
{
  SZL_PLUGIN_ID_t pluginId;               /* Id of the Plugin. May only be one of each Id registered. */
  void* pluginData;                       /* Pointer to data allocated for plugin */
}szlZabPlugin_t;


/******************************************************************************
 * SZL Service
 ******************************************************************************/
typedef struct {


  qSafeType szlDataInQ;      // Messages awaiting processing by SZL

  /* ZCL Raw Handler */
  void (*zcl_RawEventHandler)(erStatus* Status,
                              zabService* Service,
                              szl_bool* suppressProcessing,
                              zabMsgProDataInd* data);


  /* Synchronous Transaction Table */
  szlZabSyncTransactions_t szlZabSyncTransactions[SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS];

  /* Mutex for accessing szl service data */
  szl_uint8 szlServiceDataMutex;

  /* Last used Transaction Id */
  szl_uint8 LastTransactionId;

  /* Attribute/Command changed and endpoint simple descriptors need to be updated */
  szl_bool EndpointUpdateRequired;

  /* SZL stuff - should go somewhere else eventually */
  SZL_CB_AttributeReportNtf_t SZL_CB_AttributeReportNtf;


  SZL_Initialize_t initData;

  struct
  {
    szl_bool Initialized;
    SZL_ClientCluster_t ClientClusters[SZL_CFG_MAX_CLIENT_CLUSTERS];
    SZL_DataPoint_t DataPoints[SZL_CFG_MAX_DATAPOINTS];
    SZL_ClusterCmd_t ClusterCmds[SZL_CFG_MAX_CLUSTER_CMDS];
  }Lib;


  SZL_TIMER_t szlTimer;

  CBInfo CB;

  PLUGIN_TASK_t gPluginTasks[_SZL_PT_HANDLER_COUNT_+1];
  SZL_REPORT_ENTRY_t gReports[SZL_CFG_MAX_REPORTS];

  // Table of registered plugins and their data pointers.
  szlZabPlugin_t Plugins[SZL_CFG_MAX_PLUGINS];


#ifdef ZAB_CFG_ENABLE_WIRELESS_TEST_BENCH
  wtbUtility_DataReqNotification_t DataRequestNotificationCallback;
  wtbUtility_SzlDataErrorNotification_t DataConfirmNotificationCallback;
  wtbUtility_DataIndNotification_t DataIndicationNotificationCallback;
#endif

} szlZabServiceType;

#ifdef __cplusplus
}
#endif

#endif /* _SZL_ZAB_TYPES_H_ */

