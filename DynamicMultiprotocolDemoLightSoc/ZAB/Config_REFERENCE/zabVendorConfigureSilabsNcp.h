

#ifndef __ZAB_VENDOR_CONFIGURE_SILABS_NCP_H__
#define __ZAB_VENDOR_CONFIGURE_SILABS_NCP_H__

#include "ZabCoreConfigure.h"

/******************************************************************************
 *                      *****************************
 *                 ***** NETWORK PROCESSOR SELECTION *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      *****************************
 *                 *****  ENABLE / DISABLE MODULES   *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      *****************************
 *                 *****    NETWORK CONFIGURATION    *****
 *                      *****************************
 ******************************************************************************/

/* The maximum number of non-volatile bindings supported by the stack.
 *
 * Recommended settings:
 *  - Coordinators: 0, unless binding table will be used (not normally).
 *  - Routers: Number of endpoints * number of clusters to be bound * 2 destinations
 *  - Default: 0 */
#define ZAB_VENDOR_BINDING_TABLE_SIZE                 ( 10 )

/******************************************************************************
 *                      *****************************
 *                 *****          OPTIONS            *****
 *                      *****************************
 ******************************************************************************/

/* Enable very verbose printing (translation of enums).
 * Good to have enable at leat for development, but consumes some flash */
#define ZAB_VENDOR_VERBOSE_PRINTING

/******************************************************************************
 *                      *****************************
 *                 *****      BUFFER MANAGEMENT      *****
 *                      *****************************
 ******************************************************************************/

// this is the min and max put into the raw vendor frame buffer in each message
#define ZAB_VENDOR_MAX_DATA                           ( 255 )   // max ZNP message size  = 253 bytes + SOF + FCS
#define ZAB_VENDOR_MIN_DATA                           ( 48 )    // min ZNP message size (most common)

/******************************************************************************
 *                      *****************************
 *                 *****      SERIAL LINK (ASH)      *****
 *                      *****************************
 ******************************************************************************/

/* Time in ms to wait for Serial Acks
 * Default = 5000*/
#define ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS               ( 5000 )

/* Max length of received message */
#define ZAB_VENDOR_MAX_DATA_RX                        ( ZAB_VENDOR_MAX_DATA )

/******************************************************************************
 *                      *****************************
 *                 *****     OPEN STATE MACHINE      *****
 *                      *****************************
 ******************************************************************************/

/* Time in ms to wait for serial glue to open or close before timing out
 * Default = 2000 */
#define ZAB_VND_CFG_M_OPEN_SERIAL_GLUE_TIMEOUT_MS     ( 2000 )

/* Time in ms to wait for a soft reset to complete before timing out
 * Default = 2000 */
#define ZAB_VND_CFG_M_OPEN_SOFT_RST_TIMEOUT_MS        ( 2000 )

/* Time in ms to wait between pinging network processor.
 * This is used to test the serial link is working correctly.
 * Default = 10000
 * Disabled = 0xFFFF */
#define ZAB_VND_CFG_M_NET_PROC_PING_MS                ( 10000 )

/* Time in ms to backoff when retrying ping of network processor.
 * Default = 2000 */
#define ZAB_VND_CFG_M_NET_PROC_PING_BACKOFF_MS        ( 2000 )

/* Number of times to attempt to ping the network processor before considering link broken and resetting ZAB.
 * Default = 3 */
#define ZAB_VND_CFG_M_NET_PROC_PING_FAIL_LIMIT        ( 3 )

/******************************************************************************
 *                      *****************************
 *                 *****    NETWORK STATE MACHINE    *****
 *                      *****************************
 ******************************************************************************/

/* Time in ms to wait for confirms before timing out.
 * Default = 1000 */
#define ZAB_VND_CFG_M_NWK_CONFIRM_TIMEOUT_MS          ( 1000 )

/* Time in ms to wait for confirms during active scan.
 * Needs to be longer in case we scan many empty channels.
 * Default = 10000 */
#define ZAB_VND_CFG_M_ACTIVE_SCAN_CONFIRM_TIMEOUT_MS  ( 10000 )

/* Time in ms to wait for a channel change to complete.
 * Typically takes 10 seconds for SiLabs
 * Default = 15000 */
#define ZAB_VND_CFG_M_CHANNEL_CHANGE_TIMEOUT_MS       ( 15000 )

/* Time in ms to wait between Key Transport and Key Switch commands when changing network key
 * This needs to be very long for silabs!
 * Default = 20000 */
#define ZAB_VND_CFG_M_NETWORK_KEY_SWITCH_DELAY_MS     ( 20000 )

/******************************************************************************
 *                      *****************************
 *                 *****      SBL STATE MACHINE      *****
 *                      *****************************
 ******************************************************************************/

// 1000 is too fast for the last block, which takes around a second
#define ZAB_VND_CFG_M_SBL_BLOCK_ACK_TIMEOUT_MS        ( 2000 )
#define ZAB_VND_CFG_M_SBL_READY_TIMEOUT_MS            ( 5000 )


/******************************************************************************
 *                      *****************************
 *                 *****            OTHER            *****
 *                      *****************************
 ******************************************************************************/

/* Max endpoints and clusters supported in the vendor */
#define ZAB_VND_CFG_MAX_ENDPOINTS                     ( 6 )   // Should be set to the same value as SZL_CFG_ENDPOINT_CNT - TODO - Harmonise somewhere?
#define ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT         ( 16 )  // Should be set to same as SZL_CFG_MAX_CLUSTERS


/* SiLabs say this is 82 and we also get 82 from EZSP_MAXIMUM_PAYLOAD_LENGTH, but for some reason i can only use 80... */
#define MAX_APS_PAYLOAD_LENGTH                        ( 80 )

#endif // __ZAB_VENDOR_CONFIGURE_SILABS_NCP_H__
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/