/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/******************************************************************************
* File Name     : r_config.h
* Device(s)     : RX631
* Tool-chain    : KPIT GNURX-ELF 15.01
* H/W platform  : G-CPX / EU-CPX2 / G-CPX3
* Description   : Configuration Setting
******************************************************************************/

/*
 * Prevent nested inclusions
 */
#ifndef R_CONFIG_H
#define R_CONFIG_H

/******************************************************************************
Macro definitions
******************************************************************************/

/* Hardware configuration. */
/* The following defines are used by the flash API */
/* MCU that is used. */
#define MCU_RX631   (1)

/* MCU subtype */
#define MCU_RX631B  (0)
#define MCU_RX631P  (0)
#define MCU_RX631E  (1)
#define MCU_RX631F  (0)

/* Memory size of the MCU. */
#if MCU_RX631 == 1
#if MCU_RX631B == 1
    #define ROM_SIZE_BYTES  (1048576)
    #define RAM_SIZE_BYTES  (131072)
    #define DF_SIZE_BYTES   (32768)
#elif MCU_RX631P == 1
    #define ROM_SIZE_BYTES  (524288)
    #define RAM_SIZE_BYTES  (65536)
    #define DF_SIZE_BYTES   (32768)
#elif MCU_RX631E == 1
    #define ROM_SIZE_BYTES  (2097152)
    #define RAM_SIZE_BYTES  (131072)
    #define DF_SIZE_BYTES   (32768)
#elif MCU_RX631F == 1
    #define ROM_SIZE_BYTES  (2097152)
    #define RAM_SIZE_BYTES  (262144)
    #define DF_SIZE_BYTES   (32768)
#else
    #error "MCU type not defined"
#endif /* if MCU_RX631B == 1 */
#else
#error "MCU type not defined"
#endif /* if MCU_RX631 == 1 */


/* Memory management defines */
#define R_MEMORY_MAX_ELEMENTS           (20u)            /* Max number of allocated memory elements at one time */
#define R_MEMORY_MAX_INSTANCES          (1u)             /* Max number of memory instances */
#define R_MEM_INSTANCE_DEMO             (0u)             /* Name for memory instance used for serial interface / demo application */

/* CPX communication defines */
#define R_CPX_TX_FW_DL_RETRIES          (5u)             /* Maximum number of firmware download retries */

#define QUEUE_SIZE_TIMER                (16u)            /* Length of queue used for timeout events */

/* Thread timing defines */
#define R_CPX_THREAD_PERIOD             (3u)             /* Period between two thread calls in ms */
#define R_APP_THREAD_PERIOD             (100u)           /* Period between two thread calls in ms */

/* Timer defines */
#define R_MAX_TIMER_ONE_SHOT_INSTANCES  (3u)             /* Number of timer instances for this project */
#define R_HW_TIMER_TICK_PERIOD          (10u)            /* The HW timer tick period in ms of the HW timer */
#define R_TIMER_ID_CPX                  (0u)             /* Id of the first timer instance to be used */
#define R_TIMER_ID_FW_DL                (R_TIMER_ID_CPX) /* Id of the FW DL module, in PRIME only one timer instance is used */
#define R_TIMER_ID_APP                  (1u)             /* Id of the second timer instance to be used */
#define R_TIMER_ID_LED                  (2u)             /* Id of the led timer instance to be used  */

/* Frame lengths. */
#define R_MAX_MTU_SIZE                  (1280u)          /* The maximum number of octets that can be transmitted as ADP payload field */

/* NetXDuo IPv6 stack configuration. */
#define R_NETX_PACKET_POOL_SIZE         (4096u)          /* Size of NetXDuo packet pool in bytes */
#define R_NETX_THREAD_STACK_SIZE        (2048u)          /* Size of NetXDuo thread stack in bytes */
#define R_NETX_UDP_QUEUE_MAXIMUM        (16u)            /* Number of queue items stored in socket operation */

/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Exported global variables
******************************************************************************/

/******************************************************************************
Exported global functions (to be accessed by other files)
******************************************************************************/


#endif /* R_CONFIG_H */

