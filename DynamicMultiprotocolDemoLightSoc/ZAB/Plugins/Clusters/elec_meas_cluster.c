/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the Electrical Measurement Cluster.
 *   This plugin is originally designed for Nova (partner).
 * 
 *   It supports:
 *    - Multiple, application specified endpoints
 *    - Standard attributes
 *    - =S= MS attributes defined in ZigBee&GreenPower_Invariants_Partner_Products_V05.pdf
 *    - Default report configuration for time based reports (fast or slow)
 *    - Callback notification to app when a writable attribute is changed
 * 
 *   It does not (currently) support:
 *    - Non-volatile backing of any data.
 *    - Report on change. These are not required for Nova, but would be required for certification.
 *
 * USAGE:
 *   Usage of this plugin is very simple:
 *    1. Initialise it with SzlPlugin_ElecMeasClusterInit(), specify the endpoints and a write notification callback
 *    2. Use SzlPlugin_ElecMeasCluster_GetAttributePointer() to get access to the data for each endpoint.
 *    3. Populate the data
 *    4. Call SzlPlugin_ElecMeasCluster_ConfigureReporting() to start attribute reporting
 *    5. Update data as it changes, the plugin will do the rest.
 * 
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         29-Sep-13   MvdB   Original
 *    2         19-May-14   MvdB   Remove endpoint number from public attribute data
 *    3         30-Oct-14   MvdB   artf74202: Support callbacks in Nova plugins
 *    4         01-Apr-15   MvdB   ARTF116256: Ensure SZL uses szl_memset and szl_memcpy rather than memset and memcpy
 *    5         20-Jul-15   MvdB   ARTF134686: Update allocation method to be generic and protect against multiple initialisation
 * 002.001.006  27-Jul-15   MvdB   ARTF134686: Standardise on SZL_RESULT_t for plugin init return value
 *                                 ARTF138318: Improve plugin destroy, add disable reporting function
 *****************************************************************************/

#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include "zabCoreService.h"
#include "zabCorePrivate.h"
#include "szl.h"
#include "szl_plugin.h"
#include "szl_report.h"

#include "elec_meas_cluster.h"

/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/

/* Fast and slow reporting periods
 * Only time is applied, reportable change is not used */
#define ELEC_MEAS_M_FAST_REPORT_TIME_S 10
#define ELEC_MEAS_M_SLOW_REPORT_TIME_S 30

/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Cluster ID */
#define ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT     0x0B04

typedef struct
{
  SzlPlugin_ElecMeas_Attributes_t AttributeData;
  szl_uint8 EndpointNumber;
}SzlPlugin_ElecMeas_Endpoint_t;

/* Parameters for the service */
typedef struct
{
  szl_uint8 numEndpoints;
#ifdef ELEC_MEAS_M_USE_DIRECT_ACCESS
  SzlPlugin_ElecMeasCluster_AttributeWriteNotification_CB_t AttributeWriteCallback;
  SzlPlugin_ElecMeas_Endpoint_t ElecMeasEndpoints[VLA_INIT];
#else
  szl_uint8 Endpoints[VLA_INIT];
#endif
}SzlPlugin_ElecMeas_Params_t;

#ifdef ELEC_MEAS_M_USE_DIRECT_ACCESS
#define SzlPlugin_ElecMeas_Params_t_SIZE(numEndpoints) (sizeof(SzlPlugin_ElecMeas_Params_t) - (VLA_INIT * sizeof(SzlPlugin_ElecMeas_Endpoint_t))+ (sizeof(SzlPlugin_ElecMeas_Endpoint_t) * (numEndpoints)))
#else
#define SzlPlugin_ElecMeas_Params_t_SIZE(numEndpoints) (sizeof(SzlPlugin_ElecMeas_Params_t) - (VLA_INIT * sizeof(szl_uint8))+ (sizeof(szl_uint8) * (numEndpoints)))
#endif

/* 
 * The set of attributes for the cluster.
 * This const data is used to initialise the majority of the attribute table.
 * Once it is copied into RAM, Endpoint and Data Access pointers are set for each attribute.
 * 
 * Read only attributes use direct access by SZL.
 * Writable attributes use Read/Write callbacks, as the need the write callback to notify the plugin that
 *   a write is happening, so it can notify the app.
 * 
 * New attributes must be added here.
 */
const SZL_DataPoint_t elecMeasDatapointConfig[] = {
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_MeasurementType,        0,  SZL_ZB_DATATYPE_BITMAP32, 4,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, MeasurementType)}}},
                      
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ACFrequency,            0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ACFrequency)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ACFrequencyMin,         0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ACFrequencyMin)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ACFrequencyMax,         0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ACFrequencyMax)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_NeutralCurrent,         0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, NeutralCurrent)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_TotalActivePower,       0,  SZL_ZB_DATATYPE_INT32,    4,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, TotalActivePower)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_TotalReActivePower,     0,  SZL_ZB_DATATYPE_INT32,    4,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, TotalReActivePower)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_TotalApparentPower,     0,  SZL_ZB_DATATYPE_UINT32,   4,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, TotalApparentPower)}}},
                      
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ACFrequencyMultiplier,  0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ACFrequencyMultiplier)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ACFrequencyDivisor,     0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ACFrequencyDivisor)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_PowerMultiplier,        0,  SZL_ZB_DATATYPE_UINT32,   4,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, PowerMultiplier)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_PowerDivisor,           0,  SZL_ZB_DATATYPE_UINT32,   4,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, PowerDivisor)}}},
                      
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltagePhA,          0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltagePhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageMinPhA,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageMinPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageMaxPhA,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageMaxPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSCurrentPhA,          0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSCurrentPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSCurrentMinPhA,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSCurrentMinPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSCurrentMaxPhA,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSCurrentMaxPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ActivePowerPhA,         0,  SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ActivePowerPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ActivePowerMinPhA,      0,  SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ActivePowerMinPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ActivePowerMaxPhA,      0,  SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ActivePowerMaxPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ReActivePowerPhA,       0,  SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ReActivePowerPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ApparentPowerPhA,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ApparentPowerPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_PowerFactorPhA,         0,  SZL_ZB_DATATYPE_INT8,     1,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, PowerFactorPhA)}}},
                      
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ACVoltageMultiplierPhA, 0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ACVoltageMultiplierPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ACVoltageDivisorPhA,    0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ACVoltageDivisorPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ACCurrentMultiplierPhA, 0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ACCurrentMultiplierPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ACCurrentDivisorPhA,    0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ACCurrentDivisorPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ACPowerMultiplierPhA,   0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ACPowerMultiplierPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ACPowerDivisorPhA,      0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ACPowerDivisorPhA)}}},
                      
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltagePhB,          0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltagePhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageMinPhB,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageMinPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageMaxPhB,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageMaxPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSCurrentPhB,          0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSCurrentPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSCurrentMinPhB,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSCurrentMinPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSCurrentMaxPhB,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSCurrentMaxPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ActivePowerPhB,         0,  SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ActivePowerPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ActivePowerMinPhB,      0,  SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ActivePowerMinPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ActivePowerMaxPhB,      0,  SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ActivePowerMaxPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ReActivePowerPhB,       0,  SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ReActivePowerPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ApparentPowerPhB,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ApparentPowerPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_PowerFactorPhB,         0,  SZL_ZB_DATATYPE_INT8,     1,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, PowerFactorPhB)}}},
                                                                                                                                             
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltagePhC,          0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltagePhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageMinPhC,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageMinPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageMaxPhC,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageMaxPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSCurrentPhC,          0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSCurrentPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSCurrentMinPhC,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSCurrentMinPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSCurrentMaxPhC,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSCurrentMaxPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ActivePowerPhC,         0,  SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ActivePowerPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ActivePowerMinPhC,      0,  SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ActivePowerMinPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ActivePowerMaxPhC,      0,  SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ActivePowerMaxPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ReActivePowerPhC,       0,  SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ReActivePowerPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ApparentPowerPhC,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ApparentPowerPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_PowerFactorPhC,         0,  SZL_ZB_DATATYPE_INT8,     1,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, PowerFactorPhC)}}},

  /* Schneider Manufacturer Specific - See ZigBee&GreenPower_Invariants_Partner_Products_V06.pdf */
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_TotalActivePowerDemand, 0,  SZL_ZB_DATATYPE_INT32,    4,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, TotalActivePowerDemand)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_TotalActivePowerMin,    0,  SZL_ZB_DATATYPE_INT32,    4,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, TotalActivePowerMin)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_TotalActivePowerMax,    0,  SZL_ZB_DATATYPE_INT32,    4,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, TotalActivePowerMax)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_TotalReactivePowerDemand,0, SZL_ZB_DATATYPE_INT32,    4,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, TotalReActivePowerDemand)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_TotalActivePowerFundamental,0,SZL_ZB_DATATYPE_INT32,  4,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, TotalActivePowerFundamental)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_TotalReactivePowerFundamental,0,SZL_ZB_DATATYPE_INT32,4,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, TotalReActivePowerFundamental)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFun3PhAvg,    0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFun3PhAvg)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundNeutral,  0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundNeutral)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundNeutralMin,0, SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundNeutralMin)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundNeutralMax,0, SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundNeutralMax)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundNeutralAvg,0, SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundNeutralAvg)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_NeutralCurrentMin,      0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, NeutralCurrentMin)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_NeutralCurrentMax,      0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, NeutralCurrentMax)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_NeutralCurrentAvg,      0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, NeutralCurrentAvg)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_GroundCurrent,          0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, GroundCurrent)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_GroundCurrentMin,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, GroundCurrentMin)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_GroundCurrentMax,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, GroundCurrentMax)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_GroundCurrentAvg,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, GroundCurrentAvg)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ACFrequencyAvg,         0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ACFrequencyAvg)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_Avg3RMSVoltagePhN,      0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, Avg3RMSVoltagePhN)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_Avg3RMSVoltagePhPh,     0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, Avg3RMSVoltagePhPh)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_Avg3RMSCurrent,         0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, Avg3RMSCurrent)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentRMSNeutral,   0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentRMSNeutral)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentRMS3PhAvg,    0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentRMS3PhAvg)}}},
                      
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ActivePowerFundamental, 0,  SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ActivePowerFundamental)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ReactivePowerFundamental,0, SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ReActivePowerFundamental)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageAvg,          0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageAvg)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSCurrentAvg,          0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSCurrentAvg)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundPhA,      0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundPhAMin,   0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundPhAMin)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundPhAMax,   0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundPhAMax)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundPhAAvg,   0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundPhAAvg)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentRMSPhA,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentRMSPhA)}}},
                      
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ACAlarmsMask,           0,  SZL_ZB_DATATYPE_BITMAP16, 2,  {DP_METHOD_DIRECT, DP_ACCESS_RW,   szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ACAlarmsMask)}}},
                      
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ActivePowerFundamentalPhB,0,SZL_ZB_DATATYPE_INT16,    2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ActivePowerFundamentalPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ReactivePowerFundamentalPhB,0,SZL_ZB_DATATYPE_INT16,  2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ReActivePowerFundamentalPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageAvgPhB,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageAvgPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSCurrentAvgPhB,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSCurrentAvgPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundPhB,      0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundPhBMin,   0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundPhBMin)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundPhBMax,   0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundPhBMax)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundPhBAvg,   0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundPhBAvg)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentRMSPhB,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentRMSPhB)}}},
                      
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ActivePowerFundamentalPhC,  0,SZL_ZB_DATATYPE_INT16,  2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ActivePowerFundamentalPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_ReactivePowerFundamentalPhC,  0,SZL_ZB_DATATYPE_INT16,2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, ReActivePowerFundamentalPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageAvgPhC,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageAvgPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSCurrentAvgPhC,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSCurrentAvgPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundPhC,      0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundPhCMin,   0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundPhCMin)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundPhCMax,   0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundPhCMax)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentFundPhCAvg,   0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentFundPhCAvg)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_THDCurrentRMSPhC,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, THDCurrentRMSPhC)}}},
                      
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltagePhAPhB,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltagePhAPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageMinPhAPhB,    0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageMinPhAPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageMaxPhAPhB,    0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageMaxPhAPhB)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageAvgPhAPhB,    0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageAvgPhAPhB)}}},
                      
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltagePhBPhC,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltagePhBPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageMinPhBPhC,    0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageMinPhBPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageMaxPhBPhC,    0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageMaxPhBPhC)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageAvgPhBPhC,    0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageAvgPhBPhC)}}},
                      
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltagePhCPhA,       0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltagePhCPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageMinPhCPhA,    0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageMinPhCPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageMaxPhCPhA,    0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageMaxPhCPhA)}}},
{ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT,  ATTRID_ELEC_MEAS_RMSVoltageAvgPhCPhA,    0,  SZL_ZB_DATATYPE_UINT16,   2,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER},  {.DirectAccess = {(void*)offsetof(SzlPlugin_ElecMeas_Attributes_t, RMSVoltageAvgPhCPhA)}}}
};
#define ELEC_MEAS_M_NUM_ATTRIBUTES_PER_ENDPOINT ( sizeof(elecMeasDatapointConfig) / sizeof(SZL_DataPoint_t))

/* Reporting Configuration Struct - used to define default reports */
typedef struct
{
  szl_uint16 AttributeID;
  szl_uint16 MinTime;
  szl_uint16 MaxTime;
  /*
  // Reportable change values - not supported for now as Nova only requires timed reports
  union 
    {
      szl_uint16 RCuint16;
      szl_int32 RCint32;
      szl_uint32 RCuint32;
    };*/
}ReportingConfiguration_t;

/* Default Reporting Configuration
 * If an attribute is to be reported, it should be added to this table. */
const ReportingConfiguration_t DefaultReportingConfiguration[] = {
//{ATTRID_ELEC_MEAS_MeasurementType,                  ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},

{ATTRID_ELEC_MEAS_ACFrequency,                      ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
  
{ATTRID_ELEC_MEAS_ACFrequencyMin,                   ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ACFrequencyMax,                   ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_NeutralCurrent,                   ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_TotalActivePower,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_TotalReActivePower,               ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
//{ATTRID_ELEC_MEAS_TotalApparentPower,               ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},

{ATTRID_ELEC_MEAS_ACFrequencyMultiplier,            ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ACFrequencyDivisor,               ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_PowerMultiplier,                  ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_PowerDivisor,                     ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},

{ATTRID_ELEC_MEAS_RMSVoltagePhA,                    ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageMinPhA,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageMaxPhA,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSCurrentPhA,                    ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSCurrentMinPhA,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSCurrentMaxPhA,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ActivePowerPhA,                   ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},


{ATTRID_ELEC_MEAS_ActivePowerMinPhA,                ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ActivePowerMaxPhA,                ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
// ok
{ATTRID_ELEC_MEAS_ReActivePowerPhA,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},

{ATTRID_ELEC_MEAS_ApparentPowerPhA,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_PowerFactorPhA,                   ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},

 //fail
{ATTRID_ELEC_MEAS_ACVoltageMultiplierPhA,           ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ACVoltageDivisorPhA,              ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},

  
{ATTRID_ELEC_MEAS_ACCurrentMultiplierPhA,           ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ACCurrentDivisorPhA,              ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ACPowerMultiplierPhA,             ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ACPowerDivisorPhA,                ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},


{ATTRID_ELEC_MEAS_RMSVoltagePhB,                    ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageMinPhB,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageMaxPhB,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSCurrentPhB,                    ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSCurrentMinPhB,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSCurrentMaxPhB,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ActivePowerPhB,                   ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ActivePowerMinPhB,                ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ActivePowerMaxPhB,                ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ReActivePowerPhB,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ApparentPowerPhB,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_PowerFactorPhB,                   ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},

{ATTRID_ELEC_MEAS_RMSVoltagePhC,                    ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageMinPhC,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageMaxPhC,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSCurrentPhC,                    ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSCurrentMinPhC,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSCurrentMaxPhC,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ActivePowerPhC,                   ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ActivePowerMinPhC,                ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ActivePowerMaxPhC,                ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ReActivePowerPhC,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ApparentPowerPhC,                 ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_PowerFactorPhC,                   ELEC_MEAS_M_FAST_REPORT_TIME_S, ELEC_MEAS_M_FAST_REPORT_TIME_S},


/* MS */
{ATTRID_ELEC_MEAS_TotalActivePowerDemand,           ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_TotalActivePowerMin,              ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_TotalActivePowerMax,              ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_TotalReactivePowerDemand,          ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_TotalActivePowerFundamental,      ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_TotalReactivePowerFundamental,    ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFun3PhAvg,              ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundNeutral,            ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundNeutralMin,          ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundNeutralMax,          ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundNeutralAvg,          ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_NeutralCurrentMin,                ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_NeutralCurrentMax,                ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_NeutralCurrentAvg,                ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_GroundCurrent,                    ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_GroundCurrentMin,                 ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_GroundCurrentMax,                 ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_GroundCurrentAvg,                 ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ACFrequencyAvg,                   ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_Avg3RMSVoltagePhN,                ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_Avg3RMSVoltagePhPh,               ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_Avg3RMSCurrent,                   ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentRMSNeutral,             ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentRMS3PhAvg,              ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},

{ATTRID_ELEC_MEAS_ActivePowerFundamental,           ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ReactivePowerFundamental,          ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageAvg,                    ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSCurrentAvg,                    ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundPhA,                ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundPhAMin,             ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundPhAMax,             ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundPhAAvg,             ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentRMSPhA,                 ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},

//{ATTRID_ELEC_MEAS_ACAlarmsMask,                     ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},

{ATTRID_ELEC_MEAS_ActivePowerFundamentalPhB,        ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ReactivePowerFundamentalPhB,      ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageAvgPhB,                 ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSCurrentAvgPhB,                 ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundPhB,                ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundPhBMin,             ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundPhBMax,             ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundPhBAvg,             ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentRMSPhB,                 ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},

{ATTRID_ELEC_MEAS_ActivePowerFundamentalPhC,        ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_ReactivePowerFundamentalPhC,      ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageAvgPhC,                 ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSCurrentAvgPhC,                 ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundPhC,                ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundPhCMin,             ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundPhCMax,             ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentFundPhCAvg,             ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_THDCurrentRMSPhC,                 ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},

{ATTRID_ELEC_MEAS_RMSVoltagePhAPhB,                 ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageMinPhAPhB,              ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageMaxPhAPhB,              ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageAvgPhAPhB,              ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},

{ATTRID_ELEC_MEAS_RMSVoltagePhBPhC,                 ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageMinPhBPhC,              ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageMaxPhBPhC,              ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageAvgPhBPhC,              ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},

{ATTRID_ELEC_MEAS_RMSVoltagePhCPhA,                 ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageMinPhCPhA,              ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageMaxPhCPhA,              ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
{ATTRID_ELEC_MEAS_RMSVoltageAvgPhCPhA,              ELEC_MEAS_M_SLOW_REPORT_TIME_S, ELEC_MEAS_M_SLOW_REPORT_TIME_S},
};
#define ELEC_MEAS_M_NUM_DEFAULT_REPORTS (sizeof(DefaultReportingConfiguration) / sizeof(ReportingConfiguration_t))



/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Attribute Changed Notification Handler
 ******************************************************************************/
#ifdef ELEC_MEAS_M_USE_DIRECT_ACCESS
static void AttributeChangedNotificationHandler(zabService* Service, struct _SZL_AttributeChangedNtfParams_t* Params)
{
  SzlPlugin_ElecMeas_Params_t * pluginParams;
  
  /* Validate inputs */
  if ( (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_ELEC_MEAS, (void**)&pluginParams) == SZL_RESULT_SUCCESS) &&
       (pluginParams != NULL) &&
       (pluginParams->AttributeWriteCallback != NULL) )
    {
      pluginParams->AttributeWriteCallback(Service,
                                           pluginParams->Endpoint,
                                           pluginParams->AttributeID);
    }
}
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/
#define M_NUM_ATTRIBUTES_PER_REGISTRATION 10
/******************************************************************************
 * Plugin Initialisation
 * This function must be called from the application after the Library has been initialized.
 * 
 * Parameters:
 *  Service: Instance of library
 *  numEndpoints: The length of endpointList
 *  endpointList: The endpoints to be initialised with this cluster
 *  WriteAttributesCallback: Callback will be called to notify application that a writable attribute has been written from ZigBee.
 *                           It is optional. If the app does not want to be notified this may be set to NULL.
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_ElecMeasClusterInit(zabService* Service, 
                                           szl_uint8 numEndpoints, 
                                           szl_uint8 * endpointList,
#ifdef ELEC_MEAS_M_USE_DIRECT_ACCESS
                                           SzlPlugin_ElecMeasCluster_AttributeWriteNotification_CB_t WriteAttributesCallback
#else
                                           SZL_CB_DataPointRead_t ReadCallback,
                                           SZL_CB_DataPointWrite_t WriteCallback
#endif
                                           )
{
    SZL_RESULT_t result;
    szl_uint8 epIndex;
    szl_uint8 attrIndex;
    szl_uint8 attrChunkIndex;
    SzlPlugin_ElecMeas_Params_t * pluginParams = NULL;
    SZL_DataPoint_t serverAttributes[M_NUM_ATTRIBUTES_PER_REGISTRATION];
    szl_bool anyRegisterWorked;
    
    /* Validate inputs */
    if ( (Service == NULL) || (numEndpoints == 0) || (endpointList == NULL) 
#ifndef ELEC_MEAS_M_USE_DIRECT_ACCESS 
          || (ReadCallback == NULL) || (WriteCallback == NULL)                                             
#endif
                                                      )
      {
        return SZL_RESULT_INVALID_DATA;
      }
    
    /* Malloc parameters the plugin needs to store and link from the SZL */
    result = SZL_PluginRegister(Service, SZL_PLUGIN_ID_ELEC_MEAS, SzlPlugin_ElecMeas_Params_t_SIZE(numEndpoints), (void**)&pluginParams);
    if ( (result == SZL_RESULT_SUCCESS) && (pluginParams != NULL) )
      {
  
        /* Set parameters that are not per endpoint */
        pluginParams->numEndpoints = numEndpoints;

#ifdef ELEC_MEAS_M_USE_DIRECT_ACCESS
        pluginParams->AttributeWriteCallback = WriteAttributesCallback;
        if (WriteAttributesCallback != NULL)
          {
            if (SZL_AttributeChangedNtfRegisterCB(Service, AttributeChangedNotificationHandler) == SZL_RESULT_TABLE_FULL)
              {
                printError(Service, "PLUGIN EM: Init failed - callback table full\n");
                SZL_PluginUnregister(Service, SZL_PLUGIN_ID_ELEC_MEAS);
                return SZL_RESULT_TABLE_FULL;
              }
          }
#else
        szl_memcpy(pluginParams->Endpoints, endpointList, numEndpoints);
#endif

        anyRegisterWorked = szl_false;
        result = SZL_RESULT_SUCCESS;
        for (epIndex = 0; 
             (epIndex < numEndpoints) && (result == SZL_RESULT_SUCCESS || result == SZL_RESULT_ALREADY_EXISTS);  
             epIndex++)
          {

            /* Initialise endpoint number in data
             * All other data is un-initialised - this is up to the app unless we choose to add NVM support later */
#ifdef ELEC_MEAS_M_USE_DIRECT_ACCESS
            pluginParams->ElecMeasEndpoints[epIndex].EndpointNumber = endpointList[epIndex];
#else     
            pluginParams->Endpoints[epIndex] = endpointList[epIndex];       
#endif

            /* Register attributes in chunks to keep the stack usage under control */
            for (attrIndex = 0; attrIndex < ELEC_MEAS_M_NUM_ATTRIBUTES_PER_ENDPOINT; attrIndex += M_NUM_ATTRIBUTES_PER_REGISTRATION)
              {

                /* Copy the next chunk of attributes to be registered to RAM
                 * This is mostly the same for each EP, but we set the endpoint number and data pointers differently */
                szl_memcpy(serverAttributes,
                           &elecMeasDatapointConfig[attrIndex],
                           sizeof(serverAttributes));

                /* Set the data pointers in to params correctly, based on the endpoint number and attribute ID*/
                for (attrChunkIndex = 0; 
                     (attrChunkIndex < M_NUM_ATTRIBUTES_PER_REGISTRATION) && ((attrIndex + attrChunkIndex) < ELEC_MEAS_M_NUM_ATTRIBUTES_PER_ENDPOINT);
                     attrChunkIndex++)
                  {
                    serverAttributes[attrChunkIndex].Endpoint = endpointList[epIndex];

#ifdef ELEC_MEAS_M_USE_DIRECT_ACCESS
                    serverAttributes[attrChunkIndex].AccessType.DirectAccess.Data = (void*)((szl_uint8*)serverAttributes[attrChunkIndex].AccessType.DirectAccess.Data + (size_t)&params->ElecMeasEndpoints[epIndex].AttributeData);
#else
                    serverAttributes[attrChunkIndex].Property.Method = DP_METHOD_CALLBACK;
                    serverAttributes[attrChunkIndex].AccessType.Callback.Read = ReadCallback;
                    serverAttributes[attrChunkIndex].AccessType.Callback.Write = WriteCallback;       
#endif
                  }   

                /* Register the cluster attributes */
                result = SZL_AttributesRegister(Service, serverAttributes, attrChunkIndex);

                /* Keep boolean to show if any register worked, as if it did we must ensure the data it points to is valid */
                if(result == SZL_RESULT_SUCCESS || result == SZL_RESULT_ALREADY_EXISTS)
                  {
                    anyRegisterWorked = szl_true;
                  }
              } 
          } 

        /* If any register worked, we must keep Params to ensure data pointers are valid. If all failed we can free */
        if(anyRegisterWorked == szl_true)
          {
            printInfo(Service, "PLUGIN EM: Init Successful\n");
            return SZL_STATUS_SUCCESS;
          }
        else
          {
            /* Failed. Cleanup */
            SZL_PluginUnregister(Service, SZL_PLUGIN_ID_ELEC_MEAS);
            result = SZL_RESULT_FAILED;
          }
      }
    
    return result;
}

/******************************************************************************
 * Plugin Destruction
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_ElecMeasCluster_Destroy(zabService* Service)
{
  SZL_RESULT_t result;
  szl_uint8 epIndex;
  SZL_EP_t endpoint;
  szl_uint8 attrIndex;
  szl_uint8 attrChunkIndex;
  SzlPlugin_ElecMeas_Params_t * pluginParams = NULL;
  SZL_DataPoint_t serverAttributes[M_NUM_ATTRIBUTES_PER_REGISTRATION];
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_ELEC_MEAS, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
    
  
  for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
    {
      /* Get the endpoint number */
#ifdef ELEC_MEAS_M_USE_DIRECT_ACCESS 
      endpoint = pluginParams->ElecMeasEndpoints[epIndex].EndpointNumber;
#else
      endpoint = pluginParams->Endpoints[epIndex];
#endif

      /* Register attributes in chunks to keep the stack usage under control */
      for (attrIndex = 0; attrIndex < ELEC_MEAS_M_NUM_ATTRIBUTES_PER_ENDPOINT; attrIndex += M_NUM_ATTRIBUTES_PER_REGISTRATION)
        {
          /* Copy the next chunk of attributes to be registered to RAM
           * This is mostly the same for each EP, but we set the endpoint number differently */
          szl_memcpy(serverAttributes,
                     &elecMeasDatapointConfig[attrIndex],
                     sizeof(serverAttributes));

          /* Set the data pointers in to params correctly, based on the endpoint number and attribute ID*/
          for (attrChunkIndex = 0; 
               (attrChunkIndex < M_NUM_ATTRIBUTES_PER_REGISTRATION) && ((attrIndex + attrChunkIndex) < ELEC_MEAS_M_NUM_ATTRIBUTES_PER_ENDPOINT);
               attrChunkIndex++)
            {
              serverAttributes[attrChunkIndex].Endpoint = endpoint;
            }   

          /* De-Register the cluster attributes.
           * Don't care too much about the result as we will make best effort only */
          result = SZL_AttributesDeregister(Service, serverAttributes, attrChunkIndex);
          if (result != SZL_RESULT_SUCCESS)
            {
              printError(Service, "PLUGIN ELEC MEAS: WARNING: Deregister failed for EP 0x%02X with Result 0x%02X\n",
                         endpoint,
                         result);
            }
        }
    }
  
  /* Now unregister the plugin */
  return SZL_PluginUnregister(Service, SZL_PLUGIN_ID_ELEC_MEAS);
}

/******************************************************************************
 * Get pointer to Electrical Measurement Cluster Attributes for an endpoint
 * This can be used to get access to the endpoints parameters for the local 
 *  application to read or write them
 * Return NULL if not found
 ******************************************************************************/
#ifdef ELEC_MEAS_M_USE_DIRECT_ACCESS
SzlPlugin_ElecMeas_Attributes_t* SzlPlugin_ElecMeasCluster_GetAttributePointer(zabService* Service, szl_uint8 endpoint)
{
  szl_uint8 epIndex;
  SzlPlugin_ElecMeas_Params_t * pluginParams = NULL;
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_ELEC_MEAS, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return NULL;
    }
  
  for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
    {
      if (pluginParams->ElecMeasEndpoints[epIndex].EndpointNumber == endpoint)
        {
          return &pluginParams->ElecMeasEndpoints[epIndex].AttributeData;
        }
    }
  return NULL;
}
#endif

/******************************************************************************
 * Configure reporting for the cluster.
 * Data must be initialised before this function is called to avoid reporting uninitialised values
 * 
 * This function currently only configures a time based default report.
 * If report on change is required the plugin must be extended.
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_ElecMeasCluster_ConfigureReporting(zabService* Service)
{
  szl_uint8 epIndex;
  szl_uint8 reportIndex;
  szl_uint8 attrIndex;
  szl_uint8 endpoint;
  SZL_STATUS_t sts;
  szl_bool manufacturerSpecific;
  SzlPlugin_ElecMeas_Params_t * pluginParams = NULL;
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_ELEC_MEAS, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /*
   * Configure default reports:
   * For each default report in DefaultReportingConfiguration[]
   * Find the matching attribute in elecMeasDatapointConfig[]
   * Use values from elecMeasDatapointConfig and DefaultReportingConfiguration to configure reports
   * Repeat for each endpoint
   */
  for (reportIndex = 0; reportIndex < ELEC_MEAS_M_NUM_DEFAULT_REPORTS; reportIndex++)
    {
      for (attrIndex = 0; attrIndex < ELEC_MEAS_M_NUM_ATTRIBUTES_PER_ENDPOINT; attrIndex++)
        {
          if (DefaultReportingConfiguration[reportIndex].AttributeID == elecMeasDatapointConfig[attrIndex].AttributeID)
            {
              //Match found
              break;
            }
        }
      
      // If we found a match, then attrIndex will be < ELEC_MEAS_M_NUM_ATTRIBUTES_PER_ENDPOINT
      if (attrIndex < ELEC_MEAS_M_NUM_ATTRIBUTES_PER_ENDPOINT)
        {
        
          /* Register the default report for each endpoint */
          for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
            {
#ifdef ELEC_MEAS_M_USE_DIRECT_ACCESS
              endpoint = pluginParams->ElecMeasEndpoints[epIndex].EndpointNumber;
#else
              endpoint = pluginParams->Endpoints[epIndex];
#endif
              
              if (elecMeasDatapointConfig[attrIndex].Property.ManufacturerSpecific == szl_true)
                {
                  manufacturerSpecific = szl_true;
                }
              else
                {
                  manufacturerSpecific = szl_false;
                }
              

              sts = Szl_ReportSendingConfigure(Service, 
                                                  endpoint, 
                                                  manufacturerSpecific,
                                                  ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT, 
                                                  DefaultReportingConfiguration[reportIndex].AttributeID,         
                                                  elecMeasDatapointConfig[attrIndex].DataType, 
                                                  DefaultReportingConfiguration[reportIndex].MinTime, 
                                                  DefaultReportingConfiguration[reportIndex].MaxTime, 
                                                  NULL); // Reportable change not used for now as not required by Nova
              if (sts != SZL_STATUS_SUCCESS)
                {
                  printError(Service, "PLUGIN EM: ERROR - Default report configuration for AttrID 0x%04X failed: 0x%02X\n", DefaultReportingConfiguration[reportIndex].AttributeID, sts);
                  return SZL_RESULT_FAILED;
                }
            }        
        }
      else
        {
          printError(Service, "PLUGIN EM: ERROR - Default report configuration for unknown attribute ID 0x%04X\n", DefaultReportingConfiguration[reportIndex].AttributeID);  
          return SZL_RESULT_INVALID_DATA;
        } 
    }
  
  // If or when we need to support report configuration in NVM, this is probably the place to load them out
  
  return SZL_RESULT_SUCCESS;
}


/******************************************************************************
 * Disable reporting for the cluster
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_ElecMeasCluster_DisableReporting(zabService* Service)
{
  szl_uint8 epIndex;
  szl_uint8 reportIndex;
  szl_uint8 attrIndex;
  szl_uint8 endpoint;
  SZL_STATUS_t sts;
  SZL_RESULT_t result = SZL_RESULT_SUCCESS;
  szl_bool manufacturerSpecific;
  SzlPlugin_ElecMeas_Params_t * pluginParams = NULL;
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_ELEC_MEAS, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }

  /*
   * Configure default reports:
   * For each default report in DefaultReportingConfiguration[]
   * Find the matching attribute in DatapointConfig[]
   * Set SZL_REPORT_M_REPORT_DISABLED_TIME
   * Repeat for each endpoint
   */
  for (reportIndex = 0; reportIndex < ELEC_MEAS_M_NUM_DEFAULT_REPORTS; reportIndex++)
    {
      for (attrIndex = 0; attrIndex < ELEC_MEAS_M_NUM_ATTRIBUTES_PER_ENDPOINT; attrIndex++)
        {
          if (DefaultReportingConfiguration[reportIndex].AttributeID == elecMeasDatapointConfig[attrIndex].AttributeID)
            {
              //Match found
              break;
            }
        }
      
      // If we found a match, then attrIndex will be < ELEC_MEAS_M_NUM_ATTRIBUTES_PER_ENDPOINT
      if (attrIndex < ELEC_MEAS_M_NUM_ATTRIBUTES_PER_ENDPOINT)
        {
        
          /* Disable the default report for each endpoint */
          for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
            {
#ifdef ELEC_MEAS_M_USE_DIRECT_ACCESS
              endpoint = pluginParams->ElecMeasEndpoints[epIndex].EndpointNumber;
#else
              endpoint = pluginParams->Endpoints[epIndex];
#endif
              
              if (elecMeasDatapointConfig[attrIndex].Property.ManufacturerSpecific == szl_true)
                {
                  manufacturerSpecific = szl_true;
                }
              else
                {
                  manufacturerSpecific = szl_false;
                }
              
              
              /* Configure report with disabled time */
              sts = Szl_ReportSendingConfigure(Service, 
                                                endpoint, 
                                                manufacturerSpecific,
                                                ZCL_CLUSTER_ID_ELECTRCIAL_MEASUREMENT, 
                                                DefaultReportingConfiguration[reportIndex].AttributeID,         
                                                elecMeasDatapointConfig[attrIndex].DataType, 
                                                SZL_REPORT_M_REPORT_DISABLED_TIME, 
                                                SZL_REPORT_M_REPORT_DISABLED_TIME, 
                                                NULL);
              if (sts != SZL_STATUS_SUCCESS)
                {
                  result = SZL_RESULT_FAILED;
                  printError(Service, "PLUGIN ELEC MEAS: ERROR - Disable report configuration for AttrID 0x%04X failed: 0x%02X\n", DefaultReportingConfiguration[reportIndex].AttributeID, sts);
                }
            }        
        }
    }
  
  return result;
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
