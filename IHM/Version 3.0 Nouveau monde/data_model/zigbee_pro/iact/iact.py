# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to model iACT Device
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from data_model.zigbee_pro.zigbee_pro_device import ZigBeeProDevice


class IACT(ZigBeeProDevice):

    def __init__(self, address, _id_wireless_bridge):
        self.STATUS_OPEN = True
        self.STATUS_CLOSE = False
        self._status = self.STATUS_OPEN
        # call super function
        super(IACT, self).__init__(address, _id_wireless_bridge)

    def update_data(self, payload):
        pass

    def open(self):
        pass

    def close(self):
        pass

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        self._status = status
