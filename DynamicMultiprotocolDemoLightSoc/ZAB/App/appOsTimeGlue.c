/******************************************************************************
 *                          ZAB TEST APPLICATION
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the application provide time management functions for ZAB.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.03.00  16-Jan-14   MvdB   Use common header block
 * 002.002.013  14-Oct-15   MvdB   ARTF151072: Add service pointer to osTimeGetMilliseconds() and osTimeGetSeconds()
 *****************************************************************************/

/* Feature test macros required for gettimeofday */
#define _BSD_SOURCE           // For glibc <= 2.2.19
//#define _DEFAULT_SOURCE     // From glibc 2.2.0. Use if you get a warning for _BSD_SOURCE.

#include "zabCoreService.h"

#include "rail.h"

/******************************************************************************
 * Get the time in Milli-Seconds
 *
 * The start point for this time is not important (it may be system start), it
 * must just increment every milli-second and may roll over.
 ******************************************************************************/
void osTimeGetMilliseconds( zabService* Service, unsigned32* MilliSeconds )
{
  *MilliSeconds = RAIL_GetTime()/1000;
}

/******************************************************************************
 * Get the time in Seconds
 *
 * The start point for this time is not important (it may be system start), it
 * must just increment every second and may roll over.
 ******************************************************************************/
void osTimeGetSeconds( zabService* Service, unsigned32* Seconds )
{
  *Seconds = (unsigned32) (RAIL_GetTime()/1000000);
}