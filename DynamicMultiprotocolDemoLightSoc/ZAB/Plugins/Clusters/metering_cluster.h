/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the Metering Cluster.
 *   This plugin is originally designed for Nova (partner).
 *
 *   It supports:
 *    - Multiple, application specified endpoints
 *    - Standard attributes
 *    - =S= MS attributes defined in ZigBee&GreenPower_Invariants_Partner_Products_Vxx.pdf
 *    - Default report configuration for time based reports (fast or slow)
 *    - Callback notification to app when a writable attribute is changed
 *
 *   It does not (currently) support:
 *    - Non-volatile backing of any data.
 *    - Report on change. These are not required for Nova, but would be required for certification.
 *
 * CONFIGURATION:
 *   METERING_M_USE_DIRECT_ACCESS:
 *    - Define to have plugin malloc data storage for attributes and access the data directly.
 *    - Undefine to have plugin use callbacks to request attribute read/write via callbacks
 *
 * USAGE:
 *   Usage of this plugin is very simple:
 *   If using METERING_M_USE_DIRECT_ACCESS:
 *    1. Initialise it with SzlPlugin_MeteringClusterInit(), specify the endpoints and a write notification callback
 *    2. Use SzlPlugin_MeteringCluster_GetAttributePointer() to get access to the data for each endpoint.
 *    3. Populate the data
 *    4. Call SzlPlugin_MeteringCluster_ConfigureReporting() to start attribute reporting
 *    5. Update data as it changes, the plugin will do the rest.
 *   If not using METERING_M_USE_DIRECT_ACCESS:
 *    1. Initialise it with SzlPlugin_MeteringClusterInit(), specify the endpoints and read/write attribute callbacks
 *    2. Call SzlPlugin_MeteringCluster_ConfigureReporting() to start attribute reporting
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         21-Feb-14   MvdB   Original
 *    2         18-Apr-14   MvdB   Extend attribute list
 *    3         19-May-14   MvdB   Remove endpoint number from public attribute data
 *    4         18-Aug-14   MvdB   Add support for callback based operation
 *    5         19-Feb-15   MvdB   Add Attributes: 4014-4017, 4100-4105, 4200-4205, 4300-4305
 *                                 Correct name and R/W as of SZCL 1.0.2
 * 002.001.006  27-Jul-15   MvdB   ARTF134686: Standardise on SZL_RESULT_t for plugin init return value
 *                                 ARTF138318: Improve plugin destroy, add disable reporting function
 * 002.002.033  10-Jan-17   MvdB   ARTF170315: Fix incorrect data types for some MS attributes in the Metering Plugin
 *****************************************************************************/
#ifndef _METERING_CLUSTER_H_
#define _METERING_CLUSTER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "zabTypes.h"
#include "szl.h"


/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/

/* If defined, the plugin will use Direct Access to attribute data. This means:
 *  - Plugin will malloc space to store all attribute data for the cluster.
 *  - Plugin will directly access this data via pointers.
 *  - Application can get a pointer to the structure, then set values in it.
 *  - If the applicaiton is not single threaded it must handle mutexing before accessing the data.
 * If not defined, the plugin will use callbacks to access attribute data. This means:
 *  - The plugin will read/write data by calling an application supplied callback.
 *  - The application can then manage all data access itself.
 */
//#define METERING_M_USE_DIRECT_ACCESS

/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Attributes Identifiers */
typedef enum
{
    ATTR_ID_SE_METERING_READING_CURRENT_SUM_DELIVERED     = 0x0000,
    ATTR_ID_SE_METERING_READING_CURRENT_SUM_RECEIVED      = 0x0001,
    ATTR_ID_SE_METERING_POWER_FACTOR                      = 0x0006,
    ATTR_ID_SE_METERING_METER_STATUS_SET                  = 0x0200,
    ATTR_ID_SE_METERING_UNIT_OF_MEASURE                   = 0x0300,
    ATTR_ID_SE_METERING_MULTIPLIER                        = 0x0301,
    ATTR_ID_SE_METERING_DIVISOR                           = 0x0302,
    ATTR_ID_SE_METERING_SUMMATION_FORMATTING              = 0x0303,
    ATTR_ID_SE_METERING_DEVICE_TYPE                       = 0x0306,

    /* Schneider Manufacturer Specific - See ZigBee&GreenPower_Invariants_Partner_Products_Vxx.pdf */
    ATTR_ID_SE_METERING_READING_CURRENT_SUM_DELIVERED_RAZ                   = 0x4000,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_TOTAL         = 0x4010,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_TOTAL       = 0x4011,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_TOTAL       = 0x4012,
    ATTR_ID_SE_METERING_CURRENT_SUMMATION_RECEIVED_RAZ                      = 0x4013,


    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_RAZ_TOTAL     = 0x4014,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_RAZ_TOTAL   = 0x4015,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_RAZ_TOTAL   = 0x4016,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_PLUS_OUT_TOTAL          = 0x4017,

    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_PLUS_OUT_TOTAL        = 0x4018,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_PLUS_OUT_TOTAL        = 0x4019,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_PLUS_OUT_RAZ_TOTAL      = 0x401A,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_PLUS_OUT_RAZ_TOTAL    = 0x401B,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_PLUS_OUT_RAZ_TOTAL    = 0x401C,

    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_RAZ_PHA       = 0x4100,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_RAZ_PHA     = 0x4101,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_RAZ_PHA     = 0x4102,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_PHA           = 0x4103,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_PHA         = 0x4104,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_PHA         = 0x4105,

    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_RAZ_PHB       = 0x4200,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_RAZ_PHB     = 0x4201,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_RAZ_PHB     = 0x4202,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_PHB           = 0x4203,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_PHB         = 0x4204,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_PHB         = 0x4205,

    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_RAZ_PHC       = 0x4300,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_RAZ_PHC     = 0x4301,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_RAZ_PHC     = 0x4302,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_IN_MINUS_OUT_PHC           = 0x4303,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_IN_MINUS_OUT_PHC         = 0x4304,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_IN_MINUS_OUT_PHC         = 0x4305,

    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_MULTIPLIER                 = 0x4400,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_ACTIVE_DIVISOR                    = 0x4401,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_MULTIPLIER               = 0x4402,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_REACTIVE_DIVISOR                  = 0x4403,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_MULTIPLIER               = 0x4404,
    ATTR_ID_SE_METERING_ELECTRICAL_ENERGY_APPARENT_DIVISOR                  = 0x4405,
} METERING_CLUSTER_ATTRIBUTES;

/**
 * Smart Energy Metering Status attribute type
 *
 * This is the overall type for status attribute @ref ENUM_SE_METERING_STATUS_t
 */
typedef szl_uint8 SE_METERING_STATUS_t;

/**
 * Status type values
 *
 * @ref SE_METERING_STATUS_t
 */
typedef enum
{
    SE_METERING_STATUS_CHECK_METER = 0,         /**< Check meter */
    SE_METERING_STATUS_LOW_BATTERY,             /**<  */
    SE_METERING_STATUS_TAMPER_DETECT,           /**<  */
    SE_METERING_STATUS_POWER_FAILURE,           /**<  */
    SE_METERING_STATUS_POWER_QUALITY,           /**<  */
    SE_METERING_STATUS_LEAK_DETECT,             /**<  */
    SE_METERING_STATUS_SERVICE_DISCONNECT_OPEN, /**<  */
} ENUM_SE_METERING_STATUS_t;


/**
 * Smart Energy Metering 'Unit of Measure' attribute type
 *
 * Unit Of Measure provides a label for Energy, Gas, or Water being measured
 * by the metering device, @ref ENUM_SE_METERING_UNIT_t
 */
typedef szl_uint8 SE_METERING_UNIT_t;

/**
 *  Unit of measure type values
 *
 *  @ref SE_METERING_UNIT_t
 */
typedef enum
{
    SE_METERING_UNIT_KWH  = 0x00, /**< kW & kWh in pure Binary format */
} ENUM_SE_METERING_UNIT_t;

/**
 * Smart Energy Metering 'Metering Device Type' attribute type
 *
 * Metering Device Type provides a label for identifying the type of metering
 * device present, @ref ENUM_SE_METERING_DEVICE_TYPE_t
 */
typedef szl_uint8 SE_METERING_DEVICE_TYPE_t;

/**
 *  Unit of measure type values
 *
 *  @ref SE_METERING_DEVICE_TYPE_t
 */
typedef enum
{
    SE_METERING_DEVICE_TYPE_ELECTRIC  = 0x00, /**< Electric Metering */
} ENUM_SE_METERING_DEVICE_TYPE_t;

/* Attribute Data storage
 *
 * If METERING_M_USE_DIRECT_ACCESS:
 *    An instance of this is created per endpoint.
 *    Applications may access it via SzlPlugin_ElecMeasCluster_GetAttributePointer()
 * If !METERING_M_USE_DIRECT_ACCESS:
 *    This structure is not used by the plugin. The application may use it if it wishes. */
typedef struct
{
  szl_uint48 CurrentSummationDelivered;
  szl_uint48 CurrentSummationReceived;
  szl_int8 PowerFactor;
  SE_METERING_STATUS_t StatusSet;
  SE_METERING_UNIT_t UnitOfMeasure;
  szl_uint24 Multiplier;
  szl_uint24 Divisor;
  szl_uint8  SummationFormatting;
  SE_METERING_DEVICE_TYPE_t DeviceType;

  /* =S= MS Attributes */
  szl_uint48 CurrentSummationDeliveredRaz;
  szl_int48 ElectricalEnergyActiveTotal;
  szl_int48 ElectricalEnergyReactiveTotal;
  szl_int48 ElectricalEnergyApparentTotal;
  szl_uint48 CurrentSummationReceivedRaz;
  szl_int48 ElectricalEnergyActiveInMinusOutRazTotal;
  szl_int48 ElectricalEnergyReactiveInMinusOutRazTotal;
  szl_int48 ElectricalEnergyApparentInMinusOutRazTotal;
  szl_uint48 ElectricalEnergyActiveInPlusOutTotal;
  szl_uint48 ElectricalEnergyReactiveInPlusOutTotal;
  szl_uint48 ElectricalEnergyApparentInPlusOutTotal;
  szl_uint48 ElectricalEnergyActiveInPlusOutRazTotal;
  szl_uint48 ElectricalEnergyReactiveInPlusOutRazTotal;
  szl_uint48 ElectricalEnergyApparentInPlusOutRazTotal;

  szl_int48 ElectricalEnergyActiveInMinusOutRazPhA;
  szl_int48 ElectricalEnergyReactiveInMinusOutRazPhA;
  szl_int48 ElectricalEnergyApparentInMinusOutRazPhA;
  szl_int48 ElectricalEnergyActiveInMinusOutPhA;
  szl_int48 ElectricalEnergyReactiveInMinusOutPhA;
  szl_int48 ElectricalEnergyApparentInMinusOutPhA;

  szl_int48 ElectricalEnergyActiveInMinusOutRazPhB;
  szl_int48 ElectricalEnergyReactiveInMinusOutRazPhB;
  szl_int48 ElectricalEnergyApparentInMinusOutRazPhB;
  szl_int48 ElectricalEnergyActiveInMinusOutPhB;
  szl_int48 ElectricalEnergyReactiveInMinusOutPhB;
  szl_int48 ElectricalEnergyApparentInMinusOutPhB;

  szl_int48 ElectricalEnergyActiveInMinusOutRazPhC;
  szl_int48 ElectricalEnergyReactiveInMinusOutRazPhC;
  szl_int48 ElectricalEnergyApparentInMinusOutRazPhC;
  szl_int48 ElectricalEnergyActiveInMinusOutPhC;
  szl_int48 ElectricalEnergyReactiveInMinusOutPhC;
  szl_int48 ElectricalEnergyApparentInMinusOutPhC;

  szl_uint24 ElectricalEnergyActiveMultiplier;
  szl_uint24 ElectricalEnergyActiveDivisor;
  szl_uint24 ElectricalEnergyReactiveMultiplier;
  szl_uint24 ElectricalEnergyReactiveDivisor;
  szl_uint24 ElectricalEnergyApparentMultiplier;
  szl_uint24 ElectricalEnergyApparentDivisor;
}SzlPlugin_Metering_Attributes_t;


/* Attribute Write Notification Handler format */
#ifdef METERING_M_USE_DIRECT_ACCESS
typedef void (*SzlPlugin_MeteringCluster_AttributeWriteNotification_CB_t) (
                            zabService* Service,
                            szl_uint8 Endpoint,
                            szl_uint16 AttributeID);
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Plugin Initialisation
 * This function must be called from the application after the Library has been initialized.
 *
 * Parameters:
 *  Service: Instance of library
 *  numEndpoints: The length of endpointList
 *  endpointList: The endpoints to be initialised with this cluster
 *
 *  For Direct Data Access:
 *  WriteAttributesCallback: Callback will be called to notify application that a writable attribute has been written from ZigBee.
 *                           It is optional. If the app does not want to be notified this may be set to NULL.
 *
 *  For Callback Data Access:
 *  ReadCallback: Callback that will be called to read the value of an attributes data
 *  WriteCallback: Callback that will be called to write the value of an attributes data
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_MeteringClusterInit(zabService* Service,
                                           szl_uint8 numEndpoints,
                                           szl_uint8 * endpointList,
#ifdef METERING_M_USE_DIRECT_ACCESS
                                           SzlPlugin_MeteringCluster_AttributeWriteNotification_CB_t WriteAttributesCallback
#else
                                           SZL_CB_DataPointRead_t ReadCallback,
                                           SZL_CB_DataPointWrite_t WriteCallback
#endif
                                           );

/******************************************************************************
 * Plugin Destruction
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_MeteringCluster_Destroy(zabService* Service);

/******************************************************************************
 * Get pointer to Metering Cluster Attributes for an endpoint
 * This can be used to get access to the endpoints parameters for the local
 *  application to read or write them
 * Return NULL if not found
 ******************************************************************************/
#ifdef METERING_M_USE_DIRECT_ACCESS
extern
SzlPlugin_Metering_Attributes_t* SzlPlugin_MeteringCluster_GetAttributePointer(zabService* Service, szl_uint8 endpoint);
#endif

/******************************************************************************
 * Configure reporting for the cluster.
 * Data must be initialised before this function is called to avoid reporting uninitialised values
 *
 * This function currently only configures a time based default report.
 * If report on change is required the plugin must be extended.
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_MeteringCluster_ConfigureReporting(zabService* Service);


/******************************************************************************
 * Disable reporting for the cluster
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_MeteringCluster_DisableReporting(zabService* Service);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /*_METERING_CLUSTER_H_*/
