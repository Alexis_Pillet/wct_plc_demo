# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to generate frames
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from frame import *


class FrameGenerator:

    # Configuration PLC Concentrator
    DEFAULT_MAC_ADDRESS = bytearray([0x00, 0x00])
    DEFAULT_PAN_ID = bytearray([0x78, 0x1D])

    def __init__(self, serial_driver):
        self._serial = serial_driver

    def send_configuration_concentrator(self, mac_address, pan_id):

        # Define configuration concentrator frame
        # Device Type + Concentrator + MSB mac_address + LSB mac_address + MSB pan_id + LSB pan_id
        ack_conf = bytearray([Type.DEVICE_TYPE.value,
                              DeviceType.CONCENTRATOR.value, mac_address[0], mac_address[1], pan_id[0], pan_id[1]])

        # Send frame
        self._serial.write_data(Command.CONTROL.value, ack_conf)

    def send_data_frame(self, device_number, payload):

        # Define data frame
        # device number + data
        payload_data = bytearray([device_number])

        # Add payload of the frame
        for element in payload:
            payload_data.append(element)

        # Send frame
        self._serial.write_data(Command.DATA.value, payload_data)
