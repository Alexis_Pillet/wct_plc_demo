# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to model Power Tag 1P Device
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from data_model.zigbee_green_power.power_tag.power_tag import PowerTag


class PowerTag1P(PowerTag):

    def __init__(self, address, id_wireless_bridge, application_frame_generator):
        # call super function
        super(PowerTag1P, self).__init__(address, id_wireless_bridge, application_frame_generator)
        self._voltage = None
        self._current = None

    def update_data(self, payload):
        pass

    @property
    def voltage(self):
        return self._voltage

    @voltage.setter
    def voltage(self, voltage):
        self._voltage = voltage

    @property
    def current(self):
        return self._current

    @current.setter
    def current(self, current):
        self._current = current
