/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.011  19-Jul-16   MvdB   Add ZAB_DEVELOPER_TEST_MODE test flags
 *                                 Extract SBL hardware type and version number
 * 000.000.019  26-Jul-16   MvdB   Replace memcpy with osMemCopy, memset with osMemSet
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 * 002.002.048  06-Feb-17   MvdB   Test Plan: Fix EFR32 give ZAB_NET_PROC_MODEL_SILABS_EFR32_NCP for boot loader
 *****************************************************************************/

#include "zabNcpPrivate.h"
#include "zabNcpService.h"
#include "zabNcpOpen.h"
#include "zabNcpSbl.h"

#include "zabNcpXModem.h"


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

#define SOH                 (0x01)
#define EOT                 (0x04)
#define ACK                 (0x06)
#define NAK                 (0x15)
#define CAN                 (0x18)

#define DATA_SIZE           (128)
#define HEADER_SIZE         (3)
#define FOOTER_SIZE         (2)
#define FULL_BLOCK_SIZE     (HEADER_SIZE + DATA_SIZE + FOOTER_SIZE)

#define CONTROL_OFFSET      (0)
#define BLOCK_NUM_OFFSET    (1)
#define BLOCK_COMP_OFFSET   (2)
#define DATA_OFFSET         (3)
#define CRC_H_OFFSET        (131)
#define CRC_L_OFFSET        (132)

#define XMODEN_INFO( s )    (ZAB_SERVICE_VENDOR( s )->zabNcpXModem)


/******************************************************************************
 *                      *****************************
 *                 *****     EXTERNAL VARIABLES      *****
 *                      *****************************
 ******************************************************************************/

/* Some flags for causing errors and confirming they are handled during development testing */
#ifdef ZAB_DEVELOPER_TEST_MODE
#warning ZAB_DEVELOPER_TEST_MODE enabled. Must not be enabled for real products!
zab_bool zabNcpXModem_DropNextIncomingFrame = zab_false;
zab_bool zabNcpXModem_DropNextOutgoingingFrame = zab_false;
zab_bool zabNcpXModem_BreakNextOutgoingCrc = zab_false;
#endif

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/
void zabNcpXModem_SendData(erStatus* Status, zabService* Service, unsigned8 FrameLength, unsigned8* FrameData)
{
  sapMsg* serialMsg = NULL;
  unsigned8* serialMsgData = NULL;
  unsigned8 index;

  printVendor(Service, "zabNcpXModem_SendData: 0x%02X bytes: ", FrameLength);
  for (index = 0; index < FrameLength; index++)
    {
      printVendor(Service, " %02X", FrameData[index]);
    }
  printVendor(Service, "\n");

  /* Allocate serial frame */
  serialMsg = sapMsgAllocateData( Status, zabCoreSapSerial(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, FrameLength );
  if ( (erStatusIsError(Status)) || (serialMsg == NULL) )
    {
      erStatusSet(Status, SAP_ERROR_NULL_PTR);
      return;
    }
  sapMsgSetAppType (Status, serialMsg, ZAB_MSG_APP_RAW);
  sapMsgSetAppDataLength(Status, serialMsg, FrameLength);
  serialMsgData = sapMsgGetAppData(serialMsg);
  if (serialMsgData == NULL)
    {
      printError(Service,  "zabNcpXModem_SendCarriageReturn: serialMsgData invalid\n");
      sapMsgFree(Status, serialMsg);
      return;
    }

  osMemCopy(Status, serialMsgData, FrameData, FrameLength);

  sapMsgPutFree(Status, zabCoreSapSerial(Service), serialMsg);

}


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Reset the service
 ******************************************************************************/
void zabNcpXModem_Reset(erStatus* Status, zabService* Service)
{
  XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_None;
  XMODEN_INFO(Service).blockNumber = 0;
}

/******************************************************************************
 * Send a carriage return, to get the prompt from the bootloader
 ******************************************************************************/
void zabNcpXModem_SendCarriageReturn(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {0x0D};
  zabNcpXModem_SendData(Status, Service, sizeof(cmd), cmd);
}


/******************************************************************************
 * Send a Run command to the bootloader
 ******************************************************************************/
void zabNcpXModem_Run(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {'2'};
  zabNcpXModem_SendData(Status, Service, sizeof(cmd), cmd);
}

/******************************************************************************
 * Send a Get Info command to the bootloader
 ******************************************************************************/
void zabNcpXModem_GetInfo(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {'3'};
  zabNcpXModem_SendData(Status, Service, sizeof(cmd), cmd);
}

/******************************************************************************
 * Send an Initialise Upload command to the bootloader
 ******************************************************************************/
void zabNcpXModem_InitialiseUpload(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {'1'};
  zabNcpXModem_SendData(Status, Service, sizeof(cmd), cmd);
}

/******************************************************************************
 * Send a 128 byte block of data to the bootloader
 ******************************************************************************/
void zabNcpXModem_SendBlock(erStatus* Status, zabService* Service, unsigned8* BlockData)
{
  unsigned8 cmd[FULL_BLOCK_SIZE];
  unsigned16 crc = 0;

  XMODEN_INFO(Service).blockNumber++;

#ifdef ZAB_DEVELOPER_TEST_MODE
  if (zabNcpXModem_DropNextOutgoingingFrame == zab_true)
    {
      zabNcpXModem_DropNextOutgoingingFrame = zab_false;
      printApp(Service,  "zabNcpXModem_SendBlock: zabNcpXModem_DropNextOutgoingingFrame - Frame dropped!!!!!!!!\n");
      return;
    }
#endif

  cmd[CONTROL_OFFSET] = SOH;
  cmd[BLOCK_NUM_OFFSET] = XMODEN_INFO(Service).blockNumber;
  cmd[BLOCK_COMP_OFFSET] = ~(XMODEN_INFO(Service).blockNumber);
  osMemCopy(Status, &cmd[DATA_OFFSET], BlockData, DATA_SIZE);

  crc = zabNcpService_CalculateCrc16(0, DATA_SIZE, BlockData);

#ifdef ZAB_DEVELOPER_TEST_MODE
  if (zabNcpXModem_BreakNextOutgoingCrc == zab_true)
    {
      zabNcpXModem_BreakNextOutgoingCrc = zab_false;
      printApp(Service,  "zabNcpXModem_SendBlock: zabNcpXModem_BreakNextOutgoingCrc - CRC Broken!!!!!!!!\n");
      crc++;
    }
#endif

  cmd[CRC_H_OFFSET] = (unsigned8)((crc >> 8) & 0xFF);
  cmd[CRC_L_OFFSET] = (unsigned8)(crc & 0xFF);

  zabNcpXModem_SendData(Status, Service, sizeof(cmd), cmd);
}

/******************************************************************************
 * Send an End Of Transfer command to the bootloader
 ******************************************************************************/
void zabNcpXModem_EndFileTransfer(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {EOT};
  zabNcpXModem_SendData(Status, Service, sizeof(cmd), cmd);
}

/******************************************************************************
 * Process incoming messages from the bootloader and catch info/prompt
 ******************************************************************************/
void zabNcpXModem_ProcessMessageIn(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8 frameLength;
  unsigned8* frameData = NULL;
  unsigned8 index;
  unsigned8 hardwareVersion[4];

#ifdef ZAB_DEVELOPER_TEST_MODE
  if (zabNcpXModem_DropNextIncomingFrame == zab_true)
    {
      zabNcpXModem_DropNextIncomingFrame = zab_false;
      printApp(Service,  "zabNcpXModem_ProcessMessageIn: zabNcpXModem_DropNextIncomingFrame: Frame dropped!!!!!!!!\n");
      return;
    }
#endif

  /* Parameter checking */
  frameLength = sapMsgGetAppDataLength(Message);
  frameData = sapMsgGetAppData(Message);
  if ( frameData == NULL)
    {
      printError(Service,  "zabNcpXModem_ProcessMessageIn: Invalid frame size %d or pointer\n", frameLength);
      return;
    }

  /* Consider strstr() to deal with this mess?
   *  - What we have below is not very maintainable as we get more strings
   *  - strstr will not work for the version number...*/
  for (index = 0; index < frameLength; index++)
    {
      switch(frameData[index])
        {

        case ACK:
            zabNcpSbl_XModemResponseHandler(Status, Service, ZAB_SBL_RESPONSE_ACK);
          break;

        case NAK:
            zabNcpSbl_XModemResponseHandler(Status, Service, ZAB_SBL_RESPONSE_NACK);
          break;

        case CAN:
            printVendor(Service,  "zabNcpXModem_ProcessMessageIn: CAN\n");
          break;


        case 0x43: // 'C' - This is horrible as the SBL also sends strings, so only accept if its a single byte
          if (frameLength == 1)
            {
              zabNcpSbl_XModemResponseHandler(Status, Service, ZAB_SBL_RESPONSE_CLEAR);
            }
          break;

        case 'B':
          XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_B;
          break;

        case 'L':
          if (XMODEN_INFO(Service).promptState == zabNcpXModem_PromptState_B)
            {
              XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_L;
            }
          else
            {
              XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_None;
            }
          break;

        case ' ':
          if (XMODEN_INFO(Service).promptState == zabNcpXModem_PromptState_L)
            {
              XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_BLSpace;
            }
          else if (XMODEN_INFO(Service).promptState == zabNcpXModem_PromptState_EFR)
            {
              XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_EFRSpace;
            }
          else if (XMODEN_INFO(Service).promptState == zabNcpXModem_PromptState_VersionLast)
            {

              srvCoreGiveBack8(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ACTIVE_APP_TYPE, (unsigned8)ZAB_NET_PROC_APP_BOOTLOADER);
              hardwareVersion[0] = XMODEN_INFO(Service).versionMajor;
              hardwareVersion[1] = XMODEN_INFO(Service).versionMinor;
              hardwareVersion[2] = XMODEN_INFO(Service).versionInternal;
              hardwareVersion[3] = XMODEN_INFO(Service).versionTest;
              srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_BOOTLOADER_VERSION, sizeof(hardwareVersion), hardwareVersion);
            }
          else
            {
              XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_None;
            }
          break;

        case '>':
          if (XMODEN_INFO(Service).promptState == zabNcpXModem_PromptState_BLSpace)
            {
              XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_Arrow;
              /* Prompt Good */
              printVendor(Service,  "zabNcpXModem_ProcessMessageIn: Prompt Ready\n");
              zabNcpOpen_SblPromptReceivedHandler(Status, Service);
              zabNcpSbl_XModemResponseHandler(Status, Service, ZAB_SBL_RESPONSE_PROMPT_READY);
            }
          else
            {
              XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_None;
            }
          break;



        case 'E':
          XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_E;
          break;

        case 'F':
          if (XMODEN_INFO(Service).promptState == zabNcpXModem_PromptState_E)
            {
              XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_EF;
            }
          else
            {
              XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_None;
            }
          break;

        case 'R':
          if (XMODEN_INFO(Service).promptState == zabNcpXModem_PromptState_EF)
            {
              XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_EFR;
            }
          else
            {
              XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_None;
            }
          break;

        case 'v':
          XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_VersionStart;
          XMODEN_INFO(Service).versionMajor = 0;
          XMODEN_INFO(Service).versionMinor = 0;
          XMODEN_INFO(Service).versionInternal = 0;
          XMODEN_INFO(Service).versionTest = 0;
          break;

        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':

          if ( (frameData[index] == '3') &&
               ( (XMODEN_INFO(Service).promptState == zabNcpXModem_PromptState_EFRSpace) || (XMODEN_INFO(Service).promptState == zabNcpXModem_PromptState_EFR) ) )
            {
              XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_EFR3;
            }
          else if ( (frameData[index] == '2') && (XMODEN_INFO(Service).promptState == zabNcpXModem_PromptState_EFR3))
            {
              srvCoreGiveBack8(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_MODEL, (unsigned8)ZAB_NET_PROC_MODEL_SILABS_EFR32_NCP);
              XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_None;
            }
          else
            {
              switch(XMODEN_INFO(Service).promptState)
                {
                  case zabNcpXModem_PromptState_VersionStart:
                  case zabNcpXModem_PromptState_VersionMajor:
                    XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_VersionMajor;
                    XMODEN_INFO(Service).versionMajor = (XMODEN_INFO(Service).versionMajor * 10) + (frameData[index] - (unsigned8)'0');
                    break;


                  case zabNcpXModem_PromptState_VersionMajorDot:
                  case zabNcpXModem_PromptState_VersionMinor:
                    XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_VersionMinor;
                    XMODEN_INFO(Service).versionMinor = (XMODEN_INFO(Service).versionMinor * 10) + (frameData[index] - (unsigned8)'0');
                    break;

                  case zabNcpXModem_PromptState_VersionMinorDot:
                  case zabNcpXModem_PromptState_VersionInternal:
                    XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_VersionInternal;
                    XMODEN_INFO(Service).versionInternal = (XMODEN_INFO(Service).versionInternal * 10) + (frameData[index] - (unsigned8)'0');
                    break;

                  case zabNcpXModem_PromptState_VersionInternalDot:
                  case zabNcpXModem_PromptState_VersionLast:
                    XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_VersionLast;
                    XMODEN_INFO(Service).versionTest = (XMODEN_INFO(Service).versionTest * 10) + (frameData[index] - (unsigned8)'0');
                    break;

                  default:
                    XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_None;

                }
            }
          break;

        case '.':
          switch(XMODEN_INFO(Service).promptState)
            {
              case zabNcpXModem_PromptState_VersionMajor:
                XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_VersionMajorDot;
                break;


              case zabNcpXModem_PromptState_VersionMinor:
                XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_VersionMinorDot;
                break;

              case zabNcpXModem_PromptState_VersionInternal:
                XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_VersionInternalDot;
                break;

              default:
                XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_None;
            }
          break;

        default:
          XMODEN_INFO(Service).promptState = zabNcpXModem_PromptState_None;
        }
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
