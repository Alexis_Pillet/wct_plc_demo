#include <string.h>
#include "zabCoreService.h"
#include "appConfig.h"
#include "appOsMemoryGlue.h"

/******************************************************************************
 *                      *****************************
 *                 *****        CONFIGURATION        *****
 *                      *****************************
 ******************************************************************************/

//
// Validate RAM blocks configuration
//

// GZIGBEE_MALLOC_NB_BLOCK_SIZES defined and comprised between 1 and 10
#ifndef GZIGBEE_MALLOC_NB_BLOCK_SIZES
  #error GZIGBEE_MALLOC_NB_BLOCK_SIZES must be defined (number of memory block sizes) !
#elif ( GZIGBEE_MALLOC_NB_BLOCK_SIZES < 1 ) || ( GZIGBEE_MALLOC_NB_BLOCK_SIZES > 10 )
  #error GZIGBEE_MALLOC_NB_BLOCK_SIZES must be comprised between 1 and 10
#else
  // Parameter is valid
  #define MALLOC_NB_BLOCK_SIZES     ( GZIGBEE_MALLOC_NB_BLOCK_SIZES )
#endif

// Block size 0 number and size
#if !defined( GZIGBEE_MALLOC_BLOCK_NUMBER_0 ) || !defined( GZIGBEE_MALLOC_BLOCK_SIZE_0 )
  #error GZIGBEE_MALLOC_BLOCK_NUMBER_0 and GZIGBEE_MALLOC_BLOCK_SIZE_0 must be defined
#elif ( GZIGBEE_MALLOC_BLOCK_NUMBER_0 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_NUMBER_0 > 65535U )
  #error GZIGBEE_MALLOC_BLOCK_NUMBER_0 must be comprised between 1 and 65535
#elif ( GZIGBEE_MALLOC_BLOCK_SIZE_0 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_SIZE_0 > 65535U )
  #error GZIGBEE_MALLOC_BLOCK_SIZE_0 must be comprised between 1 and 65535
#else
  // Parameters are valid
  #define MALLOC_BLOCK_NUMBER_0     ( GZIGBEE_MALLOC_BLOCK_NUMBER_0 )
  #define MALLOC_BLOCK_SIZE_0       ( GZIGBEE_MALLOC_BLOCK_SIZE_0 )
#endif

// Block size 1 number and size
#if GZIGBEE_MALLOC_NB_BLOCK_SIZES > 1
  #if !defined( GZIGBEE_MALLOC_BLOCK_NUMBER_1 ) || !defined( GZIGBEE_MALLOC_BLOCK_SIZE_1 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_1 and GZIGBEE_MALLOC_BLOCK_SIZE_1 must be defined
  #elif ( GZIGBEE_MALLOC_BLOCK_NUMBER_1 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_NUMBER_1 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_1 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_1 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_SIZE_1 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_1 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_1 < GZIGBEE_MALLOC_BLOCK_SIZE_0 )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_1 must be greater than GZIGBEE_MALLOC_BLOCK_SIZE_0
  #else
    // Parameters are valid
    #define MALLOC_BLOCK_NUMBER_1   ( GZIGBEE_MALLOC_BLOCK_NUMBER_1 )
    #define MALLOC_BLOCK_SIZE_1     ( GZIGBEE_MALLOC_BLOCK_SIZE_1 )
  #endif
#else
  // Size not available
  #define MALLOC_BLOCK_NUMBER_1     ( 0 )
  #define MALLOC_BLOCK_SIZE_1       ( 0 )
  #if defined( GZIGBEE_MALLOC_BLOCK_NUMBER_1 ) || defined( GZIGBEE_MALLOC_BLOCK_SIZE_1 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_1/SIZE_1 defined but unused (GZIGBEE_MALLOC_NB_BLOCK_SIZES < 2)
  #endif
#endif

// Block size 2 number and size
#if GZIGBEE_MALLOC_NB_BLOCK_SIZES > 2
  #if !defined( GZIGBEE_MALLOC_BLOCK_NUMBER_2 ) || !defined( GZIGBEE_MALLOC_BLOCK_SIZE_2 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_2 and GZIGBEE_MALLOC_BLOCK_SIZE_2 must be defined
  #elif ( GZIGBEE_MALLOC_BLOCK_NUMBER_2 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_NUMBER_2 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_2 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_2 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_SIZE_2 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_2 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_2 < GZIGBEE_MALLOC_BLOCK_SIZE_1 )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_2 must be greater than GZIGBEE_MALLOC_BLOCK_SIZE_1
  #else
    // Parameters are valid
    #define MALLOC_BLOCK_NUMBER_2   ( GZIGBEE_MALLOC_BLOCK_NUMBER_2 )
    #define MALLOC_BLOCK_SIZE_2     ( GZIGBEE_MALLOC_BLOCK_SIZE_2 )
  #endif
#else
  // Size not available
  #define MALLOC_BLOCK_NUMBER_2     ( 0 )
  #define MALLOC_BLOCK_SIZE_2       ( 0 )
  #if defined( GZIGBEE_MALLOC_BLOCK_NUMBER_2 ) || defined( GZIGBEE_MALLOC_BLOCK_SIZE_2 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_2/SIZE_2 defined but unused (GZIGBEE_MALLOC_NB_BLOCK_SIZES < 3)
  #endif
#endif

// Block size 3 number and size
#if GZIGBEE_MALLOC_NB_BLOCK_SIZES > 3
  #if !defined( GZIGBEE_MALLOC_BLOCK_NUMBER_3 ) || !defined( GZIGBEE_MALLOC_BLOCK_SIZE_3 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_3 and GZIGBEE_MALLOC_BLOCK_SIZE_3 must be defined
  #elif ( GZIGBEE_MALLOC_BLOCK_NUMBER_3 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_NUMBER_3 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_3 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_3 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_SIZE_3 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_3 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_3 < GZIGBEE_MALLOC_BLOCK_SIZE_2 )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_3 must be greater than GZIGBEE_MALLOC_BLOCK_SIZE_2
  #else
    // Parameters are valid
    #define MALLOC_BLOCK_NUMBER_3   ( GZIGBEE_MALLOC_BLOCK_NUMBER_3 )
    #define MALLOC_BLOCK_SIZE_3     ( GZIGBEE_MALLOC_BLOCK_SIZE_3 )
  #endif
#else
  // Size not available
  #define MALLOC_BLOCK_NUMBER_3     ( 0 )
  #define MALLOC_BLOCK_SIZE_3       ( 0 )
  #if defined( GZIGBEE_MALLOC_BLOCK_NUMBER_3 ) || defined( GZIGBEE_MALLOC_BLOCK_SIZE_3 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_3/SIZE_3 defined but unused (GZIGBEE_MALLOC_NB_BLOCK_SIZES < 4)
  #endif
#endif

// Block size 4 number and size
#if GZIGBEE_MALLOC_NB_BLOCK_SIZES > 4
  #if !defined( GZIGBEE_MALLOC_BLOCK_NUMBER_4 ) || !defined( GZIGBEE_MALLOC_BLOCK_SIZE_4 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_4 and GZIGBEE_MALLOC_BLOCK_SIZE_4 must be defined
  #elif ( GZIGBEE_MALLOC_BLOCK_NUMBER_4 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_NUMBER_4 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_4 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_4 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_SIZE_4 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_4 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_4 < GZIGBEE_MALLOC_BLOCK_SIZE_3 )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_4 must be greater than GZIGBEE_MALLOC_BLOCK_SIZE_3
  #else
    // Parameters are valid
    #define MALLOC_BLOCK_NUMBER_4   ( GZIGBEE_MALLOC_BLOCK_NUMBER_4 )
    #define MALLOC_BLOCK_SIZE_4     ( GZIGBEE_MALLOC_BLOCK_SIZE_4 )
  #endif
#else
  // Size not available
  #define MALLOC_BLOCK_NUMBER_4     ( 0 )
  #define MALLOC_BLOCK_SIZE_4       ( 0 )
  #if defined( GZIGBEE_MALLOC_BLOCK_NUMBER_4 ) || defined( GZIGBEE_MALLOC_BLOCK_SIZE_4 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_4/SIZE_4 defined but unused (GZIGBEE_MALLOC_NB_BLOCK_SIZES < 5)
  #endif
#endif

// Block size 5 number and size
#if GZIGBEE_MALLOC_NB_BLOCK_SIZES > 5
  #if !defined( GZIGBEE_MALLOC_BLOCK_NUMBER_5 ) || !defined( GZIGBEE_MALLOC_BLOCK_SIZE_5 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_5 and GZIGBEE_MALLOC_BLOCK_SIZE_5 must be defined
  #elif ( GZIGBEE_MALLOC_BLOCK_NUMBER_5 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_NUMBER_5 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_5 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_5 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_SIZE_5 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_5 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_5 < GZIGBEE_MALLOC_BLOCK_SIZE_4 )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_5 must be greater than GZIGBEE_MALLOC_BLOCK_SIZE_4
  #else
    // Parameters are valid
    #define MALLOC_BLOCK_NUMBER_5   ( GZIGBEE_MALLOC_BLOCK_NUMBER_5 )
    #define MALLOC_BLOCK_SIZE_5     ( GZIGBEE_MALLOC_BLOCK_SIZE_5 )
  #endif
#else
  // Size not available
  #define MALLOC_BLOCK_NUMBER_5     ( 0 )
  #define MALLOC_BLOCK_SIZE_5       ( 0 )
  #if defined( GZIGBEE_MALLOC_BLOCK_NUMBER_5 ) || defined( GZIGBEE_MALLOC_BLOCK_SIZE_5 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_5/SIZE_5 defined but unused (GZIGBEE_MALLOC_NB_BLOCK_SIZES < 6)
  #endif
#endif

// Block size 6 number and size
#if GZIGBEE_MALLOC_NB_BLOCK_SIZES > 6
  #if !defined( GZIGBEE_MALLOC_BLOCK_NUMBER_6 ) || !defined( GZIGBEE_MALLOC_BLOCK_SIZE_6 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_6 and GZIGBEE_MALLOC_BLOCK_SIZE_6 must be defined
  #elif ( GZIGBEE_MALLOC_BLOCK_NUMBER_6 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_NUMBER_6 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_6 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_6 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_SIZE_6 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_6 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_6 < GZIGBEE_MALLOC_BLOCK_SIZE_5 )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_6 must be greater than GZIGBEE_MALLOC_BLOCK_SIZE_5
  #else
    // Parameters are valid
    #define MALLOC_BLOCK_NUMBER_6   ( GZIGBEE_MALLOC_BLOCK_NUMBER_6 )
    #define MALLOC_BLOCK_SIZE_6     ( GZIGBEE_MALLOC_BLOCK_SIZE_6 )
  #endif
#else
  // Size not available
  #define MALLOC_BLOCK_NUMBER_6     ( 0 )
  #define MALLOC_BLOCK_SIZE_6       ( 0 )
  #if defined( GZIGBEE_MALLOC_BLOCK_NUMBER_6 ) || defined( GZIGBEE_MALLOC_BLOCK_SIZE_6 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_6/SIZE_6 defined but unused (GZIGBEE_MALLOC_NB_BLOCK_SIZES < 7)
  #endif
#endif

// Block size 7 number and size
#if GZIGBEE_MALLOC_NB_BLOCK_SIZES > 7
  #if !defined( GZIGBEE_MALLOC_BLOCK_NUMBER_7 ) || !defined( GZIGBEE_MALLOC_BLOCK_SIZE_7 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_7 and GZIGBEE_MALLOC_BLOCK_SIZE_7 must be defined
  #elif ( GZIGBEE_MALLOC_BLOCK_NUMBER_7 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_NUMBER_7 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_7 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_7 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_SIZE_7 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_7 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_7 < GZIGBEE_MALLOC_BLOCK_SIZE_6 )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_7 must be greater than GZIGBEE_MALLOC_BLOCK_SIZE_6
  #else
    // Parameters are valid
    #define MALLOC_BLOCK_NUMBER_7   ( GZIGBEE_MALLOC_BLOCK_NUMBER_7 )
    #define MALLOC_BLOCK_SIZE_7     ( GZIGBEE_MALLOC_BLOCK_SIZE_7 )
  #endif
#else
  // Size not available
  #define MALLOC_BLOCK_NUMBER_7     ( 0 )
  #define MALLOC_BLOCK_SIZE_7       ( 0 )
  #if defined( GZIGBEE_MALLOC_BLOCK_NUMBER_7 ) || defined( GZIGBEE_MALLOC_BLOCK_SIZE_7 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_7/SIZE_7 defined but unused (GZIGBEE_MALLOC_NB_BLOCK_SIZES < 8)
  #endif
#endif

// Block size 8 number and size
#if GZIGBEE_MALLOC_NB_BLOCK_SIZES > 8
  #if !defined( GZIGBEE_MALLOC_BLOCK_NUMBER_8 ) || !defined( GZIGBEE_MALLOC_BLOCK_SIZE_8 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_8 and GZIGBEE_MALLOC_BLOCK_SIZE_8 must be defined
  #elif ( GZIGBEE_MALLOC_BLOCK_NUMBER_8 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_NUMBER_8 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_8 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_8 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_SIZE_8 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_8 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_8 < GZIGBEE_MALLOC_BLOCK_SIZE_7 )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_8 must be greater than GZIGBEE_MALLOC_BLOCK_SIZE_7
  #else
    // Parameters are valid
    #define MALLOC_BLOCK_NUMBER_8   ( GZIGBEE_MALLOC_BLOCK_NUMBER_8 )
    #define MALLOC_BLOCK_SIZE_8     ( GZIGBEE_MALLOC_BLOCK_SIZE_8 )
  #endif
#else
  // Size not available
  #define MALLOC_BLOCK_NUMBER_8     ( 0 )
  #define MALLOC_BLOCK_SIZE_8       ( 0 )
  #if defined( GZIGBEE_MALLOC_BLOCK_NUMBER_8 ) || defined( GZIGBEE_MALLOC_BLOCK_SIZE_8 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_8/SIZE_8 defined but unused (GZIGBEE_MALLOC_NB_BLOCK_SIZES < 9)
  #endif
#endif

// Block size 9 number and size
#if GZIGBEE_MALLOC_NB_BLOCK_SIZES > 9
  #if !defined( GZIGBEE_MALLOC_BLOCK_NUMBER_9 ) || !defined( GZIGBEE_MALLOC_BLOCK_SIZE_9 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_9 and GZIGBEE_MALLOC_BLOCK_SIZE_9 must be defined
  #elif ( GZIGBEE_MALLOC_BLOCK_NUMBER_9 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_NUMBER_9 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_9 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_9 < 1 ) || ( GZIGBEE_MALLOC_BLOCK_SIZE_9 > 65535U )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_9 must be comprised between 1 and 65535
  #elif ( GZIGBEE_MALLOC_BLOCK_SIZE_9 < GZIGBEE_MALLOC_BLOCK_SIZE_8 )
    #error GZIGBEE_MALLOC_BLOCK_SIZE_9 must be greater than GZIGBEE_MALLOC_BLOCK_SIZE_8
  #else
    // Parameters are valid
    #define MALLOC_BLOCK_NUMBER_9   ( GZIGBEE_MALLOC_BLOCK_NUMBER_9 )
    #define MALLOC_BLOCK_SIZE_9     ( GZIGBEE_MALLOC_BLOCK_SIZE_9 )
  #endif
#else
  // Size not available
  #define MALLOC_BLOCK_NUMBER_9     ( 0 )
  #define MALLOC_BLOCK_SIZE_9       ( 0 )
  #if defined( GZIGBEE_MALLOC_BLOCK_NUMBER_9 ) || defined( GZIGBEE_MALLOC_BLOCK_SIZE_9 )
    #error GZIGBEE_MALLOC_BLOCK_NUMBER_9/SIZE_9 defined but unused (GZIGBEE_MALLOC_NB_BLOCK_SIZES < 10)
  #endif
#endif


// Index of the first block of each size in the global block info array
#define MALLOC_BLOCK_INDEX_0      ( 0UL )
#define MALLOC_BLOCK_INDEX_1      ( MALLOC_BLOCK_INDEX_0 +        \
                                      MALLOC_BLOCK_NUMBER_0 )
#define MALLOC_BLOCK_INDEX_2      ( MALLOC_BLOCK_INDEX_1 +        \
                                      MALLOC_BLOCK_NUMBER_1 )
#define MALLOC_BLOCK_INDEX_3      ( MALLOC_BLOCK_INDEX_2 +        \
                                      MALLOC_BLOCK_NUMBER_2 )
#define MALLOC_BLOCK_INDEX_4      ( MALLOC_BLOCK_INDEX_3 +        \
                                      MALLOC_BLOCK_NUMBER_3 )
#define MALLOC_BLOCK_INDEX_5      ( MALLOC_BLOCK_INDEX_4 +        \
                                      MALLOC_BLOCK_NUMBER_4 )
#define MALLOC_BLOCK_INDEX_6      ( MALLOC_BLOCK_INDEX_5 +        \
                                      MALLOC_BLOCK_NUMBER_5 )
#define MALLOC_BLOCK_INDEX_7      ( MALLOC_BLOCK_INDEX_6 +        \
                                      MALLOC_BLOCK_NUMBER_6 )
#define MALLOC_BLOCK_INDEX_8      ( MALLOC_BLOCK_INDEX_7 +        \
                                      MALLOC_BLOCK_NUMBER_7 )
#define MALLOC_BLOCK_INDEX_9      ( MALLOC_BLOCK_INDEX_8 +        \
                                      MALLOC_BLOCK_NUMBER_8 )

// Offset of the first block of each size in the global block data array
#define MALLOC_BLOCK_OFFSET_0     ( 0UL )
#define MALLOC_BLOCK_OFFSET_1     ( MALLOC_BLOCK_OFFSET_0 +       \
                                    ( MALLOC_BLOCK_SIZE_0 *       \
                                      MALLOC_BLOCK_NUMBER_0 ) )
#define MALLOC_BLOCK_OFFSET_2     ( MALLOC_BLOCK_OFFSET_1 +       \
                                    ( MALLOC_BLOCK_SIZE_1 *       \
                                      MALLOC_BLOCK_NUMBER_1 ) )
#define MALLOC_BLOCK_OFFSET_3     ( MALLOC_BLOCK_OFFSET_2 +       \
                                    ( MALLOC_BLOCK_SIZE_2 *       \
                                      MALLOC_BLOCK_NUMBER_2 ) )
#define MALLOC_BLOCK_OFFSET_4     ( MALLOC_BLOCK_OFFSET_3 +       \
                                    ( MALLOC_BLOCK_SIZE_3 *       \
                                      MALLOC_BLOCK_NUMBER_3 ) )
#define MALLOC_BLOCK_OFFSET_5     ( MALLOC_BLOCK_OFFSET_4 +       \
                                    ( MALLOC_BLOCK_SIZE_4 *       \
                                      MALLOC_BLOCK_NUMBER_4 ) )
#define MALLOC_BLOCK_OFFSET_6     ( MALLOC_BLOCK_OFFSET_5 +       \
                                    ( MALLOC_BLOCK_SIZE_5 *       \
                                      MALLOC_BLOCK_NUMBER_5 ) )
#define MALLOC_BLOCK_OFFSET_7     ( MALLOC_BLOCK_OFFSET_6 +       \
                                    ( MALLOC_BLOCK_SIZE_6 *       \
                                      MALLOC_BLOCK_NUMBER_6 ) )
#define MALLOC_BLOCK_OFFSET_8     ( MALLOC_BLOCK_OFFSET_7 +       \
                                    ( MALLOC_BLOCK_SIZE_7 *       \
                                      MALLOC_BLOCK_NUMBER_7 ) )
#define MALLOC_BLOCK_OFFSET_9     ( MALLOC_BLOCK_OFFSET_8 +       \
                                    ( MALLOC_BLOCK_SIZE_8 *       \
                                      MALLOC_BLOCK_NUMBER_8 ) )

// Total RAM size available for allocation
#define MALLOC_TOTAL_SIZE         ( ( MALLOC_BLOCK_SIZE_0 *       \
                                      MALLOC_BLOCK_NUMBER_0 ) +   \
                                    ( MALLOC_BLOCK_SIZE_1 *       \
                                      MALLOC_BLOCK_NUMBER_1 ) +   \
                                    ( MALLOC_BLOCK_SIZE_2 *       \
                                      MALLOC_BLOCK_NUMBER_2 ) +   \
                                    ( MALLOC_BLOCK_SIZE_3 *       \
                                      MALLOC_BLOCK_NUMBER_3 ) +   \
                                    ( MALLOC_BLOCK_SIZE_4 *       \
                                      MALLOC_BLOCK_NUMBER_4 ) +   \
                                    ( MALLOC_BLOCK_SIZE_5 *       \
                                      MALLOC_BLOCK_NUMBER_5 ) +   \
                                    ( MALLOC_BLOCK_SIZE_6 *       \
                                      MALLOC_BLOCK_NUMBER_6 ) +   \
                                    ( MALLOC_BLOCK_SIZE_7 *       \
                                      MALLOC_BLOCK_NUMBER_7 ) +   \
                                    ( MALLOC_BLOCK_SIZE_8 *       \
                                      MALLOC_BLOCK_NUMBER_8 ) +   \
                                    ( MALLOC_BLOCK_SIZE_9 *       \
                                      MALLOC_BLOCK_NUMBER_9 ) )

// Total number of blocks
#define MALLOC_BLOCK_COUNT        ( MALLOC_BLOCK_NUMBER_0 +       \
                                    MALLOC_BLOCK_NUMBER_1 +       \
                                    MALLOC_BLOCK_NUMBER_2 +       \
                                    MALLOC_BLOCK_NUMBER_3 +       \
                                    MALLOC_BLOCK_NUMBER_4 +       \
                                    MALLOC_BLOCK_NUMBER_5 +       \
                                    MALLOC_BLOCK_NUMBER_6 +       \
                                    MALLOC_BLOCK_NUMBER_7 +       \
                                    MALLOC_BLOCK_NUMBER_8 +       \
                                    MALLOC_BLOCK_NUMBER_9 )


//
// Debug configuration
//

#define MALLOC_WITH_ALLOC_ID    ( defined( GZIGBEE_DEBUG ) && \
                                  defined( OS_MEM_UTILITY_USE_MALLOC_ID ) )

#if MALLOC_WITH_ALLOC_ID
  #ifndef GZIGBEE_DEBUG_MALLOC_WARNING_TIME_MS
    #define MALLOC_WARNING_TIME_MS  ( 20000UL )
  #else
    #define MALLOC_WARNING_TIME_MS  GZIGBEE_DEBUG_MALLOC_WARNING_TIME_MS
  #endif
#endif

#define MALLOC_WITH_HISTOGRAM   ( defined( GZIGBEE_DEBUG ) && \
                                  defined( GZIGBEE_DEBUG_WITH_MALLOC_HISTOGRAM ) )

#if MALLOC_WITH_HISTOGRAM
  // Number of divisions in each band
  #define MALLOC_HISTO_SIZE_DIVISIONS     10
#endif


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

//
// Types for static memory manager
//

// Definition of a block of a given size
typedef struct
{
  unsigned32    Size;     // Size of blocks (currently limited to 65535)
  unsigned16    Number;   // Number of blocks of the given size
  unsigned16    Index;    // Index of the first block of the given size in the data array
  unsigned32    Offset;   // Offset in bytes of the first block of the given size in the data array
} tBlockDefinition;

// Info associated to each block than can be allocated
typedef struct
{
  size_t        Size;       // Size requested at allocation (allocated size is greater or equal)
  #if MALLOC_WITH_ALLOC_ID
    unsigned32  AllocTime;  // System time in ms when allocating the block
    MallocId    Id;         // Id given by the application to associate to the block
  #endif
} tBlockInfo;

// Allocation space
typedef struct
{
  unsigned8     Data[ MALLOC_TOTAL_SIZE ];        // Memory reserved for allocation
  tBlockInfo    BlockInfo[ MALLOC_BLOCK_COUNT ];  // Information on allocated blocks
} tMallocSpace;

/******************************************************************************
 *                      *****************************
 *                 *****       LOCAL VARIABLES       *****
 *                      *****************************
 ******************************************************************************/

// Memory space for static memory manager, 8-byte alignment
#if defined( __GNUC__ )       // GCC
   static tMallocSpace MallocSpace __attribute__ ((aligned (8)));
#elif defined( __ICCARM__ )   // IAR
   #pragma data_alignment = 8
   static tMallocSpace MallocSpace;
#else
 #error "appOsMemoryGlue: Unsupported compiler for alignment of MallocSpace"
#endif

// Array containing the number of free block for each size
static unsigned16 NbFreeBlocks[ MALLOC_NB_BLOCK_SIZES ];

// Array (in flash memory) containing block definition for each size
static const tBlockDefinition BlockDefinition[ MALLOC_NB_BLOCK_SIZES + 1 ] =
{
  #if MALLOC_NB_BLOCK_SIZES > 0
    { MALLOC_BLOCK_SIZE_0, MALLOC_BLOCK_NUMBER_0,
      MALLOC_BLOCK_INDEX_0, MALLOC_BLOCK_OFFSET_0 },
  #endif
  #if MALLOC_NB_BLOCK_SIZES > 1
    { MALLOC_BLOCK_SIZE_1, MALLOC_BLOCK_NUMBER_1,
      MALLOC_BLOCK_INDEX_1, MALLOC_BLOCK_OFFSET_1 },
  #endif
  #if MALLOC_NB_BLOCK_SIZES > 2
    { MALLOC_BLOCK_SIZE_2, MALLOC_BLOCK_NUMBER_2,
      MALLOC_BLOCK_INDEX_2, MALLOC_BLOCK_OFFSET_2 },
  #endif
  #if MALLOC_NB_BLOCK_SIZES > 3
    { MALLOC_BLOCK_SIZE_3, MALLOC_BLOCK_NUMBER_3,
      MALLOC_BLOCK_INDEX_3, MALLOC_BLOCK_OFFSET_3 },
  #endif
  #if MALLOC_NB_BLOCK_SIZES > 4
    { MALLOC_BLOCK_SIZE_4, MALLOC_BLOCK_NUMBER_4,
      MALLOC_BLOCK_INDEX_4, MALLOC_BLOCK_OFFSET_4 },
  #endif
  #if MALLOC_NB_BLOCK_SIZES > 5
    { MALLOC_BLOCK_SIZE_5, MALLOC_BLOCK_NUMBER_5,
      MALLOC_BLOCK_INDEX_5, MALLOC_BLOCK_OFFSET_5 },
  #endif
  #if MALLOC_NB_BLOCK_SIZES > 6
    { MALLOC_BLOCK_SIZE_6, MALLOC_BLOCK_NUMBER_6,
      MALLOC_BLOCK_INDEX_6, MALLOC_BLOCK_OFFSET_6 },
  #endif
  #if MALLOC_NB_BLOCK_SIZES > 7
    { MALLOC_BLOCK_SIZE_7, MALLOC_BLOCK_NUMBER_7,
      MALLOC_BLOCK_INDEX_7, MALLOC_BLOCK_OFFSET_7 },
  #endif
  #if MALLOC_NB_BLOCK_SIZES > 8
    { MALLOC_BLOCK_SIZE_8, MALLOC_BLOCK_NUMBER_8,
      MALLOC_BLOCK_INDEX_8, MALLOC_BLOCK_OFFSET_8 },
  #endif
  #if MALLOC_NB_BLOCK_SIZES > 9
    { MALLOC_BLOCK_SIZE_9, MALLOC_BLOCK_NUMBER_9,
      MALLOC_BLOCK_INDEX_9, MALLOC_BLOCK_OFFSET_9 },
  #endif
  // End of memory space reserved for allocation
  { 0, 0, 0, MALLOC_BLOCK_OFFSET_0 + MALLOC_TOTAL_SIZE }
};

/******************************************************************************
 * Initialize Malloc Simulator
 ******************************************************************************/
void appOsMemoryGlue_LocalMallocInit( void )
{
  uint8_t i = 0;

  // Initialize block info array. Data is not initialized as it is done by
  // function osMemAllocate.
  memset( (void*)MallocSpace.BlockInfo, 0, sizeof( MallocSpace.BlockInfo ) );

  // Set numbers of free blocks to their default value (all is free)
  for ( i = 0; i < MALLOC_NB_BLOCK_SIZES; i++ )
  {
    NbFreeBlocks[ i ] = BlockDefinition[ i ].Number;
  }
}

/******************************************************************************
 * Check allocated memory for timeout.
 * WARNING: Don't enable this function until Creates are switched to static!
 ******************************************************************************/
void appOsMemoryGlue_LocalMallocCheck( void )
{
#if MALLOC_WITH_ALLOC_ID
  int i;
  unsigned32 timeNow;
  unsigned32 diffTime;

  osTimeGetMilliseconds( appService, &timeNow );

  for ( i = 0; i < MALLOC_BLOCK_COUNT; i++ )
  {
    if ( ( MallocSpace.BlockInfo[ i ].Size != 0 ) &&
         ( MallocSpace.BlockInfo[ i ].Id < MALLOC_ID_CFG_DATA_MIN ) )
    {
      diffTime = (unsigned32)( timeNow - MallocSpace.BlockInfo[ i ].AllocTime );
      if ( diffTime > MALLOC_WARNING_TIME_MS )
      {
        // Shift creation time so we only get the warning every MALLOC_WARNING_TIME_MS
        MallocSpace.BlockInfo[ i ].AllocTime = timeNow;
        printApp( appService, "appMemGlue: WARNING: Allocated memory Id=%02d (%s) > %u ms."
                  "Probably missing a free!\n",
                  MallocSpace.BlockInfo[ i ].Id,
                  mallocIdtoString( MallocSpace.BlockInfo[ i ].Id ),
                  MALLOC_WARNING_TIME_MS );
      }
    }
  }
#endif
}


/******************************************************************************
 * Simulate Malloc in static memory
 ******************************************************************************/
#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
void* appMemGlue_Malloc( size_t size, unsigned8 ServiceId, MallocId Id )
#else
void* appMemGlue_Malloc( size_t size )
#endif
{
  int i = 0;
  int j = 0;
  void *Ptr = (void *)NULL;
  #ifdef GZIGBEE_DEBUG
    unsigned32 MatchingSize = 0;
  #endif
  #if MALLOC_WITH_HISTOGRAM
    int HistoBand = 0;
    int HistoDiv = 0;
  #endif

#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
  if (ServiceId != APP_CONFIG_M_SERVICE_ID)
    {
      printError(appService, "appMemGlue_Malloc: Bad service id ! %d\n", Id);
    }
#endif
    
  if ( size > 0 )
  {
    // Look for a matching block size, starting from smallest blocks
    while ( i < MALLOC_NB_BLOCK_SIZES )
    {
      if ( (unsigned32)size <= BlockDefinition[ i ].Size )
      {
        // Matching size found
        if ( NbFreeBlocks[ i ] != 0 )
        {
          // Free block available
          break;
        }
        #ifdef GZIGBEE_DEBUG
          else if ( MatchingSize == 0 )
          {
            // No free block of matching size, store size for later warning
            MatchingSize = BlockDefinition[ i ].Size;
          }
        #endif
      }

      i++;
    }

    if ( i < MALLOC_NB_BLOCK_SIZES )
    {
      // Look for the first free block
      j = BlockDefinition[ i ].Index;
      while ( j < ( BlockDefinition[ i ].Index + BlockDefinition[ i ].Number ) )
      {
        if ( MallocSpace.BlockInfo[ j ].Size == 0 )
        {
          // Allocate block
          MallocSpace.BlockInfo[ j ].Size = (unsigned32)size;
          #if MALLOC_WITH_ALLOC_ID
            osTimeGetMilliseconds( appService, &MallocSpace.BlockInfo[ j ].AllocTime );
            MallocSpace.BlockInfo[ j ].Id = Id;
          #endif

          // Decrement number of free blocks
          NbFreeBlocks[ i ]--;

          // Calculate address of allocated block
          Ptr = (void *)&MallocSpace.Data[ BlockDefinition[ i ].Offset +
                                           ( ( j - BlockDefinition[ i ].Index ) *
                                             BlockDefinition[ i ].Size ) ];

          // Update statistics
          #ifdef GZIGBEE_WITH_MALLOC_STATISTICS
            AllocStat.CurAllocated += BlockDefinition[ i ].Size;
            if ( AllocStat.CurAllocated > AllocStat.MaxAllocated )
            {
              AllocStat.MaxAllocated = AllocStat.CurAllocated;
            }
            AllocStat.CurUsed += (unsigned32)size;
            if ( AllocStat.CurUsed > AllocStat.MaxUsed )
            {
              AllocStat.MaxUsed = AllocStat.CurUsed;
            }
          #endif

          // Warning if a size bigger than the matching size has been allocated
          #ifdef GZIGBEE_DEBUG
            if ( MatchingSize != 0 )
            {
              printApp( appService, "appMemGlue: WARNING: no free block size %u, allocated "
                        "block size %u (Requested=%u)\n",
                        MatchingSize,
                        BlockDefinition[ i ].Size,
                        (unsigned32)size );
            }
          #endif

          // Update histogram
          #if MALLOC_WITH_HISTOGRAM
            FindHistogramDivision( (unsigned32)size, &HistoBand, &HistoDiv );
            MallocHistoBand[ HistoBand ].Info[ HistoDiv ].CurAllocCount++;
            if ( MallocHistoBand[ HistoBand ].Info[ HistoDiv ].CurAllocCount >
                 MallocHistoBand[ HistoBand ].Info[ HistoDiv ].MaxAllocCount )
            {
              MallocHistoBand[ HistoBand ].Info[ HistoDiv ].MaxAllocCount =
                MallocHistoBand[ HistoBand ].Info[ HistoDiv ].CurAllocCount;
            }
            if ( (unsigned32)size > MallocHistoBand[ HistoBand ].Info[ HistoDiv ].MaxSize )
            {
              MallocHistoBand[ HistoBand ].Info[ HistoDiv ].MaxSize = (unsigned32)size;
            }
            MallocHistoBand[ HistoBand ].TotalAllocCount++;
          #endif

          break;
        }

        j++;
      }
    }
    #ifdef GZIGBEE_DEBUG
      else
      {
        if ( (unsigned32)size > BlockDefinition[ MALLOC_NB_BLOCK_SIZES - 1 ].Size )
        {
          // Requested size is greater than max block size
          printApp( appService, "appMemGlue: ERROR: requested size %u exceeds max block size %u\n",
                    (unsigned32)size, BlockDefinition[ MALLOC_NB_BLOCK_SIZES - 1 ].Size );
        }
        else
        {
          // No more free blocks
          printApp( appService, "appMemGlue: WARNING: all blocks size >= %u are allocated\n",
                    (unsigned32)size );
        }
      }
    #endif
  }
  #ifdef GZIGBEE_DEBUG
    else
    {
      // Requested size = 0 !
      printApp( appService, "appMemGlue: WARNING: requested size = 0 !\n" );
    }
  #endif

  return( Ptr );
}

/******************************************************************************
 * Simulate Free in static memory
 ******************************************************************************/
#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
  void appMemGlue_Free( void* ptr,
                        unsigned8 ServiceId )
#else
  void appMemGlue_Free( void* ptr )
#endif
{
  int i = 0;
  int j = 0;
  size_t Offset = 0;
  size_t Size = 0;
  #if MALLOC_WITH_HISTOGRAM
    int HistoBand = 0;
    int HistoDiv = 0;
  #endif

#ifdef OS_MEM_UTILITY_USE_MALLOC_ID    
  if (ServiceId != APP_CONFIG_M_SERVICE_ID)
    {
      printError(appService, "appMemGlue_Free: Bad service id!\n");
    }
#endif
    
  if ( ptr != (void *)NULL )
  {
    // Offset of the block in the data buffer
    Offset = (size_t)ptr - (size_t)MallocSpace.Data;

    // Look for the block range containing the given offset
    while ( i < MALLOC_NB_BLOCK_SIZES )
    {
      if ( Offset < BlockDefinition[ i + 1 ].Offset )
      {
        // Matching offset range found, calculate the block index
        j = BlockDefinition[ i ].Index +
            ( ( Offset - BlockDefinition[ i ].Offset ) / BlockDefinition[ i ].Size );

        // Free the block if it is actually allocated
        Size = MallocSpace.BlockInfo[ j ].Size;
        if ( Size != 0 )
        {
          // Update statistics
          #ifdef GZIGBEE_WITH_MALLOC_STATISTICS
            AllocStat.CurAllocated -= BlockDefinition[ i ].Size;
            AllocStat.CurUsed -= Size;
          #endif

          // Update histogram
          #if MALLOC_WITH_HISTOGRAM
            FindHistogramDivision( Size, &HistoBand, &HistoDiv );
            MallocHistoBand[ HistoBand ].Info[ HistoDiv ].CurAllocCount--;
            MallocHistoBand[ HistoBand ].TotalFreeCount++;
          #endif

          // Free the block
          MallocSpace.BlockInfo[ j ].Size = 0;
          NbFreeBlocks[ i ]++;
        }
        #ifdef GZIGBEE_DEBUG
          else
          {
            // Block is not allocated
            #ifdef OS_MEM_UTILITY_USE_MALLOC_ID
              printApp( appService, "appMemGlue: Warning: Free attempted for Id %s (0x%02X)"
                        "but block is already free\n",
                        mallocIdtoString( MallocSpace.BlockInfo[ j ].Id ),
                        MallocSpace.BlockInfo[ j ].Id );
            #else
              printApp( appService, "appMemGlue: Warning: Free attempted but block is already free\n" );
            #endif
          }
        #endif
        break;
      }

      i++;
    }

    #ifdef GZIGBEE_DEBUG
      if ( i >= MALLOC_NB_BLOCK_SIZES )
      {
        printApp( appService, "appMemGlue: Warning: Free attempted from 0x%p but block not found\n", ptr );
      }
    #endif
  }
}

/******************************************************************************
 *                      *****************************
 *                 ***** ZAB MEM MANGEMENT FUNCTIONS *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 * Allocate
 ******************************************************************************/
#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
void* osMemAllocate( erStatus* Status, unsigned8 ServiceId, size_t Length, MallocId Id)
#else
void* osMemAllocate( erStatus* Status, size_t Length )
#endif
{	
  void *pBuffer = NULL;

#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
  pBuffer = APP_MEM_GLUE_MALLOC(Length, ServiceId, Id);
#else
  pBuffer = APP_MEM_GLUE_MALLOC(Length, NULL, MALLOC_ID_NONE);
#endif

  if (pBuffer == NULL)
	{
      erStatusSet( Status, OS_ERROR_MEM_NONE);    
	}
  else
    {
      // Fill allocated memory with ZERO
      memset( (unsigned8 *)pBuffer, 0, Length );
    }
  return ( pBuffer );
}


/******************************************************************************
 * Free
 ******************************************************************************/
#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
void osMemFree ( erStatus* Status, unsigned8 ServiceId, void* Buffer )
#else
void osMemFree ( erStatus* Status, void* Buffer )
#endif
{
  if (Buffer != NULL)
  {
#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
    appMemGlue_Free( Buffer, ServiceId );
#else
    appMemGlue_Free( Buffer );
#endif
  }
  else
  {
    erStatusSet( Status, OS_ERROR_MEM_INVALID );
  }
}


/******************************************************************************
 * Zero
 ******************************************************************************/
void osMemZero ( erStatus *Status,
                 unsigned8 *Buffer,
                 size_t Length )
{
  if (Buffer != NULL)
  {
    memset( Buffer, 0, Length );
  }
  else
  {
    erStatusSet( Status, OS_ERROR_MEM_INVALID );
  }
}

/******************************************************************************
 * Set
 ******************************************************************************/
void osMemSet ( erStatus *Status,
                unsigned8 *Buffer,
                size_t Length,
                unsigned8 Byte )
{
  if (Buffer != NULL)
  {
    memset( Buffer, Byte, Length );
  }
  else
  {
    erStatusSet( Status, OS_ERROR_MEM_INVALID );
  }
}

/******************************************************************************
 * Copy
 ******************************************************************************/
void osMemCopy ( erStatus *Status,
                 unsigned8 *Dest,
                 unsigned8 *Source,
                 size_t Length )
{
  if ((Dest != NULL) && (Source != NULL))
  {
    memcpy( Dest, Source, Length );
  }
  else
  {
    erStatusSet( Status, OS_ERROR_MEM_INVALID );
  }
}


/******************************************************************************
 * Compare
 ******************************************************************************/
signed8 osMemCmp( erStatus *Status,
                  unsigned8 *Ptr1,
                  unsigned8 *Ptr2,
                  size_t Length )
{
  if ((Ptr1 != NULL) && (Ptr2 != NULL))
  {
    return( memcmp( Ptr1, Ptr2, Length ) );
  }
  else
  {
    erStatusSet( Status, OS_ERROR_MEM_INVALID );
    if ( Ptr2 == NULL )
    {
      return ( 1 );
    }
    else
    {
      return ( -1 );
    }
  }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/