#ifndef _IDENTIFY_CLUSTER_H_
#define _IDENTIFY_CLUSTER_H_

#include "zabTypes.h"
#include "szl.h"

/**
 * Identify Notifications
 */
typedef void (*SzlPlugin_IdentifyClusterNotification_CB_t) (zabService* Service, szl_uint8 endpoint, szl_bool identifying);



/**
 * Identify Clusters Initialization
 *
 * This function must be called from the application after the Library has been initialized.
 *
 * The plugin will hereafter be self contained.
 */
extern 
SZL_RESULT_t SzlPlugin_IdentifyClusterInit(zabService* Service, 
                                           szl_uint8 numEndpoints, 
                                           szl_uint8 * endpointList, 
                                           SzlPlugin_IdentifyClusterNotification_CB_t callback);

/**
 * Identify Cluster Destroy
 */
extern 
SZL_RESULT_t SzlPlugin_IdentifyCluster_Destroy(zabService* Service);

/**
 * @}
 */

#endif /*_IDENTIFY_CLUSTER_H_*/
