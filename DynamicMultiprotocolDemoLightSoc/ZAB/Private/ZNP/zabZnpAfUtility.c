/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the ZNP AF Functions
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.06.00  12-Jun-14   MvdB   Add Wireless Test Bench functions
 * 00.00.06.04  07-Oct-14   MvdB   artf104116: Add RSSI to received packet quality indication
 * 002.000.008  15-Apr-15   MvdB   ARTF130931: Fix message buffer corruption and Assert when AfDataRequest creates message of size ZAB_VENDOR_MIN_DATA if VLA_INIT != 0
 * 002.001.001  15-Jul-15   MvdB   ARTF130228: Add unique address mode for GpSrcId
 * 002.001.003  16-Jul-15   MvdB   ARTF133822/132827: Include source and destination addressing on AfIncoming frames
 * 002.002.004  02-Sep-15   MvdB   ARTF146663: Remove error set in processZnpStatus() as we already push it to the app as a notification
 * 002.002.021  20-Apr-16   MvdB   ARTF167736: Add serial retries to improve robustness against lossy serial link
 *****************************************************************************/

#include "zabZnpService.h"
#include "wtbUtility.h"

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* Latency for endpoint registration */
#define M_ZNP_LATENCY 0


#define AF_BROADCAST_ENDPOINT              0xFF



/* ZNP message length macros */
#define AF_DATA_REQUEST_EXT_DATA_LENGTH(ld)  (zabznpAf_AF_DATA_RESQUEST_EXT_REQ_SIZE + (ld))

/* Register is 9 bytes plus 2 bytes for each in/out cluster
 * Struct not used as the two variable length array's make it a mess */
#define AF_REGISTER_DATA_LENGTH(in, out)  (9 + ((in) * 2) + 1 + ((out) *2))


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/



typedef enum
{
  afAddrNotPresent = 0,
  afAddrGroup      = 1,
  afAddr16Bit      = 2,
  afAddr64Bit      = 3,
  afAddrBroadcast  = 15,
} afAddrMode_t;


//------------------------------------------------------------------------------
// Struct Status For All ZNP RESPONSE
// - AF_REGISTER_RSP
// - AF_DATA_REQUEST_RSP
// - AF_DATA_REQUEST_EXT_RSP
// - AF_DATA_REQUEST_SRC_RTG_RSP
// - AF_INTER_PAN_CTL_RSP
// - AF_DATA_STORE_RSP
// - AF_APSF_CONFIG_SET_RSP
typedef struct s_znpAfStatus
{
    unsigned8 status;       // Status
} zabznpAf_StatusResp;

/*
 * Struct ZNP of Data Request Extended
 */
typedef struct s_znpDataRequestExt
{
    unsigned8 destinationAddrMode;
    unsigned8 destinationAddr[8];
    unsigned8 destinationEndPoint;
    unsigned8 destinationPanId[2];
    unsigned8 sourceEndpoint;
    unsigned8 clusterID[2];
    unsigned8 transactionID;
    unsigned8 options;
    unsigned8 radius;
    unsigned8 len[2];
    unsigned8 data[VLA_INIT];      // Max data 99 bytes
} zabznpAf_AF_DATA_RESQUEST_EXT_REQ;
#define zabznpAf_AF_DATA_RESQUEST_EXT_REQ_SIZE (sizeof(zabznpAf_AF_DATA_RESQUEST_EXT_REQ) - (VLA_INIT*sizeof(unsigned8)))
/*
 * Struct ZNP of Data Comfirm
 */
typedef struct s_znpDataConfirm
{
    zabznpAf_StatusResp status;
    unsigned8 endPoint;
    unsigned8 transactionID;
} zabznpAf_AF_DATA_CONFIRM;

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/




/**
 * Parse Field Status for all Message with the format 'zabznpAf_StatusResp'
 * @param Status[in,out]    ZabStatus
 * @param Message[in]       input ZNP Message
 * @param status[out]       status
 */
static void zabParseZNPAf_GetStatus(erStatus* Status, sapMsg* Message, unsigned8* RspStatusPtr)
{
    zabznpAf_StatusResp* data = NULL;
    unsigned8 len;

    ER_CHECK_STATUS_NULL(Status, RspStatusPtr);

    zabParseZNPGetData(Status, Message, &len, (unsigned8 **)&data);
    ER_CHECK_STATUS(Status);

    if ( (data != NULL) && (len >= sizeof(zabznpAf_StatusResp)) )
    {
        *RspStatusPtr = data->status;
    }
    else
    {
        erStatusSet( Status, ZAB_ERROR_VENDOR_PARSE);
    }
}

// For AF_DATA_REQUEST_EXT_REQ
/**
 *
 * @param Status
 * @param Message
 * @param address
 */
static void parseDataRequestExt_SetDstAddr( erStatus* Status, sapMsg* Message, const zabProAddress *address )
{
    zabznpAf_AF_DATA_RESQUEST_EXT_REQ *msgData;
    unsigned8 msgLen;

    zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
    ER_CHECK_STATUS(Status);

    switch (address->addressMode)
    {
        case ZAB_ADDRESS_MODE_GROUP:
            msgData->destinationAddrMode = afAddrGroup;
            COPY_OUT_16_BITS(msgData->destinationAddr, address->address.shortAddress);
            break;
        case ZAB_ADDRESS_MODE_IEEE_ADDRESS:
            msgData->destinationAddrMode = afAddr64Bit;
            COPY_OUT_64_BITS(msgData->destinationAddr, address->address.ieeeAddress);
            msgData->destinationEndPoint = address->endpoint;
            break;
        case ZAB_ADDRESS_MODE_GP_SOURCE_ID:
            msgData->destinationAddrMode = afAddr64Bit;
            COPY_OUT_64_BITS(msgData->destinationAddr, (address->address.sourceId | ZNP_M_GP_SRC_ID_IEEE_EXTENSION));
            msgData->destinationEndPoint = ZAB_GP_ENDPOINT;
            break;
        case ZAB_ADDRESS_MODE_BROADCAST:
            msgData->destinationAddrMode = afAddrBroadcast;
            msgData->destinationEndPoint = AF_BROADCAST_ENDPOINT;
            COPY_OUT_16_BITS(msgData->destinationAddr, address->address.shortAddress);
            break;
        case ZAB_ADDRESS_MODE_NWK_ADDRESS:
            msgData->destinationAddrMode = afAddr16Bit;
            COPY_OUT_16_BITS(msgData->destinationAddr, address->address.shortAddress);
            msgData->destinationEndPoint = address->endpoint;
            break;
        case ZAB_ADDRESS_MODE_VIA_BIND:
            msgData->destinationAddrMode = afAddrNotPresent;
            break;
        default:
            erStatusSet( Status, ZAB_ERROR_MSG_PARAM_INVALID);
            break;
    }

    zabParseZNPSetLength(Status, Message, AF_DATA_REQUEST_EXT_DATA_LENGTH(COPY_IN_16_BITS(msgData->len)));
}

static void parseDataRequestExt_SetDstPANID( erStatus* Status, sapMsg* Message, unsigned16 panId)
{
    zabznpAf_AF_DATA_RESQUEST_EXT_REQ *msgData;
    unsigned8 msgLen;

    zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
    ER_CHECK_STATUS(Status);

    COPY_OUT_16_BITS(msgData->destinationPanId,panId);

    zabParseZNPSetLength(Status, Message, AF_DATA_REQUEST_EXT_DATA_LENGTH(COPY_IN_16_BITS(msgData->len)));
}

static void parseDataRequestExt_SetSrcEndPoint( erStatus* Status, sapMsg* Message, unsigned8 endPoint)
{
    zabznpAf_AF_DATA_RESQUEST_EXT_REQ *msgData;
    unsigned8 msgLen;

    zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
    ER_CHECK_STATUS(Status);

    msgData->sourceEndpoint = endPoint;

    zabParseZNPSetLength(Status, Message, AF_DATA_REQUEST_EXT_DATA_LENGTH(COPY_IN_16_BITS(msgData->len)));
}

static void parseDataRequestExt_SetClusterID( erStatus* Status, sapMsg* Message, unsigned16 clusterID)
{
    zabznpAf_AF_DATA_RESQUEST_EXT_REQ *msgData;
    unsigned8 msgLen;

    zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
    ER_CHECK_STATUS(Status);

    COPY_OUT_16_BITS(msgData->clusterID,clusterID);

    zabParseZNPSetLength(Status, Message, AF_DATA_REQUEST_EXT_DATA_LENGTH(COPY_IN_16_BITS(msgData->len)));
}

static void parseDataRequestExt_SetTransactionID( erStatus* Status, sapMsg* Message, unsigned8 id)
{
    zabznpAf_AF_DATA_RESQUEST_EXT_REQ *msgData;
    unsigned8 msgLen;

    zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
    ER_CHECK_STATUS(Status);

    msgData->transactionID = id;

    zabParseZNPSetLength(Status, Message, AF_DATA_REQUEST_EXT_DATA_LENGTH(COPY_IN_16_BITS(msgData->len)));
}

static void parseDataRequestExt_SetOptions( erStatus* Status, sapMsg* Message, unsigned8 opt)
{
    zabznpAf_AF_DATA_RESQUEST_EXT_REQ *msgData;
    unsigned8 msgLen;

    zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
    ER_CHECK_STATUS(Status);

    msgData->options = opt;

    zabParseZNPSetLength(Status, Message, AF_DATA_REQUEST_EXT_DATA_LENGTH(COPY_IN_16_BITS(msgData->len)));
}

static void parseDataRequestExt_SetRadius( erStatus* Status, sapMsg* Message, unsigned8 radius)

{
    zabznpAf_AF_DATA_RESQUEST_EXT_REQ *msgData;
    unsigned8 msgLen;

    zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
    ER_CHECK_STATUS(Status);

    msgData->radius = radius;

    zabParseZNPSetLength(Status, Message, AF_DATA_REQUEST_EXT_DATA_LENGTH(COPY_IN_16_BITS(msgData->len)));
}

static void parseDataRequestExt_SetData( erStatus* Status, sapMsg* Message, unsigned16 length, unsigned8 *data )
{
    zabznpAf_AF_DATA_RESQUEST_EXT_REQ *msgData;
    unsigned8 msgLen;

    ER_CHECK_NULL(Status, data);

    zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
    ER_CHECK_STATUS(Status);

    COPY_OUT_16_BITS(msgData->len,length);

    osMemCopy(Status, msgData->data, data, length );
    ER_CHECK_STATUS(Status);

    zabParseZNPSetLength(Status, Message, AF_DATA_REQUEST_EXT_DATA_LENGTH(length));
}




/******************************************************************************
 * Af Data Incoming OR Af Data Incoming Extended
 * Convert from Vendor to ZAB format and send upwards
 ******************************************************************************/
#define M_AF_INCOMING_MSG_LENGTH 20
#define M_AF_INCOMING_MSG_DATA_LENGTH_OFFSET 0x10
#define M_AF_INCOMING_MSG_EXT_LENGTH 30
#define M_AF_INCOMING_MSG_EXT_DATA_LENGTH_OFFSET 0x19
static void processAfIncomingMsg(erStatus* Status, zabService* Service, sapMsg* vendorMessage, zab_bool AfIncomingExtendedMsg)
{
  unsigned8* znp;
  sapMsg* dataMessage = NULL;
  unsigned16 dataMessageLength;
  unsigned8 znpDataLength;
  unsigned16 zclDataLength;
  zabMsgProDataInd* dataInd;
  unsigned16 lengthCheck;
  unsigned8 znpIndex;
  unsigned16 groupId;
  unsigned8 dstEp;
  unsigned8 wasBroadcast;

  printVendor(Service, "zabZnpoAfUtility: AF Data Ind\n");

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  znp = ZAB_MSG_ZNP_DATA(vendorMessage);
  ER_CHECK_NULL(Status, znp);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);

  /* Determine for each frame:
   *  zclDataLength, so we know how many bytes of ZCL data we have
   *  lengthCheck, so we know how long the frame should be to be correct and can verify it is complete */
  lengthCheck = 0;
  zclDataLength = 0;
  if (AfIncomingExtendedMsg == zab_true)
  {
    if (znpDataLength >= M_AF_INCOMING_MSG_EXT_LENGTH)
    {
      zclDataLength = COPY_IN_16_BITS(&znp[M_AF_INCOMING_MSG_EXT_DATA_LENGTH_OFFSET]);
      lengthCheck = M_AF_INCOMING_MSG_EXT_LENGTH + zclDataLength;
    }
  }
  else
  {
    if (znpDataLength >= M_AF_INCOMING_MSG_LENGTH)
    {
      zclDataLength = znp[M_AF_INCOMING_MSG_DATA_LENGTH_OFFSET];
      lengthCheck = M_AF_INCOMING_MSG_LENGTH + zclDataLength;
    }
  }

  /* Check length is big enough for header, then also for header + data */
  if (znpDataLength >= lengthCheck)
  {
    // Allocate message upwards
    dataMessageLength = zabMsgProDataInd_SIZE() + zclDataLength;
    dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, dataMessageLength);
    if (dataMessage == NULL)
      {
        return;
      }

    sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_AF_DATA_IN);
    sapMsgSetAppDataLength(Status, dataMessage, dataMessageLength);

    /* Populate message data */
    dataInd = (zabMsgProDataInd*)sapMsgGetAppData(dataMessage);
    dataInd->version = 0;

    znpIndex = 0;
    groupId = COPY_IN_16_BITS(&znp[znpIndex]); znpIndex += 2;
    dataInd->cluster = COPY_IN_16_BITS(&znp[znpIndex]); znpIndex += 2;

    if (AfIncomingExtendedMsg == zab_true)
    {
      dataInd->srcAddr.addressMode = (zabAddressMode)znp[znpIndex++];

      if (dataInd->srcAddr.addressMode == ZAB_ADDRESS_MODE_IEEE_ADDRESS)
      {
        dataInd->srcAddr.address.ieeeAddress = COPY_IN_64_BITS(&znp[znpIndex]); znpIndex += 8; // SrcAddr
      }
      else
      {
        dataInd->srcAddr.address.shortAddress = COPY_IN_16_BITS(&znp[znpIndex]); znpIndex += 8; // SrcAddr
      }

      dataInd->srcAddr.endpoint = znp[znpIndex++];

      /* Convert IEEE and GPEP to GP Source Id address mode */
      if ( (dataInd->srcAddr.addressMode == ZAB_ADDRESS_MODE_IEEE_ADDRESS) &&
            (dataInd->srcAddr.endpoint == ZAB_GP_ENDPOINT) )
      {
        dataInd->srcAddr.addressMode = ZAB_ADDRESS_MODE_GP_SOURCE_ID;
        dataInd->srcAddr.address.sourceId = (unsigned32)dataInd->srcAddr.address.ieeeAddress;
      }

      znpIndex += 2; // SrcPanId
      dstEp = znp[znpIndex++];
      wasBroadcast = znp[znpIndex++];


    }
    else
    {
      dataInd->srcAddr.addressMode = ZAB_ADDRESS_MODE_NWK_ADDRESS;
      dataInd->srcAddr.address.shortAddress = COPY_IN_16_BITS(&znp[znpIndex]); znpIndex += 2;
      dataInd->srcAddr.endpoint = znp[znpIndex++];
      dstEp = znp[znpIndex++];
      wasBroadcast = znp[znpIndex++];
    }

    /* Setup the default unicast info */
    dataInd->dstAddr.addressMode = ZAB_ADDRESS_MODE_NWK_ADDRESS;
    dataInd->dstAddr.endpoint = dstEp;
    /* If broadcast, and not a direct GP frame set mode and broadcast id*/
    if (wasBroadcast > 0)
      {
        if (dataInd->srcAddr.addressMode != ZAB_ADDRESS_MODE_GP_SOURCE_ID)
          {
            dataInd->dstAddr.addressMode = ZAB_ADDRESS_MODE_BROADCAST;
            dataInd->dstAddr.address.shortAddress = ZAB_BROADCAST_ALL;
          }
      }
    /* Not broadcast and group != 0 means it was group addressed */
    else if (groupId != 0)
      {
        dataInd->dstAddr.addressMode = ZAB_ADDRESS_MODE_GROUP;
        dataInd->dstAddr.address.shortAddress = groupId;
      }

    /* Continue processing frame */
    dataInd->lqi = znp[znpIndex++];
    dataInd->securityStatus = znp[znpIndex++];
    dataInd->timeStamp = COPY_IN_32_BITS(&znp[znpIndex]); znpIndex += 4;
    znpIndex++; // Skip MT Transaction Sequence Number, we don't need it.

    // Skip over Length, we already have it in zclDataLength
    if (AfIncomingExtendedMsg == zab_true)
    {
      znpIndex += 2;
    }
    else
    {
      znpIndex++;
    }

    dataInd->dataLength = zclDataLength;
    osMemCopy( Status, dataInd->data, &znp[znpIndex], zclDataLength);
    znpIndex += zclDataLength;

    dataInd->macSourceAddress = COPY_IN_16_BITS(&znp[znpIndex]); znpIndex += 2;
    znpIndex++; // Skip Radius, we don't care about it

    /* ARTF104116: Extension to include RSSI */
    dataInd->rssi = 127;
    if (znpDataLength >= (lengthCheck + 2))
    {
      if (znp[znpIndex++] == SCHNEIDER_MT_EXTENSION_IDENTIFIER_VERSION1)
        {
          dataInd->rssi = (signed8)znp[znpIndex++];
        }
    }

#ifdef ZAB_VND_CFG_ENABLE_WIRELESS_TEST_BENCH
    if ( erStatusIsOk(Status) && (ZAB_SERVICE_ZNP(Service)->DataIndicationNotificationCallback != NULL) )
    {
      ZAB_SERVICE_ZNP(Service)->DataIndicationNotificationCallback(Service, dataInd);
    }
#endif

    sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
  }
  else
  {
    erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
  }
}


/******************************************************************************
 * Af Data Confirm
 ******************************************************************************/
static void processAfDataConfirm(erStatus* Status, zabService* Service, sapMsg* vendorMessage)
{
  zabznpAf_AF_DATA_CONFIRM* cnf;
  unsigned8 znpDataLength;
#ifdef ZAB_VENDOR_UNLOCK_SERIAL_ON_AF_DATA_CONFIRM
  unsigned8 cmd;
#endif

  ER_CHECK_STATUS_NULL( Status, vendorMessage);
  cnf = (zabznpAf_AF_DATA_CONFIRM*)ZAB_MSG_ZNP_DATA(vendorMessage);
  ER_CHECK_NULL(Status, cnf);
  znpDataLength = ZAB_MSG_ZNP_DATA_LENGTH(vendorMessage);

  /* Check length is big enough for header, then also for header + data */
  if (znpDataLength >= sizeof(zabznpAf_AF_DATA_CONFIRM))
    {
      printVendor(Service, "zabZnpoAfUtility: AF Data Cnf: TID = 0x%02X, Status = 0x%02X\n", cnf->transactionID, cnf->status.status);
      if (cnf->status.status != ZNPI_API_ERR_SUCCESS)
        {
          zabParseZNP_AppErrorIn(Service, cnf->transactionID, ZAB_ERROR_VENDOR_DATA_CONFIRM_ERROR);
        }


#ifdef ZAB_VENDOR_UNLOCK_SERIAL_ON_AF_DATA_CONFIRM
      /* Unlock data queue if the req was a data request with the TID matching the confirm! */
      // TODO - ARTF55511: this is a bit a of a mess!!!!!!!
      cmd = ZAB_SERVICE_ZNP(Service) ->sreqTimeOut.commandId;
      if ( (ZAB_SERVICE_ZNP(Service) ->sreqTimeOut.subsystem == ZNPI_SUBSYSTEM_AF) &&
           ( (cmd == ZNPI_CMD_AF_DATA_REQUEST) || (cmd == ZNPI_CMD_AF_DATA_REQUEST_EXT) ) &&
           (ZAB_SERVICE_ZNP(Service)->sreqTimeOut.tid == cnf->transactionID))
        {
          printVendor(Service,  "zabZnpService: UNLOCK ZNP FROM AF DATA CONFIRM: OK\n");
          zabZnpService_UnlockAndFreeTxMessage(Status, Service, NULL);
        }
#endif



#ifdef ZAB_VND_CFG_ENABLE_WIRELESS_TEST_BENCH
          if ( ZAB_SERVICE_ZNP(Service)->DataConfirmNotificationCallback != NULL)
            {
              ZAB_SERVICE_ZNP(Service)->DataConfirmNotificationCallback(Service, cnf->transactionID, wtbUtility_ZnpCnfType_DataConfirm, (ZNPI_API_ERROR_CODES)cnf->status.status);
            }
#endif

    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }

}

/******************************************************************************
 * Process status from an SRSP
 ******************************************************************************/
static void processZnpStatus(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8 byteStatus = 0;

#ifdef ZAB_VND_CFG_ENABLE_WIRELESS_TEST_BENCH
  unsigned8 cmd;
  zabParseZNPGetCommandID(Status, Message, &cmd);
#endif

  /* get the corresponding information from this frame */
  zabParseZNPAf_GetStatus(Status, Message, &byteStatus);

  printVendor(Service, "TID = 0x%02X, Status : 0x%02X\n", sapMsgGetAppTransactionId(Message), byteStatus);

#ifdef ZAB_VND_CFG_ENABLE_WIRELESS_TEST_BENCH
  if ( (cmd == ZNPI_CMD_AF_DATA_REQUEST_EXT) &&
      (ZAB_SERVICE_ZNP(Service)->DataConfirmNotificationCallback != NULL) )
    {
      ZAB_SERVICE_ZNP(Service)->DataConfirmNotificationCallback(Service, sapMsgGetAppTransactionId(Message), wtbUtility_ZnpCnfType_Srsp, (ZNPI_API_ERROR_CODES)byteStatus);
    }
#endif

  if (byteStatus != ZNPI_API_ERR_SUCCESS)
    {
      printError(Service, "ERROR: AfDataReqSRSP: TID = 0x%02X, Status : 0x%02X\n", sapMsgGetAppTransactionId(Message), byteStatus);

      /* Notify error up to App data service */
      zabParseZNP_AppErrorIn(Service, sapMsgGetAppTransactionId(Message), ZAB_ERROR_VENDOR_SENDING);
    }
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 * Process all messages in the AF subsystem
 ******************************************************************************/
void zabZnpAfUtility_ProcessMsg(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8 cmd;

  zabParseZNPGetCommandID(Status, Message, &cmd);

  switch (cmd)
    {
      case ZNPI_CMD_AF_REGISTER:
        printVendor(Service, "zabZnpoAfUtility: AF Register Rsp: ");
        processZnpStatus(Status, Service, Message);
        break;

      case ZNPI_CMD_AF_DATA_REQUEST:
      case ZNPI_CMD_AF_DATA_REQUEST_EXT:
        printVendor(Service, "zabZnpoAfUtility: AF Data Req Rsp: ");
          processZnpStatus(Status, Service, Message);
          break;

      case ZNPI_CMD_AF_DATA_CONFIRM:
        processAfDataConfirm(Status, Service, Message);
        break;

      case ZNPI_CMD_AF_INCOMING_MSG:
        processAfIncomingMsg(Status, Service, Message, zab_false);
        break;

      case ZNPI_CMD_AF_INCOMING_MSG_EXT:
        processAfIncomingMsg(Status, Service, Message, zab_true);
        break;

      default:
          printError(Service, "zabZnpoAfUtility: Unknown message 0x%02X\n", cmd);
          break;
    }
}





/******************************************************************************
 * Data Request - Extended
 * This sends a ZigBee data request, as used for ZCL commands.
 * Extended function is used to support unicast, groupcast, broadcast, ieee and bind addressing
 ******************************************************************************/
void zabZnpAfUtility_DataRequestExt(erStatus* Status, zabService* Service, sapMsg *appMsg)
{
  sapMsg* Msg = NULL;
  int i;
  zabMsgProDataReq *dataReqMsg;

  /* Validate the vendor is open and networked */
  zabZnpService_GetNetworkCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  printVendor(Service, "zabZnpoAfUtility: Data Req Ext. Tid=0x%02X\n", sapMsgGetAppTransactionId(appMsg));

  /*Collect all required data from current Message*/
  dataReqMsg = (zabMsgProDataReq*)sapMsgGetAppData(appMsg);

  if(dataReqMsg->dataLength > 230)
  {
    erStatusSet( Status, ZAB_ERROR_MSG_APS_INVALID);
    return;
  }


  Msg = zabParseZNPCreateRequest(Status, Service, AF_DATA_REQUEST_EXT_DATA_LENGTH(dataReqMsg->dataLength) );
  ER_CHECK_NULL(Status, Msg);
  ER_CHECK_STATUS(Status);

  /* Copy the TID from the application message to the ZNP message */
  sapMsgSetAppTransactionId(Status, Msg, sapMsgGetAppTransactionId(appMsg));

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_AF);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_AF_DATA_REQUEST_EXT);


  /* load the data area */
  /*
   * Note: Creating parsing function like this that are only used once is inefficient and creates more code to maintain
   *       Please just do it within this function unless it is something that is re-used elsewhere
   */
  parseDataRequestExt_SetDstAddr(Status, Msg, &dataReqMsg->dstAddress);
  parseDataRequestExt_SetSrcEndPoint(Status, Msg, dataReqMsg->srcEndpoint);
  parseDataRequestExt_SetClusterID(Status, Msg, dataReqMsg->cluster);
  parseDataRequestExt_SetDstPANID(Status, Msg, 0x0000);

  parseDataRequestExt_SetTransactionID(Status, Msg, sapMsgGetAppTransactionId(appMsg));
  parseDataRequestExt_SetOptions(Status, Msg, dataReqMsg->txOptions);
  parseDataRequestExt_SetRadius(Status, Msg, dataReqMsg->radius);
  parseDataRequestExt_SetData(Status, Msg, dataReqMsg->dataLength, dataReqMsg->data);

  printVendor(Service, "zabZnpAfUtility_DataRequestExt: TID = 0x%02X, Len = 0x%X, Data = ", sapMsgGetAppTransactionId(appMsg), dataReqMsg->dataLength);
  for (i =0; i < dataReqMsg->dataLength; i++)
    {
      printVendor(Service, "%02X ", dataReqMsg->data[i]);
    }
  printVendor(Service, "\n");


  zabZnpService_SendMsg(Status, Service, Msg);

#ifdef ZAB_VND_CFG_ENABLE_WIRELESS_TEST_BENCH
  if ( erStatusIsOk(Status) && (ZAB_SERVICE_ZNP(Service)->DataRequestNotificationCallback != NULL) )
    {
      ZAB_SERVICE_ZNP(Service)->DataRequestNotificationCallback(Service, sapMsgGetAppTransactionId(appMsg), dataReqMsg);
    }
#endif
}


/******************************************************************************
 * Register an endpoint in ZNP
 ******************************************************************************/
void zabZnpAfUtility_RegisterEndpoint(erStatus* Status, zabService* Service, sapMsg *appMsg)
{
  sapMsg* Msg = NULL;
  unsigned8* znp;
  unsigned8 i;

  /* Validate the vendor is open */
  zabZnpService_GetLocalCommsReady(Status, Service);
  ER_CHECK_STATUS(Status);

  zabMsgProSimpleDescriptor* simpleDesc = (zabMsgProSimpleDescriptor*)sapMsgGetAppData(appMsg);

  ER_CHECK_STATUS_NULL(Status, simpleDesc);
  printVendor(Service, "zabZnpoAfUtility: Register Endpoint 0x%02X, DevID 0x%02X\n",
            simpleDesc->endpoint,
            simpleDesc->deviceId);

  Msg = zabParseZNPCreateRequest(Status, Service, AF_REGISTER_DATA_LENGTH(simpleDesc->numInClusters, simpleDesc->numOutClusters));
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Copy the TID from the application message to the ZNP message */
  sapMsgSetAppTransactionId(Status, Msg, sapMsgGetAppTransactionId(appMsg));

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_AF);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_AF_REGISTER);

  znp = (ZAB_MSG_ZNP(Msg)->Data);

  *znp++ = simpleDesc->endpoint;
  COPY_OUT_16_BITS(znp, simpleDesc->profileId); znp+=2;
  COPY_OUT_16_BITS(znp, simpleDesc->deviceId); znp+=2;
  *znp++ = simpleDesc->deviceVersion;
  *znp++ = M_ZNP_LATENCY;

  *znp++ = simpleDesc->numInClusters;
  printVendor(Service, "\tInClusters: Num = 0x%02X, IDs =", simpleDesc->numInClusters);
  for (i = 0; i < simpleDesc->numInClusters; i++)
    {
      COPY_OUT_16_BITS(znp, simpleDesc->clusterList[i]); znp+=2;
      printVendor(Service, " 0x%04X", simpleDesc->clusterList[i]);
    }
  printVendor(Service, "\n");

  *znp++ = simpleDesc->numOutClusters;
  printVendor(Service, "\tOutClusters: Num = 0x%02X, IDs =", simpleDesc->numOutClusters);
  for (i = 0; i < simpleDesc->numOutClusters; i++)
    {
      COPY_OUT_16_BITS(znp, simpleDesc->clusterList[i + simpleDesc->numInClusters]); znp+=2;
      printVendor(Service, " 0x%04X", simpleDesc->clusterList[i + simpleDesc->numInClusters]);
    }
  printVendor(Service, "\n");

  zabParseZNPSetLength(Status, Msg, AF_REGISTER_DATA_LENGTH(simpleDesc->numInClusters, simpleDesc->numOutClusters));

  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * Delete an endpoint in ZNP
 ******************************************************************************/
#ifdef M_ENABLE_AF_DELETE
void zabZnpAfUtility_DeleteEndpoint(erStatus* Status, zabService* Service, sapMsg *appMsg)
{
  sapMsg* Msg = NULL;
  unsigned8* znp;
  unsigned8* endpoint;

  endpoint = sapMsgGetAppData(appMsg);

  ER_CHECK_STATUS_NULL(Status, endpoint);
  printVendor(Service, "zabZnpoAfUtility: Delete Endpoint 0x%02X\n", *endpoint);

  Msg = zabParseZNPCreateRequest(Status, Service, sizeof(unsigned8));
  ER_CHECK_STATUS_NULL(Status, Msg);

  /* Copy the TID from the application message to the ZNP message */
  sapMsgSetAppTransactionId(Status, Msg, sapMsgGetAppTransactionId(appMsg));

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_AF);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_AF_DELETE);

  znp = (ZAB_MSG_ZNP(Msg)->Data);

  *znp++ = *endpoint;

  zabParseZNPSetLength(Status, Msg, sizeof(unsigned8));

  zabZnpService_SendMsg(Status, Service, Msg);
}
#endif


/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
