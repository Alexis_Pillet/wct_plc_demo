#ifndef _ON_OFF_CLUSTER_H_
#define _ON_OFF_CLUSTER_H_

#include "zabTypes.h"
#include "szl.h"

/**
 * On/Off Notifications
 */
typedef void (*SzlPlugin_OnOffClusterNotification_CB_t) (zabService* Service, szl_uint8 endpoint, szl_bool onOff);

/**
 * On/Off Clusters Initialization
 *
 * This function must be called from the application after the Library has been initialized.
 *
 * The plugin will hereafter be self contained.
 */
SZL_RESULT_t SzlPlugin_OnOffClusterInit(zabService* Service, 
                                        szl_uint8 numEndpoints, 
                                        szl_uint8 * endpointList, 
                                        SzlPlugin_OnOffClusterNotification_CB_t callback);

/**
 * On/Off Cluster Destroy
 */
SZL_RESULT_t SzlPlugin_OnOffCluster_Destroy(zabService* Service);

/**
 * @}
 */

#endif /*_ON_OFF_CLUSTER_H_*/
