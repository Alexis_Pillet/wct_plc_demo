/******************************************************************************
 * @file szl_version.h Versioning info
 ******************************************************************************/

#ifndef SZL_VERSION_H
#define SZL_VERSION_H

#ifdef __cplusplus
extern "C"
{
#endif

// This is where we specify the SZL SW Version.
// The SZL version is supported in the Basic Cluster as manifacture atrtribute 0xE007 and is constructed as follow:
// EH_SZL_VERSION_NAME "." EH_ZB_VERSION_TYPE "." EH_ZB_VERSION_MAJOR "." EH_ZB_VERSIoN_MINOR "." EH_ZB_VERSION_MICRO
// Where EH_SZL_VERSION_TYPE is for the build type:
// E = Engineering (daily build)
// D = Development
// B = Beta (field test)
// R = Release
// The API version is a 24bit integer containing EH_SZL_VERSION_MAJOR EH_SZL_VERSIoN_MINOR EH_SZL_VERSION_MICRO
#define EH_SZL_VERSION_NAME  "SZL"
#define EH_SZL_VERSION_TYPE  "R"
#define EH_SZL_VERSION_MAJOR "03"
#define EH_SZL_VERSION_MINOR "02"
#define EH_SZL_VERSION_MICRO "00"

#define EH_SZL_VERSION_STRING (EH_SZL_VERSION_NAME"."EH_SZL_VERSION_TYPE"."EH_SZL_VERSION_MAJOR"."EH_SZL_VERSION_MINOR"."EH_SZL_VERSION_MICRO)

#ifdef __cplusplus
}
#endif

#endif /* SZL_VERSION_H*/
