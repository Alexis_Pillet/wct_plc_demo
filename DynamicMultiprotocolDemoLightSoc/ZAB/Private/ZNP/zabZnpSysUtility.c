/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the SYS interface for the ZNP
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.02.00  18-Oct-13   MvdB   Upgraded from old file
 * 00.00.02.01  27-Nov-13   MvdB   Add set IEEE
 * 00.00.02.01  03-Dec-13   MvdB   Support transmit power setting
 * 00.00.03.01  19-Mar-14   MvdB   artf53851: Implement compatibility check for run time and firmware image
 * 00.00.06_GP  15-Oct-14   MvdB   ARTF105854: Support Smartlink IPZ hardware for Pro+GP ZNP
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 * 001.101.000  20-Feb-15   MvdB   Improve handling of future ZNP ProductIds
 * 002.000.000  20-Feb-15   MvdB   Update compatible versions
 * 002.001.001  29-Apr-15   MvdB   ARTF132260: Support firmware upgrade of CC2538 network processors
 * 002.002.020  18-Mar-16   MvdB   ARTF165342: Support performing SBL firmware updates of ZAB_NET_PROC_MODEL_CC2538_SBS_SERIAL_GATEWAY
 * 002.002.024  28-Jun-16   MvdB   ARTF172101: Implement ZNP ping to handle lost Reset indication when hidden inside a serial command that was not completed before reset
 * 002.002.052  30-Mar-17   MvdB   Support ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE
 * 002.002.053  12-Apr-17   SMon   Support ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE_PA
 *****************************************************************************/

#include "zabZnpService.h"



/******************************************************************************
 *                      *****************************
 *                 *****           CONFIG            *****
 *                      *****************************
 ******************************************************************************/

/* Compatible transport revision for this build of ZAB */
#define M_COMPATIBLE_TRANSPORT_REV 2
#define M_BOOTLAODER_COMPATIBLE_TRANSPORT_REV 2

/* Minimum compatible Major/Minor versions */
#define M_COMPATIBLE_MAJOR_VERSION 002
#define M_COMPATIBLE_MINOR_VERSION 000

/* Max supported number of items in an NV change notification */
#define M_MAX_NV_CHANGE_NTF_ITEMS 10

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

typedef enum
{
  ZNPI_CMD_SYS_RESET                  = 0x00,
  ZNPI_CMD_SYS_PING                   = 0x01,
  ZNPI_CMD_SYS_VERSION                = 0x02,
  ZNPI_CMD_SYS_SET_EXTADDR            = 0x03,
  ZNPI_CMD_SYS_GET_EXTADDR            = 0x04,
  ZNPI_CMD_SYS_OSAL_NV_READ           = 0x08,
  ZNPI_CMD_SYS_OSAL_NV_WRITE          = 0x09,
  ZNPI_CMD_SYS_RANDOM                 = 0x0C,
  ZNPI_CMD_SYS_OSAL_NV_LENGTH         = 0x13,
  ZNPI_CMD_SYS_SET_TX_POWER           = 0x14,
  ZNPI_CMD_SYS_ADC_READ               = 0x0D,
  ZNPI_CMD_SYS_GPIO                   = 0x0E,
  ZNPI_CMD_SYS_STACK_TUNE             = 0x0F,
  ZNPI_CMD_SYS_TEST_RF                = 0x40,
  ZNPI_CMD_SYS_GET_ANTENNA_MODE       = 0x70,
  ZNPI_CMD_SYS_SET_ANTENNA_MODE       = 0x71,
  ZNPI_CMD_SYS_SET_ZGP_STUB_DISABLED  = 0x72,
  ZNPI_CMD_SYS_RESET_IND              = 0x80,

  /* Schneider Specific Indications */
  ZNPI_CMD_SYS_SCHNEIDER_NV_CHANGE_IND = 0xF0,

} ZNPI_CMD_SYS_COMMANDCODES;

/*
 * Reason in ZNP Message SYS_RESET_IND
 */
typedef enum
{
  Power_up   = 0x00,
  External   = 0x01,
  Watch_dog   = 0x02,
} ZabZnp_SYSReset_reason;

/*
 * Struct of message ZNP SYS_RESET_IND
 */
typedef struct
{
  unsigned8 type;
} zabznp_SysResetReq;

/*
 * Struct of message ZNP SYS_RESET_IND
 */
typedef struct
{
  unsigned8 reason;
  unsigned8 transportRev;
  unsigned8 productId;
  unsigned8 majorRel;
  unsigned8 minorRel;
  unsigned8 maintRel;
} zabznp_SysResetInd;
typedef struct
{
  unsigned8 reason;
  unsigned8 transportRev;
  unsigned8 productId;
  unsigned8 majorRel;
  unsigned8 minorRel;
  unsigned8 maintRel;
  unsigned8 hardwareType;
} zabznp_SysResetInd_EXTENDED;

/*
 * Struct of message ZNP SYS_VERSION
 */
typedef struct
{
  unsigned8 transportRev;
  unsigned8 productId;
  unsigned8 majorRel;
  unsigned8 minorRel;
  unsigned8 maintRel;
} zabznp_SysVersion_rsp;
typedef struct
{
  unsigned8 transportRev;
  unsigned8 productId;
  unsigned8 majorRel;
  unsigned8 minorRel;
  unsigned8 maintRel;
  unsigned8 hardwareType;
} zabznp_SysVersion_rsp_EXTENDED;

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/
static char* getSYSReason(unsigned8 code)
{
  switch (code)
    {
      case Power_up: return "Power_up";
      case External: return "External";
      case Watch_dog: return "Watch_dog";
      default: return "UNKNOWN";
    }
}

static void processHardwareType(erStatus* Status, zabService* Service, znpHardwareType HardwareType)
{
  zabNetworkProcessorModel npModel;
  ZAB_SERVICE_VENDOR(Service)->networkProcessorHardwareType = HardwareType;

  switch (HardwareType)
  {
    case znpHardwareType_CC2530_TI_EB:
      npModel = ZAB_NET_PROC_MODEL_CC2530_TI_EB;
      break;

    case znpHardwareType_CC2531_TI_USB_Dongle:
      npModel = ZAB_NET_PROC_MODEL_CC2531_TI_USB;
      break;

    case znpHardwareType_CC2531_Spectec_USB_Dongle_PA:
      npModel = ZAB_NET_PROC_MODEL_CC2531_SPECTEC_USB_PA;
      break;

    case znpHardwareType_CC2530_OUREA_V2:
      npModel = ZAB_NET_PROC_MODEL_CC2530_SE_OUREA_V2;
      break;

    case znpHardwareType_CC2531_SE_Inventek_USB_Dongle_PA:
      npModel = ZAB_NET_PROC_MODEL_CC2531_SE_INVENTEK_PA;
      break;

    case znpHardwareType_CC2538_TI_EB:
      npModel = ZAB_NET_PROC_MODEL_CC2538_TI_EB;
      break;

    case znpHardwareType_CC2530_Nova:
      npModel = ZAB_NET_PROC_MODEL_CC2530_SE_NOVA;
      break;

    case znpHardwareType_CC2530_SmartlinkIpz:
      npModel = ZAB_NET_PROC_MODEL_CC2530_SE_SMARTLINKIPZ;
      break;

    case znpHardwareType_CC2538_OUREA_UART:
      npModel = ZAB_NET_PROC_MODEL_CC2538_SE_OUREA_UART;
      break;

    case znpHardwareType_CC2538_OUREA_USB:
      npModel = ZAB_NET_PROC_MODEL_CC2538_SE_OUREA_USB;
      break;

    case znpHardwareType_CC2538_SBS_SerialGateway:
      npModel = ZAB_NET_PROC_MODEL_CC2538_SBS_SERIAL_GATEWAY;
      break;

    case znpHardwareType_CC2538_SZCYIT_Dongle:
      npModel = ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE;
      break;

    case znpHardwareType_CC2538_SZCYIT_Dongle_PA:
      npModel = ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE_PA;
      break;
      
    default:
      npModel = ZAB_NET_PROC_MODEL_UNKNOWN;
      break;
  }

  // Pass to Open state machine so it can do it's Give if required
  zabZnpOpen_NetworkProcessorModelHandler(Status, Service, npModel);
}

/******************************************************************************
 * Process SYS_RESER Indications
 ******************************************************************************/
static void zabZnp_ProcessSYSRESETIND(erStatus* Status, zabService* Service, sapMsg* Message)
{
  zabznp_SysResetInd *msgData;
  zabznp_SysResetInd_EXTENDED *msgDataExt;
  unsigned8 msgLen;
  zab_bool compatible;
  unsigned8 hardwareVersion[4];
  znpHardwareType hardwareType = znpHardwareType_Not_Init;

  zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
  ER_CHECK_STATUS(Status);

  if (msgLen >= sizeof(zabznp_SysResetInd_EXTENDED))
    {
      msgDataExt = (zabznp_SysResetInd_EXTENDED*)msgData;
      hardwareType = (znpHardwareType)msgDataExt->hardwareType;
    }

  if (msgLen >= sizeof(zabznp_SysResetInd))
    {
      printVendor(Service, "zabZnpSysUtility: Sys Reset Ind:\n"
                           "\tReason : %s(%d)\n"
                           "\tVersion Info :\n"
                           "\t\tTransportRev = 0x%02X\n"
                           "\t\tProduct = 0x%02X\n"
                           "\t\tVersion = %02X.%02X.%02X\n"
                           "\t\tHwType = 0x%02X\n",
                            getSYSReason(msgData->reason), msgData->reason,
                            msgData->transportRev, msgData->productId,
                            msgData->majorRel, msgData->minorRel, msgData->maintRel, (unsigned8)hardwareType);

      /* Notify app of the hardware model and version */
      processHardwareType(Status, Service, hardwareType);

      hardwareVersion[0] = msgData->majorRel;
      hardwareVersion[1] = msgData->minorRel;
      hardwareVersion[2] = msgData->maintRel;
      hardwareVersion[3] = 0;


      if (msgData->productId == znpProductId_Schneider_Sbl)
        {
          // Give unless we are closing, where it is not necessary
          if (zabZnpOpen_GetOpenState(Service) != ZAB_OPEN_STATE_CLOSING)
            {
              srvCoreGiveBack8(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ACTIVE_APP_TYPE, (unsigned8)ZAB_NET_PROC_APP_BOOTLOADER);
              srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_BOOTLOADER_VERSION, sizeof(hardwareVersion), hardwareVersion);
            }
          /* Determine if the sbl is compatible with this vendor and notify Open state machine*/
          compatible = zabZnpSysUtility_BootloaderCompatibilityCheck(msgData->transportRev,
                                                                     (znpProductId)msgData->productId,
                                                                     msgData->majorRel,
                                                                     msgData->minorRel,
                                                                     msgData->maintRel);
        }
      else if (msgData->productId == znpProductId_Schneider_Pro_Znp)
        {
          // Give unless we are closing, where it is not necessary
          if (zabZnpOpen_GetOpenState(Service) != ZAB_OPEN_STATE_CLOSING)
            {
              srvCoreGiveBack8(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ACTIVE_APP_TYPE, (unsigned8)ZAB_NET_PROC_APP_ZIGBEE_PRO);
              srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_VERSION, sizeof(hardwareVersion), hardwareVersion);
            }
          /* Determine if the network processor is compatible with this vendor and notify Open state machine*/
          compatible = zabZnpSysUtility_CompatibilityCheck(msgData->transportRev,
                                                           (znpProductId)msgData->productId,
                                                           msgData->majorRel,
                                                           msgData->minorRel,
                                                           msgData->maintRel);
        }
      else if (msgData->productId == znpProductId_Schneider_Pro_GP_Znp)
        {
          // Give unless we are closing, where it is not necessary
          if (zabZnpOpen_GetOpenState(Service) != ZAB_OPEN_STATE_CLOSING)
            {
              srvCoreGiveBack8(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ACTIVE_APP_TYPE, (unsigned8)ZAB_NET_PROC_APP_ZIGBEE_PRO_GP);
              srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_VERSION, sizeof(hardwareVersion), hardwareVersion);
            }

          /* Determine if the network processor is compatible with this vendor and notify Open state machine*/
          compatible = zabZnpSysUtility_CompatibilityCheck(msgData->transportRev,
                                                           (znpProductId)msgData->productId,
                                                           msgData->majorRel,
                                                           msgData->minorRel,
                                                           msgData->maintRel);
        }
#ifdef ZAB_VND_CFG_ALLOW_STANDARD_TI_ZNP
      else if (msgData->productId == znpProductId_TI_Znp)
        {
          srvCoreGiveBack8(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ACTIVE_APP_TYPE, (unsigned8)ZAB_NET_PROC_APP_ZIGBEE_PRO);
          srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_VERSION, sizeof(hardwareVersion), hardwareVersion);

          /* It's not compatible, but allow it as we have the dev option enabled. */
          compatible = zab_true;
        }
#endif
      else
        {
          // Give some version info - hopefully it's valid!
          srvCoreGiveBack8(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ACTIVE_APP_TYPE, (unsigned8)ZAB_NET_PROC_APP_UNKNOWN);
          srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_NETWORK_PROCESSOR_ZIGBEE_APP_VERSION, sizeof(hardwareVersion), hardwareVersion);

          // Not supported.
          compatible = zab_false;
        }
      zabZnpOpen_SysResetIndHandler(Status, Service, compatible, (znpProductId)msgData->productId);


      /* Notify network state machine*/
      //zabZnpNwk_resetHandler(Status, Service);
    }
}


/******************************************************************************
 * Process SYS_VERSION response
 * Also returns capabilities bit field
 ******************************************************************************/
static void processSysVersionRsp(erStatus* Status, zabService* Service, sapMsg* Message)
{
  zabznp_SysVersion_rsp* msgData;
  zabznp_SysVersion_rsp_EXTENDED* msgDataExt;
  unsigned8 msgLen;
  znpHardwareType hardwareType = znpHardwareType_Not_Init;

  zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
  ER_CHECK_STATUS(Status);

  if (msgLen >= sizeof(zabznp_SysVersion_rsp_EXTENDED))
    {
      msgDataExt = (zabznp_SysVersion_rsp_EXTENDED*)msgData;
      hardwareType = (znpHardwareType)msgDataExt->hardwareType;
    }

  if (msgLen >= sizeof(zabznp_SysVersion_rsp))
    {
      printVendor(Service, "zabZnpSysUtility: Sys Version Rsp: TransportRev = 0x%02X, ProductId = 0x%02X, Version = %02X.%02X.%02X, Hardware = 0x%02X\n",
              msgData->transportRev,
              msgData->productId,
              msgData->majorRel,
              msgData->minorRel,
                  msgData->maintRel,
                  (unsigned8)hardwareType);

      processHardwareType(Status, Service, hardwareType);

      /* Forward to open state machine */
      zabZnpOpen_SysVersionRspHandler(Status,
                                      Service,
                                      msgData->transportRev,
                                      msgData->productId,
                                      msgData->majorRel,
                                      msgData->minorRel,
                                      msgData->maintRel);
    }
}

/******************************************************************************
 * Process SYS_PING response
 * Also returns capabilities bit field
 ******************************************************************************/
static void processSysPingRsp(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8* msgData;
  unsigned8 msgLen;
  unsigned16 capabilities;

  zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
  ER_CHECK_STATUS(Status);

  if (msgLen >= 2)
    {
      capabilities = COPY_IN_16_BITS(msgData);

      printVendor(Service, "zabZnpSysUtility: Ping Rsp: Capabilities = 0x%04X\n", capabilities);

      // CEDRIC: Make calls to handlers here
    }
}

/******************************************************************************
 * Process SYS_GET_EXTADDR response
 ******************************************************************************/
static void processGetExtAddrRsp(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8* msgData;
  unsigned8 msgLen;
  unsigned64 ieee;

  zabParseZNPGetData(Status, Message, &msgLen, &msgData);
  ER_CHECK_STATUS(Status);

  if (msgLen >= sizeof(unsigned64))
    {
      ieee = COPY_IN_64_BITS(msgData);
      printVendor(Service, "zabZnpSysUtility: Get Ieee Addr Rsp: IEEE = 0x%016llX\n",
                ieee);
    }
}

/******************************************************************************
 * Process SYS_OSAL_NV_LENGTH response
 ******************************************************************************/
static void processNvLengthRsp(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8* msgData;
  unsigned8 msgLen;
  unsigned16 nvLength;

  zabParseZNPGetData(Status, Message, &msgLen, &msgData);
  ER_CHECK_STATUS(Status);

  if (msgLen >= sizeof(unsigned16))
    {
      nvLength = COPY_IN_16_BITS(msgData);
      zabZnpClone_NvLengthResponseHandler(Status, Service, nvLength);
    }
}

/******************************************************************************
 * Process SYS_OSAL_NV_READ response
 ******************************************************************************/
static void processNvReadRsp(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8* msgData;
  unsigned8 msgLen;
  ZNPI_API_ERROR_CODES znpStatus;

  zabParseZNPGetData(Status, Message, &msgLen, &msgData);
  ER_CHECK_STATUS(Status);

  if (msgLen >= sizeof(unsigned8))
    {
      znpStatus = (ZNPI_API_ERROR_CODES)msgData[0];

      printVendor(Service, "zabZnpSysUtility: NV Read Rsp: Sts 0x%02X\n", znpStatus);

      if (znpStatus == ZNPI_API_ERR_SUCCESS)
        {
          zabZnpClone_NvReadResponseHandler(Status, Service, znpStatus, msgData[1], &msgData[2]);
        }
      else
        {
          zabZnpClone_NvReadResponseHandler(Status, Service, znpStatus, 0, NULL);
        }
    }
}

/******************************************************************************
 * Process SYS_OSAL_NV_WRITE response
 ******************************************************************************/
static void processNvWriteRsp(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8* msgData;
  unsigned8 msgLen;
  ZNPI_API_ERROR_CODES znpStatus;

  zabParseZNPGetData(Status, Message, &msgLen, &msgData);
  ER_CHECK_STATUS(Status);

  if (msgLen >= sizeof(unsigned8))
    {
      znpStatus = (ZNPI_API_ERROR_CODES)msgData[0];
      printVendor(Service, "zabZnpSysUtility: NV Write Rsp: Sts 0x%02X\n", znpStatus);
      zabZnpClone_NvWriteResponseHandler(Status, Service, znpStatus);
    }
}

/******************************************************************************
 * Process SYS_SET_TX_POWER response
 ******************************************************************************/
static void processSetTxPowerRsp(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8* msgData;
  unsigned8 msgLen;

  zabParseZNPGetData(Status, Message, &msgLen, &msgData);
  ER_CHECK_STATUS(Status);

  if (msgLen >= sizeof(unsigned8))
    {
      // Give to application
      srvCoreGiveBack8(Status, Service, ZAB_GIVE_NWK_TX_POWER, msgData[0]);
      printVendor(Service, "zabZnpSysUtility: Set Tx power Rsp: dBm %d\n", msgData[0]);
      zabZnpNwk_setTxPowerRspHandler(Status, Service);
    }
}

/******************************************************************************
 * Process SYS_GPIO response
 ******************************************************************************/
static void processGpioRsp(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8* msgData;
  unsigned8 msgLen;

  zabParseZNPGetData(Status, Message, &msgLen, &msgData);
  ER_CHECK_STATUS(Status);

  if (msgLen >= sizeof(unsigned8))
    {
      printApp(Service, "zabZnpSysUtility: GPIO Rsp: 0x%02X\n", msgData[0]);
    }
}

/******************************************************************************
 * Process SYS_ADC_READ response
 ******************************************************************************/
static void processAdcReadRsp(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8* msgData;
  unsigned8 msgLen;
  signed16 adcValue;

  zabParseZNPGetData(Status, Message, &msgLen, &msgData);
  ER_CHECK_STATUS(Status);

  if (msgLen >= sizeof(unsigned16))
    {
      adcValue = COPY_IN_16_BITS(msgData);
      printApp(Service, "zabZnpSysUtility: ADC Value: %d (0x%04X)\n", adcValue, adcValue);
    }
}


/******************************************************************************
 * Process NV Change Indication
 ******************************************************************************/
static void processNvChangeInd(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8* msgData;
  unsigned8 msgLen;
  unsigned8 numIds;
  unsigned16 nvIds[M_MAX_NV_CHANGE_NTF_ITEMS];
  unsigned8 i;

  zabParseZNPGetData(Status, Message, &msgLen, (unsigned8 **)&msgData);
  ER_CHECK_STATUS(Status);

  if (msgLen >= 1)
    {
      /* Grab length and protect against array over run */
      numIds = *msgData++;
      if (numIds > M_MAX_NV_CHANGE_NTF_ITEMS)
        {
          numIds = M_MAX_NV_CHANGE_NTF_ITEMS;
        }

      /* Check we have enough data, then parse out IDs */
      if (msgLen >= (1 + (numIds * sizeof(unsigned16))))
        {
          for (i = 0; i < numIds; i++)
            {
              nvIds[i] = COPY_IN_16_BITS(msgData); msgData += 2;
            }

          zabZnpClone_NvChangeNotificationHandler(Status, Service, numIds, nvIds);
        }
    }
}


/******************************************************************************
 * Process Get/Set Antenna Mode response
 ******************************************************************************/
static void processAntennaModeRsp(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8* msgData;
  unsigned8 msgLen;
  unsigned8 antennaSupportedModes;
  unsigned8 antennaOperatingMode;

  zabParseZNPGetData(Status, Message, &msgLen, &msgData);
  ER_CHECK_STATUS(Status);

  if (msgLen >= 4)
    {
      msgData++; // Status
      msgData++; // Version
      antennaSupportedModes = *msgData++;
      antennaOperatingMode = *msgData++;

      srvCoreGiveBack8(Status, Service, ZAB_GIVE_ANTENNA_SUPPORTED_MODES, (unsigned8)antennaSupportedModes);
      srvCoreGiveBack8(Status, Service, ZAB_GIVE_ANTENNA_CURRENT_MODE, (unsigned8)antennaOperatingMode);

      zabZnpOpen_AntennaRspHandler(Status, Service);
    }
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Check if the network processor is compatible
 * This checks if the transport revision is compatible with this ZAB.
 ******************************************************************************/
zab_bool zabZnpSysUtility_CompatibilityCheck(unsigned8 TransportRev,
                                            znpProductId ProductId,
                                            unsigned8 MajorVersion,
                                            unsigned8 MinorVersion,
                                            unsigned8 ReleaseVersion)
{
  if (ProductId == znpProductId_Schneider_Generic)
    {
      return zab_true;
    }
  if (TransportRev != M_COMPATIBLE_TRANSPORT_REV)
    {
      return zab_false;
    }
  if (MajorVersion < M_COMPATIBLE_MAJOR_VERSION)
    {
      return zab_false;
    }
  if ( (MajorVersion == M_COMPATIBLE_MAJOR_VERSION) &&
       (MinorVersion < M_COMPATIBLE_MINOR_VERSION) )
    {
      return zab_false;
    }
  return zab_true;
}

/******************************************************************************
 * Check if the SBL is compatible with ZAB
 ******************************************************************************/
zab_bool zabZnpSysUtility_BootloaderCompatibilityCheck(unsigned8 TransportRev,
                                            znpProductId ProductId,
                                            unsigned8 MajorVersion,
                                            unsigned8 MinorVersion,
                                            unsigned8 ReleaseVersion)
{
  if (TransportRev == M_BOOTLAODER_COMPATIBLE_TRANSPORT_REV)
    {
      return zab_true;
    }
  return zab_false;
}

/******************************************************************************
 * Check if a new firmware image is compatible with the current network processor.
 * This confirms that the:
 *  - Image will function with ZAB once loaded
 *  - The image is built for compatible hardware
 ******************************************************************************/
zab_bool zabZnpSysUtility_CompatibilityCheckNewFirmwareImage(zabService* Service,
                                                             unsigned8 TransportRev,
                                                             znpProductId ProductId,
                                                             unsigned8 MajorVersion,
                                                             unsigned8 MinorVersion,
                                                             unsigned8 ReleaseVersion,
                                                             znpHardwareType HardwareType)
{
  printInfo(Service, "zabZnpSys: New FW Compatibility Check: ZAB Compatibility=");

  if (zabZnpSysUtility_CompatibilityCheck(TransportRev,
                                          ProductId,
                                          MajorVersion,
                                          MinorVersion,
                                          ReleaseVersion) == zab_true)
    {
      printInfo(Service, "Ok, HW Compatibility=");

      if (ZAB_SERVICE_VENDOR(Service)->networkProcessorHardwareType == HardwareType)
        {
          printInfo(Service, "Ok\n");
          return zab_true;
        }
      else
        {
          printInfo(Service, "FAIL - Hardware=%02X != Image=%02X\n",
                    (unsigned8)ZAB_SERVICE_VENDOR(Service)->networkProcessorHardwareType,
                    (unsigned8)HardwareType);
        }
    }
  else
    {
      printInfo(Service, "FAIL\n");
    }
  return zab_false;
}

/******************************************************************************
 * Request reset of the ZNP (Send SYS_RESET command)
 ******************************************************************************/
void znpi_sys_reset(erStatus* Status, zabService* Service, RESET_TYPE type)
{
    sapMsg *msg = zabParseZNPCreateRequest(Status, Service, sizeof(zabznp_SysResetReq));
    zabznp_SysResetReq *msgData;

    ER_CHECK_STATUS_NULL( Status, msg );

    printVendor(Service, "zabZnpSysUtility: Sys Reset Req - 0x%02X\n", type);
    zabParseZNPSetType(Status, msg, ZNPI_TYPE_AREQ);
    zabParseZNPSetSubSystem(Status, msg, ZNPI_SUBSYSTEM_SYS);
    zabParseZNPSetCommandID(Status, msg, ZNPI_CMD_SYS_RESET);
    zabParseZNPSetLength(Status, msg, sizeof(zabznp_SysResetReq));

    msgData = (zabznp_SysResetReq*)ZAB_MSG_ZNP_DATA(msg);
    msgData->type = type;

    zabZnpService_SendMsg(Status, Service, msg);
}

/******************************************************************************
 * Request Version of the ZNP (Send SYS_VERSION command)
 ******************************************************************************/
void zabZnpSysUtility_VersionRequest(erStatus* Status, zabService* Service)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;

  data_length = 0;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  printVendor(Service, "zabZnpSysUtility: Sys Version Req\n");

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SYS);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SYS_VERSION);
  zabParseZNPSetLength(Status, Msg, data_length);

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Ping the ZNP (Send SYS_PING command)
 * Also returns capabilities bit field
 ******************************************************************************/
void zabZnpSysUtility_PingRequest(erStatus* Status, zabService* Service)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;

  printVendor(Service, "zabZnpSysUtility: Sys Ping Req\n");

  data_length = 0;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SYS);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SYS_PING);
  zabParseZNPSetLength(Status, Msg, data_length);

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Set the IEEE Address of the Network Processor
 * WARNING: Anyone who uses this is responsible for ensuring IEEE addresses remain unique!
 ******************************************************************************/
void zabZnpSysUtility_SetIeeeAddr(erStatus* Status, zabService* Service, unsigned64 IeeeAddr)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;
  unsigned8* msgData;

  data_length = sizeof(unsigned64);
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SYS);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SYS_SET_EXTADDR);

  zabParseZNPSetLength(Status, Msg, data_length);
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_64_BITS(msgData, IeeeAddr); msgData+=sizeof(unsigned64);

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Get the IEEE Address of the Network Processor
 ******************************************************************************/
void zabZnpSysUtility_GetIeeeAddr(erStatus* Status, zabService* Service)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;

  data_length = 0;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SYS);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SYS_GET_EXTADDR);

  zabParseZNPSetLength(Status, Msg, data_length);

  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * Get the length of an NV Item
 ******************************************************************************/
void zabZnpSysUtility_NvLength(erStatus* Status, zabService* Service, unsigned16 NvId)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;
  unsigned8* msgData;

  data_length = sizeof(unsigned16);
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SYS);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SYS_OSAL_NV_LENGTH);

  zabParseZNPSetLength(Status, Msg, data_length);
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(msgData, NvId);

  zabZnpService_SendMsg(Status, Service, Msg);
}

/* Define this to go from 1 byte offset to two byte offset in the ZNP NV Read command
 * If you do this you need a ZNP from Mark vdB! */
// TODO: ARTF49359 - Under discussion with TI.
#define M_ENABLE_TWO_BYTE_READ_OFFSET
/******************************************************************************
 * Read an NV Item
 ******************************************************************************/
void zabZnpSysUtility_NvRead(erStatus* Status, zabService* Service, unsigned16 NvId, unsigned16 Offset)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;
  unsigned8* msgData;

  printVendor(Service, "zabZnpSysUtility: NV Read: ID 0x%04X, Offset 0x%04X\n", NvId, Offset);

#ifdef M_ENABLE_TWO_BYTE_READ_OFFSET
  data_length = 4;
#else
  data_length = 3;
#endif
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SYS);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SYS_OSAL_NV_READ);

  zabParseZNPSetLength(Status, Msg, data_length);
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(msgData, NvId); msgData += sizeof(unsigned16);


#ifdef M_ENABLE_TWO_BYTE_READ_OFFSET
  COPY_OUT_16_BITS(msgData, Offset); msgData += sizeof(unsigned16);
#else
  *msgData++ = (unsigned8)Offset;
#endif

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Write an NV Item
 ******************************************************************************/
void zabZnpSysUtility_NvWrite(erStatus* Status,
                              zabService* Service,
                              unsigned16 NvId,
                              unsigned16 Offset,
                              unsigned8 Length,
                              unsigned8* DataPointer)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;
  unsigned8* msgData;

  ER_CHECK_NULL(Status, DataPointer);

  if (Length > 246)
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_PARSE);
    }

  printVendor(Service, "zabZnpSysUtility: NV Write: ID 0x%04X, Offset 0x%04X, Length 0x%04X\n", NvId, Offset, Length);
  data_length = 4 + Length;

  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SYS);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SYS_OSAL_NV_WRITE);

  zabParseZNPSetLength(Status, Msg, data_length);
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  COPY_OUT_16_BITS(msgData, NvId); msgData += sizeof(unsigned16);
  *msgData++ = (unsigned8)Offset;
  *msgData++ = (unsigned8)Length;
  osMemCopy(Status, msgData, DataPointer, Length);

  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * Set the TX power of a ZNP
 ******************************************************************************/
void zabZnpSysUtility_SetTxPower(erStatus* Status, zabService* Service, signed8 TxPowerDbm)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;
  unsigned8* msgData;

  printVendor(Service, "zabZnpSysUtility: Set Tx Power: %d dBm\n", TxPowerDbm);

  data_length = sizeof(unsigned8);
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SYS);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SYS_SET_TX_POWER);

  zabParseZNPSetLength(Status, Msg, data_length);
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  *msgData++ = (unsigned8)TxPowerDbm;

  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * Get/Set GPIO in the ZNP
 ******************************************************************************/
void zabZnpSysUtility_GpioControl(erStatus* Status, zabService* Service, znpGpioOperation gpioOperation, znpGpioItemMask gpioItemMask)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;
  unsigned8* msgData;

  printApp(Service, "zabZnpSysUtility: GPIO Control: Op=0x%02X, Mask=0x%02X\n", (unsigned8)gpioOperation, (unsigned8)gpioItemMask);

  data_length = 2;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SYS);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SYS_GPIO);

  zabParseZNPSetLength(Status, Msg, data_length);
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  *msgData++ = (unsigned8)gpioOperation;
  *msgData++ = (unsigned8)gpioItemMask;

  zabZnpService_SendMsg(Status, Service, Msg);
}


/******************************************************************************
 * ADC Read Request
 ******************************************************************************/
void zabZnpSysUtility_AdcRead(erStatus* Status, zabService* Service, znpAdcChannel adcChannel, znpAdcResolution adcResolution)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;
  unsigned8* msgData;

  data_length = 2;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SYS);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SYS_ADC_READ);

  zabParseZNPSetLength(Status, Msg, data_length);
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  *msgData++ = (unsigned8)adcChannel;
  *msgData++ = (unsigned8)adcResolution;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Get Antenna Mode Request
 ******************************************************************************/
void zabZnpSysUtility_GetAntennaMode(erStatus* Status, zabService* Service)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;

  data_length = 0;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SYS);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SYS_GET_ANTENNA_MODE);

  zabParseZNPSetLength(Status, Msg, data_length);

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Set Antenna Mode Request
 ******************************************************************************/
void zabZnpSysUtility_SetAntennaMode(erStatus* Status, zabService* Service, zabAntennaOperatingMode AntennaOperatingMode)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;
  unsigned8* msgData;

  data_length = 1;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SYS);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SYS_SET_ANTENNA_MODE);

  zabParseZNPSetLength(Status, Msg, data_length);
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  *msgData++ = (unsigned8)AntennaOperatingMode;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Set ZGP Stub Disabled - TEST FUNCTION ONLY!!!
 ******************************************************************************/
void zabZnpSysUtility_SetZgpStubDisabled(erStatus* Status, zabService* Service, zab_bool stubDisabled)
{
  sapMsg* Msg = NULL;
  unsigned8 data_length;
  unsigned8* msgData;

  data_length = 1;
  Msg = zabParseZNPCreateRequest(Status, Service, data_length);
  ER_CHECK_STATUS_NULL(Status, Msg);

  zabParseZNPSetType(Status, Msg, ZNPI_TYPE_SREQ);
  zabParseZNPSetSubSystem(Status, Msg, ZNPI_SUBSYSTEM_SYS);
  zabParseZNPSetCommandID(Status, Msg, ZNPI_CMD_SYS_SET_ZGP_STUB_DISABLED);

  zabParseZNPSetLength(Status, Msg, data_length);
  msgData = ZAB_MSG_ZNP_DATA(Msg);
  *msgData++ = (unsigned8)stubDisabled;

  zabZnpService_SendMsg(Status, Service, Msg);
}

/******************************************************************************
 * Process All Message of subsystem SYS
 ******************************************************************************/
void zabZnpSysProcessMsg(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8 cmd;

  zabParseZNPGetCommandID(Status, Message, &cmd);

  switch (cmd)
    {
      case ZNPI_CMD_SYS_RESET_IND:
        zabZnp_ProcessSYSRESETIND(Status, Service, Message);
        break;

      case ZNPI_CMD_SYS_PING:
        processSysPingRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_SYS_VERSION:
        processSysVersionRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_SYS_GET_EXTADDR:
        processGetExtAddrRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_SYS_OSAL_NV_LENGTH:
        processNvLengthRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_SYS_OSAL_NV_READ:
        processNvReadRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_SYS_OSAL_NV_WRITE:
        processNvWriteRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_SYS_SET_TX_POWER:
        processSetTxPowerRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_SYS_GPIO:
        processGpioRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_SYS_ADC_READ:
        processAdcReadRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_SYS_SCHNEIDER_NV_CHANGE_IND:
        processNvChangeInd(Status, Service, Message);
        break;

      case ZNPI_CMD_SYS_GET_ANTENNA_MODE:
      case ZNPI_CMD_SYS_SET_ANTENNA_MODE:
        processAntennaModeRsp(Status, Service, Message);
        break;

      case ZNPI_CMD_SYS_SET_ZGP_STUB_DISABLED:
        break;

      default:
        printError(Service, "zabZnpSysUtility: Warning unknown  ZNP message 0x%02X\n", cmd);
        break;
    }
}
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
