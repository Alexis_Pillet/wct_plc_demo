/*
 Name:    Private ZNP Shared Definitions
 Author:  ZigBee Excellence Center
 Company: Schneider Electric

 Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

 Description:
    Private for implementation that we do not want to expose outside the module.
 *
 * 002.002.021  20-Apr-16   MvdB   ARTF167736: Add retry count and message pointer for serial retries
 *                                             Expand function like macros for managing SRSP tracking
 * 000.000.012  19-Jul-16   MvdB   Add info params for zabNcpRft
 * 000.000.015  25-Jul-16   MvdB   Support multiple endpoints
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
*/

#ifndef __ZAB_EZSP_PRIVATE_H__
#define __ZAB_EZSP_PRIVATE_H__

#include "zabCorePrivate.h"
#include "zabNcpOpenTypes.h"
#include "zabNcpNwkTypes.h"
#include "zabNcpSblTypes.h"
#include "ezsp-protocol.h"
#include "ash-protocol.h"
#include "zabNcpRft.h"

#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/


/* Max possible value of a (32-bit) Milli-Second timer.
 * This is used when handling wrapped ms time */
#define ZAB_VND_MS_TIMEOUT_MAX  (0x80000000)

/* Default Transaction ID for messages generated in vendor layer */
#define ZAB_VND_DEFAULT_TID     ( 0 )

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/* Buffer for receiving serial data from glue */
typedef struct
{
  unsigned8 Length;
  zab_bool EscapeReceived;
  zab_bool BootloaderMode;
  unsigned8 Data[255];
} SerialReceiveBuffer_t;

/* SBL Prompt States */
typedef enum
{
  zabNcpXModem_PromptState_None,
  zabNcpXModem_PromptState_B,
  zabNcpXModem_PromptState_L,
  zabNcpXModem_PromptState_BLSpace,
  zabNcpXModem_PromptState_Arrow,

  zabNcpXModem_PromptState_E,
  zabNcpXModem_PromptState_EF,
  zabNcpXModem_PromptState_EFR,
  zabNcpXModem_PromptState_EFRSpace,
  zabNcpXModem_PromptState_EFR3,
  zabNcpXModem_PromptState_EFR32,

  zabNcpXModem_PromptState_VersionStart,
  zabNcpXModem_PromptState_VersionMajor,
  zabNcpXModem_PromptState_VersionMajorDot,
  zabNcpXModem_PromptState_VersionMinor,
  zabNcpXModem_PromptState_VersionMinorDot,
  zabNcpXModem_PromptState_VersionInternal,
  zabNcpXModem_PromptState_VersionInternalDot,
  zabNcpXModem_PromptState_VersionLast,
} zabNcpXModem_PromptState_t;

/* XModem service parameters */
typedef struct
{
  zabNcpXModem_PromptState_t promptState;
  unsigned8 blockNumber;
  unsigned8 versionMajor;
  unsigned8 versionMinor;
  unsigned8 versionInternal;
  unsigned8 versionTest;
} zabNcpXModem_t;

/* ASH service parameters */
typedef struct
{
  unsigned8 ackRx;                         // frame ack'ed from remote peer
  unsigned8 ackTx;                         // frame ack'ed to remote peer
  unsigned8 frmTx;                         // next frame to be transmitted
  unsigned8 frmRx;                         // next frame expected to be rec'd

  // Last transmitted frame and data. Used if a retry is necessary.
  // This is a little RAM hungry, so we could move to malloc/freeing it if necessary
  unsigned8 ashTxDataPayloadLength;
  unsigned8 ashTxDataPayload[ASH_MAX_DATA_FIELD_LEN];
} zabNcpAshInfo_t;


/* EZSP service parameters */
typedef struct
{
  zab_bool linkLocked;
  unsigned8 sequenceNumber;
  unsigned8 frameId;
  unsigned32 timeoutTimeMs;
} zabNcpEzspInfo_t;

/* Silabs NCP Vendor Service state information */
typedef struct
{
  unsigned32 lastOneSecondTickMs;

  zabOpenInfo_t zabOpenInfo;      // Vendor Open state information
  zabNwkInfo_t zabNwkInfo;        // Vendor network state information
  zabSblInfo_t zabSblInfo;        // Vendor bootloader information
//  zabCloneInfo_t zabCloneInfo;    // Vendor cloning information

  zabNcpAshInfo_t zabNcpAshInfo;
  zabNcpEzspInfo_t zabNcpEzspInfo;
  zabNcpXModem_t zabNcpXModem;
  zabNcpRftInfo_t zabNcpRftInfo;
  zabMsgProSimpleDescriptor* simpleDesc[ZAB_VND_CFG_MAX_ENDPOINTS];

  SerialReceiveBuffer_t SerialReceiveBuffer;
  zabSerialService_ActionOutCallback_t ActionOutCallback;
  zabSerialService_SerialOutCallback_t SerialOutCallback;
} zabVendorServiceStruct;


/******************************************************************************
 *                      *****************************
 *                 *****            MACROS           *****
 *                      *****************************
 ******************************************************************************/

// Get vendor Service
#define ZAB_SERVICE_VENDOR( sv ) ((zabVendorServiceStruct*)(ZAB_SRV( (sv) ) ->Vendor))


#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/