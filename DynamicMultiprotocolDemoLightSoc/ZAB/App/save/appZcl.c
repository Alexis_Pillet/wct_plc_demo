/******************************************************************************
 *                          ZAB TEST APPLICATION
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the ZCL handlers for the test app.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.03.00  16-Jan-14   MvdB   Use common header block
 * 00.00.06.00  18-Aug-14   MvdB   Add support for callback based metering cluster plugin
 * 00.00.06.04  07-Oct-14   MvdB   artf104116: Add RSSI to received packet quality indication
 * 00.00.06.06  30-Oct-14   MvdB   artf74202: Support callbacks in Nova plugins
 * 00.00.08.01  27-Nov-14   MvdB   artf108759: Temporary work around for reportable strings - limit to 8 bytes
 * 01.100.06.00 10-Feb-15   MvdB   Handle long octet string attributes
 *                                 Print Command ID in cluster command response handler
 * 01.100.07.00 13-Feb-15   MvdB   Split out clusters into individual files as this was getting too big
 * 002.000.004  01-Apr-15   MvdB   ConsoleTest - Print manufacturer ID in responses as requested by Olivier
 * 002.001.000  20-Apr-15   MvdB   ARTF131071: Add appZcl_ClusterCmdHandler for testing reception of Nova Online/Offline ntf
 * 002.001.001  15-Jul-15   MvdB   ARTF130228: Add unique address mode for GpSrcId
 *                                 ARTF136585: Add transaction IDs to over the air commands/responses
 *                                 ConsoleTest: Add appZcl_GetZclStatusString() and appZcl_GetSzlAddressString()
 * 002.001.003  16-Jul-15   MvdB   ARTF132827: Include source address in cluster command handlers
 * 002.002.015  21-Oct-15   MvdB   Print better formatted attribute data
 * 002.002.020  18-Mar-16   MvdB   Extern appZcl_PrintAppAttribute for use form other files
 * 002.002.021  21-Apr-16   MvdB   ARTF167807: Support Multi-Cluster Attribute Read/Write for GP
 *                                 ARTF167808: Improve Attribute Read/Write/Configure Responses to always list all attributes requested
 * 002.002.028  12-Dec-16   MvdB   ConsoleTest: Add runtime configurable options for report and RSSI printing
 * 002.002.029  06-Jan-17   MvdB   ConsoleTest: Support logging reports to reports.csv
 * 002.002.035  11-Jan-17   MvdB   ARTF170823: Make Read/Write Reporting configuration data (SZL_AttributeReportData_t) consistent with Read/Write data (SZL_AttributeData_t)
 * 002.002.037  11-Jan-17   MvdB   Tidy up printinig of SZL_ZB_DATATYPE_UINT32 and SZL_ZB_DATATYPE_LONG_OCTET_STR
 * 002.002.049  14-Feb-17   MvdB   Ensure *PayloadOutSize is set correctly by cluster command handlers
 *                                 Add appZcl_GetClusterString()
 * 002.002.051  16-Mar-17   MvdB   Remove unsued SZL_ADDRESS_MODE_COMBINED
 * 002.002.054  20-Jul-17   SMon   ARTF214292 Manage timeout for default response
 *****************************************************************************/

/* Feature test macros required for gettimeofday */
#define _BSD_SOURCE           // For glibc <= 2.2.19
//#define _DEFAULT_SOURCE     // From glibc 2.2.0. Use if you get a warning for _BSD_SOURCE.

#include "appConfig.h"
#include "appZcl.h"
#include "appGp.h"
//#include "appSerialUtility.h"
#include "appMain.h"

#include "zabCoreService.h"

#include "ecb_cluster.h"
#include "elec_meas_cluster.h"
#include "identify_cluster.h"
#include "on_off_cluster.h"
#include "metering_cluster.h"

//#include "platUtility.h"

#include <assert.h>
#include <string.h>
//#include <time.h>
//#include <sys/time.h>

//Struct for managing timeout for szl cmdreq
typedef struct
{
    szl_uint32 Tid;   
    szl_uint8 Command;
    szl_uint8 Timeout;                                 
    szl_uint8  NbRetry;
} TimeoutInfo_t;

#define M_MAX_TIMEOUT 250

static TimeoutInfo_t TimeoutInfo[M_MAX_TIMEOUT];
/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/

/* Max length of an attribute data string */
#define ATTRIBUTE_DATA_MAX_STRING_LENGTH 100

/* File name where reports will be logged (if enabled) */
#define REPORT_CSV_FILENAME "reports.csv"

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * SZL Attribute Changed Notification Handler
 ******************************************************************************/
void appZcl_AttributeChangedNotificationHandler(zabService* Service, struct _SZL_AttributeChangedNtfParams_t* Params)
{
  printApp(Service, "AppZCL: Attribute Changed Notification: Ep 0x%02X, Cl 0x%04X (%s), Attr 0x%04X\n",
         Params->Endpoint,
         Params->ClusterID, appZcl_GetClusterString(Params->ClusterID),
         Params->AttributeID);
}


/******************************************************************************
 * Get a string description of the zabAddressMode
 ******************************************************************************/
static char* getAddrModeString(zabAddressMode addressMode)
{
  switch(addressMode)
    {
      case ZAB_ADDRESS_MODE_GROUP: return "Group";
      case ZAB_ADDRESS_MODE_BROADCAST: return "Broadcast";
      case ZAB_ADDRESS_MODE_NWK_ADDRESS: return "Unicast";
      case ZAB_ADDRESS_MODE_VIA_BIND: return "Via Bind";
      case ZAB_ADDRESS_MODE_IEEE_ADDRESS: return "IEEE Address";
      default: return "Unknown";
    }
}

/******************************************************************************
 * Print attribute data into a string
 ******************************************************************************/
void appZcl_AttributeToString(zabService* Service, SZL_ZIGBEE_DATA_TYPE_t DataType, void* Data, szl_uint16 DataLength, char* AttributeString, szl_uint8 AttributeStringMaxLength)
{
  switch (DataType)
    {
      case SZL_ZB_DATATYPE_BOOLEAN:
      case SZL_ZB_DATATYPE_BITMAP8:
      case SZL_ZB_DATATYPE_DATA8:
      case SZL_ZB_DATATYPE_UINT8:
      case SZL_ZB_DATATYPE_ENUM8:
          snprintf(AttributeString, AttributeStringMaxLength, "0x%02X (%d)", *((szl_uint8*)Data), *((szl_uint8*)Data));
          break;

      case SZL_ZB_DATATYPE_INT8:
          snprintf(AttributeString, AttributeStringMaxLength, "0x%02X (%d)", *((szl_int8*)Data), *((szl_int8*)Data));
          break;

      case SZL_ZB_DATATYPE_BITMAP16:
      case SZL_ZB_DATATYPE_DATA16:
      case SZL_ZB_DATATYPE_UINT16:
      case SZL_ZB_DATATYPE_ENUM16:
      case SZL_ZB_DATATYPE_CLUSTER_ID:
      case SZL_ZB_DATATYPE_ATTR_ID:
          snprintf(AttributeString, AttributeStringMaxLength, "0x%04X (%d)", *((szl_uint16*)Data), *((szl_uint16*)Data));
          break;

      case SZL_ZB_DATATYPE_INT16:
          snprintf(AttributeString, AttributeStringMaxLength, "0x%04X (%d)", *((szl_uint16*)Data), *((szl_int16*)Data));
          break;

      case SZL_ZB_DATATYPE_BITMAP24:
      case SZL_ZB_DATATYPE_DATA24:
      case SZL_ZB_DATATYPE_UINT24:
      case SZL_ZB_DATATYPE_BITMAP32:
      case SZL_ZB_DATATYPE_DATA32:
      case SZL_ZB_DATATYPE_UINT32:
      case SZL_ZB_DATATYPE_TOD:
      case SZL_ZB_DATATYPE_DATE:
      case SZL_ZB_DATATYPE_BAC_OID:
          snprintf(AttributeString, AttributeStringMaxLength, "0x%08X (%d)", *((szl_uint32*)Data), *((szl_uint32*)Data));
          break;

      case SZL_ZB_DATATYPE_INT24:
      case SZL_ZB_DATATYPE_INT32:
          snprintf(AttributeString, AttributeStringMaxLength, "0x%08X (%d)", *((szl_uint32*)Data), *((szl_int32*)Data));
          break;

      case SZL_ZB_DATATYPE_UTC:
        {
          /* Seconds between 1 Jan 1970 and 1 Jan 2000
           *   +7 accounts for the leap years */
          /*
          #define M_SECONDS_BETWEEN_1970_AND_2000 ((365*30+7)*24*60*60)
          time_t rawtime;
          struct tm * local;
          struct tm * gm;
          int stringLength;
          rawtime = *((szl_uint32*)Data) + M_SECONDS_BETWEEN_1970_AND_2000;

          stringLength = snprintf(AttributeString, AttributeStringMaxLength, "0x%08X", *((szl_uint32*)Data));

          local = localtime ( &rawtime );
          if ( (local != NULL) && (stringLength < AttributeStringMaxLength) )
            {
              stringLength += snprintf(AttributeString+stringLength, AttributeStringMaxLength-stringLength,
                                       " (Local: %d-%d-%d %02d:%02d:%02d)",
                                       1900 + local->tm_year, local->tm_mon, local->tm_mday,
                                       local->tm_hour, local->tm_min, local->tm_sec);

            }

          gm = gmtime ( &rawtime );
          if ((gm != NULL) && (stringLength < AttributeStringMaxLength) )
            {
              stringLength += snprintf(AttributeString+stringLength, AttributeStringMaxLength-stringLength,
                                       " (GMT: %d-%d-%d %02d:%02d:%02d)",
                                       1900 + gm->tm_year, gm->tm_mon, gm->tm_mday,
                                       gm->tm_hour, gm->tm_min, gm->tm_sec);
            }
*/
        }
        break;

      case SZL_ZB_DATATYPE_SINGLE_PREC:
        {
          float singlePrec = *((float*)Data);
          snprintf(AttributeString, AttributeStringMaxLength, "%f (%e) (0x%08x)", singlePrec, singlePrec, *((szl_int32*)Data));
        }
        break;

      case SZL_ZB_DATATYPE_BITMAP40:
      case SZL_ZB_DATATYPE_DATA40:
      case SZL_ZB_DATATYPE_UINT40:
      case SZL_ZB_DATATYPE_BITMAP48:
      case SZL_ZB_DATATYPE_DATA48:
      case SZL_ZB_DATATYPE_UINT48:
      case SZL_ZB_DATATYPE_BITMAP56:
      case SZL_ZB_DATATYPE_DATA56:
      case SZL_ZB_DATATYPE_UINT56:
      case SZL_ZB_DATATYPE_BITMAP64:
      case SZL_ZB_DATATYPE_DATA64:
      case SZL_ZB_DATATYPE_UINT64:
          snprintf(AttributeString, AttributeStringMaxLength, "0x%016llX (%lld)", *((szl_uint64*)Data), *((szl_uint64*)Data));
          break;

      case SZL_ZB_DATATYPE_INT40:
      case SZL_ZB_DATATYPE_INT48:
      case SZL_ZB_DATATYPE_INT56:
      case SZL_ZB_DATATYPE_INT64:
      case SZL_ZB_DATATYPE_IEEE_ADDR:
          snprintf(AttributeString, AttributeStringMaxLength, "0x%016llX (%lld)", *((szl_uint64*)Data), *((szl_int64*)Data));
          break;

      case SZL_ZB_DATATYPE_DOUBLE_PREC:
        {
          double doublePrec = *((double*)Data);
          snprintf(AttributeString, AttributeStringMaxLength, "%f (%e) (0x%016llX)", doublePrec, doublePrec, *((szl_int64*)Data));
        }
        break;

      case SZL_ZB_DATATYPE_CHAR_STR:
      case SZL_ZB_DATATYPE_OCTET_STR:
          snprintf(AttributeString, AttributeStringMaxLength, "%s", (char*)Data);
          break;

      case SZL_ZB_DATATYPE_LONG_OCTET_STR:
      case SZL_ZB_DATATYPE_128_BIT_SEC_KEY:
        {
          szl_uint16 i;
          int stringLength = 0;
          for (i = 0; i < DataLength; i++)
            {
              if (stringLength < AttributeStringMaxLength)
                {
                  szl_uint8 longData = ((szl_uint8*)Data)[i];
                  stringLength += snprintf(AttributeString+stringLength, AttributeStringMaxLength-stringLength,  "%02X ", longData);
                }
            }
        }
        break;

      default:
          snprintf(AttributeString, AttributeStringMaxLength, "WARNING: Unknown data type 0x%02X", DataType);
    }
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/
/******************************************************************************
 * Store a new Timeout for a szl cmdreq
 ******************************************************************************/
void appZcl_SetTimeout(zabService* Service,szl_uint32 tid,szl_uint8 command, szl_uint8 timeout,szl_uint8  nbRetry)
{
    szl_uint8 i;
    for(i=0;i<M_MAX_TIMEOUT;i++)
    {
    if(TimeoutInfo[i].Tid==0)
        break;
    }
     
     TimeoutInfo[i].Tid=tid;
     TimeoutInfo[i].Command=command;
     TimeoutInfo[i].Timeout=timeout;
     TimeoutInfo[i].NbRetry=nbRetry;     
}

/******************************************************************************
 * Print out attribute data
 ******************************************************************************/
void appZcl_PrintAppAttribute(zabService* Service, SZL_ZIGBEE_DATA_TYPE_t DataType, void* Data, szl_uint16 DataLength)
{
  char attributeDataString[ATTRIBUTE_DATA_MAX_STRING_LENGTH];
  attributeDataString[0] = 0;

  appZcl_AttributeToString(Service, DataType, Data, DataLength, attributeDataString, ATTRIBUTE_DATA_MAX_STRING_LENGTH);
  printApp(Service, "%s", attributeDataString);
}

/**************************************************************************
 * Print an SZL function result with TID
 **************************************************************************/
void appZcl_PrintSzlResult(zabService* Service, char* Description, SZL_RESULT_t Result, szl_uint8 Tid)
{
  if (Result == SZL_RESULT_SUCCESS)
    {
      printApp(Service, "CMD: %s success: TID = 0x%02X\n", Description, Tid);
    }
  else
    {
      printError(Service, "CMD: %s failed with SZL_RESULT_t = %s (0x%02X)\n", Description, appZcl_GetSzlResultString(Result), (szl_uint8)Result);
    }
}

/******************************************************************************
 * Get a string for an SZL Result
 ******************************************************************************/
char* appZcl_GetSzlResultString(SZL_RESULT_t result)
{
  switch(result)
    {
      case SZL_RESULT_SUCCESS: return "SZL_RESULT_SUCCESS";
      case SZL_RESULT_FAILED: return "SZL_RESULT_FAILED";
      case SZL_RESULT_LIBRARY_NOT_INITIALIZED: return "SZL_RESULT_LIBRARY_NOT_INITIALIZED";
      case SZL_RESULT_UART_NOT_INITIALIZED: return "SZL_RESULT_UART_NOT_INITIALIZED";
      case SZL_RESULT_DATA_MISSING: return "SZL_RESULT_DATA_MISSING";
      case SZL_RESULT_NO_CALLBACK_REGISTERED: return "SZL_RESULT_NO_CALLBACK_REGISTERED";
      case SZL_RESULT_INVALID_DATA: return "SZL_RESULT_INVALID_DATA";
      case SZL_RESULT_INVALID_APPLICATION_TYPE: return "SZL_RESULT_INVALID_APPLICATION_TYPE";
      case SZL_RESULT_INVALID_ADDRESS_MODE: return "SZL_RESULT_INVALID_ADDRESS_MODE";
      case SZL_RESULT_INVALID_NV_GROUP: return "SZL_RESULT_INVALID_NV_GROUP";
      case SZL_RESULT_INVALID_NV_GROUP_ID: return "SZL_RESULT_INVALID_NV_GROUP_ID";
      case SZL_RESULT_API_INVALID_VERSION: return "SZL_RESULT_API_INVALID_VERSION";
      case SZL_RESULT_MANDATORY_CB_MISSING: return "SZL_RESULT_MANDATORY_CB_MISSING";
      case SZL_RESULT_FEATURE_INVALID_FOR_TYPE: return "SZL_RESULT_FEATURE_INVALID_FOR_TYPE";
      case SZL_RESULT_NO_FREE_TID: return "SZL_RESULT_NO_FREE_TID";
      case SZL_RESULT_CB_QUEUE_FULL: return "SZL_RESULT_CB_QUEUE_FULL";
      case SZL_RESULT_QUEUE_FULL: return "SZL_RESULT_QUEUE_FULL";
      case SZL_RESULT_TABLE_FULL: return "SZL_RESULT_TABLE_FULL";
      case SZL_RESULT_OUT_OF_RANGE: return "SZL_RESULT_OUT_OF_RANGE";
      case SZL_RESULT_DATA_TOO_BIG: return "SZL_RESULT_DATA_TOO_BIG";
      case SZL_RESULT_OPERATION_NOT_POSSIBLE: return "SZL_RESULT_OPERATION_NOT_POSSIBLE";
      case SZL_RESULT_NOT_FOUND: return "SZL_RESULT_NOT_FOUND";
      case SZL_RESULT_ALREADY_EXISTS: return "SZL_RESULT_ALREADY_EXISTS";
      case SZL_RESULT_INTERNAL_LIB_ERROR: return "SZL_RESULT_INTERNAL_LIB_ERROR";
      case SZL_RESULT_DP_NOT_FOUND: return "SZL_RESULT_DP_NOT_FOUND";
      case SZL_RESULT_DP_NOT_LOADED: return "SZL_RESULT_DP_NOT_LOADED";
      default: return "Unknown";
    }
}

/******************************************************************************
 * Get a string for a ZCL_STATUS
 ******************************************************************************/
char* appZcl_GetZclStatusString(ZCL_STATUS ZclStatus)
{
  switch(ZclStatus)
    {
      case ZCL_SUCCESS: return "ZCL_SUCCESS";
      case ZCL_FAILURE: return "ZCL_FAILURE";
      case ZCL_NOT_AUTHORIZED: return "ZCL_NOT_AUTHORIZED";
      case ZCL_MALFORMED_COMMAND: return "ZCL_MALFORMED_COMMAND";
      case ZCL_UNSUP_CLUSTER_COMMAND: return "ZCL_UNSUP_CLUSTER_COMMAND";
      case ZCL_UNSUP_GENERAL_COMMAND: return "ZCL_UNSUP_GENERAL_COMMAND";
      case ZCL_UNSUP_MANUF_CLUSTER_COMMAND: return "ZCL_UNSUP_MANUF_CLUSTER_COMMAND";
      case ZCL_UNSUP_MANUF_GENERAL_COMMAND: return "ZCL_UNSUP_MANUF_GENERAL_COMMAND";
      case ZCL_INVALID_FIELD: return "ZCL_INVALID_FIELD";
      case ZCL_UNSUPPORTED_ATTRIBUTE: return "ZCL_UNSUPPORTED_ATTRIBUTE";
      case ZCL_INVALID_VALUE: return "ZCL_INVALID_VALUE";
      case ZCL_READ_ONLY: return "ZCL_READ_ONLY";
      case ZCL_INSUFFICIENT_SPACE: return "ZCL_INSUFFICIENT_SPACE";
      case ZCL_DUPLICATE_EXISTS: return "ZCL_DUPLICATE_EXISTS";
      case ZCL_NOT_FOUND: return "ZCL_NOT_FOUND";
      case ZCL_UNREPORTABLE_ATTRIBUTE: return "ZCL_UNREPORTABLE_ATTRIBUTE";
      case ZCL_INVALID_DATA_TYPE: return "ZCL_INVALID_DATA_TYPE";
      case ZCL_WRITE_ONLY: return "ZCL_WRITE_ONLY";
      case ZCL_ACTION_DENIED: return "ZCL_ACTION_DENIED";
      case ZCL_TIMEOUT: return "ZCL_TIMEOUT";
      case ZCL_HARDWARE_FAILURE: return "ZCL_HARDWARE_FAILURE";
      case ZCL_SOFTWARE_FAILURE: return "ZCL_SOFTWARE_FAILURE";
      default: return "Unknown";
    }
}

/******************************************************************************
 * Get a string for a SZL_STATUS_t
 ******************************************************************************/
char* appZcl_GetSzlStatusString(SZL_STATUS_t SzlStatus)
{
  switch(SzlStatus)
    {
      case SZL_STATUS_SUCCESS                    : return "SUCCESS";
      case SZL_STATUS_FAILED                     : return "FAILED";
      case SZL_STATUS_UNSUPPORTED_OPERATION      : return "UNSUPPORTED_OPERATION";
      case SZL_STATUS_NO_RESPONSE                : return "NO_RESPONSE";
      case SZL_STATUS_MULTI_EH_NETWORK_FAILURE   : return "MULTI_EH_NETWORK_FAILURE";
      case SZL_STATUS_MEM_ERROR                  : return "MEM_ERROR";
      case SZL_STATUS_BUFFER_FULL                : return "BUFFER_FULL";
      case SZL_STATUS_UNSUPPORTED_MODE           : return "UNSUPPORTED_MODE";
      case SZL_STATUS_BRICK_RESETTING            : return "BRICK_RESETTING";
      case SZL_STATUS_IN_PROGRESS                : return "IN_PROGRESS";
      case SZL_STATUS_TIMEOUT                    : return "TIMEOUT";
      case SZL_STATUS_INIT                       : return "INIT";
      case SZL_STATUS_ZGP_TXQUEUE_OVERWRITEN     : return "ZGP_TXQUEUE_OVERWRITEN";
      case SZL_STATUS_TABLE_FULL                 : return "TABLE_FULL";
      case SZL_STATUS_NOT_AUTHORIZED             : return "NOT_AUTHORIZED";
      case SZL_STATUS_MALFORMED_COMMAND          : return "MALFORMED_COMMAND";
      case SZL_STATUS_UNSUP_CLUSTER_COMMAND      : return "UNSUP_CLUSTER_COMMAND";
      case SZL_STATUS_UNSUP_GENERAL_COMMAND      : return "UNSUP_GENERAL_COMMAND";
      case SZL_STATUS_UNSUP_MANU_CLUSTER_COMMAND : return "UNSUP_MANU_CLUSTER_COMMAND";
      case SZL_STATUS_UNSUP_MANU_GENERAL_COMMAND : return "UNSUP_MANU_GENERAL_COMMAND";
      case SZL_STATUS_INVALID_FIELD              : return "INVALID_FIELD";
      case SZL_STATUS_UNSUPPORTED_ATTRIBUTE      : return "UNSUPPORTED_ATTRIBUTE";
      case SZL_STATUS_INVALID_VALUE              : return "INVALID_VALUE";
      case SZL_STATUS_READ_ONLY                  : return "READ_ONLY";
      case SZL_STATUS_INSUFFICIENT_SPACE         : return "INSUFFICIENT_SPACE";
      case SZL_STATUS_DUPLICATE_EXISTS           : return "DUPLICATE_EXISTS";
      case SZL_STATUS_NOT_FOUND                  : return "NOT_FOUND";
      case SZL_STATUS_UNREPORTABLE_ATTRIBUTE     : return "UNREPORTABLE_ATTRIBUTE";
      case SZL_STATUS_INVALID_DATA_TYPE          : return "INVALID_DATA_TYPE";
      case SZL_STATUS_INVALID_SELECTOR           : return "INVALID_SELECTOR";
      case SZL_STATUS_WRITE_ONLY                 : return "WRITE_ONLY";
      case SZL_STATUS_INCONSISTENT_STARTUP_STATE : return "INCONSISTENT_STARTUP_STATE";
      case SZL_STATUS_DEFINED_OUT_OF_BAND        : return "DEFINED_OUT_OF_BAND";
      case SZL_STATUS_INCONSISTENT               : return "INCONSISTENT";
      case SZL_STATUS_ACTION_DENIED              : return "ACTION_DENIED";
      case SZL_STATUS_OTA_TIMEOUT                : return "OTA_TIMEOUT";
      case SZL_STATUS_OTA_ABORT                  : return "OTA_ABORT";
      case SZL_STATUS_OTA_IMAGE_INVALID          : return "OTA_IMAGE_INVALID";
      case SZL_STATUS_OTA_WAIT_FOR_DATA          : return "OTA_WAIT_FOR_DATA";
      case SZL_STATUS_OTA_NO_IMAGE_AVAILABLE     : return "OTA_NO_IMAGE_AVAILABLE";
      case SZL_STATUS_OTA_REQUIRE_MORE_IMAGE     : return "OTA_REQUIRE_MORE_IMAGE";
      case SZL_STATUS_SEC_NO_KEY                 : return "SEC_NO_KEY";
      case SZL_STATUS_SEC_OLD_FRM_COUNT          : return "SEC_OLD_FRM_COUNT";
      case SZL_STATUS_SEC_MAX_FRM_COUNT          : return "SEC_MAX_FRM_COUNT";
      case SZL_STATUS_SEC_CCM_FAIL               : return "SEC_CCM_FAIL";
      case SZL_STATUS_APS_FAIL                   : return "APS_FAIL";
      case SZL_STATUS_APS_TABLE_FULL             : return "APS_TABLE_FULL";
      case SZL_STATUS_APS_ILLEGAL_REQUEST        : return "APS_ILLEGAL_REQUEST";
      case SZL_STATUS_APS_INVALID_BINDING        : return "APS_INVALID_BINDING";
      case SZL_STATUS_APS_UNSUPPORTED_ATTRIBUTE  : return "APS_UNSUPPORTED_ATTRIBUTE";
      case SZL_STATUS_APS_NOT_SUPPORTED          : return "APS_NOT_SUPPORTED";
      case SZL_STATUS_APS_NO_ACK                 : return "APS_NO_ACK";
      case SZL_STATUS_APS_DUPLICATED_ENTRY       : return "APS_DUPLICATED_ENTRY";
      case SZL_STATUS_APS_NO_BOUND_DEVICE        : return "APS_NO_BOUND_DEVICE";
      case SZL_STATUS_APS_NOT_ALLOWED            : return "APS_NOT_ALLOWED";
      case SZL_STATUS_APS_NOT_AUTHENTICATED      : return "APS_NOT_AUTHENTICATED";
      case SZL_STATUS_HARDWARE_FAILURE           : return "HARDWARE_FAILURE";
      case SZL_STATUS_NWK_INVALID_PARAM          : return "NWK_INVALID_PARAM";
      case SZL_STATUS_NWK_INVALID_REQUEST        : return "NWK_INVALID_REQUEST";
      case SZL_STATUS_NWK_NOT_PERMITTET          : return "NWK_NOT_PERMITTET";
      case SZL_STATUS_NWK_STARTUP_FAILURE        : return "NWK_STARTUP_FAILURE";
      case SZL_STATUS_NWK_ALREADY_PRESENT        : return "NWK_ALREADY_PRESENT";
      case SZL_STATUS_NWK_SYNC_FAILURE           : return "NWK_SYNC_FAILURE";
      case SZL_STATUS_NWK_TABLE_FULL             : return "NWK_TABLE_FULL";
      case SZL_STATUS_NWK_UNKNOWN_DEVICE         : return "NWK_UNKNOWN_DEVICE";
      case SZL_STATUS_NWK_UNSUPPORTED_ATTRIBUTES : return "NWK_UNSUPPORTED_ATTRIBUTES";
      case SZL_STATUS_NWK_NO_NETWORKS            : return "NWK_NO_NETWORKS";
      case SZL_STATUS_NWK_LEAVE_UNCONFIRMED      : return "NWK_LEAVE_UNCONFIRMED";
      case SZL_STATUS_NWK_NO_ACK                 : return "NWK_NO_ACK";
      case SZL_STATUS_NWK_NO_ROUTE               : return "NWK_NO_ROUTE";
      case SZL_STATUS_MAC_BEACON_LOSS            : return "MAC_BEACON_LOSS";
      case SZL_STATUS_MAC_CHANNEL_ACCEPT_FAILURE : return "MAC_CHANNEL_ACCEPT_FAILURE";
      case SZL_STATUS_MAC_DENIED                 : return "MAC_DENIED";
      case SZL_STATUS_MAC_DISABLE_TRX_FAILURE    : return "MAC_DISABLE_TRX_FAILURE";
      case SZL_STATUS_MAC_FAILED_SECURITY_CHECK  : return "MAC_FAILED_SECURITY_CHECK";
      case SZL_STATUS_MAC_FRAME_TOO_LONG         : return "MAC_FRAME_TOO_LONG";
      case SZL_STATUS_MAC_INVALID_GTS            : return "MAC_INVALID_GTS";
      case SZL_STATUS_MAC_INVALID_HANDLE         : return "MAC_INVALID_HANDLE";
      case SZL_STATUS_MAC_INVALID_PARAMETER      : return "MAC_INVALID_PARAMETER";
      case SZL_STATUS_MAC_NO_ACK                 : return "MAC_NO_ACK";
      case SZL_STATUS_MAC_NO_BEACON              : return "MAC_NO_BEACON";
      case SZL_STATUS_MAC_NO_DATA                : return "MAC_NO_DATA";
      case SZL_STATUS_MAC_NO_SHORT_ADDRESS       : return "MAC_NO_SHORT_ADDRESS";
      case SZL_STATUS_MAC_OUT_OF_CAP             : return "MAC_OUT_OF_CAP";
      case SZL_STATUS_MAC_PANID_CONFLICT         : return "MAC_PANID_CONFLICT";
      case SZL_STATUS_MAC_REALIGNMENT            : return "MAC_REALIGNMENT";
      case SZL_STATUS_MAC_TRANSACTION_EXPIRED    : return "MAC_TRANSACTION_EXPIRED";
      case SZL_STATUS_MAC_TRANSACTION_OVERFLOW   : return "MAC_TRANSACTION_OVERFLOW";
      case SZL_STATUS_MAC_TX_ACTIVE              : return "MAC_TX_ACTIVE";
      case SZL_STATUS_MAC_UNAVAILABLE_KEY        : return "MAC_UNAVAILABLE_KEY";
      case SZL_STATUS_MAC_UNSUPPORTED_ATTRIBUTE  : return "MAC_UNSUPPORTED_ATTRIBUTE";
      case SZL_STATUS_MAC_UNSUPPORTED            : return "MAC_UNSUPPORTED";
      case SZL_STATUS_MAC_SRC_MATCH_INVALID_INDEX: return "MAC_SRC_MATCH_INVALID_INDEX";
      default: return "Unknown";
    }
}

/******************************************************************************
 * Get a string for an SZL Address Struct
 ******************************************************************************/
char* appZcl_GetSzlAddressString(SZL_Addresses_t* Address, char* AddrString, unsigned16 MaxStringLength)
{
  switch(Address->AddressMode)
    {
      case SZL_ADDRESS_MODE_VIA_BIND: snprintf(AddrString, MaxStringLength, "Bind"); break;
      case SZL_ADDRESS_MODE_GROUP: snprintf(AddrString, MaxStringLength, "Group:0x%04X", (unsigned int)Address->Addresses.Group.Address); break;
      case SZL_ADDRESS_MODE_NWK_ADDRESS: snprintf(AddrString, MaxStringLength, "Nwk:0x%04X, EP: 0x%02X", Address->Addresses.Nwk.Address, Address->Addresses.Nwk.Endpoint); break;
      case SZL_ADDRESS_MODE_IEEE_ADDRESS: snprintf(AddrString, MaxStringLength, "Ieee:0x%016llX, EP:0x%02X", (long long unsigned int)Address->Addresses.IEEE.Address, Address->Addresses.IEEE.Endpoint); break;
      case SZL_ADDRESS_MODE_BROADCAST:
        switch(Address->Addresses.Broadcast.Mode)
          {
            case SZL_BROADCAST_ALL: snprintf(AddrString, MaxStringLength, "Broadcast: All"); break;
            case SZL_BROADCAST_RX_ON_WHEN_IDLE: snprintf(AddrString, MaxStringLength, "Broadcast: RxOnWhenIdle"); break;
            case SZL_BROADCAST_ROUTERS_AND_COORDINATOR: snprintf(AddrString, MaxStringLength, "Broadcast: RoutersAndCoordinators"); break;
            case SZL_BROADCAST_LOW_POWER_ROUTERS: snprintf(AddrString, MaxStringLength, "Broadcast: LowpowerRouters"); break;
            default: snprintf(AddrString, MaxStringLength, "Broadcast: Unknown"); break;
          }
        break;
      case SZL_ADDRESS_MODE_GP_SOURCE_ID: snprintf(AddrString, MaxStringLength, "GpSoureId:0x%08X", Address->Addresses.GpSourceId.SourceId); break;
      default: snprintf(AddrString, MaxStringLength, "UNKNOWN"); break;
    }
  return AddrString;
}

/******************************************************************************
 * Get a string for a SZL_ADDRESS_MODE_t
 ******************************************************************************/
char* appZcl_GetSzlAddressModeString(SZL_ADDRESS_MODE_t AddressMode)
{
  switch(AddressMode)
    {
      case SZL_ADDRESS_MODE_VIA_BIND:       return "VIA_BIND";
      case SZL_ADDRESS_MODE_GROUP:          return "GROUP";
      case SZL_ADDRESS_MODE_NWK_ADDRESS:    return "NWK_ADDRESS";
      case SZL_ADDRESS_MODE_IEEE_ADDRESS:   return "IEEE_ADDRESS";
      case SZL_ADDRESS_MODE_BROADCAST:      return "BROADCAST";
      case SZL_ADDRESS_MODE_GP_SOURCE_ID:   return "GP_SOURCE_ID";
      default:                              return "UNKNOWN";
    }
}


/******************************************************************************
 * Get a string for a ZCL Cluster ID
 ******************************************************************************/
char* appZcl_GetClusterString(unsigned16 ClusterId)
{
  switch(ClusterId)
    { /* From ZigBee Alliance Doc 11-5456-13-0csg-master-cluster-list */
      case 0x0000: return "Basic";
      case 0x0001: return "PowerCfg";
      case 0x0002: return "DeviceTempCfg";
      case 0x0003: return "Identify";
      case 0x0004: return "Groups";
      case 0x0005: return "Scenes";
      case 0x0006: return "On/Off";
      case 0x0007: return "On/OffSwCfg";
      case 0x0008: return "LevelCtrl";
      case 0x0009: return "Alarms";
      case 0x000a: return "Time";
      case 0x000b: return "RSSILocation";
      case 0x000c: return "AnalogInput";
      case 0x000d: return "AnalogOutput";
      case 0x000e: return "AnalogValue";
      case 0x000f: return "BinaryInput";
      case 0x0010: return "BinaryOutput";
      case 0x0011: return "BinaryValue";
      case 0x0012: return "MultistateInput";
      case 0x0013: return "MultistateOutput";
      case 0x0014: return "MultistateValue";
      case 0x0015: return "Commissioning";
      case 0x0016: return "Partition";
      case 0x0017: return "ASKeyEst";
      case 0x0018: return "ASAccessCtrl";
      case 0x0019: return "OTAUpgrade";
      case 0x001a: return "PowerProfile";
      case 0x001b: return "ApplianceCtrl";
      case 0x001c: return "PWM";
      case 0x0020: return "PollCtrl";
      case 0x0021: return "ZGPProxy";
      case 0x0022: return "MobileDevCfg";
      case 0x0023: return "NeighborClean";
      case 0x0024: return "NearestGateway";
      case 0x0100: return "ShadeCfg";
      case 0x0101: return "DoorLock";
      case 0x0102: return "WindowCovering";
      case 0x0103: return "BarrierCtrl";
      case 0x0200: return "PumpCfg/Ctrl";
      case 0x0201: return "Thermostat";
      case 0x0202: return "FanCtrl";
      case 0x0203: return "DehumidCtrl";
      case 0x0204: return "ThermostatUICfg";
      case 0x0300: return "ColorCtrl";
      case 0x0301: return "BallastCfg";
      case 0x0400: return "IllumMeas";
      case 0x0401: return "IllumLevelSensing";
      case 0x0402: return "TempMeas";
      case 0x0403: return "PressureMeas";
      case 0x0404: return "FlowMeas";
      case 0x0405: return "RHMeas";
      case 0x0406: return "OccupancySensing";
      case 0x0407: return "LeafWetness";
      case 0x0408: return "SoilMoisture";
      case 0x0409: return "pHMeas";
      case 0x040a: return "ElecCondMeas";
      case 0x040b: return "WindSpeedMeas";
      case 0x040c: return "CarbonMonoxide";
      case 0x040d: return "CarbonDioxide";
      case 0x040e: return "Ethylene";
      case 0x040f: return "EthyleneOxide";
      case 0x0410: return "Hydrogen";
      case 0x0411: return "HydrogenSulfide";
      case 0x0412: return "NitricOxide";
      case 0x0413: return "NitrogenDioxide";
      case 0x0414: return "Oxygen";
      case 0x0415: return "Ozone";
      case 0x0416: return "SulfurDioxide";
      case 0x0417: return "DissolvedOxygen";
      case 0x0418: return "Bromate";
      case 0x0419: return "Chloramines";
      case 0x041a: return "Chlorine";
      case 0x041b: return "Fecal&EColi";
      case 0x041c: return "Fluoride";
      case 0x041d: return "HaloaceticAcids";
      case 0x041e: return "TotalTrihalomethanes";
      case 0x041f: return "TotalColiformBacteria";
      case 0x0420: return "Turbidity";
      case 0x0421: return "Copper";
      case 0x0422: return "Lead";
      case 0x0423: return "Manganese";
      case 0x0424: return "Sulfate";
      case 0x0425: return "Bromodichloromethane";
      case 0x0426: return "Bromoform";
      case 0x0427: return "Chlorodibromomethane";
      case 0x0428: return "Chloroform";
      case 0x0429: return "Sodium";
      case 0x0500: return "IASZone";
      case 0x0501: return "IASACE";
      case 0x0502: return "IASWD";
      case 0x0600: return "GenericTunnel";
      case 0x0601: return "BNProtocolTunnel";
      case 0x0602: return "AnalogInput(BNReg)";
      case 0x0603: return "AnalogInput(BNExt)";
      case 0x0604: return "AnalogOutput(BNReg)";
      case 0x0605: return "AnalogOutput(BNExt)";
      case 0x0606: return "AnalogValue(BNReg)";
      case 0x0607: return "AnalogValue(BNExt)";
      case 0x0608: return "BinaryInput(BNReg)";
      case 0x0609: return "BinaryInput(BNExt)";
      case 0x060a: return "BinaryOutput(BNReg)";
      case 0x060b: return "BinaryOutput(BNExt)";
      case 0x060c: return "BinaryValue(BNReg)";
      case 0x060d: return "BinaryValue(BNExt)";
      case 0x060e: return "MultistateInput(BNReg)";
      case 0x060f: return "MultistateInput(BNExt)";
      case 0x0610: return "MultistateOutput(BNReg)";
      case 0x0611: return "MultistateOutput(BNExt)";
      case 0x0612: return "MultistateValue(BNReg)";
      case 0x0613: return "MultistateValue(BNExt)";
      case 0x0614: return "11073ProtocolTunnel";
      case 0x0615: return "ISO7816Protocol";
      case 0x0617: return "RetailTunnel";
      case 0x0700: return "Price";
      case 0x0701: return "DemandResponseLoadCtrl";
      case 0x0702: return "Meter";
      case 0x0703: return "Messaging";
      case 0x0704: return "SmartEnergyTunnel";
      case 0x0705: return "Prepayment";
      case 0x0706: return "EnergyMgmt";
      case 0x0707: return "TOUCalendar";
      case 0x0708: return "DeviceMgmt";
      case 0x0709: return "Events";
      case 0x070a: return "MDUPairing";
      case 0x0800: return "KeyEstablishment";
      case 0x0900: return "Information";
      case 0x0901: return "DataSharing";
      case 0x0902: return "Gaming";
      case 0x0903: return "DataRateCtrl";
      case 0x0904: return "VoiceOverZigBee";
      case 0x0905: return "Chatting";
      case 0x0a01: return "Payment";
      case 0x0a02: return "Billing";
      case 0x0b00: return "ApplianceId";
      case 0x0b01: return "MeterId";
      case 0x0b02: return "ApplianceEventsandAlerts";
      case 0x0b03: return "ApplianceStatistics";
      case 0x0b04: return "ElectricalMeas";
      case 0x0b05: return "Diagnostics";
      case 0x1000: return "TouchlinkComm";

      /* From ZEC0035 01.00.04 Schneider Electric ZigBee Cluster Library */
      case 0xFF14: return "Schneider-ElecProtect";
      case 0xFF15: return "Schneider-GPD";

      default:     return "UNKNOWN";
    }
  return "UNKNOWN";
}

/******************************************************************************
 * Identify Handler
 ******************************************************************************/
void appZcl_SzlIdentifyPluginHandler(zabService* Service, szl_uint8 endpoint, szl_bool identifying)
{
  printApp(Service, "App ZCL Identify Handler: EP 0x%0X, ID = %s\n", endpoint, identifying ? "true" : "false");
}

/******************************************************************************
 * On/Off Handler
 ******************************************************************************/
void appZcl_SzlOnOffPluginHandler(zabService* Service, szl_uint8 endpoint, szl_bool onOff)
{
  printApp(Service, "App ZCL On/Off Handler: EP 0x%0X = %s\n", endpoint, onOff ? "ON" : "OFF");
}

/******************************************************************************
 * UTC Time Handler
 ******************************************************************************/
void appZcl_SzlTimePluginHandler(szl_uint32* UtcTime)
{
  platUtility_GetUtcTime(UtcTime);
}

/******************************************************************************
 * Cluster Command Response Handler
 ******************************************************************************/
void appZcl_ClusterCmdRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ClusterCmdRespParams_t *Params, szl_uint8 TransactionId)
{
  unsigned8 index;
  char addrString[APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH];

  printApp(Service, "appZcl: Cluster Cmd  Rsp: SzlSts = 0x%02X, TID = 0x%02X, ", Status, TransactionId);

  if (Params == NULL)
    {
      printApp(Service, "WARNING: Params=NULL\n");
    }
  else
    {
      printApp(Service, "DstEP = 0x%02X, SrcAddr = %s, Cl = 0x%04X (%s), MSC = 0x%02X, ManId=0x%04X, Cmd = 0x%02X, Tid = 0x%02X, Payload Length = 0x%02X Data =",
               Params->DestinationEndpoint,
               appZcl_GetSzlAddressString(&Params->SourceAddress, addrString, sizeof(addrString)),
               Params->ClusterID, appZcl_GetClusterString(Params->ClusterID),
               Params->ManufacturerSpecific,
               ZB_SCHNEIDER_MANUFACTURE_ID,
               Params->Command,
               TransactionId,
               Params->PayloadLength);

      for (index = 0; index < Params->PayloadLength; index++)
        {
          printApp(Service, " %02X", Params->Payload[index]);
        }
      printApp(Service, "\n");
    }
}

/******************************************************************************
 * Cluster Command Response timeout mgmt Handler
 ******************************************************************************/
void appZcl_ClusterCmdRspTimeOutMgmtHandler(zabService* Service, SZL_STATUS_t Status, SZL_ClusterCmdRespParams_t *Params, szl_uint8 TransactionId)
{
  unsigned8 index;
  unsigned8 i;
  char addrString[APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH];

  printApp(Service, "appZcl: Cluster Cmd  Rsp: SzlSts = 0x%02X, TID = 0x%02X, ", Status, TransactionId);

  if (Params == NULL)
    {
      printApp(Service, "WARNING: Params=NULL\n");
    }
  else
    {
      printApp(Service, "DstEP = 0x%02X, SrcAddr = %s, Cl = 0x%04X (%s), MSC = 0x%02X, ManId=0x%04X, Cmd = 0x%02X, Tid = 0x%02X, Payload Length = 0x%02X Data =",
               Params->DestinationEndpoint,
               appZcl_GetSzlAddressString(&Params->SourceAddress, addrString, sizeof(addrString)),
               Params->ClusterID, appZcl_GetClusterString(Params->ClusterID),
               Params->ManufacturerSpecific,
               ZB_SCHNEIDER_MANUFACTURE_ID,
               Params->Command,
               TransactionId,
               Params->PayloadLength);

      for (index = 0; index < Params->PayloadLength; index++)
        {
            printApp(Service, " %02X", Params->Payload[index]);
        }
      printApp(Service, "\n");
      
      if(Status == 0x21)
        {
          printApp(Service, "Timeout\n");
        
          for(i=0;i<M_MAX_TIMEOUT;i++)
            {
              if(TimeoutInfo[i].Tid==TransactionId)
                break;
            }
          printApp(Service, "transaction trouve %d", i);
     
          if(i<M_MAX_TIMEOUT)
            {
               if(TimeoutInfo[i].NbRetry>0)
               {
                  printApp(Service, "resend\n"); 
                   SZL_ClusterCmdReqParams_t cmdReq;
                   cmdReq.ManufacturerSpecific = 0;
                   cmdReq.SourceEndpoint = Params->DestinationEndpoint;
                   cmdReq.DestAddrMode = Params->SourceAddress;
                   cmdReq.ClusterID = 6;
                   cmdReq.Direction = ZCL_FRAME_DIR_CLIENT_SERVER;
                   cmdReq.PayloadLength = 0;
                   cmdReq.Command = TimeoutInfo[i].Command;

                   SZL_ClusterCmdReqTimeout(appService, appZcl_ClusterCmdRspTimeOutMgmtHandler, &cmdReq, &TransactionId,TimeoutInfo[i].Timeout,0);

                   TimeoutInfo[i].NbRetry-=1;
                   if(TimeoutInfo[i].NbRetry>0)
                   {
                      TimeoutInfo[i].Tid=TransactionId; 
                   }
                   else
                   {
                       TimeoutInfo[i].Tid=0;
                   }
               }
            }
        }
    }
}

/******************************************************************************
 * Read Attributes Response Handler
 ******************************************************************************/
void appZcl_ReadAttrRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_AttributeReadRespParams_t *Params, szl_uint8 TransactionId)
{
  unsigned8 attrIndex;
  char addrString[APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH];

  printApp(Service, "appZcl: Read Attr Rsp: SzlSts = 0x%02X, TID = 0x%02X, ", Status, TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "DstEP = 0x%02X, SrcAddr = %s, Cl = 0x%04X (%s), MSC = 0x%02X, ManId=0x%04X, NumAttrs = 0x%02X\n",
               Params->DestinationEndpoint,
               appZcl_GetSzlAddressString(&Params->SourceAddress, addrString, sizeof(addrString)),
               Params->ClusterID, appZcl_GetClusterString(Params->ClusterID),
               Params->ManufacturerSpecific,
               ZB_SCHNEIDER_MANUFACTURE_ID,
               Params->NumberOfAttributes);

      for (attrIndex = 0; attrIndex < Params->NumberOfAttributes; attrIndex++)
        {
          printApp(Service, "                AttrId = 0x%04X, Sts = 0x%02X(%s)",
                  Params->Attributes[attrIndex].AttributeID,
                  Params->Attributes[attrIndex].Status, appZcl_GetSzlStatusString(Params->Attributes[attrIndex].Status));
          if (Params->Attributes[attrIndex].Status == SZL_STATUS_SUCCESS)
            {
              printApp(Service, ", DT = 0x%02X, DLen = 0x%02X, Data = ",
                      Params->Attributes[attrIndex].DataType,
                      Params->Attributes[attrIndex].DataLength);

              appZcl_PrintAppAttribute(Service, Params->Attributes[attrIndex].DataType, Params->Attributes[attrIndex].Data, Params->Attributes[attrIndex].DataLength);
            }
          printApp(Service, "\n");
        }
    }
  else
    {
      printApp(Service, "WARNING: PARAMS = NULL!\n");
    }
}

/******************************************************************************
 * Multi Cluster Read Attributes Response Handler
 ******************************************************************************/
void appZcl_MultiCluster_ReadAttrRspHandler(zabService* Service, SZL_STATUS_t Status, SZLEXT_MultiCluster_AttributeReadRespParams_t *Params, szl_uint8 TransactionId)
{
  unsigned8 attrIndex;
  char addrString[APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH];

  printApp(Service, "appZcl: Multi Cluster Read Attr Rsp: SzlSts = 0x%02X, TID = 0x%02X, ", Status, TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "DstEP = 0x%02X, SrcAddr = %s, MSC = 0x%02X, ManId=0x%04X, NumAttrs = 0x%02X\n",
               Params->DestinationEndpoint,
               appZcl_GetSzlAddressString(&Params->SourceAddress, addrString, sizeof(addrString)),
               Params->ManufacturerSpecific,
               ZB_SCHNEIDER_MANUFACTURE_ID,
               Params->NumberOfAttributes);

      for (attrIndex = 0; attrIndex < Params->NumberOfAttributes; attrIndex++)
        {
          printApp(Service, "                Cluster = 0x%04X (%s), AttrId = 0x%04X, Sts = 0x%02X(%s)",
                   Params->Attributes[attrIndex].ClusterID, appZcl_GetClusterString(Params->Attributes[attrIndex].ClusterID),
                   Params->Attributes[attrIndex].AttributeID,
                   Params->Attributes[attrIndex].Status, appZcl_GetSzlStatusString(Params->Attributes[attrIndex].Status));
          if (Params->Attributes[attrIndex].Status == SZL_STATUS_SUCCESS)
            {
              printApp(Service, ", DT = 0x%02X, DLen = 0x%02X, Data = ",
                      Params->Attributes[attrIndex].DataType,
                      Params->Attributes[attrIndex].DataLength);

              appZcl_PrintAppAttribute(Service, Params->Attributes[attrIndex].DataType, Params->Attributes[attrIndex].Data, Params->Attributes[attrIndex].DataLength);
            }
          printApp(Service, "\n");
        }
    }
  else
    {
      printApp(Service, "WARNING: PARAMS = NULL!\n");
    }
}

/******************************************************************************
 * Write Attributes Response Handler
 ******************************************************************************/
void appZcl_WriteAttrRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_AttributeWriteRespParams_t* Params, szl_uint8 TransactionId)
{
  unsigned8 attrIndex;
  char addrString[APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH];

  printApp(Service, "appZcl: Write Attr Rsp: SzlSts = 0x%02X, TID = 0x%02X, ", Status, TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "DstEP = 0x%02X, SrcAddr = %s, Cl = 0x%04X (%s), MSC = 0x%02X, ManId=0x%04X, NumAttrs = 0x%02X\n",
               Params->DestinationEndpoint,
               appZcl_GetSzlAddressString(&Params->SourceAddress, addrString, sizeof(addrString)),
               Params->ClusterID, appZcl_GetClusterString(Params->ClusterID),
               Params->ManufacturerSpecific,
               ZB_SCHNEIDER_MANUFACTURE_ID,
               Params->NumberOfAttributes);

      for (attrIndex = 0; attrIndex < Params->NumberOfAttributes; attrIndex++)
        {
          printApp(Service, "                AttrId = 0x%04X, Sts = 0x%02X(%s)\n",
                  Params->Attributes[attrIndex].AttributeID,
                  Params->Attributes[attrIndex].Status, appZcl_GetSzlStatusString(Params->Attributes[attrIndex].Status));
        }
    }
  else
    {
      printApp(Service, "WARNING: Params=NULL\n");
    }
}

/******************************************************************************
 * Multi Cluster Write Attributes Response Handler
 ******************************************************************************/
void appZcl_MultiCluster_WriteAttrRspHandler(zabService* Service, SZL_STATUS_t Status, SZLEXT_MultiCluster_AttributeWriteRespParams_t* Params, szl_uint8 TransactionId)
{
  unsigned8 attrIndex;
  char addrString[APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH];

  printApp(Service, "appZcl: Multi Cluster Write Attr Rsp: SzlSts = 0x%02X, TID = 0x%02X, ", Status, TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "DstEP = 0x%02X, SrcAddr = %s, MSC = 0x%02X, ManId=0x%04X, NumAttrs = 0x%02X\n",
               Params->DestinationEndpoint,
               appZcl_GetSzlAddressString(&Params->SourceAddress, addrString, sizeof(addrString)),
               Params->ManufacturerSpecific,
               ZB_SCHNEIDER_MANUFACTURE_ID,
               Params->NumberOfAttributes);

      for (attrIndex = 0; attrIndex < Params->NumberOfAttributes; attrIndex++)
        {
          printApp(Service, "                Cluster = 0x%04X (%s), AttrId = 0x%04X, Sts = 0x%02X(%s)\n",
                  Params->Attributes[attrIndex].ClusterID, appZcl_GetClusterString(Params->Attributes[attrIndex].ClusterID),
                  Params->Attributes[attrIndex].AttributeID,
                  Params->Attributes[attrIndex].Status, appZcl_GetSzlStatusString(Params->Attributes[attrIndex].Status));
        }
    }
  else
    {
      printApp(Service, "WARNING: Params=NULL\n");
    }
}


/******************************************************************************
 * Read Reporting Configuration Response Handler
 ******************************************************************************/
void appZcl_ReadReportingConfigRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_AttributeReadReportCfgRespParams_t *Params, szl_uint8 TransactionId)
{
  unsigned8 attrIndex;
  char addrString[APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH];

  printApp(Service, "appZcl: Read Reporting Cfg Rsp: SzlSts = 0x%02X, TID = 0x%02X, ", Status, TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "DstEP = 0x%02X, SrcAddr = %s, Cl = 0x%04X (%s), MSC = 0x%02X, ManId=0x%04X, NumAttrs = 0x%02X\n",
               Params->DestinationEndpoint,
               appZcl_GetSzlAddressString(&Params->SourceAddress, addrString, sizeof(addrString)),
               Params->ClusterID, appZcl_GetClusterString(Params->ClusterID),
               Params->ManufacturerSpecific,
               ZB_SCHNEIDER_MANUFACTURE_ID,
               Params->NumberOfAttributes);

      for (attrIndex = 0; attrIndex < Params->NumberOfAttributes; attrIndex++)
        {
          printApp(Service, "                AttrId = 0x%04X, Sts = 0x%02X(%s), DT = 0x%02X, MinInt = 0x%04X, MaxInt = 0x%04X, DLen = 0x%02X",
                  Params->Attributes[attrIndex].AttributeID,
                  Params->Attributes[attrIndex].Status, appZcl_GetSzlStatusString(Params->Attributes[attrIndex].Status),
                  Params->Attributes[attrIndex].DataType,
                  Params->Attributes[attrIndex].MinInterval,
                  Params->Attributes[attrIndex].MaxInterval,
                  Params->Attributes[attrIndex].LenRC);

          if (Params->Attributes[attrIndex].LenRC > 0)
            {
              printApp(Service, ", RepChange = ");
              appZcl_PrintAppAttribute(Service, Params->Attributes[attrIndex].DataType, Params->Attributes[attrIndex].ReportableChangeData, Params->Attributes[attrIndex].LenRC);
            }
          printApp(Service, "\n");
        }
    }
  else
    {
      printApp(Service, "WARNING: PARAMS = NULL!\n");
    }
}


/******************************************************************************
 * Write Attributes Response Handler
 ******************************************************************************/
void appZcl_DiscoverAttrRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_AttributeDiscoverRespParams_t* Params, szl_uint8 TransactionId)
{
  unsigned8 attrIndex;
  char addrString[APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH];

  printApp(Service, "appZcl: Disc Attr Rsp: SzlSts = 0x%02X, TID = 0x%02X, ", Status, TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "DstEP = 0x%02X, SrcAddr = %s, MSC = 0x%02X, ManId=0x%04X, Cl = 0x%04X (%s), ListComp = %s, NumAttrs = 0x%02X\n",
               Params->DestinationEndpoint,
               appZcl_GetSzlAddressString(&Params->SourceAddress, addrString, sizeof(addrString)),
               Params->ManufacturerSpecific,
               ZB_SCHNEIDER_MANUFACTURE_ID,
               Params->ClusterID, appZcl_GetClusterString(Params->ClusterID),
               Params->ListComplete ? "true" : "false",
               Params->NumberOfAttributes);

      for (attrIndex = 0; attrIndex < Params->NumberOfAttributes; attrIndex++)
        {
          printApp(Service, "          AttrId = 0x%04X, DataType = 0x%02X\n",
                  Params->Attributes[attrIndex].AttributeID,
                  Params->Attributes[attrIndex].DataType);
        }
    }
  else
    {
      printApp(Service, "WARNING: Params=NULL\n");
    }
}


/******************************************************************************
 * Configure Reporting Response Handler
 ******************************************************************************/
void appZcl_ConfigureReportingHandler(zabService* Service, SZL_STATUS_t Status, SZL_AttributeReportCfgRespParams_t* Params, szl_uint8 TransactionId)
{
  unsigned8 attrIndex;
  char addrString[APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH];
  printApp(Service, "appZcl: Cfg Rep Rsp: SzlSts = 0x%02X, TID = 0x%02X, ", Status, TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "DstEP = 0x%02X, SrcAddr = %s, Cl = 0x%04X (%s), NumAttrs = 0x%02X\n",
               Params->DestinationEndpoint,
               appZcl_GetSzlAddressString(&Params->SourceAddress, addrString, sizeof(addrString)),
               Params->ClusterID, appZcl_GetClusterString(Params->ClusterID),
               Params->NumberOfAttributes);

      for (attrIndex = 0; attrIndex < Params->NumberOfAttributes; attrIndex++)
        {
          printApp(Service, "                AttrId = 0x%04X, Sts = 0x%02X(%s)\n",
                    Params->Attributes[attrIndex].AttributeID,
                    Params->Attributes[attrIndex].Status, appZcl_GetSzlStatusString(Params->Attributes[attrIndex].Status));
        }
    }
  else
    {
      printApp(Service, "WARNING: Params=NULL\n");
    }
}

/******************************************************************************
 * Report Attributes Handler
 ******************************************************************************/
void appZcl_ReportAttrHandler(zabService* Service, SZL_AttributeReportNtfParams_t *Params)
{
  unsigned8 attrIndex;
  char addrString[APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH];

  if (Params != NULL)
    {
      if (appConfig_Config.printReportInfo == zab_true)
        {
          printApp(Service, "appZcl: Report Attr: DstEP = 0x%02X, SrcAddr = %s, Cl = 0x%04X (%s), MSC = 0x%02X, ManId=0x%04X, NumAttrs = 0x%02X\n",
                   Params->DestinationEndpoint,
                   appZcl_GetSzlAddressString(&Params->SourceAddress, addrString, sizeof(addrString)),
                   Params->ClusterID, appZcl_GetClusterString(Params->ClusterID),
                   Params->ManufacturerSpecific,
                   ZB_SCHNEIDER_MANUFACTURE_ID,
                   Params->NumberOfAttributes);

          if (appConfig_Config.printFullReportInfo == zab_true)
            {
              for (attrIndex = 0; attrIndex < Params->NumberOfAttributes; attrIndex++)
                {
                  printApp(Service, "                AttrId = 0x%04X, Sts = 0x%02X(%s)",
                          Params->Attributes[attrIndex].AttributeID,
                          Params->Attributes[attrIndex].Status, appZcl_GetSzlStatusString(Params->Attributes[attrIndex].Status));
                  if (Params->Attributes[attrIndex].Status == SZL_STATUS_SUCCESS)
                    {
                      printApp(Service, ", DT = 0x%02X, DLen = 0x%02X, Data = ",
                              Params->Attributes[attrIndex].DataType,
                              Params->Attributes[attrIndex].DataLength);
                      appZcl_PrintAppAttribute(Service, Params->Attributes[attrIndex].DataType, Params->Attributes[attrIndex].Data, Params->Attributes[attrIndex].DataLength);
                    }
                  printApp(Service, "\n");
                }
            }
        }
#ifdef APP_CONFIG_ENABLE_GREEN_POWER
      /* Notify GP module of activity so it can track comms */
      if (Params->SourceAddress.AddressMode == SZL_ADDRESS_MODE_GP_SOURCE_ID)
        {
          appGp_CommsReceived(Service, Params->SourceAddress.Addresses.GpSourceId.SourceId);
        }
#endif

      /* Log report info to csv file if enabled */
      if (appConfig_Config.generateReportCsvFile == zab_true)
        {
          FILE * pFile;

          pFile = fopen (REPORT_CSV_FILENAME, "a"); // open for apending

          if (pFile != NULL)
            {
              char attributeDataString[ATTRIBUTE_DATA_MAX_STRING_LENGTH];
              time_t rawtime;
              struct tm * timeinfo;
              struct timeval  tv;
              gettimeofday(&tv, NULL);
              rawtime = tv.tv_sec;
              timeinfo = localtime ( &rawtime );

              for (attrIndex = 0; attrIndex < Params->NumberOfAttributes; attrIndex++)
                {
                  /* Timestamp */
                  if (timeinfo != NULL)
                    {
                      fprintf(pFile, "%02d/%02d/%04d,%02d:%02d:%02d:%03d,",
                              timeinfo->tm_mday, timeinfo->tm_mon + 1, 1900 + timeinfo->tm_year,
                              timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, (int)tv.tv_usec / 1000);
                    }

                  /* Source info */
                  fprintf(pFile, "%s,0x%04X,0x%04X,0x%02X,0x%02X,",
                          appZcl_GetSzlAddressString(&Params->SourceAddress, addrString, sizeof(addrString)),
                          Params->ClusterID,
                          Params->Attributes[attrIndex].AttributeID,
                          Params->Attributes[attrIndex].Status,
                          Params->Attributes[attrIndex].DataType);

                  /* Data */
                  attributeDataString[0] = 0;
                  appZcl_AttributeToString(Service,
                                           Params->Attributes[attrIndex].DataType,
                                           Params->Attributes[attrIndex].Data,
                                           Params->Attributes[attrIndex].DataLength,
                                           attributeDataString, ATTRIBUTE_DATA_MAX_STRING_LENGTH);
                  fprintf(pFile, "%s,\n", attributeDataString);
                }
              fclose (pFile);
            }
        }
    }
  else
    {
      printApp(Service, "appZcl: Read Attr Rsp: WARNING: Null pointer\n");
    }
}

/******************************************************************************
 * Received RSSI Handler
 ******************************************************************************/
void appZcl_ReceivedSignalStrengthHandler(zabService* Service, struct _SZL_ReceivedSignalStrengthNtfParams_t* Params)
{
  char addrString[APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH];

  if (appConfig_Config.printRssiInfo == zab_true)
    {
      printApp(Service, "appZcl: Received Signal Strength Handler:\n");
      printApp(Service, "          NwkSrcAddr = %s\n",
               appZcl_GetSzlAddressString(&Params->NwkSourceAddress, addrString, sizeof(addrString)));
      printApp(Service, "          MacSrcAddr = %s, LQI = 0x%02X, RSSI = %ddBm\n",
               appZcl_GetSzlAddressString(&Params->MacSourceAddress, addrString, sizeof(addrString)),
               Params->LastHopLqi,
               Params->LastHopRssi);
    }
}


/******************************************************************************
 * Raw ZCL Handler
 * Do not suppress further processing
 ******************************************************************************/
void appZcl_RawZclHandler(erStatus* Status,
                         zabService* Service,
                         zab_bool* suppressProcessing,
                         zabMsgProDataInd* dataInd)
{
  unsigned8 i;

  ER_CHECK_NULL(Status, dataInd);

  printApp(Service, "appZcl Raw Handler: AddrMode = %s, DstEP = 0x%02X, SrcAddr = 0x%02X, SrcEP = 0x%02X, Cluster = 0x%04X, LQI = 0x%02X, Sec = 0x%02X, Time = 0x%08X, DataLength = 0x%02X, Data = ",
            getAddrModeString(dataInd->dstAddr.addressMode),
            dataInd->dstAddr.endpoint,
            dataInd->srcAddr.address.shortAddress,
            dataInd->srcAddr.endpoint,
            dataInd->cluster,
            dataInd->lqi,
            dataInd->securityStatus,
            dataInd->timeStamp,
            dataInd->dataLength);

  for (i = 0; i < dataInd->dataLength; i++)
    {
      printApp(Service, "0x%02X, ", dataInd->data[i]);
    }
  printApp(Service, "\n");
}

/******************************************************************************
 * Generic Cluster Command Handler
 ******************************************************************************/
szl_bool appZcl_ClusterCmdHandler(zabService* Service,
                                  SZL_EP_t DestinationEndpoint,
                                  szl_bool ManufacturerSpecific,
                                  szl_uint16 ClusterID,
                                  szl_uint8 CmdID,
                                  szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize,
                                  szl_uint8* CmdOutID,
                                  szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize,
                                  szl_uint8 TransactionId,
                                  SZL_Addresses_t SourceAddress,
                                  SZL_ADDRESS_MODE_t DestAddressMode)
{
  unsigned8 i;
  char addrString[APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH];

  printApp(Service, "appZcl: Cluster Command: TID = 0x%02X, SrcAddr = %s, DstEP = 0x%02X, DstAddrMode = %s, MS = 0x%02X, Cluster = 0x%04X, Cmd = 0x%02X, DataIn = ",
           TransactionId,
           appZcl_GetSzlAddressString(&SourceAddress, addrString, sizeof(addrString)),
           DestinationEndpoint,
           appZcl_GetSzlAddressModeString(DestAddressMode),
           ManufacturerSpecific,
           ClusterID,
           CmdID);

  for (i = 0; i < PayloadInSize; i++)
    {
      printApp(Service, "%02X ", ZCLPayloadIn[i]);
    }
  printApp(Service, "\n");

  *PayloadOutSize = 0;
  return szl_true;
}
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/