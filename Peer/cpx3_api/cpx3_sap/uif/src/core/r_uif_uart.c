/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
* File Name   : r_uif_uart.c
*    @version
*        $Rev: 3384 $
*    @last editor
*        $Author: a5089752 $
*    @date
*        $Date:: 2017-05-31 13:49:31 +0900#$
* Description :
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_typedefs.h"
#include "r_uif_uart.h"
#include "r_uif_thread_if.h"
#include "r_uif_intc.h"
#include "r_uif_hdlc.h"
#include "r_bsp_api.h"
#include "r_crc32_api.h"

/******************************************************************************
  PROTOTYPE declared
******************************************************************************/
static void uif_int_rx_byte_ch0 (uint8_t r_data);
static void uif_int_rx_byte_ch1 (uint8_t r_data);
static void uif_int_tx_ch0 (void);
static void uif_int_tx_ch1 (void);

/******************************************************************************
Macro definitions
******************************************************************************/

/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/
extern const uint32_t g_uif_baudrate[];

/******************************************************************************
Private global variables and functions
******************************************************************************/


/******************************************************************************
* Function Name:uif_int_rx_byte_ch0
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void uif_int_rx_byte_ch0 (uint8_t r_data)
{
    R_UIF_IntRxByte (R_UIF_PORT_0, r_data);

}
/******************************************************************************
   End of function  uif_int_rx_byte_ch0
******************************************************************************/

/******************************************************************************
* Function Name:uif_int_rx_byte_ch1
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void uif_int_rx_byte_ch1 (uint8_t r_data)
{
    R_UIF_IntRxByte (R_UIF_PORT_1, r_data);

}
/******************************************************************************
   End of function  uif_int_rx_byte_ch1
******************************************************************************/

/******************************************************************************
* Function Name:uif_int_tx_ch0
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void uif_int_tx_ch0 (void)
{
    R_UIF_TxComplete (0);
    R_UIF_IntTx (R_UIF_PORT_0);
}
/******************************************************************************
   End of function  uif_int_tx_ch0
******************************************************************************/

/******************************************************************************
* Function Name:uif_int_tx_ch1
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void uif_int_tx_ch1 (void)
{
    R_UIF_TxComplete (1);
    R_UIF_IntTx (R_UIF_PORT_1);
}
/******************************************************************************
   End of function  uif_int_tx_ch1
******************************************************************************/

/******************************************************************************
* Function Name:uif_drvif_uart_init
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_sys_status_t uif_drvif_uart_init (uint8_t ch_id, r_uif_baud_t baud_rate)
{
    r_sys_status_t ret = R_SYS_STATUS_SUCCESS;
    r_result_t     drv_ret;



    /* Configure CPX UART */
    if (R_UIF_PORT_0 == ch_id)
    {
        drv_ret = R_BSP_ConfigureUart (R_UART_CPX_CLOCK,
                                       g_uif_baudrate[baud_rate],
                                       R_BSP_RX_CPX_UART,
                                       R_UART_TX_IPR,
                                       R_UART_RX_IPR,
                                       R_BSP_CLK_OUT_OFF,
                                       &uif_int_tx_ch0,
                                       &uif_int_rx_byte_ch0);
    }

    /* Configure HOST UART */
    else if (R_UIF_PORT_1 == ch_id)
    {
        drv_ret = R_BSP_ConfigureUart (R_UART_HOST_CLOCK,
                                       g_uif_baudrate[baud_rate],
                                       R_BSP_RX_HOST_UART,
                                       R_UART_TX_IPR,
                                       R_UART_RX_IPR,
                                       R_BSP_CLK_OUT_OFF,
                                       &uif_int_tx_ch1,
                                       &uif_int_rx_byte_ch1);
    }
    else
    {
        drv_ret = R_RESULT_FAILED;
    }


    if (R_RESULT_SUCCESS != drv_ret)
    {
        ret = R_SYS_STATUS_DRV_ERROR;
    }

    return ret;
} /* uif_drvif_uart_init */
/******************************************************************************
   End of function  uif_drvif_uart_init
******************************************************************************/

/******************************************************************************
* Function Name:uif_drvif_uart_deinit
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_sys_status_t uif_drvif_uart_deinit (uint8_t ch_id)
{
    r_sys_status_t ret = R_SYS_STATUS_SUCCESS;
    UNUSED (ch_id);

    return ret;
}
/******************************************************************************
   End of function  uif_drvif_uart_deinit
******************************************************************************/

/******************************************************************************
* Function Name:uif_drvif_uart_send
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_sys_status_t uif_drvif_uart_send (uint8_t ch_id, uint8_t * pdata, uint16_t length)
{
    r_sys_status_t ret = R_SYS_STATUS_SUCCESS;

    UNUSED (ch_id);

    if (R_BSP_SendUart (pdata, length, R_BSP_RX_CPX_UART) != R_RESULT_SUCCESS)
    {
        ret = R_SYS_STATUS_DRV_ERROR;
    }

    return ret;
}
/******************************************************************************
   End of function  uif_drvif_uart_send
******************************************************************************/

/******************************************************************************
* Function Name:uif_drvif_crc32
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_sys_status_t uif_drvif_crc32 (uint8_t port_id, uint16_t length, uint8_t * pin, uint32_t initial,  uint8_t * pcrc_res)
{
    r_iovec_t iovec;
    uint32_t  result;

    UNUSED (port_id);

    if ((NULL == pin) || (NULL == pcrc_res))
    {
        return R_SYS_STATUS_INVALID_PARAMETER;
    }

    iovec.paddress = pin;
    iovec.length   = length;

    result         = R_CRC_CalcCrc32 (&iovec, initial, 1u);

    pcrc_res[0]    = (uint8_t)((result >> 24) & 0xFFu);
    pcrc_res[1]    = (uint8_t)((result >> 16) & 0xFFu);
    pcrc_res[2]    = (uint8_t)((result >> 8) & 0xFFu);
    pcrc_res[3]    = (uint8_t)(result & 0xFFu);

    return R_SYS_STATUS_SUCCESS;

} /* uif_drvif_crc32 */
/******************************************************************************
   End of function  uif_drvif_crc32
******************************************************************************/

