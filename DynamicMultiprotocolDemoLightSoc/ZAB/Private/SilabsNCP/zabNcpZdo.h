/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.005  01-Jul-16   MvdB   Support mandatory ZDO client commands for MiGenie:
 *                                  - SZL_ZDO_MgmtLqiReq, SZL_ZDO_MgmtBindReq, SZL_ZDO_BindReq, SZL_ZDO_UnBindReq,
 *                                  - SZL_NwkLeaveReq, SZL_ZDO_NwkAddrReq, SZL_ZDO_PowerDescriptorReq ,SZL_ZDO_NodeDescriptorReq
 * 000.000.014  22-Jul-16   MvdB   Support: zabNcpZdo_MatchDescReq, zabNcpZdo_MgmtNwkUpdateReq, zabNcpZdo_MgmtRtgReq, zabNcpZdo_UserDescReq, zabNcpZdo_UserDescSetReq
 * 000.000.016  25-Jul-16   MvdB   Support ZAB_ACTION_CHANGE_CHANNEL and other a-synchronous network parameter changes
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/
#ifndef __ZAB_NCP_ZDO_H__
#define __ZAB_NCP_ZDO_H__



#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * ZDO IEEE Address Request
 ******************************************************************************/
void zabNcpZdo_IeeeAddressReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Network Address Request
 ******************************************************************************/
void zabNcpZdo_NwkAddressReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Node Descriptor Request
 ******************************************************************************/
void zabNcpZdo_NodeDescReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Power Descriptor Request
 ******************************************************************************/
void zabNcpZdo_PowerDescReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Active Endpoint Request
 ******************************************************************************/
void zabNcpZdo_ActiveEndpointReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Simple Descriptor Request
 ******************************************************************************/
void zabNcpZdo_SimpleDescReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Match Descriptor Request
 ******************************************************************************/
void zabNcpZdo_MatchDescReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * Bind Request
 ******************************************************************************/
void zabNcpZdo_BindReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * Unbind Request
 ******************************************************************************/
void zabNcpZdo_UnbindReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * MGMT LQI Request
 ******************************************************************************/
void zabNcpZdo_MgmtLqiReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * MGMT Bind Request
 ******************************************************************************/
void zabNcpZdo_MgmtBindReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * MGMT Routing Request
 ******************************************************************************/
void zabNcpZdo_MgmtRtgReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * MGMT Leave Request
 ******************************************************************************/
void zabNcpZdo_MgmtLeaveReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * MGMT Nwk Update Request
 ******************************************************************************/
void zabNcpZdo_MgmtNwkUpdateReq(erStatus* Status, zabService* Service, unsigned8 TransactionId, zabMsgProMgmtNwkUpdateReq* NwkUpdateReq);

/******************************************************************************
 * MGMT Nwk Update Request - Conversion from sapMsg
 ******************************************************************************/
void zabNcpZdo_MgmtNwkUpdateReqConversion(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * ZDO Permit Join Request
 ******************************************************************************/
void zabNcpZdo_PermitJoinReq(erStatus* Status, zabService* Service, unsigned16 NodeId, unsigned8 Duration, zab_bool TcSignificance);

/******************************************************************************
 * User Descriptor Request
 ******************************************************************************/
void zabNcpZdo_UserDescReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * User Descriptor Set Request
 ******************************************************************************/
void zabNcpZdo_UserDescSetReq(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * Process incoming ZDO messages
 ******************************************************************************/
void zabNcpZdo_IncomingMessageHandler(erStatus* Status, zabService* Service, unsigned16 SourceAddress, EmberApsFrame* ApsFrame, unsigned8 PayloadLength, unsigned8* PayloadData);


#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/