"""
mac.mac_frame.mac_frame_generator
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to generate mac frames

"""
from .mac_frame_structure import Mac


class MacFrameGenerator:
    """Functions to generate mac frames"""

    # Default Configuration PLC Concentrator
    DEFAULT_MAC_ADDRESS = bytearray([0x00, 0x00])
    DEFAULT_PAN_ID = bytearray([0x78, 0x1D])

    def __init__(self, serial_driver):
        """Save serial process instance"""
        self._serial = serial_driver

    def send_configuration_concentrator(self, mac_address=DEFAULT_MAC_ADDRESS, pan_id=DEFAULT_PAN_ID):
        """Allow to send a configuration concentrator frame

           Args:
               mac_address: mac address of the PLC Concentrator (Default 0x0000)
               pan_id: Pan ID of the PLC concentrator (Default 0x781D)
        """
        ack_conf = bytearray([Mac.Type.DEVICE_TYPE, Mac.DeviceType.CONCENTRATOR, mac_address[0], mac_address[1],
                              pan_id[0], pan_id[1]])

        self._serial.write_data(Mac.FrameControl.CONTROL, ack_conf)

    def send_data_frame(self, device_number, payload):
        """Allow to send a data mac frame

           Args:
               device_number: device number of the WB
               payload: payload of the frame
        """
        payload_data = bytearray([device_number])

        for element in payload:
            payload_data.append(element)

        self._serial.write_data(Mac.FrameControl.DATA, payload_data)
