/*******************************************************************************
  Filename    : szl_plugin.c
  $Date       : 2013-08-28                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : This is the Plugin handler functions for the  SZL
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include "szl_types.h"
#include "szl.h"
#include "szl_config.h"
#include "szl_timer.h"
#include "endianness.h"
#include "sort.h"
#include "szl_plugin.h"
#include "zabCorePrivate.h"

#define ZAB_SZL_PLUGIN( s ) (ZAB_SRV( s )->szlService.gPluginTasks)


static szl_bool sortNeeded = szl_false;

/**
 * Plugins Compare
 */
static int SzlPT_Compare(const void* a, const void* b)
{
    szl_uint16 da = ((PLUGIN_TASK_t*) a)->ID;
    szl_uint16 db = ((PLUGIN_TASK_t*) b)->ID;

    return (da > db) - (da < db);
}

/**
 * Initialize the plugin handler
 */
void SzlPT_Initialize(zabService* Service)
{
    szl_memset(ZAB_SZL_PLUGIN(Service), 0xFF, sizeof(ZAB_SZL_PLUGIN(Service)));

    // Initialize all the plugins
/*
    Szl_BrickHandlerInitialize(Service);
    Szl_AppHandlerInitialize(Service);
    SzlDPNV_Init(Service);
    Szl_ReportInitialize(Service);
    Szl_PingHandlerInitialize(Service);
*/
}

/**
 * Plugin Handler
 *
 * This function checks if there is any plugin's that has a registered process handler that needs to be called
 */
szl_bool SzlPT_TaskHandler(zabService* Service, SZL_PT_SYSTEM_TASKS_t task)
{
    szl_uint8 plugin = 0;

    if (sortNeeded)
        bubble_sort(ZAB_SZL_PLUGIN(Service), _SZL_PT_HANDLER_COUNT_, sizeof(PLUGIN_TASK_t), SzlPT_Compare);

    sortNeeded = szl_false;

    for (; plugin < _SZL_PT_HANDLER_COUNT_; plugin++)
    {
        if (ZAB_SZL_PLUGIN(Service)[plugin].ID == 0xFFFF )
            continue;

         if (ZAB_SZL_PLUGIN(Service)[plugin].taskHandler(Service, task) == szl_true)
             return szl_true; // still something to do - skip for now
    }

    return szl_false;
}

/**
 * Plugin Register
 *
 */
void SzlPT_Register(zabService* Service, szl_uint16 ID, SZLPT_CB_TaskHandler_t taskHandler)
{
    SZL_ASSERT(taskHandler);

    szl_uint8 idx = 0;

    for (; idx < _SZL_PT_HANDLER_COUNT_; idx++)
    {
        SZL_ASSERT(ZAB_SZL_PLUGIN(Service)[idx].taskHandler != taskHandler);

        if (ZAB_SZL_PLUGIN(Service)[idx].ID == 0xFFFF )
            break; // empty entry
    }

    SZL_ASSERT(idx < _SZL_PT_HANDLER_COUNT_);

    ZAB_SZL_PLUGIN(Service)[idx].ID = ID;
    ZAB_SZL_PLUGIN(Service)[idx].taskHandler = taskHandler;
    sortNeeded = szl_true;
}

/**
 * Plugin Unregister
 *
 */
SZL_RESULT_t SzlPT_Unregister(zabService* Service, szl_uint16 ID)
{
    int idx;

    for (idx = 0; idx < _SZL_PT_HANDLER_COUNT_; idx++)
    {
        if (ZAB_SZL_PLUGIN(Service)[idx].ID == 0xFFFF )
            continue; // empty entry

        if (ZAB_SZL_PLUGIN(Service)[idx].ID == ID)
        {
            ZAB_SZL_PLUGIN(Service)[idx].taskHandler = NULL;
            ZAB_SZL_PLUGIN(Service)[idx].ID = 0xFFFF;
            sortNeeded = szl_true;
            return SZL_RESULT_SUCCESS;
        }
    }

    return SZL_RESULT_NOT_FOUND;
}
