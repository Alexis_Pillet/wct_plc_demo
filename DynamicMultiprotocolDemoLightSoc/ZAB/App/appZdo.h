/******************************************************************************
 *                          ZAB TEST APPLICATION
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the ZDO handlers for the test app.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.03.00  16-Jan-14   MvdB   Use common header block
 * 00.00.03.01  19-Mar-14   MvdB   artf53864: Support mgmt leave request/response
 * 00.00.06.00  27-Jun-14   MvdB   Add User Descriptor support
 * 00.00.06.06  21-Oct-14   MvdB   artf106135: Handle network leave indications
 * 002.000.001  03-Feb-15   MvdB   ARTF115770: Support ZDO Node Descriptor Request
 * 002.001.002  15-Jul-15   MvdB   ARTF136585: Add transaction IDs to over the air ZDO commands/responses
 * 002.002.015  21-Oct-15   MvdB   ARTF104106: Support SZL_ZDO_MatchDescriptorReq()
 *****************************************************************************/

#ifndef APP_ZDO_H_
#define APP_ZDO_H_

#include "zabCoreService.h"
#include "szl.h"
#include "discovery.h"

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * SZL Discovery Plugin - Node Detected Handler
 ******************************************************************************/
extern
void appZdo_Discovery_NodeDetected_Handler(zabService* Service, SzlPlugin_Discovery_NodeInfo_t* NodeInfo);

/******************************************************************************
 * SZL Discovery Plugin - Confirm Handler
 ******************************************************************************/
extern 
void appZdo_Discovery_NodeConfirm_Handler(zabService* Service, SZL_STATUS_t Status, szl_uint8 NumberOfNodes, SzlPlugin_Discovery_NodeInfo_t* NodeInfo);


void appZdo_Discovery_EndpointDiscovered_Handler(zabService* Service, SzlPlugin_Discovery_EndpointInfo_t* EndpointInfo);
void appZdo_Discovery_EndpointConfirm_Handler(zabService* Service, SZL_STATUS_t Status, szl_uint16 NetworkAddress);


/******************************************************************************
 * SZL ZDO Ieee & Nwk Address Response Handler
 ******************************************************************************/
extern
void appZdo_SzlAddrRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoAddrRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * SZL ZDO Node Descriptor Response Handler
 ******************************************************************************/
void appZdo_SzlNodeDescriptorRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoNodeDescriptorRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * SZL ZDO Power Descriptor Response Handler
 ******************************************************************************/
extern 
void appZdo_SzlPowerDescriptorRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoPowerDescriptorRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * SZL ZDO Active Endpoinmt Response Handler
 ******************************************************************************/
extern 
void appZdo_SzlActiveEndpointRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoActiveEndpointRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * SZL ZDO Simple Descriptor Response Handler
 ******************************************************************************/
extern
void appZdo_SzlSimpleDescriptorRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoSimpleDescriptorRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * SZL ZDO Match Descriptor Response Handler
 ******************************************************************************/
extern 
void appZdo_SzlMatchDescriptorRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoMatchDescriptorRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * SZL ZDO MGMT LQI Response Handler
 ******************************************************************************/
extern
void appZdo_SzlMgmtLqiRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoMgmtLqiRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * SZL ZDO Mgmt Network Update Response Handler
 ******************************************************************************/
extern 
void appZdo_SzlMgmtNwkUpdateRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * SZL ZDO MGMT RTG Response Handler
 ******************************************************************************/
extern 
void appZdo_SzlMgmtRtgRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoMgmtRtgRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * SZL ZDO MGMT RTG Response Handler
 ******************************************************************************/
extern 
void appZdo_SzlMgmtBindRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoMgmtBindRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * SZL ZDO Bind Response Handler
 ******************************************************************************/
extern
void appZdo_SzlBindRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoBindRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * SZL ZDO UnBind Response Handler
 ******************************************************************************/
extern 
void appZdo_SzlUnBindRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoBindRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * SZL ZDO User Descriptor Response Handler
 ******************************************************************************/
extern
void appZdo_SzlUserDescriptorRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoUserDescriptorRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * SZL ZDO User Descriptor SetResponse Handler
 ******************************************************************************/
extern 
void appZdo_SzlUserDescriptorSetRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoUserDescriptorSetRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * SZL Leave Response Handler
 ******************************************************************************/
extern 
void appZdo_SzlLeaveRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_NwkLeaveRespParams_t* Params, szl_uint8 TransactionId);

/******************************************************************************
 * Network Leave Indication Handler
 ******************************************************************************/
extern 
void appZdo_NetworkLeaveIndicationHandler(zabService* Service, SZL_NwkLeaveNtfParams_t* Params);

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif /* APP_ZDO_H_ */
