
import logging
import datetime


def get_logger():

    # create logger
    my_logger = logging.getLogger('Wireless Bridge')
    my_logger.setLevel(logging.DEBUG)
    my_logger.propagate = False

    if not len(my_logger.handlers):

        # create console handler and set level to debug
        console_handler = logging.StreamHandler()
        console_handler.setLevel(logging.DEBUG)

        # create formatter
        my_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # add formatter to console_handler
        console_handler.setFormatter(my_formatter)

        # create console handler and set level to debug
        now = datetime.datetime.now()
        file_handler = logging.FileHandler('Wireless_bridge_log_'
                                           + now.strftime("%Y-%m-%d") + '.log')
        file_handler.setLevel(logging.DEBUG)

        # add formatter to console_handler
        file_handler.setFormatter(my_formatter)

        # add console_handler to logger
        my_logger.addHandler(console_handler)
        my_logger.addHandler(file_handler)

    return my_logger
