/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the OTA Upgrade Server.
 *   This plugin is originally designed for MiGenie.
 *
 * USAGE:
 * 
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         05-Jul-16   MvdB   Original
 *    2         12-Jul-16   MvdB   ARTF174575: Support SZL_CB_ClusterCmd_t *PayloadOutSize giving max data length application may provide
 *****************************************************************************/

#include "ota_server.h"
#include "szl_api.h"
#include "szl_internal.h"


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

#define ZCL_CLUSTER_ID_OTA_UPGRADE                          ( 0x0019 )

#define OTA_UPGRADE_FILE_IDENTIFIER                         ( 0x0BEEF11E )

#define QUERY_NEXT_IMAGE_REQ_MIN_PAYLOAD                    ( 9 )   // Fieldcontrol(1), ManCode(2), ImageType(2), CurrentFileVersion(4),
#define QUERY_NEXT_IMAGE_REQ_HARDWARE_VERSION_PRESENT_FLAG  ( 0x01 )
#define QUERY_NEXT_IMAGE_RSP_MAX_PAYLOAD                    ( 13 )  // Status(1), ManCode(2), ImageType(2), FileVersion(4), ImageSize(4)

#define IMAGE_BLOCK_REQ_MIN_PAYLOAD                         ( 14 )  // Fieldcontrol(1), ManCode(2), ImageType(2), FileVersion(4), Offset(4), MaxDataSize(1)
#define IMAGE_BLOCK_RSP_MIN_PAYLOAD                         ( 14 )  // Status(1), ManCode(2), ImageType(2), FileVersion(4), Offset(4), DataSize(1)

#define UPGRADE_END_REQ_MIN_PAYLOAD                         ( 9 )   // Status(1), ManCode(2), ImageType(2), FileVersion(4)
#define UPGRADE_END_RSP_MAX_PAYLOAD                         ( 16 )  // ManCode(2), ImageType(2), FileVersion(4), CurrentTime(4), UpgradeTime(4)

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/* Params required for plugin operation */
typedef struct
{
  szl_uint8 Endpoint;
  SzlPlugin_OtaServer_QueryNextImage_CB_t QueryNextImageCallback;
  SzlPlugin_OtaServer_ImageBlockReq_CB_t ImageBlockReqCallback;
  SzlPlugin_OtaServer_UpgradeEndReq_CB_t UpgradeEndReqCallback;
}SzlPlugin_OtaServer_Params_t;
#define SzlPlugin_OtaServer_Params_t_SIZE ( sizeof(SzlPlugin_OtaServer_Params_t) )


/**
 * OTA cluster commands
 */
typedef enum
{
  COMMAND_OTA_SERVER_TO_CLIENT_IMAGE_NOTIFY             = 0x00, // Optional, Tx Not Supported
  COMMAND_OTA_SERVER_TO_CLIENT_QUERY_NEXT_IMAGE_RSP     = 0x02, // Mandatory, Tx Supported
  COMMAND_OTA_SERVER_TO_CLIENT_IMAGE_BLOCK_RSP          = 0x05, // Mandatory, Tx Supported
  COMMAND_OTA_SERVER_TO_CLIENT_UPGRADE_END_RSP          = 0x07, // Mandatory, Tx Supported
  COMMAND_OTA_SERVER_TO_CLIENT_QUERY_SPECIFIC_FILE_RSP  = 0x09, // Optional, Tx Not Supported
} Ota_Cmd_ServerToClient_t;
typedef enum
{
  COMMAND_OTA_CLIENT_TO_SERVER_QUERY_NEXT_IMAGE_REQ     = 0x01, // Mandatory, Rx Supported
  COMMAND_OTA_CLIENT_TO_SERVER_IMAGE_BLOCK_REQ          = 0x03, // Mandatory, Rx Supported
  COMMAND_OTA_CLIENT_TO_SERVER_IMAGE_PAGE_REQ           = 0x04, // Optional, Rx Not Supported
  COMMAND_OTA_CLIENT_TO_SERVER_UPGRADE_END_REQ          = 0x06, // Mandatory, Rx Supported
  COMMAND_OTA_CLIENT_TO_SERVER_QUERY_SPECIFIC_FILE_REQ  = 0x08, // Optional, Rx Not Supported
} Ota_Cmd_ClientToServer_t;

/* Prototypes required for command table */
static szl_bool SzlPlugin_OtaServer_QueryNextImageReqHandler(zabService* Service, 
                                                             szl_uint8 DestinationEndpoint, 
                                                             szl_bool manufacturerSpecific, 
                                                             szl_uint16 ClusterID, 
                                                             szl_uint8 CmdID, 
                                                             szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize, 
                                                             szl_uint8* CmdOutID, 
                                                             szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize, 
                                                             szl_uint8 TransactionId,
                                                             SZL_Addresses_t SourceAddress,
                                                             SZL_ADDRESS_MODE_t DestAddressMode);
static szl_bool SzlPlugin_OtaServer_ImageBlockReqHandler(zabService* Service, 
                                                         szl_uint8 DestinationEndpoint, 
                                                         szl_bool manufacturerSpecific, 
                                                         szl_uint16 ClusterID, 
                                                         szl_uint8 CmdID, 
                                                         szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize, 
                                                         szl_uint8* CmdOutID, 
                                                         szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize, 
                                                         szl_uint8 TransactionId,
                                                         SZL_Addresses_t SourceAddress,
                                                         SZL_ADDRESS_MODE_t DestAddressMode);
static szl_bool SzlPlugin_OtaServer_UpgradeEndReqHandler(zabService* Service, 
                                                         szl_uint8 DestinationEndpoint, 
                                                         szl_bool manufacturerSpecific, 
                                                         szl_uint16 ClusterID, 
                                                         szl_uint8 CmdID, 
                                                         szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize, 
                                                         szl_uint8* CmdOutID, 
                                                         szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize, 
                                                         szl_uint8 TransactionId,
                                                         SZL_Addresses_t SourceAddress,
                                                         SZL_ADDRESS_MODE_t DestAddressMode);

/* Cluster Commands */
static const SZL_ClusterCmd_t constClusterCommands[] = {
  {szl_false, SZL_ADDRESS_EP_ALL, ZCL_CLUSTER_ID_OTA_UPGRADE, COMMAND_OTA_CLIENT_TO_SERVER_QUERY_NEXT_IMAGE_REQ, SZL_CLU_OP_TYPE_CUSTOM_CLIENT_TO_SERVER, {.Callback = SzlPlugin_OtaServer_QueryNextImageReqHandler}},
  {szl_false, SZL_ADDRESS_EP_ALL, ZCL_CLUSTER_ID_OTA_UPGRADE, COMMAND_OTA_CLIENT_TO_SERVER_IMAGE_BLOCK_REQ, SZL_CLU_OP_TYPE_CUSTOM_CLIENT_TO_SERVER, {.Callback = SzlPlugin_OtaServer_ImageBlockReqHandler}},
  {szl_false, SZL_ADDRESS_EP_ALL, ZCL_CLUSTER_ID_OTA_UPGRADE, COMMAND_OTA_CLIENT_TO_SERVER_UPGRADE_END_REQ, SZL_CLU_OP_TYPE_CUSTOM_CLIENT_TO_SERVER, {.Callback = SzlPlugin_OtaServer_UpgradeEndReqHandler}},
};
#define CLUSTER_COMMANDS_COUNT    ( sizeof(constClusterCommands) / sizeof(SZL_ClusterCmd_t))

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/**
 * Query Next Image Request command handler
 */
static szl_bool SzlPlugin_OtaServer_QueryNextImageReqHandler(zabService* Service, 
                                                             szl_uint8 DestinationEndpoint, 
                                                             szl_bool manufacturerSpecific, 
                                                             szl_uint16 ClusterID, 
                                                             szl_uint8 CmdID, 
                                                             szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize, 
                                                             szl_uint8* CmdOutID, 
                                                             szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize, 
                                                             szl_uint8 TransactionId,
                                                             SZL_Addresses_t SourceAddress,
                                                             SZL_ADDRESS_MODE_t DestAddressMode)
{
  szl_uint8 index;
  SzlPlugin_OtaServer_Params_t * pluginParams = NULL;
  szl_bool cmdHandled = szl_false;
  szl_uint8 fieldControl;
  szl_uint16 manufacturerCode;
  szl_uint16 imageType;
  szl_uint32 currentFileVersion;
  szl_uint16 hardwareVersion;
  szl_uint8 newImageBlockLength = 0;
  szl_uint8 newImageBlock[SZL_PLUGIN_OTA_SERVER_FILE_HEADER_BLOCK_SIZE_MAX];
  SzlPlugin_OtaServer_FileHeader_t fileHeader;
  SZL_STATUS_t sts;
  szl_uint8 maxRspDataLength;
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_OTA_SERVER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return szl_false;
    }
  
  if (CmdID == COMMAND_OTA_CLIENT_TO_SERVER_QUERY_NEXT_IMAGE_REQ)
    {
      maxRspDataLength = *PayloadOutSize;
      
      if ( (pluginParams->QueryNextImageCallback != NULL) &&
           (SourceAddress.AddressMode == SZL_ADDRESS_MODE_NWK_ADDRESS) &&
           (PayloadInSize >= QUERY_NEXT_IMAGE_REQ_MIN_PAYLOAD) &&
           (maxRspDataLength >= QUERY_NEXT_IMAGE_RSP_MAX_PAYLOAD) )
        {
          /* Parse incoming request payload */
          index = 0;
          fieldControl = ZCLPayloadIn[index++];
          manufacturerCode = COPY_IN_16_BITS(&ZCLPayloadIn[index]); index+=2;
          imageType = COPY_IN_16_BITS(&ZCLPayloadIn[index]); index+=2;
          currentFileVersion = COPY_IN_32_BITS(&ZCLPayloadIn[index]); index+=4;
          if ( (fieldControl & QUERY_NEXT_IMAGE_REQ_HARDWARE_VERSION_PRESENT_FLAG) &&
               (PayloadInSize >= (QUERY_NEXT_IMAGE_REQ_MIN_PAYLOAD + sizeof(szl_uint16))) )
            {
              hardwareVersion = COPY_IN_16_BITS(&ZCLPayloadIn[index]); index+=2;
            }
          else
            {
              hardwareVersion = SZL_PLUGIN_OTA_SERVER_HARDWARE_VERSION_INVALID;
            }
          
          /* Call application handler to get response data */
          sts = pluginParams->QueryNextImageCallback(Service,
                                                     SourceAddress.Addresses.Nwk.Address,
                                                     manufacturerCode,
                                                     imageType,
                                                     currentFileVersion,
                                                     hardwareVersion,
                                                     SZL_PLUGIN_OTA_SERVER_FILE_HEADER_BLOCK_SIZE_MAX,
                                                     &newImageBlockLength,
                                                     newImageBlock);
          
          /* If we got a file header, parse it and check it's valid. If not set status to temporarily bad */
          if (sts == SZL_STATUS_SUCCESS)
            {
              if ( (newImageBlockLength < SZL_PLUGIN_OTA_SERVER_FILE_HEADER_BLOCK_SIZE_MAX) ||
                   (SzlPlugin_OtaServer_ParseFileHeader(Service,
                                                        (szl_uint32)newImageBlockLength, 
                                                        newImageBlock, 
                                                        &fileHeader) != SZL_RESULT_SUCCESS) )
                {
                printError(Service, "SZL_STATUS_OTA_NO_IMAGE_AVAILABLE %d", newImageBlockLength);
                  sts = SZL_STATUS_OTA_NO_IMAGE_AVAILABLE;
                }
            }
          
          /* Build response. Use parameters from given file just in case they are different to what was asked for */
          *CmdOutID = COMMAND_OTA_SERVER_TO_CLIENT_QUERY_NEXT_IMAGE_RSP;
          index = 0;
          ZCLPayloadOut[index++] = (szl_uint8)sts;
          if (sts == SZL_STATUS_SUCCESS)
            {
              COPY_OUT_16_BITS(&ZCLPayloadOut[index], fileHeader.ManufacturerCode); index+=2;
              COPY_OUT_16_BITS(&ZCLPayloadOut[index], fileHeader.ImageType); index+=2;
              COPY_OUT_32_BITS(&ZCLPayloadOut[index], fileHeader.FileVersion); index+=4;
              COPY_OUT_32_BITS(&ZCLPayloadOut[index], fileHeader.TotalImageSize); index+=4;
            }
          
          *PayloadOutSize = index;
          cmdHandled = szl_true;
        }
    }
  
  return cmdHandled;
}

/**
 * Image Block Request command handler
 */
static szl_bool SzlPlugin_OtaServer_ImageBlockReqHandler(zabService* Service, 
                                                         szl_uint8 DestinationEndpoint, 
                                                         szl_bool manufacturerSpecific, 
                                                         szl_uint16 ClusterID, 
                                                         szl_uint8 CmdID, 
                                                         szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize, 
                                                         szl_uint8* CmdOutID, 
                                                         szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize, 
                                                         szl_uint8 TransactionId,
                                                         SZL_Addresses_t SourceAddress,
                                                         SZL_ADDRESS_MODE_t DestAddressMode)
{
  szl_uint8 index;
  SzlPlugin_OtaServer_Params_t * pluginParams = NULL;
  szl_bool cmdHandled = szl_false;
  szl_uint8 fieldControl;
  szl_uint16 manufacturerCode;
  szl_uint16 imageType;
  szl_uint32 fileVersion;
  szl_uint32 fileOffset;
  szl_uint8 maxDataLength;
  szl_uint8 maxRspDataLength;
  SZL_STATUS_t sts;
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_OTA_SERVER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return szl_false;
    }
  
  if (CmdID == COMMAND_OTA_CLIENT_TO_SERVER_IMAGE_BLOCK_REQ)
    {
      maxRspDataLength = *PayloadOutSize;
      
      if ( (pluginParams->ImageBlockReqCallback != NULL) &&
           (SourceAddress.AddressMode == SZL_ADDRESS_MODE_NWK_ADDRESS) &&
           (PayloadInSize >= IMAGE_BLOCK_REQ_MIN_PAYLOAD) &&
           (maxRspDataLength >= IMAGE_BLOCK_RSP_MIN_PAYLOAD) )
        {
          /* Parse incoming request payload */
          index = 0;
          fieldControl = ZCLPayloadIn[index++];
          manufacturerCode = COPY_IN_16_BITS(&ZCLPayloadIn[index]); index+=2;
          imageType = COPY_IN_16_BITS(&ZCLPayloadIn[index]); index+=2;
          fileVersion = COPY_IN_32_BITS(&ZCLPayloadIn[index]); index+=4;
          fileOffset = COPY_IN_32_BITS(&ZCLPayloadIn[index]); index+=4;
          maxDataLength = ZCLPayloadIn[index++];
          
          /* For now i don't care about the optional extensions */
          (void)fieldControl;
                    
          /* Build response. Pre populate for success so we know where the data will go */
          *CmdOutID = COMMAND_OTA_SERVER_TO_CLIENT_IMAGE_BLOCK_RSP;
          index = 0;
          ZCLPayloadOut[index++] = (szl_uint8)SZL_STATUS_SUCCESS;
          COPY_OUT_16_BITS(&ZCLPayloadOut[index], manufacturerCode); index+=2;
          COPY_OUT_16_BITS(&ZCLPayloadOut[index], imageType); index+=2;
          COPY_OUT_32_BITS(&ZCLPayloadOut[index], fileVersion); index+=4;
          COPY_OUT_32_BITS(&ZCLPayloadOut[index], fileOffset); index+=4;
          
          /* Set max payload length, allowing an extra byte for the length field in the command */
          maxDataLength = MIN(maxDataLength, maxRspDataLength -(index+1));
          
          /* Call application handler to get response data */
          sts = pluginParams->ImageBlockReqCallback(Service,
                                                    SourceAddress.Addresses.Nwk.Address,
                                                    manufacturerCode,
                                                    imageType,
                                                    fileVersion,
                                                    fileOffset,
                                                    maxDataLength,
                                                    &ZCLPayloadOut[index],
                                                    &ZCLPayloadOut[index+1]);  
          /* If success sort out the new index. Otherwise put status about or forget about the reset of the payload. */
          if ( (sts == SZL_STATUS_SUCCESS) && (ZCLPayloadOut[index] <= maxDataLength) )
            {
              index += ZCLPayloadOut[index] + 1;
            }
          else // if (sts == SZL_STATUS_OTA_ABORT)
            {
              index = 0;
              ZCLPayloadOut[index++] = (szl_uint8)SZL_STATUS_OTA_ABORT;
            }
          
          *PayloadOutSize = index;
          cmdHandled = szl_true;
        }
    }
  
  return cmdHandled;
}

/**
 * Upgrade End Request command handler
 */
static szl_bool SzlPlugin_OtaServer_UpgradeEndReqHandler(zabService* Service, 
                                                         szl_uint8 DestinationEndpoint, 
                                                         szl_bool manufacturerSpecific, 
                                                         szl_uint16 ClusterID, 
                                                         szl_uint8 CmdID, 
                                                         szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize, 
                                                         szl_uint8* CmdOutID, 
                                                         szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize, 
                                                         szl_uint8 TransactionId,
                                                         SZL_Addresses_t SourceAddress,
                                                         SZL_ADDRESS_MODE_t DestAddressMode)
{
  szl_uint8 index;
  SzlPlugin_OtaServer_Params_t * pluginParams = NULL;
  szl_bool cmdHandled = szl_false;
  szl_uint16 manufacturerCode;
  szl_uint16 imageType;
  szl_uint32 fileVersion;
  szl_uint32 currentTime = 0;
  szl_uint32 upgradeTime = 0;
  SZL_STATUS_t sts;
  szl_uint8 maxRspDataLength;
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_OTA_SERVER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return szl_false;
    }
  
  if (CmdID == COMMAND_OTA_CLIENT_TO_SERVER_UPGRADE_END_REQ)
    {
      maxRspDataLength = *PayloadOutSize;
      
      if ( (pluginParams->UpgradeEndReqCallback != NULL) &&
           (SourceAddress.AddressMode == SZL_ADDRESS_MODE_NWK_ADDRESS) &&
           (PayloadInSize >= UPGRADE_END_REQ_MIN_PAYLOAD) &&
           (maxRspDataLength >= UPGRADE_END_RSP_MAX_PAYLOAD) )
        {
          /* Parse incoming request payload */
          index = 0;
          sts = (SZL_STATUS_t)ZCLPayloadIn[index++];
          manufacturerCode = COPY_IN_16_BITS(&ZCLPayloadIn[index]); index+=2;
          imageType = COPY_IN_16_BITS(&ZCLPayloadIn[index]); index+=2;
          fileVersion = COPY_IN_32_BITS(&ZCLPayloadIn[index]); index+=4;
          
          /* Call application handler to get response data */
          pluginParams->UpgradeEndReqCallback(Service,
                                              SourceAddress.Addresses.Nwk.Address,
                                              sts,
                                              manufacturerCode,
                                              imageType,
                                              fileVersion,
                                              &currentTime,
                                              &upgradeTime); 
          
          /* Build response */
          *CmdOutID = COMMAND_OTA_SERVER_TO_CLIENT_UPGRADE_END_RSP;
          index = 0;
          COPY_OUT_16_BITS(&ZCLPayloadOut[index], manufacturerCode); index+=2;
          COPY_OUT_16_BITS(&ZCLPayloadOut[index], imageType); index+=2;
          COPY_OUT_32_BITS(&ZCLPayloadOut[index], fileVersion); index+=4;
          COPY_OUT_32_BITS(&ZCLPayloadOut[index], currentTime); index+=4;
          COPY_OUT_32_BITS(&ZCLPayloadOut[index], upgradeTime); index+=4;
          
          *PayloadOutSize = index;
          cmdHandled = szl_true;
        }
    }
  
  return cmdHandled;
}
/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/  

/**
 * OTA Cluster Cluster Server - Initialization
 *
 * This function must be called from the application after the Library has been initialized.
 * 
 * @param[in]  Service                Service Instance Pointer
 * @param[in]  Endpoint               Endpoint on which cluster will be run
 * @param[in]  QueryNextImageCallback Callback function for received Query Next Image commands
 * @param[in]  ImageBlockReqCallback  Callback function for received Image Block Request commands
 * @param[in]  UpgradeEndReqCallback  Callback function for received Upgrade End Request commands
 * 
 * @return     SZL_RESULT_t           SZL_RESULT_SUCCESS if successful.
 */
SZL_RESULT_t SzlPlugin_OtaServer_Init(zabService* Service, 
                                      SZL_EP_t Endpoint,
                                      SzlPlugin_OtaServer_QueryNextImage_CB_t QueryNextImageCallback,
                                      SzlPlugin_OtaServer_ImageBlockReq_CB_t ImageBlockReqCallback,
                                      SzlPlugin_OtaServer_UpgradeEndReq_CB_t UpgradeEndReqCallback)
{
  SZL_RESULT_t result = SZL_RESULT_SUCCESS;
  szl_uint8 cmdIndex;
  SzlPlugin_OtaServer_Params_t * pluginParams = NULL;
  SZL_ClusterCmd_t clusterCommands[CLUSTER_COMMANDS_COUNT];

  /* Validate inputs */
  if ( (Service == NULL) || 
       (Endpoint == SZL_ADDRESS_EP_INVALID) || (Endpoint == SZL_ADDRESS_EP_ALL) || (Szl_EndpointRegistered(Service, Endpoint) == szl_false) ||
       (QueryNextImageCallback == NULL) || (ImageBlockReqCallback == NULL) || (UpgradeEndReqCallback == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
    
  /* Malloc then initialise parameters the plugin needs to store and link from the SZL */
  result = SZL_PluginRegister(Service, SZL_PLUGIN_ID_OTA_SERVER, SzlPlugin_OtaServer_Params_t_SIZE, (void**)&pluginParams);
  if (result == SZL_RESULT_SUCCESS)
    {
      if (pluginParams != NULL)
        {
          pluginParams->Endpoint = Endpoint;
          pluginParams->QueryNextImageCallback = QueryNextImageCallback;
          pluginParams->ImageBlockReqCallback = ImageBlockReqCallback;
          pluginParams->UpgradeEndReqCallback = UpgradeEndReqCallback;

          /* Register cluster commands for our endpoint */
          szl_memcpy(clusterCommands, constClusterCommands, sizeof(constClusterCommands));
          for (cmdIndex = 0; cmdIndex < CLUSTER_COMMANDS_COUNT; cmdIndex++)
            {
              clusterCommands[cmdIndex].Endpoint = Endpoint;
            }
          result = SZL_ClusterCmdRegister(Service, clusterCommands, CLUSTER_COMMANDS_COUNT);
          if( (result == SZL_RESULT_SUCCESS) || (result == SZL_RESULT_ALREADY_EXISTS) )
            {
              result = SZL_RESULT_SUCCESS;
            }
        }
      else
        {
          result = SZL_RESULT_INTERNAL_LIB_ERROR;
        }
    }

  if (result != SZL_RESULT_SUCCESS)
    {
      /* Failed. Cleanup */
      printError(Service, "SzlPlugin_OtaServer_Init: Failed 0x%02X\n", result);
      SZL_PluginUnregister(Service, SZL_PLUGIN_ID_OTA_SERVER);
    }

  return result;
}


/**
 * OTA Cluster Cluster Server - Destroy
 * 
 * @param[in]  Service                Service Instance Pointer
 * 
 * @return     SZL_RESULT_t           SZL_RESULT_SUCCESS if successful.
 */
SZL_RESULT_t SzlPlugin_OtaServer_Destroy(zabService* Service)
{
  SZL_RESULT_t result;
  szl_uint8 cmdIndex;
  SzlPlugin_OtaServer_Params_t* pluginParams = NULL;
  SZL_ClusterCmd_t clusterCommands[CLUSTER_COMMANDS_COUNT];
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_OTA_SERVER, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
  
  /* De-Register commands for each endpoint (setting correct endpoint in to the registration parameters) */
  szl_memcpy(clusterCommands, constClusterCommands, sizeof(constClusterCommands));
  for (cmdIndex = 0; cmdIndex < CLUSTER_COMMANDS_COUNT; cmdIndex++)
    {
      clusterCommands[cmdIndex].Endpoint = pluginParams->Endpoint;
    }
  result = SZL_ClusterCmdDeregister(Service, clusterCommands, CLUSTER_COMMANDS_COUNT);
  if(result != SZL_RESULT_SUCCESS)
    {
      /* Print error but continue to make best effort to clean everything up! */
      printError(Service, "SzlPlugin_OtaServer_Destroy: WARNING: Deregister commands failed with Result 0x%02X\n",
                 result);
    }

  /* Now unregister the plugin */
  return SZL_PluginUnregister(Service, SZL_PLUGIN_ID_OTA_SERVER);
}

/**
 * OTA Cluster Cluster Server - Parse a file header
 * 
 * @param[in]  Service        Service Instance Pointer
 * @param[in]  FileDataLength Length of FileData provided. Must be at least SZL_PLUGIN_OTA_SERVER_FILE_HEADER_BLOCK_SIZE_MAX.
 * @param[in]  FileData       Buffer containing file contents from the start of file
 * @param[out] FileHeader     Structure that will be populated with file data if SZL_RESULT_SUCCESS returned
 * 
 * @return     SZL_RESULT_t   SZL_RESULT_SUCCESS if successful.
 */
SZL_RESULT_t SzlPlugin_OtaServer_ParseFileHeader(zabService* Service,
                                                 szl_uint32 FileDataLength, 
                                                 szl_uint8* FileData, 
                                                 SzlPlugin_OtaServer_FileHeader_t* FileHeader)
{
  SZL_RESULT_t result = SZL_RESULT_SUCCESS;
  szl_uint8 index;
  szl_uint32 fileId;
  
  /* Validate inputs */
  if ( (Service == NULL) || (FileDataLength < SZL_PLUGIN_OTA_SERVER_FILE_HEADER_BLOCK_SIZE_MAX) || (FileData == NULL) || (FileHeader == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
  
  /* Parse data into native structure */
  szl_memset(FileHeader, 0, sizeof(SzlPlugin_OtaServer_FileHeader_t)); 
  index = 0;
  fileId = COPY_IN_32_BITS(&FileData[index]); index+=4;
  if (fileId == OTA_UPGRADE_FILE_IDENTIFIER)
    {
      FileHeader->OtaHeaderVersion = COPY_IN_16_BITS(&FileData[index]); index+=2;
      if (FileHeader->OtaHeaderVersion == SZL_PLUGIN_OTA_SERVER_FILE_HEADER_VERSION)
        {
          FileHeader->OtaHeaderLength = COPY_IN_16_BITS(&FileData[index]); index+=2;
          FileHeader->OtaHeaderFieldControl.OptWord = COPY_IN_16_BITS(&FileData[index]); index+=2;
          FileHeader->ManufacturerCode = COPY_IN_16_BITS(&FileData[index]); index+=2;
          FileHeader->ImageType = COPY_IN_16_BITS(&FileData[index]); index+=2;
          FileHeader->FileVersion = COPY_IN_32_BITS(&FileData[index]); index+=4;
          FileHeader->ZigBeeStackVersion = (SzlPlugin_OtaServer_ZigBeeStackVersion_t)COPY_IN_16_BITS(&FileData[index]); index+=2;
          szl_memcpy(FileHeader->OTAHeaderString, &FileData[index], SZL_PLUGIN_OTA_SERVER_FILE_HEADER_STRING_LENGTH); index+=SZL_PLUGIN_OTA_SERVER_FILE_HEADER_STRING_LENGTH;
          FileHeader->TotalImageSize = COPY_IN_32_BITS(&FileData[index]); index+=4;
          
          /* Now process optional extensions based on their control bits */
          if (FileHeader->OtaHeaderFieldControl.optBit.SecurityCredentialVersionPresent)
            {
              FileHeader->SecurityCredentialVersion = (SzlPlugin_OtaServer_SecurityCredentialVersion_t)FileData[index++];
            }
          if (FileHeader->OtaHeaderFieldControl.optBit.DeviceSpecificFile)
            {
              FileHeader->UpgradeFileDestination = COPY_IN_64_BITS(&FileData[index]); index+=8;
            }
          if (FileHeader->OtaHeaderFieldControl.optBit.HardwareVersionsPresent)
            {
              FileHeader->MinimumHardwareVersion = COPY_IN_16_BITS(&FileData[index]); index+=2;
              FileHeader->MaximumHardwareVersion = COPY_IN_16_BITS(&FileData[index]); index+=2;
            }
        }
      else
        {
          printError(Service, "SzlPlugin_OtaServer_ParseFileHeader: Unsupported Header Version 0x%04X\n", FileHeader->OtaHeaderVersion);
          result = SZL_RESULT_OUT_OF_RANGE;
        }
    }
  else
    {
      printError(Service, "SzlPlugin_OtaServer_ParseFileHeader: Invalid File Id 0x%08X\n", fileId);
      result = SZL_RESULT_INVALID_DATA;
    }
  return result;
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/