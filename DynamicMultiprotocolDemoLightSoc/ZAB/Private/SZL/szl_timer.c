/*******************************************************************************
  Filename    : szl_timer.c
  $Date       : 2013-06-25                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : This is the Timer functions for the SZL
*******************************************************************************/
/*
.  ___________.__                                                              .
.  \__    ___/|__| _____   ___________                                         .
.    |    |   |  |/     \_/ __ \_  __ \  ______                                .
.    |    |   |  |  Y Y  \  ___/|  | \/ /_____/                                .
.    |____|   |__|__|_|  /\___  >__|                                           .
.                      \/     \/                                               .
.  ___________                   __  .__                                       .
.  \_   _____/_ __  ____   _____/  |_|__| ____   ____   ______                 .
.   |    __)|  |  \/    \_/ ___\   __\  |/  _ \ /    \ /  ___/                 .
.   |     \ |  |  /   |  \  \___|  | |  (  <_> )   |  \\___ \                  .
.   \___  / |____/|___|  /\___  >__| |__|\____/|___|  /____  >                 .
.       \/             \/     \/                    \/     \/                  .

- NOTE: Timers have the resolution of one second
*/
#include <stdio.h>
#include <string.h>
#include "szl_types.h"
#include "szl.h"
#include "szl_timer.h"
#include "zabCorePrivate.h"
#include "szl_timer_types.h"

#define ZAB_SZL_TIMER( srv )    (ZAB_SRV( srv )->szlService.szlTimer)
#define EXT_TIMERS( srv )	    (ZAB_SZL_TIMER( srv ).ExtTimers)
#define BITS_TIMER_ID( srv )    (ZAB_SZL_TIMER( srv ).BitsTimerID)

// PROTOTYPES
static szl_uint8 Szl_ExtTimer_AllocateID(zabService* Service);
static void Szl_ExtTimer_FreeID(zabService* Service, szl_uint8 ID);
static szl_uint32 Szl_TimeRemaining(zabService* Service, SZL_Timer_t* timer);


// CODE
void Szl_TimersInitialize(zabService* Service)
{
    ZAB_SZL_TIMER(Service).gSecondsTimer = 0;
    szl_memset(EXT_TIMERS(Service), 0, sizeof(EXT_TIMERS(Service)));
    szl_memset(BITS_TIMER_ID(Service), 0, EXT_TIMER_ID_BYTES);
}


/* *****************************************************************************
 * External timers
 * ****************************************************************************/

/**
 * Reserve ID for external timer
 */
static szl_uint8 Szl_ExtTimer_AllocateID(zabService* Service)
{
    szl_uint8 count = 0;

    for(count = 0; count < SZL_EXTERNAL_TIMER_COUNT; count++)
    {
        if( ( (BITS_TIMER_ID(Service)[count / 8] >> (count % 8)) & 0x01) == 0)
        {
            BITS_TIMER_ID(Service)[count / 8] |= (0x01 << (count % 8));
            return count + 1; // + 1 as 0 is reserved
        }
    }

    return 0; //0 is an invalid TIMER ID
}

/**
 * Free ID for external timer
 */
static void Szl_ExtTimer_FreeID(zabService* Service, szl_uint8 ID)
{
    if( !IS_VALIDEXT_ID(ID) )
    {
        return;
    }
    ID --;
    BITS_TIMER_ID(Service)[ID / 8] &= ~(0x01 << (ID % 8));
}

/**
 * Register an external timer
 *
 * Returns: 0 if failure otherwise the ID for the timer
 */
szl_uint8 Szl_ExtTimerRegister(zabService* Service, szl_bool repeatable, szl_uint16 duration, SZL_CB_ExtTimer_t callback)
{
  szl_uint8 ID;
  
  if (callback == NULL)
        return 0;

  ID = Szl_ExtTimer_AllocateID(Service);

    if (ID)
    {
        szl_memset(&(EXT_TIMERS(Service)[ID-1]), 0, sizeof(SZL_EXT_Timer_t));
        EXT_TIMERS(Service)[ID-1].inUse = szl_true;
        EXT_TIMERS(Service)[ID-1].repeatable = repeatable;
        EXT_TIMERS(Service)[ID-1].interval = duration;
        EXT_TIMERS(Service)[ID-1].callback = callback;
    }
    else
      {
        printError(Service, "SZL Timer: WARNING - Timer Register Failed\n");
      }

    return ID;
}

/**
 * Unregister an external timer
 */
SZL_RESULT_t Szl_ExtTimerUnregister(zabService* Service, szl_uint8 ID)
{
    if (IS_VALIDEXT_ID(ID))
    {
        Szl_ExtTimer_FreeID(Service, ID);
        szl_memset(&(EXT_TIMERS(Service)[ID-1]), 0, sizeof(SZL_EXT_Timer_t));
        return SZL_RESULT_SUCCESS;
    }

    return SZL_RESULT_NOT_FOUND;
}

/**
 * Starts an external timer
 */
SZL_RESULT_t Szl_ExtTimerStart(zabService* Service, szl_uint8 ID)
{
    if (IS_VALIDEXT_ID(ID) && EXT_TIMERS(Service)[ID-1].inUse)
    {
        Szl_TimerActivate(Service, &(EXT_TIMERS(Service)[ID-1].timer), EXT_TIMERS(Service)[ID-1].interval);
        return SZL_RESULT_SUCCESS;
    }

    return SZL_RESULT_NOT_FOUND;
}

/**
 * Starts an external timer
 */
SZL_RESULT_t Szl_ExtTimerStartWithDuration(zabService* Service, szl_uint8 ID, szl_uint16 Duration)
{
    if (IS_VALIDEXT_ID(ID) && EXT_TIMERS(Service)[ID-1].inUse)
    {
        Szl_TimerActivate(Service, &(EXT_TIMERS(Service)[ID-1].timer), Duration);
        return SZL_RESULT_SUCCESS;
    }

    return SZL_RESULT_NOT_FOUND;
}

/**
 * Stops an external timer
 */
SZL_RESULT_t Szl_ExtTimerStop(zabService* Service, szl_uint8 ID)
{
    if (IS_VALIDEXT_ID(ID) && EXT_TIMERS(Service)[ID-1].inUse)
    {
        if (! EXT_TIMERS(Service)[ID-1].timer.active)
            return SZL_RESULT_OPERATION_NOT_POSSIBLE;

        Szl_TimerDeactivate(&(EXT_TIMERS(Service)[ID-1].timer));
        return SZL_RESULT_SUCCESS;
    }

    return SZL_RESULT_NOT_FOUND;
}

/**
 * check if external timer is running
 */
szl_bool Szl_ExtTimerIsRunning(zabService* Service, szl_uint8 ID)
{
    if (IS_VALIDEXT_ID(ID) && EXT_TIMERS(Service)[ID-1].inUse)
    {
        if (EXT_TIMERS(Service)[ID-1].timer.active)
        {
            return szl_true;
        }
    }

    return szl_false;
}


/**
 * return remaining duration for timer 0 if not running
 */
szl_uint16 Szl_ExtTimerRemaining(zabService* Service, szl_uint8 ID)
{
    if (IS_VALIDEXT_ID(ID) && EXT_TIMERS(Service)[ID-1].inUse)
    {
        if (EXT_TIMERS(Service)[ID-1].timer.active)
        {
            szl_uint32 elapsed = Szl_TimerElapsed(Service, &(EXT_TIMERS(Service)[ID-1].timer));

            if (elapsed < EXT_TIMERS(Service)[ID-1].timer.interval)
            {
                return EXT_TIMERS(Service)[ID-1].timer.interval - elapsed;
            }
        }
    }

    return 0xFFFF; // invalid or not in use
}

/**
 * Process all external timers and return the time to the first timer expiration
 */
szl_uint16 Szl_ExtTimerNextExpiration(zabService* Service)
{
    int i;
    szl_uint16 nextExpire = 0xFFFF;

    for (i = 0; i < SZL_EXTERNAL_TIMER_COUNT; i++)
    {
        if (EXT_TIMERS(Service)[i].inUse && EXT_TIMERS(Service)[i].timer.active)
        {
            nextExpire = MIN( nextExpire, Szl_ExtTimerRemaining(Service, i+1));
        }
    }

    return nextExpire;
}

/**
 * Process all external timers and check if expired
 */
// MvdB: Added return value for ARTF104110: Support next work time return from zabCoreWork
szl_uint32 Szl_ExtTimerProcess(zabService* Service)
{
  szl_uint32 timeoutRemainingMs = ZAB_WORK_DELAY_MAX_MS;
  int i;
    
    for (i = 0; i < SZL_EXTERNAL_TIMER_COUNT; i++)
    {
        if (EXT_TIMERS(Service)[i].inUse && EXT_TIMERS(Service)[i].timer.active)
        {
            if (Szl_TimerExpired(Service, &(EXT_TIMERS(Service)[i].timer)))
            {
                szl_uint32 elapsed = Szl_TimerElapsed(Service, &(EXT_TIMERS(Service)[i].timer));
                Szl_TimerDeactivate(&(EXT_TIMERS(Service)[i].timer));

                EXT_TIMERS(Service)[i].callback(Service, i+1, elapsed);

                if (EXT_TIMERS(Service)[i].repeatable && !EXT_TIMERS(Service)[i].timer.active)
                {
                    Szl_TimerActivate(Service, &(EXT_TIMERS(Service)[i].timer), EXT_TIMERS(Service)[i].timer.interval);
                    timeoutRemainingMs = MIN(timeoutRemainingMs, (1000 * EXT_TIMERS(Service)[i].timer.interval));
                }
            }
            else
            {
                timeoutRemainingMs = MIN(timeoutRemainingMs, (1000 * Szl_TimeRemaining(Service, &(EXT_TIMERS(Service)[i].timer))));
            }
        }
    }
  return timeoutRemainingMs;
}

/* *****************************************************************************
 * Internal timers
 * ****************************************************************************/

/**
 * Timer - Advance
 *
 * This function updates the internal timer for the library and should be called
 * no less than every one second
 */
void Szl_TimerAdvance(zabService* Service, szl_uint32 MillisecondsToAdvance)
{
    //static szl_uint32 gMillisecondsCounter = 0;   // holding milliseconds

    // add milliseconds to the remaining milliseconds from last call
    ZAB_SZL_TIMER(Service).gMillisecondsCounter += MillisecondsToAdvance;

    // since we are running out timers in seconds, we need to do some calculations...

    // 1. update the seconds timer with the amount of seconds
    ZAB_SZL_TIMER(Service).gSecondsTimer += ZAB_SZL_TIMER(Service).gMillisecondsCounter / 1000;

    // 2. remember the milliseconds until next time
    ZAB_SZL_TIMER(Service).gMillisecondsCounter %= 1000;
}

/**
 * Timer - Is Active
 *
 * Returns szl_true if timer is active, szl_false if deactivated
 */
szl_bool Szl_TimerIsActive(SZL_Timer_t* timer)
{
    return timer->active;
}


/**
 * Timer - Activate
 *
 * Activates a timer and sets the duration
 */
void Szl_TimerActivate(zabService* Service, SZL_Timer_t* timer, szl_uint16 interval)
{
    timer->active = szl_true;
    timer->interval = interval;
    timer->start = ZAB_SZL_TIMER(Service).gSecondsTimer;
}

/**
 * Timer - Deactivate
 *
 * Deactivates the timer
 */
void Szl_TimerDeactivate(SZL_Timer_t* timer)
{
    timer->active = szl_false;
}

/**
 * Timer - Expires
 *
 * Returns szl_true  - if the timer is expired,
 *         szl_false - if not or timer is inactive
 */
szl_bool Szl_TimerExpired(zabService* Service, SZL_Timer_t* timer)
{
    return (Szl_TimerElapsed(Service, timer) >= timer->interval);
}

/**
 * Timer - Elapsed
 *
 * Returns seconds elapsed by the timer.
 *               0 if timer is inactive
 */
szl_uint32 Szl_TimerElapsed(zabService* Service, SZL_Timer_t* timer)
{
    szl_uint32 elapsed = 0;

    if (timer->active == szl_true)
    {
        elapsed = Szl_TimeElapsed(Service, timer->start);
    }

    return elapsed;
}


/**
 * Time - Elapsed
 *
 * Returns seconds elapsed since startTime
 */
szl_uint32 Szl_TimeElapsed(zabService* Service, szl_uint32 startTime)
{
    szl_uint32 elapsed = 0;

    if (ZAB_SZL_TIMER(Service).gSecondsTimer < startTime)
    {
        // The internal timer has wrapped around since the timer was started.
        // calculate time before wrapping together with the time after wrapping
        elapsed = ~startTime + 1 + ZAB_SZL_TIMER(Service).gSecondsTimer;
    }
    else
    {
        // Just calculate time elapsed since start
        elapsed = ZAB_SZL_TIMER(Service).gSecondsTimer - startTime;
    }

    return elapsed;
}

/**
 * Time - Now
 *
 * Returns the internal time for the library
 */
szl_uint32 Szl_TimeNow(zabService* Service)
{
    return ZAB_SZL_TIMER(Service).gSecondsTimer;
}

/**
 * Time - Remaining
 *
 * Returns the time remaining for a timer
 */
static szl_uint32 Szl_TimeRemaining(zabService* Service, SZL_Timer_t* timer)
{
    return (timer->interval - Szl_TimerElapsed(Service, timer));
}