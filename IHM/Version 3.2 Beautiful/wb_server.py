# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to start the web server and serial management
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from bottle import route, run, hook, response, static_file
from core_engine import CoreEngine

# Web server

core_engine = CoreEngine()  # Start engine
core_engine.set_debug(True)
core_engine.start()


@hook('after_request')  # These lines are needed for avoiding the "Access-Control-Allow-Origin" errors
def enable_header():
    response.headers['Access-Control-Allow-Origin'] = '*'


# Main Page web Application
@route('/WirelessBridge')
def hello():
    filename = "index.html"
    return static_file(filename, root="../", mimetype="text/html")


# Provides access to pictures
@route('/<name>.json')
def index(name):
    filename = name + ".json"
    return static_file(filename, root="../", mimetype="text/json")


# Provides access to CSS files
@route('/css/<name>.css')
def index(name):
    filename = name + ".css"
    return static_file(filename, root="../css/", mimetype="text/css")


# Provides access to CSS files
@route('/js/jtree/themes/default/<name>.css')
def index(name):
    filename = name + ".css"
    return static_file(filename, root="../js/jtree/themes/default/", mimetype="text/css")


# Provides access to CSS files
@route('/js/jtree/themes/default/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../js/jtree/themes/default/", mimetype="text/html")


# Provides access to Script files (JavaScript)
@route('/js/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../js/", mimetype="application/javascript")


# Provides access to Script files (JavaScript)
@route('/js/jtree/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../js/jtree/", mimetype="application/javascript")


# Provides access to pictures
@route('/images/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../images/", mimetype="image/png")


# Provides access to pictures
@route('/fonts/<name>')
def index(name):
    filename = name
    return static_file(filename, root="../fonts/", mimetype="font/woff2")


run(host='localhost', port=8080, server='waitress')  # Start web server
core_engine.stop()  # Stop engine
