/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Manufacturing Library Commands/Responses - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.012  19-Jul-16   MvdB   Original
 * 000.000.014  22-Jul-16   MvdB   Add zabNcpEzspMfglib_GetPower for test
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/

#ifndef __ZAB_NCP_EZSP_MFGLIB_H__
#define __ZAB_NCP_EZSP_MFGLIB_H__

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Start Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_Start(erStatus* Status, zabService* Service);
void zabNcpEzspMfglib_StartResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * End Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_End(erStatus* Status, zabService* Service);
void zabNcpEzspMfglib_EndResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Start Tone Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_StartTone(erStatus* Status, zabService* Service);
void zabNcpEzspMfglib_StartToneResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Stop Tone Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_StopTone(erStatus* Status, zabService* Service);
void zabNcpEzspMfglib_StopToneResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Start Stream Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_StartStream(erStatus* Status, zabService* Service);
void zabNcpEzspMfglib_StartStreamResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Stop Stream Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_StopStream(erStatus* Status, zabService* Service);
void zabNcpEzspMfglib_StopStreamResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Send Packet Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_SendPacket(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);
void zabNcpEzspMfglib_SendPacketResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Set Channel Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_SetChannel(erStatus* Status, zabService* Service, unsigned8 Channel);
void zabNcpEzspMfglib_SetChannelResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Get Power Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_GetPower(erStatus* Status, zabService* Service);
void zabNcpEzspMfglib_GetPowerResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Set Power Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_SetPower(erStatus* Status, zabService* Service, signed8 Power);
void zabNcpEzspMfglib_SetPowerResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Message Received Handler
 ******************************************************************************/
void zabNcpEzspMfglib_RxHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Get CTune Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_GetCtune(erStatus* Status, zabService* Service);
void zabNcpEzspMfglib_GetCtuneResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Set CTune Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMfglib_SetCtune(erStatus* Status, zabService* Service, unsigned16 Ctune);
void zabNcpEzspMfglib_SetCtuneResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/