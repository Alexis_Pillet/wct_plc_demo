/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_main.c
*    @version
*        $Rev: 3384 $
*    @last editor
*        $Author: a5089752 $
*    @date  
*        $Date:: 2017-05-31 13:49:31 +0900#$
* Description : 
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include "r_typedefs.h"
#include "r_bsp_api.h"
#include "iorx631.h"

#include "r_fw_download.h"
#include "r_config.h"
#include "r_stdio_api.h"

/* g3 part */
#include "r_c3sap_api.h"
#include "r_byte_swap.h"
#include "r_mem_tools.h"

#include "r_timer_api.h"

/* app part */
#include "r_demo_tools.h"
#include "r_demo_app.h"
#include "r_demo_sys.h"
#include "r_demo_app_thread.h"
#include "r_demo_app_eap.h"

#include "r_demo_nvm_process.h"
#include "r_demo_main.h"

#include "plc_Gateway.h"

#if ( R_DEFINE_APP_BOOT == R_DEMO_APP_SROM_BOOT )
const uint8_t * g_cpxprogtbl = NULL;
#else
#include "g3v2017_eap_g3both_cpx3_none_3band_none_v0200_31052017.h"
#endif

/******************************************************************************
Macro definitions
******************************************************************************/
/******************************************************************************
Typedef definitions
******************************************************************************/
#define R_DEMO_DL_OFFSET_TIME               (3000)
/******************************************************************************
Exported global variables
******************************************************************************/


/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/

static const uint32_t r_demo_baud_table[] = 
{
    9600,
    19200,
    38400,
    57600,
    115200,
    230400,
    300000,
    375000,
    460800,
    500000,
    750000,
    937500,
    1000000
};


r_demo_config_t                 g_demo_config = {
                                            R_PLATFORM_TYPE_CPX3,           //r_modem_platform_type_t
                                            R_BOARD_TYPE_G_CPX3,            //r_modem_board_type_t
                                            R_G3_BANDPLAN_CENELEC_A,        //r_g3_bandplan_t
                                            R_ADP_DEVICE_TYPE_NOT_DEFINED,  //r_adp_device_type_t
                                            R_G3_ROUTE_TYPE_NORMAL,         //r_g3_route_type_t
                                            R_TRUE,                         //verboseEnabled
                                            R_FALSE,                        //macPromiscuousEnabled
                                            R_DEMO_MODE_SIMPLE,             //appMode
                                            R_G3MAC_QOS_NORMAL,             //Normal QoS
                                            R_TRUE,                         //discoverRoute
                                            0,                              //Eui64
                                            0x781D,                         //panId
                                            0,                              //coordSHort
                                            {0,},                           //PSK
                                            {0,},                           //GMK0
                                            {0,},                           //GMK1
                                            0,                              //activeKeyIndex
                                            {0,},                           //wait[4]
                                            {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,},  //tonemask[9]
                                            {0,}                            //extId
                                            };
r_demo_buff_t                   g_demo_buff;
r_demo_entity_t                 g_demo_entity;

volatile r_demo_g3_cb_str_t              g_g3cb[R_G3_CH_MAX];

/******************************************************************************
Private global variables and functions
******************************************************************************/

/******************************************************************************
Functions
******************************************************************************/

/******************************************************************************
* Function Name: R_CPX3_Boot
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t R_CPX3_Boot(const uint8_t *pCpxFw)
{
    r_sys_callback_t    callbacks;
    r_sys_boot_info_t   bootInfo;

    bootInfo.pfw = pCpxFw;
    bootInfo.dl_baud = R_SYS_BAUD_1000000;
    bootInfo.cmd_baud = R_SYS_BAUD_500000;
    bootInfo.dl_timeout = ((((sizeof(g_cpxprogtbl))*8)*1000) / (r_demo_baud_table[bootInfo.dl_baud])) + R_DEMO_DL_OFFSET_TIME;
    
    if(R_DEMO_InitSysCallBack(&callbacks) != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }

    if(R_SYS_Init(&bootInfo, &callbacks) != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_CPX3_Boot
******************************************************************************/


/******************************************************************************
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_ModemBoot
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_ModemBoot(void) 
{
    /* This is the code performing the FW download */
    /* Start firmware download. */
    R_STDIO_Printf("\n -> CPX3 Firmware boot...");
    if (R_CPX3_Boot(g_cpxprogtbl) != R_RESULT_SUCCESS)
    {
        /* Indicate error. */
        R_DEMO_LED(R_DEMO_G3_USE_PRIMARY_CH, R_DEMO_LED_ALERT);
        R_STDIO_Printf("failed!");
        return R_RESULT_FAILED;
    }
    else
    {
        /* Indicate that CPX start-up finished sucessfully. */
        R_DEMO_LED(R_DEMO_G3_USE_PRIMARY_CH, R_DEMO_LED_BOOT);
        R_STDIO_Printf("success!");
    }
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_DEMO_ModemBoot
******************************************************************************/

/******************************************************************************
* Function Name: R_DEMO_ModemReboot
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_ModemReboot(void)
{    
    R_memset((uint8_t *)&g_demo_buff, 0, sizeof(r_demo_buff_t));
    R_memset((uint8_t *)&g_demo_entity, 0, sizeof(r_demo_entity_t));

    g_demo_config.macPromiscuousEnabled = R_FALSE;

    /* Boot the modem. */
    R_DEMO_ModemBoot();
}
/******************************************************************************
   End of function  R_DEMO_ModemReboot
******************************************************************************/

/******************************************************************************
* Function Name: R_DEMO_Main
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_Main(void)
{ 
    r_apl_mode_t apl_mode;
    r_apl_mode_ch_t *pModeCh = &apl_mode.ch[R_DEMO_G3_USE_PRIMARY_CH];

    /* Boot the modem. */
    if (R_DEMO_ModemBoot() != R_RESULT_SUCCESS)
    {
        /* Indicate error. */
        R_DEMO_LED(R_DEMO_G3_USE_PRIMARY_CH, R_DEMO_LED_ALERT);
        while(1){/**/};
    }


    /* Initialize demo application thread. */
    if (R_DEMO_AppThreadInit() != R_RESULT_SUCCESS)
    {
        /* Indicate error. */
        R_DEMO_LED(R_DEMO_G3_USE_PRIMARY_CH, R_DEMO_LED_ALERT);
        while(1){/**/};
    }

#if(R_DEFINE_APP_MODE == R_DEMO_APP_MODE_DEMO)
    r_demo_nvm_read(R_NVM_COMMON_CH,R_NVM_ID_BOOT_MODE,sizeof(r_apl_mode_t),(uint8_t *)&apl_mode);
    if(R_DEMO_CheckModeBandPlan(apl_mode.bandPlan, pModeCh) != R_RESULT_SUCCESS)
    {
        R_memset(&apl_mode, 0, sizeof(r_apl_mode_t));
    }
    else
    {
        /**/
        R_memcpy(g_demo_config.tonemask,apl_mode.tonemask, 9);
    }

#else
    if(
        (apl_get_port_bit(PORT_ID_BANDPLAN_0) == 0u) &&
        (apl_get_port_bit(PORT_ID_BANDPLAN_1) == 0u))
    {
        apl_mode.bandPlan = R_G3_BANDPLAN_CENELEC_A;
    }
    else if(
        (apl_get_port_bit(PORT_ID_BANDPLAN_0) == 0u) &&
        (apl_get_port_bit(PORT_ID_BANDPLAN_1) == 1u))
    {
        apl_mode.bandPlan = R_G3_BANDPLAN_ARIB;
    }
    else if(
        (apl_get_port_bit(PORT_ID_BANDPLAN_0) == 1u) &&
        (apl_get_port_bit(PORT_ID_BANDPLAN_1) == 0u))
    {
        apl_mode.bandPlan = R_G3_BANDPLAN_FCC;
    }
    else
    {
        apl_mode.bandPlan = R_G3_BANDPLAN_CENELEC_B;
    }
    if(apl_get_port_bit(PORT_ID_DEVICE_TYPE))
    {
        pModeCh->g3mode = R_G3_MODE_EAP;
    }
    else
    {
        pModeCh->g3mode = R_G3_MODE_ADP;
    }

#if(R_DEFINE_APP_MODE == R_DEMO_APP_MODE_CERT)
    apl_mode.ch[R_DEMO_G3_USE_PRIMARY_CH].startMode = R_DEMO_MODE_CERT;
#else
    if(apl_get_port_bit(PORT_ID_AUTO_MODE))
    {
        if(apl_get_port_bit(PORT_ID_CERT_MODE))
        {
            pModeCh->startMode = R_DEMO_MODE_CERT;
        }
        else
        {
            pModeCh->startMode = R_DEMO_MODE_AUTO;
        }
    }
    else
    {
        pModeCh->startMode = R_DEMO_MODE_SIMPLE;
    }

    if(apl_get_port_bit(PORT_ID_ROUTE_TYPE))
    {
        pModeCh->routeType = R_G3_ROUTE_TYPE_JP_B;
    }
    else
    {
        pModeCh->routeType = R_G3_ROUTE_TYPE_NORMAL;
    }
#endif

#endif

    g_demo_config.bandPlan = (r_g3_bandplan_t)apl_mode.bandPlan;
    g_demo_config.appMode = (r_demo_operation_mode_t)pModeCh->startMode;
    g_demo_config.devType = (R_G3_MODE_EAP == pModeCh->g3mode)? R_ADP_DEVICE_TYPE_COORDINATOR:R_ADP_DEVICE_TYPE_PEER;
    if(R_DEMO_MODE_CERT == g_demo_config.appMode)
    {
        g_demo_config.verboseEnabled = R_FALSE;

        if(R_G3_BANDPLAN_ARIB == g_demo_config.bandPlan)
        {
            g_demo_config.routeType = R_G3_ROUTE_TYPE_JP_B;
        }
        else
        {
            g_demo_config.routeType = (r_g3_route_type_t)pModeCh->routeType;
        }
    }

    while(1){
        
        R_memset((uint8_t *)&g_demo_buff, 0, sizeof(r_demo_buff_t));
        R_memset((uint8_t *)&g_demo_entity, 0, sizeof(r_demo_entity_t));

        if(g_demo_config.appMode)
        {
            R_DEMO_AppCert();
        }
        else
        {
            /* Start demo application menu. */
        //    R_DEMO_AppMainMenu();

            PLC_Gateway();
        }
    }
}
/******************************************************************************
   End of function  R_DEMO_Main
******************************************************************************/
