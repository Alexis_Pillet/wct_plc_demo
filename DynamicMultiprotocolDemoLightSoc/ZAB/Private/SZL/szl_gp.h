/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the profile wide client commands for the ZAB implementation
 *   of the SZL GP interface - PUBLIC HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 * 00.00.06     22-Jul-14   MvdB   Original
 * 00.00.06_GP  15-Oct-14   MvdB   ARTF105853: Complete basic GP features for Smartlink IPZ
 * 002.000.003  06-Mar-15   MvdB   Remove GpAck. Code is no longer used.
 * 002.000.005  07-Apr-15   MvdB   ARTF115365: Add new API for sending GP commands - SZL_GP_CmdReq()
 * 002.000.009  17-Apr-15   MvdB   ARTF131022: Remove SZL_GP_Initialize() as it is now obsolete
 *****************************************************************************/

#ifndef _SZL_GP_H_
#define _SZL_GP_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "szl.h"
#include "szl_gp_types.h"
  
  
#ifdef SZL_CFG_ENABLE_GREEN_POWER
  
/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/**
 * Register Handler for Green Power Commissioning Indications.
 * This will be called when a GPD requests to be commissioned to ZAB.
 * The host should inspect the parameters and return:
 *  - szl_true to accept the GPD.
 *  - szl_false to reject the GPD.
 */
extern 
SZL_RESULT_t SZL_GP_GpdCommissioningNtfRegisterCB(zabService* Service, SZL_CB_GpdCommissioningNtf_t Callback);

/**
 * Register Handler for Green Power Commissioned/Decommissioned Notifications.
 * This will be called when a GPD commissioning event occurs.
 */
extern 
SZL_RESULT_t SZL_GP_GpdCommissionedNtfRegisterCB(zabService* Service, SZL_CB_GpdCommissionedNtf_t Callback);

/**
 * Green Power Command Request
 *
 * This is the function used by the application to send a generic Green Power
 * command to a Green Power Device.
 * No response is supported, so delivery cannot be guaranteed by ZAB. Applications must
 * confirm success by means specific to the command sent.
 *
 * @param[in]  Service  (@ref zabService*) Server Instance Pointer
 * @param[in]  Params   (@ref SZL_GP_CmdReqParams_t*) pointer to the parameters
 *
 * @return @ref SZL_RESULT_t
 */
SZL_RESULT_t SZL_GP_CmdReq(zabService* Service, SZL_GP_CmdReqParams_t* Params);

/******************************************************************************
 *                      ******************************
 *                ***** INTERNAL FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/*******************************************************************************
 * Data in handler for the SZL ZDO
 ******************************************************************************/
extern
void szlGp_DataInHandler(erStatus* Status, zabService* Service, sapMsg* Message);


/*******************************************************************************
 * Data in handler for GP ZCL Commands
 ******************************************************************************/
extern
void szlGp_HandleZclMsg(erStatus* Status, zabService* Service, zabMsgProDataInd* DataInd);

/**
 * @}
 */
#endif // SZL_CFG_ENABLE_GREEN_POWER
#ifdef __cplusplus
}
#endif

#endif /* _SZL_GP_H_ */

