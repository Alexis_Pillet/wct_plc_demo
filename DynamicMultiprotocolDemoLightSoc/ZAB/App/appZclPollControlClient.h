/******************************************************************************
 *                          ZAB TEST APPLICATION
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the ZCL Poll Control Cluster client functions for the test app.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 *****************************************************************************/

#ifndef _APP_ZCL_POLL_CONTROL_CLIENT_H_
#define _APP_ZCL_POLL_CONTROL_CLIENT_H_

#include "zabCoreService.h"

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

szl_bool appZclPollControlClient_CheckinNotificationHandler(zabService* Service, szl_uint8 DesintationEndpoint, szl_uint16 SourceNetworkAddress, szl_uint16* FastPollTimeout);

void appZclPollControlClient_FastPollStopRespHandler(zabService* Service, SZL_STATUS_t Status, szl_uint16 SourceNetworkAddress, szl_uint8 TransactionId);

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif /* _APP_ZCL_POLL_CONTROL_CLIENT_H_ */
