/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/******************************************************************************
* File Name     : r_flash_api_rx600.c
* Device(s)     : RX631
* Tool-chain    : KPIT GNURX-ELF 15.01
* H/W platform  : G-CPX / EU-CPX2 / G-CPX3
* Description   : Sample software
******************************************************************************/

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/
/* Intrinsic functions of MCU */
#include "CCRXmachine.h"

/* Get board and MCU definitions. */
#include "r_config.h"
#if MCU_RX631==1
#include "iorx631.h"
#else
#error "Please specify MCU type."
#endif

/* Function prototypes and device specific info needed for Flash API */
#include "r_flash_api_rx600.h"

/* Information needed for Flash API. */
#include "r_flash_api_rx600_private.h"

/******************************************************************************
   Macro definitions
******************************************************************************/

/* Define read mode macro */
#define READ_MODE    (0u)

/* Define ROM PE mode macro */
#define ROM_PE_MODE  (1u)

/* Define data flash PE mode macro */
#define FLD_PE_MODE  (2u)

/*  The number of ICLK ticks needed for 35us delay are calculated below */
#define WAIT_TRESW   (35u * (ICLK_HZ / 1000000uL))

/*  The number of ICLK ticks needed for 10us delay are calculated below */
#define WAIT_T10USEC (10u * (ICLK_HZ / 1000000uL))

/******************************************************************************
   Typedef definitions
******************************************************************************/

/* These typedefs are used for guaranteeing correct accesses to memory. When
   working with the FCU sometimes byte or word accesses are required. */
typedef  volatile uint8_t * fcu_byte_ptr_t;
typedef  volatile uint16_t* fcu_word_ptr_t;
typedef  volatile uint32_t* fcu_long_ptr_t;

/* These flash states are used internal for locking purposes. */
typedef enum
{
    FLASH_READY,
    FLASH_ERASING,
    FLASH_WRITING,
    FLASH_BLANKCHECK,
    FLASH_LOCK_BIT

} flash_states_t;

/******************************************************************************
   Exported global variables
******************************************************************************/

/******************************************************************************
   Private global variables and functions
******************************************************************************/
/* Signals whether FCU firmware has been transferred to the FCU RAM
   0 : No, 1 : Yes */
static uint8_t fcu_transfer_complete __attribute__ ((section ("FL_DATA"))) = 0u;

/* Valid values are 'READ_MODE','ROM_PE_MODE' or 'FLD_PE_MODE' */
static uint8_t current_mode __attribute__ ((section ("FL_BSS"))) ;

/* Stores whether the peripheral clock notification command has
   been executed */
static uint8_t fcu_pclk_command __attribute__ ((section ("FL_DATA")))  = 0u;

/* Semaphore for making sure flash routines do not stomp on each other */
static int32_t flash_semaphore __attribute__ ((section ("FL_DATA")))  = 0u;

/* States for flash operations */
static flash_states_t flash_state __attribute__ ((section ("FL_BSS"))) ;

/* Used for holding data when DF to DF write is peformed */
static uint8_t temp_array[ROM_PROGRAM_SIZE] __attribute__ ((section ("FL_BSS"))) ;

/* Used for telling whether a DF to DF or ROM to ROM operation is on-going */
static uint8_t flash_to_flash_op __attribute__ ((section ("FL_BSS"))) ;

/* Flash intialisation function prototype */
static uint8_t flash_init(void);

/* Enter PE mode function prototype */
static uint8_t enter_pe_mode(uint32_t flash_addr);

/* Exit PE mode function prototype */
static void exit_pe_mode(void);

/* ROM write function prototype */
static uint8_t rom_write(uint32_t address,
                         uint32_t data);

/* Data flash write function prototype */
static uint8_t data_flash_write(uint32_t address,
                                uint32_t data,
                                uint8_t size);

/* Data flash status clear function prototype */
static void data_flash_status_clear(void);

/* Notify peripheral clock function prototype */
static uint8_t notify_peripheral_clock(fcu_byte_ptr_t flash_addr);

/* FCU reset function prototype */
static void flash_reset(void);

/* Used to grab flash state */
static uint8_t flash_grab_state(flash_states_t new_state);

/* Used to release flash state */
static void flash_release_state(void);

/* Used to issue an erase command to the FCU */
static uint8_t flash_erase_command(fcu_byte_ptr_t const erase_addr);

    /* Caution. ID CODE(FFFFFFA0-FFFFFFAF) is excluded. */
const uint32_t g_flash_BlockAddresses[86] __attribute__ ((section("FL_CONST"))) = {
    0x00FFF000,    /* EB00 */
    0x00FFE000,    /* EB01 */
    0x00FFD000,    /* EB02 */
    0x00FFC000,    /* EB03 */
    0x00FFB000,    /* EB04 */
    0x00FFA000,    /* EB05 */
    0x00FF9000,    /* EB06 */
    0x00FF8000,    /* EB07 */
    0x00FF4000,    /* EB08 */
    0x00FF0000,    /* EB09 */
    0x00FEC000,    /* EB10 */
    0x00FE8000,    /* EB11 */
    0x00FE4000,    /* EB12 */
    0x00FE0000,    /* EB13 */
    0x00FDC000,    /* EB14 */
    0x00FD8000,    /* EB15 */
    0x00FD4000,    /* EB16 */
    0x00FD0000,    /* EB17 */
    0x00FCC000,    /* EB18 */
    0x00FC8000,    /* EB19 */
    0x00FC4000,    /* EB20 */
    0x00FC0000,    /* EB21 */
    0x00FBC000,    /* EB22 */
    0x00FB8000,    /* EB23 */
    0x00FB4000,    /* EB24 */
    0x00FB0000,    /* EB25 */
    0x00FAC000,    /* EB26 */
    0x00FA8000,    /* EB27 */
    0x00FA4000,    /* EB28 */
    0x00FA0000,    /* EB29 */
    0x00F9C000,    /* EB30 */
    0x00F98000,    /* EB31 */
    0x00F94000,    /* EB32 */
    0x00F90000,    /* EB33 */
    0x00F8C000,    /* EB34 */
    0x00F88000,    /* EB35 */
    0x00F84000,    /* EB36 */
    0x00F80000,    /* EB37 */
    0x00F78000,    /* EB38 */
    0x00F70000,    /* EB39 */
    0x00F68000,    /* EB40 */
    0x00F60000,    /* EB41 */
    0x00F58000,    /* EB42 */
    0x00F50000,    /* EB43 */
    0x00F48000,    /* EB44 */
    0x00F40000,    /* EB45 */
    0x00F38000,    /* EB46 */
    0x00F30000,    /* EB47 */
    0x00F28000,    /* EB48 */
    0x00F20000,    /* EB49 */
    0x00F18000,    /* EB50 */
    0x00F10000,    /* EB51 */
    0x00F08000,    /* EB52 */
    0x00F00000,    /* EB53 */
    0x00EF0000,    /* EB54 */
    0x00EE0000,    /* EB55 */
    0x00ED0000,    /* EB56 */
    0x00EC0000,    /* EB57 */
    0x00EB0000,    /* EB58 */
    0x00EA0000,    /* EB59 */
    0x00E90000,    /* EB60 */
    0x00E80000,    /* EB61 */
    0x00E70000,    /* EB62 */
    0x00E60000,    /* EB63 */
    0x00E50000,    /* EB64 */
    0x00E40000,    /* EB65 */
    0x00E30000,    /* EB66 */
    0x00E20000,    /* EB67 */
    0x00E10000,    /* EB68 */
    0x00E00000,    /* EB69 */
    0x00100000,    /* DB00 */
    0x00100800,    /* DB01 */
    0x00101000,    /* DB02 */
    0x00101800,    /* DB03 */
    0x00102000,    /* DB04 */
    0x00102800,    /* DB05 */
    0x00103000,    /* DB06 */
    0x00103800,    /* DB07 */
    0x00104000,    /* DB08 */
    0x00104800,    /* DB09 */
    0x00105000,    /* DB10 */
    0x00105800,    /* DB11 */
    0x00106000,    /* DB12 */
    0x00106800,    /* DB13 */
    0x00107000,    /* DB14 */
    0x00107800     /* DB15 */
};

/******************************************************************************
* Function Name: flash_init
* Description  : Initializes the FCU peripheral block.
*                NOTE: This function does not have to execute from in RAM.
* Arguments    : none
* Return Value : FLASH_SUCCESS -
*                    Operation Successful
*                FLASH_FAILURE -
*                    Operation Failed
******************************************************************************/
static uint8_t  __attribute__ ((section ("FL_PRG"))) flash_init(void)
{
    /* Declare source and destination pointers */
    uint32_t* psrc;
    uint32_t* pdst;

    /* Declare iteration counter variable */
    uint16_t i;

    /* Disable FCU interrupts in FCU block */
    FLASH.FAEINT.BIT.ROMAEIE  = 0;
    FLASH.FAEINT.BIT.CMDLKIE  = 0;
    FLASH.FAEINT.BIT.DFLAEIE  = 0;
    FLASH.FAEINT.BIT.DFLRPEIE = 0;
    FLASH.FAEINT.BIT.DFLWPEIE = 0;

    /* Disable FCU interrupts in ICU */

    /* Disable flash interface error (FIFERR) */
    IPR(FCU, FIFERR) = 0;
    IEN(FCU, FIFERR) = 0;

    /* Disable flash ready interrupt (FRDYI) */
    IPR(FCU, FRDYI) = 0;
    IEN(FCU, FRDYI) = 0;

    /* Transfer Firmware to the FCU RAM. To use FCU commands, the FCU firmware
        must be stored in the FCU RAM. */

    /* Before writing data to the FCU RAM, clear FENTRYR to stop the FCU. */
    if (0x0000 != FLASH.FENTRYR.WORD)
    {
        /* Disable the FCU from accepting commands - Clear both the
           FENTRY0(ROM) and FENTRYD(Data Flash) bits to 0 */
        FLASH.FENTRYR.WORD = 0xAA00;

        /* Read FENTRYR to ensure it has been set to 0. Note that the top byte
           of the FENTRYR register is not retained and is read as 0x00. */
        while (0x0000 != FLASH.FENTRYR.WORD)
        {
            /* Wait until FENTRYR is 0. */
        }
    }

    /* Enable the FCU RAM */
    FLASH.FCURAME.WORD = 0xC401;

    /* Copies the FCU firmware to the FCU RAM.
       Source: H'FEFFE000 to H'FF00000 (FCU firmware area)
       Destination: H'007F8000 to H'007FA000 (FCU RAM area) */

    /* Set source pointer */
    psrc = (uint32_t*)FCU_PRG_TOP;

    /* Set destination pointer */
    pdst = (uint32_t*)FCU_RAM_TOP;

    /* Iterate for loop to copy the FCU firmware */
    for (i = 0 ; i < (FCU_RAM_SIZE / 4) ; i++)
    {
        /* Copy data from the source to the destination pointer */
        *pdst = *psrc;

        /* Increment the source and destination pointers */
        psrc++;
        pdst++;
    }

    /* FCU firmware transfer complete, set the flag to 1 */
    fcu_transfer_complete = 1;

    /* Return no errors */
    return FLASH_SUCCESS;
}

/******************************************************************************
   End of function  flash_init
******************************************************************************/

/******************************************************************************
* Function Name: data_flash_status_clear
* Description  : Clear the status of the Data Flash operation.
*                NOTE: This function does not have to execute from in RAM.
* Arguments    : none
* Return Value : none
******************************************************************************/
static void __attribute__ ((section ("FL_PRG"))) data_flash_status_clear(void)
{
    /* Declare temporaty pointer */
    fcu_byte_ptr_t ptrb;

    /* Set pointer to Data Flash to issue a FCU command if needed */
    ptrb = (fcu_byte_ptr_t)(DF_ADDRESS);

    /* Check to see if an error has occurred with the FCU.  If set, then
       issue a status clear command to bring the FCU out of the
       command-locked state */
    if (1u == FLASH.FSTATR0.BIT.ILGLERR)
    {
        /* FASTAT must be set to 0x10 before the status clear command
           can be successfully issued  */
        if (0x10 != FLASH.FASTAT.BYTE)
        {
            /* Set the FASTAT register to 0x10 so that a status clear
               command can be issued */
            FLASH.FASTAT.BYTE = 0x10;
        }
    }

    /* Issue a status clear command to the FCU */
    *ptrb = 0x50;
}

/******************************************************************************
   End of function  data_flash_status_clear
******************************************************************************/

/******************************************************************************
* Function Name: data_flash_write
* Description  : Write either bytes to Data Flash area.
*                NOTE: This function does not have to execute from in RAM.
* Arguments    : address -
*                    The address (in the Data Flash programming area)
*                    to write the data to
*                data -
*                    The address of the data to write
*                size -
*                    The size of the data to write. Must be set to
*                    either DF_PROGRAM_SIZE_LARGE or DF_PROGRAM_SIZE_SMALL.
* Return Value : FLASH_SUCCESS -
*                    Operation Successful
*                FLASH_FAILURE -
*                    Operation Failed
******************************************************************************/
static uint8_t __attribute__ ((section ("FL_PRG"))) data_flash_write(uint32_t address,
                                uint32_t data,
                                uint8_t size)
{
    /* Declare wait counter variable */
    int32_t wait_cnt;

    /* Define loop iteration count variable */
    uint8_t n = 0;

    /* Check data size is valid */
#if defined (DF_PROGRAM_SIZE_LARGE)
    if ((DF_PROGRAM_SIZE_LARGE == size) || (DF_PROGRAM_SIZE_SMALL == size))
#else
    if (DF_PROGRAM_SIZE_SMALL == size)
#endif
    {
        /* Perform bit shift since 2 bytes are written at a time */
        size = size >> 1;

        /* Send command to data flash area */
        *(fcu_byte_ptr_t)DF_ADDRESS = 0xE8;

        /* Specify data transfer size to data flash area */
        *(fcu_byte_ptr_t)DF_ADDRESS = size;

        /* Iterate through the number of data bytes */
        while ( n < size)
        {
            n++;
            
            /* Copy data from source address to destination area */
            *(fcu_word_ptr_t)address = *(uint16_t*)data;

            /* Increment data address by two bytes */
            data += 2;
        }

        /* Write the final FCU command for programming */
        *(fcu_byte_ptr_t)(DF_ADDRESS) = 0xD0;

        /* Set the wait counter with timeout value */
        wait_cnt = WAIT_MAX_DF_WRITE;

        /* Check if FCU has completed its last operation */
        while (0u == FLASH.FSTATR0.BIT.FRDY)
        {
            /* Decrement the wait counter */
            wait_cnt--;

            /* Check if the wait counter has reached zero */
            if (0 == wait_cnt)
            {
                /* Maximum time for writing a block has passed,
                   operation failed, reset FCU */
                flash_reset();

                /* Return FLASH_FAILURE, operation failure */
                return FLASH_FAILURE;
            }
        }

        /* Check for illegal command or programming errors */
        if ((1u == FLASH.FSTATR0.BIT.ILGLERR) || (1u == FLASH.FSTATR0.BIT.PRGERR))
        {
            /* Return FLASH_FAILURE, operation failure */
            return FLASH_FAILURE;
        }
    }

    /* Data size is invalid */
    else
    {
        /*Return FLASH_FAILURE, operation failure */
        return FLASH_FAILURE;
    }

    /* Return FLASH_SUCCESS, operation success */
    return FLASH_SUCCESS;
}

/******************************************************************************
   End of function  data_flash_write
******************************************************************************/

/******************************************************************************
* Function Name: R_FLASH_DataAreaBlankCheck
* Description  : Performs a blank check on a specified data flash block
* Arguments    : address -
*                    The address to check if is blank.
*                    If the parameter 'size'=='BLANK_CHECK_8_BYTE',
*                    this should be set to an 8-byte address boundary.
*                    If the parameter 'size'=='BLANK_CHECK_ENTIRE_BLOCK',
*                    this should be set to a defined Data Block Number
*                    ('BLOCK_DB0', 'BLOCK_DB1', etc...) or an address
*                    in the data flash block.  Either option will work.
*                    If the parameter 'size'=='BLANK_CHECK_2_BYTE',
*                    this should be set to a 2 byte address boundary.
*                size -
*                    This specifies if you are checking an 8-byte location,
*                    2-byte location, or an entire block. You must set this
*                    to either 'BLANK_CHECK_8_BYTE', 'BLANK_CHECK_2_BYTE',
*                    or 'BLANK_CHECK_ENTIRE_BLOCK'.
* Return Value : FLASH_BLANK -
*                    (2 or 8 Byte check or non-BGO) Blank
*                    (Entire Block & BGO) Blank check operation started
*                FLASH_NOT_BLANK -
*                    Not Blank
*                FLASH_FAILURE -
*                    Operation Failed
*                FLASH_BUSY -
*                    Another flash operation is in progress
*                FLASH_ERROR_ADDRESS -
*                    Invalid address
*                FLASH_ERROR_BYTES -
*                    Incorrect 'size' was submitted
******************************************************************************/
uint8_t __attribute__ ((section ("FL_PRG"))) R_FLASH_DataAreaBlankCheck(uint32_t address,
                                  uint8_t size)
{
    /* Declare data flash pointer */
    fcu_byte_ptr_t ptrb;

    /* Declare result container variable */
    uint8_t result;

    /* Declare wait counter variable */
    int32_t wait_cnt;

    /* Check to make sure address is valid. */
    if (((address > (DF_NUM_BLOCKS + BLOCK_DB0)) && (address < DF_ADDRESS)) ||
        (address > (DF_ADDRESS + DF_SIZE_BYTES) ))
    {
        /* Address is not a valid DF address or block number */
        return FLASH_ERROR_ADDRESS;
    }

    /* Check to make sure 'size' parameter is valid */
    if ((BLANK_CHECK_ENTIRE_BLOCK != size) &&
#if defined (BLANK_CHECK_8_BYTE)
        (BLANK_CHECK_8_BYTE != size)
#elif defined (BLANK_CHECK_2_BYTE)
        (BLANK_CHECK_2_BYTE != size)
#endif
        )
    {
        /* 'size' parameter is not valid. */
        return FLASH_ERROR_BYTES;
    }

    /* Attempt to grab state */
    if (flash_grab_state(FLASH_BLANKCHECK) != FLASH_SUCCESS)
    {
        /* Another operation is already in progress */
        return FLASH_BUSY;
    }

    /* Set current FCU mode to Data Flash PE Mode */
    current_mode = FLD_PE_MODE;

    /* Enter Data Flash PE mode in the FCU */
    if (enter_pe_mode(address) != FLASH_SUCCESS)
    {
        /* Make sure part is in ROM read mode. */
        exit_pe_mode();

        /* Release state */
        flash_release_state();

        /* Return FLASH_FAILURE, operation failure */
        return FLASH_FAILURE;
    }

    /* Set  bit FRDMD (bit 4) in FMODR to 1 */
    FLASH.FMODR.BIT.FRDMD = 1;

#if MCU_RX631==1
    /* Check if 2 byte size has been passed */
    if (BLANK_CHECK_2_BYTE == size)
    {
        /* Set data flash pointer to beginning of the memory block */
        ptrb = (fcu_byte_ptr_t)(address & DF_MASK);

        /* Check if the next 2 bytes are blank
           Bits BCADR to the address of the 2-byte location to check.
           Set bit BCSIZE in EEPBCCNT to 0. */
        FLASH.DFLBCCNT.WORD = (uint16_t)(address & (DF_BLOCK_SIZE_LARGE - 2));
    }
#endif

    /* Check entire data block */
    else
    {
        /* Check to see if user sent in a data block number or the
           address.  The function description tells the user to send
           in the block number but in some early examples the address
           was used. To make this work with both versions we detect which
           is sent in below and make it work either way. */
        if (address < (DF_NUM_BLOCKS + BLOCK_DB0))
        {
            /* A data block number was sent in */
            ptrb = (fcu_byte_ptr_t)g_flash_BlockAddresses[address];
        }
        else
        {
            /* Any address in the erasure block */
            ptrb = (fcu_byte_ptr_t)address;
        }

        /* Check if the entire block is blank
           Set bit BCSIZE in EEPBCCNT to 1. */
        FLASH.DFLBCCNT.BIT.BCSIZE = 1;
    }

    /* Send commands to FCU */
    *ptrb = 0x71;
    *ptrb = 0xD0;

    /* Set timeout wait counter value */
    wait_cnt = WAIT_MAX_BLANK_CHECK;

    /* Wait until FCU operation finishes, or a timeout occurs */
    while (0u == FLASH.FSTATR0.BIT.FRDY)
    {
        /* Decrement the wait counter */
        wait_cnt--;

        /* Check if the wait counter has reached zero */
        if (0 == wait_cnt)
        {
            /* Maximum timeout duration for writing to ROM has elapsed -
               assume operation failure and reset the FCU */
            flash_reset();

            /* Return FLASH_FAILURE, operation failure */
            return FLASH_FAILURE;
        }
    }

    /* Reset the FRDMD bit back to 0 */
    FLASH.FMODR.BIT.FRDMD = 0x00;

    /* Check if the 'ILGERR' was set during the command */
    if (1u == FLASH.FSTATR0.BIT.ILGLERR) /* Check 'ILGERR' bit */
    {
        /* Take the FCU out of PE mode */
        exit_pe_mode();

        /* Release state */
        flash_release_state();

        /* Return FLASH_FAILURE, operation failure */
        return FLASH_FAILURE;
    }

    /* (Read the 'BCST' bit (bit 0) in the 'DFLBCSTAT' register
       0=blank, 1=not blank */
    result = FLASH.DFLBCSTAT.BIT.BCST;

    /* Take the FCU out of PE mode */
    exit_pe_mode();

    /* Release state */
    flash_release_state();

    /* Return (Not Blank/Blank), operation successful */
    if (0u == result)
    {
        /* Block was blank */
        return FLASH_BLANK;
    }
    else
    {
        /* Block was not blank */
        return FLASH_NOT_BLANK;
    }
}

/******************************************************************************
   End of function  R_FLASH_DataAreaBlankCheck
******************************************************************************/

/******************************************************************************
* Function Name: R_FLASH_DataAreaAccess
* Description  : This function is used to allow read and program permissions
*                to the Data Flash areas.
* Arguments    : read_en_mask -
*                    Bitmasked value. Bits 0-3 represents each Data
*                    Blocks 0-3 (respectively).
*                    '0'=no Read access.
*                    '1'=Allows Read by CPU
*                write_en_mask -
*                    Bitmasked value. Bits 0-3 represents each Data
*                    Blocks 0-3 (respectively).
*                    '0'=no Erase/Write access.
*                    '1'=Allows Erase/Write by FCU
* Return Value : none
******************************************************************************/
void __attribute__ ((section ("FL_PRG"))) R_FLASH_DataAreaAccess(uint16_t read_en_mask,
                           uint16_t write_en_mask)
{
#if MCU_RX631==1
    /* Set Read access for the Data Flash blocks DB0-DB7 */
    FLASH.DFLRE0.WORD = 0x2D00 | (read_en_mask & 0x00FF);

    /* Set Read access for the Data Flash blocks DB8-DB15 */
    FLASH.DFLRE1.WORD = 0xD200 | ((read_en_mask >> 8) & 0x00FF);

    /* Set Erase/Program access for the Data Flash blocks DB0-DB7 */
    FLASH.DFLWE0.WORD = 0x1E00 | (write_en_mask & 0x00FF);

    /* Set Erase/Program access for the Data Flash blocks DB8-DB15 */
    FLASH.DFLWE1.WORD = 0xE100 | ((write_en_mask >> 8) & 0x00FF);

#else
    #error "!!! You must specify your device in r_flash_api_rx6xx.h first !!!"
#endif
}

/******************************************************************************
   End of function  R_FLASH_DataAreaAccess
******************************************************************************/

/******************************************************************************
* Function Name: R_FLASH_GetStatus
* Description  : Returns the current state of the flash
* Arguments    : none
* Return Value : FLASH_SUCCESS -
*                    Flash is ready to use
*                FLASH_BUSY -
*                    Flash is busy with another operation
******************************************************************************/
uint8_t __attribute__ ((section ("FL_PRG"))) R_FLASH_GetStatus(void)
{
    /* Return flash status */
    if (FLASH_READY == flash_state)
    {
        return FLASH_SUCCESS;
    }
    else
    {
        return FLASH_BUSY;
    }
}

/******************************************************************************
   End of function  R_FLASH_GetStatus
******************************************************************************/

/******************************************************************************
* Function Name: flash_grab_state
* Description  : Attempt to grab the flash state to perform an operation
* Arguments    : new_state -
*                    Which state to attempt to transition to
* Return Value : FLASH_SUCCESS -
*                    State was grabbed
*                FLASH_BUSY -
*                    Flash is busy with another operation
******************************************************************************/
static uint8_t __attribute__ ((section ("FL_PRG"))) flash_grab_state(flash_states_t new_state)
{
    /* Variable used in trying to grab semaphore. Using the xchg instruction
       makes this atomic */
    int32_t semaphore = 1;

    /* Try to grab semaphore to change state */
    xchg((signed long*)&semaphore, (signed long*)&flash_semaphore);

    /* Check to see if semaphore was successfully taken */
    if (0 == semaphore)
    {
        /* Semaphore grabbed, we can change state */
        flash_state = new_state;

        /* Return success */
        return FLASH_SUCCESS;
    }
    else
    {
        /* Another operation is on-going */
        return FLASH_BUSY;
    }
}

/******************************************************************************
   End of function  flash_grab_state
******************************************************************************/

/******************************************************************************
* Function Name: flash_release_state
* Description  : Release state so another flash operation can take place
* Arguments    : none
* Return Value : none
******************************************************************************/
static void __attribute__ ((section ("FL_PRG"))) flash_release_state(void)
{
    /* Set current FCU mode to READ */
    current_mode = READ_MODE;

    /* Done with programming */
    flash_state = FLASH_READY;

    /* Release hold on semaphore */
    flash_semaphore = 0;
}

/******************************************************************************
   End of function  flash_release_state
******************************************************************************/

/******************************************************************************
* Function Name: rom_write
* Description  : Write bytes to ROM Area Flash.
*                NOTE: This function MUST execute from in RAM.
* Arguments    : address -
*                    ROM address of where to write to
*                data -
*                    Pointer to the data to write
* Return Value : FLASH_SUCCESS -
*                    Operation Successful
*                FLASH_FAILURE -
*                    Operation Failed
******************************************************************************/
static uint8_t __attribute__ ((section ("FL_PRG"))) rom_write(uint32_t address,
                         uint32_t data)
{
/* If ROM programming is not enabled then do not compile this code as it is
   not needed. */

    /* Declare iteration loop count variable */
    uint8_t i;

    /* Declare wait counter variable */
    int32_t wait_cnt;

    /* Number of bytes to write */
    uint32_t size = ROM_PROGRAM_SIZE;

    /* Writes are done 16-bit at a time, scale 'size' argument */
    size = ROM_PROGRAM_SIZE >> 1;

    /* Write the FCU Program command */
    *(fcu_byte_ptr_t)address = 0xE8;
    *(fcu_byte_ptr_t)address = size;

    /* Write 'size' bytes into flash, 16-bits at a time */
    for (i = 0 ; i < size ; i++)
    {
        /* Copy data from source address to destination ROM */
        *(fcu_word_ptr_t)address = *(uint16_t*)data;

        /* Increment destination address by 2 bytes */
        data += 2;
    }

    /* Write the final FCU command for programming */
    *(fcu_byte_ptr_t)address = 0xD0;

    /* Set timeout wait counter value */
    wait_cnt = WAIT_MAX_ROM_WRITE;

    /* Wait until FCU operation finishes, or a timeout occurs */
    while (0u == FLASH.FSTATR0.BIT.FRDY)
    {
        /* Decrement the wait counter */
        wait_cnt--;

        /* Check if the wait counter has reached zero */
        if (0 == wait_cnt)
        {
            /* Maximum timeout duration for writing to ROM has elapsed -
               assume operation failure and reset the FCU */
            flash_reset();

            /* Return FLASH_FAILURE, operation failure */
            return FLASH_FAILURE;
        }
    }

    /* Check for illegal command or programming errors */
    if ((1u == FLASH.FSTATR0.BIT.ILGLERR) || (1u == FLASH.FSTATR0.BIT.PRGERR))
    {
        /* Return FLASH_FAILURE, operation failure */
        return FLASH_FAILURE;
    }

    /* If ROM programming is not enabled then this function will always just
       return FLASH_SUCCESS. This is okay because this function will never
       actually be called. The API write function will return an error when
       a ROM address is entered for programming. */

    /* Return FLASH_SUCCESS, operation successful */
    return FLASH_SUCCESS;
}

/******************************************************************************
   End of function  rom_write
******************************************************************************/

/******************************************************************************
* Function Name: enter_pe_mode
* Description  : Puts the FCU into program/erase mode.
*                NOTE: This function MUST execute from in RAM for 'ROM Area'
*                programming, but if you are ONLY doing Data Flash programming,
*                this function can reside and execute in Flash.
* Arguments    : flash_addr -
*                    The programming/erasure address
*                bytes -
*                    The number of bytes you are writing (if you are writing).
* Return Value : FLASH_SUCCESS -
*                    Operation Successful
*                FLASH_FAILURE -
*                    Operation Failed
******************************************************************************/
static uint8_t __attribute__ ((section ("FL_PRG"))) enter_pe_mode(uint32_t flash_addr)
{
    /* If FCU firmware has already been transferred to FCU RAM,
       no need to do it again */
    if (!fcu_transfer_complete)
    {
        /* Initialise the FCU, and store operation resilt in result variable */
        /* Check if FCU initialisation was successful */
        if (flash_init() != FLASH_SUCCESS)
        {
            /* FCU initialisiation failed - return operation failure */
            return FLASH_FAILURE;
        }
    }

    /* FENTRYR must be 0x0000 before bit FENTRY0 or FENTRYD can be set to 1 */
    FLASH.FENTRYR.WORD = 0xAA00;

    /* Read FENTRYR to ensure it has been set to 0. Note that the top byte
       of the FENTRYR register is not retained and is read as 0x00. */
    while (0x0000 != FLASH.FENTRYR.WORD)
    {
        /* Wait until FENTRYR is 0. */
    }

    /* Check if FCU mode is set to ROM PE */
    if (ROM_PE_MODE == current_mode)
    {
        /* Disable the FRDYI interrupt */
        FLASH.FRDYIE.BIT.FRDYIE = 0;

#if MCU_RX631==1
        /* Check which area of flash this address is in */
        if (flash_addr >= ROM_AREA_0)
        {
            /* Area 0 */
            /* Enter ROM PE mode for addresses 0xFFF80000 - 0xFFFFFFFF */
            FLASH.FENTRYR.WORD = 0xAA01;
        }
        else if ((flash_addr < ROM_AREA_0) && (flash_addr >= ROM_AREA_1))
        {
            /* Area 1 */
            /* Enter ROM PE mode for addresses 0xFFF00000 - 0xFFF7FFFF */
            FLASH.FENTRYR.WORD = 0xAA02;
        }
        else if ((flash_addr < ROM_AREA_1) && (flash_addr >= ROM_AREA_2))
        {
            /* Area 2 */
            /* Enter ROM PE mode for addresses 0xFFE80000 - 0xFFEFFFFF */
            FLASH.FENTRYR.WORD = 0xAA04;
        }
        else
        {
            /* Area 3 */
            /* Enter ROM PE mode for addresses 0xFFE00000 - 0xFFE7FFFF */
            FLASH.FENTRYR.WORD = 0xAA08;
        }
#endif
    }

    /* Check if FCU mode is set to data flash PE */
    else if (FLD_PE_MODE == current_mode)
    {
        /* Disable the FRDYI interrupt */
        FLASH.FRDYIE.BIT.FRDYIE = 0;

        /* Set FENTRYD bit(Bit 7) and FKEY (B8-15 = 0xAA) */
        FLASH.FENTRYR.WORD = 0xAA80;

        /*  First clear the FCU's status before doing Data Flash programming.
           This is to clear out any previous errors that may have occured.
           For example, if you attempt to read the Data Flash area
           before you make it readable using R_FLASH_DataAreaAccess(). */
        data_flash_status_clear();

    }

    /* Catch-all for invalid FCU mode */
    else
    {
        /* Invalid value of 'current_mode' */
        return FLASH_FAILURE;
    }

    /* Enable Write/Erase of ROM/Data Flash */
    FLASH.FWEPROR.BYTE = 0x01;

    /* Check for FCU error */
    if (((1u == FLASH.FSTATR0.BIT.ILGLERR)  ||
         (1u == FLASH.FSTATR0.BIT.ERSERR)) ||
        ((1u == FLASH.FSTATR0.BIT.PRGERR)  ||
         (1u == FLASH.FSTATR1.BIT.FCUERR)))
    {
        /* Return FLASH_FAILURE, operation failure */
        return FLASH_FAILURE;
    }

    /* Check to see if peripheral clock notification command is needed */
    if (0u == fcu_pclk_command)
    {
        /* Disable FCU interrupts, so interrupt will not trigger after
           peripheral clock notification command */
        FLASH.FRDYIE.BIT.FRDYIE = 0;

        /* Inform FCU of flash clock speed, check if operation is succesful */
        if (notify_peripheral_clock((fcu_byte_ptr_t)flash_addr) != 0)
        {
            /* Return FLASH_FAILURE, operation failure */
            return FLASH_FAILURE;
        }

        /* No need to notify FCU of clock supplied to flash again */
        fcu_pclk_command = 1;
    }

    /* Return FLASH_SUCCESS, operation successful */
    return FLASH_SUCCESS;
}

/******************************************************************************
   End of function enter_pe_mode
******************************************************************************/

/******************************************************************************
* Function Name: exit_pe_mode
* Description  : Takes the FCU out of program/erase mode.
*                NOTE: This function MUST execute from in RAM for 'ROM Area'
*                programming, but if you are ONLY doing Data Flash
*                programming, this function can reside and execute in Flash.
* Arguments    : none
* Return Value : none
******************************************************************************/
static void __attribute__ ((section ("FL_PRG"))) exit_pe_mode(void)
{
    /* Declare wait timer count variable */
    int32_t wait_cnt;

    /* Declare address pointer */
    fcu_byte_ptr_t paddr;

    /* Set wait timer count duration */
    wait_cnt = WAIT_MAX_ERASE;

    /* Iterate while loop whilst FCU operation is in progress */
    while (0u == FLASH.FSTATR0.BIT.FRDY)
    {
        /* Decrement wait timer count variable */
        wait_cnt--;

        /* Check if wait timer count value has reached zero */
        if (0 == wait_cnt)
        {
            /* Timeout duration has elapsed, assuming operation failure and
               resetting the FCU */
            flash_reset();

            /* Break from the while loop prematurely */
            break;
        }
    }

    /* Check FSTATR0 and execute a status register clear command if needed */
    if (((1u == FLASH.FSTATR0.BIT.ILGLERR)  ||
         (1u ==FLASH.FSTATR0.BIT.ERSERR)) ||
        (1u == FLASH.FSTATR0.BIT.PRGERR))
    {
        /* Clear ILGLERR */
        if (1u == FLASH.FSTATR0.BIT.ILGLERR)
        {
            /* FASTAT must be set to 0x10 before the status clear command
               can be successfully issued  */
            if (0x10 != FLASH.FASTAT.BYTE)
            {
                /* Set the FASTAT register to 0x10 so that a status clear
                    command can be issued */
                FLASH.FASTAT.BYTE = 0x10;
            }
        }

        /* Issue a status register clear command to clear all error bits */
        if (ROM_PE_MODE == current_mode)
        {
            /* If there is more than one area then we need to check and make
                           sure we are sending the status register clear command
                           to the correct section. */
#if MCU_RX631==1
            /* Check to see which area we are in */
            if (1u == FLASH.FENTRYR.BIT.FENTRY0)
            {
                /* Area 0 */
                paddr = (fcu_byte_ptr_t)(ROM_AREA_0);
            }
            else if (1u == FLASH.FENTRYR.BIT.FENTRY1)
            {
                /* Area 1 */
                paddr = (fcu_byte_ptr_t)(ROM_AREA_1);
            }
            else if (1u == FLASH.FENTRYR.BIT.FENTRY2)
            {
                /* Area 2 */
                paddr = (fcu_byte_ptr_t)(ROM_AREA_2);
            }
            else
            {
                /* Area 3 */
                paddr = (fcu_byte_ptr_t)(ROM_AREA_3);
            }
#endif

            /* Send status clear command to FCU */
            *paddr = 0x50;
        }
        else
        {
            /* Set address pointer to data flash area */
            paddr = (fcu_byte_ptr_t)DF_ADDRESS;

            /* Send status clear command to FCU */
            *paddr = 0x50;
        }
    }

    /* Enter ROM Read mode */
    FLASH.FENTRYR.WORD = 0xAA00;

    /* Read FENTRYR to ensure it has been set to 0. Note that the top byte
       of the FENTRYR register is not retained and is read as 0x00. */
    while (0x0000 != FLASH.FENTRYR.WORD)
    {
        /* Wait until FENTRYR is 0. */
    }

    /* Disable the FRDYI interrupt */
    FLASH.FRDYIE.BIT.FRDYIE = 0;

    /* Flash write/erase disabled */
    FLASH.FWEPROR.BYTE = 0x02;

}

/******************************************************************************
   End of function  exit_pe_mode
******************************************************************************/

/******************************************************************************
* Function Name: R_FLASH_Erase
* Description  : Erases an entire flash block.
*                NOTE: This function MUST execute from in RAM for 'ROM Area'
*                programming, but if you are ONLY doing Data Flash programming,
*                this function can reside and execute in Flash.
* Arguments    : block -
*                    The block number to erase (BLOCK_0, BLOCK_1, etc...)
* Return Value : FLASH_SUCCESS -
*                    Operation Successful
*                FLASH_FAILURE -
*                    Operation Failed
*                FLASH_BUSY -
*                    Another flash operation is in progress
******************************************************************************/
uint8_t __attribute__ ((section ("FL_PRG"))) R_FLASH_Erase(uint8_t block)
{
    /* Declare address pointer */
    uint32_t paddr;

    /* Declare erase operation result container variable */
    uint8_t result = FLASH_SUCCESS;

    /* Attempt to grab state */
    if (flash_grab_state(FLASH_ERASING) != FLASH_SUCCESS)
    {
        /* Another operation is already in progress */
        return FLASH_BUSY;
    }

    /* Erase Command Address */
    paddr = g_flash_BlockAddresses[block];

    /* Do we want to erase a Data Flash block or ROM block? */
    if (block >= BLOCK_DB0)
    {
        /* Set current FCU mode to data flash PE */
        current_mode = FLD_PE_MODE;
    }
    else
    {
        /* Set current FCU mode to ROM PE */
        current_mode = ROM_PE_MODE;
    }

    /* Enter ROM PE mode, check if operation successful */
    if (enter_pe_mode(paddr) != FLASH_SUCCESS)
    {
        /* Make sure part is in ROM read mode. */
        exit_pe_mode();

        /* Release state */
        flash_release_state();

        /* Return FLASH_FAILURE, operation failure */
        return FLASH_FAILURE;
    }

    /* Cancel the ROM Protect feature */
    FLASH.FPROTR.WORD = 0x5501;

#if MCU_RX631==1
    /* NOTE:
       The RX631 actually has 1024 x 32 byte blocks instead of the
       16 x 2Kbyte blocks defined in r_flash_api_rx600.h. These are grouped
       into 16 blocks to make it easier for the user to delete larger sections
       of the data flash at once. The user can still delete individual blocks
       but they will need to use the new flash erase function that takes
       addresses instead of blocks. For reference, the 2KB blocks defined
       are called 'fake' blocks. */
    if (FLD_PE_MODE == current_mode)
    {
        /* Calculate how many bytes to erase */
        int32_t bytes_to_erase = g_flash_BlockAddresses[BLOCK_DB1] -
            g_flash_BlockAddresses[BLOCK_DB0];

        /* Erase real data flash blocks until the 'fake' block is erased .*/
        while (0 < bytes_to_erase)
        {
            /* Send FCU command to erase block */
            result = flash_erase_command((fcu_byte_ptr_t)paddr);

            /* Advance pointer to next block */
            paddr += DF_ERASE_BLOCK_SIZE;

            /* Subtract off bytes erased */
            bytes_to_erase -= DF_ERASE_BLOCK_SIZE;
        }
    }
    else
    {
        /* This is a ROM block erase */
        /* Send FCU command to erase block */
        result = flash_erase_command((fcu_byte_ptr_t)paddr);
    }

#else
    #error "!!! You must specify your device in r_flash_api_rx6xx.h first !!!"
#endif

    /* Leave Program/Erase Mode */
    exit_pe_mode();

    /* Release state */
    flash_release_state();

    /* Return erase result */
    return result;
}

/******************************************************************************
   End of function  R_FLASH_Erase
******************************************************************************/

#if MCU_RX631==1

/******************************************************************************
* Function Name: R_FLASH_EraseRange
* Description  : Erases blocks starting at an address and stopping when the
*                number of bytes to erase has been reached.
*                NOTE: This function is currently only for data flash blocks
*                on RX MCUs that have 32 byte erase sectors.
*                NOTE: This function MUST execute from in RAM for 'ROM Area'
*                programming, but if you are ONLY doing Data Flash programming,
*                this function can reside and execute in Flash.
* Arguments    : start_addr -
*                    The address of where to start erasing. Must be on
*                    erase boundary.
*                bytes -
*                    The numbers of bytes to erase. Must be multiple of
*                    erase size.
* Return Value : FLASH_SUCCESS -
*                    Operation Successful
*                FLASH_FAILURE -
*                    Operation Failed
*                FLASH_ERROR_ALIGNED -
*                    Flash address was not on correct boundary
*                FLASH_BUSY -
*                    Another flash operation is in progress
*                FLASH_ERROR_BYTES -
*                    Number of bytes did not match erase size
*                FLASH_ERROR_ADDRESS -
*                    Invalid address, this is only for DF
******************************************************************************/
uint8_t __attribute__ ((section ("FL_PRG"))) R_FLASH_EraseRange(uint32_t start_addr,
                          uint32_t bytes)
{
    /* Declare erase operation result container variable */
    uint8_t result = FLASH_SUCCESS;

    /* Take off upper byte since for programming/erase addresses for ROM are
                the same as read addresses except upper byte is masked off to 0's.
                Data Flash addresses are not affected. */
    start_addr &= 0x00FFFFFF;

    /* Confirm this is for data flash */
    if ((start_addr >= DF_ADDRESS) && (start_addr < (DF_ADDRESS + DF_SIZE_BYTES)))
    {
        /* Check if the number of bytes were passed is a multiple of the
           programming size for the data flash */
        if (bytes & (DF_ERASE_BLOCK_SIZE - 1))
        {
            /* Return number of bytes not a multiple of the programming size */
            return FLASH_ERROR_BYTES;
        }

        /* Check for an address on an erase boundary. */
        if (start_addr & (DF_ERASE_BLOCK_SIZE - 1))
        {
            /* Return address not on a programming boundary */
            return FLASH_ERROR_ALIGNED;
        }
    }
    else
    {
        /* This function is only for data flash blocks. */
        return FLASH_ERROR_ADDRESS;
    }

    /* Attempt to grab state */
    if (flash_grab_state(FLASH_ERASING) != FLASH_SUCCESS)
    {
        /* Another operation is already in progress */
        return FLASH_BUSY;
    }

    /* Set current FCU mode to data flash PE */
    current_mode = FLD_PE_MODE;

    /* Enter ROM PE mode, check if operation successful */
    if (enter_pe_mode(start_addr) != FLASH_SUCCESS)
    {
        /* Make sure part is in ROM read mode. */
        exit_pe_mode();

        /* Release state */
        flash_release_state();

        /* Return FLASH_FAILURE, operation failure */
        return FLASH_FAILURE;
    }

    /* Cancel the ROM Protect feature */
    FLASH.FPROTR.WORD = 0x5501;

    /* Erase real data flash blocks until the 'fake' block is erased .*/
    while (0 < bytes)
    {
        /* Send FCU command to erase block */
        result = flash_erase_command((fcu_byte_ptr_t)start_addr);

        /* Advance pointer to next block */
        start_addr += DF_ERASE_BLOCK_SIZE;

        /* Subtract off bytes erased */
        bytes -= DF_ERASE_BLOCK_SIZE;

    }

    /* Leave Program/Erase Mode */
    exit_pe_mode();

    /* Release state */
    flash_release_state();

    /* Return erase result */
    return result;
}

/******************************************************************************
   End of function  R_FLASH_EraseRange
******************************************************************************/
#endif /* MCU_RX631==1 */

/******************************************************************************
* Function Name: flash_erase_command
* Description  : Issues the FCU command to erase a flash block
*                NOTE: This function MUST execute from in RAM for 'ROM Area'
*                programming, but if you are ONLY doing Data Flash programming,
*                this function can reside and execute in Flash.
* Arguments    : erase_addr -
*                    An address in the block to be erased
* Return Value : FLASH_SUCCESS -
*                    Operation Successful
*                FLASH_FAILURE -
*                    Operation Failed
******************************************************************************/
static uint8_t __attribute__ ((section ("FL_PRG"))) flash_erase_command(fcu_byte_ptr_t const erase_addr)
{
    /* Declare timer wait count variable */
    int32_t wait_cnt;

    /* Declare erase operation result container variable */
    uint8_t result = FLASH_SUCCESS;

    /* Send the FCU Command */
    *erase_addr = 0x20;
    *erase_addr = 0xD0;

    /* Set the wait counter timeout duration */
    wait_cnt = WAIT_MAX_ERASE;

    /* Wait while FCU operation is in progress */
    while (0u == FLASH.FSTATR0.BIT.FRDY)
    {
        /* Decrement the wait counter */
        wait_cnt--;

        /* Check if the wait counter has reached zero */
        if (0 == wait_cnt)
        {
            /* Maximum timeout duration for erasing a ROM block has
               elapsed, assuming operation failure - reset the FCU */
            flash_reset();

            /* Return FLASH_FAILURE, operation failure */
            return FLASH_FAILURE;
        }
    }

    /* Check if erase operation was successful by checking
       bit 'ERSERR' (bit5) and 'ILGLERR' (bit 6) of register 'FSTATR0' */

    /* Check FCU error */
    if ((1u == FLASH.FSTATR0.BIT.ILGLERR) || (1u == FLASH.FSTATR0.BIT.ERSERR))
    {
        result = FLASH_FAILURE;
    }

    /* Return erase result */
    return result;
}

/******************************************************************************
   End of function  flash_erase_command
******************************************************************************/

/******************************************************************************
* Function Name: R_FLASH_Write
* Description  : Writes bytes into flash.
*                NOTE: This function MUST execute from in RAM for 'ROM Area'
*                programming, but if you are ONLY doing Data Flash programming,
*                this function can reside and execute in Flash.
* Arguments    : flash_addr -
*                    Flash address location to write to. This address
*                    must be on a program boundary (e.g. RX62N has
*                    256-byte ROM writes and 8-byte DF writes).
*                buffer_addr -
*                    Address location of data buffer to write into flash.
*                bytes -
*                    The number of bytes to write. You must always pass a
*                    multiple of the programming size (e.g. RX62N has
*                    256-byte ROM writes and 8-byte DF writes).
* Return Value : FLASH_SUCCESS -
*                    Operation Successful
*                FLASH_FAILURE -
*                    Operation Failed
*                FLASH_ERROR_ALIGNED -
*                    Flash address was not on correct boundary
*                FLASH_ERROR_BYTES -
*                    Number of bytes did not match programming size of ROM or DF
*                FLASH_ERROR_ADDRESS -
*                    Invalid address
*                FLASH_ERROR_BOUNDARY -
*                    (ROM) Cannot write across flash areas.
*                FLASH_BUSY -
*                    Flash is busy with another operation
******************************************************************************/
uint8_t __attribute__ ((section ("FL_PRG"))) R_FLASH_Write(uint32_t flash_addr,
                     uint32_t buffer_addr,
                     uint16_t bytes)
{
    /* Declare result container and number of bytes to write variables */
    uint8_t result = FLASH_SUCCESS;
    uint8_t num_byte_to_write;
    uint16_t i;

    /* Take off upper byte since for programming/erase addresses for ROM are
                the same as read addresses except upper byte is masked off to 0's.
                Data Flash addresses are not affected. */
    flash_addr &= 0x00FFFFFF;

    /* ROM area or Data Flash area - Check for DF first */
    if ((flash_addr >= DF_ADDRESS) && (flash_addr < (DF_ADDRESS + DF_SIZE_BYTES)))
    {
        /* Check if the number of bytes were passed is a multiple of the
           programming size for the data flash */
        if (bytes & (DF_PROGRAM_SIZE_SMALL - 1))
        {
            /* Return number of bytes not a multiple of the programming size */
            return FLASH_ERROR_BYTES;
        }

        /* Check for an address on a programming boundary. */
        if (flash_addr & (DF_PROGRAM_SIZE_SMALL - 1))
        {
            /* Return address not on a programming boundary */
            return FLASH_ERROR_ALIGNED;
        }
    }

    /* Check for ROM area */
    else if ((flash_addr >= ROM_PE_ADDR) &&
             (flash_addr < (ROM_PE_ADDR + ROM_SIZE_BYTES)))
    {
        /* Check if the number of bytes were passed is a multiple of the
           programming size for ROM */
        if (bytes & (ROM_PROGRAM_SIZE - 1))
        {
            /* Return number of bytes not a multiple of the programming size */
            return FLASH_ERROR_BYTES;
        }

        /* Check for an address on a programming boundary. */
        if (flash_addr & (ROM_PROGRAM_SIZE - 1))
        {
            /* Return address not on a ROM programming byte boundary */
            return FLASH_ERROR_ALIGNED;
        }

        /* Check for attempts to program over flash area boundaries. 
           These boundaries are defined in r_flash_api_rx600_private.h as 'ROM_AREA_#'. */
#if MCU_RX631==1
        if ((((flash_addr < (ROM_AREA_0)) && ((flash_addr + bytes) > ROM_AREA_0)) ||
             ((flash_addr < (ROM_AREA_1)) && ((flash_addr + bytes) > ROM_AREA_1))) ||
            (((flash_addr < (ROM_AREA_2)) && ((flash_addr + bytes) > ROM_AREA_2)) ||
             ((flash_addr < (ROM_AREA_3)) && ((flash_addr + bytes) > ROM_AREA_3))))
        {
            /* Return cannot write across a flash area boundary */
            return FLASH_ERROR_BOUNDARY;
        }

#endif /* MCU_RX631==1 */
    }
    else
    {
        /* Return invalid flash address */
        return FLASH_ERROR_ADDRESS;
    }

    /* Attempt to grab state */
    if (flash_grab_state(FLASH_WRITING) != FLASH_SUCCESS)
    {
        /* Another operation is already in progress */
        return FLASH_BUSY;
    }

    /* Do we want to program a DF area or ROM area? */
    if (flash_addr < g_flash_BlockAddresses[ROM_NUM_BLOCKS - 1])
    {
        /* Set current FCU mode to data flash PE */
        current_mode = FLD_PE_MODE;
    }
    else
    {
        /* Set FCU to ROM PE mode */
        current_mode = ROM_PE_MODE;
    }

    /* Are we doing a ROM to ROM or DF to DF transfer? */
    if ((buffer_addr >= ROM_PE_ADDR) ||
        ((buffer_addr >= DF_ADDRESS) && (buffer_addr < (DF_ADDRESS + DF_SIZE_BYTES))))
    {
        /* Performing ROM to ROM, DF to ROM, ROM to DF, or DF to DF tranbsfer */
        if (ROM_PE_MODE == current_mode)
        {
            /* Performing ROM to ROM or DF to ROM */

            /* Copy over first programming 'page' */
            for (i = 0 ; i < ROM_PROGRAM_SIZE ; i++)
            {
                /* Copy over each byte */
                temp_array[i] = *((uint8_t*)(buffer_addr + i));
            }

            /* Check size */
            if (ROM_PROGRAM_SIZE == bytes)
            {
                /* Since we were able to put the entire buffer in RAM we can
                   just change the buffer pointer and go on as usual. */
                flash_to_flash_op = 0;

                /* Set new buffer address */
                buffer_addr = (uint32_t)&temp_array[0];
            }
            else
            {
                /* Set FLASH_TO_FLASH flag */
                flash_to_flash_op = 1;
            }
        }
        else
        {
            /* Performing DF to DF or ROM to DF transfer */

            /* Check if address is on a data flash programming boundary, and
               there is at least 1 'page' remaining to write. */

            /* Not all MCUs have two programming sizes for the data flash. If a
               MCU only has 1 then DF_PROGRAM_SIZE_SMALL is defined and not
               DF_PROGRAM_SIZE_LARGE. */
    #if defined (DF_PROGRAM_SIZE_LARGE)
            if ((bytes >= DF_PROGRAM_SIZE_LARGE) &&
                ((flash_addr & (DF_PROGRAM_SIZE_LARGE - 1)) == 0x00))
            {
                /* Use large write size since it's more time efficient */
                num_byte_to_write = DF_PROGRAM_SIZE_LARGE;
            }
            else
            {
                /* Cannot use large program size, use small */
                num_byte_to_write = DF_PROGRAM_SIZE_SMALL;
            }

    #else

            /* Set the next data transfer size to be DF_PROGRAM_SIZE_SMALL */
            num_byte_to_write = DF_PROGRAM_SIZE_SMALL;
    #endif

            /* Copy over first buffer */
            for (i = 0 ; i < num_byte_to_write ; i++)
            {
                /* Copy over each byte */
                temp_array[i] = *((uint8_t*)(buffer_addr + i));
            }

            /* Check size */
            if (bytes == num_byte_to_write)
            {
                /* Since we were able to put the entire buffer in RAM we can
                   just change the buffer pointer and go on as usual. */
                flash_to_flash_op = 0;

                /* Set new buffer address */
                buffer_addr = (uint32_t)&temp_array[0];
            }
            else
            {
                /* Set FLASH_TO_FLASH flag */
                flash_to_flash_op = 1;
            }
        }
    }
    else
    {
        /* No need to buffer */
        flash_to_flash_op = 0;
    }

    /* Enter PE mode, check if operation is successful */
    if (enter_pe_mode(flash_addr) != FLASH_SUCCESS)
    {
        /* Make sure part is in ROM read mode. */
        exit_pe_mode();

        /* Release state */
        flash_release_state();

        /* Return operation failure */
        return FLASH_FAILURE;
    }

    /* Cancel the ROM Protect feature */
    FLASH.FPROTR.WORD = 0x5501;

    /* Check if FCU mode is set to data flash PE */
    if (FLD_PE_MODE == current_mode)
    {
        /* Iterate while there are still bytes remaining to write */
        while (bytes)
        {
            /* Check if address is on a data flash programming boundary, and
               there is at least 1 'page' remaining to write. */

            /* Not all MCUs have two programming sizes for the data flash. If a
               MCU only has 1 then DF_PROGRAM_SIZE_SMALL is defined and not
               DF_PROGRAM_SIZE_LARGE. */
#if defined (DF_PROGRAM_SIZE_LARGE)
            if ((bytes >= DF_PROGRAM_SIZE_LARGE) &&
                ((flash_addr & (DF_PROGRAM_SIZE_LARGE - 1)) == 0x00))
            {
                /* Use large write size since it's more time efficient */
                num_byte_to_write = DF_PROGRAM_SIZE_LARGE;
            }
            else
            {
                /* Cannot use large program size, use small */
                num_byte_to_write = DF_PROGRAM_SIZE_SMALL;
            }

#else

            /* Set the next data transfer size to be DF_PROGRAM_SIZE_SMALL */
            num_byte_to_write = DF_PROGRAM_SIZE_SMALL;
#endif
            /* Call the Programming function, store the operation status in the
               container variable, result */
            if (1u == flash_to_flash_op)
            {
                result = data_flash_write(flash_addr, (uint32_t)&temp_array[0],
                                          num_byte_to_write);
            }
            else
            {
                result = data_flash_write(flash_addr, buffer_addr,
                                          num_byte_to_write);
            }


            /* Check the container variable result for errors */
            if (FLASH_SUCCESS != result)
            {
                /* Data flash write error detected, break from flash write
                   while loop prematurely */
                break;
            }

            /* Increment the flash address and the buffer address by the size
               of the transfer thats just completed */
            flash_addr  += num_byte_to_write;
            buffer_addr += num_byte_to_write;

            /* Decrement the number of bytes remaining by the size of the last
               flash write */
            bytes -= num_byte_to_write;

            /* Check to see if we need to buffer more data */
            if ((bytes > 0) &&
                (1u == flash_to_flash_op))
            {
                /* We must leave PE mode to transfer next buffer to RAM */
                exit_pe_mode();

                /* What's the maximum number of bytes we can program next
                   iteration? */
        #if defined (DF_PROGRAM_SIZE_LARGE)
                if ((bytes >= DF_PROGRAM_SIZE_LARGE) &&
                    ((flash_addr & (DF_PROGRAM_SIZE_LARGE - 1)) == 0))
                {
                    /* Use large write size since it's more time efficient */
                    num_byte_to_write = DF_PROGRAM_SIZE_LARGE;
                }
                else
                {
                    /* Cannot use large program size, use small */
                    num_byte_to_write = DF_PROGRAM_SIZE_SMALL;
                }

        #else

                /* Set the next data transfer size to be DF_PROGRAM_SIZE_SMALL*/
                num_byte_to_write = DF_PROGRAM_SIZE_SMALL;
        #endif

                /* Copy over up to next bytes */
                for (i = 0 ; i < num_byte_to_write ; i++)
                {
                    /* Copy over each byte */
                    temp_array[i] = *((uint8_t*)(buffer_addr + i));
                }

                /* Re-enter PE mode, check if operation is successful */
                if (enter_pe_mode(flash_addr) != FLASH_SUCCESS)
                {
                    /* Make sure part is in ROM read mode. */
                    exit_pe_mode();

                    /* Release state */
                    flash_release_state();

                    /* Return operation failure */
                    return FLASH_FAILURE;
                }
            }
        }
    }

    /* Catch-all - FCU mode must be ROM PE */
    else
    {
        /* Iterate loop while there are still bytes remaining to write */
        while (bytes)
        {
            /* Call the Programming function */
            if (1u == flash_to_flash_op)
            {
                /* Use RAM array */
                result = rom_write(flash_addr, (uint32_t)&temp_array[0]);
            }
            else
            {
                /* Go as usual */
                result = rom_write(flash_addr, buffer_addr);
            }


            /* Check for errors */
            if (FLASH_SUCCESS != result)
            {
                /* Stop programming */
                break;
            }

            /* Increment the flash address and the buffer address by the size
               of the transfer thats just completed */
            flash_addr  += ROM_PROGRAM_SIZE;
            buffer_addr += ROM_PROGRAM_SIZE;

            /* Decrement the number of bytes remaining by the size of the last
               flash write */
            bytes -= ROM_PROGRAM_SIZE;

            /* Check to see if we need to buffer more data */
            if ((bytes > 0) &&
                (1u == flash_to_flash_op))
            {
                /* We must leave PE mode to transfer next buffer to RAM */
                exit_pe_mode();

                /* Copy over next ROM_PROGRAM_SIZE bytes */
                for (i = 0 ; i < ROM_PROGRAM_SIZE ; i++)
                {
                    /* Copy over each byte */
                    temp_array[i] = *((uint8_t*)(buffer_addr + i));
                }

                /* Re-enter PE mode, check if operation is successful */
                if (enter_pe_mode(flash_addr) != FLASH_SUCCESS)
                {
                    /* Make sure part is in ROM read mode. */
                    exit_pe_mode();

                    /* Release state */
                    flash_release_state();

                    /* Return operation failure */
                    return FLASH_FAILURE;
                }
            }
        }
    }

    /* Leave Program/Erase Mode */
    exit_pe_mode();

    /* Release state */
    flash_release_state();

    /* Return flash programming result */
    return result;
}

/******************************************************************************
   End of function  R_FLASH_Write
******************************************************************************/

/******************************************************************************
* Function Name: notify_peripheral_clock
* Description  : Notifies FCU or clock supplied to flash unit
*                NOTE: This function MUST execute from in RAM for 'ROM Area'
*                programming, but if you are ONLY doing Data Flash programming,
*                this function can reside and execute in Flash.
* Arguments    : flash_addr -
*                    Flash address you will be erasing or writing to
* Return Value : FLASH_SUCCESS -
*                    Operation Successful
*                FLASH_FAILURE -
*                    Operation Failed
******************************************************************************/
static uint8_t __attribute__ ((section ("FL_PRG"))) notify_peripheral_clock(fcu_byte_ptr_t flash_addr)
{
    /* Declare wait counter variable */
    int32_t wait_cnt;

    /* Notify Peripheral Clock(PCK) */
    /* Set frequency of PCK in MHz */
    FLASH.PCKAR.WORD = (FLASH_CLOCK_HZ / 1000000);

    /* Execute Peripheral Clock Notification Commands */
    *flash_addr               = 0xE9;
    *flash_addr               = 0x03;
    *(fcu_word_ptr_t)flash_addr = 0x0F0F;
    *(fcu_word_ptr_t)flash_addr = 0x0F0F;
    *(fcu_word_ptr_t)flash_addr = 0x0F0F;
    *flash_addr               = 0xD0;

    /* Set timeout wait duration */
    wait_cnt = WAIT_MAX_NOTIFY_FCU_CLOCK;

    /* Check FRDY */
    while (0u == FLASH.FSTATR0.BIT.FRDY)
    {
        /* Decrement wait counter */
        wait_cnt--;

        /* Check if wait counter has reached zero */
        if (0 == wait_cnt)
        {
            /* Timeout duration elapsed, assuming operation failure - Reset
               FCU */
            flash_reset();

            /* Return FLASH_FAILURE, operation failure  */
            return FLASH_FAILURE;
        }
    }

    /* Check ILGLERR */
    if (1u == FLASH.FSTATR0.BIT.ILGLERR)
    {
        /* Return FLASH_FAILURE, operation failure*/
        return FLASH_FAILURE;
    }

    /* Return FLASH_SUCCESS, operation success */
    return FLASH_SUCCESS;
}

/******************************************************************************
   End of function notify_peripheral_clock
******************************************************************************/

/******************************************************************************
* Function Name: flash_reset
* Description  : Reset the FCU.
*                NOTE: This function MUST execute from in RAM for 'ROM Area'
*                programming, but if you are ONLY doing Data Flash
*                programming, this function can reside and execute in Flash.
* Arguments    : none
* Return Value : none
******************************************************************************/
static void __attribute__ ((section ("FL_PRG"))) flash_reset(void)
{
    /* Declare wait counter variable */
    int32_t wait_cnt;

    /* Reset the FCU */
    FLASH.FRESETR.WORD = 0xCC01;

    /* Give FCU time to reset */
    wait_cnt = WAIT_TRESW;

    /* Wait until the timer reaches zero */
    while (0 != wait_cnt)
    {
        /* Decrement the timer count each iteration */
        wait_cnt--;
    }

    /* Release state */
    flash_release_state();

    /* FCU is not reset anymore */
    FLASH.FRESETR.WORD = 0xCC00;

    /* Enter ROM Read mode */
    FLASH.FENTRYR.WORD = 0xAA00;

    /* Read FENTRYR to ensure it has been set to 0. Note that the top byte
       of the FENTRYR register is not retained and is read as 0x00. */
    while (0x0000 != FLASH.FENTRYR.WORD)
    {
        /* Wait until FENTRYR is 0. */
    }

    /* Flash write/erase disabled */
    FLASH.FWEPROR.BYTE = 0x02;
}

/******************************************************************************
   End of function  flash_reset
******************************************************************************/
