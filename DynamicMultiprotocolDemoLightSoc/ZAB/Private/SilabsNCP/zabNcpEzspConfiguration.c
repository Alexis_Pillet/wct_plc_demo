/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Configuration Commands/Responses
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.012  19-Jul-16   MvdB   Move utilities functions into zabNcpEzspUtilities
 * 000.000.015  25-Jul-16   MvdB   Support multiple endpoints
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 * 002.002.039  17-Jan-17   MvdB   ARTF175875: Support policy setting in Nwk init
 *****************************************************************************/

#include "zabNcpPrivate.h"
#include "zabNcpService.h"
#include "zabNcpEzsp.h"
#include "zabNcpEzspConfiguration.h"
#include "zabNcpOpen.h"
#include "zabNcpNwk.h"

#include "ezsp-enum.h"
#include "ezsp-enum-decode.h"
#include "ezsp-protocol.h"

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/




/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Version Request + Response Handler
 ******************************************************************************/
void zabNcpEzspConfiguration_Version(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_VERSION, EZSP_PROTOCOL_VERSION};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspConfiguration_VersionResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 4)
    {
      printVendor(Service, "EZSP_VERSION: Protocol Version: 0x%02X, Stack Type: 0x%02X, Stack Version: 0x%04X\n",
                  PayloadData[0],
                  PayloadData[1],
                  COPY_IN_16_BITS(&PayloadData[2]));

      zabNcpOpen_VersionHandler(Status, Service, COPY_IN_16_BITS(&PayloadData[2]));
    }
}

/******************************************************************************
 * Get Config Value Request + Response Handler
 ******************************************************************************/
void zabNcpEzspConfiguration_GetConfigValue(erStatus* Status, zabService* Service, EzspConfigId ConfigId)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH+1] = {0/*seq*/, 0/*FrameControl*/, EZSP_GET_CONFIGURATION_VALUE};
  unsigned8 index = EZSP_PARAMETERS_INDEX;

  printVendor(Service, "EZSP_GET_CONFIGURATION_VALUE: %s\n", decodeEzspConfigId(ConfigId));

  cmd[index++] = ConfigId;

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspConfiguration_GetConfigValueResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned16 ConfigValue;

  if (PayloadLength >= 3)
    {
      ConfigValue = COPY_IN_16_BITS(&PayloadData[1]);
      printVendor(Service, "EZSP_GET_CONFIGURATION_VALUE: Status = %s (0x%02X), Value = 0x%04X\n",
                  decodeEzspStatus(PayloadData[0]),
                  PayloadData[0],
                  ConfigValue);
    }
}

/******************************************************************************
 * Set Config Value Request + Response Handler
 ******************************************************************************/
void zabNcpEzspConfiguration_SetConfigValue(erStatus* Status, zabService* Service, EzspConfigId ConfigId, unsigned16 ConfigValue)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH+3] = {0/*seq*/, 0/*FrameControl*/, EZSP_SET_CONFIGURATION_VALUE};
  unsigned8 index = EZSP_PARAMETERS_INDEX;

  printVendor(Service, "EZSP_SET_CONFIGURATION_VALUE: %s, Value = 0x%04X\n",
              decodeEzspConfigId(ConfigId),
              ConfigValue);

  cmd[index++] = ConfigId;
  COPY_OUT_16_BITS(&cmd[index], ConfigValue); index+=2;

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspConfiguration_SetConfigValueResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_SET_CONFIGURATION_VALUE: Status = %s (0x%02X)\n",
                  decodeEzspStatus(PayloadData[0]),
                  PayloadData[0]);

      zabNcpNwk_SetConfigurationResponseHandler(Status, Service, PayloadData[0]);
    }
}

/******************************************************************************
 * Get Value / Extended Value Request + Response Handler
 ******************************************************************************/
void zabNcpEzspConfiguration_GetValue(erStatus* Status, zabService* Service, EzspValueId ValueId)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH+1] = {0/*seq*/, 0/*FrameControl*/, EZSP_GET_VALUE};
  unsigned8 index = EZSP_PARAMETERS_INDEX;

  printVendor(Service, "EZSP_GET_VALUE: %s\n", decodeEzspValueId(ValueId));

  cmd[index++] = ValueId;

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspConfiguration_GetExtendedValue(erStatus* Status, zabService* Service, EzspValueId ValueId, unsigned32 Characteristics)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH+5] = {0/*seq*/, 0/*FrameControl*/, EZSP_GET_EXTENDED_VALUE};
  unsigned8 index = EZSP_PARAMETERS_INDEX;

  printVendor(Service, "EZSP_GET_EXTENDED_VALUE: %s, 0x%08X\n",
              decodeEzspValueId(ValueId),
              Characteristics);

  cmd[index++] = ValueId;
  COPY_OUT_32_BITS(&cmd[index], Characteristics); index+=4;

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspConfiguration_GetValueResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EzspStatus ezspStatus;
  unsigned8 length = 0;
  unsigned8 index = 0;
  unsigned8* data = NULL;

  if (PayloadLength > 0)
    {
      ezspStatus = PayloadData[index++];

      if ( (ezspStatus == EZSP_SUCCESS) && (PayloadLength > 1) )
        {
          length = PayloadData[index++];

          if (PayloadLength >= (length+index))
            {
              data = &PayloadData[index];
            }
          else
            {
              length = 0;
            }
        }

      printVendor(Service, "EZSP_GET_VALUE: Status = %s (0x%02X), Length = %d, Data = ",
                  decodeEzspStatus(ezspStatus), ezspStatus,
                  length);
      for (index = 0; index < length; index++)
        {
          printVendor(Service, "%02X", data[index]);
        }
      printVendor(Service, "\n");
    }
}

/******************************************************************************
 * Add Endpoint Request + Response Handler
 * Note: We are handling the endpoints in ZAB now, so this function does not send a command.
 * It just registers the endpoint with the ZAB vendor.
 ******************************************************************************/
void zabNcpEzspConfiguration_AddEndpoint(erStatus* Status, zabService* Service, sapMsg *appMsg)
{

#define SILABS_USE_LOCAL_SIMPLE_DESC
#ifdef SILABS_USE_LOCAL_SIMPLE_DESC
  ER_CHECK_STATUS(Status);
  unsigned16 length = sapMsgGetAppDataLength(appMsg);
  zabMsgProSimpleDescriptor* simpleDesc = (zabMsgProSimpleDescriptor*)sapMsgGetAppData(appMsg);
  zabMsgProSimpleDescriptor* newSimpleDesc = NULL;
  unsigned8 index;
  unsigned8 firstUnusedIndex = ZAB_VND_CFG_MAX_ENDPOINTS;

  /* Find endpoint if it already exists, or first unused index */
  for (index = 0; index < ZAB_VND_CFG_MAX_ENDPOINTS; index++)
    {
      if (ZAB_SERVICE_VENDOR(Service)->simpleDesc[index] != NULL)
        {
          if (ZAB_SERVICE_VENDOR(Service)->simpleDesc[index]->endpoint == simpleDesc->endpoint)
            {
              break;
            }
        }
      else if (firstUnusedIndex >= ZAB_VND_CFG_MAX_ENDPOINTS)
        {
          firstUnusedIndex = index;
        }
    }
  /* If we didn't find the endpoint already, then switch to first unused*/
  if (index >= ZAB_VND_CFG_MAX_ENDPOINTS)
    {
      index = firstUnusedIndex;
    }

  if (index < ZAB_VND_CFG_MAX_ENDPOINTS)
    {
      newSimpleDesc = OS_MEM_MALLOC(Status,
                                    srvCoreGetServiceId(Status, Service),
                                    length,
                                    MALLOC_ID_CFG_SIMPLE_DESC);
      if (newSimpleDesc != NULL)
        {
          osMemCopy( Status, (unsigned8*)newSimpleDesc, (unsigned8*)simpleDesc, length );
          if (ZAB_SERVICE_VENDOR(Service)->simpleDesc[index] != NULL)
            {
              OS_MEM_FREE(Status, srvCoreGetServiceId(Status, Service), ZAB_SERVICE_VENDOR(Service)->simpleDesc[index]);
            }
          ZAB_SERVICE_VENDOR(Service)->simpleDesc[index] = newSimpleDesc;
        }
      else
        {
          erStatusSet(Status, OS_ERROR_MEM_NONE);
        }
    }
  else
    {
      printError(Service, "zabNcpEzspConfiguration_AddEndpoint: All endpoints used\n");
      erStatusSet(Status, ZAB_ERROR_CFG_INVALID);
    }


#else //SILABS_USE_LOCAL_SIMPLE_DESC

  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH+8+(ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT*sizeof(unsigned16))] = {0/*seq*/, 0/*FrameControl*/, EZSP_ADD_ENDPOINT};
  unsigned8 index = EZSP_PARAMETERS_INDEX;
  unsigned8 i;
  unsigned8 numInClusters;
  unsigned8 numOutCluster;
  ER_CHECK_STATUS(Status);

  ER_CHECK_STATUS_NULL(Status, simpleDesc);
  printVendor(Service, "zabNcpEzspConfiguration_AddEndpoint: Register Endpoint 0x%02X, DevID 0x%02X\n",
              simpleDesc->endpoint,
              simpleDesc->deviceId);

  cmd[index++] = simpleDesc->endpoint; // ep
  COPY_OUT_16_BITS(&cmd[index], simpleDesc->profileId); index+=2;
  COPY_OUT_16_BITS(&cmd[index], simpleDesc->deviceId); index+=2;
  cmd[index++] = 0; // AppFlags

  /* Validate limits on cluster list and trim if neccessary */
  if ( (simpleDesc->numInClusters + simpleDesc->numOutClusters) <= ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT)
    {
      numInClusters = simpleDesc->numInClusters;
      numOutCluster = simpleDesc->numOutClusters;
    }
  else if (simpleDesc->numInClusters  <= ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT)
    {
      numInClusters = simpleDesc->numInClusters;
      numOutCluster = (ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT - simpleDesc->numInClusters);
    }
  else
    {
      numInClusters = ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT;
      numOutCluster = 0;
    }
  cmd[index++] = numInClusters; // in Cluster Count
  cmd[index++] = numOutCluster; // outClusterCount


  for (i = 0; i < (numInClusters + numOutCluster); i++)
    {
      COPY_OUT_16_BITS(&cmd[index], simpleDesc->clusterList[i]); index+=2;
    }

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, index, cmd);
#endif // SILABS_USE_LOCAL_SIMPLE_DESC
}


/******************************************************************************
 * Set Policy Request + Response Handler
 ******************************************************************************/
void zabNcpEzspConfiguration_SetPolicy(erStatus* Status, zabService* Service, EzspPolicyId PolicyId, EzspDecisionId DecisionId)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH+2] = {0/*seq*/, 0/*FrameControl*/, EZSP_SET_POLICY};
  unsigned8 index = EZSP_PARAMETERS_INDEX;

  printVendor(Service, "EZSP_SET_POLICY: Policy: %s, Decision: %s\n",
              decodeEzspPolicyId(PolicyId),
              decodeEzspDecisionId(DecisionId));

  cmd[index++] = PolicyId;
  cmd[index++] = DecisionId;

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspConfiguration_SetPolicyResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_SET_POLICY: Status: %s (0x%02X)\n",
                  decodeEmberStatus(PayloadData[0]),
                  PayloadData[0]);

      zabNcpNwk_SetPolicyResponseHandler(Status, Service, PayloadData[0]);
    }
}


/******************************************************************************
 * Launch Bootloader Request + Response Handler
 ******************************************************************************/
void zabNcpEzspConfiguration_LaunchStandaloneBootloader(erStatus* Status, zabService* Service)
{
  #define STANDALONE_BOOTLOADER_NORMAL_MODE   1
  unsigned8 cmd[] = {0/*seq*/, 0/*FrameControl*/, EZSP_LAUNCH_STANDALONE_BOOTLOADER, STANDALONE_BOOTLOADER_NORMAL_MODE};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspConfiguration_LaunchStandaloneBootloaderResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_LAUNCH_STANDALONE_BOOTLOADER: Status: %s (0x%02X)\n",
               decodeEmberStatus(PayloadData[0]),
               PayloadData[0]);
      zabNcpOpen_LaunchStandaloneBootloaderResponseHandler(Status,
                                                           Service,
                                                           PayloadData[0]);
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
