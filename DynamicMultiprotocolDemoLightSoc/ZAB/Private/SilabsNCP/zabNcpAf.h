/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the AF Data Service - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.018  26-Jul-16   MvdB   Support AF Broadcasts and Groupcasts
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 * 002.002.040  17-Jan-17   MvdB   ARTF175875: Upgrade zabNcpAf_DataRequest() to support send via bind
 *****************************************************************************/
#ifndef __ZAB_NCP_AF_H__
#define __ZAB_NCP_AF_H__

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Send an AF Data Request
 *
 * Return zab_bool:
 *  - zab_true = retain message on output queue as it needs to be sent again
 *  - zab_false = remove message from output queue as we are finished with it
 ******************************************************************************/
zab_bool zabNcpAf_DataRequest(erStatus* Status, zabService* Service, sapMsg* Message);

/******************************************************************************
 * Process an incoming AF Data frame
 ******************************************************************************/
void zabNcpAf_IncomingMessageHandler(erStatus* Status, zabService* Service,
                                     EmberIncomingMessageType IncomingMessageType,
                                     unsigned16 SourceAddress,
                                     EmberApsFrame* ApsFrame,
                                     unsigned8 Lqi,
                                     unsigned8 Rssi,
                                     unsigned8 PayloadLength, unsigned8* PayloadData);

#ifdef __cplusplus
}
#endif

#endif // __ZAB_NCP_AF_H__
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/