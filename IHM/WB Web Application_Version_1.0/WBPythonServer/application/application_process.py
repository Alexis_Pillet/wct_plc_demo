
# ***************************
"""
application.application_process
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to manage the application layout.

"""

import types
from queue import Queue, Empty
from threading import Thread
from apscheduler.schedulers.background import BackgroundScheduler
from .application_frame import Application, ApplicationFrameGenerator
from datetime import datetime, timedelta
from .zigbee_device_container import ZigBeeDeviceContainer
from .application_frame_management import ApplicationFrameManagement
from network import NetworkFrameGenerator
from logger import LoggerManager
from network import Network
log = LoggerManager.get_logger(__name__)


class ApplicationProcess(object):

    def __init__(self):
        # Thread
        self._receiver_alive = None
        self._receiver = None
        # Read/Write management
        self._local_application_sequence_number = 0
        self._external_application_sequence_number = -1
        # Connection to network process
        self._queue = Queue()
        self._network_frame_generator = None
        # Send command/data management
        self._application_frame_generator = None
        self._application_scheduler = BackgroundScheduler()
        self._application_frame_in_progress = []
        # storage of devices
        self._zigbee_device_container = None
        # treatment of received application frames
        self._application_frame_management = ApplicationFrameManagement()

    @property
    def queue(self):
        return self._queue

    @queue.setter
    def queue(self, queue):
        self._queue = queue

    @property
    def application_frame_generator(self):
        return self._application_frame_generator

    @application_frame_generator.setter
    def application_frame_generator(self, frame_generator):
        self._application_frame_generator = frame_generator

    @property
    def network_frame_generator(self):
        return self._network_frame_generator

    @network_frame_generator.setter
    def network_frame_generator(self, frame_generator):
        self._network_frame_generator = frame_generator

    @property
    def zigbee_device_container(self):
        return self._zigbee_device_container

    @zigbee_device_container.setter
    def zigbee_device_container(self, zigbee_device_container):
        self._zigbee_device_container = zigbee_device_container

    def _start_receiver(self):
        self._receiver_alive = True
        self._receiver = Thread(target=self._receiver_loop, name='application_rx', args=())
        self._receiver.daemon = True
        self._receiver.start()

    def _stop_receiver(self):
        self._receiver_alive = False

    def start(self):
        """Start receiver thread/scheduler"""
        self._start_receiver()
        self._application_frame_generator = ApplicationFrameGenerator(self)
        self._zigbee_device_container = ZigBeeDeviceContainer(self._application_frame_generator)
        self._application_scheduler.start()

    def stop(self):
        """Stop receiver thread/scheduler"""
        self._stop_receiver()
        self._receiver.join()
        self._application_scheduler.remove_all_jobs()
        self._application_scheduler.shutdown()

    def _receiver_loop(self):
        """loop and copy serial->console"""

        while self._receiver_alive:

            # We are waiting for data coming from network process
            try:
                data = self._queue.get(True, 0.1)
            except Empty:
                continue

            result = self._parse_default_response_frame(data["frame"])
            if result["is_default_response"]:

                # Check if the ack corresponding to a command/data frames sent
                if self._application_scheduler.get_job(str(result["application_sequence_number"])) is None:
                    continue

                # Remove retry_jobs/metadata since we received the network ack
                self._application_scheduler.remove_job(str(result["application_sequence_number"]))
                self._application_frame_in_progress.pop(0)
                # Check if there are retry_jobs pending => if yes start it
                if len(self._application_frame_in_progress) >= 1:
                    self._application_scheduler.resume_job(self._application_frame_in_progress[0].id_job)
                    # We are adding 10 milliseconds to permit to to PLC concentrator to see the frame
                    self._application_scheduler.modify_job(self._application_frame_in_progress[0].id_job,
                                                           next_run_time=(datetime.now() + timedelta(milliseconds=10)))

                if result["default_response_status"] != Application.AckStatus.SUCCESS:
                    log.error('ACK_RECEIVED - {}'.format(Application.display_ack()[result["ack_status"]]))
            else:

                result = self._parse_frame(Application.CommandID.UPDATE_DEVICE_DATA, data["frame"])
                # We don't combine the two if conditions because we need to enter in the first if condition
                # if we find the command ID, otherwise if there are another error in the frame, this error
                # will be erase by the other check perform in the next if condition
                if result["is_command_id_frame"]:
                    if result["is_good_frame"]:
                        self._application_frame_management.update_device_data(data["device_number"], data["frame"])
                        log.debug('UDD - {}'.format(data["frame"].hex()))
                else:

                    result = self._parse_frame(Application.CommandID.UPDATE_DEVICE_LINK_INDICATOR, data["frame"])
                    if result["is_command_id_frame"]:
                        if result["is_good_frame"]:
                            self._application_frame_management.update_device_link_indicator(data["device_number"],
                                                                                            data["frame"])
                            log.debug('UDLI - {}'.format(data["frame"].hex()))
                    else:

                        result = self._parse_frame(Application.CommandID.CLUSTER_COMMAND, data["frame"])
                        if result["is_command_id_frame"]:
                            if result["is_good_frame"]:
                                self._application_frame_management.cluster_command(data["device_number"], data["frame"])
                                log.debug('CC - {}'.format(data["frame"].hex()))

                self._send_default_response(data["device_number"], result["default_response_status_to_send"])

                if result["ack_status_to_send"] == Application.AckStatus.SUCCESS_NO_ACK:
                    log.debug('ACK_TO_SEND - {}'.format(Application.display_ack()[result["ack_status_to_send"]]))
                elif result["ack_status_to_send"] != Application.AckStatus.SUCCESS:
                    log.error('ACK_TO_SEND - {}'.format(Application.display_ack()[result["ack_status_to_send"]]))

            self._queue.task_done()

    def _parse_default_response_frame(self, frame):
        """Check if the frame is a default response (applicative ack)

        Args:
            frame: data of the frame

        Returns:
            A dict with the result of the control
            "is_default_response": If the frame is a default response or not
            "network_sequence_number": network sequence number of the frame
            "ack_status": Status of the network ack
            example:
                >>> {"is_default_response": True, "application_sequence_number": 0, "ack_status": 0}
        """
        result = {"is_default_response": False, "application_sequence_number": -1,
                  "ack_status": Application.AckStatus.ERROR_NO_ACK}

        if int(frame[0]) != Application.Interface.ZIGBEE:
            return result

        if int(frame[1]) != Application.CommandID.DEFAULT_RESPONSE:
            return result

        # Check if we are waiting an acknowledgment
        if len(self._application_frame_in_progress) == 0:
            return result

        if int(frame[2]) != self._application_frame_in_progress[0].application_sequence_number:
            return result

        # Good frame
        result["is_default_response"] = True
        result["application_sequence_number"] = int(frame[2])
        result["ack_status"] = int(frame[3])
        return result

    def _parse_frame(self, application_command_id_to_check, frame):
        """Check if the frame is good according to the command ID specified

        Args:
            application_command_id_to_check: value to check
            frame: data of the frame

        Returns:
            A dict with the result of the control
            "is_command_id_frame": If the frame got the command ID specified or not
            "is_good_frame": If the frame is a good frame or not
            "ack_status_to_send": status of the default response to send back
            example:
                >>> {"is_command_id_frame": True, "is_good_frame": True, "ack_status_to_send": 0x00}
        """

        result = {"is_command_id_frame": False, "is_good_frame": False,
                  "ack_status_to_send": Application.AckStatus.ERROR_NO_ACK}

        if int(frame[0]) != Application.Interface.ZIGBEE:
            result["ack_status_to_send"] = Application.AckStatus.ERROR_NO_ACK
            return result

        if int(frame[1]) == application_command_id_to_check:
            if int(frame[1]) == Application.CommandID.DEFAULT_RESPONSE:
                result["ack_status_to_send"] = Application.AckStatus.ERROR_NO_ACK
            else:
                result["ack_status_to_send"] = Application.AckStatus.INVALID_COMMAND
            return result

        # We found the good command ID
        result["is_command_id_frame"] = True

        if int(frame[2]) == self._external_application_sequence_number:
            result["ack_status_to_send"] = Application.AckStatus.ALREADY_RX
            return result

        if (int(frame[3]) & Application.OptionMask.RESERVED) != 0x00:
            result["ack_status_to_send"] = Application.AckStatus.ERROR_NO_ACK
            return result

        # Good frame
        self._external_application_sequence_number = int(frame[2])
        result["is_good_frame"] = True
        # Check if the bit "Response Enable" is set or not
        # If this is set, we send a network acknowledgement
        if (int(frame[3]) & Application.OptionMask.RESPONSE_ENABLE) == Application.OptionResponse.ENABLE:
            result["ack_status_to_send"] = Application.AckStatus.SUCCESS
        else:
            result["ack_status_to_send"] = Application.AckStatus.SUCCESS_NO_ACK
        return result

    def _send_default_response(self, device_number, result):
        """Send data to network process to send a default response

        Args:
            device_number: device number of the WB
            result: ack status to send
        """
        # In some cases we don't want to send a ack
        # Wrong frame or "Response Enable" Option
        if (result != Application.AckStatus.ERROR_NO_ACK) & (result != Application.AckStatus.SUCCESS_NO_ACK):

            frame = bytearray([Application.Interface.ZIGBEE, Application.CommandID.DEFAULT_RESPONSE,
                               self._external_application_sequence_number])
            frame.append(result)

            # We don't want an network acknowledgment because we send a default response
            default_response_option = NetworkFrameGenerator.set_option(self._network_frame_generator.default_option,
                                                                       Network.OptionMask.RESPONSE_ENABLE,
                                                                       Network.OptionResponse.DISABLE)

            self._network_frame_generator.send_data_frame(device_number, frame, option=default_response_option)

    def send_applicative_frame(self, application_interface, command_id, option, device_number, payload):
        """Send network frame to mac process

        Args:
            application_interface: application interface field
            command_id: command id field
            option: option field
            device_number: device number of the WB
            payload: payload of the frame
        """
        if self._local_application_sequence_number == 255:
            self._local_application_sequence_number = 0
        else:
            self._local_application_sequence_number += 1

        frame = bytearray([application_interface, command_id, self._local_application_sequence_number, option])
        for element in payload:
            frame.append(element)

        # Send applicative frame to network layout without retry system because we don't want an acknowledgment
        if (option & Application.OptionCommandMask.RESPONSE_ENABLE) == Application.OptionResponse.DISABLE:
            self._network_frame_generator.send_data_frame(device_number, frame)
        else:

            # Save metadata of the frame to be able to manage the retry process of it
            metadata_frame = types.SimpleNamespace()
            metadata_frame.application_sequence_number = self._local_application_sequence_number
            metadata_frame.id_job = str(self._local_application_sequence_number)
            metadata_frame.number_retry = 0
            self._application_frame_in_progress.append(metadata_frame)

            self._application_scheduler.add_job(self._sender, trigger='interval',
                                                args=(frame, device_number,), seconds=4.0,
                                                id=str(self._local_application_sequence_number))
            self._application_scheduler.pause_job(metadata_frame.id_job)

            # If there are no retry ob running => start it immediately
            if len(self._application_frame_in_progress) == 1:
                self._application_scheduler.resume_job(self._application_frame_in_progress[0].id_job)
                # We are adding 10 milliseconds to permit to to PLC concentrator to see the frame
                self._application_scheduler.modify_job(self._application_frame_in_progress[0].id_job,
                                                       next_run_time=(datetime.now() + timedelta(milliseconds=10)))

    def _sender(self, frame, device_number):
        """retry job function

        We are waiting 4 seconds to receive an default response
        If you don't receive one, we retry to send the frame
        After 3 retry, we raise an error

        Args:
            frame: frame to send to mac process
            device_number: device number of the WB
        """
        try:

            if self._application_frame_in_progress[0].number_retry < 3:
                self._application_frame_in_progress[0].number_retry += 1
                self._network_frame_generator.send_data_frame(device_number, frame)
            else:
                # Stop the retry process for this frame
                self._application_scheduler.remove_job(self._application_frame_in_progress[0].id_job)
                self._application_frame_in_progress.pop(0)
                # Check if there are retry_jobs pending => if yes start it
                if len(self._application_frame_in_progress) >= 1:
                    self._application_scheduler.resume_job(self._application_frame_in_progress[0].id_job)
                    # We are adding 10 milliseconds to permit to to PLC concentrator to see the frame
                    self._application_scheduler.modify_job(self._application_frame_in_progress[0].id_job,
                                                           next_run_time=(datetime.now() + timedelta(milliseconds=10)))
                pass

        except Exception:
            raise
