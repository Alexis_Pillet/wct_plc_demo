
"""
data_model.zigbee_green_power.power_tag.power_tag_1p
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to model Power Tag 1P Device

"""
from data_model.zigbee_green_power.power_tag.power_tag import PowerTag


class PowerTag1P(PowerTag):
    """Model Power Tag 1P Device"""

    def __init__(self, address, id_wireless_bridge, application_frame_generator):
        """Initialization of the Power Tag 1P Device"""
        super(PowerTag1P, self).__init__(address, id_wireless_bridge, application_frame_generator)
        self._VOLTAGE_PHA_ID = "0505"
        self._CURRENT_PHA_ID = "0508"
        self._voltage = 0
        self._current = 0

    def update_data(self, data):
        """Update the attributes of the device according to the value specified

        Args:
            data: dict of the value received
        """
        list_attribute = data[self._BASIC_CLUSTER]
        for attribute in list_attribute:
            if attribute.attribute_id == self._MANUFACTURER_NAME:
                self.manufacturer_name = attribute.attribute_value
            elif attribute.attribute_id == self._MODEL_IDENTIFIER:
                self.model_identifier = attribute.attribute_value
            elif attribute.attribute_id == self._APPLICATION_FIRMWARE_VERSION_ID:
                self.application_firmware_version = attribute.attribute_value
            elif attribute.attribute_id == self._APPLICATION_HARDWARE_VERSION_ID:
                self.application_hardware_version = attribute.attribute_value
            elif attribute.attribute_id == self._PRODUCT_SERIAL_NUMBER_ID:
                self.product_serial_number = attribute.attribute_value
            elif attribute.attribute_id == self._PRODUCT_IDENTIFIER_ID:
                self.product_identifier = attribute.attribute_value
            elif attribute.attribute_id == self._PRODUCT_MODEL:
                self.product_model = attribute.attribute_value

        list_attribute = data[self._ELECTRICAL_MEASUREMENT_CLUSTER]
        for attribute in list_attribute:
            if attribute.attribute_id == self._VOLTAGE_PHA_ID:
                self._voltage = attribute.attribute_value
            elif attribute.attribute_id == self._CURRENT_PHA_ID:
                self._current = attribute.attribute_value
            elif attribute.attribute_id == self._ACTIVE_POWER_PHA_ID:
                self._active_power_pha = attribute.attribute_value
            elif attribute.attribute_id == self._PHASE_SEQUENCE_ID:
                self._phase_sequence = attribute.attribute_value
            elif attribute.attribute_id == self._AC_ALARMS_MASK_ID:
                self._ac_alarms_mask = attribute.attribute_value
            elif attribute.attribute_id == self._ALARMS_MASK_ID:
                self._alarms_mask = attribute.attribute_value
            elif attribute.attribute_id == self._POWER_DIVISOR_ID:
                self._power_divisor = attribute.attribute_value
            elif attribute.attribute_id == self._VOLTAGE_DIVISOR_ID:
                self._voltage_divisor = attribute.attribute_value
            elif attribute.attribute_id == self._CURRENT_DIVISOR_ID:
                self._current_divisor = attribute.attribute_value

        list_attribute = data[self._METERING_CLUSTER]
        for attribute in list_attribute:
            if attribute.attribute_id == self._CUMULATED_ENERGY_ID:
                self._cumulated_energy = attribute.attribute_value
            elif attribute.attribute_id == self._PARTIAL_ENERGY_ID:
                self._partial_energy = attribute.attribute_value
            elif attribute.attribute_id == self._DIVISOR_ID:
                self._divisor = attribute.attribute_value

    def update_rssi(self, lqi, rssi):
        """Update the lqi and rssi of the product

        Args:
            lqi: lqi value
            rssi: rssi value
        """
        self.rssi = rssi
        self.lqi = lqi

    def get_json_data(self):
        """Get the json representation of the data of the Power Tag 1P product"""
        return {"product": {"address": self.address.upper(),
                            "lqi": self.lqi,
                            "rssi": self.rssi,
                            "model_identifier": self.model_identifier,
                            "application_firmware_version": self.application_firmware_version,
                            "application_hardware_version": self.application_hardware_version,
                            "product_serial_number": self.product_serial_number,
                            "product_identifier": self.product_identifier,
                            "product_model": self.product_model,
                            "manufacturer_name": self.manufacturer_name},
                "electrical_measurement": {"voltage_pha": self.voltage / self.voltage_divisor,
                                           "current_pha": self.current / self.current_divisor,
                                           "active_power_pha": self.active_power_pha,
                                           "phase_sequence": self._phase_sequence_value[self.phase_sequence],
                                           "ac_alarms_mask": self.ac_alarms_mask,
                                           "alarms_mask": self.alarms_mask},
                "metering": {"cumulated_energy": self.cumulated_energy / self.divisor,
                             "partial_energy": self.partial_energy / self.divisor}
                }

    def get_json_device(self, device_number):
        """Get the json representation of the Power Tag 1P product

        Args:
            device_number: number of the device
        """
        return {"text": self.address.upper() + ' - Power Tag 1P',
                'id': "WB_" + str(self.id_wireless_bridge + 1) + "_DEVICE_" + self.address,
                'type': 'power_tag_1p',
                'data': self.get_json_data()
                }

    @property
    def voltage(self):
        return self._voltage

    @voltage.setter
    def voltage(self, voltage):
        self._voltage = voltage

    @property
    def current(self):
        return self._current

    @current.setter
    def current(self, current):
        self._current = current
