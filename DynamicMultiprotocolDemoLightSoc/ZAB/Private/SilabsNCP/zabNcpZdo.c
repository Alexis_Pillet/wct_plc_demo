/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.004  30-Jun-16   MvdB   Queue generated ZDO responses to respect serial link management
 * 000.000.005  01-Jul-16   MvdB   Support mandatory ZDO client commands for MiGenie:
 *                                  - SZL_ZDO_MgmtLqiReq, SZL_ZDO_MgmtBindReq, SZL_ZDO_BindReq, SZL_ZDO_UnBindReq,
 *                                  - SZL_NwkLeaveReq, SZL_ZDO_NwkAddrReq, SZL_ZDO_PowerDescriptorReq ,SZL_ZDO_NodeDescriptorReq
 * 000.000.013  21-Jul-16   MvdB   Upgrade ZDO responses to use correct network address, not just 0
 * 000.000.014  22-Jul-16   MvdB   Support: zabNcpZdo_MatchDescReq, zabNcpZdo_MgmtNwkUpdateReq, zabNcpZdo_MgmtRtgReq, zabNcpZdo_UserDescReq, zabNcpZdo_UserDescSetReq
 * 000.000.015  25-Jul-16   MvdB   Support multiple endpoints
 * 000.000.016  25-Jul-16   MvdB   Support ZAB_ACTION_CHANGE_CHANNEL and other a-synchronous network parameter changes
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/

#include "zabNcpService.h"
#include "zabNcpPrivate.h"
#include "zabNcpEzspMessaging.h"
#include "zabNcpNwk.h"

#include "zabNcpZdo.h"

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* Sequence Number */
#define ZDO_MESSAGE_OVERHEAD                        ( 1 )

/* Bit masks for Flags of Device Announce */
#define DEVIC_ANNCE_MASK_ALT_PAN_COORD 0x01
#define DEVIC_ANNCE_MASK_DEV_TYPE_FFD 0x02
#define DEVIC_ANNCE_MASK_POWER_SRC_MAINS 0x04
#define DEVIC_ANNCE_MASK_RX_ON_WHEN_IDLE 0x08
#define DEVIC_ANNCE_MASK_SECURITY_CAP 0x40
#define DEVIC_ANNCE_MASK_ALLOCATE_ADDR 0x80

#define M_ZDO_ERROR_RSP_PAYLOAD_LENGTH              ( 1 ) // Status(1)

#define M_NWK_ADDR_REQ_PAYLOAD_LENGTH               ( 10 ) // IEEE(8), RequestType(1), StartIndex(1)

#define M_MIN_NODE_DESC_RESP_PAYLOAD                ( 3 ) // Status(1), NwkAddrOfInterest(2)
#define M_MIN_NODE_DESC_LENGTH                      ( 13 ) // Flags(2), MacCapFlags(1), ManCode(2), BuffSize(1), InTransSize(2), SrvrMask(2), OutTransSize(2), DescCap(1)

#define M_MIN_POWER_DESC_RESP_PAYLOAD               ( 3 ) // Status(1), NwkAddrOfInterest(2)
#define M_MIN_POWER_DESC_LENGTH                     ( 2 ) // 4 x 4bit fields

#define M_MIN_ACTIVE_EP_REQ_PAYLOAD                 ( 2 )   // NetworkAddressOfInterest
#define M_MIN_ACTIVE_EP_RESP_PAYLOAD                ( 4 )   // NetworkAddressOfInterest + Status + NumberOfEndpoints
#define M_MAX_ACTIVE_EPS                            ( 16 )  // Maximum number of active endpoitns we can support in an incoming

#define M_MIN_SIMPLE_DESC_REQ_PAYLOAD               ( 3 )   // NetworkAddressOfInterest(2), Endpoint
#define M_MIN_SIMPLE_DESC_RSP_PAYLOAD               ( 4 )   // Status, NetworkAddressOfInterest(2), Endpoint
#define M_SIMPLE_DESC_LENGTH( in, out )             ( 8 + (((in) + (out)) * sizeof(unsigned16)) ) // Endpoint, Profile(2), DevId(2), AppDevVer, InClusterCnt, OutClusterCnt

#define M_MIN_MATCH_DESC_REQ_PAYLOAD                ( 6 )  // NetworkAddressOfInterest(2), Profile(2), NumInClusters, NumOutClusters
#define M_MIN_MATCH_DESC_RSP_PAYLOAD                ( 4 )  // Status, NetworkAddressOfInterest(2), Length
#define M_MATCH_DESC_MAX_NUMBER_OF_CLUSTERS         ( 32 ) // Max clusters in an outgoing MAtchDescReq
#define M_MATCH_DESC_MAX_NUMBER_OF_ENDPOINTS        ( 32 ) // Max endpoints in an incoming MAtchDescResp
#define M_MATCH_DESC_LENGTH( in, out )              (M_MIN_MATCH_DESC_REQ_PAYLOAD + (((in) + (out)) * sizeof(unsigned16)) )

#define M_MIN_BIND_RESP_PAYLOAD                     ( 1 ) // Status(1)

#define M_MIN_MGMT_LQI_RSP_PAYLOAD                  ( 1 )   // Status
#define M_MGMT_LQI_RSP_MAX_NEIGHBOR_TABLE_ITEMS     ( 3 )
#define M_MGMT_LQI_RSP_NEIGHBOR_TABLE_ITEM_SIZE     ( 22 ) // EPID(8), IEEE(8), NwkAddr(2), BitField(1), PJ(1), Depth(1), LQI(1)

#define M_MIN_MGMT_BIND_RSP_PAYLOAD                 ( 1 )   // Status
#define M_MGMT_BIND_RSP_MAX_BINDING_TABLE_ITEMS     ( 4 )
#define M_MIN_SIZE_OF_BIND_ITEM                     ( 14 ) // SrcIEEE(8), SrcEp(1), Cluster(2), DstaddrMode(1), DstAddr(2 - Group)
#define M_MAX_SIZE_OF_BIND_ITEM                     ( 21 ) // SrcIEEE(8), SrcEp(1), Cluster(2), DstaddrMode(1), DstAddr(8 - IEEE), DstEp(1)

#define M_MIN_MGMT_RTG_RSP_PAYLOAD                  ( 1 )   // Status
#define M_MIN_SUCCESS_MGMT_RTG_RSP_PAYLOAD          ( 4 )   // Status(1), RoutingTableEntries(1), StartIndex(1), RoutingTableListCount(1),
#define M_MGMT_RTG_RSP_MAX_TABLE_ITEMS              ( 15 )
#define M_SIZE_OF_RTG_ITEM                          ( 5 ) // DstAddr(2), Flags(1), NextHop(2)

#define M_MGMT_LEAVE_REQ_PAYLOAD_LENGTH             ( 9 ) // IEEE(8), Flags(1)
#define M_MIN_MGMT_LEAVE_RESP_PAYLOAD               ( 1 ) // Status(1)

#define M_MGMT_NWK_UPDATE_REQ_MAX_PAYLOAD_LENGTH    ( 9 ) // ScanChannels(4), ScanDuration(1), ScanCount(1), nwkUpdateId(1), nwkManagerAddr(2)
#define M_MIN_MGMT_NWK_UPDATE_RESP_PAYLOAD          ( 1 ) // Status(1)
#define M_MIN_SUCCESS_MGMT_NWK_UPDATE_RESP_PAYLOAD  ( 10 ) // Status(1), ScannedChannels(4), TotalTransmissions(2), TransmissionFailures(2), ScannedChannelsListCount(1)

#define M_USER_DESC_REQ_PAYLOAD_LENGTH              ( 2 ) // NWKAddrOfInterest(2)
#define M_USER_DESCRIPTOR_MAX_LENGTH                ( 16 ) // UserDescriptor(16)
#define M_USER_DESC_SET_REQ_PAYLOAD_LENGTH          ( 3 + M_USER_DESCRIPTOR_MAX_LENGTH ) // NWKAddrOfInterest(2), Length(1), UserDescriptor(16))

#define M_MIN_USER_DESC_RESP_PAYLOAD                ( 4 ) // Status(1), NWKAddrOfInterest(2), Length(1),
#define M_MIN_USER_DESC_SET_RESP_PAYLOAD            ( 3 ) // Status(1), NWKAddrOfInterest(2)

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/



/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 * Send message via unicast or broadcast as required by DestinationAddress
 ******************************************************************************/
static void zabNcpZdo_SendMessage(erStatus* Status, zabService* Service,
                                  EmberNodeId DestinationAddress,
                                  EmberApsFrame *apsFrame,
                                  uint8_t messageTag,
                                  uint8_t messageLength,
                                  uint8_t *messageContents)
{
  if (DestinationAddress < EMBER_MIN_BROADCAST_ADDRESS)
    {
      zabNcpEzspMessaging_SendUnicast(Status, Service,
                                      EMBER_OUTGOING_DIRECT,
                                      DestinationAddress,
                                      apsFrame,
                                      messageTag,
                                      messageLength,
                                      messageContents);
    }
  else
    {
      zabNcpEzspMessaging_SendBroadcast(Status, Service,
                                        DestinationAddress,
                                        apsFrame,
                                        0, // Zero is converted to EMBER_MAX_HOPS
                                        messageTag,
                                        messageLength,
                                        messageContents);
    }
}

/******************************************************************************
 * Common processing of an incoming IEEE/Network Address Response
 ******************************************************************************/
static void ieeeNwkAddrRespHandler(erStatus* Status, zabService* Service, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData, unsigned16 ClusterId)
{
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  unsigned8 device;
  SZL_ZdoAddrRespParams_t* ieeeRsp;
  unsigned8 offset;
  #define M_IEEE_ADDR_RSP_MAX_DEVICES 35

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= 11)
    {
      // Allocate New Message
      length = SZL_ZdoAddrRespParams_t_SIZE(M_IEEE_ADDR_RSP_MAX_DEVICES);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ClusterId);

      /* Extract from the  command into the data sap structure */
      ieeeRsp = (SZL_ZdoAddrRespParams_t*)sapMsgGetAppData(dataMessage);
      ieeeRsp->Status = (SZL_ZDO_STATUS_t)PayloadData[0];
      ieeeRsp->IeeeAddr = COPY_IN_64_BITS(&PayloadData[1]);
      ieeeRsp->NetworkAddress = COPY_IN_16_BITS(&PayloadData[9]);

      /* Associated device info is only included if status == success */
      if ( (ieeeRsp->Status == SZL_ZDO_STATUS_SUCCESS) && (PayloadLength >= 13) )
        {
          ieeeRsp->StartIndex = PayloadData[11];
          ieeeRsp->NumAssocDev = PayloadData[12];
          /* ARTF113871: Clear start address if no associated device list. As the value provided by stack is uninitialised */
          if (ieeeRsp->NumAssocDev == 0)
            {
              ieeeRsp->StartIndex = 0;
            }

          if (ieeeRsp->NumAssocDev > M_IEEE_ADDR_RSP_MAX_DEVICES)
            {
              printError(Service, "ZNP ZDO: Warning: Max num devices %d exceeds %d, truncating\n", ieeeRsp->NumAssocDev, M_IEEE_ADDR_RSP_MAX_DEVICES);
              ieeeRsp->NumAssocDev = M_IEEE_ADDR_RSP_MAX_DEVICES;
            }

          offset=13;
          for (device = 0; ( (device < ieeeRsp->NumAssocDev) && (PayloadLength >= (15 + (2*device))) ); device++)
            {
              ieeeRsp->AssocDevNetworkAddresses[device] = COPY_IN_16_BITS(&PayloadData[offset]); offset+=2;
            }
          /* Set back to the actual number of devices we managed to add - handles the case where data is shorter then indicated by numAssociatedDevices*/
          ieeeRsp->NumAssocDev = device;
        }
      else
        {
          ieeeRsp->StartIndex = 0;
          ieeeRsp->NumAssocDev = 0;
        }

      sapMsgSetAppDataLength(Status, dataMessage, SZL_ZdoAddrRespParams_t_SIZE(ieeeRsp->NumAssocDev));
      sapMsgSetAppTransactionId(Status, dataMessage, ZdoTransSeqNum);
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process an incoming Nwk Address Response
 ******************************************************************************/
static void zabNcpZdo_NwkAddrRespHandler(erStatus* Status, zabService* Service, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  ieeeNwkAddrRespHandler(Status, Service, ZdoTransSeqNum, PayloadLength, PayloadData, ZAB_MSG_APP_ZDO_NWK_RSP);
}

/******************************************************************************
 * Process an incoming IEEE Address Response
 ******************************************************************************/
static void zabNcpZdo_IeeeAddrRespHandler(erStatus* Status, zabService* Service, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  ieeeNwkAddrRespHandler(Status, Service, ZdoTransSeqNum, PayloadLength, PayloadData, ZAB_MSG_APP_ZDO_IEEE_RSP);
}

/******************************************************************************
 * Process an incoming Node Descriptor Response
 ******************************************************************************/
static void zabNcpZdo_NodeDescRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceNetworkAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoNodeDescriptorRespParams_t* rsp;
  unsigned8 index;

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= M_MIN_NODE_DESC_RESP_PAYLOAD)
    {
      // Allocate New Message
      length = SZL_ZdoNodeDescriptorRespParams_t_SIZE;
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_NODE_DESC_RSP);
      sapMsgSetAppTransactionId(Status, dataMessage, ZdoTransSeqNum);

      /* Extract from the ZigBee command into the data sap structure */
      rsp = (SZL_ZdoNodeDescriptorRespParams_t*)sapMsgGetAppData(dataMessage);
      rsp->SourceAddress = SourceNetworkAddress;

      index = 0;
      rsp->Status = (SZL_ZDO_STATUS_t)PayloadData[index++];
      rsp->NetworkAddressOfInterest = COPY_IN_16_BITS(&PayloadData[index]); index+=2;

      /* Power Descriptor is only included if status == success */
      if ( (rsp->Status == SZL_ZDO_STATUS_SUCCESS) && (PayloadLength >= (index + M_MIN_NODE_DESC_LENGTH)) )
        {
          rsp->Option.optWord = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
          rsp->MACCapabilityFlag.optByte = PayloadData[index++];
          rsp->ManufacturerCode = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
          rsp->MaximumBufferSize = PayloadData[index++];
          rsp->MaximumIncomingTransferSize = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
          rsp->ServerMask.optWord = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
          rsp->MaximumOutgoingTransferSize = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
          rsp->DescriptorCapability.optByte = PayloadData[index++];
        }
      else
        {
          /* Something went wrong with the length - setup defaults */
          rsp->Option.optBits.LogicalType = SZL_ZDO_MGMT_LQI_DEVICE_UNKNOWN;
          rsp->Option.optBits.ComplexDescriptorAvailable = NOT_SUPPORTED;
          rsp->Option.optBits.UserDescriptorAvailable = NOT_SUPPORTED;
          rsp->Option.optBits.FrequencyBand = SZL_ZDO_FEQUENCY_BAND_UNKNOWN;
          rsp->MACCapabilityFlag.optBits.AllocateAddress = NOT_SUPPORTED;
          rsp->MACCapabilityFlag.optBits.AlternatePANCoordinator = NOT_SUPPORTED;
          rsp->MACCapabilityFlag.optBits.PowerSource = NOT_SUPPORTED;
          rsp->MACCapabilityFlag.optBits.ReceiverOnWhenIdle = NOT_SUPPORTED;
          rsp->MACCapabilityFlag.optBits.SecurityCapability = NOT_SUPPORTED;
          rsp->MACCapabilityFlag.optBits.DeviceType = 0;
          rsp->ManufacturerCode = DEFAULT_MANUFACTURER_CODE;
          rsp->MaximumBufferSize = DEFAULT_SIZE;
          rsp->MaximumIncomingTransferSize = DEFAULT_SIZE;
          rsp->ServerMask.optBits.BackupBindingTableCache = NOT_SUPPORTED;
          rsp->ServerMask.optBits.BackupDiscoveryCache = NOT_SUPPORTED;
          rsp->ServerMask.optBits.BackupTrustCenter = NOT_SUPPORTED;
          rsp->ServerMask.optBits.NetworkManager = NOT_SUPPORTED;
          rsp->ServerMask.optBits.PrimaryBindingTableCache = NOT_SUPPORTED;
          rsp->ServerMask.optBits.PrimaryDiscoveryCache = NOT_SUPPORTED;
          rsp->ServerMask.optBits.PrimaryTrustCenter = NOT_SUPPORTED;
          rsp->MaximumOutgoingTransferSize = DEFAULT_SIZE;
          rsp->DescriptorCapability.optBits.ExtendedActiveEndpointListAvailable = NOT_SUPPORTED;
          rsp->DescriptorCapability.optBits.ExtendedSimpleDescriptorListAvailable = NOT_SUPPORTED;
        }

      sapMsgSetAppDataLength(Status, dataMessage, length);
      sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process an incoming Power Descriptor Response
 ******************************************************************************/
static void zabNcpZdo_PowerDescRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceNetworkAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoPowerDescriptorRespParams_t* rsp;
  unsigned8 index;

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= M_MIN_POWER_DESC_RESP_PAYLOAD)
    {
      // Allocate New Message
      length = SZL_ZdoPowerDescriptorRespParams_t_SIZE;
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_POWER_DESC_RSP);
      sapMsgSetAppTransactionId(Status, dataMessage, ZdoTransSeqNum);

      /* Extract from the ZigBee command into the data sap structure */
      rsp = (SZL_ZdoPowerDescriptorRespParams_t*)sapMsgGetAppData(dataMessage);
      rsp->SourceAddress = SourceNetworkAddress;

      index = 0;
      rsp->Status = (SZL_ZDO_STATUS_t)PayloadData[index++];
      rsp->NetworkAddressOfInterest = COPY_IN_16_BITS(&PayloadData[index]); index+=2;


      /* Descriptor is only included if status == success */
      if ( (rsp->Status == SZL_ZDO_STATUS_SUCCESS) && (PayloadLength >= (index + M_MIN_POWER_DESC_LENGTH)) )
        {
          rsp->CurrentPowerMode = PayloadData[index] & 0x0F;
          rsp->AvailablePowerSources = (PayloadData[index]>>4) & 0x0F;
          index++;

          rsp->CurrentPowerSource = PayloadData[index] & 0x0F;
          rsp->CurrentPowerSourceLevel = (PayloadData[index]>>4) & 0x0F;
          index++;
        }
      else
        {
          /* Something went wrong with the length - setup defaults */
          rsp->AvailablePowerSources = SZL_AVAILABLE_POWER_SOURCE_UNKNOWN;
          rsp->CurrentPowerMode = SZL_ZDO_CURRENT_POWER_MODE_RX_ON_WHEN_IDLE;
          rsp->CurrentPowerSourceLevel = SZL_CURRENT_POWER_SOURCE_LEVEL_100_PERCENT;
          rsp->CurrentPowerSource = SZL_CURRENT_POWER_SOURCE_UNKNWON;
        }

      sapMsgSetAppDataLength(Status, dataMessage, length);
      sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process an incoming Active Endpoint Response
 ******************************************************************************/
static void zabNcpZdo_ActiveEpRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceNetworkAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoActiveEndpointRespParams_t* activeEpRsp;
  unsigned8 endpointCount;
  unsigned8 index;

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= M_MIN_ACTIVE_EP_RESP_PAYLOAD)
    {
      // Allocate New Message
      length = SZL_ZdoActiveEndpointRespParams_t_SIZE(M_MAX_ACTIVE_EPS);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_ACTIVE_EP_RSP);
      sapMsgSetAppTransactionId(Status, dataMessage, ZdoTransSeqNum);

      /* Extract from the ZigBee command into the data sap structure */
      activeEpRsp = (SZL_ZdoActiveEndpointRespParams_t*)sapMsgGetAppData(dataMessage);

      index = 0;
      activeEpRsp->Status = (SZL_ZDO_STATUS_t)PayloadData[index++];
      // activeEpRsp->nwkAddress = COPY_IN_16_BITS(&dp[0]);
      activeEpRsp->NetworkAddressOfInterest = COPY_IN_16_BITS(&PayloadData[index]); index+=2;

      endpointCount =  PayloadData[index++];
      if (endpointCount > M_MAX_ACTIVE_EPS)
        {
          printError(Service, "ZDO: Warning: Max num endpoints %d exceeds %d, truncating\n", endpointCount, M_MAX_ACTIVE_EPS);
          endpointCount = M_MAX_ACTIVE_EPS;
        }
      activeEpRsp->ActiveEndpointCount = endpointCount;
      osMemCopy( Status, activeEpRsp->ActiveEndpointList, &PayloadData[index], endpointCount);

      sapMsgSetAppDataLength(Status, dataMessage, length);
      sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Handle an incoming Active Endpoint Request from the network
 ******************************************************************************/
static void zabNcpZdo_ActiveEpReqHandler(erStatus* Status, zabService* Service, unsigned16 SourceNetworkAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 index;
  unsigned8 endpointIndex;
  unsigned8 endpointCount;
  unsigned8 endpointCountIndex;
  unsigned16 networkAddressOfInterest;
  unsigned8 length;
  sapMsg* zdoRspMsg;
  sapMsgAps * apsFrame;

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= M_MIN_ACTIVE_EP_REQ_PAYLOAD)
    {
      networkAddressOfInterest = COPY_IN_16_BITS(PayloadData);

      /* Create message for ZDO Response */
      length = ZDO_MESSAGE_OVERHEAD + M_MIN_ACTIVE_EP_RESP_PAYLOAD + ZAB_VND_CFG_MAX_ENDPOINTS;
      zdoRspMsg = sapMsgAllocateData(Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, sapMsgAps_SIZE() + length);
      if ( (erStatusIsError(Status)) || (zdoRspMsg == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, zdoRspMsg, ZAB_MSG_APP_AF_DATA_OUT);
      sapMsgSetAppTransactionId(Status, zdoRspMsg, ZAB_VND_DEFAULT_TID);

      apsFrame = (sapMsgAps *)sapMsgGetAppData(zdoRspMsg);
      apsFrame->dataReq.profile = EMBER_ZDO_PROFILE_ID;
      apsFrame->dataReq.cluster = ACTIVE_ENDPOINTS_RESPONSE;
      apsFrame->dataReq.srcEndpoint = EMBER_ZDO_ENDPOINT;
      apsFrame->dataReq.version  = 0;
      apsFrame->dataReq.radius = 0x0F;
      apsFrame->dataReq.txOptions = 0x00;
      apsFrame->dataReq.dstAddress.addressMode = ZAB_ADDRESS_MODE_NWK_ADDRESS;
      apsFrame->dataReq.dstAddress.address.shortAddress = SourceNetworkAddress;
      apsFrame->dataReq.dstAddress.endpoint = EMBER_ZDO_ENDPOINT;

      index = 0;
      apsFrame->dataReq.data[index++] = ZdoTransSeqNum;

      if (networkAddressOfInterest == zabNcpNwk_GetNwkAddress(Service))
        {
          apsFrame->dataReq.data[index++] = EMBER_ZDP_SUCCESS;
          COPY_OUT_16_BITS(&apsFrame->dataReq.data[index], networkAddressOfInterest); index+=2;

          /* Add endpoints:
           *  - Save index where we will put count of endpoints
           *  - Set count of endpoint to zero in payload
           *  - Add endpoints and save count
           *  - Update count in payload with final count */
          endpointCount = 0;
          endpointCountIndex = index;
          apsFrame->dataReq.data[index++] = 0;
          for (endpointIndex = 0; endpointIndex < ZAB_VND_CFG_MAX_ENDPOINTS; endpointIndex++)
            {
              if (ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex] != NULL)
                {
                  endpointCount++;
                  apsFrame->dataReq.data[index++] = ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->endpoint;
                }
            }
          apsFrame->dataReq.data[endpointCountIndex] = endpointCount;
        }
      else
        {
          apsFrame->dataReq.data[index++] = EMBER_ZDP_DEVICE_NOT_FOUND;
          COPY_OUT_16_BITS(&apsFrame->dataReq.data[index], networkAddressOfInterest); index+=2;
          apsFrame->dataReq.data[index++] = 0; // No endpoints
        }
      apsFrame->dataReq.dataLength = index;
      sapMsgSetAppDataLength(Status, zdoRspMsg, sapMsgAps_SIZE() + index);

      // Deliver APS message to ZigBee Service by DataSAP
      sapMsgPutFree(Status, zabCoreSapData(Service), zdoRspMsg );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process an incoming Simple Descriptor Request
 ******************************************************************************/
static void zabNcpZdo_SimpleDescReqHandler(erStatus* Status, zabService* Service, unsigned16 SourceNetworkAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 index;
  unsigned8 endpointIndex;
  unsigned16 networkAddressOfInterest;
  unsigned8 endpoint;
  unsigned8 length;
  sapMsg* zdoRspMsg;
  sapMsgAps * apsFrame;
  unsigned16 clusterIndex;
  unsigned8 inClusterCount;
  unsigned8 outClusterCount;

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= M_MIN_SIMPLE_DESC_REQ_PAYLOAD)
    {
      index = 0;
      networkAddressOfInterest = COPY_IN_16_BITS(PayloadData); index+=2;
      endpoint = PayloadData[index++];

      /* Create message for ZDO Response */
      length = ZDO_MESSAGE_OVERHEAD + M_MIN_SIMPLE_DESC_RSP_PAYLOAD + M_SIMPLE_DESC_LENGTH(ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT, 0);
      zdoRspMsg = sapMsgAllocateData(Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, sapMsgAps_SIZE() + length);
      if ( (erStatusIsError(Status)) || (zdoRspMsg == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, zdoRspMsg, ZAB_MSG_APP_AF_DATA_OUT);
      sapMsgSetAppTransactionId(Status, zdoRspMsg, ZAB_VND_DEFAULT_TID);

      apsFrame = (sapMsgAps *)sapMsgGetAppData(zdoRspMsg);
      apsFrame->dataReq.profile = EMBER_ZDO_PROFILE_ID;
      apsFrame->dataReq.cluster = SIMPLE_DESCRIPTOR_RESPONSE;
      apsFrame->dataReq.srcEndpoint = EMBER_ZDO_ENDPOINT;
      apsFrame->dataReq.version  = 0;
      apsFrame->dataReq.radius = 0x0F;
      apsFrame->dataReq.txOptions = 0x00;
      apsFrame->dataReq.dstAddress.addressMode = ZAB_ADDRESS_MODE_NWK_ADDRESS;
      apsFrame->dataReq.dstAddress.address.shortAddress = SourceNetworkAddress;
      apsFrame->dataReq.dstAddress.endpoint = EMBER_ZDO_ENDPOINT;

      index = 0;
      apsFrame->dataReq.data[index++] = ZdoTransSeqNum;

      if (networkAddressOfInterest == zabNcpNwk_GetNwkAddress(Service))
        {
          for (endpointIndex = 0; endpointIndex < ZAB_VND_CFG_MAX_ENDPOINTS; endpointIndex++)
            {
              if ( (ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex] != NULL) &&
                   (ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->endpoint == endpoint) )
                {
                  break;
                }
            }
          if (endpointIndex < ZAB_VND_CFG_MAX_ENDPOINTS)
            {
              apsFrame->dataReq.data[index++] = EMBER_ZDP_SUCCESS;
              COPY_OUT_16_BITS(&apsFrame->dataReq.data[index], networkAddressOfInterest); index+=2;

              /* Get the number of clusters, validate and trim lengths */
              inClusterCount = ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->numInClusters;
              outClusterCount = ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->numOutClusters;
              if ( (inClusterCount + outClusterCount) > ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT)
                {
                  if (inClusterCount < ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT)
                    {
                      outClusterCount = ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT-inClusterCount;
                    }
                  else
                    {
                      inClusterCount = ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT;
                      outClusterCount = 0;
                    }
                }
              apsFrame->dataReq.data[index++] = M_SIMPLE_DESC_LENGTH(inClusterCount, outClusterCount);

              apsFrame->dataReq.data[index++] = ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->endpoint;
              COPY_OUT_16_BITS(&apsFrame->dataReq.data[index], ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->profileId); index+=2;
              COPY_OUT_16_BITS(&apsFrame->dataReq.data[index], ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->deviceId); index+=2;
              apsFrame->dataReq.data[index++] = ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->deviceVersion;
              apsFrame->dataReq.data[index++] = inClusterCount;
              for (clusterIndex = 0; clusterIndex < inClusterCount; clusterIndex++)
                {
                  COPY_OUT_16_BITS(&apsFrame->dataReq.data[index], ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->clusterList[clusterIndex]); index+=2;
                }
              apsFrame->dataReq.data[index++] = outClusterCount;
              for (clusterIndex = 0; clusterIndex < outClusterCount; clusterIndex++)
                {
                  COPY_OUT_16_BITS(&apsFrame->dataReq.data[index], ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->clusterList[inClusterCount + clusterIndex]); index+=2;
                }
            }
          else
            {
              apsFrame->dataReq.data[index++] = EMBER_ZDP_NOT_ACTIVE;
              COPY_OUT_16_BITS(&apsFrame->dataReq.data[index], networkAddressOfInterest); index+=2;
              apsFrame->dataReq.data[index++] = 0; // Length
            }
        }
      else
        {
          apsFrame->dataReq.data[index++] = EMBER_ZDP_DEVICE_NOT_FOUND;
          COPY_OUT_16_BITS(&apsFrame->dataReq.data[index], networkAddressOfInterest); index+=2;
          apsFrame->dataReq.data[index++] = 0;
        }
      apsFrame->dataReq.dataLength = index;
      sapMsgSetAppDataLength(Status, zdoRspMsg, sapMsgAps_SIZE() + index);

      // Deliver APS message to ZigBee Service by DataSAP
      sapMsgPutFree(Status, zabCoreSapData(Service), zdoRspMsg );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process an incoming Simple Descriptor Response
 ******************************************************************************/
static void zabNcpZdo_SimpleDescRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceNetworkAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoSimpleDescriptorRespParams_t* simpleDescRsp;
  unsigned8 descriptorLength;
  unsigned8 cluster;
  unsigned8 index;

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= M_MIN_SIMPLE_DESC_RSP_PAYLOAD)
    {
      // Allocate New Message - allocating space for ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT is slightly inefficient, but simpler
      length = SZL_ZdoSimpleDescriptorRespParams_t_SIZE(ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT, 0);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_SIMPLE_DESC_RSP);
      sapMsgSetAppTransactionId(Status, dataMessage, ZdoTransSeqNum);

      simpleDescRsp = (SZL_ZdoSimpleDescriptorRespParams_t*)sapMsgGetAppData(dataMessage);

      /* Extract from the ZNP command into the data sap structure */
      index = 0;
      simpleDescRsp->Status = (SZL_ZDO_STATUS_t)PayloadData[index++];
      simpleDescRsp->NetworkAddress = SourceNetworkAddress;
      simpleDescRsp->NetworkAddressOfInterest = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
      descriptorLength = PayloadData[index++];

      simpleDescRsp->NumInClusters= 0;
      simpleDescRsp->NumOutClusters = 0;

      /* Descriptor is only included if status is success */
      if (simpleDescRsp->Status == SZL_ZDO_STATUS_SUCCESS)
        {
          if ( (PayloadLength >= (M_SIMPLE_DESC_LENGTH(0, 0) + M_MIN_SIMPLE_DESC_RSP_PAYLOAD)) && (descriptorLength >= M_SIMPLE_DESC_LENGTH(0, 0)) )
            {
              simpleDescRsp->Endpoint = PayloadData[index++];
              simpleDescRsp->ProfileId = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
              simpleDescRsp->DeviceId = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
              simpleDescRsp->DeviceVersion = PayloadData[index++];
              simpleDescRsp->NumInClusters = PayloadData[index++];
              simpleDescRsp->InClusterList = simpleDescRsp->ClusterList;
              simpleDescRsp->NumOutClusters = PayloadData[index+(2*simpleDescRsp->NumInClusters)];
              simpleDescRsp->OutClusterList = &simpleDescRsp->ClusterList[simpleDescRsp->NumInClusters];

              /* Check there are not too many clusters for the memory allocated, and that we have enough data for however many there are meant to be */
              if ( ((simpleDescRsp->NumInClusters + simpleDescRsp->NumOutClusters) > ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT) ||
                   (PayloadLength < (M_MIN_SIMPLE_DESC_RSP_PAYLOAD + M_SIMPLE_DESC_LENGTH(simpleDescRsp->NumInClusters, simpleDescRsp->NumOutClusters))) )
                {
                  printError(Service, "ZDO: ERROR: Number of clusters exceeds %d, truncating\n", ZAB_VND_CFG_MAX_CLUSTERS_PER_ENDPOINT);
                  /* Keep it simple for now - can deliver some cluster later if we want */
                  simpleDescRsp->NumInClusters = 0;
                  simpleDescRsp->NumOutClusters = 0;
                }

              /* Add the cluster lists */
              for (cluster = 0; cluster < simpleDescRsp->NumInClusters; cluster++)
                {
                  simpleDescRsp->ClusterList[cluster] = COPY_IN_16_BITS(&PayloadData[index+(2*cluster)]);
                }
              for (cluster = 0; cluster < simpleDescRsp->NumOutClusters; cluster++)
                {
                  simpleDescRsp->ClusterList[simpleDescRsp->NumInClusters + cluster] = COPY_IN_16_BITS(&PayloadData[index+1+(2*(simpleDescRsp->NumInClusters + cluster))]);
                }
            }
          else
            {
              simpleDescRsp->Status = SZL_ZDO_STATUS_INSUFFICIENT_SPACE;
            }
        }

      // Set actual data length and send upwards
      sapMsgSetAppDataLength(Status, dataMessage,  SZL_ZdoSimpleDescriptorRespParams_t_SIZE(simpleDescRsp->NumInClusters, simpleDescRsp->NumOutClusters));
      sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process an incoming Match Descriptor Response
 ******************************************************************************/
static void zabNcpZdo_MatchDescRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceNetworkAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoMatchDescriptorRespParams_t* matchDescRsp;
  unsigned8 endpointIndex;
  unsigned8 index;

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= M_MIN_MATCH_DESC_RSP_PAYLOAD)
    {
      // Allocate New Message - allocating space for M_MATCH_DESC_MAX_NUMBER_OF_ENDPOINTS is slightly inefficient, but simpler
      length = SZL_ZdoMatchDescriptorRespParams_t_SIZE(M_MATCH_DESC_MAX_NUMBER_OF_ENDPOINTS);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_MATCH_DESC_RSP);
      sapMsgSetAppTransactionId(Status, dataMessage, ZdoTransSeqNum);

      matchDescRsp = (SZL_ZdoMatchDescriptorRespParams_t*)sapMsgGetAppData(dataMessage);

      /* Extract from the ZDO command into the data sap structure */
      index = 0;
      matchDescRsp->Status = (SZL_ZDO_STATUS_t)PayloadData[index++];
      matchDescRsp->NetworkAddress = SourceNetworkAddress;
      matchDescRsp->NetworkAddressOfInterest = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
      matchDescRsp->NumberOfEndpoints = PayloadData[index++];

      /* Clip number of endpoints to our limit and ensure there is enough valid data */
      if (matchDescRsp->NumberOfEndpoints > M_MATCH_DESC_MAX_NUMBER_OF_ENDPOINTS)
        {
          matchDescRsp->NumberOfEndpoints = M_MATCH_DESC_MAX_NUMBER_OF_ENDPOINTS;
        }
      if (PayloadLength >= (M_MIN_MATCH_DESC_RSP_PAYLOAD + matchDescRsp->NumberOfEndpoints))
        {
          for (endpointIndex = 0; endpointIndex < matchDescRsp->NumberOfEndpoints; endpointIndex++)
            {
              matchDescRsp->Endpoints[endpointIndex] = PayloadData[index++];
            }

          // Set actual data length and send upwards
          sapMsgSetAppDataLength(Status, dataMessage,  SZL_ZdoMatchDescriptorRespParams_t_SIZE(matchDescRsp->NumberOfEndpoints));
          sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
        }
      else
        {
          sapMsgFree(Status, dataMessage);
          erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
        }
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Generic processing for Bind and Unbind responses
 ******************************************************************************/
static void bindUnbindRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData, zabMsgAppType msgType)
{
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoBindRespParams_t* bindRsp;
  unsigned8 index;

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= M_MIN_BIND_RESP_PAYLOAD)
    {
      // Allocate New Message
      length = SZL_ZdoBindRespParams_t_SIZE;
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, msgType);
      sapMsgSetAppTransactionId(Status, dataMessage, ZdoTransSeqNum);

      /* Build up incoming data structure */
      bindRsp = (SZL_ZdoBindRespParams_t*)sapMsgGetAppData(dataMessage);
      index = 0;
      bindRsp->NetworkAddress = SourceAddress;
      bindRsp->Status = PayloadData[index++];

      sapMsgSetAppDataLength(Status, dataMessage, length);
      sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process an incoming Bind Response
 ******************************************************************************/
static void zabNcpZdo_BindRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  bindUnbindRespHandler(Status, Service, SourceAddress, ZdoTransSeqNum, PayloadLength, PayloadData, ZAB_MSG_APP_ZDO_BIND_RSP);
}

/******************************************************************************
 * Process an incoming Unbind Response
 ******************************************************************************/
static void zabNcpZdo_UnbindRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  bindUnbindRespHandler(Status, Service, SourceAddress, ZdoTransSeqNum, PayloadLength, PayloadData, ZAB_MSG_APP_ZDO_UNBIND_RSP);
}

/******************************************************************************
 * Process an incoming MGMT LQI Response
 ******************************************************************************/
static void zabNcpZdo_MgmtLqiRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoMgmtLqiRespParams_t* lqiRsp;
  unsigned8 index;
  unsigned8 lqiItem;

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= M_MIN_MGMT_LQI_RSP_PAYLOAD)
    {
      // Allocate New Message to send upwards
      length = SZL_ZdoMgmtLqiRespParams_t_SIZE(M_MGMT_LQI_RSP_MAX_NEIGHBOR_TABLE_ITEMS);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_MGMT_LQI_RSP);
      sapMsgSetAppTransactionId(Status, dataMessage, ZdoTransSeqNum);

      /* Build up incoming data structure */
      lqiRsp = (SZL_ZdoMgmtLqiRespParams_t*)sapMsgGetAppData(dataMessage);
      index = 0;
      lqiRsp->NetworkAddress = SourceAddress;
      lqiRsp->Status = PayloadData[index++];

      /* If status is good we now look for the next three bytes*/
      if ( (lqiRsp->Status == SZL_ZDO_STATUS_SUCCESS) &&
           (PayloadLength >= (index + 3)) )
        {
          lqiRsp->NeighborTableEntries = PayloadData[index++];
          lqiRsp->StartIndex = PayloadData[index++];
          lqiRsp->NeighborTableListCount = PayloadData[index++];

          /* Limit the table size */
          if (lqiRsp->NeighborTableListCount > M_MGMT_LQI_RSP_MAX_NEIGHBOR_TABLE_ITEMS)
            {
              lqiRsp->NeighborTableListCount = M_MGMT_LQI_RSP_MAX_NEIGHBOR_TABLE_ITEMS;
            }
          /* If we don't have enough data in the frame, tidy up */
          if (PayloadLength < (index + (lqiRsp->NeighborTableListCount * M_MGMT_LQI_RSP_NEIGHBOR_TABLE_ITEM_SIZE)))
            {
              lqiRsp->NeighborTableListCount = 0;
            }

          for (lqiItem = 0; lqiItem < lqiRsp->NeighborTableListCount; lqiItem++)
            {
              lqiRsp->NeighborTableList[lqiItem].Epid = COPY_IN_64_BITS(&PayloadData[index]); index += MACRO_DATA_SIZE_QUAD_WORD;
              lqiRsp->NeighborTableList[lqiItem].Ieee = COPY_IN_64_BITS(&PayloadData[index]); index += MACRO_DATA_SIZE_QUAD_WORD;
              lqiRsp->NeighborTableList[lqiItem].NetworkAddress = COPY_IN_16_BITS(&PayloadData[index]); index += MACRO_DATA_SIZE_WORD;

              lqiRsp->NeighborTableList[lqiItem].DeviceType = PayloadData[index] & 0x03;
              lqiRsp->NeighborTableList[lqiItem].RxOnWhenIdle = (ENUM_SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_TYPE_t)((PayloadData[index] >> 2) & 0x03);
              lqiRsp->NeighborTableList[lqiItem].Relationship = (ENUM_SZL_ZDO_MGMT_LQI_REALATIONSHIP_TYPE)((PayloadData[index] >> 4) & 0x07);
              index++;

              lqiRsp->NeighborTableList[lqiItem].PermitJoin = PayloadData[index++];
              lqiRsp->NeighborTableList[lqiItem].Depth = PayloadData[index++];
              lqiRsp->NeighborTableList[lqiItem].Lqi = PayloadData[index++];
            }
        }
      else
        {
          lqiRsp->NeighborTableEntries = 0;
          lqiRsp->StartIndex = 0;
          lqiRsp->NeighborTableListCount = 0;
        }

      // Set actual data length and send upwards
      sapMsgSetAppDataLength(Status, dataMessage, SZL_ZdoMgmtLqiRespParams_t_SIZE(lqiRsp->NeighborTableListCount));
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
}

/******************************************************************************
 * Process an incoming MGMT Bind Response
 ******************************************************************************/
static void zabNcpZdo_MgmtBindRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 length;
  unsigned8 index;
  sapMsg* dataMessage = NULL;
  SZL_ZdoMgmtBindRespParams_t* bindRsp;
  unsigned8 bindItem;

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= M_MIN_MGMT_BIND_RSP_PAYLOAD)
    {
      // Allocate New Message to send upwards
      length = SZL_ZdoMgmtBindRespParams_t_SIZE(M_MGMT_LQI_RSP_MAX_NEIGHBOR_TABLE_ITEMS);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_MGMT_BIND_RSP);
      sapMsgSetAppTransactionId(Status, dataMessage, ZdoTransSeqNum);

      /* Build up incoming data structure */
      bindRsp = (SZL_ZdoMgmtBindRespParams_t*)sapMsgGetAppData(dataMessage);
      index = 0;
      bindRsp->SourceNetworkAddress = SourceAddress;
      bindRsp->Status = PayloadData[index++];

      /* If status is good we now look for the next three bytes*/
      if ( (bindRsp->Status == SZL_ZDO_STATUS_SUCCESS) && (PayloadLength >= (index+3)) )
        {
          bindRsp->BindingTableEntries = PayloadData[index++];
          bindRsp->StartIndex = PayloadData[index++];
          bindRsp->BindingTableListCount = PayloadData[index++];

          /* Limit count to the amount of data allocated */
          if (bindRsp->BindingTableListCount > M_MGMT_BIND_RSP_MAX_BINDING_TABLE_ITEMS)
            {
              bindRsp->BindingTableListCount = M_MGMT_BIND_RSP_MAX_BINDING_TABLE_ITEMS;
            }

          for (bindItem = 0; bindItem < bindRsp->BindingTableListCount; bindItem++)
            {
              if (PayloadLength < (index + M_MIN_SIZE_OF_BIND_ITEM))
                {
                  // Command is corrupt, so throw it away
                  sapMsgFree(Status, dataMessage);
                  return;
                }

              bindRsp->BindingTableList[bindItem].SourceIeeeAddress = COPY_IN_64_BITS(&PayloadData[index]); index += MACRO_DATA_SIZE_QUAD_WORD;
              bindRsp->BindingTableList[bindItem].SourceEndpoint = PayloadData[index++];
              bindRsp->BindingTableList[bindItem].ClusterID = COPY_IN_16_BITS(&PayloadData[index]); index += MACRO_DATA_SIZE_WORD;

              /* Set nice defaults, then load real value based on the mode */
              bindRsp->BindingTableList[bindItem].DestinationGroupAddress = 0xFFFF;
              bindRsp->BindingTableList[bindItem].DestinationIeeeAddress = 0xFFFFFFFFFFFFFFFFULL;
              bindRsp->BindingTableList[bindItem].DestinationEndpoint = 0xFF;
              switch((zabAddressMode)PayloadData[index++])
                {
                  case ZAB_ADDRESS_MODE_GROUP:
                    bindRsp->BindingTableList[bindItem].DestinationAddressMode = SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_GROUP;
                    bindRsp->BindingTableList[bindItem].DestinationGroupAddress = COPY_IN_16_BITS(&PayloadData[index]); index += MACRO_DATA_SIZE_WORD;
                    break;

                  case ZAB_ADDRESS_MODE_IEEE_ADDRESS:
                    if (PayloadLength < (index + MACRO_DATA_SIZE_QUAD_WORD + 1))
                      {
                        // Command is corrupt, so throw it away
                        sapMsgFree(Status, dataMessage);
                        return;
                      }
                    bindRsp->BindingTableList[bindItem].DestinationAddressMode = SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_UNICAST;
                    bindRsp->BindingTableList[bindItem].DestinationIeeeAddress = COPY_IN_64_BITS(&PayloadData[index]); index += MACRO_DATA_SIZE_QUAD_WORD;
                    bindRsp->BindingTableList[bindItem].DestinationEndpoint = PayloadData[index++];
                    break;

                  default:
                    // Command is corrupt, so throw it away
                    sapMsgFree(Status, dataMessage);
                    return;
                }
            }
          // Tidy up in case we had less valid entries than expected
          bindRsp->BindingTableListCount = bindItem;
        }
      else
        {
          bindRsp->BindingTableEntries = 0;
          bindRsp->StartIndex = 0;
          bindRsp->BindingTableListCount = 0;
        }

      // Set actual data length and send upwards
      sapMsgSetAppDataLength(Status, dataMessage, SZL_ZdoMgmtBindRespParams_t_SIZE(bindRsp->BindingTableListCount));
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
}

/******************************************************************************
 * Process an incoming MGMT Rtg Response
 ******************************************************************************/
static void zabNcpZdo_MgmtRtgRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 length;
  unsigned8 index;
  sapMsg* dataMessage = NULL;
  SZL_ZdoMgmtRtgRespParams_t* rtgRsp;
  unsigned8 rtgItem;

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= M_MIN_MGMT_RTG_RSP_PAYLOAD)
    {
      // Allocate New Message to send upwards
      length = SZL_ZdoMgmtRtgRespParams_t_SIZE(M_MGMT_RTG_RSP_MAX_TABLE_ITEMS);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_MGMT_RTG_RSP);
      sapMsgSetAppTransactionId(Status, dataMessage, ZdoTransSeqNum);

      /* Build up incoming data structure */
      rtgRsp = (SZL_ZdoMgmtRtgRespParams_t*)sapMsgGetAppData(dataMessage);
      index = 0;
      rtgRsp->SourceNetworkAddress = SourceAddress;
      rtgRsp->Status = PayloadData[index++];

      /* If status is good we now look for the next three bytes*/
      if ( (rtgRsp->Status == SZL_ZDO_STATUS_SUCCESS) && (PayloadLength >= M_MIN_SUCCESS_MGMT_RTG_RSP_PAYLOAD) )
        {
          rtgRsp->RoutingTableEntries = PayloadData[index++];
          rtgRsp->StartIndex = PayloadData[index++];
          rtgRsp->RoutingTableListCount = PayloadData[index++];

          /* Limit count to the amount of data allocated */
          if (rtgRsp->RoutingTableListCount > M_MGMT_RTG_RSP_MAX_TABLE_ITEMS)
            {
              rtgRsp->RoutingTableListCount = M_MGMT_RTG_RSP_MAX_TABLE_ITEMS;
            }
          /* Check length. If not good then cut out the entries */
          if (PayloadLength < (index + (rtgRsp->RoutingTableListCount * M_SIZE_OF_RTG_ITEM)))
            {
              rtgRsp->RoutingTableListCount = 0;
            }

          for (rtgItem = 0; rtgItem < rtgRsp->RoutingTableListCount; rtgItem++)
            {
              rtgRsp->RoutingTableList[rtgItem].DestinationNetworkAddress = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
              rtgRsp->RoutingTableList[rtgItem].RtgStatus = (PayloadData[index++] & 0x07);
              rtgRsp->RoutingTableList[rtgItem].NextHopNetworkAddress = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
            }
        }
      else
        {
          rtgRsp->RoutingTableEntries = 0;
          rtgRsp->StartIndex = 0;
          rtgRsp->RoutingTableListCount = 0;
        }

      // Set actual data length and send upwards
      sapMsgSetAppDataLength(Status, dataMessage, SZL_ZdoMgmtRtgRespParams_t_SIZE(rtgRsp->RoutingTableListCount));
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
}

/******************************************************************************
 * Process an incoming Mgmt Leave Response
 ******************************************************************************/
static void zabNcpZdo_MgmtLeaveRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  zabMsgProMgmtLeaveRsp* leaveRsp;
  unsigned8 index;

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= M_MIN_MGMT_LEAVE_RESP_PAYLOAD)
    {
      // Allocate New Message
      length = zabMsgProMgmtLeaveRsp_SIZE;
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_MGMT_LEAVE_RSP);
      sapMsgSetAppTransactionId(Status, dataMessage, ZdoTransSeqNum);

      /* Build up incoming data structure */
      leaveRsp = (zabMsgProMgmtLeaveRsp*)sapMsgGetAppData(dataMessage);
      index = 0;
      leaveRsp->srcNwkAddress = SourceAddress;
      leaveRsp->status = PayloadData[index++];

      sapMsgSetAppDataLength(Status, dataMessage, length);
      sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process an incoming Mgmt Nwk Update (notify) Response
 ******************************************************************************/
static void zabNcpZdo_MgmtNwkUpdateRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t* updateRsp;
  unsigned8 index;
  unsigned32 chMask;

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= M_MIN_MGMT_NWK_UPDATE_RESP_PAYLOAD)
    {
      // Allocate New Message
      length = SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t_SIZE;
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_MGMT_NWK_UPDATE_RSP);
      sapMsgSetAppTransactionId(Status, dataMessage, ZdoTransSeqNum);

      /* Build up incoming data structure */
      updateRsp = (SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t*)sapMsgGetAppData(dataMessage);
      index = 0;
      updateRsp->NetworkAddress = SourceAddress;
      updateRsp->Status = PayloadData[index++];

      if ( (updateRsp->Status == SZL_ZDO_STATUS_SUCCESS) && (PayloadLength >= M_MIN_SUCCESS_MGMT_NWK_UPDATE_RESP_PAYLOAD))
        {
          chMask = COPY_IN_32_BITS(&PayloadData[index]); index += 4;
          updateRsp->ScannedChannelsMask = (chMask >> 11); // WARNING - this is SZL magic and should not really be done here

          updateRsp->TotalTransmisisons = COPY_IN_16_BITS(&PayloadData[index]); index += 2;
          updateRsp->TransmisisonFailures = COPY_IN_16_BITS(&PayloadData[index]); index += 2;
          updateRsp->ScannedChannelListCount = PayloadData[index++];

          if (PayloadLength >= (M_MIN_SUCCESS_MGMT_NWK_UPDATE_RESP_PAYLOAD + updateRsp->ScannedChannelListCount))
            {
              osMemCopy( Status, updateRsp->EnergyValues, &PayloadData[index], updateRsp->ScannedChannelListCount);
              index += updateRsp->ScannedChannelListCount;
            }
          else
            {
              updateRsp->ScannedChannelListCount = 0;
            }
        }

      sapMsgSetAppDataLength(Status, dataMessage, length);
      sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process an incoming User Descriptor Response
 ******************************************************************************/
static void zabNcpZdo_UserDescRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoUserDescriptorRespParams_t* userDescRsp;
  unsigned8 index;

  /* Check there is at least the minimum frame length in the data.
   * This is an optional command so we must handle a response of status unsupported */
  if (PayloadLength >= M_ZDO_ERROR_RSP_PAYLOAD_LENGTH)
    {
      // Allocate New Message
      length = SZL_ZdoUserDescriptorRespParams_t_SIZE(M_USER_DESCRIPTOR_MAX_LENGTH+1); // +1 for null terminator
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_USER_DESC_RSP);
      sapMsgSetAppTransactionId(Status, dataMessage, ZdoTransSeqNum);

      /* Build up incoming data structure */
      userDescRsp = (SZL_ZdoUserDescriptorRespParams_t*)sapMsgGetAppData(dataMessage);
      index = 0;
      userDescRsp->SourceAddress = SourceAddress;
      userDescRsp->Status = PayloadData[index++];
      if ( (userDescRsp->Status == SZL_ZDO_STATUS_SUCCESS) && (PayloadLength >= M_MIN_USER_DESC_RESP_PAYLOAD) )
        {
          userDescRsp->NetworkAddressOfInterest = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
          userDescRsp->UserDescriptorLength = PayloadData[index++];
          if (userDescRsp->UserDescriptorLength > M_USER_DESCRIPTOR_MAX_LENGTH)
            {
              userDescRsp->UserDescriptorLength = M_USER_DESCRIPTOR_MAX_LENGTH;
            }
          if (PayloadLength >= (index + userDescRsp->UserDescriptorLength))
            {
              osMemCopy( Status, userDescRsp->UserDescriptor, &PayloadData[index], userDescRsp->UserDescriptorLength);
              index += userDescRsp->UserDescriptorLength;
            }
          else
            {
              userDescRsp->UserDescriptorLength = 0;
            }
        }
      else
        {
          userDescRsp->UserDescriptorLength = 0;
        }
      // Null terminate string to be nice to apps
      userDescRsp->UserDescriptor[userDescRsp->UserDescriptorLength] = 0;

      sapMsgSetAppDataLength(Status, dataMessage, length);
      sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process an incoming User Descriptor Set Response
 ******************************************************************************/
static void zabNcpZdo_UserDescSetRespHandler(erStatus* Status, zabService* Service, unsigned16 SourceAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoUserDescriptorSetRespParams_t* userDescSetRsp;
  unsigned8 index;

  /* Check there is at least the minimum frame length in the data.
   * This is an optional command so we must handle a response of status unsupported */
  if (PayloadLength >= M_ZDO_ERROR_RSP_PAYLOAD_LENGTH)
    {
      // Allocate New Message
      length = SZL_ZdoUserDescriptorSetRespParams_t_SIZE;
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_USER_DESC_SET_RSP);
      sapMsgSetAppTransactionId(Status, dataMessage, ZdoTransSeqNum);

      /* Build up incoming data structure */
      userDescSetRsp = (SZL_ZdoUserDescriptorSetRespParams_t*)sapMsgGetAppData(dataMessage);
      index = 0;
      userDescSetRsp->SourceAddress = SourceAddress;
      userDescSetRsp->Status = PayloadData[index++];
      if ( (userDescSetRsp->Status == SZL_ZDO_STATUS_SUCCESS) && (PayloadLength >= M_MIN_USER_DESC_SET_RESP_PAYLOAD) )
        {
          userDescSetRsp->NetworkAddressOfInterest = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
        }
      sapMsgSetAppDataLength(Status, dataMessage, length);
      sapMsgPutFree( Status, zabCoreSapData(Service), dataMessage );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Utility function for processing Match Descriptor Request
 ******************************************************************************/
static unsigned8 addClusterMatchingEndpointToBuffer(zabService* Service, unsigned16 profile, unsigned16 cluster, zab_bool inCluster, unsigned8* buffer, unsigned8 length)
{
  unsigned16* clusterList;
  unsigned8 clisterListLength;
  unsigned8 clusterIndex;
  unsigned8 epIndex;
  unsigned8 endpointIndex;

  for (endpointIndex = 0; endpointIndex < ZAB_VND_CFG_MAX_ENDPOINTS; endpointIndex++)
    {
      if ( (ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex] != NULL) &&
           (ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->profileId == profile) )
        {
          if (inCluster == zab_true)
            {
              clisterListLength = ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->numInClusters;
              clusterList = ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->clusterList;
            }
          else
            {
              clisterListLength = ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->numOutClusters;
              clusterList = &ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->clusterList[ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->numInClusters];
            }

          for (clusterIndex = 0; clusterIndex < clisterListLength; clusterIndex++)
            {
              if (clusterList[clusterIndex] == cluster)
                {
                  for (epIndex = 0; epIndex < length; epIndex++)
                    {
                      if (buffer[epIndex] == ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->endpoint)
                        {
                          // Already added
                          break;
                        }
                    }
                  if (epIndex >= length)
                    {
                      buffer[epIndex] = ZAB_SERVICE_VENDOR(Service)->simpleDesc[endpointIndex]->endpoint;
                      return length+1;
                    }
                }
            }
        }
    }
  return length;
}

/******************************************************************************
 * Process an incoming Match Descriptor Request
 ******************************************************************************/
static void zabNcpZdo_MatchDescReqHandler(erStatus* Status, zabService* Service, unsigned16 SourceNetworkAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 index;
  unsigned16 networkAddressOfInterest;
  unsigned16 profileId;
  unsigned8 length;
  sapMsg* zdoRspMsg;
  sapMsgAps * apsFrame;
  unsigned16 clusterIndex;
  unsigned8 inClusterCount;
  unsigned8 outClusterCount;
  unsigned16 cluster;
  unsigned8 endpointListLength;
  unsigned8 endpointList[ZAB_VND_CFG_MAX_ENDPOINTS];

  /* Check there is at least the minimum frame length in the data*/
  if (PayloadLength >= M_MIN_MATCH_DESC_REQ_PAYLOAD)
    {
      /* Create message for ZDO Response */
      length = ZDO_MESSAGE_OVERHEAD + M_MIN_MATCH_DESC_RSP_PAYLOAD + ZAB_VND_CFG_MAX_ENDPOINTS;
      zdoRspMsg = sapMsgAllocateData(Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, sapMsgAps_SIZE() + length);
      if ( (erStatusIsError(Status)) || (zdoRspMsg == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, zdoRspMsg, ZAB_MSG_APP_AF_DATA_OUT);
      sapMsgSetAppTransactionId(Status, zdoRspMsg, ZAB_VND_DEFAULT_TID);
      apsFrame = (sapMsgAps *)sapMsgGetAppData(zdoRspMsg);
      apsFrame->dataReq.profile = EMBER_ZDO_PROFILE_ID;
      apsFrame->dataReq.cluster = MATCH_DESCRIPTORS_RESPONSE;
      apsFrame->dataReq.srcEndpoint = EMBER_ZDO_ENDPOINT;
      apsFrame->dataReq.version  = 0;
      apsFrame->dataReq.radius = 0x0F;
      apsFrame->dataReq.txOptions = 0x00;
      apsFrame->dataReq.dstAddress.addressMode = ZAB_ADDRESS_MODE_NWK_ADDRESS;
      apsFrame->dataReq.dstAddress.address.shortAddress = SourceNetworkAddress;
      apsFrame->dataReq.dstAddress.endpoint = EMBER_ZDO_ENDPOINT;

      index = 0;
      networkAddressOfInterest = COPY_IN_16_BITS(&PayloadData[index]); index+=2;

      /* If it is for me or a broadcast, then process */
      if ( (networkAddressOfInterest == zabNcpNwk_GetNwkAddress(Service)) || (networkAddressOfInterest >= EMBER_BROADCAST_ADDRESS) )
        {
          profileId = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
          inClusterCount = PayloadData[index++];

          /* Process clusters and add any matching endpoints to endpointList*/
          endpointListLength = 0;
          for (clusterIndex = 0; clusterIndex < inClusterCount; clusterIndex++)
            {
              if (PayloadLength > (index + sizeof(unsigned16)))
                {
                  cluster = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
                  endpointListLength = addClusterMatchingEndpointToBuffer(Service, profileId, cluster, zab_true, endpointList, endpointListLength);
                }
              else
                {
                  erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
                  return;
                }
            }
          outClusterCount = PayloadData[index++];
          for (clusterIndex = 0; clusterIndex < outClusterCount; clusterIndex++)
            {
              if (PayloadLength > (index + sizeof(unsigned16)))
                {
                  cluster = COPY_IN_16_BITS(&PayloadData[index]); index+=2;
                  endpointListLength = addClusterMatchingEndpointToBuffer(Service, profileId, cluster, zab_false, endpointList, endpointListLength);
                }
              else
                {
                  erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
                  return;
                }
            }

          /* Build good response payload */
          index = 0;
          apsFrame->dataReq.data[index++] = ZdoTransSeqNum;
          apsFrame->dataReq.data[index++] = EMBER_ZDP_SUCCESS;
          COPY_OUT_16_BITS(&apsFrame->dataReq.data[index], zabNcpNwk_GetNwkAddress(Service)); index+=2;
          apsFrame->dataReq.data[index++] = endpointListLength;
          osMemCopy(Status, &apsFrame->dataReq.data[index], endpointList, endpointListLength); index+=endpointListLength;
        }
      else
        {
          /* Build error response payload */
          index = 0;
          apsFrame->dataReq.data[index++] = ZdoTransSeqNum;
          apsFrame->dataReq.data[index++] = EMBER_ZDP_DEVICE_NOT_FOUND;
          COPY_OUT_16_BITS(&apsFrame->dataReq.data[index], networkAddressOfInterest); index+=2;
          apsFrame->dataReq.data[index++] = 0;
        }
      apsFrame->dataReq.dataLength = index;
      sapMsgSetAppDataLength(Status, zdoRspMsg, sapMsgAps_SIZE() + index);

      // Deliver APS message to ZigBee Service by DataSAP
      sapMsgPutFree(Status, zabCoreSapData(Service), zdoRspMsg );
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process an incoming Device Announce
 ******************************************************************************/
static void zabNcpZdo_DeviceAnnounceHandler(erStatus* Status, zabService* Service, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 length;
  sapMsg* dataMessage = NULL;
  SZL_ZdoDeviceAnnounceIndParams_t* DeviceAnnounce;
  unsigned8 index;
  #define M_DEVICE_ANNOUNCE_LENGTH 11

  /* Validate length before using data */
  if (PayloadLength >= M_DEVICE_ANNOUNCE_LENGTH)
    {
      // Allocate New Message
      length = sizeof(SZL_ZdoDeviceAnnounceIndParams_t);
      dataMessage = sapMsgAllocateData( Status, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, length);
      if ( (erStatusIsError(Status)) || (dataMessage == NULL) )
        {
          erStatusSet(Status, SAP_ERROR_NULL_PTR);
          return;
        }
      sapMsgSetAppType(Status, dataMessage, ZAB_MSG_APP_ZDO_DEVICE_ANNOUNCE);
      sapMsgSetAppDataLength(Status, dataMessage, length);
      DeviceAnnounce = (SZL_ZdoDeviceAnnounceIndParams_t*)sapMsgGetAppData(dataMessage);

      /* Extract from the ZNP command into the data sap structure */
      index = 0;
      DeviceAnnounce->NwkAddress = COPY_IN_16_BITS(&PayloadData[index]); index += 2;
      DeviceAnnounce->IeeeAddress = COPY_IN_64_BITS(&PayloadData[index]); index += 8;

      if ( (PayloadData[index] & DEVIC_ANNCE_MASK_ALT_PAN_COORD) > 0)
        {
          DeviceAnnounce->AltPanCoordinator = zab_true;
        }
      else
        {
          DeviceAnnounce->AltPanCoordinator = zab_false;
        }
      if ( (PayloadData[index] & DEVIC_ANNCE_MASK_DEV_TYPE_FFD) > 0)
        {
          DeviceAnnounce->DeviceType_Ffd = zab_true;
        }
      else
        {
          DeviceAnnounce->DeviceType_Ffd = zab_false;
        }
      if ( (PayloadData[index] & DEVIC_ANNCE_MASK_POWER_SRC_MAINS) > 0)
        {
          DeviceAnnounce->PowerSource_Mains = zab_true;
        }
      else
        {
          DeviceAnnounce->PowerSource_Mains = zab_false;
        }
      if ( (PayloadData[index] & DEVIC_ANNCE_MASK_RX_ON_WHEN_IDLE) > 0)
        {
          DeviceAnnounce->RxOnWhenIdle = zab_true;
        }
      else
        {
          DeviceAnnounce->RxOnWhenIdle = zab_false;
        }
      if ( (PayloadData[index] & DEVIC_ANNCE_MASK_SECURITY_CAP) > 0)
        {
          DeviceAnnounce->SecurityCapability = zab_true;
        }
      else
        {
          DeviceAnnounce->SecurityCapability = zab_false;
        }
      if ( (PayloadData[index] & DEVIC_ANNCE_MASK_ALLOCATE_ADDR) > 0)
        {
          DeviceAnnounce->AllocateAddress = zab_true;
        }
      else
        {
          DeviceAnnounce->AllocateAddress = zab_false;
        }

      /* Send up through data sap */
      sapMsgPutFree(Status, zabCoreSapData(Service), dataMessage);
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process an incoming Permit Join Request
 * Note: This is handled internally by the NCP. We jsut use it to notify the app
 *       when changes are received from the network
 ******************************************************************************/
static void zabNcpZdo_PermitJoinReqHandler(erStatus* Status, zabService* Service, unsigned16 SourceNetworkAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 index;
  unsigned8 duration;
  #define M_PERMIT_JOIN_REQ_LENGTH 2

  /* Validate length before using data */
  if (PayloadLength >= M_PERMIT_JOIN_REQ_LENGTH)
    {
      index = 0;
      duration = PayloadData[index++];
      // tcSignificance ignored - silabs ignore it too

      zabNcpNwk_PermitJoinRequestHandler(Status, Service, SourceNetworkAddress, duration);
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process an incoming Leave Request
 ******************************************************************************/
static void zabNcpZdo_LeaveReqHandler(erStatus* Status, zabService* Service, unsigned16 SourceNetworkAddress, unsigned8 ZdoTransSeqNum, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 index;
  unsigned64 ieee;
  unsigned8 flags;
  zab_bool rejoin;
  #define M_LEAVE_REQ_LENGTH 9 // IEEE(8), Flags(1)

  /* Validate length before using data */
  if (PayloadLength >= M_LEAVE_REQ_LENGTH)
    {
      index = 0;
      ieee = COPY_IN_64_BITS(&PayloadData[index]); index+=8;
      flags = PayloadData[index++];
      if (flags & LEAVE_REQUEST_REJOIN_FLAG)
        {
          rejoin = zab_true;
        }
      else
        {
          rejoin = zab_false;
        }

      zabNcpNwk_LeaveRequestHandler(Status, Service, ieee, rejoin);
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * ZDO IEEE Address Request
 ******************************************************************************/
void zabNcpZdo_IeeeAddressReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoIeeeAddrReqParams_t* dp;
  EmberApsFrame apsFrame = {EMBER_ZDO_PROFILE_ID, IEEE_ADDRESS_REQUEST, EMBER_ZDO_ENDPOINT, EMBER_ZDO_ENDPOINT,
                            EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY,
                            0x0000/*groupId*/, 0x00/*sequence*/};
  unsigned8 zdpFrame[ZDO_MESSAGE_OVERHEAD + 4];
  unsigned8 index;

  dp = (SZL_ZdoIeeeAddrReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  index = 0;
  zdpFrame[index++] = sapMsgGetAppTransactionId(Message);
  COPY_OUT_16_BITS(&zdpFrame[index], dp->NetworkAddress); index+=2;
  zdpFrame[index++] = (unsigned8)dp->RequestType;
  zdpFrame[index++] = dp->StartIndex;

  zabNcpEzspMessaging_SendUnicast(Status, Service,
                                  EMBER_OUTGOING_DIRECT,
                                  dp->NetworkAddress,
                                  &apsFrame,
                                  sapMsgGetAppTransactionId(Message), // message tag
                                  index,
                                  zdpFrame);
}

/******************************************************************************
 * ZDO Network Address Request
 ******************************************************************************/
void zabNcpZdo_NwkAddressReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoNwkAddrReqParams_t* dp;
  EmberApsFrame apsFrame = {EMBER_ZDO_PROFILE_ID, NETWORK_ADDRESS_REQUEST, EMBER_ZDO_ENDPOINT, EMBER_ZDO_ENDPOINT,
                            EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY,
                            0x0000/*groupId*/, 0x00/*sequence*/};
  unsigned8 zdpFrame[ZDO_MESSAGE_OVERHEAD + M_NWK_ADDR_REQ_PAYLOAD_LENGTH];
  unsigned8 index;

  dp = (SZL_ZdoNwkAddrReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  index = 0;
  zdpFrame[index++] = sapMsgGetAppTransactionId(Message);
  COPY_OUT_64_BITS(&zdpFrame[index], dp->IeeeAddress); index+=8;
  zdpFrame[index++] = (unsigned8)dp->RequestType;
  zdpFrame[index++] = dp->StartIndex;

  zabNcpEzspMessaging_SendBroadcast(Status, Service,
                                    EMBER_BROADCAST_ADDRESS,
                                    &apsFrame,
                                    0, // Zero is converted to EMBER_MAX_HOPS
                                    sapMsgGetAppTransactionId(Message), // message tag
                                    index,
                                    zdpFrame);
}

/******************************************************************************
 * Generic descriptor request
 ******************************************************************************/
static void genericNodeDescriptorReq(erStatus* Status, zabService* Service, unsigned8 TransactionId, unsigned16 ClusterId, unsigned16 DstNwkAddress, unsigned16 NwkAddressOfInterest)
{
  EmberApsFrame apsFrame = {EMBER_ZDO_PROFILE_ID, ClusterId, EMBER_ZDO_ENDPOINT, EMBER_ZDO_ENDPOINT,
                            EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY,
                            0x0000/*groupId*/, 0x00/*sequence*/};
  unsigned8 zdpFrame[ZDO_MESSAGE_OVERHEAD + 2];
  unsigned8 index;

  index = 0;
  zdpFrame[index++] = TransactionId;
  COPY_OUT_16_BITS(&zdpFrame[index], NwkAddressOfInterest); index+=2;

  zabNcpEzspMessaging_SendUnicast(Status, Service,
                                  EMBER_OUTGOING_DIRECT,
                                  DstNwkAddress,
                                  &apsFrame,
                                  TransactionId,
                                  index,
                                  zdpFrame);
}

/******************************************************************************
 * ZDO Node Descriptor Request
 ******************************************************************************/
void zabNcpZdo_NodeDescReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoNodeDescriptorReqParams_t* dp;
  dp = (SZL_ZdoNodeDescriptorReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  genericNodeDescriptorReq(Status, Service, sapMsgGetAppTransactionId(Message), NODE_DESCRIPTOR_REQUEST, dp->NetworkAddress, dp->NetworkAddressOfInterest);
}

/******************************************************************************
 * ZDO Power Descriptor Request
 ******************************************************************************/
void zabNcpZdo_PowerDescReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoPowerDescriptorReqParams_t* dp;
  dp = (SZL_ZdoPowerDescriptorReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  genericNodeDescriptorReq(Status, Service, sapMsgGetAppTransactionId(Message), POWER_DESCRIPTOR_REQUEST, dp->NetworkAddress, dp->NetworkAddressOfInterest);
}

/******************************************************************************
 * ZDO Active Endpoint Request
 ******************************************************************************/
void zabNcpZdo_ActiveEndpointReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoActiveEndpointReqParams_t* dp;
  dp = (SZL_ZdoActiveEndpointReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  genericNodeDescriptorReq(Status, Service, sapMsgGetAppTransactionId(Message), ACTIVE_ENDPOINTS_REQUEST, dp->NetworkAddress, dp->NetworkAddressOfInterest);
}

/******************************************************************************
 * ZDO Simple Descriptor Request
 ******************************************************************************/
void zabNcpZdo_SimpleDescReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoSimpleDescriptorReqParams_t* dp;
  EmberApsFrame apsFrame = {EMBER_ZDO_PROFILE_ID, SIMPLE_DESCRIPTOR_REQUEST, EMBER_ZDO_ENDPOINT, EMBER_ZDO_ENDPOINT,
                            EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY,
                            0x0000/*groupId*/, 0x00/*sequence*/};
  unsigned8 zdpFrame[ZDO_MESSAGE_OVERHEAD + 3];
  unsigned8 index;

  dp = (SZL_ZdoSimpleDescriptorReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);


  index = 0;
  zdpFrame[index++] = sapMsgGetAppTransactionId(Message);
  COPY_OUT_16_BITS(&zdpFrame[index], dp->NetworkAddress); index+=2;
  zdpFrame[index++] = dp->Endpoint;

  zabNcpEzspMessaging_SendUnicast(Status, Service,
                                  EMBER_OUTGOING_DIRECT,
                                  dp->NetworkAddress,
                                  &apsFrame,
                                  sapMsgGetAppTransactionId(Message), // message tag
                                  index,
                                  zdpFrame);
}

/******************************************************************************
 * ZDO Match Descriptor Request
 ******************************************************************************/
void zabNcpZdo_MatchDescReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoMatchDescriptorReqParams_t* dp;
  EmberApsFrame apsFrame = {EMBER_ZDO_PROFILE_ID, MATCH_DESCRIPTORS_REQUEST, EMBER_ZDO_ENDPOINT, EMBER_ZDO_ENDPOINT,
                            EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY,
                            0x0000/*groupId*/, 0x00/*sequence*/};
  unsigned8 zdpFrame[ZDO_MESSAGE_OVERHEAD + M_MATCH_DESC_LENGTH(M_MATCH_DESC_MAX_NUMBER_OF_CLUSTERS, 0)];
  unsigned8 index;
  unsigned8 clusterIndex;

  dp = (SZL_ZdoMatchDescriptorReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  if ( (dp->NumInClusters + dp->NumOutClusters) <= M_MATCH_DESC_MAX_NUMBER_OF_CLUSTERS)
    {
      index = 0;
      zdpFrame[index++] = sapMsgGetAppTransactionId(Message);
      COPY_OUT_16_BITS(&zdpFrame[index], dp->NetworkAddressOfInterest); index+=2;
      COPY_OUT_16_BITS(&zdpFrame[index], dp->ProfileId); index+=2;
      zdpFrame[index++] = dp->NumInClusters;
      for (clusterIndex = 0; clusterIndex < dp->NumInClusters; clusterIndex++)
        {
          COPY_OUT_16_BITS(&zdpFrame[index], dp->InClusterList[clusterIndex]); index+=2;
        }
      zdpFrame[index++] = dp->NumOutClusters;
      for (clusterIndex = 0; clusterIndex < dp->NumOutClusters; clusterIndex++)
        {
          COPY_OUT_16_BITS(&zdpFrame[index], dp->OutClusterList[clusterIndex]); index+=2;
        }

      zabNcpZdo_SendMessage(Status, Service,
                            dp->NetworkAddress,
                            &apsFrame,
                            sapMsgGetAppTransactionId(Message), // message tag
                            index,
                            zdpFrame);
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_PARSE);
    }
}

/******************************************************************************
 * Generic Bind/Unbind Request
 ******************************************************************************/
static void bindUnbindReq(erStatus* Status, zabService* Service, sapMsg* Message, unsigned16 ClusterId)
{
  SZL_ZdoBindReqParams_t* dp;
  EmberApsFrame apsFrame = {EMBER_ZDO_PROFILE_ID, ClusterId, EMBER_ZDO_ENDPOINT, EMBER_ZDO_ENDPOINT,
                            EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY,
                            0x0000/*groupId*/, 0x00/*sequence*/};
  unsigned8 zdpFrame[ZDO_MESSAGE_OVERHEAD + M_MAX_SIZE_OF_BIND_ITEM];
  unsigned8 index;

  dp = (SZL_ZdoBindReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  /* Build the ZDP Frame */
  index = 0;
  zdpFrame[index++] = sapMsgGetAppTransactionId(Message);
  COPY_OUT_64_BITS(&zdpFrame[index], dp->SourceIeeeAddress); index += MACRO_DATA_SIZE_QUAD_WORD;
  zdpFrame[index++] = dp->SourceEndpoint;
  COPY_OUT_16_BITS(&zdpFrame[index], dp->ClusterID); index += MACRO_DATA_SIZE_WORD;

  /* Convert address into ZigBee format */
  switch (dp->DestinationAddressMode)
    {
      case SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_GROUP:
        zdpFrame[index++] = ZAB_ADDRESS_MODE_GROUP;
        COPY_OUT_16_BITS(&zdpFrame[index], dp->DestinationGroupAddress); index += MACRO_DATA_SIZE_WORD;
        break;

      case SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_UNICAST:
        zdpFrame[index++] = ZAB_ADDRESS_MODE_IEEE_ADDRESS;
        COPY_OUT_64_BITS(&zdpFrame[index], dp->DestinationIeeeAddress); index += MACRO_DATA_SIZE_QUAD_WORD;
        zdpFrame[index++] = dp->DestinationEndpoint;
        break;

      default:
        erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
        break;
    }

  zabNcpEzspMessaging_SendUnicast(Status, Service,
                                  EMBER_OUTGOING_DIRECT,
                                  dp->NetworkAddress,
                                  &apsFrame,
                                  sapMsgGetAppTransactionId(Message),
                                  index,
                                  zdpFrame);
}

/******************************************************************************
 * Bind Request
 ******************************************************************************/
void zabNcpZdo_BindReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  bindUnbindReq(Status, Service, Message, BIND_REQUEST);
}

/******************************************************************************
 * Unbind Request
 ******************************************************************************/
void zabNcpZdo_UnbindReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  bindUnbindReq(Status, Service, Message, UNBIND_REQUEST);
}

/******************************************************************************
 * MGMT LQI Request
 ******************************************************************************/
void zabNcpZdo_MgmtLqiReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoMgmtLqiReqParams_t* dp;
  EmberApsFrame apsFrame = {EMBER_ZDO_PROFILE_ID, LQI_TABLE_REQUEST, EMBER_ZDO_ENDPOINT, EMBER_ZDO_ENDPOINT,
                            EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY,
                            0x0000/*groupId*/, 0x00/*sequence*/};
  unsigned8 zdpFrame[ZDO_MESSAGE_OVERHEAD + 1];
  unsigned8 index;

  dp = (SZL_ZdoMgmtLqiReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);


  index = 0;
  zdpFrame[index++] = sapMsgGetAppTransactionId(Message);
  zdpFrame[index++] = dp->StartIndex;

  zabNcpEzspMessaging_SendUnicast(Status, Service,
                                  EMBER_OUTGOING_DIRECT,
                                  dp->NetworkAddress,
                                  &apsFrame,
                                  sapMsgGetAppTransactionId(Message),
                                  index,
                                  zdpFrame);
}

/******************************************************************************
 * MGMT Bind Request
 ******************************************************************************/
void zabNcpZdo_MgmtBindReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoMgmtBindReqParams_t* dp;
  EmberApsFrame apsFrame = {EMBER_ZDO_PROFILE_ID, BINDING_TABLE_REQUEST, EMBER_ZDO_ENDPOINT, EMBER_ZDO_ENDPOINT,
                            EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY,
                            0x0000/*groupId*/, 0x00/*sequence*/};
  unsigned8 zdpFrame[ZDO_MESSAGE_OVERHEAD + 1];
  unsigned8 index;

  dp = (SZL_ZdoMgmtBindReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  index = 0;
  zdpFrame[index++] = sapMsgGetAppTransactionId(Message);
  zdpFrame[index++] = dp->StartIndex;

  zabNcpEzspMessaging_SendUnicast(Status, Service,
                                  EMBER_OUTGOING_DIRECT,
                                  dp->NetworkAddress,
                                  &apsFrame,
                                  sapMsgGetAppTransactionId(Message),
                                  index,
                                  zdpFrame);
}

/******************************************************************************
 * MGMT Routing Request
 ******************************************************************************/
void zabNcpZdo_MgmtRtgReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoMgmtRtgReqParams_t* dp;
  EmberApsFrame apsFrame = {EMBER_ZDO_PROFILE_ID, ROUTING_TABLE_REQUEST, EMBER_ZDO_ENDPOINT, EMBER_ZDO_ENDPOINT,
                            EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY,
                            0x0000/*groupId*/, 0x00/*sequence*/};
  unsigned8 zdpFrame[ZDO_MESSAGE_OVERHEAD + 1];
  unsigned8 index;

  dp = (SZL_ZdoMgmtRtgReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  index = 0;
  zdpFrame[index++] = sapMsgGetAppTransactionId(Message);
  zdpFrame[index++] = dp->StartIndex;

  zabNcpEzspMessaging_SendUnicast(Status, Service,
                                  EMBER_OUTGOING_DIRECT,
                                  dp->NetworkAddress,
                                  &apsFrame,
                                  sapMsgGetAppTransactionId(Message),
                                  index,
                                  zdpFrame);
}

/******************************************************************************
 * MGMT Leave Request
 ******************************************************************************/
void zabNcpZdo_MgmtLeaveReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_NwkLeaveReqParams_t* dp;
  EmberApsFrame apsFrame = {EMBER_ZDO_PROFILE_ID, LEAVE_REQUEST, EMBER_ZDO_ENDPOINT, EMBER_ZDO_ENDPOINT,
                            EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY,
                            0x0000/*groupId*/, 0x00/*sequence*/};
  unsigned8 zdpFrame[ZDO_MESSAGE_OVERHEAD + M_MGMT_LEAVE_REQ_PAYLOAD_LENGTH];
  unsigned8 index;

  dp = (SZL_NwkLeaveReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  index = 0;
  zdpFrame[index++] = sapMsgGetAppTransactionId(Message);
  COPY_OUT_64_BITS(&zdpFrame[index], dp->DeviceIEEEAddress); index += MACRO_DATA_SIZE_QUAD_WORD;

  /* Set flags - we never set remove children */
  zdpFrame[index] = 0;
  if (dp->Rejoin)
    {
      zdpFrame[index] |= LEAVE_REQUEST_REJOIN_FLAG;
    }
  index++;

  zabNcpEzspMessaging_SendUnicast(Status, Service,
                                  EMBER_OUTGOING_DIRECT,
                                  dp->DeviceNwkAddress,
                                  &apsFrame,
                                  sapMsgGetAppTransactionId(Message),
                                  index,
                                  zdpFrame);
}

/******************************************************************************
 * MGMT Nwk Update Request
 ******************************************************************************/
void zabNcpZdo_MgmtNwkUpdateReq(erStatus* Status, zabService* Service, unsigned8 TransactionId, zabMsgProMgmtNwkUpdateReq* NwkUpdateReq)
{
  EmberApsFrame apsFrame = {EMBER_ZDO_PROFILE_ID, NWK_UPDATE_REQUEST, EMBER_ZDO_ENDPOINT, EMBER_ZDO_ENDPOINT,
                            EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY,
                            0x0000/*groupId*/, 0x00/*sequence*/};
  unsigned8 zdpFrame[ZDO_MESSAGE_OVERHEAD + M_MGMT_NWK_UPDATE_REQ_MAX_PAYLOAD_LENGTH];
  unsigned8 index;

  index = 0;
  zdpFrame[index++] = TransactionId;
  COPY_OUT_32_BITS(&zdpFrame[index], NwkUpdateReq->ChannelMask); index += 4;
  zdpFrame[index++] = NwkUpdateReq->ScanDuration;

  if (NwkUpdateReq->ScanDuration <= 5)
    {
      zdpFrame[index++] = NwkUpdateReq->ScanCount;
    }
  if ( (NwkUpdateReq->ScanDuration == 0xFE) || (NwkUpdateReq->ScanDuration == 0xFF) )
    {
      zdpFrame[index++] = zabNcpNwk_GetNwkUpdateId(Service);
    }
  if (NwkUpdateReq->ScanDuration == 0xFF)
    {
      COPY_OUT_16_BITS(&zdpFrame[index], NwkUpdateReq->NetworkManagerAddress); index += 2;
    }

  zabNcpZdo_SendMessage(Status, Service,
                        NwkUpdateReq->DestinationAddress,
                        &apsFrame,
                        TransactionId,
                        index,
                        zdpFrame);
}

/******************************************************************************
 * MGMT Nwk Update Request - Conversion from sapMsg
 ******************************************************************************/
void zabNcpZdo_MgmtNwkUpdateReqConversion(erStatus* Status, zabService* Service, sapMsg* Message)
{
  zabMsgProMgmtNwkUpdateReq* dp;

  dp = (zabMsgProMgmtNwkUpdateReq*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  zabNcpZdo_MgmtNwkUpdateReq(Status, Service, sapMsgGetAppTransactionId(Message), dp);
}

/******************************************************************************
 * ZDO Permit Join Request
 ******************************************************************************/
void zabNcpZdo_PermitJoinReq(erStatus* Status, zabService* Service, unsigned16 NodeId, unsigned8 Duration, zab_bool TcSignificance)
{
  EmberApsFrame apsFrame = {EMBER_ZDO_PROFILE_ID, PERMIT_JOINING_REQUEST, EMBER_ZDO_ENDPOINT, EMBER_ZDO_ENDPOINT,
                            EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY,
                            0x0000/*groupId*/, 0x00/*sequence*/};
  unsigned8 zdpFrame[ZDO_MESSAGE_OVERHEAD + 2];
  unsigned8 index;
  ER_CHECK_STATUS(Status);

  index = 0;
  zdpFrame[index++] = 0x55; // zdo tid
  zdpFrame[index++] = Duration;
  zdpFrame[index++] = (unsigned8)TcSignificance;

  if ( (NodeId == EMBER_BROADCAST_ADDRESS) ||
       (NodeId == EMBER_RX_ON_WHEN_IDLE_BROADCAST_ADDRESS) ||
       (NodeId == EMBER_SLEEPY_BROADCAST_ADDRESS) )
    {
      zabNcpEzspMessaging_SendBroadcast(Status, Service,
                                        NodeId,
                                        &apsFrame,
                                        0, // Zero is converted to EMBER_MAX_HOPS
                                        ZAB_VND_DEFAULT_TID, // message tag
                                        index,
                                        zdpFrame);
    }
  else
    {
      zabNcpEzspMessaging_SendUnicast(Status, Service,
                                      EMBER_OUTGOING_DIRECT,
                                      NodeId,
                                      &apsFrame,
                                      ZAB_VND_DEFAULT_TID, // message tag
                                      index,
                                      zdpFrame);
    }
}

/******************************************************************************
 * User Descriptor Request
 ******************************************************************************/
void zabNcpZdo_UserDescReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoUserDescriptorReqParams_t* dp;
  EmberApsFrame apsFrame = {EMBER_ZDO_PROFILE_ID, USER_DESCRIPTOR_REQUEST, EMBER_ZDO_ENDPOINT, EMBER_ZDO_ENDPOINT,
                            EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY,
                            0x0000/*groupId*/, 0x00/*sequence*/};
  unsigned8 zdpFrame[ZDO_MESSAGE_OVERHEAD + M_USER_DESC_REQ_PAYLOAD_LENGTH];
  unsigned8 index;

  dp = (SZL_ZdoUserDescriptorReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  index = 0;
  zdpFrame[index++] = sapMsgGetAppTransactionId(Message);
  COPY_OUT_16_BITS(&zdpFrame[index], dp->NetworkAddressOfInterest); index+=2;

  zabNcpEzspMessaging_SendUnicast(Status, Service,
                                  EMBER_OUTGOING_DIRECT,
                                  dp->NetworkAddress,
                                  &apsFrame,
                                  sapMsgGetAppTransactionId(Message),
                                  index,
                                  zdpFrame);
}

/******************************************************************************
 * User Descriptor Set Request
 ******************************************************************************/
void zabNcpZdo_UserDescSetReq(erStatus* Status, zabService* Service, sapMsg* Message)
{
  SZL_ZdoUserDescriptorSetReqParams_t* dp;
  EmberApsFrame apsFrame = {EMBER_ZDO_PROFILE_ID, USER_DESCRIPTOR_SET, EMBER_ZDO_ENDPOINT, EMBER_ZDO_ENDPOINT,
                            EMBER_APS_OPTION_RETRY | EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY,
                            0x0000/*groupId*/, 0x00/*sequence*/};
  unsigned8 zdpFrame[ZDO_MESSAGE_OVERHEAD + M_USER_DESC_SET_REQ_PAYLOAD_LENGTH];
  unsigned8 index;

  dp = (SZL_ZdoUserDescriptorSetReqParams_t*)sapMsgGetAppData(Message);
  ER_CHECK_STATUS_NULL(Status, dp);

  index = 0;
  zdpFrame[index++] = sapMsgGetAppTransactionId(Message);
  COPY_OUT_16_BITS(&zdpFrame[index], dp->NetworkAddressOfInterest); index+=2;
  zdpFrame[index++] = dp->UserDescriptorLength;
  if (dp->UserDescriptorLength > M_USER_DESCRIPTOR_MAX_LENGTH)
    {
      dp->UserDescriptorLength = M_USER_DESCRIPTOR_MAX_LENGTH;
    }
  osMemCopy( Status, &zdpFrame[index], dp->UserDescriptor, dp->UserDescriptorLength); index+=dp->UserDescriptorLength;

  zabNcpEzspMessaging_SendUnicast(Status, Service,
                                  EMBER_OUTGOING_DIRECT,
                                  dp->NetworkAddress,
                                  &apsFrame,
                                  sapMsgGetAppTransactionId(Message),
                                  index,
                                  zdpFrame);
}

/******************************************************************************
 * Process incoming ZDO messages
 ******************************************************************************/
void zabNcpZdo_IncomingMessageHandler(erStatus* Status, zabService* Service, unsigned16 SourceAddress, EmberApsFrame* ApsFrame, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 zdoTransSeqNum;
  unsigned8 cmdPayloadLength;
  unsigned8* cmdPayloadData;

  printVendor(Service, "zabNcpZdo_IncomingMessageHandler: Cluster 0x%04X, PayloadLength %d\n", ApsFrame->clusterId, PayloadLength);

  if (PayloadLength >= 1)
    {
      zdoTransSeqNum = PayloadData[0];
      cmdPayloadLength = PayloadLength-1;
      cmdPayloadData = &PayloadData[1];

      switch(ApsFrame->clusterId)
        {
          case NETWORK_ADDRESS_RESPONSE:     zabNcpZdo_NwkAddrRespHandler(Status, Service, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case IEEE_ADDRESS_RESPONSE:        zabNcpZdo_IeeeAddrRespHandler(Status, Service, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case END_DEVICE_ANNOUNCE:          zabNcpZdo_DeviceAnnounceHandler(Status, Service, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;

          case ACTIVE_ENDPOINTS_RESPONSE:    zabNcpZdo_ActiveEpRespHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case SIMPLE_DESCRIPTOR_RESPONSE:   zabNcpZdo_SimpleDescRespHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case MATCH_DESCRIPTORS_RESPONSE:   zabNcpZdo_MatchDescRespHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case NODE_DESCRIPTOR_RESPONSE:     zabNcpZdo_NodeDescRespHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case POWER_DESCRIPTOR_RESPONSE:    zabNcpZdo_PowerDescRespHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case BIND_RESPONSE:                zabNcpZdo_BindRespHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case UNBIND_RESPONSE:              zabNcpZdo_UnbindRespHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case LQI_TABLE_RESPONSE:           zabNcpZdo_MgmtLqiRespHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case BINDING_TABLE_RESPONSE:       zabNcpZdo_MgmtBindRespHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case ROUTING_TABLE_RESPONSE:       zabNcpZdo_MgmtRtgRespHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case LEAVE_RESPONSE:               zabNcpZdo_MgmtLeaveRespHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case NWK_UPDATE_RESPONSE:          zabNcpZdo_MgmtNwkUpdateRespHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case USER_DESCRIPTOR_RESPONSE:     zabNcpZdo_UserDescRespHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case USER_DESCRIPTOR_CONFIRM:      zabNcpZdo_UserDescSetRespHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;

          /* Incoming requests that ZAB has decided to handle rather than leave to the stack */
          case ACTIVE_ENDPOINTS_REQUEST:     zabNcpZdo_ActiveEpReqHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case SIMPLE_DESCRIPTOR_REQUEST:    zabNcpZdo_SimpleDescReqHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case MATCH_DESCRIPTORS_REQUEST:    zabNcpZdo_MatchDescReqHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;

          /* Notifications of commands that are handled by the stack */
          case PERMIT_JOINING_REQUEST:       zabNcpZdo_PermitJoinReqHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;
          case LEAVE_REQUEST:                zabNcpZdo_LeaveReqHandler(Status, Service, SourceAddress, zdoTransSeqNum, cmdPayloadLength, cmdPayloadData); break;

          default:
            printVendor(Service, "zabNcpZdo_IncomingMessageHandler: Unhandled Cluster 0x%04X\n", ApsFrame->clusterId);
        }
    }
}
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/