/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains utility functions for the Wireless Test Bench
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev     Date     Author  Change Description
 *  00.00.06.00  12-Jun-14   MvdB   Original
 *****************************************************************************/

#ifndef _WTB_UTILITY_H
#define _WTB_UTILITY_H

#ifdef ZAB_VENDOR_TI_ZNP

#include "zabZnpFrame.h"
#include "zabParseProPrivate.h"

/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Type of ZNP Data Confirm */
typedef enum
{
  wtbUtility_ZnpCnfType_Srsp,
  wtbUtility_ZnpCnfType_DataConfirm,
} wtbUtility_ZnpCnfType_t;

/* Data Request Notification
 *  - Used for both SZL and ZNP notifications */
typedef void (*wtbUtility_DataReqNotification_t) (zabService* Service,
                                                  unsigned8 TransactionId,
                                                  zabMsgProDataReq* DataRequest);

/* ZNP Data Confirm Notification
 *  - Called for both SRSP and Data Confirm
 *  - If ZnpStatus for SRSP is not ZNPI_API_ERR_SUCCESS then there will be no data confirm*/
typedef void (*wtbUtility_ZnpDataCnfNotification_t) (zabService* Service,
                                                     unsigned8 TransactionId,
                                                     wtbUtility_ZnpCnfType_t ConfirmType,
                                                     ZNPI_API_ERROR_CODES ZnpStatus);

/* SZL Data Request Error Notification
 *  - Used to notify of an error with a data request from in the SZL */
typedef void (*wtbUtility_SzlDataErrorNotification_t) (zabService* Service,
                                                       unsigned8 TransactionId,
                                                       zabError ErrorCode);

/* Data Indication Notification - Used for both SZL and ZNP notifications */
typedef void (*wtbUtility_DataIndNotification_t) (zabService* Service,
                                                  zabMsgProDataInd* DataIndication);

#endif // ZAB_VENDOR_TI_ZNP


#endif /* _WTB_UTILITY_H */