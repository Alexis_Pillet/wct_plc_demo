/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the SiLabs NCP Network State Machine
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.005  30-Jun-16   MvdB   Support energy scan for quietest channel during form
 * 000.000.009  12-Jul-16   MvdB   Give network address during network Init/Form actions
 *                                 Support ZAB_ACTION_NWK_GET_INFO - INCOMPLETE SiLabs Case 110845 - does not currently give good network address or device type
 * 000.000.012  19-Jul-16   MvdB   Move utilities functions from zabNcpEzspConfiguration into zabNcpEzspUtilities
 * 000.000.013  21-Jul-16   MvdB   Support Network Discover and Join
 *                                 Upgrade ZDO responses to use correct network address, not just 0
 * 000.000.014  22-Jul-16   MvdB   Support ZAB_ACTION_SET_TX_POWER
 * 000.000.016  25-Jul-16   MvdB   Support ZAB_ACTION_CHANGE_CHANNEL and other a-synchronous network parameter changes
 * 000.000.017  26-Jul-16   MvdB   Support ZAB_ACTION_CHANGE_NETWORK_KEY
 * 000.000.019  26-Jul-16   MvdB   Tidy up permit join notifications
 * 000.000.020  27-Jul-16   MvdB   Add network processor ping: Add zabNcpNwk_GetNwkStateMachineInProgress()
 * 000.000.021  28-Jul-16   MvdB   General review, commenting and tidy up
 * 000.000.023  02-Aug-16   MvdB   Support better getting of TxPower
 *                                 Set MS Code in node descriptor during network init
 *                                 Handle leave requests from network cleanly
 * 002.002.038  12-Jan-17   MvdB   ARTF113872: Deny key change unless node is Trust Center (it fails if done from other devices)
 * 002.002.039  17-Jan-17   MvdB   ARTF175875: In progress (binding table copy synchronised but not yet used)
 *                                  - Support policy setting in Nwk init and set binding policy to EZSP_ALLOW_BINDING_MODIFICATION
 *                                  - Clear binding table on network join/form. Read binding table on network start.
 * 002.002.040  17-Jan-17   MvdB   ARTF175875: Add zabNcpNwk_GetBindingIndex() to support send via bind
 * 002.002.041  18-Jan-17   MvdB   ARTF199128: Fix NCP networking after reception of a leave command
 *****************************************************************************/

#include <stdint.h>
#include "ember-types.h"
#include "ezsp-enum.h"
#include "zabNcpService.h"
#include "zabNcpPrivate.h"
#include "zabNcpEzspMfglib.h"
#include "zabNcpEzspNetworking.h"
#include "zabNcpEzspBinding.h"
#include "zabNcpEzspSecurity.h"
#include "zabNcpEzspConfiguration.h"
#include "zabNcpEzspTrustCenter.h"
#include "zabNcpEzspUtilities.h"
#include "zabNcpOpen.h"
#include "zabNcpZdo.h"

#include "zabNcpNwk.h"

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

#define M_DEFAULT_CHANNEL_MASK          ( 0x07FFF800 )
#define M_DEFAULT_SCAN_DURATION         ( 0x02 )
#define M_DEFAULT_RESUME                ( 0x01 )
#define M_DEFAULT_NWK_STEER             ( ZAB_NWK_STEER_NONE )
#define M_DEFAULT_TX_POWER              ( 3 ) // TODO - Do something here when we have a solution for ZAB_ACTION_SET_TX_POWER and getting max power
#define TX_POWER_ERROR_VALUE            ( -127 )

#define M_TRUST_CENTER_NETWORK_ADDRESS    ( 0x0000 )

/* Default set to an invalid value so it fails if app does not support the item*/
#define M_DEFAULT_CHANNEL_TO_CHANGE_TO  ( 0 )

#define M_TIMEOUT_DISABLED              ( 0xFFFFFFFF )

/* Time = ((2^duration) + 1) scan periods where a scan period is 960 symbols
 * 5 works out to about 550ms which is nice*/
#define ENERGY_SCAN_DURATION            ( 5 )

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

#define ZAB_VND_NWK(_srv)  (ZAB_SERVICE_VENDOR(_srv)->zabNwkInfo)

/* Configuration Item structure */
typedef struct
{
  EzspConfigId configId;
  unsigned16 configValue;
} nwkConfigurationItem_t;

/* Policy Item structure */
typedef struct
{
  EzspPolicyId policyId;
  EzspDecisionId decisionId;
} nwkPolicyItem_t;

/******************************************************************************
 *                      *****************************
 *                 *****       LOCAL VARIABLES       *****
 *                      *****************************
 ******************************************************************************/

/* Table of configuration items that will be set for the network */
static const nwkConfigurationItem_t nwkConfigurationItems[] =
{
  {EZSP_CONFIG_SOURCE_ROUTE_TABLE_SIZE,         0x0020}, //TODO - comment and setup as config values where necessary
  {EZSP_CONFIG_SECURITY_LEVEL,                  0x0005},
  {EZSP_CONFIG_ADDRESS_TABLE_SIZE,              0x0002},
  {EZSP_CONFIG_TRUST_CENTER_ADDRESS_CACHE_SIZE, 0x0002},
  {EZSP_CONFIG_STACK_PROFILE,                   0x0002},
  {EZSP_CONFIG_INDIRECT_TRANSMISSION_TIMEOUT,   0x1E00},
  {EZSP_CONFIG_MAX_HOPS,                        0x001E},
  {EZSP_CONFIG_TX_POWER_MODE,                   0x8000},
  {EZSP_CONFIG_SUPPORTED_NETWORKS,              0x0001},
  {EZSP_CONFIG_BINDING_TABLE_SIZE,              ZAB_VENDOR_BINDING_TABLE_SIZE},
  {EZSP_CONFIG_APPLICATION_ZDO_FLAGS,           EMBER_APP_HANDLES_ZDO_ENDPOINT_REQUESTS | EMBER_APP_RECEIVES_SUPPORTED_ZDO_REQUESTS},
};
#define NUM_NWK_CONFIGURATION_ITEMS (sizeof(nwkConfigurationItems) / sizeof(nwkConfigurationItem_t))

/* Table of policy items that will be set for the network */
static const nwkPolicyItem_t nwkPolicyItems[] =
{
  {EZSP_BINDING_MODIFICATION_POLICY,            EZSP_ALLOW_BINDING_MODIFICATION},
};
#define NUM_NWK_POLICY_ITEMS (sizeof(nwkPolicyItems) / sizeof(nwkPolicyItem_t))

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Set state machine inactive
 ******************************************************************************/
static void setInactive(zabService* Service)
{
  ZAB_VND_NWK(Service).currentItem = ZAB_NWK_CURRENT_ITEM_NONE;
  ZAB_VND_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
}

/******************************************************************************
 * Set a timeout
 ******************************************************************************/
static void setTimeout(erStatus* Status, zabService* Service, unsigned32 Timeout)
{
  if (erStatusIsOk(Status))
    {
      if (Timeout == M_TIMEOUT_DISABLED)
        {
          ZAB_VND_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;
        }
      else
        {
          osTimeGetMilliseconds(Service, &ZAB_VND_NWK(Service).timeoutExpiryMs);
          ZAB_VND_NWK(Service).timeoutExpiryMs += Timeout;

          /* Handle the (very unlikely) case our timeout works out to the disabled value */
          if (ZAB_VND_NWK(Service).timeoutExpiryMs == M_TIMEOUT_DISABLED)
            {
              ZAB_VND_NWK(Service).timeoutExpiryMs++;
            }
        }
    }
}

/******************************************************************************
 * Set item is status is good
 * Otherwise graceful harakiri
 ******************************************************************************/
static void checkStatusSetItemCritical(erStatus* Status, zabService* Service, zabNwkCurrentItem item, unsigned32 Timeout)
{
  if (erStatusIsOk(Status))
    {
      ZAB_VND_NWK(Service).currentItem = item;
      setTimeout(Status, Service, Timeout);
    }
  else
    {
      printError(Service, "NWK: Error: Command failed at item 0x%02X with error 0x%04X. Resetting state machine\n", item, Status->Error);
      zabNcpNwk_Create(Status, Service);
    }
}

static zab_bool checkStatusSetItemNonCritical(erStatus* Status, zabService* Service, zabNwkCurrentItem item, unsigned32 Timeout)
{
  zab_bool allGood;

  if (erStatusIsOk(Status))
    {
      ZAB_VND_NWK(Service).currentItem = item;
      setTimeout(Status, Service, Timeout);
      allGood = zab_true;
    }
  else
    {
      setInactive(Service);
      printError(Service, "NWK: Warning: Command failed at item 0x%02X with error 0x%04X\n", item, Status->Error);
      sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, Status->Error);
      allGood = zab_false;
    }
  return allGood;
}

/******************************************************************************
 * Set network state and notify the manage sap
 ******************************************************************************/
static void setAndNotifyNwkState(erStatus* Status, zabService* Service, zabNwkState nwkState)
{
  if (ZAB_VND_NWK(Service).nwkState != nwkState)
    {
      ZAB_VND_NWK(Service).nwkState = nwkState;
      sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, nwkState);
    }
}

/******************************************************************************
 * Set policy items by index
 ******************************************************************************/
static void setPolicies(erStatus* Status, zabService* Service)
{
  unsigned8 index = ZAB_VND_NWK(Service).configItemIndex;

  if (index < NUM_NWK_POLICY_ITEMS)
    {
      zabNcpEzspConfiguration_SetPolicy(Status, Service, nwkPolicyItems[index].policyId, nwkPolicyItems[index].decisionId);
      checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_INIT_SET_POLICY, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
    }
  else
    {
      zabNcpEzspNetworking_SetManufacturerCode(Status, Service, ZAB_TYPES_M_SCHNEIDER_MANUFACTURER_CODE);
      checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_INIT_SET_MS_CODE, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
    }
}

/******************************************************************************
 * Set configuration items by index
 ******************************************************************************/
static void setConfiguration(erStatus* Status, zabService* Service)
{
  unsigned8 index = ZAB_VND_NWK(Service).configItemIndex;

  if (index < NUM_NWK_CONFIGURATION_ITEMS)
    {
      zabNcpEzspConfiguration_SetConfigValue(Status, Service, nwkConfigurationItems[index].configId, nwkConfigurationItems[index].configValue);
      checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_INIT_SET_CONFIG, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
    }
  else
    {
      /* Set index back to zero and start on policy table */
      ZAB_VND_NWK(Service).configItemIndex = 0;
      setPolicies(Status, Service);
    }
}

/******************************************************************************
 * Get binding table entries by index
 ******************************************************************************/
static void getBindingTable(erStatus* Status, zabService* Service)
{
  unsigned8 index = ZAB_VND_NWK(Service).configItemIndex;

  if (index < ZAB_VENDOR_BINDING_TABLE_SIZE)
    {
      zabNcpEzspBinding_Get(Status, Service, index);
      checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_GET_BINDING_TABLE, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
    }
  else
    {
      /* We are complete, so set networked */
      setInactive(Service);
      setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NETWORKED);
    }
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Create - Initialises networking state / state machine
 ******************************************************************************/
void zabNcpNwk_Create(erStatus* Status, zabService* Service)
{
  unsigned8 index;

  printVendor(Service, "zabNcpNwk_Create\n");
  setInactive(Service);
  ZAB_VND_NWK(Service).permitJoinTimeRemaining = 0;
  ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining = 0;
  ZAB_VND_NWK(Service).nwkAddr = EMBER_NULL_NODE_ID;
  ZAB_VND_NWK(Service).getNetworkInfoRequired = zab_false;

  for (index = 0; index < ZAB_VENDOR_BINDING_TABLE_SIZE; index++)
    {
      ZAB_VND_NWK(Service).bindingTable[index].type = EMBER_UNUSED_BINDING;
    }

  setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_UNINITIALISED);
  sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_NONE);
  sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
}

/******************************************************************************
 * Handle actions for the networking state machines
 ******************************************************************************/
void zabNcpNwk_Action(erStatus* Status, zabService* Service, unsigned8 Action)
{
  unsigned32 channelMask;
  signed8 TxPowerDbm;
  unsigned8 channel;
  zabMsgProMgmtNwkUpdateReq nwkUpdateReq;
  ER_CHECK_STATUS(Status);

  /* If its an action we are interested in, make sure vendor is open and no other nwk actions are in progress*/
  if ( (Action == ZAB_ACTION_NWK_INIT) ||
       (Action == ZAB_ACTION_NWK_DISCOVER) ||
       (Action == ZAB_ACTION_NWK_STEER) ||
       (Action == ZAB_ACTION_NWK_PERMIT_JOIN_ENABLE) ||
       (Action == ZAB_ACTION_NWK_PERMIT_JOIN_DISABLE) ||
       (Action == ZAB_ACTION_SET_TX_POWER) ||
       (Action == ZAB_ACTION_NWK_GET_INFO) ||
       (Action == ZAB_ACTION_CHANGE_CHANNEL) ||
       (Action == ZAB_ACTION_CHANGE_NETWORK_KEY) )
    {
      if (ZAB_VND_NWK(Service).currentItem != ZAB_NWK_CURRENT_ITEM_NONE)
        {
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_NETWORK_ACTION_IN_PROGRESS);
          sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_VND_NWK(Service).nwkState);
          return;
        }
      if (zabNcpOpen_GetOpenState(Service) != ZAB_OPEN_STATE_OPENED)
        {
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
          sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_VND_NWK(Service).nwkState);
          return;
        }

      /* Some actions also require us to be networked */
      if ( (Action == ZAB_ACTION_NWK_PERMIT_JOIN_ENABLE) ||
           (Action == ZAB_ACTION_NWK_PERMIT_JOIN_DISABLE) ||
           (Action == ZAB_ACTION_CHANGE_CHANNEL) ||
           (Action == ZAB_ACTION_CHANGE_NETWORK_KEY) )
        {
          if (ZAB_VND_NWK(Service).nwkState != ZAB_NWK_STATE_NETWORKED)
            {
              sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_INVALID_NETWORK_STATE);
              sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_VND_NWK(Service).nwkState);
              return;
            }
        }
    }

  /* Perform actions */
  switch(Action)
    {
      /*
      ** Initialise:
      ** Offers to resume previous network settings if available. Otherwise ends in NetworkNone state,
      ** ready for discovery/steering.
      */
      case ZAB_ACTION_NWK_INIT:
        if (ZAB_VND_NWK(Service).nwkState == ZAB_NWK_STATE_UNINITIALISED)
          {
            setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NETWORKING);
            ZAB_VND_NWK(Service).configItemIndex = 0;
            setConfiguration(Status, Service);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_INVALID_NETWORK_STATE);
            sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_VND_NWK(Service).nwkState);
          }
        break;

      /*
      ** Discover:
      ** Active scan. Indicates beacons which can then be used for Join.
      */
      case ZAB_ACTION_NWK_DISCOVER:
        if (ZAB_VND_NWK(Service).nwkState == ZAB_NWK_STATE_NONE)
          {
            setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_DISCOVERING);

            /* Discover Step 1: Ask for the channel mask, then run a network discovery request */
            channelMask = srvCoreAskBack32( Status, Service, ZAB_ASK_NWK_CHANNEL_MASK, M_DEFAULT_CHANNEL_MASK);
            zabNcpEzspNetworking_StartScan(Status, Service, EZSP_ACTIVE_SCAN, channelMask, M_DEFAULT_SCAN_DURATION);
            if (checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_NWK_DISCOVER, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS) == zab_false)
              {
                sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NWK_STATE_NONE);
              }
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_INVALID_NETWORK_STATE);
            sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_VND_NWK(Service).nwkState);
          }
        break;

      /*
      ** Steering:
      ** Join or Form a network.
      */
      case ZAB_ACTION_NWK_STEER:
        if (ZAB_VND_NWK(Service).nwkState == ZAB_NWK_STATE_NONE)
          {
            setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NETWORKING);

            /* Steer Common Step 1: Clear binding table*/
            zabNcpEzspBinding_ClearAll(Status, Service);
            checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_FORM_OR_JOIN_CLEAR_BINDS, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_INVALID_NETWORK_STATE);
            sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_VND_NWK(Service).nwkState);
          }
        break;

      /*
      ** Get Network Info
      */
      case ZAB_ACTION_NWK_GET_INFO:
        // TODO - WARNING - Incomplete. Does not currently give network address or device type before networked - SiLabs Case 110845
        sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_IN_PROGRESS);
        zabNcpEzspUtilities_GetNodeId(Status, Service);
        if (checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_GET_NETWORK_INFO_NODE_ID, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS) == zab_false)
          {
            sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_FAILED);
            sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_NONE);
          }
        break;

      /*
      ** Permit Join
      */
      case ZAB_ACTION_NWK_PERMIT_JOIN_ENABLE:
      case ZAB_ACTION_NWK_PERMIT_JOIN_DISABLE:
          {
            if (Action == ZAB_ACTION_NWK_PERMIT_JOIN_ENABLE)
              {
                ZAB_VND_NWK(Service).permitJoinTimeRemaining = srvCoreAskBack16( Status, Service, ZAB_ASK_PERMIT_JOIN_TIME, ZAB_M_PERMIT_JOIN_TIME);
                if (ZAB_VND_NWK(Service).permitJoinTimeRemaining > ZAB_M_NWK_MAX_PERMIT_JOIN_TIME)
                  {
                    ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining = ZAB_M_NWK_MAX_PERMIT_JOIN_TIME;
                  }
                else
                  {
                    ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining = ZAB_VND_NWK(Service).permitJoinTimeRemaining;
                  }
              }
            else
              {
                ZAB_VND_NWK(Service).permitJoinTimeRemaining = 0;
                ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining = 0;
              }

            zabNcpEzspNetworking_PermitJoin(Status, Service, ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining);
            if (checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_PJ_SET_LOCAL, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS) == zab_false)
              {
                ZAB_VND_NWK(Service).permitJoinTimeRemaining = 0;
                ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining = 0;
              }
            sapMsgPutNotifyNetworkPermitJoinState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_VND_NWK(Service).permitJoinTimeRemaining);
          }
        break;

      /*
      ** Update Tx Power
      */
      case ZAB_ACTION_SET_TX_POWER:
        TxPowerDbm = (signed8)srvCoreAskBack8( Status, Service, ZAB_ASK_TX_POWER_DBM, M_DEFAULT_TX_POWER);
        zabNcpEzspNetworking_SetRadioPower(Status, Service, TxPowerDbm);
        if (checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_TX_POWER_SET, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS) == zab_false)
          {
            srvCoreGiveBack8(Status, Service, ZAB_GIVE_NWK_TX_POWER, (unsigned8)TX_POWER_ERROR_VALUE);
          }
        break;


      /*
      ** Channel Change
      */
      case ZAB_ACTION_CHANGE_CHANNEL:
        sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_IN_PROGRESS);

        channel = srvCoreAskBack8( Status, Service, ZAB_ASK_CHANNEL_TO_CHANGE_TO, M_DEFAULT_CHANNEL_TO_CHANGE_TO);

        /* ARTF164900: Handle channel change to self gracefully */
        if (channel == ZAB_VND_NWK(Service).channel)
          {
            srvCoreGiveBack8(Status, Service, ZAB_GIVE_NWK_CHANNEL_NUMBER, channel);
            sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_SUCCESS);
            sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_NONE);
            return;
          }
        else if ( (channel >= ZAB_ZNP_NWK_M_CHANNEL_MIN) && (channel <= ZAB_ZNP_NWK_M_CHANNEL_MAX) )
          {
            nwkUpdateReq.DestinationAddressMode = ZAB_ADDRESS_MODE_BROADCAST;
            nwkUpdateReq.DestinationAddress = ZAB_BROADCAST_RX_ON_WHEN_IDLE;
            nwkUpdateReq.ChannelMask = 1 << channel;
            nwkUpdateReq.ScanDuration = 0xFE;
            ZAB_VND_NWK(Service).nwkUpdateId++;
            zabNcpZdo_MgmtNwkUpdateReq(Status, Service, ZAB_VND_DEFAULT_TID, &nwkUpdateReq);
            if (checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE, ZAB_VND_CFG_M_CHANNEL_CHANGE_TIMEOUT_MS) == zab_true)
              {
                /* Exit on success so we can clean up failures in one place below */
                return;
              }
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ASKED_VALUE_INVALID);
          }
        /* It failed for some reason, so notify it */
        sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_FAILED);
        sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_NONE);
        break;


      /*
      ** Network Key Change
      */
      case ZAB_ACTION_CHANGE_NETWORK_KEY:
        sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_IN_PROGRESS);
        /* ARTF113872: Only allow key change from TC */
        if (zabNcpNwk_GetNwkAddress(Service) == M_TRUST_CENTER_NETWORK_ADDRESS)
          {
            zabNcpEzspTrustCenter_BroadcastNextNetworkKey(Status, Service);
            if (checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_KEY_BROADCAST, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS) == zab_true)
              {
                /* Exit on success so we can clean up failures in one place below */
                return;
              }
          }
        else
          {
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_CFG_INVALID);
          }
        sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_FAILED);
        sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
        break;
    }
}

/******************************************************************************
 * Get Network State
 ******************************************************************************/
zabNwkState zabNcpNwk_GetNwkState(zabService* Service)
{
  return ZAB_VND_NWK(Service).nwkState;
}

/******************************************************************************
 * Get if the Network State Machine is currently busy doing something
 ******************************************************************************/
zab_bool zabNcpNwk_GetNwkStateMachineInProgress(zabService* Service)
{
  zab_bool inProgress = zab_true;

  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NONE)
    {
      inProgress = zab_false;
    }

  return inProgress;
}

/******************************************************************************
 * Get Network Address
 ******************************************************************************/
unsigned16 zabNcpNwk_GetNwkAddress(zabService* Service)
{
  return ZAB_VND_NWK(Service).nwkAddr;
}

/******************************************************************************
 * Get Network Update ID
 ******************************************************************************/
unsigned8 zabNcpNwk_GetNwkUpdateId(zabService* Service)
{
  return ZAB_VND_NWK(Service).nwkUpdateId;
}

/******************************************************************************
 * Check for timeouts within the networking state machine
 ******************************************************************************/
unsigned32 zabNcpNwk_UpdateTimeout(erStatus* Status, zabService* Service, unsigned32 Time)
{
  erStatus LocalStatus;
  unsigned32 timeoutRemaining = ZAB_WORK_DELAY_MAX_MS;
  zabNwkCurrentItem currentItem = ZAB_VND_NWK(Service).currentItem;

  if (ZAB_VND_NWK(Service).timeoutExpiryMs != M_TIMEOUT_DISABLED)
    {
      /* Check timeout while handling wrapped time*/
      timeoutRemaining = ZAB_VND_NWK(Service).timeoutExpiryMs - Time;
      if ( (timeoutRemaining == 0) || (timeoutRemaining > ZAB_VND_MS_TIMEOUT_MAX) )
        {
          printVendor(Service, "zabZnpNwk_updateTimeout: Timeout expired for item %d\n", currentItem);

          /* Ask for fast work if timer expired as it may have triggered some other event */
          timeoutRemaining = 0;
          setInactive(Service);

          switch(currentItem )
            {
              case ZAB_NWK_CURRENT_ITEM_PJ_SET_LOCAL:
              case ZAB_NWK_CURRENT_ITEM_PJ_SET_NETWORK:
                ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining = 0;
                ZAB_VND_NWK(Service).permitJoinTimeRemaining = 0;
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
                sapMsgPutNotifyNetworkPermitJoinState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_VND_NWK(Service).permitJoinTimeRemaining);
                break;

              case ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE:
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
                sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_FAILED);
                sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_NONE);
                break;

              case ZAB_NWK_CURRENT_ITEM_KEY_BROADCAST:
              case ZAB_NWK_CURRENT_ITEM_KEY_SWITCH:
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
                sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_FAILED);
                sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
                break;

              /* This is a delay between key broadcast and switch, so continue the process.
               * Use local status and be extra careful as send can fail if someone else has the link locked. */
              case ZAB_NWK_CURRENT_ITEM_DELAY_BEFORE_KEY_SWITCH:
                erStatusClear(&LocalStatus, Service);
                zabNcpEzspTrustCenter_BroadcastNextNetworkKeySwitch(&LocalStatus, Service);
                if (erStatusIsOk(&LocalStatus))
                  {
                    checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_KEY_SWITCH, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
                  }
                else if (erStatusGetError(&LocalStatus) == ZAB_ERROR_VENDOR_BUSY)
                  {
                    /* Link locked, so defer next retry a while */
                    timeoutRemaining = 100;
                  }
                else
                  {
                    sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, erStatusGetError(&LocalStatus));
                    sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_FAILED);
                    sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
                  }
                break;

              case ZAB_NWK_CURRENT_ITEM_NWK_DISCOVER:
              case ZAB_NWK_CURRENT_ITEM_ACTIVE_SCAN_IN_PROGRESS:
                setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
                break;

              case ZAB_NWK_CURRENT_ITEM_GET_NETWORK_INFO_NODE_ID:
              case ZAB_NWK_CURRENT_ITEM_GET_NETWORK_INFO_NWK_PARAMS:
                sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_FAILED);
                sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_NONE);
                break;

              case ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE_NODE_ID:
              case ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE_NWK_PARAMS:
                sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_FAILED);
                sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_NONE);
                break;

              case ZAB_NWK_CURRENT_ITEM_TX_POWER_SET:
              case ZAB_NWK_CURRENT_ITEM_TX_POWER_GET:
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
                srvCoreGiveBack8(Status, Service, ZAB_GIVE_NWK_TX_POWER, (unsigned8)TX_POWER_ERROR_VALUE);
                break;

              /* Do nothing, revert to normal operation  */
              case ZAB_NWK_CURRENT_ITEM_INTERNAL_NODE_ID:
              case ZAB_NWK_CURRENT_ITEM_INTERNAL_NWK_PARAMS:
                break;

              /* Critical timeouts. Revert to nothing */
              case ZAB_NWK_CURRENT_ITEM_INIT_SET_CONFIG:
              case ZAB_NWK_CURRENT_ITEM_INIT_SET_POLICY:
              case ZAB_NWK_CURRENT_ITEM_GET_BINDING_TABLE:
              case ZAB_NWK_CURRENT_ITEM_NCP_INIT:
              case ZAB_NWK_CURRENT_ITEM_INIT_WAIT_FOR_UP:
              case ZAB_NWK_CURRENT_FORM_OR_JOIN_CLEAR_BINDS:
              case ZAB_NWK_CURRENT_FORM_OR_JOIN_SEC_INIT:
              case ZAB_NWK_CURRENT_ITEM_ENERGY_SCAN_BEFORE_FORM_STARTING:
              case ZAB_NWK_CURRENT_ITEM_ENERGY_SCAN_IN_PROGRESS:
              case ZAB_NWK_CURRENT_ITEM_FORM:
              case ZAB_NWK_CURRENT_ITEM_FORM_WAIT_UP:
              case ZAB_NWK_CURRENT_ITEM_JOIN:
              case ZAB_NWK_CURRENT_ITEM_JOIN_WAIT_UP:
              case ZAB_NWK_CURRENT_ITEM_INIT_FORM_JOIN_NODE_NODE_ID:
              case ZAB_NWK_CURRENT_ITEM_INIT_FORM_JOIN_NODE_NWK_PARAMS:
              case ZAB_NWK_CURRENT_ITEM_LEAVE_TO_NOT_RESUME:
              case ZAB_NWK_CURRENT_ITEM_LEAVE_WAIT_FOR_DOWN:
              default:
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
                zabNcpNwk_Create(Status, Service);
                break;
            }
        }
    }

  /* If nothing is happening and we need to refresh discovery, try to start.
   * Be careful with status as there is a decent chance link can be locked by some other command. */
  else if (ZAB_VND_NWK(Service).getNetworkInfoRequired == zab_true)
    {
      if (currentItem == ZAB_NWK_CURRENT_ITEM_NONE)
        {
          erStatusClear(&LocalStatus, Service);
          zabNcpEzspUtilities_GetNodeId(&LocalStatus, Service);
          if (erStatusIsOk(&LocalStatus))
            {
              timeoutRemaining = 0;
              checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_INTERNAL_NODE_ID, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
            }
          else
            {
              /* Defer next retry a while */
              timeoutRemaining = 100;
            }
        }
    }
  return timeoutRemaining;
}

/******************************************************************************
 * Reset Indication Handler
 * Re-initialises networking states if network processor is reset
 ******************************************************************************/
void zabNcpNwk_ResetHandler(erStatus* Status, zabService* Service)
{
  zabNcpNwk_Create(Status, Service);
}

/******************************************************************************
 * Set Tx Power Response Handler
 ******************************************************************************/
void zabNcpNwk_SetTxPowerResponseHandler(erStatus* Status, zabService* Service, unsigned8 PowerStatus)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_TX_POWER_SET)
    {
      /* Notify that it failed but continue to get anyway */
      if (PowerStatus != EMBER_SUCCESS)
        {
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
        }

      /* Now get actual Tx Power */
      // zabNcpEzspNetworking_GetNetworkParameters(Status, Service); // WARNING:  this gets a constant value for tx power defined at form/join - it does not get the ram value!
      zabNcpEzspMfglib_GetPower(Status, Service); // For now we will use MfgLib, which works but is not very clean (could be compiled out))
      if (checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_TX_POWER_GET, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS) == zab_false)
        {
          srvCoreGiveBack8(Status, Service, ZAB_GIVE_NWK_TX_POWER, (unsigned8)TX_POWER_ERROR_VALUE);
        }
    }
}

/******************************************************************************
 * Get Tx Power Response Handler
 ******************************************************************************/
void zabNcpNwk_GetPowerResponseHandler(erStatus* Status, zabService* Service, signed8 TxPower)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_TX_POWER_GET)
    {
      setInactive(Service);
      srvCoreGiveBack8(Status, Service, ZAB_GIVE_NWK_TX_POWER, (unsigned8)TxPower);
    }
}

/******************************************************************************
 * Permit Join Response Handler
 ******************************************************************************/
void zabNcpNwk_PermitJoinResponseHandler(erStatus* Status, zabService* Service, unsigned8 PermitStatus)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_PJ_SET_LOCAL)
    {
      setInactive(Service);

      if (PermitStatus == EMBER_SUCCESS)
        {
          zabNcpZdo_PermitJoinReq(Status, Service, EMBER_BROADCAST_ADDRESS, ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining, zab_true);
          // Broadcast is best effort. If it fails we don't care!
        }
      else
        {
          printError(Service, "NCP NWK: Permit Join Failed with Status = 0x%02X\n", PermitStatus);
          ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining = 0;
          ZAB_VND_NWK(Service).permitJoinTimeRemaining = 0;
          sapMsgPutNotifyNetworkPermitJoinState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_VND_NWK(Service).permitJoinTimeRemaining);
        }
    }
}

/******************************************************************************
 * Handle an incoming Permit Join Request from the network
 ******************************************************************************/
void zabNcpNwk_PermitJoinRequestHandler(erStatus* Status, zabService* Service, unsigned16 SourceNetworkAddress, unsigned8 Duration)
{
  /* Processing incoming PJ notifications as long as they were not from ourself! */
  if (SourceNetworkAddress != zabNcpNwk_GetNwkAddress(Service))
    {
      /* Clip 0xFE, which is now illegal to use */
      if (Duration > ZAB_M_NWK_MAX_PERMIT_JOIN_TIME)
        {
          Duration = ZAB_M_NWK_MAX_PERMIT_JOIN_TIME;
        }

      ZAB_VND_NWK(Service).permitJoinTimeRemaining = Duration;
      ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining = Duration;

      /* If we are part way through an action then cancel it */
      if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_PJ_SET_LOCAL)
        {
          setInactive(Service);
        }

      sapMsgPutNotifyNetworkPermitJoinState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_VND_NWK(Service).permitJoinTimeRemaining);
    }
}

/******************************************************************************
 * Handle an incoming Leave Request from the network
 ******************************************************************************/
void zabNcpNwk_LeaveRequestHandler(erStatus* Status, zabService* Service, unsigned64 Ieee, zab_bool Rejoin)
{
  /* Processing incoming leave request as long as it is for ourself! */
  if ( (Ieee == 0) && (Rejoin == zab_false) )
    {
      setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_LEAVING);
    }
}


/******************************************************************************
 * 1 second tick handler
 ******************************************************************************/
void zabNcpNwk_OneSecondTickHandler(erStatus* Status, zabService* Service)
{
  unsigned32 nextPermitJoinChunkTime;
  erStatus LocalStatus;
  erStatusClear(&LocalStatus, Service);

  /* If we are timing out, clean up and notify it has ended */
  if (ZAB_VND_NWK(Service).permitJoinTimeRemaining == 1)
    {
      ZAB_VND_NWK(Service).permitJoinTimeRemaining = 0;
      ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining = 0;
      sapMsgPutNotifyNetworkPermitJoinState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_VND_NWK(Service).permitJoinTimeRemaining);
    }

  /* If timer is active */
  else if (ZAB_VND_NWK(Service).permitJoinTimeRemaining > 0)
    {
      ZAB_VND_NWK(Service).permitJoinTimeRemaining--;
      if (ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining > 0)
        {
          ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining--;
        }

      /* If we are close to finishing this chunk, then try to start the next one */
      if ( (ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining < 10) &&
           (ZAB_VND_NWK(Service).permitJoinTimeRemaining > ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining) &&
           (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NONE) )
        {
          if (ZAB_VND_NWK(Service).permitJoinTimeRemaining > ZAB_M_NWK_MAX_PERMIT_JOIN_TIME)
            {
              nextPermitJoinChunkTime = ZAB_M_NWK_MAX_PERMIT_JOIN_TIME;
            }
          else
            {
              nextPermitJoinChunkTime = ZAB_VND_NWK(Service).permitJoinTimeRemaining;
            }
          zabNcpEzspNetworking_PermitJoin(&LocalStatus, Service, nextPermitJoinChunkTime);
          if (erStatusIsOk(&LocalStatus))
            {
              ZAB_VND_NWK(Service).currentPermitJoinTimeRemaining = nextPermitJoinChunkTime;
              checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_PJ_SET_LOCAL, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
            }
        }
    }
}

/******************************************************************************
 * Clear Binding Table Response Handler
 ******************************************************************************/
void zabNcpNwk_ClearBindingTableResponseHandler(erStatus* Status, zabService* Service, unsigned8 ClearStatus)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_FORM_OR_JOIN_CLEAR_BINDS)
    {
      if (ClearStatus == EMBER_SUCCESS)
        {
          /* Steer Common Step 2: Initialise security */
          zabNcpEzspSecurity_SetInitialState(Status, Service);
          checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_FORM_OR_JOIN_SEC_INIT, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
        }
      else
        {
          zabNcpNwk_Create(Status, Service);
        }
    }
}

/******************************************************************************
 * Security Init Response Handler
 ******************************************************************************/
void zabNcpNwk_SecurityInitResponseHandler(erStatus* Status, zabService* Service, unsigned8 SecurityStatus)
{
  zabNwkSteerOptions nwkSteer;
  BeaconFormat_t beacon;
  EmberNetworkParameters networkParameters;

  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_FORM_OR_JOIN_SEC_INIT)
    {
      setInactive(Service);

      if (SecurityStatus == EMBER_SUCCESS)
        {
          /* Ask the app what steering they want to do */
          nwkSteer = (zabNwkSteerOptions)srvCoreAskBack8( Status, Service, ZAB_ASK_NWK_STEER, M_DEFAULT_NWK_STEER);
          switch(nwkSteer)
            {
              /* Steer Join: Ask for beacon of network to join then go for it */
              case ZAB_NWK_STEER_JOIN:
                osMemSet(Status, (unsigned8*)&beacon, sizeof(BeaconFormat_t), 0xFF);
                srvCoreAskBackBuffer(Status, Service,ZAB_ASK_NWK_JOIN_BEACON, 0, sizeof(BeaconFormat_t), (unsigned8*)&beacon);

                COPY_OUT_64_BITS(networkParameters.extendedPanId, beacon.epid);
                networkParameters.panId = beacon.pan;
                networkParameters.radioTxPower = (signed8)srvCoreAskBack8( Status, Service, ZAB_ASK_TX_POWER_DBM, M_DEFAULT_TX_POWER);
                networkParameters.radioChannel = beacon.channel;
                networkParameters.joinMethod = EMBER_USE_MAC_ASSOCIATION;
                networkParameters.nwkManagerId = 0xFFFF;
                networkParameters.nwkUpdateId = beacon.updateID;
                networkParameters.channels = M_DEFAULT_CHANNEL_MASK;

                zabNcpEzspNetworking_NetworkJoin(Status, Service, EMBER_ROUTER, &networkParameters);
                checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_JOIN, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
                break;

              /* Steer: Form */
              case ZAB_NWK_STEER_FORM:
                /* Get channel list, then start passive scan of channels.
                 * There is an optimisation to make here if only one channel is listed, but the gain is not considered worth the extra code */
                ZAB_VND_NWK(Service).formChannelMask = srvCoreAskBack32( Status, Service, ZAB_ASK_NWK_CHANNEL_MASK, M_DEFAULT_CHANNEL_MASK);
                ZAB_VND_NWK(Service).formLowestEnergyChannel = 0;
                ZAB_VND_NWK(Service).formLowestEnergyLevel = 127;
                zabNcpEzspNetworking_StartScan(Status, Service, EZSP_ENERGY_SCAN, ZAB_VND_NWK(Service).formChannelMask, ENERGY_SCAN_DURATION);
                checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_ENERGY_SCAN_BEFORE_FORM_STARTING, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
                break;

              default:
                printError(Service, "ERROR: Nwk Steer: Invalid nwkSteer %d\n", nwkSteer);
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ASKED_VALUE_INVALID);
                zabNcpNwk_Create(Status, Service);
            }
        }
      else
        {
          zabNcpNwk_Create(Status, Service);
        }
    }
}

/******************************************************************************
 * Start Scan Response Handler
 ******************************************************************************/
void zabNcpNwk_StartScanResponseHandler(erStatus* Status, zabService* Service, unsigned8 ScanStartStatus)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_ENERGY_SCAN_BEFORE_FORM_STARTING)
    {
      if (ScanStartStatus == EMBER_SUCCESS)
        {
          /* Wait for channel results or stop */
          checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_ENERGY_SCAN_IN_PROGRESS, ZAB_VND_CFG_M_NWK_CONFIRM_TIMEOUT_MS);
        }
      else
        {
          zabNcpNwk_Create(Status, Service);
        }
    }
  else if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NWK_DISCOVER)
    {
      /* If the scan started then we want to set a timeout to wait for scan result indications to arrive or eventually scan compelte */
      if ( (ScanStartStatus != EMBER_SUCCESS) ||
           (checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_ACTIVE_SCAN_IN_PROGRESS, ZAB_VND_CFG_M_ACTIVE_SCAN_CONFIRM_TIMEOUT_MS) == zab_false) )
        {
          setInactive(Service);
          sapMsgPutNotifyNetworkState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NWK_STATE_NONE);
        }
    }
}

/******************************************************************************
 * Energy Scan Result Handler
 ******************************************************************************/
void zabNcpNwk_EnergyScanResultHandler(erStatus* Status, zabService* Service, unsigned8 Channel, signed8 MaxRssiValue)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_ENERGY_SCAN_IN_PROGRESS)
    {
      /* Store the new channel and level if it's RSSI is lower than the previous best */
      if (MaxRssiValue < ZAB_VND_NWK(Service).formLowestEnergyLevel)
        {
          ZAB_VND_NWK(Service).formLowestEnergyChannel = Channel;
          ZAB_VND_NWK(Service).formLowestEnergyLevel = MaxRssiValue;
        }

      /* Extend timeout */
      setTimeout(Status, Service, ZAB_VND_CFG_M_NWK_CONFIRM_TIMEOUT_MS);
    }
}

/******************************************************************************
 * Active Scan Result Handler
 ******************************************************************************/
void zabNcpNwk_NetworkFoundResultHandler(erStatus* Status, zabService* Service, EmberZigbeeNetwork Network, unsigned8 LastHopLqi, signed8 LastHopRssi)
{
  BeaconFormat_t beacon;

  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_ACTIVE_SCAN_IN_PROGRESS)
    {
      /* Indicate beacon */
      beacon.src = 0xFFFF;
      beacon.pan = Network.panId;
      beacon.channel = Network.channel;
      beacon.permitJoin = Network.allowingJoin;
      beacon.routerCapacity = 1;
      beacon.deviceCapacity = 1;
      beacon.protocolVersion = 0; // ZigBee
      beacon.stackProfile = 2;
      beacon.lqi = LastHopLqi;
      beacon.depth = 0;
      beacon.updateID = Network.nwkUpdateId;
      beacon.epid = COPY_IN_64_BITS(Network.extendedPanId);
      srvCoreGiveBackBuffer (Status, Service, ZAB_GIVE_NWK_BEACON, sizeof(BeaconFormat_t), (unsigned8*)&beacon);

      /* Extend timeout */
      setTimeout(Status, Service, ZAB_VND_CFG_M_ACTIVE_SCAN_CONFIRM_TIMEOUT_MS);
    }
}

/******************************************************************************
 * Scan Complete Handler
 ******************************************************************************/
void zabNcpNwk_ScanCompleteHandler(erStatus* Status, zabService* Service, unsigned8 ScanStatus)
{
  EmberNetworkParameters networkParameters = { {0} };
  unsigned8 count;
  unsigned64 epid;

  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_ENERGY_SCAN_IN_PROGRESS)
    {
      if (ScanStatus == EMBER_SUCCESS)
        {
          if ( (ZAB_VND_NWK(Service).formLowestEnergyChannel >= 11) && (ZAB_VND_NWK(Service).formLowestEnergyChannel <= 26) )
            {
              networkParameters.radioChannel = ZAB_VND_NWK(Service).formLowestEnergyChannel;
              networkParameters.channels = M_DEFAULT_CHANNEL_MASK; // Set this to have the full channel set available for changes

              /* Generate a non 0 or FFFF PanId with a fair number of tries */
              count = 0;
              do
                {
                  networkParameters.panId = ZAB_RAND_16();
                  count++;
                } while ( ((networkParameters.panId == 0x0000) || (networkParameters.panId == ZAB_TYPES_M_NETWORK_PAN_ID_UNKNOWN)) && (count < 10) );
              networkParameters.panId = srvCoreAskBack16( Status, Service, ZAB_ASK_NWK_PAN_ID, networkParameters.panId);

              /* Create a Schneider EPID with 16bits of randomness*/
              epid = ZAB_RAND_16();
              epid <<= 48;
              epid += 0x02AB04015E10ULL;
              epid = srvCoreAskBack64( Status, Service, ZAB_ASK_NWK_EPID, epid);
              COPY_OUT_64_BITS(networkParameters.extendedPanId, epid);

              networkParameters.radioTxPower = (signed8)srvCoreAskBack8( Status, Service, ZAB_ASK_TX_POWER_DBM, M_DEFAULT_TX_POWER);


              zabNcpEzspNetworking_NetworkForm(Status, Service, &networkParameters);
              checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_FORM, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
            }
          else
            {
              zabNcpNwk_Create(Status, Service);
            }
        }
      else
        {
          zabNcpNwk_Create(Status, Service);
        }
    }
  else if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_ACTIVE_SCAN_IN_PROGRESS)
    {
      setInactive(Service);
      setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_DISCOVERY_COMPLETE);
      setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
    }
}

/******************************************************************************
 * Network Form Response Handler
 ******************************************************************************/
void zabNcpNwk_NetworkFormResponseHandler(erStatus* Status, zabService* Service, unsigned8 FormStatus)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_FORM)
    {
      switch(FormStatus)
        {
          case EMBER_SUCCESS:
            /* Wait for stack status handler - up */
            checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_FORM_WAIT_UP, ZAB_VND_CFG_M_NWK_CONFIRM_TIMEOUT_MS);
            break;

          default:
            zabNcpNwk_Create(Status, Service);
        }
    }
}

/******************************************************************************
 * Network Join Response Handler
 ******************************************************************************/
void zabNcpNwk_NetworkJoinResponseHandler(erStatus* Status, zabService* Service, unsigned8 JoinStatus)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_JOIN)
    {
      switch(JoinStatus)
        {
          case EMBER_SUCCESS:
            /* Wait for stack status handler - up */
            checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_JOIN_WAIT_UP, ZAB_VND_CFG_M_NWK_CONFIRM_TIMEOUT_MS);
            break;

          default:
            setInactive(Service);
            sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_JOIN_NETWORK_FAILED);
            setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
            break;
        }
    }
}

/******************************************************************************
 * Network Init Response Handler
 ******************************************************************************/
void zabNcpNwk_NetworkInitResponseHandler(erStatus* Status, zabService* Service, unsigned8 InitStatus)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_NCP_INIT)
    {
      setInactive(Service);

      switch(InitStatus)
        {
          case EMBER_SUCCESS:
            /* Wait for stack status handler - up */
            checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_INIT_WAIT_FOR_UP, ZAB_VND_CFG_M_NWK_CONFIRM_TIMEOUT_MS);
            break;

          case EMBER_NOT_JOINED:
            /* Network init finished with no network */
            setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
            break;

          default:
            zabNcpNwk_Create(Status, Service);
        }
    }
}

/******************************************************************************
 * Stack Status Indication Handler
 ******************************************************************************/
void zabNcpNwk_StackStatusHandler(erStatus* Status, zabService* Service, unsigned8 StackStatus)
{
  unsigned8 resume;

  /* If an a-synchronous change occurred, then flag that re-discover of network parameters is required*/
  if ( (StackStatus == EMBER_CHANNEL_CHANGED) ||
       (StackStatus == EMBER_NODE_ID_CHANGED) ||
       (StackStatus == EMBER_PAN_ID_CHANGED) )
    {
      ZAB_VND_NWK(Service).getNetworkInfoRequired = zab_true;
    }

  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_INIT_WAIT_FOR_UP)
    {
      if (StackStatus == EMBER_NETWORK_UP)
        {
          /* Network was restored. Check resume to see if we want to keep it or kill it */
          resume = srvCoreAskBack8( Status, Service, ZAB_ASK_NWK_RESUME, M_DEFAULT_RESUME);
          if(resume)
            {
              /* Keep the current network, get network info */
              zabNcpEzspUtilities_GetNodeId(Status, Service);
              checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_INIT_FORM_JOIN_NODE_NODE_ID, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
            }
          else
            {
              /* Leave current network */
              setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_LEAVING);
              zabNcpEzspNetworking_Leave(Status, Service);
              checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_LEAVE_TO_NOT_RESUME, ZAB_VND_CFG_M_NWK_CONFIRM_TIMEOUT_MS);
            }
        }
    }

  else if ( (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_FORM_WAIT_UP) ||
            (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_JOIN_WAIT_UP) )
    {
      if (StackStatus == EMBER_NETWORK_UP)
        {
          zabNcpEzspUtilities_GetNodeId(Status, Service);
          checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_INIT_FORM_JOIN_NODE_NODE_ID, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
        }
      else
        {
          if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_FORM_WAIT_UP)
            {
              sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_FORM_NETWORK_FAILED);
            }
          else
            {
              sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_JOIN_NETWORK_FAILED);
            }
          setInactive(Service);
          setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
        }
    }

  else if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_LEAVE_WAIT_FOR_DOWN)
    {
      if (StackStatus == EMBER_NETWORK_DOWN)
        {
          setInactive(Service);
          setAndNotifyNwkState(Status, Service, ZAB_NWK_STATE_NONE);
        }
    }

  else if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE)
    {
      if (StackStatus == EMBER_CHANNEL_CHANGED)
        {
          /* Get network info now channel has changed */
          zabNcpEzspUtilities_GetNodeId(Status, Service);
          if (checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE_NODE_ID,ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS) == zab_false)
            {
              sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_FAILED);
              sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_NONE);
            }
        }
    }
  else if (StackStatus == EMBER_NETWORK_DOWN)
    {
      // ARTF199128: A-synchronous down, either an error or a remote leave received.
      //             "Reopen" connection to re-establish synchronisation
      zabNcpOpen_ReopenRequest(Status, Service);
    }
}

/******************************************************************************
 * Get Network Address Response Handler
 ******************************************************************************/
void zabNcpNwk_GetNetworkAddressResponseHandler(erStatus* Status, zabService* Service, unsigned16 NetworkAddress)
{
  if ( (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_INIT_FORM_JOIN_NODE_NODE_ID) ||
       (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_NETWORK_INFO_NODE_ID) ||
       (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE_NODE_ID) ||
       (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_INTERNAL_NODE_ID) )
    {
      ZAB_VND_NWK(Service).timeoutExpiryMs = M_TIMEOUT_DISABLED;

      // Handle strange special case where when not UP it returns 0xFFFE (expect 0xFFFF)
      if (NetworkAddress == 0xFFFE)
        {
          NetworkAddress = EMBER_NULL_NODE_ID;
        }
      ZAB_VND_NWK(Service).nwkAddr = NetworkAddress;
      srvCoreGiveBack16(Status, Service, ZAB_GIVE_NWK_ADDRESS, NetworkAddress);

      zabNcpEzspNetworking_GetNetworkParameters(Status, Service);

      if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_INIT_FORM_JOIN_NODE_NODE_ID)
        {
          checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_INIT_FORM_JOIN_NODE_NWK_PARAMS, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
        }
      else if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_NETWORK_INFO_NODE_ID)
        {
          if (checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_GET_NETWORK_INFO_NWK_PARAMS, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS) == zab_false)
            {
              sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_FAILED);
              sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_NONE);
            }
        }
      else if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE_NODE_ID)
        {
          if (checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE_NWK_PARAMS, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS) == zab_false)
            {
              sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_FAILED);
              sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_NONE);
            }
        }
      else if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_INTERNAL_NODE_ID)
        {
          checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_INTERNAL_NWK_PARAMS, ZAB_VND_CFG_M_SERIAL_TIMEOUT_MS);
        }
    }
}

/******************************************************************************
 * Get Network Parameters Response Handler
 ******************************************************************************/
void zabNcpNwk_GetNetworkParametersResponseHandler(erStatus* Status, zabService* Service, unsigned8 GetStatus, EmberNodeType NodeType, EmberNetworkParameters* NetworkParams)
{
  unsigned64 epid;
  zabDeviceType deviceType;
  zab_bool initFormJoinInfo;
  zab_bool actionInfo;
  zab_bool internalInfo;
  zab_bool channelChangeInfo;

  /* Store anything that is required to keep up to date about the network */
  ZAB_VND_NWK(Service).channel = NetworkParams->radioChannel;
  ZAB_VND_NWK(Service).nwkUpdateId = NetworkParams->nwkUpdateId;
  ZAB_VND_NWK(Service).getNetworkInfoRequired = zab_false;

  initFormJoinInfo = zab_false;
  actionInfo = zab_false;
  internalInfo = zab_false;
  channelChangeInfo = zab_false;
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_INIT_FORM_JOIN_NODE_NWK_PARAMS)
    {
      initFormJoinInfo = zab_true;
    }
  else if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_NETWORK_INFO_NWK_PARAMS)
    {
      actionInfo = zab_true;
    }
  else if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_INTERNAL_NWK_PARAMS)
    {
      internalInfo = zab_true;
    }
  else if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_CHANNEL_CHANGE_NWK_PARAMS)
    {
      channelChangeInfo = zab_true;
    }
  else if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_TX_POWER_GET)
    {
      setInactive(Service);
      srvCoreGiveBack8(Status, Service, ZAB_GIVE_NWK_TX_POWER, (unsigned8)NetworkParams->radioTxPower);
    }


  if ( (initFormJoinInfo == zab_true) || (actionInfo == zab_true) || (internalInfo == zab_true) || (channelChangeInfo == zab_true) )
    {
      setInactive(Service);

      if ( (GetStatus == EMBER_SUCCESS) ||
           ( (actionInfo == zab_true) && (GetStatus == EMBER_NOT_JOINED) ) )
        {
            switch (NodeType)
            {
              case EMBER_COORDINATOR:                     deviceType = ZAB_DEVICE_TYPE_COORDINATOR; break;
              case EMBER_ROUTER:                          deviceType = ZAB_DEVICE_TYPE_ROUTER; break;
              case EMBER_END_DEVICE:
              case EMBER_SLEEPY_END_DEVICE:
              case EMBER_MOBILE_END_DEVICE:               deviceType = ZAB_DEVICE_TYPE_END_DEVICE; break;
              default:                                    deviceType = ZAB_DEVICE_TYPE_NONE;
            }
          srvCoreGiveBack8(Status, Service, ZAB_GIVE_DEVICE_TYPE, (unsigned8)deviceType);

          srvCoreGiveBack8(Status, Service, ZAB_GIVE_NWK_CHANNEL_NUMBER, NetworkParams->radioChannel);
          srvCoreGiveBack16(Status, Service, ZAB_GIVE_NWK_PAN_ID, NetworkParams->panId);
          epid = COPY_IN_64_BITS(NetworkParams->extendedPanId);
          srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_NWK_EPID, 8, (unsigned8*)&epid);
          srvCoreGiveBack8(Status, Service, ZAB_GIVE_NWK_TX_POWER, (unsigned8)NetworkParams->radioTxPower);
        }

      if (initFormJoinInfo == zab_true)
        {
          /* Get the binding table */
          ZAB_VND_NWK(Service).configItemIndex = 0;
          getBindingTable(Status, Service);
        }
      else if (actionInfo == zab_true)
        {
          if ( (GetStatus == EMBER_SUCCESS) || (GetStatus == EMBER_NOT_JOINED) )
            {
              sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_SUCCESS);
            }
          else
            {
              sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_FAILED);
            }
          sapMsgPutNotifyNetworkInfoState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_INFO_STATE_NONE);
        }
      else if (channelChangeInfo == zab_true)
        {
          sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_SUCCESS);
          sapMsgPutNotifyChannelChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CHANNEL_CHANGE_STATE_NONE);
        }
    }
}

/******************************************************************************
 * Leave Response Handler
 ******************************************************************************/
void zabNcpNwk_LeaveResponseHandler(erStatus* Status, zabService* Service, unsigned8 LeaveStatus)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_LEAVE_TO_NOT_RESUME)
    {
      if (LeaveStatus == EMBER_SUCCESS)
        {
          checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_LEAVE_WAIT_FOR_DOWN, ZAB_VND_CFG_M_NWK_CONFIRM_TIMEOUT_MS);
        }
      else
        {
          zabNcpNwk_Create(Status, Service);
        }
    }
  else
    {
      zabNcpNwk_Create(Status, Service);
    }
}

/******************************************************************************
 * Set Configuration Response Handler
 ******************************************************************************/
void zabNcpNwk_SetConfigurationResponseHandler(erStatus* Status, zabService* Service, unsigned8 ConfigStatus)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_INIT_SET_CONFIG)
    {
      if (ConfigStatus != EMBER_SUCCESS)
        {
          printError(Service, "NCP NWK: Set Config item index %d failed with Status = 0x%02X\n", ZAB_VND_NWK(Service).configItemIndex, ConfigStatus);
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
          zabNcpNwk_Create(Status, Service);
        }
      else
        {
          ZAB_VND_NWK(Service).configItemIndex++;
          setConfiguration(Status, Service);
        }
    }
}

/******************************************************************************
 * Set Policy Response Handler
 ******************************************************************************/
void zabNcpNwk_SetPolicyResponseHandler(erStatus* Status, zabService* Service, unsigned8 PolicyStatus)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_INIT_SET_POLICY)
    {
      if (PolicyStatus != EMBER_SUCCESS)
        {
          printError(Service, "NCP NWK: Set Policy item index %d failed with Status = 0x%02X\n", ZAB_VND_NWK(Service).configItemIndex, PolicyStatus);
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
          zabNcpNwk_Create(Status, Service);
        }
      else
        {
          ZAB_VND_NWK(Service).configItemIndex++;
          setPolicies(Status, Service);
        }
    }
}

/******************************************************************************
 * Get Binding Response Handler
 ******************************************************************************/
void zabNcpNwk_GetBindingResponseHandler(erStatus* Status, zabService* Service, unsigned8 BindingStatus, EmberBindingTableEntry* BindingEntry)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_GET_BINDING_TABLE)
    {
      /* If everything is good then copy entry to binding table, otherwise fail */
      if ( (BindingStatus == EMBER_SUCCESS) &&
           (ZAB_VND_NWK(Service).configItemIndex < ZAB_VENDOR_BINDING_TABLE_SIZE) &&
           (BindingEntry != NULL) )
        {
          osMemCopy( Status, (unsigned8*)&ZAB_VND_NWK(Service).bindingTable[ZAB_VND_NWK(Service).configItemIndex], (unsigned8*)BindingEntry, sizeof(EmberBindingTableEntry) );

          ZAB_VND_NWK(Service).configItemIndex++;
          getBindingTable(Status, Service);
        }
      else
        {
          printError(Service, "NCP NWK: Get Binding item index %d failed with Status = 0x%02X\n", ZAB_VND_NWK(Service).configItemIndex, BindingStatus);
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
          zabNcpNwk_Create(Status, Service);
        }
    }
}

/******************************************************************************
 * Remote Set Binding Handler
 ******************************************************************************/
void zabNcpNwk_RemoteSetBindingHandler(erStatus* Status, zabService* Service, unsigned8 BindingStatus, unsigned8 BindingIndex, EmberBindingTableEntry* BindingEntry)
{
  /* If we are networked and everything else is good then copy entry to binding table, otherwise ignore */
  if ( (zabNcpNwk_GetNwkState(Service) == ZAB_NWK_STATE_NETWORKED) &&
       (BindingStatus == EMBER_SUCCESS) &&
       (BindingIndex < ZAB_VENDOR_BINDING_TABLE_SIZE) &&
       (BindingEntry != NULL) )
    {
      printVendor(Service, "NCP NWK: Adding Binding item at index %d\n", BindingIndex);
      osMemCopy( Status, (unsigned8*)&ZAB_VND_NWK(Service).bindingTable[BindingIndex], (unsigned8*)BindingEntry, sizeof(EmberBindingTableEntry) );
    }
}

/******************************************************************************
 * Remote Delete Binding Handler
 ******************************************************************************/
void zabNcpNwk_RemoteDeleteBindingHandler(erStatus* Status, zabService* Service, unsigned8 BindingStatus, unsigned8 BindingIndex)
{
  /* If we are networked and everything else is good then delete entry in binding table, otherwise ignore */
  if ( (zabNcpNwk_GetNwkState(Service) == ZAB_NWK_STATE_NETWORKED) &&
       (BindingStatus == EMBER_SUCCESS) &&
       (BindingIndex < ZAB_VENDOR_BINDING_TABLE_SIZE) )
    {
      printVendor(Service, "NCP NWK: Deleting Binding item at index %d\n", BindingIndex);
      ZAB_VND_NWK(Service).bindingTable[BindingIndex].type = EMBER_UNUSED_BINDING;
    }
}

/******************************************************************************
 * Set Manufacturer Code Response Handler
 ******************************************************************************/
void zabNcpNwk_SetManufacturerCodeResponseHandler(erStatus* Status, zabService* Service)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_INIT_SET_MS_CODE)
    {
      zabNcpEzspNetworking_NetworkInit(Status, Service);
      checkStatusSetItemCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_NCP_INIT, ZAB_VND_CFG_M_NWK_CONFIRM_TIMEOUT_MS);
    }
}

/******************************************************************************
 * Broadcast Next Network Key Response Handler
 ******************************************************************************/
void zabNcpNwk_BroadcastNextNetworkKeyResponseHandler(erStatus* Status, zabService* Service, unsigned8 KeyStatus)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_KEY_BROADCAST)
    {
      setInactive(Service);

      if ( (KeyStatus != EMBER_SUCCESS) ||
           (checkStatusSetItemNonCritical(Status, Service, ZAB_NWK_CURRENT_ITEM_DELAY_BEFORE_KEY_SWITCH, ZAB_VND_CFG_M_NETWORK_KEY_SWITCH_DELAY_MS) == zab_false) )
        {
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
          sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_FAILED);
          sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
        }
    }
}

/******************************************************************************
 * Broadcast Next Network Key Switch Response Handler
 ******************************************************************************/
void zabNcpNwk_BroadcastNextNetworkKeySwitchResponseHandler(erStatus* Status, zabService* Service, unsigned8 KeyStatus)
{
  if (ZAB_VND_NWK(Service).currentItem == ZAB_NWK_CURRENT_ITEM_KEY_SWITCH)
    {
      setInactive(Service);

      if (KeyStatus == EMBER_SUCCESS)
        {
          sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_SUCCESS);
          sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
        }
      else
        {
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_ACTION_FAILED);
          sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_FAILED);
          sapMsgPutNotifyNetworkKeyChangeState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_NETWORK_KEY_CHANGE_STATE_NONE);
        }
    }
}

/******************************************************************************
 * Get a binding index out of the binding table
 *
 * Returns:
 *   Binding index:
 *     ZAB_NCP_NWK_BINDING_INDEX_NONE if not found
 *     Otherwise index of matching bind in binding table
 *   *BindItem:
 *     Pointer to binding item (if binding index != ZAB_NCP_NWK_BINDING_INDEX_NONE)
 *   *MoreBindsInTable:
 *     zab_true if there are more matching bind items following in the table
 ******************************************************************************/
unsigned8 zabNcpNwk_GetBindingIndex(erStatus* Status, zabService* Service,
                                    unsigned8 SourceEndpoint, unsigned16 Cluster, unsigned8 TransactionId,
                                    const EmberBindingTableEntry** BindItem,
                                    zab_bool* MoreBindsInTable)
{
  unsigned8 index;
  unsigned8 bindIndex;

  bindIndex = ZAB_NCP_NWK_BINDING_INDEX_NONE;
  *BindItem = NULL;
  *MoreBindsInTable = zab_false;


  if (zabNcpNwk_GetNwkState(Service) == ZAB_NWK_STATE_NETWORKED)
    {

      /* If we are continuing with the previous transaction, start from after last returned index.
       * Otherwise start from zero.*/
      index = 0;
      if (ZAB_VND_NWK(Service).bindSearchLastTid == TransactionId)
        {
          index = ZAB_VND_NWK(Service).bindSearchLastIndex+1;
        }

      /* Search table for matching bind.
       * Save index of first one to return.
       * If there are more, flag it. */
      for (; index < ZAB_VENDOR_BINDING_TABLE_SIZE; index++)
        {
          if ( (ZAB_VND_NWK(Service).bindingTable[index].local == SourceEndpoint) &&
               (ZAB_VND_NWK(Service).bindingTable[index].clusterId == Cluster) )
            {
              if (*BindItem == NULL)
                {
                  bindIndex = index;
                  *BindItem = &ZAB_VND_NWK(Service).bindingTable[index];
                }
              else
                {
                  *MoreBindsInTable = zab_true;
                }
            }
        }

      /* Save transaction id so we can tell if next call is a new search or a continuation.
       * Save returned index if there are more to come, otherwise clear. */
      ZAB_VND_NWK(Service).bindSearchLastTid = TransactionId;
      if (*MoreBindsInTable == zab_true)
        {
          ZAB_VND_NWK(Service).bindSearchLastIndex = bindIndex;
        }
      else
        {
          ZAB_VND_NWK(Service).bindSearchLastIndex = 0;
        }
    }

  return bindIndex;
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/