/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/******************************************************************************
* File Name     : r_flash_api_rx600_private.h
* Device(s)     : RX631
* Tool-chain    : KPIT GNURX-ELF 15.01
* H/W platform  : G-CPX / EU-CPX2 / G-CPX3
* Description   : Sample software
******************************************************************************/

#ifndef R_FLASH_API_RX600_PRIVATE_H
#define R_FLASH_API_RX600_PRIVATE_H

#include "r_config.h"

/* System clock speed in Hz. */
#define ICLK_HZ                              (96000000uL)

/* Peripheral clock speed in Hz. */
#define PCLK_HZ                              (24000000uL)

/* External bus clock speed in Hz. */
#define BCLK_HZ                              (6000000uL)

/* FlashIF clock speed in Hz. */
#define FCLK_HZ                              (48000000uL)

/******************************************************************************
   Macro definitions
******************************************************************************/

/* This catches to make sure the user specified a CPU clock */
#if !defined (ICLK_HZ)
    #error "ERROR !!! You must specify the System Clock Frequency (ICLK_HZ) !";
#endif

/* Define the clock frequency supplied to the FCU. On the RX610 and Rx62x
   this is the PCLK. On the RX63x it is the FCLK. */
#if MCU_RX631==1
    #define FLASH_CLOCK_HZ (FCLK_HZ)
#endif

/******************************************************************************
   MCU Specific Items
******************************************************************************/

/* This flag is used for setting/clearing the I bit in the PSW */
#define I_FLAG (0x00010000uL)

/* Below is memory information that is specific to device families (RX610,
   RX62N, etc...).  While memory sizes will differ between family members,
   the memory layout should remain the same */

/* Do not edit below this line unless you are editing for another RX with
   a different memory layout. */

/*  Bottom of User Flash Area */
#define ROM_PE_ADDR (((0xFFFFFFFFuL - ROM_SIZE_BYTES) & (0x00FFFFFFuL)) + 1)

#if MCU_RX631==1
/*  According to HW Manual the Max Programming Time for 128 bytes (ROM)
            is 12ms.  This is with a FCLK of 50MHz. The calculation below
            calculates the number of ICLK ticks needed for the timeout delay.
            The 12ms number is adjusted linearly depending on the FCLK frequency.
 */
    #define     WAIT_MAX_ROM_WRITE \
    ((int32_t)(12000 * (50.0 / (FLASH_CLOCK_HZ / 1000000))) * (ICLK_HZ / 1000000))

/*  According to HW Manual the Max Programming Time for 2 bytes
            (Data Flash) is 2ms.  This is with a FCLK of 50MHz. The calculation
            below calculates the number of ICLK ticks needed for the timeout delay.
            The 5ms number is adjusted linearly depending on the FCLK frequency.
 */
    #define     WAIT_MAX_DF_WRITE \
    ((int32_t)(2000 * (50.0 / (FLASH_CLOCK_HZ / 1000000))) * (ICLK_HZ / 1000000))

/*  According to HW Manual the Max Blank Check time for 2k bytes
            (Data Flash) is 0.7ms.  This is with a FCLK of 50MHz. The calculation
            below calculates the number of ICLK ticks needed for the timeout delay.
            The 0.7ms number is adjusted linearly depending on the FCLK frequency.
 */
    #define     WAIT_MAX_BLANK_CHECK \
    ((int32_t)(700 * (50.0 / (FLASH_CLOCK_HZ / 1000000))) * (ICLK_HZ / 1000000))

/*  According to HW Manual the max timeout value when using the peripheral
    clock notification command is 60us. This is with a FCLK of 50MHz. The
        calculation below calculates the number of ICLK ticks needed for the
        timeout delay. The 10us number is adjusted linearly depending on
        the FCLK frequency.
 */
    #define     WAIT_MAX_NOTIFY_FCU_CLOCK \
    ((int32_t)(60 * (50.0 / (FLASH_CLOCK_HZ / 1000000))) * (ICLK_HZ / 1000000))

#else
    #error "ERROR !!! Define timeout values for this device \
    in r_flash_api_rx600.h !!!"
#endif

#if MCU_RX631==1
/* FCU-RAM address define */
/* FCU F/W Store Address */
    #define FCU_PRG_TOP  (0xFEFFE000uL)

/* FCU RAM Address */
    #define FCU_RAM_TOP  (0x007F8000uL)

/* FCU RAM Size */
    #define FCU_RAM_SIZE (0x2000u)
#else
    #error "ERROR !!! Set memory locations for FCU RAM and ROM \
    in r_flash_api_rx600.h !!!"
#endif

#if MCU_RX631==1

/* Defines the number of flash areas */
    #define NUM_ROM_AREAS         (4u)

/* Defines the start program/erase address for the different flash areas */
    #define ROM_AREA_0            (0x00F80000uL)
    #define ROM_AREA_1            (0x00F00000uL)
    #define ROM_AREA_2            (0x00E80000uL)
    #define ROM_AREA_3            (0x00E00000)

/*  Bottom of DF Area */
    #define DF_ADDRESS            (0x00100000uL)

/* Used for getting DF block */
    #define DF_MASK               (0xFFFFF800uL)

/* Used for getting erase boundary in DF block when doing blank checking */
    #define DF_ERASE_BLOCK_SIZE   (0x00000020uL)

/* This is used to get the boundary of the 'fake' blocks that are 2KB. */
    #define DF_BLOCK_SIZE_LARGE   (0x00000800uL)

/* Defines how many DF blocks are on this part */
    #define DF_NUM_BLOCKS         (16u)

/* Defines how many ROM blocks are on this part */
    #if ROM_SIZE_BYTES == 2097152u
        #define ROM_NUM_BLOCKS    (70u) /* 2MB part */
    #elif ROM_SIZE_BYTES == 1572864u
        #define ROM_NUM_BLOCKS    (62u) /* 1.5MB part */
    #elif ROM_SIZE_BYTES == 1048576u
        #define ROM_NUM_BLOCKS    (54u) /* 1MB part */
    #elif ROM_SIZE_BYTES == 786432u
        #define ROM_NUM_BLOCKS    (46u) /* 768KB part */
    #elif ROM_SIZE_BYTES == 524288u
        #define ROM_NUM_BLOCKS    (38u) /* 512KB part */
    #elif ROM_SIZE_BYTES == 393216u
        #define ROM_NUM_BLOCKS    (30u) /* 384KB part */
    #endif

/* Programming size for ROM in bytes */
    #define ROM_PROGRAM_SIZE      (128u)

/* Programming size for data flash in bytes */
/* RX631 only programs in 2-byte intervals */
    #define DF_PROGRAM_SIZE_SMALL (2u)

/*  According to HW Manual the Max Erasure Time for a 64kB block is
            around 1152ms.  This is with a FCLK of 50MHz. The calculation below
            calculates the number of ICLK ticks needed for the timeout delay.
            The 1152ms number is adjusted linearly depending on the FCLK frequency.
 */
    #define WAIT_MAX_ERASE \
    ((int32_t)(1152000 * (50.0 / (FLASH_CLOCK_HZ / 1000000))) * (ICLK_HZ / 1000000))

#else
    #error "!!! Need to define memory specifics for this RX600 family \
    in r_flash_api_rx600_private.h !!!"
#endif

#endif /* R_FLASH_API_RX600_PRIVATE_H */
