/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the networking state machine for the ZAB Pro ZNP
 *   vendor.
 *   For details and flow charts see XXX.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 02.00.00.01  31-May-13   MvdB   Original
 * 00.00.06.02  30-Sep-14   MvdB   artf55512: Support network key change action
 * 00.00.06.04  06-Oct-14   MvdB   artf56243: Handle local network processor leaves gracefully
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
 * 002.000.003  06-Mar-15   MvdB   ARTF116061: Support run time configuration of Green Power endpoint.
 * 002.000.004  01-Apr-15   MvdB   ARTF56504: Support missed channel change recovery, with actions/notifications/asks/gives to get/set parameters.
 * 002.002.012  14-Oct-15   MvdB   ARTF104107: Support End Device operation
 *****************************************************************************/
#ifndef __ZAB_ZNP_NWK_H__
#define __ZAB_ZNP_NWK_H__


#include "zabZnpNwkTypes.h"
#include "zabZnpService.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Create
 * Initialises networking state / state machine
 ******************************************************************************/
extern void zabZnpNwk_create(erStatus* Status, zabService* Service);

/******************************************************************************
 * Get Network State
 ******************************************************************************/
extern zabNwkState zabZnpNwk_GetNwkState(zabService* Service);

/******************************************************************************
 * Get if the Network State Machine is currently busy doing something
 ******************************************************************************/
extern zab_bool zabZnpNwk_GetNwkStateMachineInProgress(zabService* Service);

/******************************************************************************
 * Update timeout
 * Check for timeouts within the networking state machine
 ******************************************************************************/
extern unsigned32 zabZnpNwk_updateTimeout(erStatus* Status, zabService* Service, unsigned32 Time);

/******************************************************************************
 * 1 second tick handler
 ******************************************************************************/
extern void zabZnpNwk_oneSecondTickHandler(erStatus* Status, zabService* Service);

/******************************************************************************
 * SRSP Timeout Handler
 * Notifies state machine a command has timed out. It will handle it if
 * it generated the command.
 ******************************************************************************/
extern void zabZnpNwk_srspTimeoutHandler(erStatus* Status, zabService* Service);

/******************************************************************************
 * Reset Indication Handler
 * Re-initialises networking states if dongle is reset
 ******************************************************************************/
extern void zabZnpNwk_resetHandler(erStatus* Status, zabService* Service);

/******************************************************************************
 * Serial Offline Handler
 * Re-initialises networking states if dongle goes offline
 ******************************************************************************/
extern void zabZnpNwk_serialOfflineHandler(erStatus* Status, zabService* Service);

/******************************************************************************
 * Action - Networking
 * Handles Init, Discover and Steer actions for the networking state machines
 ******************************************************************************/
extern void zabZnpNwk_action(erStatus* Status, zabService* Service, unsigned8 Action);

/******************************************************************************
 * Network Key Update SRSP Handler
 ******************************************************************************/
extern void zabZnpNwk_nwkKeyUpdateSrspHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result);

/******************************************************************************
 * Network Key Switch SRSP Handler
 ******************************************************************************/
extern void zabZnpNwk_nwkKeySwitchSrspHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result);

/******************************************************************************
 * Mgmt Permit Join SRSP Handler
 ******************************************************************************/
extern void zabZnpNwk_mgmtPermitJoinSrspHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result);

/******************************************************************************
 * Write configuration response handler
 * Used to confirm success/fail of writes performed by networking state machine
 ******************************************************************************/
extern void zabZnpNwk_writeCfgRspHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result);

/******************************************************************************
 * Network Address Response Handler
 ******************************************************************************/
extern void zabZnpNwk_setTxPowerRspHandler(erStatus* Status, zabService* Service);

/******************************************************************************
 * GP Set Parameter response handler
 * Used to confirm success/fail of writes performed by networking state machine
 ******************************************************************************/
extern void zabZnpNwk_gpSetParamRspHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result);

/******************************************************************************
 * Device Logical Type Response Handler
 ******************************************************************************/
extern void zabZnpNwk_deviceLogicalTypeHandler(erStatus* Status, zabService* Service, znpSapiLogicalType logicalType);

/******************************************************************************
 * Device State Response Handler
 ******************************************************************************/
extern void zabZnpNwk_deviceStateHandler(erStatus* Status, zabService* Service, ZDO_STATE state);

/******************************************************************************
 * Parent Network Address Response Handler
 ******************************************************************************/
extern void zabZnpNwk_parentNwkAddrHandler(erStatus* Status, zabService* Service, unsigned16 nwkAddr);

/******************************************************************************
 * Parent IEEE Address  Response Handler
 ******************************************************************************/
extern void zabZnpNwk_parentIeeeAddressHandler(erStatus* Status, zabService* Service, unsigned64 ieeeAddress);

/******************************************************************************
 * Network Address Response Handler
 ******************************************************************************/
extern void zabZnpNwk_nwkAddrHandler(erStatus* Status, zabService* Service, unsigned16 nwkAddr);

/******************************************************************************
 * Channel Response Handler
 ******************************************************************************/
extern void zabZnpNwk_channelHandler(erStatus* Status, zabService* Service, unsigned8 channel);

/******************************************************************************
 * PAN ID Response Handler
 ******************************************************************************/
extern void zabZnpNwk_panIdlHandler(erStatus* Status, zabService* Service, unsigned16 panId);

/******************************************************************************
 * Netowrk Key Sequence Number Response Handler
 ******************************************************************************/
extern void zabZnpNwk_nwkKeySequenceNumberHandler(erStatus* Status, zabService* Service, unsigned8 nwkKeySeqNum);

/******************************************************************************
 * IEEE Address Handler
 ******************************************************************************/
extern void zabZnpNwk_ieeeAddressHandler(erStatus* Status, zabService* Service, unsigned64 ieeeAddress);

/******************************************************************************
 * EPID Response Handler
 ******************************************************************************/
extern void zabZnpNwk_extendedPanIdHandler(erStatus* Status, zabService* Service, unsigned64 extendedPanId);

/******************************************************************************
 * ZDO Startup From App Response Handler
 ******************************************************************************/
extern void zabZnpNwk_startupFromAppRspHandler(erStatus* Status, zabService* Service, unsigned8 result);

/******************************************************************************
 * ZDO State Change Handler
 ******************************************************************************/
extern void zabZnpNwk_zdoStateChangeHandler(erStatus* Status, zabService* Service, ZDO_STATE zdoState);

/******************************************************************************
 * Network Discovery Response Handler
 ******************************************************************************/
extern void zabZnpNwk_nwkDiscoveryRspHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result);

/******************************************************************************
 * Network Discovery Confirm Handler
 ******************************************************************************/
extern void zabZnpNwk_NwkDiscoveryCnfHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result);

/******************************************************************************
 * Join Response Handler
 ******************************************************************************/
extern void zabZnpNwk_joinRspHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result);

/******************************************************************************
 * Join Confirm Handler
 ******************************************************************************/
extern void zabZnpNwk_joinCnfHandler(erStatus* Status, zabService* Service, ZNPI_API_ERROR_CODES result);

/******************************************************************************
 * Handle an incoming Permit Join Indication
 ******************************************************************************/
extern void zabZnpNwk_permitJoinIndicationHandler(erStatus* Status, zabService* Service, unsigned8 permitJoinTime);


/******************************************************************************
 * Handle an incoming Local Leave Indications
 ******************************************************************************/
extern void zabZnpNwk_localLeaveHandler(erStatus* Status, zabService* Service, unsigned8 rejoin);

/******************************************************************************
 * SRSP Timeout Handler
 * Notifies state machine a command has timed out. It will handle it if
 * it generated the command.
 ******************************************************************************/
extern
void zabZnpNwk_nwkMaintenaceParamSrspHandler(erStatus* Status, zabService* Service,
                                             ZNPI_API_ERROR_CODES result,
                                             unsigned32 PingTimeSlow,
                                             unsigned32 PingTimeFast);

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/

#ifdef __cplusplus
}
#endif


#endif
