/*******************************************************************************
  Filename    : endianness.c
  $Date       : 2013-05-22                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : Endianness functions for the SZL
*******************************************************************************/

/**************************************************************************************************
* INCLUDES
**************************************************************************************************/
#include "szl.h"
#include "endianness.h"

/**************************************************************************************************
* DEFINES
**************************************************************************************************/
/*
 * Global defines
*/

/*
 * Application data defines
 */

/**************************************************************************************************
* ENUMs, STRUCTs & UNIONs
**************************************************************************************************/

/**************************************************************************************************
* PROTOS
**************************************************************************************************/

/**************************************************************************************************
* VARIABLES
**************************************************************************************************/

/**************************************************************************************************
* FUNCTIONS
**************************************************************************************************/

/**
 * @brief   Copies an szl_uint24 (32bit) in native endianness into a szl_uint24 (24bit) LE field
 *
 * @param   na - Native endian to be converted
 *
 * @return  the converted data
 */
le_uint24 UINT24_TO_LE(szl_uint24 na)
{
    le_uint24 le;

    le.hi = (szl_uint8) UINT16_TO_LE( UINT32_HI(na));
    le.lo = UINT16_TO_LE( UINT32_LO(na));

    return le;
}

/**
 * @brief   Copies an szl_uint24 (24bit) in LE into a szl_uint24 (24bit) native endianness field
 *
 * @param   le - Little Endian to be converted
 *
 * @return  the converted data
 */
szl_uint24 LE_TO_UINT24(le_uint24 le)
{
    szl_uint24 na;

    na = ((szl_uint32)le.hi << 16) | LE_TO_UINT16(le.lo);

    return na;
}

/**
 * @brief   Copies an szl_uint40 (64bit) in native endianness into a szl_uint40 (40bit) LE field
 *
 * @param   na - Native endian to be converted
 *
 * @return  the converted data
 */
le_uint40 UINT40_TO_LE(szl_uint40 na)
{
    le_uint40 le;

#if !SUPPORTS_64_BIT_DATATYPES
    // swap every values
    le.hi = na.hi;
    le.lo = UINT32_TO_LE(na.lo);
#else
    le.hi = (szl_uint8)UINT16_TO_LE( UINT32_LO( UINT64_HI(na)) );
    le.lo = UINT32_TO_LE( UINT64_LO(na));
#endif
    return le;
}

/**
 * @brief   Copies an szl_uint40 (40bit) in LE into a szl_uint40 (64bit) native endianness field
 *
 * @param   le - Little Endian to be converted
 *
 * @return  the converted data
 */
szl_uint40 LE_TO_UINT40(le_uint40 le)
{
    szl_uint40 na;

#if !SUPPORTS_64_BIT_DATATYPES
    // swap every values
    na.reserved = 0;
    na.reserved2 = 0;
    na.hi = le.hi;
    na.lo = LE_TO_UINT32(le.lo);
#else
    na = (szl_uint40)le.hi << 32 | LE_TO_UINT32(le.lo);
#endif
    return na;
}

/**
 * @brief   Copies an szl_uint48 (64bit) in native endianness into a szl_uint48 (48bit) LE field
 *
 * @param   na - Native endian to be converted
 *
 * @return  the converted data
 */
le_uint48 UINT48_TO_LE(szl_uint48 na)
{
    le_uint48 le;

#if !SUPPORTS_64_BIT_DATATYPES
    // swap every values
    le.hi = UINT16_TO_LE(na.hi);
    le.lo = UINT32_TO_LE(na.lo);
#else
    le.hi = UINT16_TO_LE( UINT32_LO( UINT64_HI(na)) );
    le.lo = UINT32_TO_LE( UINT64_LO(na));
#endif
    return le;
}

/**
 * @brief   Copies an szl_uint48 (48bit) in LE into a szl_uint48 (64bit) native endianness field
 *
 * @param   le - Little Endian to be converted
 *
 * @return  the converted data
 */
szl_uint48 LE_TO_UINT48(le_uint48 le)
{
    szl_uint48 na;

#if !SUPPORTS_64_BIT_DATATYPES
    // swap every values
    na.reserved = 0;
    na.hi = LE_TO_UINT16(le.hi);
    na.lo = LE_TO_UINT32(le.lo);
#else
    na = ((szl_uint48)LE_TO_UINT16(le.hi)) << 32 | LE_TO_UINT32(le.lo);
#endif
    return na;
}

/**
 * @brief   Copies an szl_uint56 (64bit) in native endianness into a szl_uint56 (56bit) LE field
 *
 * @param   na - Native endian to be converted
 *
 * @return  the converted data
 */
le_uint56 UINT56_TO_LE(szl_uint56 na)
{
    le_uint56 le;

#if !SUPPORTS_64_BIT_DATATYPES
    // swap every values
    le.hi_hi = na.hi_hi;
    le.hi_lo = UINT16_TO_LE(na.hi_lo);
    le.lo = UINT32_TO_LE(na.lo);
#else
    le.hi_hi = (szl_uint8)UINT16_TO_LE( UINT32_HI( UINT64_HI(na)) );
    le.hi_lo = UINT16_TO_LE( UINT32_LO( UINT64_HI(na)) );
    le.lo = UINT32_TO_LE( UINT64_LO(na));
#endif
    return le;
}

/**
 * @brief   Copies an szl_uint56 (56bit) in LE into a szl_uint56 (64bit) native endianness field
 *
 * @param   le - Little Endian to be converted
 *
 * @return  the converted data
 */
szl_uint56 LE_TO_UINT56(le_uint56 le)
{
    szl_uint56 na;

#if !SUPPORTS_64_BIT_DATATYPES
    // swap every values
    na.reserved = 0;
    na.hi_hi = le.hi_hi;
    na.hi_lo = LE_TO_UINT16(le.hi_lo);
    na.lo = LE_TO_UINT32(le.lo);
#else
    na = ((szl_uint56)le.hi_hi << 16 | (szl_uint56)LE_TO_UINT16(le.hi_lo)) << 32 | (szl_uint56)LE_TO_UINT32(le.lo);
#endif
    return na;
}

/**
 * @brief   Copies an szl_uint64 (64bit) in native endianness into a szl_uint64 (64bit) LE field
 *
 * @param   na - Native endian to be converted
 *
 * @return  the converted data
 */
le_uint64 UINT64_TO_LE(szl_uint64 na)
{
    le_uint64 le;
#if !SUPPORTS_64_BIT_DATATYPES

    // swap every 32bit values
    le.hi = UINT32_TO_LE(na.hi);
    le.lo = UINT32_TO_LE(na.lo);
#else
    le.hi = UINT32_TO_LE( UINT64_HI(na));
    le.lo = UINT32_TO_LE( UINT64_LO(na));
#endif

    return le;
}

/**
 * @brief   Copies an szl_uint64 (64bit) in LE into a szl_uint64 (64bit) native endianness field
 *
 * @param   le - Little Endian to be converted
 *
 * @return  the converted data
 */
szl_uint64 LE_TO_UINT64(le_uint64 le)
{
    szl_uint64 na;

#if !SUPPORTS_64_BIT_DATATYPES
    // swap every values
    na.hi = LE_TO_UINT32(le.hi);
    na.lo = LE_TO_UINT32(le.lo);
#else
    na = (szl_uint64)LE_TO_UINT32(le.hi) << 32 | LE_TO_UINT32(le.lo);
#endif

    return na;
}

#if !SUPPORTS_64_BIT_DATATYPES
/**
 * @brief   Copies an szl_uint64 (64bit) in LE into a szl_uint64 (64bit) native endianness field
 *
 * @param   hi - High bytes
 * @param   lo - Low bytes
 *
 * @return  szl_uint64 from a hi and a lo value
 */
szl_uint64 UINT64_FROM_HI_LO(szl_uint32 hi, szl_uint32 lo)
{
    szl_uint64 n64;

    n64.hi = hi;
    n64.lo = lo;

    return n64;
}

#endif
