/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the Basic Cluster.
 *   This plugin is originally designed for Nova (partner).
 * 
 *   It supports:
 *    - Multiple, application specified endpoints
 *    - Standard attributes
 * 
 *   It does not (currently) support:
 *    - Non-volatile backing of any data.
 *
 * USAGE:
 *   Usage of this plugin is very simple:
 *    1. Initialise it with SzlPlugin_BasicClusterInit(), specifying the endpoints
 *    2. Use SzlPlugin_BasicCluster_GetAttributePointer() to get access to the data for each endpoint.
 *    3. Populate the data
 * 
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    1         20-Feb-14   MvdB   Original
 *    2         19-May-14   MvdB   Remove endpoint number from public attribute data
 *    3         17-Sep-14   MvdB   #56501: Add generation of Node Communications Status notification
 *    4         30-Oct-14   MvdB   artf74202: Support callbacks in Nova plugins
 *    6         19-Feb-15   MvdB   Remove obsoleted attributes: ATTRID_BASIC_DEVICE_NAME_STRING
 *    7         26-Feb-15   MvdB   Correct ATTRID_BASIC_POWER_SOURCE DataType from SZL_ZB_DATATYPE_UINT8 to SZL_ZB_DATATYPE_ENUM8
 *    8         01-Apr-15   MvdB   ARTF116256: Ensure SZL uses szl_memset and szl_memcpy rather than memset and memcpy
 *    9         15-Jul-15   MvdB   ARTF136585: Add transaction IDs to over the air commands/responses
 *   10         20-Jul-15   MvdB   ARTF134686: Update allocation method to be generic and protect against multiple initialisation
 * 002.001.006  27-Jul-15   MvdB   ARTF134686: Standardise on SZL_RESULT_t for plugin init return value
 *                                 ARTF138318: Improve plugin destroy
 * 002.002.010  09-Oct-15   MvdB   ARTF150980: Support Service ID in memory allocation functions for WTB
 *****************************************************************************/

#include <stdio.h>
#include <stddef.h>
#include <stddef.h>
#include <string.h>
#include "zabCoreService.h"
#include "zabCorePrivate.h"
#include "szl.h"
#include "szl_plugin.h"
#include "szl_report.h"

#include "basic_cluster.h"

/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Cluster ID */
#define ZCL_CLUSTER_ID_BASIC     0x0000



/**
 * Basic Cluster commands
 */
typedef enum
{
  BASIC_COMMAND_MS_NODE_COMMUNICATION_NTF     = 0x80,
} BASIC_CLUSTER_CMDS;

/* Command payload lengths */
#define M_NODE_COMMUNICATION_NTF_PAYLOAD_LENGTH 1

typedef struct
{
  SzlPlugin_Basic_Attributes_t AttributeData;
  szl_uint8 EndpointNumber;
}SzlPlugin_Basic_Endpoints_t;

/* Parameters for the service */
typedef struct
{
  szl_uint8 numEndpoints;
#ifdef BASIC_M_USE_DIRECT_ACCESS
  SzlPlugin_Basic_Endpoints_t BasicEndpoints[VLA_INIT];
#else
  szl_uint8 Endpoints[VLA_INIT];
#endif
}SzlPlugin_Basic_Params_t;

#ifdef BASIC_M_USE_DIRECT_ACCESS
#define SzlPlugin_Basic_Params_t_SIZE(numEndpoints) (sizeof(SzlPlugin_Basic_Params_t) - (VLA_INIT * sizeof(SzlPlugin_Basic_Endpoints_t))+ (sizeof(SzlPlugin_Basic_Endpoints_t) * (numEndpoints)))
#else
#define SzlPlugin_Basic_Params_t_SIZE(numEndpoints) (sizeof(SzlPlugin_Basic_Params_t) - (VLA_INIT * sizeof(szl_uint8))+ (sizeof(szl_uint8) * (numEndpoints)))
#endif

/* 
 * The set of attributes for the cluster.
 * This const data is used to initialise the majority of the attribute table.
 * Once it is copied into RAM, Endpoint and Data Access pointers are set for each attribute.
 * 
 * Read only attributes use direct access by SZL.
 * Writable attributes use Read/Write callbacks, as the need the write callback to notify the plugin that
 *   a write is happening, so it can notify the app.
 * 
 * New attributes must be added here.
 */
const SZL_DataPoint_t basicDatapointConfig[] = {
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_ZCL_VERSION,          0, SZL_ZB_DATATYPE_UINT8,     1,                                {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, ZclVersion)}}},
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_MANUFACTURER_NAME,    0, SZL_ZB_DATATYPE_CHAR_STR,  BASIC_M_MANUFACTURER_NAME_LENGTH, {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, ManufacturerName)}}},
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_MODEL_IDENTIFIER,     0, SZL_ZB_DATATYPE_CHAR_STR,  BASIC_M_MODEL_IDENTIFIER_LENGTH,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, ModelIdentifier)}}},
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_DATE_CODE,            0, SZL_ZB_DATATYPE_CHAR_STR,  BASIC_M_DATE_CODE_LENGTH,         {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, DateCode)}}},
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_POWER_SOURCE,         0, SZL_ZB_DATATYPE_ENUM8,     1,                                {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_false, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, PowerSource)}}},

  /* =S= MS Attributes */
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_NETWORK_PROCESSOR_FIRMWARE_VERSION, 0, SZL_ZB_DATATYPE_CHAR_STR,  BASIC_M_NETWORK_PROCESSOR_FIRMWARE_VERSION_LENGTH,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, NetworkProcessorFirmwareVersion)}}},
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_APPLICATION_FIRMWARE_VERSION,       0, SZL_ZB_DATATYPE_CHAR_STR,  BASIC_M_APPLICATION_FIRMWARE_VERSION_LENGTH,        {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, ApplicationFirmwareVersion)}}},
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_APPLICATION_HARDWARE_VERSION,       0, SZL_ZB_DATATYPE_CHAR_STR,  BASIC_M_APPLICATION_HARDWARE_VERSION_LENGTH,        {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, ApplicationHardwareVersion)}}},
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_PRODUCT_SERIAL_NUMBER,              0, SZL_ZB_DATATYPE_CHAR_STR,  BASIC_M_PRODUCT_SERIAL_NUMBER_LENGTH,               {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, ProductSerialNumber)}}},

  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_NETWORK_PROCESSOR_HARDWARE_VERSION, 0, SZL_ZB_DATATYPE_CHAR_STR,  BASIC_M_NETWORK_PROCESSOR_HARDWARE_VERSION_LENGTH,  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, NetworkProcessorHardwareVersion)}}},
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_PRODUCT_IDENTIFIER,                 0, SZL_ZB_DATATYPE_ENUM16,    2,                                                  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, ProductIdentifier)}}},
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_PRODUCT_RANGE,                      0, SZL_ZB_DATATYPE_CHAR_STR,  BASIC_M_PRODUCT_RANGE_LENGTH,                       {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, ProductRange)}}},
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_PRODUCT_MODEL,                      0, SZL_ZB_DATATYPE_CHAR_STR,  BASIC_M_PRODUCT_MODEL_LENGTH,                       {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, ProductModel)}}},
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_PRODUCT_FAMILY,                     0, SZL_ZB_DATATYPE_CHAR_STR,  BASIC_M_PRODUCT_FAMILY_LENGTH,                      {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, ProductFamily)}}},
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_VENDOR_URL,                         0, SZL_ZB_DATATYPE_CHAR_STR,  BASIC_M_VENDOR_URL_LENGTH,                          {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, VendorUrl)}}},
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_PRODUCT_CAPABILITY_1,               0, SZL_ZB_DATATYPE_CHAR_STR,  BASIC_M_PRODUCT_CAPABILITY_1_LENGTH,                {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, ProductCapability1)}}},
  {ZCL_CLUSTER_ID_BASIC, ATTRID_BASIC_SYSTEM_MODULE_NUMBER,               0, SZL_ZB_DATATYPE_UINT8,     1,                                                  {DP_METHOD_DIRECT, DP_ACCESS_READ, szl_true, DP_DIRECTION_SERVER}, {.DirectAccess = {(void*)offsetof(SzlPlugin_Basic_Attributes_t, SystemModuleNumber)}}},
};
#define BASIC_M_NUM_ATTRIBUTES_PER_ENDPOINT ( sizeof(basicDatapointConfig) / sizeof(SZL_DataPoint_t))




/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/




/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Plugin Initialisation
 * This function must be called from the application after the Library has been initialized.
 * 
 * Parameters:
 *  Service: Instance of library
 *  numEndpoints: The length of endpointList
 *  endpointList: The endpoints to be initialised with this cluster
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_BasicClusterInit(zabService* Service, 
                                        szl_uint8 numEndpoints, 
                                        szl_uint8 * endpointList
#ifndef BASIC_M_USE_DIRECT_ACCESS
                                        ,
                                        SZL_CB_DataPointRead_t ReadCallback,
                                        SZL_CB_DataPointWrite_t WriteCallback
#endif
                                        )
{
    SZL_RESULT_t result;
    szl_uint8 epIndex;
    szl_uint8 attr;
    SzlPlugin_Basic_Params_t* pluginParams = NULL;
    SZL_DataPoint_t serverAttributes[BASIC_M_NUM_ATTRIBUTES_PER_ENDPOINT];
    szl_bool anyRegisterWorked;
    
    /* Validate inputs */
    if ( (Service == NULL) || (numEndpoints == 0) || (endpointList == NULL) 
#ifndef BASIC_M_USE_DIRECT_ACCESS 
          || (ReadCallback == NULL) || (WriteCallback == NULL)                                             
#endif
        )
      {
        return SZL_RESULT_INVALID_DATA;
      }
    
    /* Malloc parameters the plugin needs to store and link from the SZL */
    result = SZL_PluginRegister(Service, SZL_PLUGIN_ID_BASIC, SzlPlugin_Basic_Params_t_SIZE(numEndpoints), (void**)&pluginParams);
    if ( (result == SZL_RESULT_SUCCESS) && (pluginParams != NULL) )
      {
        /* Set parameters that are not per endpoint */
        pluginParams->numEndpoints = numEndpoints;

        /* Init table of attributes to be registered for each endpoint
         * This is mostly the same for each EP, but we set the endpoint number and data pointers differently */
        anyRegisterWorked = szl_false;
        result = SZL_RESULT_SUCCESS;
        for (epIndex = 0; 
             (epIndex < numEndpoints) && (result == SZL_RESULT_SUCCESS || result == SZL_RESULT_ALREADY_EXISTS);  
             epIndex++)
          {

            /* Init table of attributes to be registered for each endpoint
             * This is mostly the same for each EP, but we set the endpoint number and data pointers differently */
            szl_memcpy(serverAttributes,
                       basicDatapointConfig,
                       sizeof(basicDatapointConfig));

            /* Initialise endpoint number in data
             * All other data is un-initialised - this is up to the app unless we choose to add NVM support later */
#ifdef BASIC_M_USE_DIRECT_ACCESS 
            pluginParams->BasicEndpoints[epIndex].EndpointNumber = endpointList[epIndex];
#else     
            pluginParams->Endpoints[epIndex] = endpointList[epIndex];       
#endif

            /* Set the data pointers in to params correctly by applying offset to base params set*/
            for (attr = 0; attr < BASIC_M_NUM_ATTRIBUTES_PER_ENDPOINT; attr++)
              {
                serverAttributes[attr].Endpoint = endpointList[epIndex];

#ifdef BASIC_M_USE_DIRECT_ACCESS 
                serverAttributes[attr].AccessType.DirectAccess.Data = (void*)((szl_uint8*)serverAttributes[attr].AccessType.DirectAccess.Data + (size_t)&params->BasicEndpoints[epIndex].AttributeData);
#else
                serverAttributes[attr].Property.Method = DP_METHOD_CALLBACK;
                serverAttributes[attr].AccessType.Callback.Read = ReadCallback;
                serverAttributes[attr].AccessType.Callback.Write = WriteCallback;
#endif
              }   

            /* Register the cluster attributes */
            result = SZL_AttributesRegister(Service, serverAttributes, BASIC_M_NUM_ATTRIBUTES_PER_ENDPOINT);

            /* Keep boolean to show if any register worked, as if it did we must ensure the data it points to is valid */
            if(result == SZL_RESULT_SUCCESS || result == SZL_RESULT_ALREADY_EXISTS)
              {
                anyRegisterWorked = szl_true;
              }
          } 

        /* If any register worked, we must keep Params to ensure data pointers are valid. If all failed we can free */
        if(anyRegisterWorked == szl_true)
          {
            printInfo(Service, "PLUGIN BASIC: Init Successful\n");
            return SZL_RESULT_SUCCESS;
          }
        else
          {
            /* Failed. Cleanup */
            SZL_PluginUnregister(Service, SZL_PLUGIN_ID_BASIC);
            result = SZL_RESULT_FAILED;
          }
      }

    return result;
}

/******************************************************************************
 * Plugin Destruction
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_BasicCluster_Destroy(zabService* Service)
{
  SZL_RESULT_t result;
  szl_uint8 epIndex;
  szl_uint8 attr;
  SZL_EP_t endpoint;
  SzlPlugin_Basic_Params_t* pluginParams = NULL;
  SZL_DataPoint_t serverAttributes[BASIC_M_NUM_ATTRIBUTES_PER_ENDPOINT];
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_BASIC, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return SZL_RESULT_INVALID_DATA;
    }
  
  for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
    {
      /* Get the endpoint number */
#ifdef BASIC_M_USE_DIRECT_ACCESS 
      endpoint = pluginParams->BasicEndpoints[epIndex].EndpointNumber;
#else
      endpoint = pluginParams->Endpoints[epIndex];
#endif

      /* Init table of attributes to be de-registered for each endpoint
       * This is mostly the same for each EP, but we set the endpoint number differently */
      szl_memcpy(serverAttributes,
                 basicDatapointConfig,
                 sizeof(basicDatapointConfig));

      /* Set the endpoint number. For deregister we don't care about the access types etc. */
      for (attr = 0; attr < BASIC_M_NUM_ATTRIBUTES_PER_ENDPOINT; attr++)
        {
          serverAttributes[attr].Endpoint = endpoint;
        }   

      /* De-Register the cluster attributes.
       * Don't care too much about the result as we will make best effort only */
      result = SZL_AttributesDeregister(Service, serverAttributes, BASIC_M_NUM_ATTRIBUTES_PER_ENDPOINT);
      if (result != SZL_RESULT_SUCCESS)
        {
          printError(Service, "PLUGIN BASIC: WARNING: Deregister failed for EP 0x%02X with Result 0x%02X\n",
                     endpoint,
                     result);
        }
    }
  
  /* Now unregister the plugin */
  return SZL_PluginUnregister(Service, SZL_PLUGIN_ID_BASIC);
}

/******************************************************************************
 * Get pointer to Basic Cluster Attributes for an endpoint
 * This can be used to get access to the endpoints parameters for the local 
 *  application to read or write them
 * Return NULL if not found
 ******************************************************************************/
#ifdef BASIC_M_USE_DIRECT_ACCESS
SzlPlugin_Basic_Attributes_t* SzlPlugin_BasicCluster_GetAttributePointer(zabService* Service, szl_uint8 endpoint)
{
  szl_uint8 epIndex;
  SzlPlugin_Basic_Params_t* pluginParams = NULL;
  
  /* Validate inputs */
  if ( (Service == NULL) || 
       (SZL_PluginGetDataPointer(Service, SZL_PLUGIN_ID_BASIC, (void**)&pluginParams) != SZL_RESULT_SUCCESS) ||
       (pluginParams == NULL) )
    {
      return NULL;
    }
  
  for (epIndex = 0; epIndex < pluginParams->numEndpoints; epIndex++)
    {
      if (pluginParams->BasicEndpoints[epIndex].EndpointNumber == endpoint)
        {
          return &pluginParams->BasicEndpoints[epIndex].AttributeData;
        }
    }
  return NULL;
}
#endif


/******************************************************************************
 * Cluster Command Response Handler
 ******************************************************************************/
void SzlPlugin_BasicCluster_ClusterCmdRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ClusterCmdRespParams_t *Params, szl_uint8 TransactionId)
{
  /* Do nothing. This always gets called with an error for broadcasts in this version of SZL, so we just suppress it.*/
}


/******************************************************************************
 * Broadcast a Schneider Manufacturer Specific Node Communication Notification command
 * See the Schneider ZCL for details.
 ******************************************************************************/
SZL_RESULT_t SzlPlugin_BasicCluster_SendNodeCommunicationNotification(zabService* Service, 
                                                                      szl_uint8 endpoint, 
                                                                      ENUM_BASIC_NODE_COMMUNICATION_STATUS_t NodeCommsStatus)
{
  SZL_ClusterCmdReqParams_t* cmdReq;
  SZL_RESULT_t res;
  
  /* Malloc command */
  cmdReq = (SZL_ClusterCmdReqParams_t*)szl_mem_alloc(SZL_ClusterCmdReqParams_t_SIZE(M_NODE_COMMUNICATION_NTF_PAYLOAD_LENGTH),
                                                     MALLOC_ID_CFG_PLG_BASIC);
  if (cmdReq == NULL)
    {
      printError(Service, "PLUGIN BASIC: Malloc 2 failed\n");
      return SZL_RESULT_FAILED;
    }
  
  cmdReq->ManufacturerSpecific = szl_true;
  cmdReq->SourceEndpoint = endpoint;
  cmdReq->DestAddrMode.AddressMode = SZL_ADDRESS_MODE_BROADCAST;
  cmdReq->DestAddrMode.Addresses.Broadcast.Mode = SZL_BROADCAST_ROUTERS_AND_COORDINATOR;
  cmdReq->ClusterID = ZCL_CLUSTER_ID_BASIC;
  cmdReq->Command = BASIC_COMMAND_MS_NODE_COMMUNICATION_NTF;
  cmdReq->Direction = ZCL_FRAME_DIR_SERVER_CLIENT;
  cmdReq->PayloadLength = M_NODE_COMMUNICATION_NTF_PAYLOAD_LENGTH;
  cmdReq->Payload[0] = (szl_uint8)NodeCommsStatus;

  res = SZL_ClusterCmdReq(Service, SzlPlugin_BasicCluster_ClusterCmdRspHandler, cmdReq, NULL);
  
  szl_mem_free(cmdReq);
  return res;
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/