/* 
  Name:    Safe Queue Interface
  Author:  ZigBee Excellence Center
  Company: Schneider Electric

  Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

  Description:
    Linked list queue with identifier passed to protection macros. Item structure must have next
    pointer as first field.

 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
*/

#ifndef __Q_SAFE_INTERFACE_H__
#define __Q_SAFE_INTERFACE_H__

#ifdef __cplusplus
extern "C" {
#endif
#include "osTypes.h"

typedef struct qSafeItemStruct qSafeItem;

struct qSafeItemStruct // an item must have Next as the first member 
{
  qSafeItem* Next;
};

typedef struct
{
  qSafeItem* First;  

  /* Service is required here so we can find the correct Mutex */
  void* Service;   
  unsigned8 MutexId;
  unsigned8 Count;     
  
  /* ARTF67933: Deliberately place the pointer at the end to inherit alignment of next byte after the pointer.
   *            DO NOT MOVE THIS OR PUT ITEMS AFTER IT!!!!! */
  qSafeItem* Last; 
} qSafeType;

void qSafeInitialize( void* Service, qSafeType* Q, unsigned8 Id );  // initialize and identify queue
void  qSafeClear( qSafeType* Q );                        // clears queue (reinitialize with same Id)
void  qSafeAppend( qSafeType* Q, void* Item );           // append to back of queue
void  qSafePush( qSafeType* Q, void* Item );             // push on front of queue
void* qSafeRemove( qSafeType* Q );                       // remove from front of queue

unsigned8 qSafeId( qSafeType* Q );    // queue Id from initialize
unsigned8 qSafeCount( qSafeType* Q ); // count of items on queue

#ifdef __cplusplus
}
#endif

#endif // __QUEUE_H__

