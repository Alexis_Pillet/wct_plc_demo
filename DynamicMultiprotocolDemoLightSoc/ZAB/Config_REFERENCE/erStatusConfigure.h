/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file provides configuration options for status management in ZAB.
 *
 *   Almost every ZAB function has as a first parameter type called Status.
 *   This is similar to a function return code value, but much more comprehensive
 *   for error detection and recovery.
 *   Once Status is set with an error, it can not be overwritten with another error
 *   until it is cleared. All other processing shall be suspended until the error
 *   is processed and cleared. The service code always checks the status at function
 *   entry and if there is an error, it will return immediately.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Cam Williams
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.00.02  30-Oct-13   MvdB   Major tidy up
 * 002.002.045  26-Jan-17   MvdB   Remove old options in erStatus
 *****************************************************************************/

#ifndef __ER_STATUS_CONFIGURE_H__
#define __ER_STATUS_CONFIGURE_H__

/* Define ER_ENABLE_VERBOSE_ERROR_DESCRIPTIONS if you want textual descriptions of errors.
 * Leave it undefined to use error numbers only and save flash */
#define ER_ENABLE_VERBOSE_ERROR_DESCRIPTIONS

/* Define ER_CHECK_NULL_ERROR if you want parameters checked for NULL
 * This is strongly recommended during development and testing. */
#define ER_CHECK_NULL_ERROR


/* If ER_STATUS_USER_SET is defined, then erStatusUserSet() is called to allow the user
 * to modify the error data when an error occurs.
 * The app is responsible for implementing erStatusUserSet() */
//#define ER_STATUS_USER_SET

#endif // __ER_STATUS_CONFIGURE_H__
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/

