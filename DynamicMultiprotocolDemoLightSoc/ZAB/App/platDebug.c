/******************************************************************************
 *                          ZAB TEST APPLICATION
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the application Debug Trace functions for a platform
 *   This file is a demonstration only. Application developers are responsible
 *   for adapting this to their platform.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.04.00  14-Apr-14   MvdB   Original
 * 002.000.001  03-Feb-15   MvdB   ARTF104099: Add service pointer to print function for multi instance apps.
 * 002.000.005  07-Apr-15   MvdB   Support colours in console
 * 002.001.004  21-Jul-15   MvdB   Remove console colours. They were cool but clog up the log files.
 * 002.002.005  11-Sep-15   MvdB   Add platDebug_Mutex to help tidy up console output
 * 002.002.009  07-Oct-15   MvdB   Add ms to timestamp
 * 002.002.028  12-Dec-16   MvdB   Add date to timestamp
 *****************************************************************************/

/* Feature test macros required for gettimeofday */
#define _BSD_SOURCE           // For glibc <= 2.2.19
//#define _DEFAULT_SOURCE     // From glibc 2.2.0. Use if you get a warning for _BSD_SOURCE.

#include "osTypes.h"
#include "zabTypes.h"
#include "mutex.h"
#include <time.h>
//#include <sys/time.h>
#include "platDebug.h"
#include <stdio.h>
#include <stdarg.h>
#include "app/framework/include/af.h"

/******************************************************************************
 *                      ******************************
 *                *****         LOCAL VARIABLES        *****
 *                      ******************************
 ******************************************************************************/

/* Boolean value used to manage when to print a timestamp */
static zab_bool platDebug_DebugNewLine = zab_true;

#define US_TO_MS  1000
#define MS_TO_S  1000
#define S_TO_M  60
#define M_TO_H  60
/******************************************************************************
 *                      ******************************
 *                *****        EXTERN VARIABLES        *****
 *                      ******************************
 ******************************************************************************/

/* These are for the prototypes in zabDebug.h */
zab_bool platDebug_DebugInfoOn = zab_false;
zab_bool platDebug_DebugVendorOn = zab_false;
zab_bool platDebug_DebugSapOn = zab_false;
zab_bool platDebug_DebugSerialOn = zab_false;

unsigned8 platDebug_Mutex = 0;

/******************************************************************************
 *                      ******************************
 *                *****        EXTERN FUNCTIONS        *****
 *                      ******************************
 ******************************************************************************/

/* Function to print a timestamp  */
static void platDebug_PrintTimeStamp(void)
{
  uint32_t microSec;
  uint32_t milliSecondes;
  uint8_t heures, minutes,secondes;
  RTOS_ERR             err;
/*  time_t rawtime;
  struct tm * timeinfo;
  struct timeval  tv;
  gettimeofday(&tv, NULL);
  rawtime = tv.tv_sec;
  timeinfo = localtime ( &rawtime );
  if (timeinfo != NULL)
    {
      printf("<%02d/%02d/%04d %02d:%02d:%02d:%03d ",
             timeinfo->tm_mday, timeinfo->tm_mon + 1, 1900 + timeinfo->tm_year,
             timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, (int)tv.tv_usec / 1000);
    }*/
 
  microSec = RAIL_GetTime();
  milliSecondes = microSec/US_TO_MS;
  heures = milliSecondes/(M_TO_H*S_TO_M*MS_TO_S);
  minutes = (milliSecondes - (heures*(M_TO_H*S_TO_M*MS_TO_S)))/(S_TO_M*MS_TO_S);
  secondes = (milliSecondes - (minutes*(S_TO_M*MS_TO_S)) - (heures*(M_TO_H*S_TO_M*MS_TO_S)))/MS_TO_S;
  milliSecondes = (milliSecondes - (secondes*MS_TO_S)- (minutes*(S_TO_M*MS_TO_S)) - (heures*(M_TO_H*S_TO_M*MS_TO_S)));
  emberAfCorePrint("<%d     %02d:%02d:%02d:%03d ",microSec,heures,minutes,secondes,milliSecondes);
  
}


void platPrintApp( zabService* Service, const char * format, ... )
{
  va_list parg;
  va_start(parg, format);

  if ( (platDebug_Mutex > 0) && (Service != NULL) )
    {
      MutexEnter(Service, platDebug_Mutex);
    }

  if(platDebug_DebugNewLine==zab_true)
    {
      platDebug_PrintTimeStamp();
      emberAfCorePrint("App> ");
      platDebug_DebugNewLine=zab_false;
    }
  if(format[strlen(format)-1] == '\n')
    {
		//format[strlen(format)-1] = 0;
      platDebug_DebugNewLine = zab_true;
	  //emberAfCorePrintln(format,parg);
      //emAfPrintInternalVarArg(1, true, format, parg);
      emberSerialPrintfVarArg(EMBER_AF_PRINT_OUTPUT, format, parg);
      emberSerialPrintf(EMBER_AF_PRINT_OUTPUT, "\r\n");
    }
	else
	{
		//emberAfCorePrint(format,parg);
          //emAfPrintInternalVarArg(1, false, format, parg);
          emberSerialPrintfVarArg(EMBER_AF_PRINT_OUTPUT, format, parg);
	}
 // vprintf(format, parg);

  if ( (platDebug_Mutex > 0) && (Service != NULL) )
    {
      MutexExit(Service, platDebug_Mutex);
    }

  va_end(parg);
}


#ifdef PLAT_DEBUG_ENABLE_ERRORS
void platPrintError( zabService* Service, const char * file, const char * func, int line, const char * format, ... )
{
	
  va_list parg;
  va_start(parg, format);

/* DO NOT use mutex for errors. Otherwise error printing of mutex errors causes an infinite loop!
  if ( (platDebug_Mutex > 0) && (Service != NULL) )
    {
      MutexEnter(Service, platDebug_Mutex);
    }
*/

  if(platDebug_DebugNewLine==zab_true)
    {
      platDebug_PrintTimeStamp();
      emberAfCorePrint("ERROR> ");
      platDebug_DebugNewLine=zab_false;
    }
emberSerialPrintfVarArg(EMBER_AF_PRINT_OUTPUT, format, parg);
 // vprintf(format, parg);

  if(format[strlen(format)-1] == '\n')
    {
      emberAfCorePrint("                  - File: %s", file);
      emberAfCorePrint("                  - Func: %s", func);
      emberAfCorePrintln("                  - Line: %d", line);
      platDebug_DebugNewLine = zab_true;
    }

/* DO NOT use mutex for errors. Otherwise error printing of mutex errors causes an infinite loop!
  if ( (platDebug_Mutex > 0) && (Service != NULL) )
    {
      MutexExit(Service, platDebug_Mutex);
    }
*/

  va_end(parg);
}
#endif

#ifdef PLAT_DEBUG_ENABLE_INFO
void platPrintInfo( zabService* Service, const char * format, ... )
{
  
  va_list parg;

  if(platDebug_DebugInfoOn)
    {
      va_start(parg, format);

      if ( (platDebug_Mutex > 0) && (Service != NULL) )
        {
          MutexEnter(Service, platDebug_Mutex);
        }

      if(platDebug_DebugNewLine==zab_true)
        {
          platDebug_PrintTimeStamp();
          emberAfCorePrint("Inf> ");
          platDebug_DebugNewLine=zab_false;
        }
		emberSerialPrintfVarArg(EMBER_AF_PRINT_OUTPUT, format, parg);
      if(format[strlen(format)-1] == '\n')
        {
          platDebug_DebugNewLine = zab_true;
		  emberSerialPrintf(EMBER_AF_PRINT_OUTPUT, "\r\n");
        }

    //  vprintf(format, parg);

      if ( (platDebug_Mutex > 0) && (Service != NULL) )
        {
          MutexExit(Service, platDebug_Mutex);
        }

      va_end(parg);
    }

}
#endif

#ifdef PLAT_DEBUG_ENABLE_VENDOR
void platPrintVendor( zabService* Service, const char * format, ... )
{
  
  va_list parg;

  if(platDebug_DebugVendorOn)
    {
      va_start(parg, format);

      if ( (platDebug_Mutex > 0) && (Service != NULL) )
        {
          MutexEnter(Service, platDebug_Mutex);
        }
      if(platDebug_DebugNewLine==zab_true)
        {
          platDebug_PrintTimeStamp();
          emberAfCorePrint("Vnd> ");
          platDebug_DebugNewLine=zab_false;
        }
		emberSerialPrintfVarArg(EMBER_AF_PRINT_OUTPUT, format, parg);
      if(format[strlen(format)-1] == '\n')
        {
          platDebug_DebugNewLine = zab_true;
		  emberSerialPrintf(EMBER_AF_PRINT_OUTPUT, "\r\n");
        }

      //vprintf(format, parg);

      if ( (platDebug_Mutex > 0) && (Service != NULL) )
        {
          MutexExit(Service, platDebug_Mutex);
        }

      va_end(parg);
    }

}
#endif

#ifdef PLAT_DEBUG_ENABLE_SAP
void platPrintSap( zabService* Service, const char * format, ... )
{
  
  va_list parg;

  if(platDebug_DebugSapOn)
    {
      va_start(parg, format);

      if ( (platDebug_Mutex > 0) && (Service != NULL) )
        {
          MutexEnter(Service, platDebug_Mutex);
        }
      if(platDebug_DebugNewLine==zab_true)
        {
          platDebug_PrintTimeStamp();
          emberAfCorePrint("Sap> ");
          platDebug_DebugNewLine=zab_false;
        }
		emberSerialPrintfVarArg(EMBER_AF_PRINT_OUTPUT, format, parg);
      if(format[strlen(format)-1] == '\n')
        {
          platDebug_DebugNewLine = zab_true;
		  emberSerialPrintf(EMBER_AF_PRINT_OUTPUT, "\r\n");
        }

      //vprintf(format, parg);

      if ( (platDebug_Mutex > 0) && (Service != NULL) )
      {
        MutexExit(Service, platDebug_Mutex);
      }

      va_end(parg);
    }

}
#endif

#ifdef PLAT_DEBUG_ENABLE_SERIAL
void platPrintSerial( zabService* Service, const char * format, ... )
{
  
  va_list parg;

  if(platDebug_DebugSerialOn)
    {
      va_start(parg, format);

      if ( (platDebug_Mutex > 0) && (Service != NULL) )
        {
          MutexEnter(Service, platDebug_Mutex);
        }
      if(platDebug_DebugNewLine==zab_true)
        {
          platDebug_PrintTimeStamp();
          emberAfCorePrint("Ser> ");
          platDebug_DebugNewLine=zab_false;
        }
		emberSerialPrintfVarArg(EMBER_AF_PRINT_OUTPUT, format, parg);
      if(format[strlen(format)-1] == '\n')
        {
          platDebug_DebugNewLine = zab_true;
		  emberSerialPrintf(EMBER_AF_PRINT_OUTPUT, "\r\n");
        }

      //vprintf(format, parg);

      if ( (platDebug_Mutex > 0) && (Service != NULL) )
        {
          MutexExit(Service, platDebug_Mutex);
        }

      va_end(parg);
    }

}
#endif



/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/