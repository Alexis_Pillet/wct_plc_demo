/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the main interface for the ZAB ZNP Vendor.
 *   Warning: Headers are split between:
 *            Public: zabVendorService.h
 *            Private: zabZnpService.h
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.02.00  31-Oct-13   MvdB   Tidy up from original
 * 00.00.03.01  19-Mar-14   MvdB   artf53864: Support mgmt leave request/response
 * 00.00.04.00  23-Apr-14   MvdB   Fix issue with zabZnpService_GetLocalCommsReady() & zabZnpService_GetNetworkCommsReady()
 * 00.00.06.00  12-Jun-14   MvdB   Add Wireless Test Bench functions
 * 00.00.06.03  01-Oct-14   MvdB   artf104879: Support energy scans via Mgmt Nwk Update Request
 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 * 01.100.06.00 10-Feb-15   MvdB   Split ZAB_VENDOR_SREQ_TIMEOUT into ZAB_VENDOR_DATA_CONFIRM_TIMEOUT and ZAB_VENDOR_SRSP_TIMEOUT
 * 002.000.001  03-Feb-15   MvdB   ARTF115770: Support ZDO Node Descriptor Request
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
 * 002.000.006  07-Apr-15   MvdB   ARTF130273: Have LocalWorkIn() check status is ok before dequeueing messages
 * 002.000.007  14-Apr-15   MvdB   ARTF130627: Handle SRSP errors in Open state machine for better error management
 *                                 ARTF130628: Replace Clone Timeout with SRSP timeout
 *                                 ARTF110329: Review handling of serial transmit failures and close when detected.
 * 002.000.008  15-Apr-15   MvdB   ARTF130931: Fix message buffer corruption and Assert when AfDataRequest creates message of size ZAB_VENDOR_MIN_DATA if VLA_INIT != 0
 * 002.000.009  17-Apr-15   MvdB   ARTF131022: Remove SZL_GP_Initialize() and associated functions as it is now obsolete
 * 002.001.004  21-Jul-15   MvdB   ARTF132296: Add zabZnpOpen_ReadyForMessagesIn()
 *                                             Drop messages coming in from serial glue if we are not open/opening
 *                                 ARTF132296: Do not close on ZAB_ERROR_VENDOR_SENDING. Just notify app.
 * 002.002.010  09-Oct-15   MvdB   ARTF150980: Support Service ID in memory allocation functions for WTB
 * 002.002.013  14-Oct-15   MvdB   ARTF151072: Add service pointer to osTimeGetMilliseconds() and osTimeGetSeconds()
 * 002.002.015  21-Oct-15   MvdB   ARTF104106: Support SZL_ZDO_MatchDescriptorReq()
 * 002.002.021  20-Apr-16   MvdB   ARTF167736: Add serial retries to improve robustness against lossy serial link
 *                                             Add ZAB_ACTION_INTERNAL_SERIAL_FLUSH
 * 002.002.024  28-Jun-16   MvdB   ARTF172101: Implement ZNP ping to handle lost Reset indication when hidden inside a serial command that was not completed before reset
 * 002.002.028  13-Oct-16   MvdB   ARTF186964: ZNP Ping should check network action not in process before starting ping
 *                                 ARTF186981: SLIPZ stuck with CLOSED but NETWORKED when ZNP ping fails first send
 *****************************************************************************/


#include "zabZnpService.h"
#include "zabSerialServicePrivate.h"


/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Return true if a serial command has an expected response
 ******************************************************************************/
zab_bool commandHasSerialRsp(ZNPI_TYPE type, ZNPI_SUBSYSTEM ss)
{
  zab_bool hasRsp = zab_false;

  /* All SRSP's expect a response.
   * SBL commands are all AREQs but expect an AREQ in response (Thanks TI), so we will await responses for these too*/
  if ( (type == ZNPI_TYPE_SREQ) ||
       (ss == ZNPI_SUBSYSTEM_SBL) )
    {
      hasRsp = zab_true;
    }
  return hasRsp;
}

/******************************************************************************
 * Give the Vendor Version to the application
 ******************************************************************************/
static void giveBackVendorVersionInfo(erStatus* Status, zabService* Service)
{
  unsigned8 vendorVersion[4] = {ZAB_VENDOR_VERSION_MAJOR, ZAB_VENDOR_VERSION_MINOR, ZAB_VENDOR_VERSION_RELEASE, ZAB_VENDOR_VERSION_BUILD};
  ER_CHECK_STATUS(Status);

  srvCoreGiveBackBuffer(Status, Service, ZAB_GIVE_VENDOR_VERSION, sizeof(vendorVersion), vendorVersion);
  srvCoreGiveBack16(Status, Service, ZAB_GIVE_VENDOR_TYPE, ZAB_VENDOR_TYPE_TEXAS_INSTRUMENTS_ZNP);
}

/******************************************************************************
 * Distribute ZNP In Messages to their various detailed handlers
 ******************************************************************************/
static void zabZnpDistributeMessageIn(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8 cmdId;
  ZNPI_SUBSYSTEM subsystem;

  zabParseZNPGetSubSystem(Status, Message, &subsystem);
  zabParseZNPGetCommandID(Status, Message, &cmdId);

  printVendor(Service, "ZnpMsgIn: SS 0x%X, Cmd 0x%X\n", (unsigned8)subsystem, cmdId);

  switch (subsystem)
    {
      case ZNPI_SUBSYSTEM_RPC:
          zabZnpRpcProcessMsg(Status, Service, Message);
          break;
      case ZNPI_SUBSYSTEM_SYS:
          zabZnpSysProcessMsg(Status, Service, Message);
          break;
      case ZNPI_SUBSYSTEM_SAPI:
          zabZnpSapiProcessMsg(Status, Service, Message);
          break;
      case ZNPI_SUBSYSTEM_AF:
          zabZnpAfUtility_ProcessMsg(Status, Service, Message);
          break;
      case ZNPI_SUBSYSTEM_ZDO:
          zabZnpZdoUtility_ProcessIncomingMessage(Status, Service, Message);
          break;

      case ZNPI_SUBSYSTEM_SBL:
          zabZnpSBLUtility_ProcessMsg(Status, Service, Message);
          break;

#ifdef ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER
      case ZNPI_SUBSYSTEM_GP:
        zabZnpGp_ProcessIncomingMessage(Status, Service, Message);
        break;
#endif

#ifdef ZAB_VND_CFG_ZNP_ENABLE_RFT
      case ZNPI_SUBSYSTEM_RFT:
        zabZnpRftUtility_ProcessMsg(Status, Service, Message);
        break;
#endif // ZAB_VND_CFG_ZNP_ENABLE_RFT

      default:
          printError(Service, "ZnpMsgIn: WARNING: Unknown subsystem 0x%02X\n", (unsigned8)subsystem);
          break;
    }
}

/******************************************************************************
 * Unlock serial queue and free the Tx message
 * If TransactionId not null, return the TID of the transaction
 ******************************************************************************/
static void zabZnpService_UnlockAndFreeTxMessage(erStatus* Status, zabService* Service, unsigned8* TransactionId)
{
  sapMsg* TxMessage;

  ZAB_MUTEX_ENTER(Service, ZAB_SRV( Service )->MutexId);

  if (TransactionId != NULL)
    {
      *TransactionId = ZAB_SERVICE_ZNP(Service)->sreqTimeOut.tid;
    }

  TxMessage = ZAB_SERVICE_ZNP(Service)->sreqTimeOut.message;
  ZAB_SERVICE_ZNP(Service)->sreqTimeOut.message = NULL;

  ZAB_SERVICE_ZNP_SET_SRSP_TIMEOUT_IDLE(Service);
  ZAB_MUTEX_EXIT(Service, ZAB_SRV( Service )->MutexId);

  /* If there was a TxMessage, free it.
   * This is done outside of the mutex to minimise locked time! */
  if (TxMessage != NULL)
    {
      sapMsgFree(Status, TxMessage);
    }
}

/******************************************************************************
 * Process an incoming ZNP message
 * Manage unlock of serial connection
 ******************************************************************************/
static void zabZnpProcessMessageIn(erStatus* Status, zabService* Service, sapMsg* Message)
{
  ZNPI_TYPE type;
  ZNPI_SUBSYSTEM ss;
  unsigned8 cmd;

  /* This message could be an SRSP or an ASYNC request.
   * If it is an SRSP for our command we want to use the TID from sreqTimeOut to match the SRSP to the req.
   * If its anything else (ASYNC), then use TID = 0 */
  unsigned8 tid = 0;

  printVendor(Service,  "zabZnpService: zabZnpProcessMessageIn\n");

  zabParseZNPGetType(Status, Message, &type);
  zabParseZNPGetSubSystem(Status, Message, &ss);
  zabParseZNPGetCommandID(Status, Message, &cmd);
  ER_CHECK_STATUS(Status);

  /* For dev purposes its sometimes use full to print full messages in here,
   * as printing of serial in in messy in some products that feed individual bytes*/
//#define ZNP_PRINT_FULL_MSG_IN
#ifdef ZNP_PRINT_FULL_MSG_IN
  unsigned8 LengthPtr;
  unsigned8* DataPtr;
  zabParseZNPGetData(Status, Message, &LengthPtr, &DataPtr );
  printVendor(Service, "zabZnpProcessMessageIn: Received 0x%02X bytes: %02X %02X %02X", LengthPtr, LengthPtr, ZAB_MSG_ZNP( Message )->Cmd0, ZAB_MSG_ZNP( Message )->Cmd1);
  for (int index = 0; index < LengthPtr; index++)
    {
      printVendor(Service, " %02X", DataPtr[index]);
    }
  printVendor(Service, "\n");
#endif // ZNP_PRINT_FULL_MSG_IN

  switch(type)
    {
      /* SRSP: Mange unlock of serial connection, then pass upwards as it may have useful information*/
      case ZNPI_TYPE_SRSP:

        if (zabZnpService_GetSerialLinkLocked(Service))
          {

            if ( (ZAB_SERVICE_ZNP(Service) ->sreqTimeOut.subsystem == (unsigned8)ss) &&
                 (ZAB_SERVICE_ZNP(Service) ->sreqTimeOut.commandId == cmd) )
              {

#ifdef ZAB_VENDOR_UNLOCK_SERIAL_ON_AF_DATA_CONFIRM
                /* Unlock on SRSP, unless it is a DataRequest, in which case we want to wait for the data confirm */
                if ( (ss == ZNPI_SUBSYSTEM_AF) &&
                     ( (cmd == ZNPI_CMD_AF_DATA_REQUEST) || (cmd == ZNPI_CMD_AF_DATA_REQUEST_EXT) ) )
                  {
                    /* Do nothing, we will wait for the data confirm */
                  }
                else
#endif
                  {
                    printVendor(Service,  "zabZnpService: UNLOCK ZNP : OK\n");
                    zabZnpService_UnlockAndFreeTxMessage(Status, Service, &tid);
                  }
              }
            else if ( (ss == ZNPI_SUBSYSTEM_RPC) && (cmd == ZNPI_CMD_RPC_COMMAND_ERROR) )
              {

                erStatusSet(Status, ZAB_ERROR_VENDOR_REQ_INVALID);

                zabZnpService_UnlockAndFreeTxMessage(Status, Service, &tid);
                sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_REQ_INVALID);

                /* Notify state machines there has been an error*/
                zabZnpOpen_srspTimeoutHandler(Status, Service);
                zabZnpNwk_srspTimeoutHandler(Status, Service);
                zabZnpClone_srspTimeoutHandler(Status, Service);
              }
            else
              {
                printError(Service,  "zabZnpService: OTHER RESPONSE ZNP: Warning\n");
              }
          }
        else
          {
            printError(Service, "zabZnpService: WARNING: SRSP received when not locked\n");
          }
        sapMsgSetAppTransactionId(Status, Message, tid);
        zabZnpDistributeMessageIn(Status, Service, Message);
        break;

      /* Pass a-synchronous data upwards */
      case ZNPI_TYPE_AREQ:
        /* SBL makes as it uses all AREQs and expects an AREQ in response, so we need to handle this*/
        if ( (commandHasSerialRsp(type, ss) == zab_true) &&
             (zabZnpService_GetSerialLinkLocked(Service)) &&
             (ZAB_SERVICE_ZNP(Service) ->sreqTimeOut.subsystem == (unsigned8)ss) &&
             (ZAB_SERVICE_ZNP(Service) ->sreqTimeOut.commandId == (cmd&0x7F)) // SBL response has 0x80 bit set in cmd
            )
          {
            zabZnpService_UnlockAndFreeTxMessage(Status, Service, NULL);
          }

        sapMsgSetAppTransactionId(Status, Message, tid);
        zabZnpDistributeMessageIn(Status, Service, Message);
        break;

      /* This should not happen! */
      default:
        printError(Service, "zabZnpService: ERROR: Invalid message type = 0x%02X\n", type);
        erStatusSet(Status, ZAB_ERROR_VENDOR_TYPE_INVALID);
        return;
    }
}


/******************************************************************************
 * Vendor In Work
 * Checks the VendorInQ from the serial SAP.
 * Process messages and notifications and forwards them to application and state machines
 ******************************************************************************/
static unsigned32 LocalWorkIn(erStatus* Status, zabService* Service)
{
  sapMsg* Msg;
  sapMsgType Type;
  zabMsgAppType App;
  unsigned8 Count = qSafeCount(&ZAB_SRV(Service)->VendorInQ);

  while ( (Count > 0) && (erStatusIsOk(Status)) )
    {
      Msg = (sapMsg*) qSafeRemove(&ZAB_SRV(Service)->VendorInQ);
      if (Msg == NULL)
        {
          erStatusSet( Status, SAP_ERROR_MSG_NOT_AVAILABLE );
          return 0;
        }

      /* Only process messages if we are ready to accept them. Otherwise just drop them.
       * This should never happen, but has occurred on products like Nova that do not close the serial glue properly */
      if ( zabZnpOpen_ReadyForMessagesIn(Service) == zab_true)
        {
          Type = sapMsgGetType(Msg);
          if (Type == SAP_MSG_TYPE_APP)
            {
              App = sapMsgGetAppType(Msg);
              if (App == ZAB_MSG_APP_ZNP)
                {
                  // Process ZNP message, manages unlock on SRSP
                  zabZnpProcessMessageIn(Status, Service, Msg);
                }
              else
                {
                  printError(Service, "zabZnpSerive: WARNING: Unknown IN application type = 0x%X\n", (unsigned8)App);
                  erStatusSet(Status, ZAB_ERROR_MSG_APP_INVALID);
                }
            }
          else if (Type == SAP_MSG_TYPE_NOTIFY)
            {
              if (sapMsgGetNotifyType(Msg) == ZAB_NOTIFICATION_OPEN_STATE)
                {
                  zabZnpOpen_InNotificationHandler(Status, Service, sapMsgGetNotifyData(Msg).openState);
                }
              else
                {
                  printError(Service, "zabZnpSerive: WARNING: Unknown IN notification type = 0x%X\n", (unsigned8)sapMsgGetNotifyType(Msg));
                }
            }
          else
            {
              printError(Service, "zabZnpSerive: WARNING: Unknown IN message type = 0x%X\n", (unsigned8)Type);
              erStatusSet(Status, ZAB_ERROR_MSG_TYPE_INVALID);
            }
        }

      sapMsgFree(Status, Msg);
      Count--;
    }

  /* If we successfully cleared all messages then return max delay.
   * If not, request more work ASAP */
  if (Count == 0)
    {
      return ZAB_WORK_DELAY_MAX_MS;
    }
  return 0;
}


/******************************************************************************
 * Vendor Out Work
 * Process messages on the VendorOutQ and send commands and action to the
 * network processor and serial glue.
 ******************************************************************************/
static unsigned32 LocalWorkOut(erStatus* Status, zabService* Service)
{
  unsigned32 timeoutRemaining = ZAB_WORK_DELAY_MAX_MS;
  sapMsg *Msg;
  sapMsgType Type;
  zabAction Action;

  unsigned8 Count = qSafeCount(&ZAB_SRV(Service)->VendorOutQ);

  /* Process outwards messages until queue is empty or ZNP is busy */
  while ( (Count > 0) &&
          erStatusIsOk(Status) &&
          ZAB_SERVICE_ZNP_GET_SRSP_TIMEOUT_IDLE(Service))
    {
      printVendor(Service, "zabZnpService: LocalWorkOut - sending message\n");

      // These actions or commands will have done something, so get the next work done quickly to correctly calculate real next timeout
      timeoutRemaining = 0;

      Msg = (sapMsg*) qSafeRemove(&ZAB_SRV(Service)->VendorOutQ);
      if (Msg == NULL )
        {
          printError(Service, "zabZnpService: ERROR: Null message\n");
          break;
        }

      Type = sapMsgGetType(Msg);
      if (Type == SAP_MSG_TYPE_ACTION)
        {
          Action = sapMsgGetAction(Msg);

          /* Forward all actions to the state machines. They will decide if they want to use them or ignore them */
          zabZnpOpen_Action(Status, Service, Action);
          zabZnpNwk_action(Status, Service, Action);
          zabZnpSblUtility_Action(Status, Service, Action);
          zabZnpClone_action(Status, Service, Action);
        }
      else if (Type == SAP_MSG_TYPE_APP)
        {
          /* Application messages need to be converted from the ZAB internal format into the vendor format */
          switch (sapMsgGetAppType(Msg))
            {
              case ZAB_MSG_APP_AF_DATA_OUT:
                zabZnpAfUtility_DataRequestExt(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_IEEE_REQ:
                zabZnpZdoUtility_IeeeMsgConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_NWK_REQ:
                zabZnpZdoUtility_NwkMsgConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_NODE_DESC_REQ:
                zabZnpZdoUtility_NodeDescReqConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_POWER_DESC_REQ:
                zabZnpZdoUtility_PowerDescReqConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_ACTIVE_EP_REQ:
                zabZnpZdoUtility_ActiveEndpointReqConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_SIMPLE_DESC_REQ:
                zabZnpZdoUtility_SimpleDescriptorReqConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_MATCH_DESC_REQ:
                zabZnpZdoUtility_MatchDescriptorReqConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_USER_DESC_REQ:
                zabZnpZdoUtility_UserDescriptorReqConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_USER_DESC_SET_REQ:
                zabZnpZdoUtility_UserDescriptorSetReqConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_MGMT_LQI_REQ:
                zabZnpZdoUtility_MgmtLqiReqConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_MGMT_RTG_REQ:
                zabZnpZdoUtility_MgmtRtgReqConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_MGMT_BIND_REQ:
                zabZnpZdoUtility_MgmtBindReqConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_MGMT_LEAVE_REQ:
                zabZnpZdoUtility_MgmtLeaveReqConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_MGMT_NWK_UPDATE_REQ:
                zabZnpZdoUtility_MgmtNwkUpdateReqConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_ZDO_BIND_REQ:
                zabZnpZdoUtility_BindReqConversion(Status, Service, Msg);
                break;
              case ZAB_MSG_APP_ZDO_UNBIND_REQ:
                zabZnpZdoUtility_UnBindReqConversion(Status, Service, Msg);
                break;

              case ZAB_MSG_APP_REGISTER_REQ:
                zabZnpAfUtility_RegisterEndpoint(Status, Service, Msg);
                break;

#ifdef ZAB_VND_CFG_ZNP_ENABLE_GREEN_POWER
              case ZAB_MSG_APP_GP_COM_REPLY_REQ:
                zabZnpGp_CommissioningReplyReqConversion(Status, Service, Msg);
                break;
#endif
              default:
                printError(Service, "zabZnpService: ERROR - Unknown app message type 0x%02X\n", sapMsgGetAppType(Msg));
                break;
            }

          /* If the conversion failed, report it to the app */
          if (erStatusIsError(Status))
            {
              zabParseZNP_AppErrorIn(Service, sapMsgGetAppTransactionId(Msg), ZAB_ERROR_VENDOR_SENDING);
            }
        }
      else
        {
          printError(Service, "zabZnpService: ERROR - Unknown message type 0x%02X\n", (unsigned8)Type);
        }

      /* Free the message and decrement count before moving onto next item*/
      sapMsgFree(Status, Msg);
      Count--;
    }
  return timeoutRemaining;
}

/******************************************************************************
 * This function receives serial port data.
 * It is registered as the IN handler with the serial SAP.
 ******************************************************************************/
static void SerialToVendorIn(erStatus* Status, sapHandle Sap, sapMsg* Message)
{
  erStatus localStatus;
  zabServiceStruct* This = ZAB_SRV( sapCoreGetService( Sap ) );

  erStatusClear(&localStatus, This);

  ER_CHECK_NULL(Status, Message);

  sapMsgUse(&localStatus, Message); // must do this because we are storing it on a queue

  if (erStatusIsError(&localStatus))
    {
      erStatusSet(Status, erStatusGetError(&localStatus));
    }
  else
    {
      qSafeAppend(&This->VendorInQ, Message);           // just put it on the queue
    }
}



/******************************************************************************
 * Check for TimeOut of Request ZNP
 ******************************************************************************/
static unsigned32 zabZnpUpdateTimeOut(erStatus* Status, zabService* Service, unsigned32 Time)
{
  zab_bool Timeout = zab_false;
  zab_bool retry = zab_false;
  unsigned32 timeoutRemaining;
  unsigned8 retryCount;
  sapMsg* TxMessage;
  erStatus LocalStatus;
  erStatusClear(&LocalStatus, Service);

  if (ZAB_SERVICE_ZNP_GET_SRSP_TIMEOUT_IDLE(Service))
    {
      return ZAB_WORK_DELAY_MAX_MS;
    }

  /* Keep mutex for timeout access as quick as possible */
  ZAB_MUTEX_ENTER(Service, ZAB_SRV( Service )->MutexId);
  timeoutRemaining = ZAB_SERVICE_ZNP(Service)->sreqTimeOut.timeoutTimeMs - Time;
  retryCount = ZAB_SERVICE_ZNP(Service)->sreqTimeOut.retryCount;
  TxMessage = ZAB_SERVICE_ZNP(Service)->sreqTimeOut.message;
  ZAB_MUTEX_EXIT(Service, ZAB_SRV( Service )->MutexId);

  /* Check timeout while handling wrapped time*/
  if ( (timeoutRemaining == 0) || (timeoutRemaining > ZAB_ZNP_MS_TIMEOUT_MAX) )
    {
      if (retryCount < ZAB_VENDOR_SERIAL_RETRIES)
        {
          retry = zab_true;
        }
      else
        {
          Timeout = zab_true;
        }

      // Stuff has happened so request fast work to move on to next item
      timeoutRemaining = 0;
    }

  if (retry == zab_true)
    {
      printError(Service, "zabZnpService: WARNING: Serial Retry %d!!!!!!!\n", retryCount+1);

      /* Flush the serial in data before retrying to increase chances of successful recovery.
       * Use a local status here to contain any failures until all retries are used up */
      sapMsgPutAction(&LocalStatus, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_INTERNAL_SERIAL_FLUSH);
      sapMsgPut(&LocalStatus, zabCoreSapSerial(Service), TxMessage);

      /* If there was an error in the glue, then we close ZAB */
      if(erStatusIsError(&LocalStatus))
        {
          printError(Service, "zabZnpService: WARNING: Serial Tx Failed - backing off!!!!!!!\n");
          // Set a short backoff timeout, which hopefully allows glue to recover from whatever the failure was
          Time += ZAB_VENDOR_SERIAL_BACKOFF_MS;
        }
      else
        {
          Time += ZAB_VENDOR_SRSP_TIMEOUT;
        }
      ZAB_MUTEX_ENTER(Service, ZAB_SRV( Service )->MutexId);
      ZAB_SERVICE_ZNP_UPDATE_SRSP_TIMEOUT(Service, Time, retryCount+1);
      ZAB_MUTEX_EXIT(Service, ZAB_SRV( Service )->MutexId);
    }
  else if (Timeout == zab_true)
    {
      /* Flush the serial in data upon failure to try to leave things more tidy for the next command */
      sapMsgPutAction(Status, zabCoreSapSerial(Service), SAP_MSG_DIRECTION_OUT, ZAB_ACTION_INTERNAL_SERIAL_FLUSH);

      zabZnpService_UnlockAndFreeTxMessage(Status, Service, NULL);
      // push error(tid)

      erStatusSet(Status, ZAB_ERROR_VENDOR_TIMEOUT);
      printError(Service,  "ZNP Service: Time Out : UNLOCK ZNP\n");
      sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_TIMEOUT);

      /* Notify state machines there has been an error*/
      zabZnpOpen_srspTimeoutHandler(Status, Service);
      zabZnpNwk_srspTimeoutHandler(Status, Service);
      zabZnpClone_srspTimeoutHandler(Status, Service);
    }

  return timeoutRemaining;
}


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                 *****         VENDOR PUBLIC        *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Create an instance of ZAB Vendor
 ******************************************************************************/
void zabVendorService_Create(erStatus* Status, zabService* Service)
{
  zabServiceStruct* This = ZAB_SRV( Service );
  unsigned8 MutexId;

  This->Vendor = OS_MEM_MALLOC(Status,
                               srvCoreGetServiceId(Status, Service),
                               sizeof(zabVendorServiceStruct),
                               MALLOC_ID_CFG_VENDOR);
  if (This->Vendor == NULL)
    {
      return;
    }

  MutexId = ZAB_PROTECT_CREATE(Service);
  This->SerialSAP = sapCoreCreate(Status,
                                  Service,
                                  srvCoreAskBack8(Status, Service, ZAB_ASK_SAP_SERIAL_MAX, 0) + 2 /* in and out handlers */,
                                  MutexId);
  sapCoreRegisterCallBack(Status, zabCoreSapSerial(This), SAP_MSG_DIRECTION_IN, SerialToVendorIn);
  sapCoreRegisterCallBack(Status, zabCoreSapSerial(This), SAP_MSG_DIRECTION_OUT, zabSerialService_OutHandler);
  sapMsgRegAllocate(Status, zabCoreSapSerial(This), zabCoreAllocateApp, zabCoreFreeApp);

  zabVendorService_Reset(Status, Service);

  /* Give info to the app */
  giveBackVendorVersionInfo(Status, Service);
}

/******************************************************************************
 * Reset an instance of ZAB Vendor
 ******************************************************************************/
void zabVendorService_Reset(erStatus* Status, zabService* Service)
{
  printVendor(Service, "Reset ZNP Service\n");

  zabCoreFreeQueue(Status, &ZAB_SRV( Service )->VendorInQ);

  zabZnpService_UnlockAndFreeTxMessage(Status, Service, NULL);
  osTimeGetMilliseconds(Service, &ZAB_SERVICE_VENDOR(Service)->lastOneSecondTickMs);
  ZAB_SERVICE_VENDOR(Service)->networkProcessorHardwareType = znpHardwareType_Not_Init;

  /* Initialise state machines */
  zabZnpOpen_Create(Status, Service);
  zabZnpNwk_create(Status, Service);
  zabZnpSblUtility_Create(Status, Service);
  zabZnpClone_create(Status, Service);

  ZAB_SERVICE_ZNP( Service )->SerialReceiveBuffer.Length = 0;
}

/******************************************************************************
 * Destroy an instance of ZAB Vendor
 ******************************************************************************/
void zabVendorService_Destroy(erStatus* Status, zabService* Service)
{
  if (Service != NULL)
    {
      zabZnpService_UnlockAndFreeTxMessage(Status, Service, NULL);
      zabCoreFreeQueue(Status, &ZAB_SRV( Service )->VendorInQ);
      ZAB_PROTECT_RELEASE(Service, qSafeId(&ZAB_SRV( Service )->VendorInQ));

      ZAB_PROTECT_RELEASE(Service, sapCoreGetMutexId(ZAB_SRV(Service)->SerialSAP));
      OS_MEM_FREE(Status, srvCoreGetServiceId(Status, Service), ZAB_SRV(Service)->SerialSAP);
      OS_MEM_FREE(Status, srvCoreGetServiceId(Status, Service), ZAB_SRV(Service)->Vendor);
    }
}

/******************************************************************************
 * Vendor Work
 *   Process VendorOutQ and send commands to network processor
 *   Process VendorInQ and indicate messages up to the core
 *   Check for SRSP timeouts
 *   Update state machines
 ******************************************************************************/
unsigned32 zabVendorService_Work(erStatus* Status, zabService* Service)
{
  unsigned32 time;
  unsigned32 nextWorkRequiredDelayMs = ZAB_WORK_DELAY_MAX_MS;
  unsigned32 nextWorkRequiredDelayMsTemp;

  /* Local work in first, which may generate commands out from the state machines.
   * This gives them first go at the serial link before it is locked */
  nextWorkRequiredDelayMsTemp = LocalWorkIn(Status, Service);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  /* Get time now and check all timeouts. Much more efficient than getting time several times over */
  osTimeGetMilliseconds(Service, &time);

  nextWorkRequiredDelayMsTemp = zabZnpUpdateTimeOut(Status, Service, time);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  nextWorkRequiredDelayMsTemp = zabZnpOpen_UpdateTimeout(Status, Service, time);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  nextWorkRequiredDelayMsTemp = zabZnpNwk_updateTimeout(Status, Service, time);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  nextWorkRequiredDelayMsTemp = zabZnpSblUtility_UpdateTimeout(Status, Service, time);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  /* One second tick. */
  if ( (time+1000) > ZAB_SERVICE_VENDOR(Service)->lastOneSecondTickMs)
    {
      if (time >= (ZAB_SERVICE_VENDOR(Service)->lastOneSecondTickMs +1000))
        {
          ZAB_SERVICE_VENDOR(Service)->lastOneSecondTickMs+=1000;
          nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, 1000);

          zabZnpNwk_oneSecondTickHandler(Status, Service);
        }
      else
        {
          nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, (ZAB_SERVICE_VENDOR(Service)->lastOneSecondTickMs +1000) - time);
        }
    }
  /* Time is within a second of wrapping, just update the last tick time to timeNow until timeNow gets to zero */
  else
    {
      ZAB_SERVICE_VENDOR(Service)->lastOneSecondTickMs = time;
      nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, 1000);
    }

  /* Network processor ping
   * Lower priority than work in or timeouts to not disrupt state machines.
   * Higher priority than work out to ensure it still gets done if link is broken and retries are monopolising the link */
  nextWorkRequiredDelayMsTemp = zabZnpOpen_UpdateNetworkProcessorPing(Status, Service, time);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  /* Do work out at the end, so all the state machines got a chance to grab the serial link before anything new is started */
  nextWorkRequiredDelayMsTemp = LocalWorkOut(Status, Service);
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, nextWorkRequiredDelayMsTemp);

  // Tidy up range on delay
  nextWorkRequiredDelayMs = MIN(nextWorkRequiredDelayMs, ZAB_WORK_DELAY_MAX_MS);
  return nextWorkRequiredDelayMs;
}

/******************************************************************************
 * This function receives DATA and MANAGE SAP messages from the core.
 * It queues them for processing by Vendor Work
 ******************************************************************************/
void zabVendorService_CoreToVendorOut(erStatus* Status, sapHandle Sap, sapMsg* Message)
{
  zabServiceStruct* This = ZAB_SRV( sapCoreGetService( Sap ) );
  sapMsgType Type = sapMsgGetType(Message);

  ER_CHECK_STATUS(Status);

  printVendor(This, "zabZnpService: zabVendorService_CoreToVendorOut is awake\n");

  if ((Type == SAP_MSG_TYPE_ACTION) || (Type == SAP_MSG_TYPE_APP))
    {
      sapMsgUse(Status, Message); // must do this because we are storing it on a queue

      ER_CHECK_STATUS(Status);

      qSafeAppend(&This->VendorOutQ, Message); // store app messages to send to network
    }
  else
    {
      erStatusSet(Status, ZAB_ERROR_MSG_TYPE_INVALID);
    }
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                 *****         VENDOR ONLY          *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 * Vendor is ready for local comms from the App
 ******************************************************************************/
void zabZnpService_GetLocalCommsReady(erStatus* Status, zabService* Service)
{
  ER_CHECK_STATUS(Status);
  if (zabZnpOpen_GetOpenState(Service) != ZAB_OPEN_STATE_OPENED)
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
    }
}

/******************************************************************************
 * Vendor is ready for network comms from the App
 ******************************************************************************/
void zabZnpService_GetNetworkCommsReady(erStatus* Status, zabService* Service)
{
  ER_CHECK_STATUS(Status);
  if (zabZnpOpen_GetOpenState(Service) != ZAB_OPEN_STATE_OPENED)
    {
      erStatusSet(Status, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
    }
  else if (zabZnpNwk_GetNwkState(Service) != ZAB_NWK_STATE_NETWORKED)
    {
      erStatusSet(Status, ZAB_ERROR_INVALID_NETWORK_STATE);
    }
}


/******************************************************************************
 * Get status of serial link
 ******************************************************************************/
zab_bool zabZnpService_GetSerialLinkLocked(zabService* Service)
{
  zab_bool serialActive = zab_false;

  ZAB_MUTEX_ENTER(Service, ZAB_SRV( Service )->MutexId);
  if (ZAB_SERVICE_ZNP_GET_SRSP_TIMEOUT_ACTIVE(Service))
    {
      serialActive = zab_true;
    }
  ZAB_MUTEX_EXIT(Service, ZAB_SRV( Service )->MutexId);

  return serialActive;
}

/******************************************************************************
 * Send a ZNP message.
 * Message will be discarded if ZNP link is locked, so caller should verify first.
 ******************************************************************************/
void zabZnpService_SendMsg(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8 znpDataLength;
  ZNPI_TYPE type;
  ZNPI_SUBSYSTEM ss;
  unsigned32 timeNow;
  unsigned8 cmd;
  unsigned8 tid;
  unsigned16 timeout;
  unsigned8 attemptNumber = 0;

  ER_CHECK_NULL(Status, Message);
  if (erStatusIsError(Status))
    {
      sapMsgFree(Status, Message);
      return;
    }

  /* Set App Data length to ZNP data length + ZNPI_MSG_HEADER_LENGTH */
  zabParseZNPGetLength(Status, Message, &znpDataLength);
  sapMsgSetAppDataLength(Status, Message, znpDataLength + ZNPI_MSG_HEADER_LENGTH);
  tid = sapMsgGetAppTransactionId(Message);

  /* Get message type data. It may not be used, but its better to do it outside the mutex */
  zabParseZNPGetType(Status, Message, &type);
  zabParseZNPGetSubSystem(Status, Message, &ss);
  zabParseZNPGetCommandID(Status, Message, &cmd);

#ifdef ZAB_VENDOR_UNLOCK_SERIAL_ON_AF_DATA_CONFIRM
  /* Unlock on SRSP (With short timeout), unless it is a DataRequest, in which case we want to wait for the data confirm (long timeout) */
  if ( (ss == ZNPI_SUBSYSTEM_AF) &&
       ( (cmd == ZNPI_CMD_AF_DATA_REQUEST) || (cmd == ZNPI_CMD_AF_DATA_REQUEST_EXT) ) )
    {
      timeout = ZAB_VENDOR_DATA_CONFIRM_TIMEOUT;
    }
  else
#endif
    {
      timeout = ZAB_VENDOR_SRSP_TIMEOUT;
    }

  /* Perform a status check here, as we have seen cases like ARTF130931 where length errors cause a crash */
  if (erStatusIsError(Status))
    {
      printError(Service,  "zabZnpService: zabZnpService_SendMsg - Dropping message for error 0x%04X\n", Status->Error);
      sapMsgFree(Status, Message);
      return;
    }

  /* Convert from ZNP into RAW serial data: Insert SOF and FCS if enabled*/
  sapMsgSetAppType (Status, Message, ZAB_MSG_APP_RAW);
#ifdef ZAB_VND_CFG_ZNP_M_SOF_AND_FCS_ENABLED
  ZAB_MSG_ZNP_SOF(Message) = ZAB_VND_CFG_ZNP_M_START_OF_FRAME;
  ZAB_MSG_ZNP_FCS(Message) = zabUtility_ComputeFCS((unsigned8*)ZAB_MSG_ZNP(Message), znpDataLength + ZNPI_MSG_HEADER_LENGTH_WITHOUT_SOF_FCS);
#endif


  printVendor(Service, "zabZnpService_SendMsg: TID = 0x%02X, FCS = 0x%02X\n", tid, ZAB_MSG_ZNP_FCS(Message));

  /* Check if ZNP is already locked. If so discard this command. If not lock it (if command requires a response) and send */
  if (zabZnpService_GetSerialLinkLocked(Service) == zab_true)
    {
      printError(Service, "zabZnpService: SendMsg failed as serial link locked SS 0x%X, Cmd 0x%X\n", ZAB_SERVICE_ZNP(Service)->sreqTimeOut.subsystem, ZAB_SERVICE_ZNP(Service)->sreqTimeOut.commandId);
      sapMsgFree(Status, Message);
      erStatusSet(Status, ZAB_ERROR_VENDOR_BUSY);
      return;
    }


  /* Now send the message if the status is still good, otherwise discard it */
  if (erStatusIsOk(Status))
    {
      /* Use a local status for sending messages so we can handle failures/retries gracefully*/
      erStatus localStatus;
      erStatusClear(&localStatus, Service);

      sapMsgPut(&localStatus, zabCoreSapSerial(Service), Message);

      /* If command expects a serial response (ack) and it was sent successfully, set normal timeout.
       * If it was not sent successfully:
       *  - And we have retries: set a short timeout for a retry which hopefully allows glue to recover from whatever the failure was.
       *  - No retries: Notify error and free message */
      if (commandHasSerialRsp(type, ss) == zab_true)
        {
          osTimeGetMilliseconds(Service, &timeNow);
          if (erStatusIsOk(&localStatus))
            {
              ZAB_SERVICE_ZNP_SET_SRSP_TIMEOUT_ACTIVE(Service, Message, (unsigned8)ss, cmd, tid, timeNow + timeout, attemptNumber);
            }
#if (ZAB_VENDOR_SERIAL_RETRIES > 0)
          else
            {
              printError(Service, "zabZnpService: WARNING: Serial Tx Failed - backing off!!!!!!!\n");
              ZAB_SERVICE_ZNP_SET_SRSP_TIMEOUT_ACTIVE(Service, Message, (unsigned8)ss, cmd, tid, timeNow + ZAB_VENDOR_SERIAL_BACKOFF_MS, attemptNumber);
            }
#else
          else
            {

              /* Close on error is disabled under ARTF132296.
               * We now just notify the app and let it handle it */
              //zabZnpOpen_CloseDueToError(Status, Service);
              sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, (zabError)localStatus.Error);


              sapMsgFree(Status, Message);
            }
#endif
        }
      else
        {
          sapMsgFree(Status, Message);
        }
    }
  else
    {
      printError(Service, "zabZnpService: SendMsg dropped due to error 0x%04X\n", Status->Error);
      sapMsgFree(Status, Message);
    }
}

/******************************************************************************
 * Send a raw message to the network processor.
 * Message will be discarded if ZNP link is locked, so caller should verify first.
 ******************************************************************************/
#ifdef M_ENABLE_RAW
void zabZnpService_SendMsgRaw(erStatus* Status, zabService* Service, sapMsg* Message)
{
  ER_CHECK_NULL(Status, Message);
  if (erStatusIsError(Status))
    {
      sapMsgFree(Status, Message);
    }

  /* Check if ZNP is locked. If so discard this command. If not, send */
  ZAB_MUTEX_ENTER(Service, ZAB_SRV( Service )->MutexId);
  if (zabZnpService_GetSerialLinkLocked(Service))
    {
      ZAB_MUTEX_EXIT(ZAB_SRV( Service )->MutexId);
      printError(Service, "zabZnpService: SendMsgRaw failed\n");
      sapMsgFree(Status, Message);
      erStatusSet(Status, ZAB_ERROR_VENDOR_BUSY);
      return;
    }
  ZAB_MUTEX_EXIT(Service, ZAB_SRV( Service )->MutexId);

  sapMsgPutFree(Status, zabCoreSapSerial(Service), Message);
}
#endif




#ifdef ZAB_VND_CFG_ENABLE_WIRELESS_TEST_BENCH
/******************************************************************************
 * WIRELESS TEST BENCH: Register for Data Request Notification from vendor
 ******************************************************************************/
void zabZnpService_RegisterDataRequestNotificationHandler(erStatus* Status, zabService* Service, wtbUtility_DataReqNotification_t Callback)
{
  ZAB_SERVICE_ZNP(Service)->DataRequestNotificationCallback = Callback;
}

/******************************************************************************
 * WIRELESS TEST BENCH: Register for Data Confirm Notifications from vendor
 ******************************************************************************/
void zabZnpService_RegisterDataConfirmNotificationHandler(erStatus* Status, zabService* Service, wtbUtility_ZnpDataCnfNotification_t Callback)
{
  ZAB_SERVICE_ZNP(Service)->DataConfirmNotificationCallback = Callback;
}

/******************************************************************************
 * WIRELESS TEST BENCH: Register for Data Indication Notification from vendor
 ******************************************************************************/
void zabZnpService_RegisterDataIndicationNotificationHandler(erStatus* Status, zabService* Service, wtbUtility_DataIndNotification_t Callback)
{
  ZAB_SERVICE_ZNP(Service)->DataIndicationNotificationCallback = Callback;
}
#endif

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
