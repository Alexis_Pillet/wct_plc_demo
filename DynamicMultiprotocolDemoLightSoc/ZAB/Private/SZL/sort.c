/*******************************************************************************
  Filename    : sort.c
  $Date       : 2013-05-01                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : Sort functions for the SZL

 * MODIFICATION HISTORY:
 *  Core Rev      Date     Author  Change Description
 * 002.002.009  07-Oct-15   MvdB   ARTF150763: Optimise SZL sort for Nova by replacing bubble sort with comb sort
*******************************************************************************/
/*
  _________              __
 /   _____/ ____________/  |_
 \_____  \ /  _ \_  __ \   __\  ______
 /        (  <_> )  | \/|  |   /_____/
/_______  /\____/|__|   |__|
        \/
___________                   __  .__
\_   _____/_ __  ____   _____/  |_|__| ____   ____   ______
 |    __)|  |  \/    \_/ ___\   __\  |/  _ \ /    \ /  ___/
 |     \ |  |  /   |  \  \___|  | |  (  <_> )   |  \\___ \
 \___  / |____/|___|  /\___  >__| |__|\____/|___|  /____  >
     \/             \/     \/                    \/     \/
*/
#include <string.h>
#include <assert.h>
#include "szl.h"
#include "sort.h"


/* Define to revert to origninal bubble sort.
 * Recommended to leave undefined to use newer, more efficient comb sort */
//#define USE_OLD_BUBBLE_SORT


#define _BUBBLE_SORT_ELEMENT_MAX_SIZE_ 64

static void swap(void* a,void* b, int size);

/**
 * swap
 *
 * This function swaps the content of two elements for a given size
 */
static void swap(void* a,void* b, int size)
{
    szl_uint8 tmp[_BUBBLE_SORT_ELEMENT_MAX_SIZE_];

    szl_memcpy(tmp, a, size);
    szl_memcpy(a, b, size);
    szl_memcpy(b, tmp, size);
}

/**
 * Bubble Sort
 *
 * This is a simpel bubble sort to sort a array of any type
 */
void bubble_sort(void* listElem, const int numElem, const int sizeElem, bubble_sort_compare_fn_t cmp_func)
{

#ifdef USE_OLD_BUBBLE_SORT
#warning SZL/sort.c: You are using bubble sort, which is slow. Comb sort is now recommended. Undefine USE_OLD_BUBBLE_SORT
    SZL_ASSERT( sizeElem <= _BUBBLE_SORT_ELEMENT_MAX_SIZE_ );

    int i,j;
    for(i=0; i<(numElem-1); i++)
    {
        for(j=0; j<(numElem-(i+1)); j++)
        {
            szl_uint8* a = (szl_uint8*)listElem + (j*sizeElem);
            szl_uint8* b = a + sizeElem;
            if (cmp_func(a, b) > 0)
            {
                swap(a,b, sizeElem);
            }
        }
    }
#else
// ** Bubble sort algorithm replaced by the faster Comb sort 11 algorithm **
// The Comb Sort is a variant of the Bubble Sort. It increases the gap used in
// comparisons and exchanges. The gap in divided by 1.25 (can be 1.25 to 1.33)
// at each iteration, starting from numElements / 1.25.
// Comb sort 11 variant ensures the gap ends in (11, 8, 6, 4, 3, 2, 1), which is
// significantly faster than the other two possible endings (10, 7, 5, 3, 2, 1)
// or  (9, 6, 4, 3, 2, 1).
  int i = 0;
  int j = 0;
  unsigned int gap = numElem;
  szl_bool swapped = szl_false;
  szl_uint8 *a = (szl_uint8 *)0;
  szl_uint8 *b = (szl_uint8 *)0;

  SZL_ASSERT( sizeElem <= _BUBBLE_SORT_ELEMENT_MAX_SIZE_ );

  while ( ( gap > 1 ) || ( swapped != szl_false ) )
  {
    if ( gap > 1 )
    {
      gap = ( gap * 51 ) >> 6; // division by ~1.25 done in integer math
      if ( ( gap == 9 ) || ( gap == 10) )
      {
        gap = 11;
      }
    }
    swapped = szl_false;
    i = 0;
    j = gap;
    while ( j < numElem )
    {
      a = (szl_uint8 *)listElem + ( i * sizeElem );
      b = (szl_uint8 *)listElem + ( j * sizeElem );
      if ( cmp_func( a, b ) > 0 )
      {
        swap( a, b, sizeElem );
        swapped = szl_true;
      }
      i++;
      j++;
    }
  }
#endif

}
