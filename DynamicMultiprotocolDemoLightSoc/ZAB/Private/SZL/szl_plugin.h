#ifndef _SZL_PLUGIN_TASK_H_
#define _SZL_PLUGIN_TASK_H_

#include "szl.h"

typedef enum
{
    // DO NOT CHANGE THE ORDER AS THIS STATES THE PRIORITY

    //SZL_PT_HANDLER_BRICK_INIT, // MUST BE THE FIRST ONE!
    // Library internal
    ///SZL_PT_HANDLER_DATAPOINT_NV,
    //SZL_PT_HANDLER_REPORT,
    //SZL_PT_HANDLER_PING,

    // App handler - This should have the lowest priority holding back the app until library is functional
    //SZL_PT_HANDLER_APP_CALLBACK,    // enabling the APP to do some stuff

    // DO NOT CHANGE BELOW HERE
    _SZL_PT_HANDLER_COUNT_
} SZL_PT_HANDLERS_t;

typedef enum
{
    PT_SYSTEM_TASK_NONE,        /**< Nothing to do for the plugin task handlers */
    PT_SYSTEM_TASK_INIT,        /**< The plugin should load it's stored values from NV */
    PT_SYSTEM_TASK_RESET,       /**< The plugin should delete any NV stored values and use the default values on next bootup NOTE this is always followed ba a @ref PLUGIN_SYSTEM_TASK_SHUTDOWN */
    PT_SYSTEM_TASK_SHUTDOWN,    /**< The plugin should save all unsaved data and prepare to be shutdown */
    PT_SYSTEM_TASK_PROCESS,     /**< Normal processing - should not block*/
} SZL_PT_SYSTEM_TASKS_t;

typedef szl_bool (*SZLPT_CB_TaskHandler_t) (zabService* Service, SZL_PT_SYSTEM_TASKS_t task);

typedef struct
{
    szl_uint16 ID;
    SZLPT_CB_TaskHandler_t taskHandler;
} PLUGIN_TASK_t;

void SzlPT_Initialize(zabService* Service);
szl_bool SzlPT_TaskHandler(zabService* Service, SZL_PT_SYSTEM_TASKS_t task);
void SzlPT_Register(zabService* Service, szl_uint16 ID, SZLPT_CB_TaskHandler_t taskHandler);
SZL_RESULT_t SzlPT_Unregister(zabService* Service, szl_uint16 ID);






#endif /*_SZL_PLUGIN_TASK_H_*/
