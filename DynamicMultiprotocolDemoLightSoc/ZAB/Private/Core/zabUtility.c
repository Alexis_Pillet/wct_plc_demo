/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains general utility functions for ZAB
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 02.00.00.04  22-Feb-14   MvdB   Cleanup
 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 00.00.06.02  30-Sep-14   MvdB   artf55512: Support network key change action
 * 00.00.06_GP  15-Oct-14   MvdB   ARTF105854: Support Smartlink IPZ hardware for Pro+GP ZNP
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 * 002.000.004  01-Apr-15   MvdB   ARTF56504: Support missed channel change recovery, with actions/notifications/asks/gives to get/set parameters.
 * 002.001.001  29-Apr-15   MvdB   ARTF132260: Support firmware upgrade of CC2538 network processors
 * 002.002.005  11-Sep-15   MvdB   ARTF112436: Add zabUtility_GetNetworkInfoStateString()
 * 002.002.012  14-Oct-15   MvdB   ARTF104107: Support updated enums for End Device operation
 * 002.002.020  18-Mar-16   MvdB   ARTF165342: Support performing SBL firmware updates of ZAB_NET_PROC_MODEL_CC2538_SBS_SERIAL_GATEWAY
 * 002.002.052  30-Mar-17   MvdB   Support ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE
 * 002.002.053  12-Apr-17   SMon   Support ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE_PA
 *****************************************************************************/

#include "zabCorePrivate.h"



/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Calculate the TI ZNP Frame Check Sequence (FCS) for a buffer
 * This is used for ZNP commands transported over UART.
 ******************************************************************************/
unsigned8 zabUtility_ComputeFCS(unsigned8* pBuffer, unsigned16 length)
{
  unsigned16 x;
  unsigned8 xorResult;

  xorResult = 0;
  if (pBuffer != NULL)
    {
      for (x = 0; x < length; x++, pBuffer++)
        {
          xorResult = xorResult ^ *pBuffer;
        }
    }
  return xorResult;
}

/******************************************************************************
 * Get a string description of zabOpenState
 ******************************************************************************/
char* zabUtility_GetOpenStateString(zabOpenState openState)
{
  switch(openState)
    {
      case ZAB_OPEN_STATE_NULL: return "ZAB_OPEN_STATE_NULL";
      case ZAB_OPEN_STATE_ERROR: return "ZAB_OPEN_STATE_ERROR";
      case ZAB_OPEN_STATE_CLOSED: return "ZAB_OPEN_STATE_CLOSED";
      case ZAB_OPEN_STATE_CLOSING: return "ZAB_OPEN_STATE_CLOSING";
      case ZAB_OPEN_STATE_OPENED: return "ZAB_OPEN_STATE_OPENED";
      case ZAB_OPEN_STATE_OPENING: return "ZAB_OPEN_STATE_OPENING";
      case ZAB_OPEN_STATE_GOING_TO_FW_UPDATE: return "ZAB_OPEN_STATE_GOING_TO_FW_UPDATE";
      case ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE: return "ZAB_OPEN_STATE_OPEN_FOR_FW_UPDATE";
      case ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE: return "ZAB_OPEN_STATE_OPEN_BUT_INCOMPATIBLE";
      default: return "ZAB_OPEN_STATE_UNKOWN";
    }
}

/******************************************************************************
 * Get a string description of zabFwUpdateState
 ******************************************************************************/
char* zabUtility_GetFirmwareUpdateStateString(zabFwUpdateState updateState)
{
  switch(updateState)
    {
      case ZAB_FW_STATE_NONE: return "ZAB_FW_STATE_NONE";
      case ZAB_FW_STATE_UPDATING: return "ZAB_FW_STATE_UPDATING";
      case ZAB_FW_STATE_VERIFYING: return "ZAB_FW_STATE_VERIFYING";
      case ZAB_FW_STATE_FAILED: return "ZAB_FW_STATE_FAILED";
      case ZAB_FW_STATE_COMPLETE: return "ZAB_FW_STATE_COMPLETE";
      default: return "ZAB_FW_STATE_UNKNOWN";
    }
}

/******************************************************************************
 * Get a string description of zabNwkState
 ******************************************************************************/
char* zabUtility_GetNetworkStateString(zabNwkState nwkState)
{
  switch(nwkState)
    {
      case ZAB_NWK_STATE_UNINITIALISED: return "ZAB_NWK_STATE_UNINITIALISED";
      case ZAB_NWK_STATE_NONE: return "ZAB_NWK_STATE_NONE";
      case ZAB_NWK_STATE_NETWORKED: return "ZAB_NWK_STATE_NETWORKED";
      case ZAB_NWK_STATE_NETWORKING: return "ZAB_NWK_STATE_NETWORKING";
      case ZAB_NWK_STATE_DISCOVERING: return "ZAB_NWK_STATE_DISCOVERING";
      case ZAB_NWK_STATE_DISCOVERY_COMPLETE: return "ZAB_NWK_STATE_DISCOVERY_COMPLETE";
      case ZAB_NWK_STATE_LEAVING: return "ZAB_NWK_STATE_LEAVING";
      case ZAB_NWK_STATE_NETWORKED_NO_COMMS: return "ZAB_NWK_STATE_NETWORKED_NO_COMMS";
      default: return "ZAB_NWK_STATE_UNKNOWN";
    }
}

/******************************************************************************
 * Get a string description of zabNwkSteerOptions
 ******************************************************************************/
char* zabUtility_GetNwkSteerString(zabNwkSteerOptions nwkSteerOptions)
{
  switch(nwkSteerOptions)
    {
      case ZAB_NWK_STEER_JOIN: return "ZAB_NWK_STEER_JOIN";
      case ZAB_NWK_STEER_FORM: return "ZAB_NWK_STEER_FORM";
      case ZAB_NWK_STEER_JOIN_ENDDEVICE: return "ZAB_NWK_STEER_JOIN_ENDDEVICE";
      default: return "ZAB_NWK_STEER_UNKNOWN";
    }
}

/******************************************************************************
 * Get a string description of zabDeviceType
 ******************************************************************************/
char* zabUtility_GetDeviceTypeString(zabDeviceType nwkDevType)
{
  switch(nwkDevType)
    {
      case ZAB_DEVICE_TYPE_NONE: return "ZAB_DEVICE_TYPE_NONE";
      case ZAB_DEVICE_TYPE_COORDINATOR: return "ZAB_DEVICE_TYPE_COORDINATOR";
      case ZAB_DEVICE_TYPE_ROUTER: return "ZAB_DEVICE_TYPE_ROUTER";
      case ZAB_DEVICE_TYPE_END_DEVICE : return "ZAB_DEVICE_TYPE_END_DEVICE";
      default: return "ZAB_DEVICE_TYPE_UNKOWN";
    }
}

/******************************************************************************
 * Get a string description of zabCloneState
 ******************************************************************************/
char* zabUtility_GetCloneStateString(zabCloneState cloneState)
{
  switch(cloneState)
    {
      case ZAB_CLONE_STATE_NONE: return "ZAB_CLONE_STATE_NONE";
      case ZAB_CLONE_STATE_READING: return "ZAB_CLONE_STATE_READING";
      case ZAB_CLONE_STATE_READ_COMPLETE_SUCCESS: return "ZAB_CLONE_STATE_READ_COMPLETE_SUCCESS";
      case ZAB_CLONE_STATE_READ_FAILED: return "ZAB_CLONE_STATE_READ_FAILED";
      case ZAB_CLONE_STATE_WRITING: return "ZAB_CLONE_STATE_WRITING";
      case ZAB_CLONE_STATE_WRITE_COMPLETE_SUCCESS: return "ZAB_CLONE_STATE_WRITE_COMPLETE_SUCCESS";
      case ZAB_CLONE_STATE_WRITE_FAILED: return "ZAB_CLONE_STATE_WRITE_FAILED";
      default: return "ZAB_CLONE_STATE_UNKNOWN";
    }
}


/******************************************************************************
 * Get a string description of zabChannelChangeState
 ******************************************************************************/
char* zabUtility_GetChannelChangeStateString(zabChannelChangeState channelChangeState)
{
  switch(channelChangeState)
    {
      case ZAB_CHANNEL_CHANGE_STATE_NONE: return "ZAB_CHANNEL_CHANGE_STATE_NONE";
      case ZAB_CHANNEL_CHANGE_STATE_IN_PROGRESS: return "ZAB_CHANNEL_CHANGE_STATE_IN_PROGRESS";
      case ZAB_CHANNEL_CHANGE_STATE_FAILED: return "ZAB_CHANNEL_CHANGE_STATE_FAILED";
      case ZAB_CHANNEL_CHANGE_STATE_SUCCESS: return "ZAB_CHANNEL_CHANGE_STATE_SUCCESS";
      default: return "ZAB_CHANNEL_CHANGE_STATE_ERROR";
    }
}

/******************************************************************************
 * Get a string description of zabNetworkKeyChangeState
 ******************************************************************************/
char* zabUtility_GetNetworkKeyChangeStateString(zabNetworkKeyChangeState nwkKeyChangeState)
{
  switch(nwkKeyChangeState)
    {
      case ZAB_NETWORK_KEY_CHANGE_STATE_NONE: return "ZAB_NETWORK_KEY_CHANGE_STATE_NONE";
      case ZAB_NETWORK_KEY_CHANGE_STATE_IN_PROGRESS: return "ZAB_NETWORK_KEY_CHANGE_STATE_IN_PROGRESS";
      case ZAB_NETWORK_KEY_CHANGE_STATE_FAILED: return "ZAB_NETWORK_KEY_CHANGE_STATE_FAILED";
      case ZAB_NETWORK_KEY_CHANGE_STATE_SUCCESS: return "ZAB_NETWORK_KEY_CHANGE_STATE_SUCCESS";
      default: return "ZAB_NETWORK_KEY_CHANGE_STATE_ERROR";
    }
}

/******************************************************************************
 * Get a string description of zabNetworkMaintenanceParamState
 ******************************************************************************/
char* zabUtility_GetNetworkMaintenanceParamStateString(zabNetworkMaintenanceParamState nwkMaintenanceParamState)
{
  switch(nwkMaintenanceParamState)
    {
      case ZAB_NETWORK_MAINTENANCE_PARAM_STATE_NONE: return "ZAB_NETWORK_MAINTENANCE_PARAM_STATE_NONE";
      case ZAB_NETWORK_MAINTENANCE_PARAM_STATE_GET: return "ZAB_NETWORK_MAINTENANCE_PARAM_STATE_GET";
      case ZAB_NETWORK_MAINTENANCE_PARAM_STATE_GET_FAILED: return "ZAB_NETWORK_MAINTENANCE_PARAM_STATE_GET_FAILED";
      case ZAB_NETWORK_MAINTENANCE_PARAM_STATE_GET_SUCCESS: return "ZAB_NETWORK_MAINTENANCE_PARAM_STATE_GET_SUCCESS";
      case ZAB_NETWORK_MAINTENANCE_PARAM_STATE_SET: return "ZAB_NETWORK_MAINTENANCE_PARAM_STATE_SET";
      case ZAB_NETWORK_MAINTENANCE_PARAM_STATE_SET_FAILED: return "ZAB_NETWORK_MAINTENANCE_PARAM_STATE_SET_FAILED";
      case ZAB_NETWORK_MAINTENANCE_PARAM_STATE_SET_SUCCESS: return "ZAB_NETWORK_MAINTENANCE_PARAM_STATE_SET_SUCCESS";
      default: return "ZAB_NETWORK_MAINTENANCE_PARAM_STATE_UNKNOWN";
    }
}

/******************************************************************************
 * Get a string description of zabNetworkInfoState
 ******************************************************************************/
char* zabUtility_GetNetworkInfoStateString(zabNetworkInfoState nwkInfoState)
{
  switch(nwkInfoState)
    {
      case ZAB_NETWORK_INFO_STATE_NONE: return "ZAB_NETWORK_INFO_STATE_NONE";
      case ZAB_NETWORK_INFO_STATE_IN_PROGRESS: return "ZAB_NETWORK_INFO_STATE_IN_PROGRESS";
      case ZAB_NETWORK_INFO_STATE_FAILED: return "ZAB_NETWORK_INFO_STATE_FAILED";
      case ZAB_NETWORK_INFO_STATE_SUCCESS: return "ZAB_NETWORK_INFO_STATE_SUCCESS";
      default: return "ZAB_NETWORK_INFO_STATE_ERROR";
    }
}

/******************************************************************************
 * Get a string description of zabAction
 ******************************************************************************/
char* zabUtility_GetZabActionString(zabAction Action)
{
  switch( Action )
  {
    case ZAB_ACTION_NULL: return "ZAB_ACTION_NULL";
    case ZAB_ACTION_ERROR: return "ZAB_ACTION_ERROR";
    case ZAB_ACTION_OPEN: return "ZAB_ACTION_OPEN";
    case ZAB_ACTION_CLOSE: return "ZAB_ACTION_CLOSE";
    case ZAB_ACTION_NWK_INIT: return "ZAB_ACTION_NWK_INIT";
    case ZAB_ACTION_NWK_DISCOVER: return "ZAB_ACTION_NWK_DISCOVER";
    case ZAB_ACTION_NWK_STEER: return "ZAB_ACTION_NWK_STEER";
    case ZAB_ACTION_EZ_MODE_NWK_STEER: return "ZAB_ACTION_EZ_MODE_NWK_STEER";
    case ZAB_ACTION_NWK_PERMIT_JOIN_ENABLE: return "ZAB_ACTION_NWK_PERMIT_JOIN_ENABLE";
    case ZAB_ACTION_NWK_PERMIT_JOIN_DISABLE: return "ZAB_ACTION_NWK_PERMIT_JOIN_DISABLE";
    case ZAB_ACTION_GO_TO_FIRMWARE_UPDATER: return "ZAB_ACTION_GO_TO_FIRMWARE_UPDATER";
    case ZAB_ACTION_EXIT_FIRMWARE_UPDATER: return "ZAB_ACTION_EXIT_FIRMWARE_UPDATER";
    case ZAB_ACTION_UPDATE_FIRMWARE: return "ZAB_ACTION_UPDATE_FIRMWARE";
    case ZAB_ACTION_SET_TX_POWER: return "ZAB_ACTION_SET_TX_POWER";
    case ZAB_ACTION_CHANGE_CHANNEL: return "ZAB_ACTION_CHANGE_CHANNEL";
    case ZAB_ACTION_CHANGE_NETWORK_KEY: return "ZAB_ACTION_CHANGE_NETWORK_KEY";
    case ZAB_ACTION_CLONE_GET: return "ZAB_ACTION_CLONE_GET";
    case ZAB_ACTION_CLONE_SET: return "ZAB_ACTION_CLONE_SET";
    case ZAB_ACTION_SELECT_ANTENNA: return "ZAB_ACTION_SELECT_ANTENNA";
    case ZAB_ACTION_NWK_GET_INFO: return "ZAB_ACTION_NWK_GET_INFO";
    case ZAB_ACTION_NWK_MAINTENANCE_GET_PARAMS: return "ZAB_ACTION_NWK_MAINTENANCE_GET_PARAMS";
    case ZAB_ACTION_NWK_MAINTENANCE_SET_PARAMS: return "ZAB_ACTION_NWK_MAINTENANCE_SET_PARAMS";
    default: return "ZAB_ACTION_UNKOWN";
  }
}

/******************************************************************************
 * Get a string description of sapMsgDirection
 ******************************************************************************/
char* zabUtility_GetSapMsgDirectionString(sapMsgDirection sapMsgDirection)
{
  switch( sapMsgDirection )
  {
      case SAP_MSG_DIRECTION_NULL: return "SAP_MSG_DIRECTION_NULL";
      case SAP_MSG_DIRECTION_IN: return "SAP_MSG_DIRECTION_IN";
      case SAP_MSG_DIRECTION_OUT: return "SAP_MSG_DIRECTION_OUT";
      case SAP_MSG_DIRECTION_ALL: return "SAP_MSG_DIRECTION_ALL";
      default: return "SAP_MSG_DIRECTION_UNKNOWN";
    }
}

/******************************************************************************
 * Get a string description of sapMsgType
 ******************************************************************************/
char* zabUtility_GetSapMsgTypeString(sapMsgType MsgType)
{
  switch( MsgType )
  {
      case SAP_MSG_TYPE_NULL: return "SAP_MSG_TYPE_NULL";
      case SAP_MSG_TYPE_ERROR: return "SAP_MSG_TYPE_ERROR";
      case SAP_MSG_TYPE_APP: return "SAP_MSG_TYPE_APP";
      case SAP_MSG_TYPE_ACTION: return "SAP_MSG_TYPE_ACTION";
      case SAP_MSG_TYPE_NOTIFY: return "SAP_MSG_TYPE_NOTIFY";
      default: return "SAP_MSG_TYPE_UNKNOWN";
  }
}

/******************************************************************************
 * Get a string description of zabVendorType
 ******************************************************************************/
char* zabUtility_GetVendorTypeString(zabVendorType vendorType)
{
  switch(vendorType)
    {
      case ZAB_VENDOR_TYPE_TEXAS_INSTRUMENTS_ZNP: return "ZAB_VENDOR_TYPE_TEXAS_INSTRUMENTS_ZNP";
      case ZAB_VENDOR_TYPE_SILABS_EZSP:           return "ZAB_VENDOR_TYPE_SILABS_EZSP";
      default: return "ZAB_VENDOR_TYPE_UNKNOWN";
    }
}

/******************************************************************************
 * Get a string description of zabNetworkProcessorModel
 ******************************************************************************/
char* zabUtility_GetNetworkProcessorModelString(zabNetworkProcessorModel networkProcessorModel)
{
  switch(networkProcessorModel)
    {
      case ZAB_NET_PROC_MODEL_CC2530_TI_EB:               return "ZAB_NET_PROC_MODEL_CC2530_TI_EB";
      case ZAB_NET_PROC_MODEL_CC2531_TI_USB:              return "ZAB_NET_PROC_MODEL_CC2531_TI_USB";
      case ZAB_NET_PROC_MODEL_CC2531_SPECTEC_USB_PA:      return "ZAB_NET_PROC_MODEL_CC2531_SPECTEC_USB_PA";
      case ZAB_NET_PROC_MODEL_CC2530_SE_OUREA_V2:         return "ZAB_NET_PROC_MODEL_CC2530_SE_OUREA_V2";
      case ZAB_NET_PROC_MODEL_CC2531_SE_INVENTEK_PA:      return "ZAB_NET_PROC_MODEL_CC2531_SE_INVENTEK_PA";
      case ZAB_NET_PROC_MODEL_CC2530_SE_NOVA:             return "ZAB_NET_PROC_MODEL_CC2530_SE_NOVA";
      case ZAB_NET_PROC_MODEL_CC2530_SE_SMARTLINKIPZ:     return "ZAB_NET_PROC_MODEL_CC2530_SE_SMARTLINKIPZ";
      case ZAB_NET_PROC_MODEL_CC2538_TI_EB:               return "ZAB_NET_PROC_MODEL_CC2538_TI_EB";
      case ZAB_NET_PROC_MODEL_CC2538_SE_OUREA_UART:       return "ZAB_NET_PROC_MODEL_CC2538_SE_OUREA_UART";
      case ZAB_NET_PROC_MODEL_CC2538_SE_OUREA_USB:        return "ZAB_NET_PROC_MODEL_CC2538_SE_OUREA_USB";
      case ZAB_NET_PROC_MODEL_CC2538_SBS_SERIAL_GATEWAY:  return "ZAB_NET_PROC_MODEL_CC2538_SE_OUREA_USB";
      case ZAB_NET_PROC_MODEL_SILABS_EFR32_NCP:           return "ZAB_NET_PROC_MODEL_SILABS_EFR32_NCP";
      case ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE:       return "ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE";
      case ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE_PA:    return "ZAB_NET_PROC_MODEL_CC2538_SZCYIT_DONGLE_PA";
      default:                                            return "ZAB_NET_PROC_MODEL_UNKNOWN";
    }
}


/******************************************************************************
 * Get a string description of zabNetworkProcessorApplication
 ******************************************************************************/
char* zabUtility_GetNetworkProcessorApplicationString(zabNetworkProcessorApplication networkProcessorApplication)
{
  switch(networkProcessorApplication)
    {
      case ZAB_NET_PROC_APP_BOOTLOADER:         return "ZAB_NET_PROC_APP_BOOTLOADER";
      case ZAB_NET_PROC_APP_ZIGBEE_PRO:         return "ZAB_NET_PROC_APP_ZIGBEE_PRO";
      case ZAB_NET_PROC_APP_GP_15_4_BRICK:      return "ZAB_NET_PROC_APP_GP_15_4_BRICK";
      case ZAB_NET_PROC_APP_ZIGBEE_EMC_BRICK:   return "ZAB_NET_PROC_APP_ZIGBEE_EMC_BRICK";
      case ZAB_NET_PROC_APP_GPD_BRICK:          return "ZAB_NET_PROC_APP_GPD_BRICK";
      case ZAB_NET_PROC_APP_ZIGBEE_PRO_GP:      return "ZAB_NET_PROC_APP_ZIGBEE_PRO_GP";
      default: return "ZAB_NET_PROC_APP_UNKNOWN";
    }
}


/******************************************************************************
 * Get a string description of zabAntennaOperatingMode
 ******************************************************************************/
char* zabUtility_GetAntennaOperatingModeString(zabAntennaOperatingMode antennaOperatingMode)
{
  switch(antennaOperatingMode)
    {
      case zabAntennaOperatingMode_Antenna1:        return "Antenna1";
      case zabAntennaOperatingMode_Antenna2:        return "Antenna2";
      case zabAntennaOperatingMode_DiversityAuto:   return "DiversityAuto";
      default: return "Unknown";
  }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/