/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the profile wide client commands for the ZAB implementation
 *   of the SZL ZDO interface - TYPES
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *              23-Aug-13   MvdB   Original
 *              27-Jun-14   MvdB   Add User Descriptor support
 * 002.000.001  03-Feb-15   MvdB   ARTF115770: Support ZDO Node Descriptor Request
 * 002.001.001  29-Apr-15   MvdB   ARTF132256: Typo in SZL_ZDO_MGMT_LQI_DEVICE_COORINDATOR corrected to SZL_ZDO_MGMT_LQI_DEVICE_COORDINATOR
 * 002.001.002  15-Jul-15   MvdB   ARTF136585: Add transaction IDs to over the air ZDO commands/responses
 * 002.002.015  21-Oct-15   MvdB   ARTF104106: Support SZL_ZDO_MatchDescriptorReq()
 * 002.002.047  31-Jan-17   MvdB   Add and enforce SZL_ZDO_USER_DESCRIPTOR_LENGTH_MAX
 *****************************************************************************/

#ifndef _SZL_ZDO_TYPES_H_
#define _SZL_ZDO_TYPES_H_

#ifdef __cplusplus
extern "C"
{
#endif








/******************************************************************************
 * ZDO IEEE Address Request / Response
 ******************************************************************************/
/**
 * IEEE Address Request type
 *
 * Whether the list of neighbors is requested in the response or not @ref ENUM_SZL_ZDO_ADDR_REQ_TYPE_t
 */
typedef szl_uint8 ENUM_SZL_ZDO_ADDR_REQ_TYPE;

/**
 * Request type values
 *
 * @ref ENUM_SZL_ZDO_ADDR_REQ_TYPE
 */
typedef enum
{
    SZL_ZDO_ADDR_REQ_SINGLE = 0,     /**< Single device response */
    SZL_ZDO_ADDR_REQ_EXTENDED = 1    /**< Extended response. Includes Network Addresses of neighbours starting from StartIndex */
} ENUM_SZL_ZDO_ADDR_REQ_TYPE_t;

/**
 * Type for IeeeAddrReq.
 * This struct holds the information for the address to read.
 */
typedef struct
{
    szl_uint16 NetworkAddress;                          /**< Network address of the node */
    ENUM_SZL_ZDO_ADDR_REQ_TYPE RequestType;    /**< Type of request: IEEE Addr only, or include neighbours network addresses */
    szl_uint8 StartIndex;                               /**< Start index in list of neighbours if RequestType == SZL_ZDO_ADDR_REQ_EXTENDED */
} SZL_ZdoIeeeAddrReqParams_t;

/**
 * Type for ZdoIeeeAddrRespParams.
 * This struct holds the information for the returned IEEE Address.
 */
typedef struct
{
    szl_uint64 IeeeAddr;                                /**< IEEE Address of the node */
    szl_uint16 NetworkAddress;                          /**< Network address of the node */
    SZL_ZDO_STATUS_t Status;                        /**< Status as returned by the node */
    szl_uint8 NumAssocDev;                              /**< Count of network addresses to follow */
    szl_uint8 StartIndex;                               /**< Start index in list of neighbours if NumAssocDev > 0 */
    szl_uint16 AssocDevNetworkAddresses[VLA_INIT];             /**< Array of network addresses of associated devices. VARIABLE LENGTH ARRAY */
} SZL_ZdoAddrRespParams_t;
#define SZL_ZdoAddrRespParams_t_SIZE(numAssociatedDevices) (sizeof(SZL_ZdoAddrRespParams_t) - (sizeof(szl_uint16)*VLA_INIT) + (sizeof(szl_uint16)*(numAssociatedDevices)))
/**
 *
 * IEEE or Nwk Address response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_ZDO_IeeeAddrReq <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService) Pointer to the instance of ZAB. May be ignored for single instance implementations.
 * @param[in]  Status   (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_ZdoAddrRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_ZdoAddrResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_ZdoAddrRespParams_t* Params, szl_uint8 TransactionId);


/******************************************************************************
 * ZDO Network Address Request / Response
 ******************************************************************************/

/**
 * Type for NwkAddrReq.
 * This struct holds the information for the address to read.
 */
typedef struct
{
    szl_uint64 IeeeAddress;                             /**< IEEE address of the node */
    ENUM_SZL_ZDO_ADDR_REQ_TYPE RequestType;         /**< Type of request: Addr only, or include neighbours network addresses */
    szl_uint8 StartIndex;                               /**< Start index in list of neighbours if RequestType == SZL_ZDO_ADDR_REQ_EXTENDED */
} SZL_ZdoNwkAddrReqParams_t;







/******************************************************************************
 * ZDO Active Endpoint Request / Response
 ******************************************************************************/
/**
 * Type for ActiveEndpointReqParams.
 * This struct holds the information for the endpoint list to read.
 */
typedef struct
{
    szl_uint16 NetworkAddress;                          /**< Network address of the node the command is addressed to */
    szl_uint16 NetworkAddressOfInterest;                /**< Network address of the node we want the response for */
} SZL_ZdoActiveEndpointReqParams_t;

/**
 * Type for ZdoActiveEndpointRespParams.
 * This struct holds the information for the returned endpoint list.
 */
typedef struct
{
    SZL_ZDO_STATUS_t Status;                        /**< Status as returned by the node */
    szl_uint16 NetworkAddressOfInterest;                /**< Network address of the node */
    szl_uint8 ActiveEndpointCount;                      /**< Number of active endpoints to follow in ActiveEndpointList[] */
    szl_uint8 ActiveEndpointList[VLA_INIT];                    /**< Array of active endpoints. VARIABLE LENGTH ARRAY */
} SZL_ZdoActiveEndpointRespParams_t;
#define SZL_ZdoActiveEndpointRespParams_t_SIZE(_num_endpoints) (sizeof(SZL_ZdoActiveEndpointRespParams_t) - (VLA_INIT*sizeof(szl_uint8)) + ((_num_endpoints)*sizeof(szl_uint8)))

/**
 *
 * Active Endpoint response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_ZDO_ActiveEndpointReq <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService) Pointer to the instance of ZAB. May be ignored for single instance implementations.
 * @param[in]  Status   (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_ZdoActiveEndpointRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_ZdoActiveEndpointResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_ZdoActiveEndpointRespParams_t* Params, szl_uint8 TransactionId);



/******************************************************************************
 * Simple Descriptor Request / Response
 ******************************************************************************/
/**
 * Type for SimpleDescriptorReqParams.
 * This struct holds the information for the descriptor to be read.
 */
typedef struct
{
    szl_uint16 NetworkAddress;                          /**< Network address of the node the command is addressed to */
    szl_uint16 NetworkAddressOfInterest;                /**< Network address of the node we want the response for */
    szl_uint8 Endpoint;                                 /**< Endpoint whose descriptor we want */
} SZL_ZdoSimpleDescriptorReqParams_t;

/**
 * Type for ZdoSimpleRescriptorRespParams.
 * This struct holds the information for the returned descriptor.
 */
typedef struct
{
    SZL_ZDO_STATUS_t Status;                        /**< Status as returned by the node */
    szl_uint16 NetworkAddress;                          /**< Network address of the node that returned the data */
    szl_uint16 NetworkAddressOfInterest;                /**< Network address of the node that the data refers to */
    szl_uint8 Endpoint;                                 /**< Endpoint whose descriptor we have */
    szl_uint16 ProfileId;                               /**< Profile of the endpoint */
    szl_uint16 DeviceId;                                /**< Device ID of the endpoint */
    szl_uint8 DeviceVersion;                            /**< Device Version */
    szl_uint8 NumInClusters;                            /**< Length of InClusterList */
    szl_uint16* InClusterList;                          /**< List of in clusters */
    szl_uint8 NumOutClusters;                           /**< Length of OutClusterList */
    szl_uint16* OutClusterList;                         /**< Lits of out clusters */

    /* Private data */
    szl_uint16 ClusterList[VLA_INIT];                   /**< This is where the in/out cluster data is actually stored. */
} SZL_ZdoSimpleDescriptorRespParams_t;
#define SZL_ZdoSimpleDescriptorRespParams_t_SIZE(_num_in_clusters, _num_out_clusters) (sizeof(SZL_ZdoSimpleDescriptorRespParams_t) - (VLA_INIT*sizeof(szl_uint16)) + (((_num_in_clusters)+(_num_out_clusters)*sizeof(szl_uint16))))

/**
 *
 * Simple Descriptor response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_ZDO_SimpleDescriptorReq <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService) Pointer to the instance of ZAB. May be ignored for single instance implementations.
 * @param[in]  Status   (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_ZdoSimpleDescriptorRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_ZdoSimpleDescriptorResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_ZdoSimpleDescriptorRespParams_t* Params, szl_uint8 TransactionId);



/******************************************************************************
 * Match Descriptor Request / Response
 ******************************************************************************/
/**
 * Type for MatchDescriptorReqParams.
 * This struct holds the information for the descriptor matching
 */
typedef struct
{
    szl_uint16 NetworkAddress;                          /**< Network address of the node the command is addressed to */
    szl_uint16 NetworkAddressOfInterest;                /**< Network address of the node we want the response for */
    szl_uint16 ProfileId;                               /**< Profile ID of endpoints to match with. Typically 0x0104 */
    szl_uint8  NumInClusters;                           /**< Number of clusters in the InClusterList */
    szl_uint16 InClusterList[SZL_CFG_MAX_CLUSTERS];     /**< List of In (Server) Clusters to match against */
    szl_uint8  NumOutClusters;                          /**< Number of clusters in the OutClusterList */
    szl_uint16 OutClusterList[SZL_CFG_MAX_CLUSTERS];    /**< List of Out (Client) Clusters to match against */
} SZL_ZdoMatchDescriptorReqParams_t;


/**
 * Type for ZdoMatchRescriptorRespParams.
 * This struct holds the information for the returned endpoint matches.
 */
typedef struct
{
    SZL_ZDO_STATUS_t Status;                            /**< Status as returned by the node */
    szl_uint16 NetworkAddress;                          /**< Network address of the node that returned the data */
    szl_uint16 NetworkAddressOfInterest;                /**< Network address of the node that the data refers to */
    szl_uint8  NumberOfEndpoints;                       /**< Number of items in Endpoints array */
    szl_uint8  Endpoints[VLA_INIT];                     /**< Endpoints whose descriptors matched */
} SZL_ZdoMatchDescriptorRespParams_t;
#define SZL_ZdoMatchDescriptorRespParams_t_SIZE(_num_endpoints) (sizeof(SZL_ZdoMatchDescriptorRespParams_t) - (VLA_INIT*sizeof(szl_uint8)) + (((_num_endpoints)*sizeof(szl_uint8))))

/**
 *
 * Match Descriptor response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_ZDO_MatchDescriptorReq <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService) Pointer to the instance of ZAB. May be ignored for single instance implementations.
 * @param[in]  Status   (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_ZdoMatchDescriptorRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_ZdoMatchDescriptorResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_ZdoMatchDescriptorRespParams_t* Params, szl_uint8 TransactionId);




/******************************************************************************
 * MGMT Network Update Request / Response - FOR ENERGY SCANS ONLY
 ******************************************************************************/
/**
 * Type for SZL_ZdoMgmtNwkUpdateReq_EnergyScan_Params.
 * Note: Scan duration and scan count are handled internally.
 */
typedef struct
{
    szl_uint16 NetworkAddress;                          /**< Network address of the node the command is addressed to */
    SZL_ZIGBEE_CHANNELS_t ScanChannelsMask;             /**< Channels to scan */
} SZL_ZdoMgmtNwkUpdateReq_EnergyScan_Params_t;



/**
 * Type for ZdoMgmtNwkUpdateRespParams.
 * This struct holds the information for the returned energy scan values
 */
typedef struct
{
    SZL_ZDO_STATUS_t Status;                            /**< Status as returned by the node */
    szl_uint16 NetworkAddress;                          /**< Network address of the node that returned the data */
    SZL_ZIGBEE_CHANNELS_t ScannedChannelsMask;          /**< Channels that were scanned */
    szl_uint16 TotalTransmisisons;                      /**< Total transmission count reported by the node */
    szl_uint16 TransmisisonFailures;                    /**< Total transmission failure count reported by the node */
    szl_uint8 ScannedChannelListCount;                  /**< The number of records in the EnergyValues field */
    szl_uint8 EnergyValues[16];                         /**< Energy value detected on each channel */
} SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t;
#define SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t_SIZE (sizeof(SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t))

/**
 *
 * Mgmt Nwk Update response - For Energy Scans Only
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_ZDO_MgmtNwkUpdateReq_EnergyScan <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService) Pointer to the instance of ZAB. May be ignored for single instance implementations.
 * @param[in]  Status   (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_ZdoMgmtNwkUpdateResp_EnergyScan_t) (zabService* Service, SZL_STATUS_t Status, SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t* Params, szl_uint8 TransactionId);







/******************************************************************************
 * MGMT Lqi Request / Response
 ******************************************************************************/

/**
 * Type for ZdoMgmtLqiReqParams.
 * This struct holds the information for the node to read.
 */
typedef struct
{
    szl_uint16 NetworkAddress;                          /**< Network address of the node the command is addressed to */
    szl_uint8 StartIndex;                               /**< Neighbour table index to start reading from */
} SZL_ZdoMgmtLqiReqParams_t;

/**
 * Unknown values in LQI rsp
 */
#define SZL_ZDO_MGMT_LQI_IEEE_ADDR_UNKNOWN 0xFFFFFFFFFFFFFFFFULL
#define SZL_ZDO_MGMT_LQI_NWK_ADDR_UNKNOWN 0xFFFF


/**
 * Device type
 *
 * Logical type of a node @ref ENUM_SZL_ZDO_MGMT_LQI_DEVICE_TYPE_t
 */
typedef szl_uint8 ENUM_SZL_ZDO_MGMT_LQI_DEVICE_TYPE;

/**
 * Device type values
 *
 * @ref ENUM_SZL_ZDO_MGMT_LQI_DEVICE_TYPE
 */
typedef enum
{
    SZL_ZDO_MGMT_LQI_DEVICE_COORDINATOR = 0,      /**< ZigBee Coordinator */
    SZL_ZDO_MGMT_LQI_DEVICE_ROUTER = 1,           /**< ZigBee Router */
    SZL_ZDO_MGMT_LQI_DEVICE_END_DEVICE = 2,       /**< ZigBee End Device */
    SZL_ZDO_MGMT_LQI_DEVICE_UNKNOWN = 3,          /**< Unknown */
} ENUM_SZL_ZDO_MGMT_LQI_DEVICE_TYPE_t;

/**
 * Receiver On When Idle type
 *
 * Whether a node runs its receiver continuously or not @ref ENUM_SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_TYPE_t
 */
typedef szl_uint8 ENUM_SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_TYPE;

/**
 * Receiver On When Idle values
 *
 * @ref ENUM_SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_TYPE
 */
typedef enum
{
    SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_FALSE = 0,   /**< Receiver is off when idle */
    SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_TRUE = 1,    /**< Receiver is on when idle */
    SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_UNKNOWN = 2, /**< Unknown */
} ENUM_SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_TYPE_t;

/**
 * Relationship type
 *
 * Parent/Child relationship of a node @ref ENUM_SZL_ZDO_MGMT_LQI_RELATIONSHIP_TYPE_t
 */
typedef szl_uint8 ENUM_SZL_ZDO_MGMT_LQI_REALATIONSHIP_TYPE;

/**
 * Relationship values
 *
 * @ref ENUM_SZL_ZDO_MGMT_LQI_REALATIONSHIP_TYPE
 */
typedef enum
{
    SZL_ZDO_MGMT_LQI_RELATIONSHIP_NEIGHBOR_IS_PARENT = 0,       /**< Neighbor is the parent */
    SZL_ZDO_MGMT_LQI_RELATIONSHIP_NEIGHBOR_IS_CHILD = 1,        /**< Neighbor is a child */
    SZL_ZDO_MGMT_LQI_RELATIONSHIP_NEIGHBOR_IS_SIBLING = 2,      /**< Neighbor is a sibling */
    SZL_ZDO_MGMT_LQI_RELATIONSHIP_NEIGHBOR_IS_NONE = 3,         /**< Neighbor is none of the above */
    SZL_ZDO_MGMT_LQI_RELATIONSHIP_NEIGHBOR_IS_PREV_CHILD = 4,   /**< Neighbor was a previous child */
} ENUM_SZL_ZDO_MGMT_LQI_RELATIONSHIP_TYPE_t;

/**
 * Permit Joining type
 *
 * Whether the neighbour is accepting join requests @ref ENUM_SZL_ZDO_MGMT_LQI_PERMIT_JOIN_TYPE_t
 */
typedef szl_uint8 ENUM_SZL_ZDO_MGMT_LQI_PERMIT_JOIN_TYPE;

/**
 * Permit Joining values
 *
 * @ref ENUM_SZL_ZDO_MGMT_LQI_PERMIT_JOIN_TYPE
 */
typedef enum
{
    SZL_ZDO_MGMT_LQI_PERMIT_JOIN_FALSE = 0,       /**< Neighbor is not accepting join requests */
    SZL_ZDO_MGMT_LQI_PERMIT_JOIN_TRUE = 1,        /**< Neighbor is accepting join requests */
    SZL_ZDO_MGMT_LQI_PERMIT_JOIN_UNKNOWN = 2,     /**< Unknown */
} ENUM_SZL_ZDO_MGMT_LQI_PERMIT_JOIN_TYPE_t;

/**
 * Type for ZdoMgmtLqiNeighborTableItem.
 * This struct holds the information for a neighbour table entry.
 */
typedef struct
{
    szl_uint64 Epid;                                                /**< Extended PAN ID */
    szl_uint64 Ieee;                                                /**< IEEE Address */
    szl_uint16 NetworkAddress;                                      /**< Network address */
    ENUM_SZL_ZDO_MGMT_LQI_DEVICE_TYPE DeviceType;               /**< ZigBee Device Type */
    ENUM_SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_TYPE_t RxOnWhenIdle;  /**< If the node runs its receiver continuously, or turns it off */
    ENUM_SZL_ZDO_MGMT_LQI_REALATIONSHIP_TYPE Relationship;      /**< The relationship between the neighbor and the current device */
    ENUM_SZL_ZDO_MGMT_LQI_PERMIT_JOIN_TYPE PermitJoin;          /**< Whether the neighbor is accepting join requests */
    szl_uint8 Depth;                                                /**< Tree depth of the neighbor. 0x00 for Pro networks */
    szl_uint8 Lqi;                                                  /**< Estimated link quality for transmissions from this node */
} SZL_ZdoMgmtLqiNeighborTableItem_t;

/**
 * Type for ZdoMgmtLqiRespParams.
 * This struct holds the information for the returned neighbour table entries.
 */
typedef struct
{
    SZL_ZDO_STATUS_t Status;                                  /**< Status as returned by the node */ // NOTE: TBD if we use this or just wrap it into the main status somehow
    szl_uint16 NetworkAddress;                                    /**< Network address of the node that returned the data */
    szl_uint8 NeighborTableEntries;                               /**< Endpoint whose descriptor we have */
    szl_uint8 StartIndex;                                         /**< Profile of the endpoint */
    szl_uint8 NeighborTableListCount;                             /**< Device ID of the endpoint */
    SZL_ZdoMgmtLqiNeighborTableItem_t NeighborTableList[VLA_INIT];   /**< Neighbor table items. VARIABLE LENGTH ARRAY */
} SZL_ZdoMgmtLqiRespParams_t;
#define SZL_ZdoMgmtLqiRespParams_t_SIZE(_num_neighbours) (sizeof(SZL_ZdoMgmtLqiRespParams_t) - (sizeof(SZL_ZdoMgmtLqiNeighborTableItem_t)*VLA_INIT) + (sizeof(SZL_ZdoMgmtLqiNeighborTableItem_t)*(_num_neighbours)))
/**
 *
 * Mgmt Lqi response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_ZDO_MgmtLqiReq <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService) Pointer to the instance of ZAB. May be ignored for single instance implementations.
 * @param[in]  Status   (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_ZdoMgmtLqiRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_ZdoMgmtLqiResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_ZdoMgmtLqiRespParams_t* Params, szl_uint8 TransactionId);



/******************************************************************************
 * MGMT RTG Request / Response
 ******************************************************************************/
/**
 * Type for ZdoMgmtRtgReqParams.
 * This struct holds the information for the node to read.
 */
typedef struct
{
    szl_uint16 NetworkAddress;                          /**< Network address of the node the command is addressed to */
    szl_uint8 StartIndex;                               /**< Neighbour table index to start reading from */
} SZL_ZdoMgmtRtgReqParams_t;



/**
 * Routing Status
 *
 * Status of a routing table entry @ref ENUM_SZL_ZDO_MGMT_RTG_STATUS_t
 */
typedef szl_uint8 ENUM_SZL_ZDO_MGMT_RTG_STATUS;

/**
 * Routing Status values
 *
 * @ref ENUM_SZL_ZDO_MGMT_RTG_STATUS
 */
typedef enum
{
    SZL_ZDO_MGMT_RTG_STATUS_ACTIVE = 0,               /**<  */
    SZL_ZDO_MGMT_RTG_STATUS_DISC_UNDERWAY = 1,        /**<  */
    SZL_ZDO_MGMT_RTG_STATUS_DISC_FAILED = 2,          /**<  */
    SZL_ZDO_MGMT_RTG_STATUS_INACTIVE = 3,             /**<  */
    SZL_ZDO_MGMT_RTG_STATUS_VALIDATION_UNDERWAY = 4,  /**<  */
} ENUM_SZL_ZDO_MGMT_RTG_STATUS_t;

/**
 * Type for ZdoMgmtLqiNeighborTableItem.
 * This struct holds the information for a neighbour table entry.
 */
typedef struct
{
    szl_uint16 DestinationNetworkAddress;                       /**< Destination address for the table entry */
    ENUM_SZL_ZDO_MGMT_RTG_STATUS RtgStatus;                 /**< Status of the route */
    szl_uint16 NextHopNetworkAddress;                           /**< Network address of the next hop for the route */
} SZL_ZdoMgmtRoutingTableItem_t;

/**
 * Type for ZdoMgmtLqiRespParams.
 * This struct holds the information for the returned neighbour table entries.
 */
typedef struct
{
    szl_uint16 SourceNetworkAddress;                              /**< Network address */
    SZL_ZDO_STATUS_t Status;                                  /**< Status as returned by the node. If NOT_SUPPORTED, all other parameters are invalid. */
    szl_uint8 RoutingTableEntries;                               /**< Total number of routing table entriesi n the remote device */
    szl_uint8 StartIndex;                                         /**< Start index within the Routing Table for RoutingTableList */
    szl_uint8 RoutingTableListCount;                             /**< Number of items in the RoutingTableList */
    SZL_ZdoMgmtRoutingTableItem_t RoutingTableList[VLA_INIT];   /**< Routing table items. VARIABLE LENGTH ARRAY */
} SZL_ZdoMgmtRtgRespParams_t;
#define SZL_ZdoMgmtRtgRespParams_t_SIZE(_num_rt_items) (sizeof(SZL_ZdoMgmtRtgRespParams_t) - (sizeof(SZL_ZdoMgmtRoutingTableItem_t)*VLA_INIT) + (sizeof(SZL_ZdoMgmtRoutingTableItem_t)*(_num_rt_items)))
/**
 *
 * Mgmt Rtg response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_ZDO_MgmtRtgReq <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService) Pointer to the instance of ZAB. May be ignored for single instance implementations.
 * @param[in]  Status   (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_ZdoMgmtLqiRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_ZdoMgmtRtgResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_ZdoMgmtRtgRespParams_t* Params, szl_uint8 TransactionId);




/******************************************************************************
 * Bind Request / Response
 ******************************************************************************/
/**
 * Binding Destination Address Modetype
 *
 * The type of addressing used for a binding @ref ENUM_SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_t
 */
typedef szl_uint8 ENUM_SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE;

/**
 * Permit Joining values
 *
 * @ref ENUM_SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE
 */
typedef enum
{
    SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_GROUP,          /**< Binding is to a group, specified by DestinationGroupAddress */
    SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_UNICAST,         /**< Binding is to an endpoint, specified by DestinationIeeeAddress and DestinationEndpoint */
    SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_UNKNOWN         /**< Binding error */
} ENUM_SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_t;

/**
 * Type for ZdoBindReqParams.
 * This struct holds the information for the binding to be made
 */
typedef struct
{
    szl_uint16 NetworkAddress;                                              /**< Network address of the node the command is addressed to */
    szl_uint64 SourceIeeeAddress;                                           /**< IEEE Address of the binding source. This is the device that will generate messages via the bind to the destination */
    szl_uint8 SourceEndpoint;                                               /**< Endpoint of the binding source */
    szl_uint16 ClusterID;                                                   /**< Cluster to be bound */
    ENUM_SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE DestinationAddressMode;  /**< Destination addressing mofe for the bind */
    szl_uint16 DestinationGroupAddress;                                     /**< Desination Group Address. Used if  DestinationAddressMode == SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_GROUP */
    szl_uint64 DestinationIeeeAddress;                                      /**< Desination IEEE Address. Used if  DestinationAddressMode == SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_UNICAST */
    szl_uint8 DestinationEndpoint;                                          /**< Desination endpoint. Used if  DestinationAddressMode == SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_UNICAST */
} SZL_ZdoBindReqParams_t;

/**
 * Type for ZdoBindRespParams.
 * This struct holds the information for the response to a Bind Request
 */
typedef struct
{
    SZL_ZDO_STATUS_t Status;                                  /**< Status as returned by the node */
    szl_uint16 NetworkAddress;                                    /**< Network address of the node that returned the data */
} SZL_ZdoBindRespParams_t;
#define SZL_ZdoBindRespParams_t_SIZE (sizeof(SZL_ZdoBindRespParams_t))
/**
 *
 * Bind response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_ZDO_BindReq <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService) Pointer to the instance of ZAB. May be ignored for single instance implementations.
 * @param[in]  Status   (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_ZdoMgmtLqiRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_ZdoBindResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_ZdoBindRespParams_t* Params, szl_uint8 TransactionId);







/******************************************************************************
 * MGMT Bind Request / Response
 ******************************************************************************/
/**
 * Type for ZdoMgmtRtgBindParams.
 * This struct holds the information for the node to read.
 */
typedef struct
{
    szl_uint16 NetworkAddress;                          /**< Network address of the node the command is addressed to */
    szl_uint8 StartIndex;                               /**< Neighbour table index to start reading from */
} SZL_ZdoMgmtBindReqParams_t;



/**
 * Type for ZdoMgmtBindingTableItem.
 * This struct holds the information for a binding table entry.
 */
typedef struct
{
    szl_uint64 SourceIeeeAddress;                                           /**< IEEE Address of the binding source. This is the device that will generate messages via the bind to the destination */
    szl_uint8 SourceEndpoint;                                               /**< Endpoint of the binding source */
    szl_uint16 ClusterID;                                                   /**< Cluster to be bound */
    ENUM_SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE DestinationAddressMode;  /**< Destination addressing mofe for the bind */
    szl_uint16 DestinationGroupAddress;                                     /**< Desination Group Address. Used if  DestinationAddressMode == SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_GROUP */
    szl_uint64 DestinationIeeeAddress;                                      /**< Desination IEEE Address. Used if  DestinationAddressMode == SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_UNICAST */
    szl_uint8 DestinationEndpoint;                                          /**< Desination endpoint. Used if  DestinationAddressMode == SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_UNICAST */
} SZL_ZdoMgmtBindingTableItem_t;

/**
 * Type for ZdoMgmtLqiRespParams.
 * This struct holds the information for the returned binding table entries.
 */
typedef struct
{
    szl_uint16 SourceNetworkAddress;                                /**< Network address */
    SZL_ZDO_STATUS_t Status;                                    /**< Status as returned by the node. If NOT_SUPPORTED, all other parameters are invalid. */
    szl_uint8 BindingTableEntries;                                  /**< Total number of routing table entriesi n the remote device */
    szl_uint8 StartIndex;                                           /**< Start index within the Routing Table for RoutingTableList */
    szl_uint8 BindingTableListCount;                                /**< Number of items in the RoutingTableList */
    SZL_ZdoMgmtBindingTableItem_t BindingTableList[VLA_INIT];   /**< Routing table items. VARIABLE LENGTH ARRAY */
} SZL_ZdoMgmtBindRespParams_t;
#define SZL_ZdoMgmtBindRespParams_t_SIZE(_num_rt_items) (sizeof(SZL_ZdoMgmtBindRespParams_t) - (sizeof(SZL_ZdoMgmtBindingTableItem_t)*VLA_INIT) + (sizeof(SZL_ZdoMgmtBindingTableItem_t)*(_num_rt_items)))
/**
 *
 * Mgmt Bind response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_ZDO_MgmtBindReq <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService) Pointer to the instance of ZAB. May be ignored for single instance implementations.
 * @param[in]  Status   (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_ZdoMgmtBindRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_ZdoMgmtBindResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_ZdoMgmtBindRespParams_t* Params, szl_uint8 TransactionId);




/******************************************************************************
 * Mgmt Network Update Request
 ******************************************************************************/

/**
 * Enuneration values for Scan Duration field of MGMT Network Update Request
 */
typedef enum
{
  SZLZDO_MGMT_NWK_UPDATE_SCAN_DURATION_ZERO                         = 0,
  SZLZDO_MGMT_NWK_UPDATE_SCAN_DURATION_ONE                          = 1,
  SZLZDO_MGMT_NWK_UPDATE_SCAN_DURATION_TWO                          = 2,
  SZLZDO_MGMT_NWK_UPDATE_SCAN_DURATION_THREE                        = 3,
  SZLZDO_MGMT_NWK_UPDATE_SCAN_DURATION_FOUR                         = 4,
  SZLZDO_MGMT_NWK_UPDATE_SCAN_DURATION_FIVE                         = 5,
  SZLZDO_MGMT_NWK_UPDATE_SCAN_DURATION_CHANNEL_CHANGE               = 0xFE,
  SZLZDO_MGMT_NWK_UPDATE_SCAN_DURATION_UPDATE_MASK_AND_NWK_MANAGER  = 0xFF
} SZLZDO_MGMT_NWK_UPDATE_SCAN_DURATION_t;

/**
 * Type for ZdoMgmtNwkUpdateReqParams.
 * This struct holds the information for the node(s) to update
 */
typedef struct
{
  SZL_Addresses_t DestAddress;                          /**< Destination address: Unicast or SZL_BROADCAST_RX_ON_WHEN_IDLE */
  SZL_ZIGBEE_CHANNELS_t ChannelMask;                    /**< Channel Mask */
  SZLZDO_MGMT_NWK_UPDATE_SCAN_DURATION_t ScanDuration;  /**< 0-5 = A value used to calculate the length of time to spend scanning each channel.
                                                         *   0xFE = Request for channel change
                                                         *   0xFF = Request to change the apsChannelMask and nwkManagerAddr attributes */
  szl_uint8 ScanCount;                                  /**< The number of energy scans to be conducted and reported.
                                                             Only valid for 0 <= ScanDuration = 5 */
  szl_uint8 NwkUpdateId;                                /**< Network Update Id. Only valid for ScanDuration = 0xFE */
  szl_uint16 NwkManagerAddress;                         /**< This field shall be present only if the ScanDuration is set to 0xff, and
                                                             indicates the NWK address of the Network Manager */
} SZL_ZdoMgmtNwkUpdateReqParams_t;

/******************************************************************************
 * Device Announce Indication
 ******************************************************************************/

/**
 * Type for ZdoDeviceAnnounceInd.
 */
typedef struct
{
  szl_bool AltPanCoordinator;
  szl_bool DeviceType_Ffd;
  szl_bool PowerSource_Mains;
  szl_bool RxOnWhenIdle;
  szl_bool SecurityCapability;
  szl_bool AllocateAddress;
  unsigned16 NwkAddress;
  unsigned64 IeeeAddress;
} SZL_ZdoDeviceAnnounceIndParams_t;

/**
 *
 * Device Announce Indication
 *
 * This is a Callback function used by SZL in order to indicate reception of a Device Announce <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService) Pointer to the instance of ZAB. May be ignored for single instance implementations.
 * @param[in]  Params   (@ref SZL_ZdoDeviceAnnounceIndParams_t*) pointer to the parameters
 *
 * @return -
 */
typedef void (*SZL_CB_ZdoDeviceAnnounceInd_t) (zabService* Service, SZL_ZdoDeviceAnnounceIndParams_t* Params);






/******************************************************************************
 * User Descriptor Request / Response
 ******************************************************************************/
/**
 * Type for UserDescriptorReqParams.
 */
typedef struct
{
    szl_uint16 NetworkAddress;                          /**< Network address of the node the command is addressed to */
    szl_uint16 NetworkAddressOfInterest;                /**< Network address of the node we want the response for */
} SZL_ZdoUserDescriptorReqParams_t;

/**
 * Type for ZdoUserDescriptorRespParams.
 * This struct holds the information for the returned descriptor.
 */
typedef struct
{
    SZL_ZDO_STATUS_t Status;                        /**< Status as returned by the node */
    szl_uint16 SourceAddress;                           /**< Network address of the node that returned the response */
    szl_uint16 NetworkAddressOfInterest;                /**< Network address of the node the that desriptor belongs too */
    szl_uint8 UserDescriptorLength;                     /**< Length of UserDescriptor[] */
    szl_uint8 UserDescriptor[VLA_INIT];                 /**< User descriptor string. VARIABLE LENGTH ARRAY */
} SZL_ZdoUserDescriptorRespParams_t;
#define SZL_ZdoUserDescriptorRespParams_t_SIZE(_desc_length) (sizeof(SZL_ZdoUserDescriptorRespParams_t) - (VLA_INIT*sizeof(szl_uint8)) + ((_desc_length)*sizeof(szl_uint8)))

/**
 *
 * User Descriptor response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_ZDO_UserDescriptorReq <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService) Pointer to the instance of ZAB. May be ignored for single instance implementations.
 * @param[in]  Status   (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_ZdoUserDescriptorRespParams_t*) pointer to the parameters
 *
 * @return -
 */
typedef void (*SZL_CB_ZdoUserDescriptorResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_ZdoUserDescriptorRespParams_t* Params, szl_uint8 TransactionId);


/******************************************************************************
 * User Descriptor Set Request / Response
 ******************************************************************************/

#define SZL_ZDO_USER_DESCRIPTOR_LENGTH_MAX    ( 16 )    /**< Maximum length of the User Descriptor */

/**
 * Type for UserDescriptorSetReqParams.
 */
typedef struct
{
    szl_uint16 NetworkAddress;                          /**< Network address of the node the command is addressed to */
    szl_uint16 NetworkAddressOfInterest;                /**< Network address of the node we want to set the descriptor of */
    szl_uint8 UserDescriptorLength;                     /**< Length of UserDescriptor[]. Maximum is SZL_ZDO_USER_DESCRIPTOR_LENGTH_MAX. */
    szl_uint8 UserDescriptor[VLA_INIT];                 /**< User descriptor string. VARIABLE LENGTH ARRAY */
} SZL_ZdoUserDescriptorSetReqParams_t;
#define SZL_ZdoUserDescriptorSetReqParams_t_SIZE(_desc_length) (sizeof(SZL_ZdoUserDescriptorSetReqParams_t) - (VLA_INIT*sizeof(szl_uint8)) + ((_desc_length)*sizeof(szl_uint8)))

/**
 * Type for ZdoUSerDescriptorSetRespParams.
 * This struct holds the information for the returned descriptor.
 */
typedef struct
{
    SZL_ZDO_STATUS_t Status;                        /**< Status as returned by the node */
    szl_uint16 SourceAddress;                           /**< Network address of the node that returned the response */
    szl_uint16 NetworkAddressOfInterest;                /**< Network address of the node the that desriptor belongs too */
} SZL_ZdoUserDescriptorSetRespParams_t;
#define SZL_ZdoUserDescriptorSetRespParams_t_SIZE (sizeof(SZL_ZdoUserDescriptorSetRespParams_t))

/**
 *
 * User Descriptor Set response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_ZDO_UserDescriptorSetReq <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService) Pointer to the instance of ZAB. May be ignored for single instance implementations.
 * @param[in]  Status   (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_ZdoUserDescriptorSetRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_ZdoUserDescriptorSetResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_ZdoUserDescriptorSetRespParams_t* Params, szl_uint8 TransactionId);



/******************************************************************************
 * ZDO Power Descriptor Request / Response
 ******************************************************************************/


/**
 * Type for PowerDescriptorReqParams.
 */
typedef struct
{
    szl_uint16 NetworkAddress;                          /**< Network address of the node the command is addressed to */
    szl_uint16 NetworkAddressOfInterest;                /**< Network address of the node we want the response for */
} SZL_ZdoPowerDescriptorReqParams_t;

/**
 * Current Power Mode
 *
 * Current Sleep/Power Saving mode of the node @ref ENUM_SZL_ZDO_CURRENT_POWER_MODE_t
 */
typedef szl_uint8 ENUM_SZL_ZDO_CURRENT_POWER_MODE;

/**
 * Current Power Mode Values
 *
 * @ref ENUM_SZL_ZDO_CURRENT_POWER_MODE
 */
typedef enum
{
  SZL_ZDO_CURRENT_POWER_MODE_RX_ON_WHEN_IDLE = 0,     /**< Receiver synchronized with the receiver on when idle subfield of the node descriptor */
  SZL_ZDO_CURRENT_POWER_MODE_RX_PERIODIC = 1,         /**< Receiver comes on periodically as defined by the node power descriptor */
  SZL_ZDO_CURRENT_POWER_MODE_RX_ON_STIMULATION = 2    /**< Receiver comes on when stimulated, e.g. by a user pressing a button */
} ENUM_SZL_ZDO_CURRENT_POWER_MODE_t;


/**
 * Available Power Sources
 *
 * Power sources available on this node @ref ENUM_SZL_ZDO_AVAILABLE_POWER_SOURCES_t
 */
typedef szl_uint8 ENUM_SZL_ZDO_AVAILABLE_POWER_SOURCES;

/**
 * Available Power Source Values
 *
 * Note: These are bit fields and may be ORed together!
 *
 * @ref ENUM_SZL_ZDO_AVAILABLE_POWER_SOURCES
 */
typedef enum
{
  SZL_AVAILABLE_POWER_SOURCE_UNKNOWN = 0,               /**< Unknown */
  SZL_AVAILABLE_POWER_SOURCE_MAINS = 1,                 /**< Constant (mains) power */
  SZL_AVAILABLE_POWER_SOURCE_RECHARGEABLE_BATTERY = 2,  /**< Rechargeable battery */
  SZL_AVAILABLE_POWER_SOURCE_DISPOSABLE_BATTERY = 4     /**< Disposable battery */
} ENUM_SZL_ZDO_AVAILABLE_POWER_SOURCES_t;


/**
 * Current Power Sources
 *
 * Power sources current used by this node @ref ENUM_SZL_ZDO_CURRENT_POWER_SOURCE_t
 */
typedef szl_uint8 ENUM_SZL_ZDO_CURRENT_POWER_SOURCE;

/**
 * Current Power Source Values
 *
 * @ref ENUM_SZL_ZDO_CURRENT_POWER_SOURCE
 */
typedef enum
{
  SZL_CURRENT_POWER_SOURCE_UNKNWON = 0,               /**< Unknown */
  SZL_CURRENT_POWER_SOURCE_MAINS = 1,                 /**< Constant (mains) power */
  SZL_CURRENT_POWER_SOURCE_RECHARGEABLE_BATTERY = 2,  /**< Rechargeable battery */
  SZL_CURRENT_POWER_SOURCE_DISPOSABLE_BATTERY = 4     /**< Disposable battery */
} ENUM_SZL_ZDO_CURRENT_POWER_SOURCE_t;


/**
 * Current Power Source Level
 *
 * Level of current power source used by this node @ref ENUM_SZL_ZDO_CURRENT_POWER_SOURCE_LEVEL_t
 */
typedef szl_uint8 ENUM_SZL_ZDO_CURRENT_POWER_SOURCE_LEVEL;

/**
 * Current Power Source Values
 *
 * @ref ENUM_SZL_ZDO_CURRENT_POWER_SOURCE_LEVEL
 */
typedef enum
{
  SZL_CURRENT_POWER_SOURCE_LEVEL_CRITICAL = 0,          /**< Critical */
  SZL_CURRENT_POWER_SOURCE_LEVEL_33_PERCENT = 4,        /**< 33% */
  SZL_CURRENT_POWER_SOURCE_LEVEL_66_PERCENT = 8,        /**< 66% */
  SZL_CURRENT_POWER_SOURCE_LEVEL_100_PERCENT = 12,      /**< 100% */
} ENUM_SZL_ZDO_CURRENT_POWER_SOURCE_LEVEL_t;


/**
 * Type for ZdoPowerDescriptorRespParams.
 * This struct holds the information for the returned descriptor.
 */
typedef struct
{
  SZL_ZDO_STATUS_t Status;                        /**< Status as returned by the node */
  szl_uint16 SourceAddress;                           /**< Network address of the node that returned the response */
  szl_uint16 NetworkAddressOfInterest;                /**< Network address of the node the that desriptor belongs too */
  ENUM_SZL_ZDO_CURRENT_POWER_MODE CurrentPowerMode;                 /**< Current Power Mode */
  ENUM_SZL_ZDO_AVAILABLE_POWER_SOURCES AvailablePowerSources;       /**< Available Power Sources */
  ENUM_SZL_ZDO_CURRENT_POWER_SOURCE CurrentPowerSource;             /**< Current Power Source */
  ENUM_SZL_ZDO_CURRENT_POWER_SOURCE_LEVEL CurrentPowerSourceLevel;  /**< Current Power Source Level */
} SZL_ZdoPowerDescriptorRespParams_t;
#define SZL_ZdoPowerDescriptorRespParams_t_SIZE (sizeof(SZL_ZdoPowerDescriptorRespParams_t))


/**
 *
 * ZDO Power Descriptor response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_ZDO_PowerDescriptorReq <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService) Pointer to the instance of ZAB. May be ignored for single instance implementations.
 * @param[in]  Status   (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_ZdoPowerDescriptorRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_ZdoPowerDescriptorResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_ZdoPowerDescriptorRespParams_t* Params, szl_uint8 TransactionId);



/******************************************************************************
 * ZDO Node Descriptor Request / Response
 ******************************************************************************/

/**
 * Type for NodeDescriptorReqParams.
 */
typedef struct
{
    szl_uint16 NetworkAddress;                          /**< Network address of the node the command is addressed to */
    szl_uint16 NetworkAddressOfInterest;                /**< Network address of the node we want the response for */
} SZL_ZdoNodeDescriptorReqParams_t;

/**
 * Frequency Band
 *
 * Frequency band suppported by the node @ref ENUM_SZL_ZDO_FREQUENCY_BAND_t
 */
typedef szl_uint8 ENUM_SZL_ZDO_FREQUENCY_BAND;

/**
 * Frequency Band Values
 *
 * @ref ENUM_SZL_ZDO_FREQUENCY_BAND
 */
typedef enum
{
    SZL_ZDO_FEQUENCY_BAND_UNKNOWN = 0,       /**< Inknown*/
    SZL_ZDO_FEQUENCY_BAND_868 = 1,           /**< Supported Frequency Band : 868  - 868.6 MHz */
    SZL_ZDO_FEQUENCY_BAND_RESERVED_1 = 2,      /**< Reserved */
    SZL_ZDO_FEQUENCY_BAND_902_928 = 4,         /**< Supported Frequency Band : 902  - 928 MHz */
    SZL_ZDO_FEQUENCY_BAND_2400_2483 = 8,       /**< Supported Frequency Band : 2400 - 2483.5 MHz */
    SZL_ZDO_FEQUENCY_BAND_RESERVED_2 = 16,     /**< Reserved */
} ENUM_SZL_ZDO_FREQUENCY_BAND_t;

/**
* Type for SZL_ZdoNodeDescriptorOption_t.
* This struct holds the information .
*/
typedef union
{
    struct {
        ENUM_SZL_ZDO_MGMT_LQI_DEVICE_TYPE LogicalType:3;                    /**< The device type of the ZigBee node */
        szl_bool ComplexDescriptorAvailable:1;                              /**< Complex descriptor is available or not */
        szl_bool UserDescriptorAvailable:1;                                 /**< User descriptor is available or not */
        szl_uint16   Reserved : 3;                                          /**< Reserved */
        szl_uint8 APSFlag:3;                                                /**< NOT SUPPORTED YET : The applicatiion support sub-layer capabilities */
        ENUM_SZL_ZDO_FREQUENCY_BAND FrequencyBand:5;                        /**< Frequency band suppported */
    } optBits;
    szl_uint16 optWord;
} SZL_ZdoNodeDescriptorOption_t;

/**
 * Device Type
 *
 * Type of device @ref ENUM_SZL_ZDO_DEVICE_TYPE_t
 */
typedef szl_uint8 ENUM_SZL_ZDO_DEVICE_TYPE;

/**
 * Type of device
 *
 * @ref ENUM_SZL_ZDO_DEVICE_TYPE
 */
typedef enum
{
    SZL_ZDO_DEVICE_TYPE_REDUCED_FUNCTION_DEVICE = 0,   /**< Reduced function device */
    SZL_ZDO_DEVICE_TYPE_FULL_FUNCTION_DEVICE = 1,      /**< Full function device */
} ENUM_SZL_ZDO_DEVICE_TYPE_t;

/**
 * Type for SZL_ZdoNodeDescriptorMACCapabilityFlags_t.
 * This struct holds the information for MAC Capabilities.
 */
typedef union
{
    struct {
        szl_bool AlternatePANCoordinator:1;               /**< Node's capability of becoming a PAN coordinator */
        ENUM_SZL_ZDO_DEVICE_TYPE DeviceType:1;            /**< Type of device */
        szl_bool PowerSource:1;                           /**< Current power source is main Power source or not */
        szl_bool ReceiverOnWhenIdle:1;                    /**< Device does not disable its receiver to conserve power during idle periods or not */
        szl_uint8    Reserved:1;
        szl_bool SecurityCapability:1;                    /**< Devise is capable of sending and receiving frames secured using the security suite or not */
        szl_bool AllocateAddress:1;                       /**< Set to 1 or 0 */
    } optBits;
    szl_uint8 optByte;
} SZL_ZdoNodeDescriptorMACCapabilityFlags_t;

/**
* Type for SZL_ZdoNodeDescriptorServerMask_t.
* This struct holds the information for Server Mask.
*/
typedef union
{
    struct {
        szl_bool PrimaryTrustCenter : 1;            /**< PrimaryTrustCenter : Supported or not by this node */
        szl_bool BackupTrustCenter : 1;             /**< BackupTrustCenter : Supported or not by this node */
        szl_bool PrimaryBindingTableCache : 1;      /**< PrimaryBindingTableCache : Supported or not by this node */
        szl_bool BackupBindingTableCache : 1;       /**< BackupBindingTableCache : Supported or not by this node */
        szl_bool PrimaryDiscoveryCache : 1;         /**< PrimaryDiscoveryCache : Supported or not by this node */
        szl_bool BackupDiscoveryCache : 1;          /**< BackupDiscoveryCache : Supported or not by this node */
        szl_bool NetworkManager : 1;                /**< NetworkManager : Supported or not by this node */
        szl_uint16 Reserved : 9;                    /**< Reserved */
    } optBits;
    szl_uint16 optWord;
} SZL_ZdoNodeDescriptorServerMask_t;

/**
 * Type for SZL_ZdoNodeDescriptorDescriptoCapability_t.
 * This struct holds the information for Descriptor capabilities
 */
typedef union
{
    struct {
        szl_bool ExtendedActiveEndpointListAvailable:1;    /**< ExtendedActiveEndpointList is available or not */
        szl_bool ExtendedSimpleDescriptorListAvailable:1;  /**< ExtendedSimpleDescriptorList is available or not */
        szl_uint8    Reserved : 6;                         /**< reseverd */
    } optBits;
    szl_uint8 optByte;
} SZL_ZdoNodeDescriptorDescriptorCapability_t;


/** Default Value : Node Descriptor */

#define NOT_SUPPORTED szl_false;
#define DEFAULT_MANUFACTURER_CODE 0xffff;
#define DEFAULT_SIZE 0;

/**
 * Type for ZdoNodeDescriptorRespParams.
 * This struct holds the information for the returned descriptor.
 */
typedef struct
{
  SZL_ZDO_STATUS_t Status;                                          /**< Status as returned by the node */
  szl_uint16 SourceAddress;                                         /**< Network address of the node that returned the response */
  szl_uint16 NetworkAddressOfInterest;                              /**< Network address of the node the that desriptor belongs too */
  SZL_ZdoNodeDescriptorOption_t Option;				    /**< Option */
  SZL_ZdoNodeDescriptorMACCapabilityFlags_t MACCapabilityFlag;      /**< The node capabilities, as required by the IEEE 802.15.4-2033 MAC sub-layer */
  szl_uint16 ManufacturerCode;                                      /**< Manufacturer code that is allocated by the ZigBee Alliance */
  szl_uint8 MaximumBufferSize;                                      /**< The maximum size, in octets, of the network sub-layer unit (NSDU) for this node */
  szl_uint16 MaximumIncomingTransferSize;                           /**< The maximum size, in octets, of the application sub-layer data unit (ASDU) that can be transferred to this node in one single message transfer */
  SZL_ZdoNodeDescriptorServerMask_t ServerMask;                     /**< System server capabilities of this node */
  szl_uint16 MaximumOutgoingTransferSize;                           /**< The maximum size, in octets, of the application sub-layer data unit (ASDU) that can be transferred from this node in one single message transfer */
  SZL_ZdoNodeDescriptorDescriptorCapability_t DescriptorCapability; /**< Descriptor capabilities of this node */
} SZL_ZdoNodeDescriptorRespParams_t;
#define SZL_ZdoNodeDescriptorRespParams_t_SIZE (sizeof(SZL_ZdoNodeDescriptorRespParams_t))


/**
 *
 * ZDO Node Descriptor response.
 *
 * This is a Callback function used by SZL in order to reply to the \ref SZL_ZDO_NodeDescriptorReq <br>
 *
 *
 \verbatim
  .---------------------------.
  |     .---| Coordinator | X |
  | App | X |-------------+---|
  |-----+---| Router      | X |
  | SZL |   |-------------+---|
  |     .---| SED         | X |
  .---------------------------.
 \endverbatim
 *
 * @param[in]  Service  (@ref zabService) Pointer to the instance of ZAB. May be ignored for single instance implementations.
 * @param[in]  Status   (@ref SZL_STATUS_t) The status for the operation
 * @param[in]  Params   (@ref SZL_ZdoNodeDescriptorRespParams_t*) pointer to the parameters
 * @param[in]  TransactionId The Transaction ID of this response. May be used to match response to request.
 *
 * @return -
 */
typedef void (*SZL_CB_ZdoNodeDescriptorResp_t) (zabService* Service, SZL_STATUS_t Status, SZL_ZdoNodeDescriptorRespParams_t* Params, szl_uint8 TransactionId);




/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* _SZL_ZDO_TYPES_H_ */

