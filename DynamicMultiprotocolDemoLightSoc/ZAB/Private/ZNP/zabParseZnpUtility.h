/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file provides functions to build and parse a ZNP message.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *  00.00.00.02 31-Oct-13   MvdB   Tidy up from original
 * 002.002.021  21-Apr-16   MvdB   ARTF167734: Move masks for command type and subsystem to frame header
 *****************************************************************************/

#ifndef __ZAB_PARSE_ZNP_UTILITY_H__
#define __ZAB_PARSE_ZNP_UTILITY_H__

#include "zabCoreService.h"
#include "zabZnpFrame.h"

#ifdef __cplusplus
extern "C" {
#endif
  

/******************************************************************************
 *                      ******************************
 *                 *****       TYPES & CONSTANTS      *****
 *                      ******************************
 ******************************************************************************/
  
/* Struct of message ZNP 
 * Note: Allow aliasing as this is designed to alias over a data duffer. This prevents strict aliasing warnings for this specific case */
typedef struct 
{
  unsigned8  Length;
  unsigned8  Cmd0;
  unsigned8  Cmd1;
  unsigned8  Data[VLA_INIT];  // Variable length data array
} /*__attribute__((__may_alias__))*/ zabZnpMessageStruct; //SESA274466

/* Length of ZNP command without data */
#ifdef ZAB_VND_CFG_ZNP_M_SOF_AND_FCS_ENABLED
#define ZNPI_MSG_HEADER_LENGTH          (5)
#else
#define ZNPI_MSG_HEADER_LENGTH          (3)
#endif

#define ZNPI_MSG_HEADER_LENGTH_WITHOUT_SOF_FCS          (3)

/******************************************************************************
 *                      ******************************
 *                 *****            MACROS            *****
 *                      ******************************
 ******************************************************************************/

/* So you can use ZAB_MSG_ZNP( Message )->xxxx to access internal structure */
#ifdef ZAB_VND_CFG_ZNP_M_SOF_AND_FCS_ENABLED
#define ZAB_MSG_ZNP( m )      ((zabZnpMessageStruct*)((unsigned8*)&SAP_MSG_GET_APP_DATA_BUFFER( m )[1]))
#else
#define ZAB_MSG_ZNP( m )      ((zabZnpMessageStruct*)(SAP_MSG_GET_APP_DATA_BUFFER( m )))
#endif

/* Quick access to the ZNP data */
#define ZAB_MSG_ZNP_DATA( m )           (ZAB_MSG_ZNP( m )->Data)
#define ZAB_MSG_ZNP_DATA_LENGTH( m )    (ZAB_MSG_ZNP( m )->Length)

#ifdef ZAB_VND_CFG_ZNP_M_SOF_AND_FCS_ENABLED
#define ZAB_MSG_ZNP_SOF( m )           (SAP_MSG_GET_APP_DATA_BUFFER( m )[0])
#define ZAB_MSG_ZNP_FCS( m )           ( ZAB_MSG_ZNP_DATA( m )[ZAB_MSG_ZNP_DATA_LENGTH( m )])
#endif

/* Check if Message is App/ZNP */
#define ZAB_MSG_IS_APP_ZNP( m )  ((sapMsgGetType( m ) == SAP_MSG_TYPE_APP)    && \
                                  (sapMsgGetAppType( m ) == ZAB_MSG_APP_ZNP))

/* Quick check that mesage is App/ZNP, with status setting if it fails */
#ifdef ZAB_CHECK_MSG_ALL
#define ZAB_CHECK_MSG_ZNP( s, m ) do { ER_CHECK_STATUS_NULL( s, m ); if (ZAB_MSG_IS_APP_ZNP( m )) break; erStatusSet( s, ZAB_ERROR_VENDOR_MSG_INVALID ); return; } while (0)
#else
#define ZAB_CHECK_MSG_ZNP( s, m )
#endif  
  
  

  
/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/ 

/******************************************************************************
 * Create a ZNP Serial SAP Out Messages
 * This will be sent over the serial link with SOF and FCS.
 ******************************************************************************/
extern
sapMsg* zabParseZNPCreateRequest( erStatus* Status, zabService *Service, unsigned16 DataLength );

/******************************************************************************
 * Create a ZNP Serial SAP IN Message
 * This is for received data. Data should be stripepd of SOF and FCS, but include
 * the ZNP Length, Cmd[0] and Cmd[1]
 ******************************************************************************/
extern
sapMsg* zabParseZNPCreateResponse( erStatus* Status, zabService *Service, unsigned16 DataLength );

/******************************************************************************
 * Create and send an Application Error Data SAP IN Message
 ******************************************************************************/
extern
void zabParseZNP_AppErrorIn(zabService *Service, unsigned8 TransactionId, zabError Error);

/******************************************************************************
 * Create a RAW Serial SAP Out Message
 * This will be sent over the serial link with no SOF or FCS.
 * It is intended for flushing serial buffers and should normally not be used.
 ******************************************************************************/
extern
sapMsg* zabParseZNPCreateRaw( erStatus* Status, zabService *Service, unsigned16 DataLength );

/******************************************************************************
 * Get/Set ZNP Length field of a message
 ******************************************************************************/
extern void zabParseZNPGetLength( erStatus* Status, sapMsg* Message, unsigned8* Length );
extern void zabParseZNPSetLength( erStatus* Status, sapMsg* Message, unsigned8 Length );

/******************************************************************************
 * Get/Set ZNP Type in Cmd[0] field of a message
 ******************************************************************************/
extern void zabParseZNPGetType( erStatus* Status, sapMsg* Message, ZNPI_TYPE *type );
extern void zabParseZNPSetType( erStatus* Status, sapMsg* Message, ZNPI_TYPE type );

/******************************************************************************
 * Get/Set ZNP Subsystem in Cmd[0] field of a message
 ******************************************************************************/
extern void zabParseZNPSetSubSystem( erStatus* Status, sapMsg* Message, ZNPI_SUBSYSTEM subsystem );
extern void zabParseZNPGetSubSystem( erStatus* Status, sapMsg* Message, ZNPI_SUBSYSTEM *subsystem );

/******************************************************************************
 * Get/Set ZNP Command ID in Cmd[1] field of a message
 ******************************************************************************/
extern void zabParseZNPSetCommandID( erStatus* Status, sapMsg* Message, unsigned8 cmdId );
extern void zabParseZNPGetCommandID( erStatus* Status, sapMsg* Message, unsigned8 *cmdId );

/******************************************************************************
 * Get/Set the data of a ZNP message
 ******************************************************************************/
extern void zabParseZNPGetData( erStatus* Status, sapMsg* Message, unsigned8* LengthPtr, unsigned8** DataPtr );
extern void zabParseZNPSetData( erStatus* Status, sapMsg* Message, unsigned8 Length, unsigned8* Data);

#ifdef __cplusplus
}
#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif // __ZAB_PARSE_ZNP_UTILITY_H__
