#include "appCmdLine.h"
#include "appMain.h"
#include "appConfig.h"
#include "appGateway.h"

/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/
/* Max lenth of a console command */
#define M_MAX_COMMAND_LENGTH 128

//*****************************************************************************
//
// Defines the maximum number of arguments that can be parsed.
//
//*****************************************************************************
#ifndef CMDLINE_MAX_ARGS
#define CMDLINE_MAX_ARGS        32
#endif

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/
  int cmd_help(int argc, char *argv[]);
 int cmd_get(int argc, char *argv[]);
 int cmd_form(int argc, char *argv[]);
 int cmd_permit(int argc, char *argv[]);
 int cmd_white(int argc, char *argv[]);
 

void ZABStateDisplay(void)
{
  if (appService != NULL)
    {
      if ( (zabCoreGetNetworkState(appService) == ZAB_NWK_STATE_NETWORKED) ||
           (zabCoreGetNetworkState(appService) == ZAB_NWK_STATE_NETWORKED_NO_COMMS) )
        {
          printApp(appService, "ZAB:<%s><%s><%s><Ch=0x%02X><Tx=%d/%ddBm><PanID=0x%04X><NwkAddr=0x%04X>\n",
                  "USART1",
                  zabUtility_GetOpenStateString(zabCoreGetOpenState(appService)),
                  zabUtility_GetNetworkStateString(zabCoreGetNetworkState(appService)),
                  appConfig_Info.channel,
                  appConfig_Config.txPower, appConfig_Info.actualTxPower,
                  appConfig_Info.panID,
                  appConfig_Info.nwkAddress);
        }
      else
        {
          printApp(appService, "ZAB:<%s><%s><%s><%s><%s><Ch=%d><Tx=%ddBm>\n",
                    "USART1",
                    zabUtility_GetOpenStateString(zabCoreGetOpenState(appService)),
                    zabUtility_GetNetworkStateString(zabCoreGetNetworkState(appService)),
                    appConfig_Config.nwkResume ? "Resume":"Don't Resume",
                    zabUtility_GetNwkSteerString(appConfig_Config.nwkSteerOptions),
                    appConfig_Config.channelNumber,
                    appConfig_Config.txPower);
        }
    }
}
 
 /**************************************************************************
 * Configuration Table
 **************************************************************************/
tCmdLineEntry g_sCmdTable[] =
{
   { "?",            cmd_help,         "\t: Alias for help" },
   { "GET",          cmd_get,         "\t: Alias for get" },
   { "FORM",          cmd_form,         "\t: Alias for form network" },
   { "PERMIT",          cmd_permit,         "\t: Alias for enable permit join during x seconds" },
   { "WHITE",          cmd_white,         "\t: Alias for adding elements in the white liste" },
     // Leave this last!
  { NULL,           NULL,             NULL}
};
 /**************************************************************************
 * Help commands
 **************************************************************************/
int cmd_help(int argc, char *argv[])
{
  tCmdLineEntry *pEntry;

  //
  // Print some header text.
  //
  printApp(appService, "Available command sets\r\n");
  printApp(appService, "All command sets have a sub help for individual commands accessible via 'set_name ?'\r\n");
  printApp(appService, "------------------\r\n");

  //
  // Point at the beginning of the command table.
  //
  pEntry = &g_sCmdTable[0];

  //
  // Enter a loop to read each entry from the command table.  The end of the
  // table has been reached when the command name is NULL.
  //
  while(pEntry->pcCmd)
  {
    // Print the command name and the brief description.
    printApp(appService, pEntry->pcCmd);
    printApp(appService, pEntry->pcHelp);
    printApp(appService, "\r\n");

    // Advance to the next entry in the table.
    pEntry++;
  }

  // Return success.
  return(0);
}
/**************************************************************************
 * white commands
 **************************************************************************/
int cmd_white(int argc, char *argv[])
{
  uint8_t modifyList = 0;
  uint8_t i,j;
  uint8_t nbElements;
  unsigned32 SourceId;
    if (argc < 2)
    {
      return CMDLINE_BAD_CMD;
    }
   switch (argv[1][0])
    {
      // list
    case 'L':
      if(appConfig_Config.greenPowerFilterByWhiteList > 0)
      {
        for(i=0;i<appConfig_Config.greenPowerFilterByWhiteList;i++)
        {
            printApp(appService, "white list element nb %d : %lx\n",i,appConfig_Config.greenPowerWhiteList[i]);
        }
      }
      else
      {
        printApp(appService, "white list empty\n");
      }
      return CMDLINE_OK;
 
  //add      //nb + SourceIDs
    case 'A':
      if (argc < 4)
      {
        return CMDLINE_BAD_CMD;
      }
      nbElements = strtoul(argv[2],0,16);
      if (argc < (3+nbElements))
      {
        return CMDLINE_BAD_CMD;
      }
      
      for(i=0;i<nbElements;i++)
      {
        if(appConfig_Config.greenPowerFilterByWhiteList == APP_CONFIG_WHITE_LIST_SIZE)
        {
          printApp(appService, "white list full, remove element before adding new one\n");
          return CMDLINE_OK;
        }
        appConfig_Config.greenPowerWhiteList[appConfig_Config.greenPowerFilterByWhiteList] = strtoul(argv[3+i],0,16);
        printApp(appService, "sourceId %lx added in the white liste at position %d\n",appConfig_Config.greenPowerWhiteList[appConfig_Config.greenPowerFilterByWhiteList],appConfig_Config.greenPowerFilterByWhiteList);
        appConfig_Config.greenPowerFilterByWhiteList +=1;
      }
      return CMDLINE_OK;
  //remove //nb+SourceIDs
    case 'R':
      if (argc < 4)
      {
        return CMDLINE_BAD_CMD;
      }
      nbElements = strtoul(argv[2],0,16);
      if (argc < (3+nbElements))
      {
        return CMDLINE_BAD_CMD;
      }
      for(i=0;i<nbElements;i++)
      {
        modifyList = 0;
        SourceId = strtoul(argv[3+i],0,16);
        for(j=0;j<appConfig_Config.greenPowerFilterByWhiteList;j++)
        {
          if(modifyList == 1)
          {
            appConfig_Config.greenPowerWhiteList[j-1] = appConfig_Config.greenPowerWhiteList[j]; 
          }
          if(appConfig_Config.greenPowerWhiteList[j] == SourceId)
          {
            modifyList = 1;        
          }         
        }
        if(modifyList == 1)
        {
          appConfig_Config.greenPowerFilterByWhiteList -= 1;
          printApp(appService, "sourceId %lx removed from the white liste\n",SourceId);
        }
        else
        {
          printApp(appService, "sourceId %lx not is the white liste\n",SourceId);
        }
      }
      return CMDLINE_OK;
  case '?':
        printApp(appService, "Available white liste Commands:\n");
        printApp(appService, "white list               list the content of the white liste\n");
        printApp(appService, "white add                add elements in the white liste\n"); // only 1 elemnts in console test
        printApp(appService, "white remove             remove element in the white liste\n");
        return CMDLINE_OK;
    }
   return CMDLINE_BAD_CMD;
}  

/**************************************************************************
 * form commands
 **************************************************************************/
int cmd_form(int argc, char *argv[])
{   
  unsigned64 Epid =0;
  uint8_t channel = 0;
  uint16_t panId = 0;
  uint8_t argument = FORM_NOARG;
  if (argc == 4)
  {
    argument = FORM_ARG_EPID+FORM_ARG_CHAN+FORM_ARG_PANID;
    Epid= strtoull(argv[1],0,16);
    channel= strtoul(argv[2],0,16);
    panId= strtoul(argv[3],0,16);
  }
  
  if(formNetworkCmd(argument, Epid,channel,panId) == true)
  {
    return CMDLINE_OK;
  } 

  return CMDLINE_BAD_CMD;
} 
/**************************************************************************
 * permit commands
 **************************************************************************/
int cmd_permit(int argc, char *argv[])
{
  uint16_t nbSecondes;
  if (argc < 2)
    {
      return CMDLINE_BAD_CMD;
    }
  nbSecondes = strtoul(argv[1],0,10);
  
  if(permitJoin(nbSecondes) == true)
  {
    return CMDLINE_OK;
  }
  return CMDLINE_BAD_CMD;
} 
/**************************************************************************
 * get commands
 **************************************************************************/
int cmd_get(int argc, char *argv[])
{
  char readNwkProcAppVersion[4];
  
   if (argc < 2)
    {
      return CMDLINE_BAD_CMD;
    }
   switch (argv[1][0])
    {
case 'N':
  printApp(appService, "network state %s\n",zabUtility_GetNetworkStateString(zabCoreGetNetworkState(appService)));
  //zabCoreGetNetworkState(appService);  //network state value 
  return CMDLINE_OK;
case 'Z':
  if ( (zabCoreGetNetworkState(appService) == ZAB_NWK_STATE_NETWORKED) ||
           (zabCoreGetNetworkState(appService) == ZAB_NWK_STATE_NETWORKED_NO_COMMS) )
        {
          printApp(appService, "ZAB:<Ch=0x%02X><EPID=0x%016llX><PanID=0x%04X><NwkAddr=0x%04X>\n",                
                  appConfig_Info.channel,
                  appConfig_Info.epid,
                  appConfig_Info.panID,
                  appConfig_Info.nwkAddress);
        }
      else
        {
          printApp(appService, "Not networked couldn't send network adress\n");
          printApp(appService, "ZAB:<Ch=0x%02X><EPID=0x%016llX><PanID=0x%04X>\n",                
                  appConfig_Info.channel,
                  appConfig_Info.epid,
                  appConfig_Info.panID);
        }
  return CMDLINE_OK;
  case 'L':

    printApp(appService, "IEEE adresse: 0x%016llX\n", appConfig_Info.ieee );

    printApp(appService, "ZAB Firmware version\n");
    printApp(appService, "Core Version: %03d.%03d.%03d.%03d\n",
                   appConfig_Info.coreVersion[0],
                   appConfig_Info.coreVersion[1],
                   appConfig_Info.coreVersion[2] ,
                   appConfig_Info.coreVersion[3]);
    printApp(appService, "Vendor Version: %03d.%03d.%03d.%03d\n",
                   appConfig_Info.vendorVersion[0],
                   appConfig_Info.vendorVersion[1],
                   appConfig_Info.vendorVersion[2],
                   appConfig_Info.vendorVersion[3]);
    
      appGateway_GetNetworkProcessorIeee(readNwkProcAppVersion);
    printApp(appService, "ZNP Firmware version\n");
    printApp(appService, "Nwk Proc ZigBee Application Version = %03d.%03d.%03d.%03d\n",
                   readNwkProcAppVersion[0],
                   readNwkProcAppVersion[1],
                   readNwkProcAppVersion[2],
                   readNwkProcAppVersion[3]);
  return CMDLINE_OK;
   case '?':
        printApp(appService, "Available get Commands:\n");
        printApp(appService, "get net                     get network state\n");
        printApp(appService, "get zigbee                  get zigbee network information\n");
        printApp(appService, "get local                   get local information\n");
        return CMDLINE_OK;
    }
   return CMDLINE_BAD_CMD;
}

//*****************************************************************************
//
//! Process a command line string into arguments and execute the command.
//!
//! \param pcCmdLine points to a string that contains a command line that was
//! obtained by an application by some means.
//!
//! This function will take the supplied command line string and break it up
//! into individual arguments.  The first argument is treated as a command and
//! is searched for in the command table.  If the command is found, then the
//! command function is called and all of the command line arguments are passed
//! in the normal argc, argv form.
//!
//! The command table is contained in an array named <tt>g_sCmdTable</tt> which
//! must be provided by the application.
//!
//! \return Returns \b CMDLINE_BAD_CMD if the command is not found,
//! \b CMDLINE_TOO_MANY_ARGS if there are more arguments than can be parsed.
//! Otherwise it returns the code that was returned by the command function.
//
//*****************************************************************************
int
CmdLineProcess(char *pcCmdLine)
{
    static char *argv[CMDLINE_MAX_ARGS + 1];
    char *pcChar;
    int argc;
    int bFindArg = 1;
    tCmdLineEntry *pCmdEntry;

    //
    // Initialize the argument counter, and point to the beginning of the
    // command line string.
    //
    argc = 0;
    pcChar = pcCmdLine;

    //
    // Advance through the command line until a zero character is found.
    //
    while(*pcChar)
    {
        //
        // If there is a space, then replace it with a zero, and set the flag
        // to search for the next argument.
        //
        if(*pcChar == ' ')
        {
            *pcChar = 0;
            bFindArg = 1;
        }

        //
        // Otherwise it is not a space, so it must be a character that is part
        // of an argument.
        //
        else
        {
            //
            // If bFindArg is set, then that means we are looking for the start
            // of the next argument.
            //
            if(bFindArg)
            {
                //
                // As long as the maximum number of arguments has not been
                // reached, then save the pointer to the start of this new arg
                // in the argv array, and increment the count of args, argc.
                //
                if(argc < CMDLINE_MAX_ARGS)
                {
                    argv[argc] = pcChar;
                    argc++;
                    bFindArg = 0;
                }

                //
                // The maximum number of arguments has been reached so return
                // the error.
                //
                else
                {
                    return(CMDLINE_TOO_MANY_ARGS);
                }
            }
        }

        //
        // Advance to the next character in the command line.
        //
        pcChar++;
    }

    //
    // If one or more arguments was found, then process the command.
    //
    if(argc)
    {
        //
        // Start at the beginning of the command table, to look for a matching
        // command.
        //
        pCmdEntry = &g_sCmdTable[0];

        //
        // Search through the command table until a null command string is
        // found, which marks the end of the table.
        //
        while(pCmdEntry->pcCmd)
        {
            //
            // If this command entry command string matches argv[0], then call
            // the function for this command, passing the command line
            // arguments.
            //
            if(!strcmp(argv[0], pCmdEntry->pcCmd))
            {
                return(pCmdEntry->pfnCmd(argc, argv));
            }

            //
            // Not found, so advance to the next entry.
            //
            pCmdEntry++;
        }
    }

    //
    // Fall through to here means that no matching command was found, so return
    // an error.
    //
    return(CMDLINE_BAD_CMD);
}

//uint8_t tstClusterCmd[]={0x01, 0x10, 0x01, 0x15, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x01, 0x05, 0x06, 0x01};
uint8_t tstClusterCmd[]={0x01, 0x10, 0x01, 0x15, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x01, 0x00, 0x06, 0x01};
void uiProcess(char* data)
{
  char cmdCopy[M_MAX_COMMAND_LENGTH];
 /* Take a copy as the parser tokenises the string, so up arrow breaks */
          strncpy(cmdCopy, data, M_MAX_COMMAND_LENGTH-1);
          cmdCopy[M_MAX_COMMAND_LENGTH-1] = 0;
          if (CmdLineProcess(cmdCopy) == CMDLINE_OK)
            {
              appMain_WorkRequired();
            }
          else
            {
              printApp(appService, "Unknown command\n");
               sendMsgQ(&PlcToZigbeeMsgQ, tstClusterCmd, 16);
            }
          ZABStateDisplay();
}