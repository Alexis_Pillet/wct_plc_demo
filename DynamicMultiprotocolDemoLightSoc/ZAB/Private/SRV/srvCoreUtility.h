/*
 Name:    Service Runtime Configuration Utility
 Author:  Cam Williams
 Company: Schneider Electric

 Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

 Description:
   This utility is for run time configuration of a service.
 * 
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.04.00  21-May-14   MvdB   artf58379: Change Instance from 8 to 32 bits and rename Param1 in ask/give callbacks
*/

#ifndef __SRV_CFG_UTILITY_H__
#define __SRV_CFG_UTILITY_H__

#include "erStatusTypes.h"
#include "zabTypes.h"


#ifdef __cplusplus
extern "C" {
#endif

// R U N   T I M E   C O N F I G U R A T I O N 

/* These functions are called by a service to ask and give config items from/to the app. 
   
   Asking data from that app may happen at any time during the service operation. The 
   service may provide a default which will be loaded into the Buffer and the Size will
   be set to the default value size. If there is no default, the service sets the Size 
   to zero (not null). If the app returns a zero Size, or leaves the Size as zero, than 
   this is an error.

   Giving data to the app from the service is called based on compile time options for
   the service. Data is temporary and must be copied into the app for storage.

   The What parameter is the identifier of the data. Param1/2 are defined per zabAskId. 
   The What identifiers are unique per service, so
   the service id may be used by the app to recognize which service is asking or giving.
*/
typedef void (*srvConfigureBack)( erStatus*   Status,  zabService*  Service,
                                  zabAskId  What,      unsigned32 Param1,
                                  unsigned32* Size,    unsigned8*  Buffer ); 
typedef void (*srvGiveBack)( erStatus*   Status,  zabService*  Service,
                                  zabGiveId  What,     unsigned32 Param1,
                                  unsigned32* Size,    unsigned8*  Buffer ); 

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// S E R V I C E   M A N A G E M E N T

/* These tool methods are used  by the app using the service. 
*/

/******************************************************************************
 * Get the service ID
 ******************************************************************************/
extern
unsigned8 srvCoreGetServiceId( erStatus* Status, zabService* Service );

#ifdef __cplusplus
}
#endif
#endif 


