/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the Open state machine for the ZAB Pro ZNP
 *   vendor.
 *   For details and flow charts see XXX.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 *  xx.xx.xx.xx 16-Oct-13   MvdB   Original
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
 * 002.000.007  13-Apr-15   MvdB   ARTF130627: Handle SRSP errors in Open state machine for better error management
 * 002.001.004  21-Jul-15   MvdB   ARTF132296: Add zabZnpOpen_ReadyForMessagesIn()
 * 002.002.024  28-Jun-16   MvdB   ARTF172101: Implement ZNP ping to handle lost Reset indication when hidden inside a serial command that was not completed before reset
 * 002.002.043  20-Jan-17   MvdB   ARTF190099: Make ping time run time controllable for ZNP & NCP. Private APIs only at this time, for industrial tester.
 *****************************************************************************/
#ifndef __ZAB_ZNP_OPEN_H__
#define __ZAB_ZNP_OPEN_H__


#include "zabZnpOpenTypes.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/  
  
/******************************************************************************
 * Create
 * Initialises open state / state machine
 ******************************************************************************/
extern
void zabZnpOpen_Create(erStatus* Status, zabService* Service);

/******************************************************************************
 * Get Open State
 ******************************************************************************/
extern
zabOpenState zabZnpOpen_GetOpenState(zabService* Service);

/******************************************************************************
 * Is ZAB in a state that is ready to accept messages from the Serial Glue?
 ******************************************************************************/
extern 
zab_bool zabZnpOpen_ReadyForMessagesIn(zabService* Service);

/******************************************************************************
 * Update timeout
 * Check for timeouts within the networking state machine
 ******************************************************************************/
extern
unsigned32 zabZnpOpen_UpdateTimeout(erStatus* Status, zabService* Service, unsigned32 Time);

/******************************************************************************
 * SRSP Timeout Handler
 * Notifies state machine a command has timed out. It will handle it if
 * it generated the command.
 ******************************************************************************/
extern 
void zabZnpOpen_srspTimeoutHandler(erStatus* Status, zabService* Service);

/******************************************************************************
 * Action
 * Handles Open and Close actions for the open state machines
 ******************************************************************************/
extern 
void zabZnpOpen_Action(erStatus* Status, zabService* Service, unsigned8 Action);


/******************************************************************************
 * Close due to an Error
 * This can be called by the vendor code when a critical error happens and 
 * ZAB needs to be re-initialised. Typically failure of serial comms.
 ******************************************************************************/
extern 
void zabZnpOpen_CloseDueToError(erStatus* Status, zabService* Service);

/******************************************************************************
 * In Notification Handler
 * Handles notification from the serial glue for the open state machine
 ******************************************************************************/
extern 
void zabZnpOpen_InNotificationHandler(erStatus* Status, zabService* Service, zabOpenState OpenState);

/******************************************************************************
 * SYS Version Response Handler
 * Accepts the version info of SBL or ZNP
 ******************************************************************************/
extern 
void zabZnpOpen_SysVersionRspHandler(erStatus* Status, 
                                     zabService* Service, 
                                     unsigned8 TransportRev,
                                     unsigned8 ProductId,
                                     unsigned8 MajorVersion,
                                     unsigned8 MinorVersion,
                                     unsigned8 ReleaseVersion);

/******************************************************************************
 * SBL App Version Response Handler
 * Accepts the version info of the app seen by the SBL
 ******************************************************************************/
extern 
void zabZnpOpen_SblAppVersionRspHandler(erStatus* Status, 
                                        zabService* Service, 
                                        sblStatus SblStatus,
                                        unsigned8 TransportRev,
                                        znpProductId ProductId,
                                        unsigned8 MajorVersion,
                                        unsigned8 MinorVersion,
                                        unsigned8 ReleaseVersion);

/******************************************************************************
 * SBL App Start Response Handler
 * Accepts the status of the start request
 ******************************************************************************/
extern 
void zabZnpOpen_SblAppStartRspHandler(erStatus* Status, 
                                      zabService* Service, 
                                      sblStatus SblStatus);


/******************************************************************************
 * SYS Reset Indication Handler
 * Accepts zab_bool parameter which indicates if the vendor is compatible
 * with the version of network processor.
 ******************************************************************************/
extern 
void zabZnpOpen_SysResetIndHandler(erStatus* Status, 
                                   zabService* Service,
                                   zab_bool Compatible,
                                   znpProductId ProductId);

/******************************************************************************
 * IEEE Response Handler
 * This just confirms we got an IEEE address
 ******************************************************************************/
extern
void zabZnpOpen_IeeeRspHandler(erStatus* Status, zabService* Service);


/******************************************************************************
 * Antenna Response Handler
 * This just confirms we got a response
 ******************************************************************************/
extern 
void zabZnpOpen_AntennaRspHandler(erStatus* Status, zabService* Service);

/******************************************************************************
 * Notify open state machine that the SBL has started the app and it should expect a reset indication soon
 ******************************************************************************/
extern
void zabZnpOpen_SblAppStartNotification(erStatus* Status, zabService* Service);

/******************************************************************************
 * Init network processor ping service
 ******************************************************************************/
extern
void zabZnpOpen_InitNetworkProcessorPing(erStatus* Status, zabService* Service, unsigned16 PingTimeMs);

/******************************************************************************
 * Run network processor ping service
 ******************************************************************************/
extern 
unsigned32 zabZnpOpen_UpdateNetworkProcessorPing(erStatus* Status, zabService* Service, unsigned32 Time);


/******************************************************************************
 * Notify network processor model (if necessary)
 ******************************************************************************/
extern 
void zabZnpOpen_NetworkProcessorModelHandler(erStatus* Status, zabService* Service, zabNetworkProcessorModel NpModel);

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/

#ifdef __cplusplus
}
#endif


#endif
