/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the messaging interface for the ZAB EZSP Vendor
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.009  12-Jul-16   MvdB   ARTF174575: Support MAX_APS_PAYLOAD_LENGTH
 *                                 Notify errors in Af Messaging acks and data confirms for faster timeouts
 * 000.000.018  26-Jul-16   MvdB   Support AF Multicasts
 * 000.000.019  26-Jul-16   MvdB   Replace memcpy with osMemCopy, memset with osMemSet
 *                                 Notify upwards on timeout of data request acks
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/

#include "zabNcpService.h"
#include "zabNcpEzsp.h"
#include "zabNcpNwk.h"
#include "zabNcpZdo.h"
#include "zabNcpAf.h"
#include "zabNcpEzspMessaging.h"

#include "ezsp-enum.h"
#include "ezsp-enum-decode.h"
#include "ezsp-protocol.h"
#include "ember-types.h"


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/


/* Length of serialised EmberApsFrame */
#define EMBER_APS_FRAME_HEADER_LENGTH     ( 11 ) // Profile(2), Cluster(2), SrcEp(1), DstEp(1), Options(2), Grp(2), Seq(1)

/* Max/Min messaging frame lengths */
#define UNICAST_FRAME_LENGTH_MAX        ( 5 + EMBER_APS_FRAME_HEADER_LENGTH + MAX_APS_PAYLOAD_LENGTH ) // Type(1), IndexOrDest(2), apsFrame(EMBER_APS_FRAME_HEADER_LENGTH), Tag(1), Length(1), Data(MAX_APS_PAYLOAD_LENGTH)
#define BROADCAST_FRAME_LENGTH_MAX      ( 5 + EMBER_APS_FRAME_HEADER_LENGTH + MAX_APS_PAYLOAD_LENGTH ) // Dest(2), apsFrame(EMBER_APS_FRAME_HEADER_LENGTH), Radius(1), Tag(1), Length(1), Data(MAX_APS_PAYLOAD_LENGTH)
#define MULTICAST_FRAME_LENGTH_MAX      ( 4 + EMBER_APS_FRAME_HEADER_LENGTH + MAX_APS_PAYLOAD_LENGTH ) // apsFrame(EMBER_APS_FRAME_HEADER_LENGTH), Hops(1), NumMemberRadius(1), Tag(1), Length(1), Data(MAX_APS_PAYLOAD_LENGTH)
#define MESSAGE_SENT_HANDLER_MIN_LENGTH ( 6 + EMBER_APS_FRAME_HEADER_LENGTH ) // Type(1), IndexOrDest(2), apsFrame(EMBER_APS_FRAME_HEADER_LENGTH), Tag(1), Status(1), Length(1),
#define INCOMING_FRAME_MIN_LENGTH       ( 8 + EMBER_APS_FRAME_HEADER_LENGTH ) // EmberIncomingMessageType(1), apsFrame(EMBER_APS_FRAME_HEADER_LENGTH), lastHopLqi(1), lastHopRssi(1), EmberNodeId(2), bindingIndex(1), addressIndex(1), messageLength(1)

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Serialise/Deserialise an EmberApsFrame from a buffer
 ******************************************************************************/
static unsigned8 appendEmberApsFrame(unsigned8* Buffer, EmberApsFrame *value)
{
  unsigned8 index = 0;

  COPY_OUT_16_BITS(&Buffer[index], value->profileId); index+=2;
  COPY_OUT_16_BITS(&Buffer[index], value->clusterId); index+=2;
  Buffer[index++] = value->sourceEndpoint;
  Buffer[index++] = value->destinationEndpoint;
  COPY_OUT_16_BITS(&Buffer[index], value->options); index+=2;
  COPY_OUT_16_BITS(&Buffer[index], value->groupId); index+=2;
  Buffer[index++] = value->sequence;

  return index;
}
static unsigned8 fetchEmberApsFrame(unsigned8* Buffer, EmberApsFrame *value)
{
  unsigned8 index = 0;

  value->profileId = COPY_IN_16_BITS(&Buffer[index]); index += 2;
  value->clusterId = COPY_IN_16_BITS(&Buffer[index]); index += 2;
  value->sourceEndpoint = Buffer[index++];
  value->destinationEndpoint = Buffer[index++];
  value->options = COPY_IN_16_BITS(&Buffer[index]); index += 2;
  value->groupId = COPY_IN_16_BITS(&Buffer[index]); index += 2;
  value->sequence = Buffer[index++];

  return index;
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Get Max Payload Length Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMessaging_MaximumPayloadLength(erStatus* Status, zabService* Service)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH] = {0/*seq*/, 0/*FrameControl*/, EZSP_MAXIMUM_PAYLOAD_LENGTH};

  zabNcpEzsp_Send(Status, Service, ZAB_NCP_EZSP_DEFAULT_SEQ_NUM, sizeof(cmd), cmd);
}
void zabNcpEzspMessaging_MaximumPayloadLengthResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  if (PayloadLength >= 1)
    {
      printVendor(Service, "EZSP_MAXIMUM_PAYLOAD_LENGTH: 0x%02X\n",
                  PayloadData[0]);
    }
}

/******************************************************************************
 * Send Unicast Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMessaging_SendUnicast(erStatus* Status, zabService* Service,
                                     EmberOutgoingMessageType type,
                                     EmberNodeId indexOrDestination,
                                     EmberApsFrame *apsFrame,
                                     unsigned8 messageTag,
                                     unsigned8 messageLength,
                                     unsigned8 *messageContents)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH + UNICAST_FRAME_LENGTH_MAX] = {0/*seq*/, 0/*FrameControl*/, EZSP_SEND_UNICAST};
  unsigned8 index;
  ER_CHECK_STATUS(Status);

  index = EZSP_PARAMETERS_INDEX;
  cmd[index++] = type;
  COPY_OUT_16_BITS(&cmd[index], indexOrDestination); index+=2;
  index += appendEmberApsFrame(&cmd[index], apsFrame);
  cmd[index++] = messageTag;

  printVendor(Service, "zabNcpEzspMessaging_SendUnicast: Tag %d, Length %d\n",
              messageTag,
              messageLength);

  if ( messageLength <= MAX_APS_PAYLOAD_LENGTH)
    {
      cmd[index++] = messageLength;
      osMemCopy(Status, &cmd[index], messageContents, messageLength); index+=messageLength;
    }
  else
    {
      cmd[index++] = 0;
      printError(Service, "EZSP_SEND_UNICAST: Length %d exceeds max %d\n", messageLength, MAX_APS_PAYLOAD_LENGTH);
      erStatusSet(Status, ZAB_ERROR_VENDOR_PARSE);
    }

  printVendor(Service, "zabNcpEzspMessaging_SendUnicast: Total Send Length %d\n",
              index);
  zabNcpEzsp_Send(Status, Service, messageTag, index, cmd);
}
void zabNcpEzspMessaging_SendUnicastResponseHandler(erStatus* Status, zabService* Service, unsigned8 TransactionId, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus sentStatus;

  if (PayloadLength >= 2)
    {
      sentStatus = PayloadData[0];

      if (sentStatus != EMBER_SUCCESS)
        {
          printError(Service, "EZSP_SEND_UNICAST: Status: %s (0x%02X), TID = 0x%02X\n",
                      decodeEmberStatus(sentStatus),
                      sentStatus,
                      TransactionId);

          zabNcpEzspMessaging_AppErrorIn(Service, TransactionId, ZAB_ERROR_VENDOR_DATA_CONFIRM_ERROR);
        }
      else
        {
          printVendor(Service, "EZSP_SEND_UNICAST: Status: %s (0x%02X), TID = 0x%02X\n",
                      decodeEmberStatus(sentStatus),
                      sentStatus,
                      TransactionId);
        }
    }
}

/******************************************************************************
 * Send Broadcast Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMessaging_SendBroadcast(erStatus* Status, zabService* Service,
                                       EmberNodeId destination,
                                       EmberApsFrame *apsFrame,
                                       unsigned8 radius, // Zero is converted to EMBER_MAX_HOPS
                                       unsigned8 messageTag,
                                       unsigned8 messageLength,
                                       unsigned8 *messageContents)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH + BROADCAST_FRAME_LENGTH_MAX] = {0/*seq*/, 0/*FrameControl*/, EZSP_SEND_BROADCAST};
  unsigned8 index;
  ER_CHECK_STATUS(Status);

  index = EZSP_PARAMETERS_INDEX;
  COPY_OUT_16_BITS(&cmd[index], destination); index+=2;
  index += appendEmberApsFrame(&cmd[index], apsFrame);
  cmd[index++] = radius;
  cmd[index++] = messageTag;

  if ( messageLength <= MAX_APS_PAYLOAD_LENGTH)
    {
      cmd[index++] = messageLength;
      osMemCopy(Status, &cmd[index], messageContents, messageLength); index+=messageLength;
    }
  else
    {
      cmd[index++] = 0;
      printError(Service, "EZSP_SEND_BROADCAST: Length %d exceeds max %d\n", messageLength, MAX_APS_PAYLOAD_LENGTH);
      erStatusSet(Status, ZAB_ERROR_VENDOR_PARSE);
    }

  zabNcpEzsp_Send(Status, Service, messageTag, index, cmd);
}
void zabNcpEzspMessaging_SendBroadcastResponseHandler(erStatus* Status, zabService* Service, unsigned8 TransactionId, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus sentStatus;

  if (PayloadLength >= 2)
    {
      sentStatus = PayloadData[0];

      if (sentStatus != EMBER_SUCCESS)
        {
          printError(Service, "EZSP_SEND_BROADCAST: Status: %s (0x%02X), TID = 0x%02X\n",
                      decodeEmberStatus(sentStatus),
                      sentStatus,
                      TransactionId);

          zabNcpEzspMessaging_AppErrorIn(Service, TransactionId, ZAB_ERROR_VENDOR_DATA_CONFIRM_ERROR);
        }
      else
        {
          printVendor(Service, "EZSP_SEND_BROADCAST: Status: %s (0x%02X), TID = 0x%02X\n",
                      decodeEmberStatus(sentStatus),
                      sentStatus,
                      TransactionId);
        }
    }
}

/******************************************************************************
 * Send Multicast Request + Response Handler
 ******************************************************************************/
void zabNcpEzspMessaging_SendMulticast(erStatus* Status, zabService* Service,
                                       EmberApsFrame *apsFrame,
                                       unsigned8 hops, // Zero is converted to EMBER_MAX_HOPS
                                       unsigned8 nonmemberRadius, // A value of 7 or greater is treated as infinite
                                       unsigned8 messageTag,
                                       unsigned8 messageLength,
                                       unsigned8 *messageContents)
{
  unsigned8 cmd[EZSP_MIN_FRAME_LENGTH + MULTICAST_FRAME_LENGTH_MAX] = {0/*seq*/, 0/*FrameControl*/, EZSP_SEND_MULTICAST};
  unsigned8 index;
  ER_CHECK_STATUS(Status);

  index = EZSP_PARAMETERS_INDEX;
  index += appendEmberApsFrame(&cmd[index], apsFrame);
  cmd[index++] = hops;
  cmd[index++] = nonmemberRadius;
  cmd[index++] = messageTag;

  if ( messageLength <= MAX_APS_PAYLOAD_LENGTH)
    {
      cmd[index++] = messageLength;
      osMemCopy(Status, &cmd[index], messageContents, messageLength); index+=messageLength;
    }
  else
    {
      cmd[index++] = 0;
      printError(Service, "EZSP_SEND_MULTICAST: Length %d exceeds max %d\n", messageLength, MAX_APS_PAYLOAD_LENGTH);
      erStatusSet(Status, ZAB_ERROR_VENDOR_PARSE);
    }

  zabNcpEzsp_Send(Status, Service, messageTag, index, cmd);
}
void zabNcpEzspMessaging_SendMulticastResponseHandler(erStatus* Status, zabService* Service, unsigned8 TransactionId, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus sentStatus;

  if (PayloadLength >= 2)
    {
      sentStatus = PayloadData[0];

      if (sentStatus != EMBER_SUCCESS)
        {
          printError(Service, "EZSP_SEND_MULTICAST: Status: %s (0x%02X), TID = 0x%02X\n",
                      decodeEmberStatus(sentStatus),
                      sentStatus,
                      TransactionId);

          zabNcpEzspMessaging_AppErrorIn(Service, TransactionId, ZAB_ERROR_VENDOR_DATA_CONFIRM_ERROR);
        }
      else
        {
          printVendor(Service, "EZSP_SEND_MULTICAST: Status: %s (0x%02X), TID = 0x%02X\n",
                      decodeEmberStatus(sentStatus),
                      sentStatus,
                      TransactionId);
        }
    }
}

/******************************************************************************
 * Message Sent Handler
 ******************************************************************************/
void zabNcpEzspMessaging_MessageSentHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  EmberStatus sentStatus;
  unsigned8 transactionId;

  if (PayloadLength >= MESSAGE_SENT_HANDLER_MIN_LENGTH)
    {
      transactionId = PayloadData[3 + EMBER_APS_FRAME_HEADER_LENGTH];
      sentStatus = PayloadData[4 + EMBER_APS_FRAME_HEADER_LENGTH];

      printVendor(Service, "EZSP_MESSAGE_SENT_HANDLER: Status: %s (0x%02X), TID = 0x%02X\n",
                  decodeEmberStatus(sentStatus),
                  sentStatus,
                  transactionId);

      if (sentStatus != EMBER_SUCCESS)
        {
          zabNcpEzspMessaging_AppErrorIn(Service, transactionId, ZAB_ERROR_VENDOR_DATA_CONFIRM_ERROR);
        }
    }
}

/******************************************************************************
 * Incoming Message Handler
 ******************************************************************************/
void zabNcpEzspMessaging_IncomingMessageHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData)
{
  unsigned8 index;
  EmberIncomingMessageType type;
  EmberApsFrame apsFrame;
  unsigned8 lastHopLqi;
  signed8 lastHopRssi;
  unsigned16 sender;
  unsigned8 messageLength;
  unsigned8 *messageContents;

  if (PayloadLength >= INCOMING_FRAME_MIN_LENGTH)
    {
      index = 0;
      type = (EmberIncomingMessageType)PayloadData[index++];
      index += fetchEmberApsFrame(&PayloadData[index], &apsFrame);
      lastHopLqi = PayloadData[index++];
      lastHopRssi = (signed8)PayloadData[index++];
      sender = COPY_IN_16_BITS(&PayloadData[index]); index += 2;
      index++; // bindingIndex = PayloadData[index++];
      index++; // addressIndex = PayloadData[index++];
      messageLength = PayloadData[index++];

      if ( PayloadLength >= (INCOMING_FRAME_MIN_LENGTH + messageLength))
        {
          messageContents = &PayloadData[index++];
        }
      else
        {
          printError(Service, "zabNcpEzspMessaging_IncomingMessageHandler: Message dropped Length error - Total Length = %d, Payload = %d\n", PayloadLength, messageLength);
          return;
        }

      /* Forward messages through their parsers */
      if ( (apsFrame.profileId == EMBER_ZDO_PROFILE_ID) || (apsFrame.destinationEndpoint == EMBER_ZDO_ENDPOINT) )
        {
          zabNcpZdo_IncomingMessageHandler(Status, Service, sender, &apsFrame, messageLength, messageContents);
        }
      else
        {
          zabNcpAf_IncomingMessageHandler(Status, Service, type, sender, &apsFrame, lastHopLqi, lastHopRssi, messageLength, messageContents);
        }
    }
}

/******************************************************************************
 * Create and send an Application Error Data SAP IN Message
 *
 * Note: This function deliberately does not use a status parameter, as it
 * is typically called upon detection of bad status, so using that status will
 * cause the error report to fail!
 * Error reports are "best effort" only. If this function fails then there
 * is no status returned and hence there will never be the opportunity to retry.
 ******************************************************************************/
void zabNcpEzspMessaging_AppErrorIn(zabService *Service, unsigned8 TransactionId, zabError Error)
{
  erStatus errorStatus;
  sapMsg* Msg = NULL;

  erStatusClear(&errorStatus, Service);

  Msg = sapMsgAllocateData( &errorStatus, zabCoreSapData(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_IN, sizeof(zabError));
  ER_CHECK_STATUS_NULL( &errorStatus, Msg );

  sapMsgSetAppType(&errorStatus, Msg, ZAB_MSG_APP_CMD_ERROR);
  sapMsgSetAppTransactionId(&errorStatus, Msg, TransactionId);
  sapMsgCopyAppDataTo(&errorStatus, Msg, (unsigned8*)&Error, sizeof(zabError));
  sapMsgPutFree(&errorStatus, zabCoreSapData(Service), Msg);
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
