/******************************************************************************
 *                          ZAB TEST APPLICATION
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the application Debug Trace functions for a platform
 *   This file is a demonstration only. Application developers are responsible
 *   for adapting this to their platform.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.04.00  14-Apr-14   MvdB   Original
 * 002.000.001  03-Feb-15   MvdB   ARTF104099: Add service pointer to print function for multi instance apps.
 * 002.002.005  11-Sep-15   MvdB   Add platDebug_Mutex to help tidy up console output
 *****************************************************************************/

#ifndef __PLAT_DEBUG_H__
#define __PLAT_DEBUG_H__
#include "osTypes.h"
#include <stdio.h>
#include <string.h>

/******************************************************************************
 *                      ******************************
 *                 *****         CONFIGURATION        *****
 *                      ******************************
 ******************************************************************************/

/* Compile in Error Printf */
#define PLAT_DEBUG_ENABLE_ERRORS

/* Compile in Vendor Printf */
#define PLAT_DEBUG_ENABLE_VENDOR

/* Compile in Debug Printf */
#define PLAT_DEBUG_ENABLE_INFO

/* Compile in Sap Printf */
#define PLAT_DEBUG_ENABLE_SAP

/* Compile in Serial Printf */
#define PLAT_DEBUG_ENABLE_SERIAL

/******************************************************************************
 *                      ******************************
 *                *****        EXTERN VARIABLES        *****
 *                      ******************************
 ******************************************************************************/ 

/* Runtime control of debug output (Controlled by the APP!) */
extern zab_bool platDebug_DebugInfoOn;
extern zab_bool platDebug_DebugVendorOn;       
extern zab_bool platDebug_DebugSapOn; 
extern zab_bool platDebug_DebugSerialOn;

extern unsigned8 platDebug_Mutex;
/******************************************************************************
 *                      ******************************
 *                 *****            MACROS            *****
 *                      ******************************
 ******************************************************************************/ 

/* Actual unformatted output of all ZAB and App debug.
 * These macros can be re-defined by the application if they wish to use something other than printf/fprintf */
/* void return of printf to try to keep Klockwork happy about return values not being handled... */
//#define printRaw( format, ... )    (void)printf( format, ##__VA_ARGS__ )

/* General debug output that is used by all ZAB outputs and should also be used by the app.
 * This handles the formatting of the output with a time stamp on each new line */
void platPrintApp( zabService* Service, const char * format, ... );

/* Individual levels of debug output */
#ifdef PLAT_DEBUG_ENABLE_ERRORS
  void platPrintError( zabService* Service, const char * file, const char * func, int line, const char * format, ... );
#else
  #define platPrintError( srv, format, ... )
#endif

#ifdef PLAT_DEBUG_ENABLE_INFO
  void platPrintInfo( zabService* Service, const char * format, ... );
#else
  #define platPrintInfo( srv, format, ... )
#endif

#ifdef PLAT_DEBUG_ENABLE_VENDOR
  void platPrintVendor( zabService* Service, const char * format, ... );
#else
  #define platPrintVendor( srv, format, ... )
#endif

#ifdef PLAT_DEBUG_ENABLE_SAP
  void platPrintSap( zabService* Service, const char * format, ... );
#else
  #define platPrintSap( srv, format, ... )
#endif

#ifdef PLAT_DEBUG_ENABLE_SERIAL
  void platPrintSerial( zabService* Service, const char * format, ... );
#else
  #define platPrintSerial( srv, format, ... )
#endif

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif /* __PLAT_DEBUG_H__ */