
"""
network.network_process
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to store the ZigBeeDevice Objects defining the topology of the network

"""
from collections import namedtuple
from data_model import ZigBeeDeviceFactory


class ZigBeeDeviceContainer:
    """Container to store/get the ZigBeeDevice of the network"""

    def __init__(self, application_frame_generator):
        """Initialization of the ZigBeeDeviceContainer"""
        self._list_zigbee_device = {}
        self._key = namedtuple("Key", ["id_wireless_bridge", "address"])
        self._application_frame_generator = application_frame_generator

    @property
    def list_zigbee_device(self):
        return self._list_zigbee_device

    @list_zigbee_device.setter
    def list_zigbee_device(self, list_zigbee_device):
        self._list_zigbee_device = list_zigbee_device

    def add_device(self, model_id, id_wireless_bridge, address):
        """Check if the frame is a network ack

        Args:
            model_id: Model ID of the product to create
            id_wireless_bridge: ID Wireless Bridge linked to the product
            address: Mac address of the product
        """
        device_factory = ZigBeeDeviceFactory.get_factory(model_id)
        device = device_factory.create(address, id_wireless_bridge, self._application_frame_generator)
        self._list_zigbee_device[self._key(id_wireless_bridge=id_wireless_bridge, address=address)] = device

    def get_device(self, address, id_wireless_bridge):
        """Get a specific ZigBee device

        Args:
            address: Address of the product to get
            id_wireless_bridge: ID Wireless Bridge linked to the product

        Returns:
            The desired ZigBeeDevice

        Warning:
            Always check with is_device_saved() function if the device is present in the ZigBeeDeviceContainer
        """
        return self._list_zigbee_device[self._key(id_wireless_bridge=id_wireless_bridge, address=address)]

    def is_device_saved(self, address, id_wireless_bridge):
        """Check if a specific ZigBee device is stored in ZigBeeDeviceContainer

        Args:
            address: Address of the product
            id_wireless_bridge: ID Wireless Bridge linked to the product

        Returns:
            True is the product is saved, False otherwise
        """
        return self._list_zigbee_device.get(self._key(id_wireless_bridge=id_wireless_bridge,
                                                      address=address), None) is not None
