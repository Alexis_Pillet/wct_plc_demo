/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the ASH (A-Synchronous Host) service
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.002  29-Jun-16   MvdB   Original
 * 000.000.003  29-Jun-16   MvdB   Move statics to vendor service stuct
 * 000.000.010  12-Jul-16   MvdB   General upgrade, review and test of zabNcpAsh to handle errors and retries
 *                                  - Add ZAB_DEVELOPER_TEST_MODE for creating errors during testing
 * 000.000.019  26-Jul-16   MvdB   Replace memcpy with osMemCopy, memset with osMemSet
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/

#include "zabNcpPrivate.h"
#include "zabNcpService.h"
#include "zabNcpEzsp.h"
#include "zabNcpOpen.h"

#include "ash-protocol.h"
#include "ezsp-protocol.h"
#include "ezsp-enum-decode.h"
#include "zabNcpAsh.h"

/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/

/* Offset of items in a general ASH frame */
#define ASH_CONTROL_BYTE_OFFSET       ( 0 )
#define ASH_PAYLOAD_OFFSET            ( 1 )

/* Offset of items in a specific ASH frames */
#define ASH_RSTACK_ERROR_CODE_OFFSET  ( 2 )
#define ASH_ERROR_ERROR_CODE_OFFSET   ( 2 )

/* Reset enumerations, taken from __EM2XX_RESET_DEFS_H__*/
#define EM2XX_RESET_UNKNOWN               0
#define EM2XX_RESET_EXTERNAL              1   // EM2XX reports POWERON instead
#define EM2XX_RESET_POWERON               2
#define EM2XX_RESET_WATCHDOG              3
#define EM2XX_RESET_ASSERT                6
#define EM2XX_RESET_BOOTLOADER            9
#define EM2XX_RESET_SOFTWARE              11

/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

#define ASH_INFO( s )     (ZAB_SERVICE_VENDOR( s )->zabNcpAshInfo)

/******************************************************************************
 *                      *****************************
 *                 *****     EXTERNAL VARIABLES      *****
 *                      *****************************
 ******************************************************************************/

/* Some flags for causing errors and confirming they are handled during development testing */
#ifdef ZAB_DEVELOPER_TEST_MODE
#warning ZAB_DEVELOPER_TEST_MODE enabled. Must not be enabled for real products!
zab_bool zabNcpAsh_DropNextIncomingFrame = zab_false;
zab_bool zabNcpAsh_BreakNextIncomingCrc = zab_false;
zab_bool zabNcpAsh_DropNextOutgoingingFrame = zab_false;
zab_bool zabNcpAsh_BreakNextOutgoingCrc = zab_false;
zab_bool zabNcpAsh_BreakAllOutgoingCrc = zab_false;
#endif

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

static const uint8_t* ezspErrorString(uint8_t error)
{
#ifdef ZAB_VENDOR_VERBOSE_PRINTING
  switch (error) {
  case EM2XX_RESET_UNKNOWN:
    return (uint8_t *) "unknown reset";
  case EM2XX_RESET_EXTERNAL:
    return (uint8_t *) "external reset";
  case EM2XX_RESET_POWERON:
    return (uint8_t *) "power on reset";
  case EM2XX_RESET_WATCHDOG:
    return (uint8_t *) "watchdog reset";
  case EM2XX_RESET_ASSERT:
    return (uint8_t *) "assert reset";
  case EM2XX_RESET_BOOTLOADER:
    return (uint8_t *) "bootloader reset";
  case EM2XX_RESET_SOFTWARE:
    return (uint8_t *) "software reset";
  default:
    return (uint8_t *) decodeEzspStatus(error);
  }
#else
  return (uint8_t *) "";
#endif
}

// Determines frame type from the control byte then validates its length.
// If invalid type or length, returns TYPE_INVALID.
// Values returned by ashFrameType()
#define TYPE_INVALID          0
#define TYPE_DATA             1
#define TYPE_ACK              2
#define TYPE_NAK              3
#define TYPE_RSTACK           4
#define TYPE_ERROR            5
static unsigned8 ashFrameType(unsigned8 control, unsigned8 len)
{
  if (control == ASH_CONTROL_RSTACK) {
    if (len == ASH_FRAME_LEN_RSTACK) {
      return TYPE_RSTACK;
    }
  } else if (control == ASH_CONTROL_ERROR) {
    if (len == ASH_FRAME_LEN_ERROR) {
      return TYPE_ERROR;
    }
  } else if ( (control & ASH_DFRAME_MASK) == ASH_CONTROL_DATA) {
    if (len >= ASH_FRAME_LEN_DATA_MIN) {
      return TYPE_DATA;
    }
  } else if ( (control & ASH_SHFRAME_MASK) == ASH_CONTROL_ACK) {
    if (len == ASH_FRAME_LEN_ACK) {
      return TYPE_ACK;
    }
  } else if ( (control & ASH_SHFRAME_MASK) == ASH_CONTROL_NAK) {
    if (len == ASH_FRAME_LEN_NAK) {
      return TYPE_NAK;
    }
  } else {
    return TYPE_INVALID;
  }
  return TYPE_INVALID;
}

/******************************************************************************
 * Return if the byte is a reserved byte for serial transport (requiring escape)
 ******************************************************************************/
static zab_bool ashReservedByte(unsigned8 byte)
{
  if ( (byte == ASH_XON)
    || (byte == ASH_XOFF)
    || (byte == ASH_SUB)
    || (byte == ASH_CAN)
    || (byte == ASH_ESC)
    || (byte == ASH_FLAG) )
    {
      return zab_true;
    }
  else
    {
      return zab_false;
    }
}

/******************************************************************************
 * Add a byte to a data buffer with escape character if required
 ******************************************************************************/
static unsigned8* stuffByte(unsigned8* DataBuffer, unsigned8 DataByte)
{
  if (ashReservedByte(DataByte) == zab_true)
    {
      *DataBuffer++ = ASH_ESC;
      *DataBuffer++ = (DataByte ^ ASH_FLIP);
    }
  else
    {
      *DataBuffer++ = DataByte;
    }
  return DataBuffer;
}

/******************************************************************************
 * Calculate the final length of the message when all ASH overhead is added.
 *  - Serial Framing: Terminate Byte(1) + CRC(2) + EOF(1)
 *  - escape characters for reserved bytes
 ******************************************************************************/
#define ASH_SERIAL_FRAMING_LENGTH 4
static unsigned8 calculateSerialFrameLength(unsigned8 FrameLength, unsigned8* FrameBuffer, unsigned16 Crc)
{
  unsigned8 totalFrameLength = 0;
  unsigned8 index;

  if (FrameBuffer != NULL)
    {
      totalFrameLength = ASH_SERIAL_FRAMING_LENGTH + FrameLength;

      // Now check for bytes that will need to be stuffed
      for (index = 0; index < FrameLength; index++)
        {
          if (ashReservedByte(FrameBuffer[index]) == zab_true)
            {
              totalFrameLength++;
            }
        }

      if (ashReservedByte((unsigned8)(Crc >> 8)) == zab_true)
        {
          totalFrameLength++;
        }
      if (ashReservedByte((unsigned8)(Crc & 0xFF)) == zab_true)
        {
          totalFrameLength++;
        }
    }

  /* Sanity check for wrap around */
  if (totalFrameLength < FrameLength)
    {
      totalFrameLength = 0;
    }

  return totalFrameLength;
}

/******************************************************************************
 * Randomise an ASH message
 * This is used to reduce the probability of data requiring too many escape characters
 ******************************************************************************/
#define LFSR_POLY   0xB8      // polynomial
#define LFSR_SEED   0x42      // initial value (seed)
static uint8_t ashRandomizeArray(uint8_t seed, uint8_t *buf, uint8_t len)
{
  if (seed == 0) {
    seed = LFSR_SEED;
  }
  while (len--) {
    *buf++ ^= seed;
    seed = (seed & 1) ? ((seed >> 1) ^ LFSR_POLY) : (seed >> 1);
  }
  return seed;
}

/******************************************************************************
 * Send an ASH message
 ******************************************************************************/
static void zabNcpAsh_Send(erStatus* Status, zabService* Service, unsigned8 ashFrameLength, unsigned8* ashFrameData)
{
  unsigned16 crc;
  unsigned8 serialFrameLength;
  sapMsg* serialMsg = NULL;
  unsigned8* serialMsgData = NULL;
  unsigned8 index;
  ER_CHECK_STATUS(Status);

#ifdef ZAB_DEVELOPER_TEST_MODE
  if (zabNcpAsh_DropNextOutgoingingFrame == zab_true)
    {
      zabNcpAsh_DropNextOutgoingingFrame = zab_false;
      printApp(Service,  "zabNcpAsh_Send: zabNcpAsh_DropNextOutgoingingFrame - Frame dropped!!!!!!!!\n");
      return;
    }
#endif

  if ( (ashFrameLength < ASH_MIN_FRAME_LEN ) || (ashFrameLength > ASH_MAX_FRAME_LEN) || (ashFrameData == NULL) )
    {
      printError(Service,  "zabEzspService: Invalid ash frame size %d or pointer\n", ashFrameLength);
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
      return;
    }

  /* Calculate CRC */
  crc = zabNcpService_CalculateCrc16(0xFFFF, ashFrameLength, ashFrameData);

  printVendor(Service,  "zabNcpAsh_Send: ");
  for (index = 0; index < ashFrameLength; index++)
    {
      printVendor(Service,  "%02X ", ashFrameData[index]);
    }
  printVendor(Service,  "- CRC=%04X\n", crc);

#ifdef ZAB_DEVELOPER_TEST_MODE
  if (zabNcpAsh_BreakNextOutgoingCrc == zab_true)
    {
      zabNcpAsh_BreakNextOutgoingCrc = zab_false;
      printApp(Service,  "zabNcpAsh_Send: zabNcpAsh_BreakNextOutgoingCrc - CRC Broken!!!!!!!!\n");
      crc++;
    }
  if (zabNcpAsh_BreakAllOutgoingCrc == zab_true)
    {
      printApp(Service,  "zabNcpAsh_Send: zabNcpAsh_BreakAllOutgoingCrc - CRC Broken!!!!!!!!\n");
      crc++;
    }
#endif

  /* Calculate total length of serial frame with framing, crc and byte stuffing */
  serialFrameLength = calculateSerialFrameLength(ashFrameLength, ashFrameData, crc);
  if (serialFrameLength == 0)
    {
      printError(Service,  "zabEzspService: Invalid serial frame length %d\n", serialFrameLength);
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
      return;
    }

  /* Allocate serial frame */
  serialMsg = sapMsgAllocateData( Status, zabCoreSapSerial(Service), SAP_MSG_TYPE_APP, SAP_MSG_DIRECTION_OUT, serialFrameLength );
  if ( (erStatusIsError(Status)) || (serialMsg == NULL) )
    {
      erStatusSet(Status, SAP_ERROR_NULL_PTR);
      return;
    }
  sapMsgSetAppType (Status, serialMsg, ZAB_MSG_APP_RAW);
  sapMsgSetAppTransactionId(Status, serialMsg, 0);
  sapMsgSetAppDataLength(Status, serialMsg, serialFrameLength);
  serialMsgData = sapMsgGetAppData(serialMsg);
  if (serialMsgData == NULL)
    {
      printError(Service,  "zabEzspService: serialMsgData invalid\n");
      sapMsgFree(Status, serialMsg);
      erStatusSet(Status, SAP_ERROR_NULL_PTR);
      return;
    }

  /* Build the full serial frame with:
   *  - Cancel byte to terminate any frame in progress
   *  - Byte stuffed ash frame
   *  - CRC
   *  - EOF
   */
  *serialMsgData++ = ASH_CAN;
  for (index = 0; index < ashFrameLength; index++)
    {
      serialMsgData = stuffByte(serialMsgData, ashFrameData[index]);
    }
  serialMsgData = stuffByte(serialMsgData, (unsigned8)(crc >> 8));
  serialMsgData = stuffByte(serialMsgData, (unsigned8)(crc & 0xFF));
  *serialMsgData++ = ASH_FLAG;

  sapMsgPutFree(Status, zabCoreSapSerial(Service), serialMsg);
}

/******************************************************************************
 * Send an ASH Ack
 ******************************************************************************/
static void zabNcpAsh_Ack(erStatus* Status, zabService* Service, unsigned8 AckNumber)
{
  unsigned8 ashFrameData[ASH_FRAME_LEN_ACK];

  printVendor(Service,  "zabNcpAsh_Ack: AckNum = %d\n", AckNumber);

  ASH_INFO(Service).ackTx = (AckNumber & ASH_ACKNUM_MASK);
  ashFrameData[ASH_CONTROL_BYTE_OFFSET] = ASH_CONTROL_ACK | ASH_INFO(Service).ackTx;

  zabNcpAsh_Send(Status, Service, ASH_FRAME_LEN_ACK, ashFrameData);
}

/******************************************************************************
 * Send an ASH Nack
 ******************************************************************************/
static void zabNcpAsh_Nack(erStatus* Status, zabService* Service, unsigned8 AckNumber)
{
  unsigned8 ashFrameData[ASH_FRAME_LEN_NAK];

  printVendor(Service,  "zabNcpAsh_Nack: AckNum = %d\n", AckNumber);

  ASH_INFO(Service).ackTx = (AckNumber & ASH_ACKNUM_MASK);
  ashFrameData[ASH_CONTROL_BYTE_OFFSET] = ASH_CONTROL_NAK | ASH_INFO(Service).ackTx;

  zabNcpAsh_Send(Status, Service, ASH_FRAME_LEN_NAK, ashFrameData);
}

/******************************************************************************
 * Process an incoming Ack
 ******************************************************************************/
static void zabNcpAsh_ProcessAck(erStatus* Status, zabService* Service, unsigned8 ControlByte)
{
  unsigned8 ackNum;
  unsigned8 lastFrmTx;

  ackNum = ASH_GET_ACKNUM(ControlByte);
  lastFrmTx = ASH_INFO(Service).frmTx;
  printVendor(Service,  "zabNcpAsh_ProcessAck: AckNum = %d, LastFrmTx = %d\n", ackNum, lastFrmTx);

  /* "Note that ackNum is the number of the next frame the receiver expects, and it is one greater than the last frame received"
   * So if it is frmTx+1 then it is a good ack and we can advance to the next frmTx */
  if (ackNum == INC8(lastFrmTx))
    {
      INC8(ASH_INFO(Service).frmTx);
    }
}

/******************************************************************************
 * Process an incoming Nack
 ******************************************************************************/
static void zabNcpAsh_ProcessNack(erStatus* Status, zabService* Service, unsigned8 ControlByte)
{
  unsigned8 ackNum;
  unsigned8 lastFrmTx;

  ackNum = ASH_GET_ACKNUM(ControlByte);
  lastFrmTx = ASH_INFO(Service).frmTx;
  printVendor(Service,  "zabNcpAsh_ProcessNack: AckNum = %d, LastFrmTx = %d - WARNING!!!\n", ackNum, lastFrmTx);

  /* If the NACK is for the last sent frame, then resend frame (Note that the NCP will not Nack a resend, so we cannot stick in a loop).
   * If it's not a NACK for the last sent frame then something horrible has happened and we will reset the service. */
  if (ackNum == lastFrmTx)
    {
      zabNcpAsh_Data(Status, Service, ASH_INFO(Service).ashTxDataPayloadLength, ASH_INFO(Service).ashTxDataPayload, zab_true);
    }
  else
    {
      printError(Service,  "zabNcpAsh_ProcessNack: Mismatch ASH NAK: %d FrmTx: %d\n", ackNum, ASH_INFO(Service).frmTx);

      zabNcpAsh_InitService(Status, Service);
      zabNcpOpen_ResetIndicationHandler(Status, Service, EM2XX_RESET_UNKNOWN);
    }
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 * Initialise the ASH service
 ******************************************************************************/
void zabNcpAsh_InitService(erStatus* Status, zabService* Service)
{
  ASH_INFO(Service).frmTx = 0;
  ASH_INFO(Service).frmRx = 0;
  ASH_INFO(Service).ackTx = 0;
  ASH_INFO(Service).ackRx = 0;
}

/******************************************************************************
 * Send an ASH Reset
 ******************************************************************************/
void zabNcpAsh_Reset(erStatus* Status, zabService* Service)
{
  unsigned8 ashFrameData[ASH_FRAME_LEN_RST];

  printVendor(Service,  "zabNcpAsh_Reset\n");

  ashFrameData[ASH_CONTROL_BYTE_OFFSET] = ASH_CONTROL_RST;

  zabNcpAsh_Send(Status, Service, ASH_FRAME_LEN_RST, ashFrameData);
}


/******************************************************************************
 * Send ASH Data
 ******************************************************************************/
void zabNcpAsh_Data(erStatus* Status, zabService* Service, unsigned8 BufferLength, unsigned8* BufferData, zab_bool Resend)
{
  unsigned8 ashFrameData[ASH_MAX_FRAME_LEN]; // Note: This uses quite a lot of stack but is simple. Can change to a malloc in the future if necessary.
  unsigned8 index;
  ER_CHECK_STATUS(Status);

  if (BufferLength <= ASH_MAX_DATA_FIELD_LEN)
    {
      printVendor(Service,  "zabNcpAsh_Data:%s Frm=%d, Ack=%d, TX Length = %d, Data = ",
                  Resend ? " RESEND!!!" : "",
                  ASH_INFO(Service).frmTx,
                  ASH_INFO(Service).ackTx,
                  BufferLength);
      for (index = 0; index < BufferLength; index++)
        {
          printVendor(Service,  " %02X", BufferData[index]);
        }
      printVendor(Service, "\n");

      ashFrameData[ASH_CONTROL_BYTE_OFFSET] =  ASH_CONTROL_DATA |
                                              (ASH_INFO(Service).frmTx << ASH_FRMNUM_BIT) |
                                              (ASH_INFO(Service).ackTx << ASH_ACKNUM_BIT);
      if (Resend == zab_true)
        {
          /* Set the resend flag */
          ashFrameData[ASH_CONTROL_BYTE_OFFSET] |= ASH_RFLAG_MASK;
        }
      else
        {
          // This is a first send, so save the payload in case we need to re-send it
          ASH_INFO(Service).ashTxDataPayloadLength = BufferLength;
          osMemCopy(Status, ASH_INFO(Service).ashTxDataPayload, BufferData, BufferLength);
        }
      osMemCopy(Status, &ashFrameData[ASH_PAYLOAD_OFFSET], BufferData, BufferLength);
      ashRandomizeArray(0, &ashFrameData[ASH_PAYLOAD_OFFSET], BufferLength);

      zabNcpAsh_Send(Status, Service, BufferLength+ASH_PAYLOAD_OFFSET, ashFrameData);
    }
  else
    {
      printError(Service,  "zabNcpAsh_Data: Invalid TX Length = %d\n", BufferLength);
      erStatusSet(Status, ZAB_ERROR_VENDOR_MSG_INVALID);
    }
}

/******************************************************************************
 * Process incoming ASH messages
 ******************************************************************************/
void zabNcpAsh_ProcessMessageIn(erStatus* Status, zabService* Service, sapMsg* Message)
{
  unsigned8 ashFrameLength;
  unsigned8* ashFrameData = NULL;
  unsigned16 calculatedCrc;
  unsigned16 incomingCrc;
  uint8_t frmNum;
  unsigned8 frameType;

  /* Parameter checking */
  ashFrameLength = sapMsgGetAppDataLength(Message);
  ashFrameData = sapMsgGetAppData(Message);
  if ( (ashFrameLength < ASH_MIN_FRAME_WITH_CRC_LEN ) || (ashFrameLength > ASH_MAX_FRAME_WITH_CRC_LEN) || (ashFrameData == NULL) )
    {
      printError(Service,  "zabNcpAsh_ProcessMessageIn: Invalid ash frame size %d or pointer\n", ashFrameLength);
      return;
    }

  printVendor(Service,  "zabNcpAsh_ProcessMessageIn: Length = %d, Data =", ashFrameLength);
  for (calculatedCrc = 0; calculatedCrc < ashFrameLength; calculatedCrc++)
    {
      printVendor(Service,  " %02X", ashFrameData[calculatedCrc]);
    }
  printVendor(Service,  "\n");

#ifdef ZAB_DEVELOPER_TEST_MODE
  if (zabNcpAsh_DropNextIncomingFrame == zab_true)
    {
      zabNcpAsh_DropNextIncomingFrame = zab_false;
      printApp(Service,  "zabNcpAsh_ProcessMessageIn: zabNcpAsh_DropNextIncomingFrame: Frame dropped!!!!!!!!\n");
      return;
    }
#endif

  /* Confirm CRC. If it is good strip CRC off end of frame. */
  calculatedCrc = zabNcpService_CalculateCrc16(0xFFFF, ashFrameLength-ASH_CRC_LEN, ashFrameData);
  incomingCrc = ((unsigned16)ashFrameData[ashFrameLength-2] << 8) | ashFrameData[ashFrameLength-1];
#ifdef ZAB_DEVELOPER_TEST_MODE
  if (zabNcpAsh_BreakNextIncomingCrc == zab_true)
    {
      printApp(Service,  "zabNcpAsh_ProcessMessageIn: zabNcpAsh_BreakNextIncomingCrc: Crc broken!!!!!!!!\n");
      zabNcpAsh_BreakNextIncomingCrc = zab_false;
      calculatedCrc++;
    }
#endif
  if (calculatedCrc != incomingCrc)
    {
      printError(Service,  "zabNcpAsh_ProcessMessageIn: CalculatedCRC 0x%04X != IncomingCRC 0x%04X\n", calculatedCrc, incomingCrc);
      zabNcpAsh_Nack(Status, Service, ASH_INFO(Service).frmRx);
      return;
    }
  ashFrameLength -= ASH_CRC_LEN;

  /* Validate Control and Length*/
  frameType = ashFrameType(ashFrameData[ASH_CONTROL_BYTE_OFFSET], ashFrameLength);
  switch (frameType)
    {
      /* Data message from NCP */
      case TYPE_DATA:
        /* Handle any piggy backed Ack */
        zabNcpAsh_ProcessAck(Status, Service, ashFrameData[ASH_CONTROL_BYTE_OFFSET]);

        /* If it is the frmNum we are expecting:
         *  - Inc to next frmNum and ack
         *  - Strip out control byte to have bare EZSP command.
         *  - De-randomise the data.
         *  - Forward to EZSP service  */
        frmNum = ASH_GET_FRMNUM(ashFrameData[ASH_CONTROL_BYTE_OFFSET]);
        if (frmNum == ASH_INFO(Service).frmRx)
          {
            INC8(ASH_INFO(Service).frmRx);
            zabNcpAsh_Ack(Status, Service, ASH_INFO(Service).frmRx);

            ashFrameData+=ASH_PAYLOAD_OFFSET;
            ashFrameLength-=ASH_PAYLOAD_OFFSET;

            ashRandomizeArray(0, ashFrameData, ashFrameLength);

            zabNcpEzsp_ProcessIncomingFrame(Status, Service, ashFrameLength, ashFrameData);
          }
        /* Nack unless it is a re-send, which is never acknowledged */
        else if (ASH_GET_RFLAG(ashFrameData[ASH_CONTROL_BYTE_OFFSET]) == 0)
          {
            printError(Service,  "zabNcpAsh_ProcessMessageIn: ASH DATA bad sequence: %d expected %d\n", frmNum, ASH_INFO(Service).frmRx);
            zabNcpAsh_Nack(Status, Service, ASH_INFO(Service).frmRx);
          }
        break;

      case TYPE_ACK:
        zabNcpAsh_ProcessAck(Status, Service, ashFrameData[ASH_CONTROL_BYTE_OFFSET]);
        break;

      case TYPE_NAK:
        zabNcpAsh_ProcessNack(Status, Service, ashFrameData[ASH_CONTROL_BYTE_OFFSET]);
        break;

      case TYPE_RSTACK:
        printVendor(Service,  "zabNcpAsh_ProcessMessageIn: NCP Reset (RSTACK): %s (0x%02X)\n", ezspErrorString(ashFrameData[2]), ashFrameData[2]);
        zabNcpAsh_InitService(Status, Service);
        zabNcpOpen_ResetIndicationHandler(Status, Service, ashFrameData[ASH_RSTACK_ERROR_CODE_OFFSET]);
        break;

      case TYPE_ERROR:
        printVendor(Service,  "zabNcpAsh_ProcessMessageIn: ASH NCP Disconnect: %s (0x%02X)\n", ezspErrorString(ashFrameData[2]), ashFrameData[2]);
        zabNcpAsh_InitService(Status, Service);
        zabNcpOpen_ResetIndicationHandler(Status, Service, ashFrameData[ASH_ERROR_ERROR_CODE_OFFSET]);
        break;

      case TYPE_INVALID:
      default:
        printVendor(Service,  "zabNcpAsh_ProcessMessageIn: WARNING: ASH frame type invalid\n");
        return;
    }
}

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/