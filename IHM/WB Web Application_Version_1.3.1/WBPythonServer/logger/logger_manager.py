
import logging
import datetime


class LoggerManager(object):

    _loggers = {}

    def __init__(self, *args, **kwargs):
        pass

    _log_level = logging.DEBUG

    @staticmethod
    def set_log_level(log_level):
        LoggerManager._log_level = log_level

    @staticmethod
    def get_logger(name=None):
        if not name:
            logging.basicConfig()
            return logging.getLogger()
        elif name not in LoggerManager._loggers.keys():
            my_logger = logging.getLogger(str(name))
            my_logger.setLevel(logging.DEBUG)
            my_logger.propagate = False

            if not len(my_logger.handlers):
                # create console handler and set level to debug
                console_handler = logging.StreamHandler()
                console_handler.setLevel(LoggerManager._log_level)

                # create formatter
                my_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

                # add formatter to console_handler
                console_handler.setFormatter(my_formatter)

                # create console handler and set level to debug
                now = datetime.datetime.now()
                file_handler = logging.FileHandler('Wireless_bridge_log_'
                                                   + now.strftime("%Y-%m-%d") + '.log')
                file_handler.setLevel(logging.DEBUG)

                # add formatter to console_handler
                file_handler.setFormatter(my_formatter)

                # add console_handler to logger
                my_logger.addHandler(console_handler)
                my_logger.addHandler(file_handler)
            LoggerManager._loggers[name] = my_logger
        return LoggerManager._loggers[name]
