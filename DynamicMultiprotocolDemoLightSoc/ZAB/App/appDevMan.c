/******************************************************************************
 *                          ZAB TEST APPLICATION
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the ConsoleTest application for the Device Manager
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 *****************************************************************************/

#include "zabCoreService.h"

#include "appZcl.h"
#include "device_manager.h"
#include "appDevMan.h"


/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      *****************************
 *                 *****       LOCAL VARIABLES       *****
 *                      *****************************
 ******************************************************************************/


/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Address Changed Handler
 ******************************************************************************/
static void appDevMan_AddressChangedHandler(zabService* Service,
                                            szl_uint16 NetworkAddress,
                                            szl_uint64 IeeeAddress)
{
  printApp(Service, "appDevMan_AddressChangedHandler: NetworkAddress = 0x%04X, IeeeAddress = 0x%016llX\n", NetworkAddress, IeeeAddress);
}

/******************************************************************************
 * Communication from untracked node Handler
 ******************************************************************************/
static void appDevMan_UntrackedNodeCommsHandler(zabService* Service,
                                                szl_uint16 NetworkAddress,
                                                szl_uint64 IeeeAddress)
{
  SZL_RESULT_t res;
  
  printApp(Service, "appDevMan_UntrackedNodeCommsHandler: NetworkAddress = 0x%04X, IeeeAddress = 0x%016llX\n", NetworkAddress, IeeeAddress);
  
  /* Add unknown devices to the manager */
  if (NetworkAddress != SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN)
    {
      printApp(Service, "appDevMan_UntrackedNodeCommsHandler: Adding node with NetworkAddress = 0x%04X\n", NetworkAddress);
      res = SzlPlugin_DevMan_LoadByNetworkAddress(Service, NetworkAddress);
      if (res != SZL_RESULT_SUCCESS)
        {
          printError(Service , "appDevMan_UntrackedNodeCommsHandler: Load by network address failed: 0x%02X (%s)\n", 
                     (unsigned8)res,
                     appZcl_GetSzlResultString(res));  
        }
    }
}

/******************************************************************************
 * Device Left Handler
 ******************************************************************************/
static void appDevMan_DeviceLeftHandler(zabService* Service,
                                        szl_uint16 NetworkAddress,
                                        szl_uint64 IeeeAddress)
{
  printApp(Service, "appDevMan_DeviceLeftHandler: NetworkAddress = 0x%04X, IeeeAddress = 0x%016llX\n", NetworkAddress, IeeeAddress);
}

/******************************************************************************
 * Time since last communication exceeds timeout Handler
 ******************************************************************************/
static void appDevMan_TimeSinceLastCommsExceedsThresholdHandler(zabService* Service,
                                                                szl_uint16 NetworkAddress,
                                                                szl_uint64 IeeeAddress,
                                                                szl_uint32 TimeSinceLastComms)
{
  printApp(Service, "appDevMan_TimeSinceLastCommsExceedsThreshold: NetworkAddress = 0x%04X, IeeeAddress = 0x%016llX, Time = %d\n", 
           NetworkAddress, IeeeAddress, TimeSinceLastComms);
}

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise the service
 ******************************************************************************/
SZL_RESULT_t appDevMan_Init(zabService* Service, szl_uint16 MaxSupportedNodes, szl_uint32 CommsTimeThreshold)
{
  SZL_RESULT_t res;
  
  res = SzlPlugin_DevMan_Init(Service,
                              MaxSupportedNodes,
                              (SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS / 2),
                              SZL_PLUGIN_DEV_MAN_INCOMPLETE_DISCOVERY_RETRY_TIME_MIN,
                              SZL_PLUGIN_DEV_MAN_DISCOVERY_REFRESH_TIME_MIN,
                              CommsTimeThreshold,
                              appDevMan_AddressChangedHandler,
                              appDevMan_UntrackedNodeCommsHandler,
                              appDevMan_DeviceLeftHandler,
                              appDevMan_TimeSinceLastCommsExceedsThresholdHandler);
  return res;
}

/******************************************************************************
 * Destroy the service
 ******************************************************************************/
SZL_RESULT_t appDevMan_Destroy(zabService* Service)
{
  return SzlPlugin_DevMan_Destroy(Service);
}

/******************************************************************************
 * Print the Device Manager
 ******************************************************************************/
void appDevMan_Print(zabService* Service)
{
  SZL_RESULT_t res;
  szl_uint16 index = 0;
  szl_uint16 networkAddress;
  szl_uint64 ieeeAddress;
  szl_uint32 timeSinceLastComms;
  
  printApp(Service, "Device Manager Table:\n");
  printApp(Service, "Index  NwkAddr  Ieee              TimeSinceLastComms\n");
  
  do
    {
      res = SzlPlugin_DevMan_GetByIndex(Service, index, &networkAddress, &ieeeAddress, &timeSinceLastComms);
      if ( (res == SZL_RESULT_SUCCESS) && 
           ( (networkAddress != SZL_PLUGIN_DEV_MAN_NETWORK_ADDRESS_UNKNOWN) || (ieeeAddress != SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN) ) )
        {
          printApp(Service, "%03d    %04X     %016llX  %d\n", index, networkAddress, ieeeAddress, timeSinceLastComms);
        }
      index++;
    } while (res == SZL_RESULT_SUCCESS);
  
}
    
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/