
"""
application.application_frame_management
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to treat the received applicative frames (update data model)

"""
from .application_frame import get_address, get_attributes
from json_parser import JsonParser
from .application_frame import Application


class ApplicationFrameManagement(object):

    def __init__(self, application_process):
        """Save application process instance"""
        self._application_process = application_process
        self._json_parser = JsonParser("../topology.json")

    def default_response(self, device_number, payload):

        if (int(payload[0]) & Application.OptionAckMask.SOURCE_ADDRESS_PRESENT) != Application.OptionAddress.PRESENT:
            return

        if (int(payload[0]) & Application.OptionAckMask.PAYLOAD_PRESENT) != Application.OptionPayload.PRESENT:
            return

        result = get_address(payload, 3)
        if self._application_process.zigbee_device_container.is_device_saved(id_wireless_bridge=device_number,
                                                                             address=result["address"]):

            device = self._application_process.zigbee_device_container.get_device(id_wireless_bridge=device_number,
                                                                                  address=result["address"])
            data = get_attributes(result["data"])
            device.update_data(data)
            self._json_parser.update_device(device)

    def update_device_data(self, device_number, payload):
        result = get_address(payload, 1)
        if self._application_process.zigbee_device_container.is_device_saved(id_wireless_bridge=device_number,
                                                                             address=result["address"]):

            device = self._application_process.zigbee_device_container.get_device(id_wireless_bridge=device_number,
                                                                                  address=result["address"])
            data = get_attributes(result["data"])
            device.update_data(data)
            self._json_parser.update_device(device)

    def update_device_link_indicator(self, device_number, payload):
        pass

    def cluster_command(self, device_number, payload):
        pass
