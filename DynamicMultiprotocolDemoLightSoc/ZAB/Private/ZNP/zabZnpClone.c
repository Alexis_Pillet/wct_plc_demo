/******************************************************************************
 *                        ZigBee Application Brick
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the cloning engine for ZNP.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.02.01  29-Nov-13   MvdB   Original
 * 00.00.06.00  11-Sep-14   MvdB   Add NV Change Notification
 * 00.00.07.03  13-Nov-14   MvdB   ARTF107632: Improve notifications when there are failures
 * 01.100.06.00 11-Feb-15   MvdB   ARTF113139: Upgrade ZNP Clone to cover Green Power NV Items
 * 002.000.002  05-Mar-15   MvdB   ARTF104110: Support next work time return from zabCoreWork
 * 002.000.007  14-Apr-15   MvdB   ARTF130628: Replace Clone Timeout with SRSP timeout
 * 002.002.007  28-Sep-15   MvdB   ARTF148934: Clone Get: Support reading of selected items
 * 002.002.010  08-Oct-15   MvdB   Check SELECTIVE_READ_ID_CALLBACK is not null before using in nextReadItem()
 * 002.002.011  12-Oct-15   MvdB   ARTF148934: Upgrade clone get to allow app to read only changed items
 *                                 ARTF104108: Add notification of progress for Clone actions
 *****************************************************************************/

#include "zabZnpService.h"



/******************************************************************************
 *                      *****************************
 *                 *****          CONSTANTS          *****
 *                      *****************************
 ******************************************************************************/



/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/* Configuration struct for the file, used to specify (inclusive) ranges of NV items to be replicated */
typedef struct
{
  unsigned16 startId;
  unsigned16 endId;
}cloneNvBlock_t;


/* Macros for clean access to CloneInfo items */
#define ZAB_SERVICE_ZNP_CLONE( _srv )         (ZAB_SERVICE_VENDOR( (_srv) )->zabCloneInfo)
#define CLONE_ACTION( _srv )                  (ZAB_SERVICE_ZNP_CLONE( (_srv) ).cloneAction)
#define CLONE_BLOCK_INDEX( _srv )             (ZAB_SERVICE_ZNP_CLONE( (_srv) ).cloneBlockIndex)
#define CLONE_ITEM_ID( _srv )                 (ZAB_SERVICE_ZNP_CLONE( (_srv) ).cloneItemId)
#define CURRENT_ITEM_LENGTH( _srv )           (ZAB_SERVICE_ZNP_CLONE( (_srv) ).currentItemLength)
#define CURRENT_ITEM_OFFSET( _srv )           (ZAB_SERVICE_ZNP_CLONE( (_srv) ).currentOffset)
#define STORE_CALLBACK( _srv )                (ZAB_SERVICE_ZNP_CLONE( (_srv) ).DataStoreCallback)
#define RETRIEVE_CALLBACK( _srv )             (ZAB_SERVICE_ZNP_CLONE( (_srv) ).DataRetrieveCallback)
#define CHANGE_NOTIFICATION_CALLBACK( _srv )  (ZAB_SERVICE_ZNP_CLONE( (_srv) ).DataChangeNotificationCallback)

#define CLONE_PROGRESS_COUNT_TOTAL( _srv )    (ZAB_SERVICE_ZNP_CLONE( (_srv) ).progressItemCountTotal)
#define CLONE_PROGRESS_COUNT_CURRENT( _srv )  (ZAB_SERVICE_ZNP_CLONE( (_srv) ).progressItemCountCurrent)

#define SELECTIVE_GET_NUM_IDS_CALLBACK( _srv )  (ZAB_SERVICE_VENDOR( (_srv) )->zabCloneInfo.SelectiveGetNumberOfIdsCallback)
#define SELECTIVE_GET_ID_CALLBACK( _srv )       (ZAB_SERVICE_VENDOR( (_srv) )->zabCloneInfo.SelectiveGetDataIdCallback)

/* Defaults */
#define M_CLONE_BLOCK_INDEX_DISABLED          0xFFFF
#define M_CLONE_ITEM_ID_DISABLED              0xFFFF
#define M_LENGTH_UNKNOWN                      0xFFFF

/******************************************************************************
 *                      *****************************
 *                 *****       LOCAL VARIABLES       *****
 *                      *****************************
 ******************************************************************************/

/* Ranges (inclusive) of NV items to be replicated */
static const cloneNvBlock_t cloneNvBlocks[] =
{
  // WARNING: IF YOU ADD OR CHANGE ANYTHING HERE YOU MUST MAKE THE SAME CHANGE IN M_NUMBER_OF_NV_ID!!!
  {0x0001, 0x0084}, // Normal Pro Items
  // Do not replicate ZCD_NV_LEAVE_CTRL 0x0085
  {0x0086, 0x03FF}, // Normal Pro Items
  {0x0B00, 0x0D20}, // Green Power Items
  // WARNING: IF YOU ADD OR CHANGE ANYTHING HERE YOU MUST MAKE THE SAME CHANGE IN M_NUMBER_OF_NV_ID!!!
};
#define M_NUMBER_OF_NV_BLOCKS (sizeof(cloneNvBlocks) / sizeof(cloneNvBlock_t))

/* Total number of items in a full read. */
#define M_NUMBER_OF_NV_ID  (  \
     (-0x0001 + 0x0084 + 1) /* Normal Pro Items*/   \
   + (-0x0086 + 0x03FF + 1) /* Normal Pro Items*/   \
   + (-0x0B00 + 0x0D20 + 1) /* Green Power Items*/  )

/******************************************************************************
 *                      *****************************
 *                 ***** LOCAL FUNCTION DECLARATIONS *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Read the next chunk of an item at the next offset
 ******************************************************************************/
static void readItemChunk(erStatus* Status, zabService* Service)
{
  zabZnpSysUtility_NvRead(Status, Service, CLONE_ITEM_ID(Service), CURRENT_ITEM_OFFSET(Service));
  if (erStatusIsOk(Status))
    {
      // Set state for SRSP timeout
      CLONE_ACTION(Service) = ZAB_CLONE_ACTION_GET_READ;
    }
  else
    {
      // Tidy up
      sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_READ_FAILED);
      zabZnpClone_Reset(Status, Service);
    }
}

/******************************************************************************
 * Get the length of the next item
 ******************************************************************************/
static void getItemLength(erStatus* Status, zabService* Service)
{
  CURRENT_ITEM_LENGTH(Service) = M_LENGTH_UNKNOWN;
  CURRENT_ITEM_OFFSET(Service) = 0;
  zabZnpSysUtility_NvLength(Status, Service, CLONE_ITEM_ID(Service));
  if (erStatusIsOk(Status))
    {
      // Set state for SRSP timeout
      CLONE_ACTION(Service) = ZAB_CLONE_ACTION_GET_LENGTH;
    }
  else
    {
      // Tidy up
      sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_READ_FAILED);
      zabZnpClone_Reset(Status, Service);
    }
}

/******************************************************************************
 * Write the next chunk of an item at the next offset
 ******************************************************************************/
#define M_MAX_WRITE_LENGTH 246
static void writeItemChunk(erStatus* Status, zabService* Service)
{
  unsigned8 write_length = 0;
  unsigned16 item_length = 0;
  unsigned16 item_offset;
  unsigned8 Data[unsigned8_max];
  
  item_offset = CURRENT_ITEM_OFFSET(Service);
  
  if (RETRIEVE_CALLBACK(Service) != NULL)
    {
      RETRIEVE_CALLBACK(Service)(Service, CLONE_ITEM_ID(Service), &item_length, item_offset, &write_length, Data);
    }
  CURRENT_ITEM_LENGTH(Service) = item_length;
  
  if (write_length > 0)
    {
      
      if (write_length > M_MAX_WRITE_LENGTH)
        {
          write_length = M_MAX_WRITE_LENGTH;
        }
      
      zabZnpSysUtility_NvWrite(Status, 
                               Service, 
                               CLONE_ITEM_ID(Service),
                               item_offset,
                               write_length,
                               Data);
        
      if (erStatusIsOk(Status))
        {
          CURRENT_ITEM_OFFSET(Service) += write_length;
          
          // Set state for SRSP timeout
          CLONE_ACTION(Service) = ZAB_CLONE_ACTION_SET_WRITE;
        }
      else
        {
          // Tidy up
          sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_WRITE_FAILED);
          zabZnpClone_Reset(Status, Service);
        }
    }
  else
    {
      // Tidy up
      sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_WRITE_FAILED);
      zabZnpClone_Reset(Status, Service);
    }
}

/******************************************************************************
 * Start writing the next item
 ******************************************************************************/
static void nextWriteItem(erStatus* Status, zabService* Service)
{
  unsigned8 write_length = 0;
  unsigned16 item_length = 0;
  unsigned16 item_offset;
  unsigned8 Data[unsigned8_max];
  unsigned32 progress;
  
  // Increment. Skip items that have length 0.
  CURRENT_ITEM_OFFSET(Service) = 0;
  item_offset = CURRENT_ITEM_OFFSET(Service);
  do
    {
      CLONE_ITEM_ID(Service)++;
      CLONE_PROGRESS_COUNT_CURRENT(Service)++;      
      
      /* This is quite inefficient using the get data callback just to get the length 
       * That said, its not that bad is this is a once in a lifetime operation!*/
      if (RETRIEVE_CALLBACK(Service) != NULL)
        {
          RETRIEVE_CALLBACK(Service)(Service, CLONE_ITEM_ID(Service), &item_length, item_offset, &write_length, Data);
        }
    }
  while ( (CLONE_ITEM_ID(Service) <= cloneNvBlocks[CLONE_BLOCK_INDEX(Service)].endId)
          &&
          (item_length == 0) );

  
  if ( (CLONE_ITEM_ID(Service) > cloneNvBlocks[CLONE_BLOCK_INDEX(Service)].endId) ||
       ((CLONE_ITEM_ID(Service) == cloneNvBlocks[CLONE_BLOCK_INDEX(Service)].endId) && (item_length == 0)) )
    {
      /* Finished Block! */
      CLONE_BLOCK_INDEX(Service)++;
      if (CLONE_BLOCK_INDEX(Service) >= M_NUMBER_OF_NV_BLOCKS)
        {
          /* Finished all blocks! */
          sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_WRITE_COMPLETE_SUCCESS);
          zabZnpClone_Reset(Status, Service);
          return;
        }
      else
        {
          CLONE_ITEM_ID(Service) = cloneNvBlocks[CLONE_BLOCK_INDEX(Service)].startId;
          writeItemChunk(Status, Service);
        }
    }
  else
    {
      writeItemChunk(Status, Service);
    }
  progress = (CLONE_PROGRESS_COUNT_CURRENT(Service) << 8) / CLONE_PROGRESS_COUNT_TOTAL(Service);
  sapMsgPutNotifyCloneProgress(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, (unsigned8)progress);
}


/******************************************************************************
 * Start reading the next item
 ******************************************************************************/
static void nextReadItem(erStatus* Status, zabService* Service)
{
  zab_bool finished = zab_false;
  unsigned32 progress;
  
  /* We are reading all, so move to next item in block */
  if (CLONE_PROGRESS_COUNT_TOTAL(Service) >= M_NUMBER_OF_NV_ID)
    {
      CLONE_ITEM_ID(Service)++;
      CLONE_PROGRESS_COUNT_CURRENT(Service)++;
      
      if (CLONE_ITEM_ID(Service) > cloneNvBlocks[CLONE_BLOCK_INDEX(Service)].endId)
        {
          /* Finished Block! */
          CLONE_BLOCK_INDEX(Service)++;
          if (CLONE_BLOCK_INDEX(Service) >= M_NUMBER_OF_NV_BLOCKS)
            {
              /* Finished All Blocks! */
              finished = zab_true;
            }
          else
            {              
              CLONE_ITEM_ID(Service) = cloneNvBlocks[CLONE_BLOCK_INDEX(Service)].startId;
            }
        }      
    }
  /* We are doing selective reads, so get next one from the app */
  else
    {    
      if (SELECTIVE_GET_ID_CALLBACK(Service) != NULL)
        {
          CLONE_PROGRESS_COUNT_CURRENT(Service)++;
          if (CLONE_PROGRESS_COUNT_CURRENT(Service) >= CLONE_PROGRESS_COUNT_TOTAL(Service))
            {
              /* Finished All App items! */
              finished = zab_true;
            }
          else
            {
              CLONE_ITEM_ID(Service) = SELECTIVE_GET_ID_CALLBACK(Service)(Service, CLONE_PROGRESS_COUNT_CURRENT(Service));
              if (CLONE_ITEM_ID(Service) == ZAB_ZNP_CLONE_M_ID_ALL)
                {
                  sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_READ_FAILED);
                  zabZnpClone_Reset(Status, Service);
                  return;
                }
            }          
        }
      else
        {
          sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_READ_FAILED);
          zabZnpClone_Reset(Status, Service);
          return;
        }
    }
  
  /* If we are finished, then notify the states. If not proceed with new item */
  if (finished == zab_true)
    {
      sapMsgPutNotifyCloneProgress(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, 0xFF);
      sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_READ_COMPLETE_SUCCESS);
      zabZnpClone_Reset(Status, Service);
    }
  else
    {
      /* Notify progress */
      progress = (CLONE_PROGRESS_COUNT_CURRENT(Service) << 8) / CLONE_PROGRESS_COUNT_TOTAL(Service);
      sapMsgPutNotifyCloneProgress(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, (unsigned8)progress);
      
      getItemLength(Status, Service);
    }
}


/******************************************************************************
 * Run the state machine
 ******************************************************************************/
static void churnStateMachine(erStatus* Status, zabService* Service)
{
  unsigned16 localNumItems;
  unsigned16 localItemId;
  
  /* Kick off get machine for the first time */
  if (CLONE_ACTION(Service) == ZAB_CLONE_ACTION_GET_START)
    {
      /* By default, set up for normal (non selective get */
      CLONE_BLOCK_INDEX(Service) = 0;
      CLONE_ITEM_ID(Service) = cloneNvBlocks[CLONE_BLOCK_INDEX(Service)].startId;          
      CLONE_PROGRESS_COUNT_TOTAL(Service) = M_NUMBER_OF_NV_ID;
      CLONE_PROGRESS_COUNT_CURRENT(Service) = 0;
    
      /* See if the app wants to read selected IDs only.
       *  - Get number of items to read and confirm it is within valid range.
       *  - Get the first item ID and confirm it is not ALL.
       * If anything goes wrong stick with a full read  */    
      if ( (SELECTIVE_GET_NUM_IDS_CALLBACK(Service) != NULL) &&
           (SELECTIVE_GET_ID_CALLBACK(Service) != NULL) )
        {
          localNumItems = SELECTIVE_GET_NUM_IDS_CALLBACK(Service)(Service);
          
          /* If the app asked for a valid number of IDs, setup for app controlled read */
          if ( (localNumItems > 0) &&
               (localNumItems < M_NUMBER_OF_NV_ID) )
            {
              localItemId = SELECTIVE_GET_ID_CALLBACK(Service)(Service, 0);
              
              if (localItemId != ZAB_ZNP_CLONE_M_ID_ALL)
                {
                  CLONE_PROGRESS_COUNT_TOTAL(Service) = localNumItems;
                  CLONE_ITEM_ID(Service) = localItemId;
                }
            }
        }

      /* Go */
      getItemLength(Status, Service);
    }
  
  /* Kick off set machine for the first time */
  else if (CLONE_ACTION(Service) == ZAB_CLONE_ACTION_SET_START)
    {
      CLONE_BLOCK_INDEX(Service) = 0;
      CLONE_ITEM_ID(Service) = cloneNvBlocks[CLONE_BLOCK_INDEX(Service)].startId;
      CURRENT_ITEM_OFFSET(Service) = 0;
      
      CLONE_PROGRESS_COUNT_TOTAL(Service) = M_NUMBER_OF_NV_ID;
      CLONE_PROGRESS_COUNT_CURRENT(Service) = 0;
      
      writeItemChunk(Status, Service);
    }
  
  
  /* 
   * GET STATE MACHINE
   */

  /* If we just got the length, go on to reading data */
  else if (CLONE_ACTION(Service) == ZAB_CLONE_ACTION_GET_LENGTH)
    {
      if (CURRENT_ITEM_LENGTH(Service) == 0)
        {
         // Skip to the next item
          nextReadItem(Status, Service);
        }
      else if (CURRENT_ITEM_LENGTH(Service) != M_LENGTH_UNKNOWN)
        {
          readItemChunk(Status, Service);
        }
      else
        {
          sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_READ_FAILED);
          zabZnpClone_Reset(Status, Service);
        }
    }
      
  /* We are reading. Either read the next chunk of a long item or move onto the next item */
  else if (CLONE_ACTION(Service) == ZAB_CLONE_ACTION_GET_READ)
    {
      /* If the read is not complete, get the next chunk */
      if ( CURRENT_ITEM_OFFSET(Service) < CURRENT_ITEM_LENGTH(Service))
        {
          readItemChunk(Status, Service);
        }
      else
        {
          // Next
          nextReadItem(Status, Service);
        }
    }
      
      
  /* 
   * SET STATE MACHINE
   */
  else if (CLONE_ACTION(Service) == ZAB_CLONE_ACTION_SET_WRITE)
    {
      /* If the read is not complete, get the next chunk */
      if ( CURRENT_ITEM_OFFSET(Service) < CURRENT_ITEM_LENGTH(Service))
        {
          writeItemChunk(Status, Service);
        }
      else
        {
          // Next
          nextWriteItem(Status, Service);
        }
    }
}


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/


/******************************************************************************
 * Register Clone Data Store Callback
 * 
 * This callback will be called with data from the network processor after a 
 * ZAB_ACTION_CLONE_GET, or on change of data if a callback is registered.
 * 
 * Applications that do not support cloning should leave this callback unregistered.
 ******************************************************************************/
void zabZnpClone_RegisterDataStoreCallback(zabService* Service, zabZnpClone_CB_DataStore_t Callback)
{
  STORE_CALLBACK(Service) = Callback;
}

/******************************************************************************
 * Register Clone Data Retrieve Callback
 * 
 * This callback will be called to request stored data to be written back into
 * a network processor after a ZAB_ACTION_CLONE_SET.
 * 
 * Applications that do not support cloning should leave this callback unregistered.
 ******************************************************************************/
void zabZnpClone_RegisterDataRetrieveCallback(zabService* Service, zabZnpClone_CB_DataRetrieve_t Callback)
{
  RETRIEVE_CALLBACK(Service) = Callback;
}

/******************************************************************************
 * Register Clone Data Change Notification Callback
 * 
 * This callback will be called to notify the application that NV item(s) have changed.
 * Data should then be read again to update the backup.
 * 
 * Applications that do not support cloning should leave this callback unregistered.
 ******************************************************************************/
void zabZnpClone_RegisterChangeNotificationCallback(zabService* Service, 
                                                    zabZnpClone_CB_DataChangeNotification_t Callback)
{
  CHANGE_NOTIFICATION_CALLBACK(Service) = Callback;
}

/******************************************************************************
 * Register Selective Get Callback
 * 
 * This callback will be called to ask the application if it wishes to specify
 * which items are read during a ZAB_ACTION_CLONE_GET.
 * Application may:
 *  - Not register this callback, in which case the action will get all items.
 *  - Register the callback and:
 *   - Return ZAB_ZNP_CLONE_M_ID_ALL for all items
 *   - Individual item IDs if only selected items are required.
 *     - Typically this list should be as supplied by zabZnpClone_CB_DataChangeNotification_t
 * 
 * Applications that do not support cloning should leave this callback unregistered.
 ******************************************************************************/
void zabZnpClone_RegisterSelectiveGetCallback(zabService* Service, 
                                              zabZnpClone_CB_SelectiveGetNumberOfIds_t NumberOfIdsCallback,
                                              zabZnpClone_CB_SelectiveGetDataId_t DataIdCallback)
{
  SELECTIVE_GET_NUM_IDS_CALLBACK(Service) = NumberOfIdsCallback;
  SELECTIVE_GET_ID_CALLBACK(Service) = DataIdCallback;
}

/******************************************************************************
 * Create
 ******************************************************************************/
void zabZnpClone_create(erStatus* Status, zabService* Service)
{
  zabZnpClone_Reset(Status, Service);
  STORE_CALLBACK(Service) = NULL;
  RETRIEVE_CALLBACK(Service) = NULL;
  CHANGE_NOTIFICATION_CALLBACK(Service) = NULL;
  SELECTIVE_GET_NUM_IDS_CALLBACK(Service) = NULL;
  SELECTIVE_GET_ID_CALLBACK(Service) = NULL;
}


/******************************************************************************
 * Reset the service
 ******************************************************************************/
void zabZnpClone_Reset(erStatus* Status, zabService* Service)
{
  erStatus localStatus;
  erStatusClear(&localStatus, Service);
  
  CLONE_ACTION(Service) = ZAB_CLONE_ACTION_NONE;
  CLONE_BLOCK_INDEX(Service) = M_CLONE_BLOCK_INDEX_DISABLED;
  CLONE_ITEM_ID(Service) = M_CLONE_ITEM_ID_DISABLED;
  CURRENT_ITEM_LENGTH(Service) = M_LENGTH_UNKNOWN;
  sapMsgPutNotifyCloneState(&localStatus, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_NONE);
  sapMsgPutNotifyCloneProgress(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, 0);
}

/******************************************************************************
 * Action
 ******************************************************************************/
void zabZnpClone_action(erStatus* Status, zabService* Service, unsigned8 Action)
{
  ER_CHECK_STATUS(Status);
  
  /* Common error checking for clone actions first */
  if ( (Action == ZAB_ACTION_CLONE_GET) ||
       (Action == ZAB_ACTION_CLONE_SET) )
    {
      if (CLONE_ACTION(Service) != ZAB_CLONE_ACTION_NONE)
        {
          printError(Service, "zabZnpClone: ERROR: Cloning action in progress\n");
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_INVALID_CLONE_STATE);
          return;
        }
      if (zabZnpOpen_GetOpenState(Service) != ZAB_OPEN_STATE_OPENED)
        {
          printError(Service, "zabZnpClone: ERROR: ZAB not opened\n");
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_VENDOR_INVALID_OPEN_STATE);
          return;
        }
    }
  
  if ( (Action == ZAB_ACTION_CLONE_GET) )
    {
      if (STORE_CALLBACK(Service) == NULL)
        {
          printError(Service, "zabZnpClone: ERROR: Get but no store callback registered\n");
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_CLONE_STORE_CALLBACK_INVALID);
          return;
        }
      
      printVendor(Service, "zabZnpClone: Clone Get\n");
      sapMsgPutNotifyCloneProgress(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, 0);
      sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_READING);
      CLONE_ACTION(Service) = ZAB_CLONE_ACTION_GET_START;
      churnStateMachine(Status, Service);
    }
  else if ( (Action == ZAB_ACTION_CLONE_SET) )
    {
      if (RETRIEVE_CALLBACK(Service) == NULL)
        {
          printError(Service, "zabZnpClone: ERROR: Set but no retrieve callback registered\n");
          sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_CLONE_RETRIEVE_CALLBACK_INVALID);
          return;
        }
      
      printVendor(Service, "zabZnpClone: Clone Set\n");
      sapMsgPutNotifyCloneProgress(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, 0);
      sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_WRITING);
      CLONE_ACTION(Service) = ZAB_CLONE_ACTION_SET_START;
      churnStateMachine(Status, Service);
    }
}


/******************************************************************************
 * Handle SRSP timeouts
 ******************************************************************************/
void zabZnpClone_srspTimeoutHandler(erStatus* Status, zabService* Service)
{
  switch (CLONE_ACTION(Service))
    {
      case ZAB_CLONE_ACTION_GET_START:
      case ZAB_CLONE_ACTION_GET_LENGTH:
      case ZAB_CLONE_ACTION_GET_READ:
        sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_CLONE_COMMAND_TIMEOUT);
        sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_READ_FAILED);
        zabZnpClone_Reset(Status, Service);
        break;

      case ZAB_CLONE_ACTION_SET_START:
      case ZAB_CLONE_ACTION_SET_WRITE:
        sapMsgPutNotifyError(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_ERROR_CLONE_COMMAND_TIMEOUT);
        sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_WRITE_FAILED);
        zabZnpClone_Reset(Status, Service);
        break;

      case ZAB_CLONE_ACTION_NONE:
      default:
        break;
    }                    
}

/******************************************************************************
 * NV Length Response Handler
 ******************************************************************************/
void zabZnpClone_NvLengthResponseHandler(erStatus* Status, zabService* Service, unsigned16 Length)
{
  /* If we are discovering (we caused this response) then go to the next one */
  if (CLONE_ACTION(Service) == ZAB_CLONE_ACTION_GET_LENGTH)
    {
      CURRENT_ITEM_LENGTH(Service) = Length;
      churnStateMachine(Status, Service);
    }
}

/******************************************************************************
 * NV Read Response Handler
 ******************************************************************************/
void zabZnpClone_NvReadResponseHandler(erStatus* Status, 
                                       zabService* Service, 
                                       ZNPI_API_ERROR_CODES znpStatus, 
                                       unsigned8 Length, 
                                       unsigned8* DataPointer)
{
  /* If we are discovering (we caused this response) then go to the next one */
  if (CLONE_ACTION(Service) == ZAB_CLONE_ACTION_GET_READ)
    {
      if (Length > 0)
        {
          if (STORE_CALLBACK(Service) != NULL)
            {
              STORE_CALLBACK(Service)(Service,
                                      CLONE_ITEM_ID(Service), 
                                      CURRENT_ITEM_LENGTH(Service), 
                                      CURRENT_ITEM_OFFSET(Service), 
                                      Length, 
                                      DataPointer);
            }

          CURRENT_ITEM_OFFSET(Service) += Length;
          churnStateMachine(Status, Service);
        }
      else
        {
          sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_READ_FAILED);
          zabZnpClone_Reset(Status, Service);
        }
    }
}

/******************************************************************************
 * NV Write Response Handler
 ******************************************************************************/
void zabZnpClone_NvWriteResponseHandler(erStatus* Status, 
                                       zabService* Service, 
                                       ZNPI_API_ERROR_CODES znpStatus)
{  
  /* If we are writing (we caused this response) then go to the next one */
  if (CLONE_ACTION(Service) == ZAB_CLONE_ACTION_SET_WRITE)
    {
      if (znpStatus == ZNPI_API_ERR_SUCCESS)
        { 
          churnStateMachine(Status, Service);
        }
      else
        {
          printError(Service, "zabZnpClone: ERROR: NV Write Rsp: ERROR: Write Failed\n");
          sapMsgPutNotifyCloneState(Status, zabCoreSapManage(Service), SAP_MSG_DIRECTION_IN, ZAB_CLONE_STATE_WRITE_FAILED);
          zabZnpClone_Reset(Status, Service);
        }
    }
}

/******************************************************************************
 * NV Change Notification Handler
 ******************************************************************************/
void zabZnpClone_NvChangeNotificationHandler(erStatus* Status, 
                                             zabService* Service, 
                                             unsigned8 NumberOfIds,
                                             unsigned16* CloneDataIds)
{  
  if ( (CHANGE_NOTIFICATION_CALLBACK(Service) != NULL) &&
       (NumberOfIds > 0) &&
       (CloneDataIds != NULL) )
    {
      CHANGE_NOTIFICATION_CALLBACK(Service)(Service, NumberOfIds, CloneDataIds);
    }
}


/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/