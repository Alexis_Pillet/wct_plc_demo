/******************************************************************************
 *                          ZAB TEST APPLICATION
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the Basic Cluster ZCL handlers for the test app.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 01.100.07.00 13-Feb-15   MvdB   Split out from appZcl, which was getting too big
 *                                 artf108759: Szl_DataPointReadNative (and similar) should include maxDataLength
 * 002.002.047  31-Jan-17   MvdB   Add appZcl_SzlBasicPlugin_SetProductIdentifier()
 *****************************************************************************/

#ifndef _APP_ZCL_BASIC_H_
#define _APP_ZCL_BASIC_H_

#include "zabCoreService.h"

#include "szl_zab_types.h"

#include "basic_cluster.h"

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/
/******************************************************************************
 * Nova Online/Offline Notification Handler
 ******************************************************************************/
extern
szl_bool appZclBasic_NovaOnlineNtfHandler(zabService* Service,
                                          SZL_EP_t DestinationEndpoint,
                                          szl_bool ManufacturerSpecific,
                                          szl_uint16 ClusterID,
                                          szl_uint8 CmdID,
                                          szl_uint8* ZCLPayloadIn, szl_uint8 PayloadInSize,
                                          szl_uint8* CmdOutID,
                                          szl_uint8* ZCLPayloadOut, szl_uint8* PayloadOutSize,
                                          szl_uint8 TransactionId,
                                          SZL_Addresses_t SourceAddress,
                                          SZL_ADDRESS_MODE_t DestAddressMode);

/******************************************************************************
 * Basic Cluster - Increment Values for an endpoint
 ******************************************************************************/
extern
SZL_RESULT_t appZcl_SzlBasicPlugin_IncValues(szl_uint8 Endpoint);


/******************************************************************************
 * Basic Cluster - Set product identifier for an endpoint
 ******************************************************************************/
extern
SZL_RESULT_t appZcl_SzlBasicPlugin_SetProductIdentifier(szl_uint8 Endpoint, szl_uint16 ProductIdentifier);

#ifndef BASIC_M_USE_DIRECT_ACCESS
/******************************************************************************
 * Basic Cluster - Initialise endpoints in the application
 ******************************************************************************/
extern
SZL_RESULT_t appZcl_SzlBasicPlugin_InitEndpoints(szl_uint8 NumEndpoints, szl_uint8* EndpointList);

/******************************************************************************
 * Basic Cluster Read Attribute Handler
 ******************************************************************************/
extern
SZL_RESULT_t appZcl_SzlBasicPlugin_ReadAttrHandler(zabService* Service,
                                                      SZL_EP_t Endpoint,
                                                      szl_bool ManufacturerSpecific, szl_uint16 ClusterID,
                                                      szl_uint16 AttributeID,
                                                      void* Data, szl_uint8 *DataLength);

/******************************************************************************
 * Basic Cluster Write Attribute Handler
 ******************************************************************************/
extern
SZL_RESULT_t appZcl_SzlBasicPlugin_WriteAttrHandler(zabService* Service,
                                                       SZL_EP_t Endpoint,
                                                       szl_bool ManufacturerSpecific, szl_uint16 ClusterID,
                                                       szl_uint16 AttributeID,
                                                       void* Data, szl_uint8 DataLength);
#endif

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif /* _APP_ZCL_BASIC_H_ */
