/* 
  Name:    Safe Ring Buffer Interface
  Author:  ZigBee Excellence Center
  Company: Schneider Electric

  Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

  Description:
    Ring buffer with identifier passed to protection macros. 

    Assumes one producer and one consumer.
*/

#ifndef __RB_SAFE_INTERFACE_H__
#define __RB_SAFE_INTERFACE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "osTypes.h"
#include "zabVendorConfigure.h"


/******************************************************************************
 * Configuration (Inherited)
 ******************************************************************************/ 
#define RB_SAFE_MAX            (ZAB_VENDOR_MAX_DATA)
#define RB_SAFE_ITEM_TYPE      unsigned8                
#define RB_SAFE_ITEM_NULL      0xFF                     // no items left


typedef struct
{
  unsigned8 First;
  unsigned8 Last;
  unsigned8 Count;
  unsigned8 Id;
  RB_SAFE_ITEM_TYPE Buffer[ RB_SAFE_MAX ];
} rbSafeType;


void rbSafeInitialize( rbSafeType* RB, unsigned8 Id );         // initialize and identify
void rbSafeClear( rbSafeType* RB );                               // clears (reinitialize with same Id)

RB_SAFE_ITEM_TYPE rbSafeAppend( rbSafeType* RB, RB_SAFE_ITEM_TYPE Item ); // append to back (return null if no room)
RB_SAFE_ITEM_TYPE rbSafeRemove( rbSafeType* RB );                         // remove from front (return null if empty)

zab_bool rbSafeFull( rbSafeType* RB );
zab_bool rbSafeEmpty( rbSafeType* RB );

unsigned8 rbSafeCount( rbSafeType* RB ); // count of items
unsigned8 rbSafeId( rbSafeType* RB );    // queue Id from initialize

#ifdef __cplusplus
}
#endif

#endif

