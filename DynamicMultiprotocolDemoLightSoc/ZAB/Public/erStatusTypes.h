/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file provides types for status in ZAB.
 *
 *   Almost every ZAB function has as a first parameter type called Status.
 *   This is similar to a function return code value, but much more comprehensive
 *   for error detection and recovery.
 *   Once Status is set with an error, it can not be overwritten with another error
 *   until it is cleared. All other processing shall be suspended until the error
 *   is processed and cleared. The service code always checks the status at function
 *   entry and if there is an error, it will return immediately.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Cam Williams
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.00.02  30-Oct-13   MvdB   Major tidy up
 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 00.00.07.03  13-Nov-14   MvdB   ARTF107632: Add error codes for clone failures
 * 002.000.001  03-Feb-15   MvdB   ARTF104099: Add service pointer to status for print function for multi instance apps.
 * 002.001.004  21-Jul-15   MvdB   ARTF134686: Add SZL_ERROR_PLUGIN_NOT_UNREGISTERED
 * 002.002.020  18-Mar-16   MvdB   Remove unused SZL_ERROR_UNKNOWN_DATA_TYPE
 * 002.002.038  12-Jan-17   MvdB   ARTF113872: Add ZAB_ERROR_DEVICE_TYPE_INVALID
 * 002.002.045  26-Jan-17   MvdB   Error tidy up: Remove unused errors, update descriptions, merge silabs and ti errors into common vendor codes, remove old options in erStatus
 *****************************************************************************/



#ifndef __ER_STATUS_TYPES_H__
#define __ER_STATUS_TYPES_H__


#include "erStatusConfigure.h"
#include "osTypes.h"

/* Structure to define an Error*/
typedef struct
{
  unsigned16 Error;       // This should really be a zabError type
  void* Service;          // zabService the error is used for
} erStatus;



#define ER_ERROR_BASE   0x0000 // error with errors
#define OS_ERROR_BASE   0x0100 // OpSys errors
#define SRV_ERROR_BASE  0x0200 // Service Errors
#define SAP_ERROR_BASE  0x0300 // Service Access Point
#define ZAB_ERROR_BASE  0x0400 // ZigBee Application Brick errors
#define SZL_ERROR_BASE  0x0700 // Schneider ZigBee Library

typedef enum
{
  ZAB_SUCCESS                                     = 0x0000,

  /*** General Errors ***/
  /* A message has exceeded its allocated or maximum data length*/
  ER_ERROR_BUFFER_OVERFLOW                        = ER_ERROR_BASE + 0x02,

  /*** OS Errors ***/
  /* Memory allocation failed (out of memory)*/
  OS_ERROR_MEM_NONE                               = OS_ERROR_BASE + 0x01,
  /* Memory operation (free, compare, set etc) failed due to invalid pointer */
  OS_ERROR_MEM_INVALID                            = OS_ERROR_BASE + 0x02,

  /*** Service Errors ***/
  /* Service failed to get asked item, either due to not having a callback or the callback returning invalid data */
  SRV_ERROR_ASK_NO_ITEM                           = SRV_ERROR_BASE + 0x01,

  /*** SAP Errors ***/
  /* A NULL pointer has been detected */
  SAP_ERROR_NULL_PTR                              = SAP_ERROR_BASE + 0x00,
  /* Insufficient sapAccessStruct - Attempted to register more callbacks than allocated for */
  SAP_ERROR_ACCESS_OVERFLOW                       = SAP_ERROR_BASE + 0x01,
  /* Attempted to get an item from a queue but queue is empty */
  SAP_ERROR_MSG_NOT_AVAILABLE                     = SAP_ERROR_BASE + 0x02,
  /* More the 255 services tried to register for this message */
  SAP_ERROR_MSG_USE_OVERFLOW                      = SAP_ERROR_BASE + 0x04,
  /* More the services tried to release this message than registered for it */
  SAP_ERROR_MSG_USE_UNDERFLOW                     = SAP_ERROR_BASE + 0x05,
  /* Invalid message data length */
  SAP_ERROR_MSG_INVALID_LENGTH                    = SAP_ERROR_BASE + 0x09,

  /*** ZAB Errors ***/
  /* Error in config or asked value */
  ZAB_ERROR_CFG_INVALID                           = ZAB_ERROR_BASE + 0x02,
  /* Invalid action */
  ZAB_ERROR_ACTION_INVALID                        = ZAB_ERROR_BASE + 0x0C,
  /* Ask returned an invalid value */
  ZAB_ERROR_ASKED_VALUE_INVALID                   = ZAB_ERROR_BASE + 0x0E,

  /*** Message Errors ***/
  /* Internal message type invalid */
  ZAB_ERROR_MSG_TYPE_INVALID                      = ZAB_ERROR_BASE + 0x10,
  /* Internal message application invalid */
  ZAB_ERROR_MSG_APP_INVALID                       = ZAB_ERROR_BASE + 0x11,
  /* Internal message parameter invalid */
  ZAB_ERROR_MSG_PARAM_INVALID                     = ZAB_ERROR_BASE + 0x12,

  /*** Vendor Layer Errors ***/
  /* Serial port access failed */
  ZAB_ERROR_VENDOR_NULL                           = ZAB_ERROR_BASE + 0x20,
  /* Error sending a vendor message to the network processor.
   * For example: serial glue failed to send, or serial ack indicated failure. */
  ZAB_ERROR_VENDOR_SENDING                        = ZAB_ERROR_BASE + 0x24,
  /* Open State is invalid for the requested operation */
  ZAB_ERROR_VENDOR_INVALID_OPEN_STATE             = ZAB_ERROR_BASE + 0x25,
  /* Network State is invalid for the requested operation */
  ZAB_ERROR_INVALID_NETWORK_STATE                 = ZAB_ERROR_BASE + 0x26,
  /* Network action already in progress. New action will be dropped. */
  ZAB_ERROR_VENDOR_NETWORK_ACTION_IN_PROGRESS     = ZAB_ERROR_BASE + 0x27,
  /* Failed to erase network settings */
  ZAB_ERROR_VENDOR_ERASE_NETWORK_SETTINGS_FAILED  = ZAB_ERROR_BASE + 0x29,
  /* Failed to form network */
  ZAB_ERROR_VENDOR_FORM_NETWORK_FAILED            = ZAB_ERROR_BASE + 0x2A,
  /* Failed to join network */
  ZAB_ERROR_VENDOR_JOIN_NETWORK_FAILED            = ZAB_ERROR_BASE + 0x2B,
  /* Failed to change to the bootloader */
  ZAB_ERROR_VENDOR_GO_TO_BOOTLOADER_FAILED        = ZAB_ERROR_BASE + 0x2D,
  /* The layer vendor cannot recognise the network processor */
  ZAB_ERROR_VENDOR_UNKNOWN_NETWORK_PROCESSOR      = ZAB_ERROR_BASE + 0x2E,
  /* Invalid clone state */
  ZAB_ERROR_VENDOR_INVALID_CLONE_STATE            = ZAB_ERROR_BASE + 0x2F,

  /*** ZigBee Message Errors ***/
  /* ZigBee payload exceeded max length */
  ZAB_ERROR_MSG_APS_INVALID                       = ZAB_ERROR_BASE + 0x40,

  /* Action failed. ZAB_ERROR_OPERATION_FAILED has been merged into this more meaningful error code */
  ZAB_ERROR_ACTION_FAILED                         = ZAB_ERROR_BASE + 0x46,

  /*** More Vendor Layer Errors ***/
  /* Message incorrectly formatted for vendor. Will be dropped. */
  ZAB_ERROR_VENDOR_MSG_INVALID                    = ZAB_ERROR_BASE + 0x50, // "Vendor message not property formed"
  /* Vendor serial link is busy (locked).
   * This should not happen as ZAB manages the link. If it does happen either:
   *  - There is a bug in ZAB (typically a vendor state machine sending an a-synchronous command).
   *  - Illegal vednor layer private API's being accessed.  */
  ZAB_ERROR_VENDOR_BUSY                           = ZAB_ERROR_BASE + 0x51,
  /* Timeout in vendor/network processor communications.
   * Network processor never acked a serial command (with ZAB_VENDOR_SERIAL_RETRIES).
   * This indicates a serious issue, most likely either:
   *  - The network processor or glue is broken
   *  - Network processor is not in the mode we expect (bootloader/zigbee app).
   * Internal handling:
   *  - For application commands the error will be returned, but no action taken (decision left to application).
   *  - For commands within an action the action will fail and revert to its default state.
   *  - For internal network processor pings (if enabled), ZAB will be closed. */
  ZAB_ERROR_VENDOR_TIMEOUT                        = ZAB_ERROR_BASE + 0x52,
  /* Invalid Vendor message type. Will be dropped. */
  ZAB_ERROR_VENDOR_TYPE_INVALID                   = ZAB_ERROR_BASE + 0x53,
  /* Invalid Vendor request. Will be dropped. */
  ZAB_ERROR_VENDOR_REQ_INVALID                    = ZAB_ERROR_BASE + 0x54,
  /* Error in parsing Vendor message. Will be dropped. */
  ZAB_ERROR_VENDOR_PARSE                          = ZAB_ERROR_BASE + 0x56,
  /* Data confirm reported an error, so message was not successfully sent on network  */
  ZAB_ERROR_VENDOR_DATA_CONFIRM_ERROR             = ZAB_ERROR_BASE + 0x58,
  /* Network processor reset  */
  ZAB_ERROR_VENDOR_NWK_PROC_RESET                 = ZAB_ERROR_BASE + 0x59,

  /*** Serial Glue Errors Errors ***/
  /* Open or CLsoe of serial glue timed out */
  ZAB_ERROR_SERIAL_GLUE_ACTION_TIMEOUT            = ZAB_ERROR_BASE + 0x80,

  /*** Bootloader Errors ***/
  /* Firmware update: Verify failed */
  ZAB_ERROR_BOOTLOADER_VERIFY_FAILED              = ZAB_ERROR_BASE + 0x90,
  /* Firmware update: Write failed */
  ZAB_ERROR_BOOTLOADER_WRITE_FAILED               = ZAB_ERROR_BASE + 0x91,
  /* Firmware update: Image invalid" */
  ZAB_ERROR_BOOTLOADER_IMAGE_INVALID              = ZAB_ERROR_BASE + 0x92,
  /* Firmware update: Action already in progress */
  ZAB_ERROR_BOOTLOADER_ACTION_IN_PROGRESS         = ZAB_ERROR_BASE + 0x93,

  /*** Action Errors ***/
  /* Tried to change key, but we have reached the maximum key sequence number */
  ZAB_ERROR_ACTION_KEY_SEQ_NUMBER_INVALID         = ZAB_ERROR_BASE + 0xA1,

  /*** Clone Errors ***/
  /* Clone: Store callback invalid */
  ZAB_ERROR_CLONE_STORE_CALLBACK_INVALID          = ZAB_ERROR_BASE + 0xB0,
  /* Clone: Retrieve callback invalid */
  ZAB_ERROR_CLONE_RETRIEVE_CALLBACK_INVALID       = ZAB_ERROR_BASE + 0xB1,
  /* Clone: Command timeout */
  ZAB_ERROR_CLONE_COMMAND_TIMEOUT                 = ZAB_ERROR_BASE + 0xB2,

  /*** SZL Errors ***/
  /* An SZL plugin was not unregistered before destroy. */
  SZL_ERROR_PLUGIN_NOT_UNREGISTERED               = SZL_ERROR_BASE + 0x03,
} zabError;


#endif /* __ER_STATUS_TYPES_H__ */
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
