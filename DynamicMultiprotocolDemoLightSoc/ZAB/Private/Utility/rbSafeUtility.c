/*
 Name:    Safe Ring Buffer Implementation
 Author:  ZigBee Excellence Center
 Company: Schneider Electric

 Copyright (c) 2011-2012 by Schneider Electric, all rights reserved

 Description:

 Light weight ring buffer. Assumes one producer and one consumer.

 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
*/

#include "osTypes.h"
#include "zabCoreConfigure.h"
#include "zabMutexConfigure.h"
#include "rbSafeUtility.h"


/*******************************************************************************
 *
 *                     !!!!!!!!!!  WARNING  !!!!!!!!!!
 *
 * This file does not correctly use service pointer in RB_SAFE_LOCK / RB_SAFE_UNLOCK
 * The file is currently unused in ZAB so it has not been upgraded.
 * If you need it you need to fix the NULL in the macros!!!
 *******************************************************************************/


// Define the mutex macros below, if you wish to protect your Safe Circular Queue
#define RB_SAFE_LOCK( id )   while (id) { ZAB_PROTECT_ENTER( (void*)NULL, (unsigned8)(id) ); break; }    // enters a mutex if id > 0
#define RB_SAFE_UNLOCK( id ) while (id) { ZAB_PROTECT_EXIT(  (void*)NULL, (unsigned8)(id) ); break; }    // exits a mutex if id > 0

// helper functions not to be used

#define FULL( RB )   ((RB)->Count >= RB_SAFE_MAX ? zab_true : zab_false)
#define EMPTY( RB )  ((RB)->Count == 0 ? zab_true : zab_false)

// clears and initializes queue
void rbSafeInitialize( rbSafeType* RB, unsigned8 Id )
{
  RB_SAFE_LOCK( Id );
  RB->Id    = Id;
  RB->First = 0;
  RB->Last  = 0;
  RB->Count = 0;
  RB_SAFE_UNLOCK( Id );
}

// does not set Id
void rbSafeClear( rbSafeType* RB )
{
  RB_SAFE_LOCK( RB->Id );
  RB->First = 0;
  RB->Last  = 0;
  RB->Count = 0;
  RB_SAFE_UNLOCK( RB->Id );
}

unsigned8 rbSafeId( rbSafeType* RB )
{
  return RB->Id; // get the Id (unprotected)
}

unsigned8 rbSafeCount( rbSafeType* RB )
{
  unsigned8 Count;
  RB_SAFE_LOCK( RB->Id );
  Count = RB->Count;                 // count of items
  RB_SAFE_UNLOCK( RB->Id );
  return Count;
}

zab_bool rbSafeFull( rbSafeType* RB )
{
  return rbSafeCount( RB ) < RB_SAFE_MAX ? zab_false : zab_true;
}

zab_bool rbSafeEmpty( rbSafeType* RB )
{
  return rbSafeCount( RB ) == 0 ? zab_true : zab_false;
}

// append to buffer end
RB_SAFE_ITEM_TYPE rbSafeAppend( rbSafeType* RB, RB_SAFE_ITEM_TYPE Item )      // append to back
{
  RB_SAFE_LOCK( RB->Id );

  if (RB->Count < RB_SAFE_MAX) // not full
  {
    RB->Buffer[ RB->Last++ ] = Item;
    RB->Last %= RB_SAFE_MAX;
    RB->Count++;
  }
  else
  {
    Item = RB_SAFE_ITEM_NULL; // return the well defined NULL item value meaning the buffer is full
  }

  RB_SAFE_UNLOCK( RB->Id );
  return Item;
}

// remove from buffer start
RB_SAFE_ITEM_TYPE rbSafeRemove( rbSafeType* RB )
{
  RB_SAFE_ITEM_TYPE Item = RB_SAFE_ITEM_NULL; // return the well defined NULL item value meaning underflow

  RB_SAFE_LOCK( RB->Id );

  if (RB->Count != 0)                        // something to remove
  {
    Item = RB->Buffer[ RB->First++ ];
    RB->First %= RB_SAFE_MAX;
    RB->Count--;
  }

  RB_SAFE_UNLOCK( RB->Id );
  return Item;
}
