/******************************************************************************
 *         ZigBee Application Brick - Schneider ZigBee Library
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file is the SZL plugin for the Electrical Measurement Cluster.
 *   This plugin is originally designed for Nova (partner).
 * 
 *   It supports:
 *    - Multiple, application specified endpoints
 *    - Standard attributes
 *    - =S= MS attributes defined in ZigBee&GreenPower_Invariants_Partner_Products_V05.pdf
 *    - Default report configuration for time based reports (fast or slow)
 *    - Callback notification to app when a writeable attribute is changed
 * 
 *   It does not (currently) support:
 *    - Non-volatile backing of any data.
 *    - Report on change. These are not required for Nova, but would be required for certification.
 *
 * CONFIGURATION:
 *   ELEC_MEAS_M_USE_DIRECT_ACCESS:
 *    - Define to have plugin malloc data storage for attributes and access the data directly.
 *    - Undefine to have plugin use callbacks to request attribute read/write via callbacks
 *
 * USAGE:
 *   Usage of this plugin is very simple:
 *   If using ELEC_MEAS_M_USE_DIRECT_ACCESS:
 *    1. Initialise it with SzlPlugin_ElecMeasClusterInit(), specify the endpoints and a write notification callback
 *    2. Use SzlPlugin_ElecMeasCluster_GetAttributePointer() to get access to the data for each endpoint.
 *    3. Populate the data
 *    4. Call SzlPlugin_ElecMeasCluster_ConfigureReporting() to start attribute reporting
 *    5. Update data as it changes, the plugin will do the rest.
 *   If not using METERING_M_USE_DIRECT_ACCESS:
 *    1. Initialise it with SzlPlugin_ElecMeasClusterInit(), specify the endpoints and read/write attribute callbacks
 *    2. Call SzlPlugin_ElecMeasCluster_ConfigureReporting() to start attribute reporting
 * 
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *   Rev          Date     Author  Change Description
 *    2         19-May-14   MvdB   Remove endpoint number from public attribute data
 *    3         29-Oct-14   MvdB   artf74202: Support use of callbacks instead of direct access
 * 002.001.006  27-Jul-15   MvdB   ARTF134686: Standardise on SZL_RESULT_t for plugin init return value
 *                                 ARTF138318: Improve plugin destroy, add disable reporting function
 *****************************************************************************/
#ifndef _ELEC_MEAS_CLUSTER_H_
#define _ELEC_MEAS_CLUSTER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "zabTypes.h"
#include "szl.h"


/******************************************************************************
 *                      ******************************
 *                 *****        CONFIGURATION         *****
 *                      ******************************
 ******************************************************************************/

/* If defined, the plugin will use Direct Access to attribute data. This means:
 *  - Plugin will malloc space to store all attribute data for the cluster.
 *  - Plugin will directly access this data via pointers.
 *  - Application can get a pointer to the structure, then set values in it.
 *  - If the applicaiton is not single threaded it must handle mutexing before accessing the data.
 * If not defined, the plugin will use callbacks to access attribute data. This means:
 *  - The plugin will read/write data by calling an application supplied callback.
 *  - The application can then manage all data access itself.
 */
//#define ELEC_MEAS_M_USE_DIRECT_ACCESS
  
/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Attributes Identifiers */
typedef enum
{
  ATTRID_ELEC_MEAS_MeasurementType = 0x0000,

  ATTRID_ELEC_MEAS_ACFrequency = 0x0300,

  ATTRID_ELEC_MEAS_ACFrequencyMin = 0x0301,
  ATTRID_ELEC_MEAS_ACFrequencyMax = 0x0302,
  ATTRID_ELEC_MEAS_NeutralCurrent = 0x0303,
  ATTRID_ELEC_MEAS_TotalActivePower = 0x0304,
  ATTRID_ELEC_MEAS_TotalReActivePower = 0x0305,
  ATTRID_ELEC_MEAS_TotalApparentPower = 0x0306,

  ATTRID_ELEC_MEAS_ACFrequencyMultiplier = 0x0400,
  ATTRID_ELEC_MEAS_ACFrequencyDivisor = 0x0401,
  ATTRID_ELEC_MEAS_PowerMultiplier = 0x0402,
  ATTRID_ELEC_MEAS_PowerDivisor = 0x0403,

  ATTRID_ELEC_MEAS_RMSVoltagePhA = 0x0505,
  ATTRID_ELEC_MEAS_RMSVoltageMinPhA = 0x0506,
  ATTRID_ELEC_MEAS_RMSVoltageMaxPhA = 0x0507,
  ATTRID_ELEC_MEAS_RMSCurrentPhA = 0x0508,
  ATTRID_ELEC_MEAS_RMSCurrentMinPhA = 0x0509,
  ATTRID_ELEC_MEAS_RMSCurrentMaxPhA = 0x050A,
  ATTRID_ELEC_MEAS_ActivePowerPhA = 0x050B,
  ATTRID_ELEC_MEAS_ActivePowerMinPhA = 0x050C,
  ATTRID_ELEC_MEAS_ActivePowerMaxPhA = 0x050D,
  ATTRID_ELEC_MEAS_ReActivePowerPhA = 0x050E,
  ATTRID_ELEC_MEAS_ApparentPowerPhA = 0x050F,
  ATTRID_ELEC_MEAS_PowerFactorPhA = 0x0510,

  ATTRID_ELEC_MEAS_ACVoltageMultiplierPhA = 0x0600,
  ATTRID_ELEC_MEAS_ACVoltageDivisorPhA = 0x0601,
  ATTRID_ELEC_MEAS_ACCurrentMultiplierPhA = 0x0602,
  ATTRID_ELEC_MEAS_ACCurrentDivisorPhA = 0x0603,
  ATTRID_ELEC_MEAS_ACPowerMultiplierPhA = 0x0604,
  ATTRID_ELEC_MEAS_ACPowerDivisorPhA = 0x0605,

  ATTRID_ELEC_MEAS_RMSVoltagePhB = 0x0905,
  ATTRID_ELEC_MEAS_RMSVoltageMinPhB = 0x0906,
  ATTRID_ELEC_MEAS_RMSVoltageMaxPhB = 0x0907,
  ATTRID_ELEC_MEAS_RMSCurrentPhB = 0x0908,
  ATTRID_ELEC_MEAS_RMSCurrentMinPhB = 0x0909,
  ATTRID_ELEC_MEAS_RMSCurrentMaxPhB = 0x090A,
  ATTRID_ELEC_MEAS_ActivePowerPhB = 0x090B,
  ATTRID_ELEC_MEAS_ActivePowerMinPhB = 0x090C,
  ATTRID_ELEC_MEAS_ActivePowerMaxPhB = 0x090D,
  ATTRID_ELEC_MEAS_ReActivePowerPhB = 0x090E,
  ATTRID_ELEC_MEAS_ApparentPowerPhB = 0x090F,
  ATTRID_ELEC_MEAS_PowerFactorPhB = 0x0910,

  ATTRID_ELEC_MEAS_RMSVoltagePhC = 0x0A05,
  ATTRID_ELEC_MEAS_RMSVoltageMinPhC = 0x0A06,
  ATTRID_ELEC_MEAS_RMSVoltageMaxPhC = 0x0A07,
  ATTRID_ELEC_MEAS_RMSCurrentPhC = 0x0A08,
  ATTRID_ELEC_MEAS_RMSCurrentMinPhC = 0x0A09,
  ATTRID_ELEC_MEAS_RMSCurrentMaxPhC = 0x0A0A,
  ATTRID_ELEC_MEAS_ActivePowerPhC = 0x0A0B,
  ATTRID_ELEC_MEAS_ActivePowerMinPhC = 0x0A0C,
  ATTRID_ELEC_MEAS_ActivePowerMaxPhC = 0x0A0D,
  ATTRID_ELEC_MEAS_ReActivePowerPhC = 0x0A0E,
  ATTRID_ELEC_MEAS_ApparentPowerPhC = 0x0A0F,
  ATTRID_ELEC_MEAS_PowerFactorPhC = 0x0A10,    

  /* Schneider Manufacturer Specific - See ZigBee&GreenPower_Invariants_Partner_Products_V05.pdf */
  ATTRID_ELEC_MEAS_TotalActivePowerDemand = 0x4300,
  ATTRID_ELEC_MEAS_TotalActivePowerMin = 0x4301,
  ATTRID_ELEC_MEAS_TotalActivePowerMax = 0x4302,
  ATTRID_ELEC_MEAS_TotalReactivePowerDemand = 0x4303,
  ATTRID_ELEC_MEAS_TotalActivePowerFundamental = 0x4304,
  ATTRID_ELEC_MEAS_TotalReactivePowerFundamental = 0x4305,
  ATTRID_ELEC_MEAS_THDCurrentFun3PhAvg = 0x4306,
  ATTRID_ELEC_MEAS_THDCurrentFundNeutral = 0x4307,
  ATTRID_ELEC_MEAS_THDCurrentFundNeutralMin = 0x4308,
  ATTRID_ELEC_MEAS_THDCurrentFundNeutralMax = 0x4309,
  ATTRID_ELEC_MEAS_THDCurrentFundNeutralAvg = 0x430A,
  ATTRID_ELEC_MEAS_NeutralCurrentMin = 0x430B,
  ATTRID_ELEC_MEAS_NeutralCurrentMax = 0x430C,
  ATTRID_ELEC_MEAS_NeutralCurrentAvg = 0x430D,
  ATTRID_ELEC_MEAS_GroundCurrent = 0x430E,
  ATTRID_ELEC_MEAS_GroundCurrentMin = 0x430F,
  ATTRID_ELEC_MEAS_GroundCurrentMax = 0x4310,
  ATTRID_ELEC_MEAS_GroundCurrentAvg = 0x4311,
  ATTRID_ELEC_MEAS_ACFrequencyAvg = 0x4312,
  ATTRID_ELEC_MEAS_Avg3RMSVoltagePhN = 0x4313,
  ATTRID_ELEC_MEAS_Avg3RMSVoltagePhPh = 0x4314,
  ATTRID_ELEC_MEAS_Avg3RMSCurrent = 0x4315,
  ATTRID_ELEC_MEAS_THDCurrentRMSNeutral = 0x4316,
  ATTRID_ELEC_MEAS_THDCurrentRMS3PhAvg = 0x4317,

  ATTRID_ELEC_MEAS_ActivePowerFundamental = 0x4500,
  ATTRID_ELEC_MEAS_ReactivePowerFundamental = 0x4501,
  ATTRID_ELEC_MEAS_RMSVoltageAvg = 0x4502,
  ATTRID_ELEC_MEAS_RMSCurrentAvg = 0x4503,
  ATTRID_ELEC_MEAS_THDCurrentFundPhA = 0x4504,
  ATTRID_ELEC_MEAS_THDCurrentFundPhAMin = 0x4505,
  ATTRID_ELEC_MEAS_THDCurrentFundPhAMax = 0x4506,
  ATTRID_ELEC_MEAS_THDCurrentFundPhAAvg = 0x4507,
  ATTRID_ELEC_MEAS_THDCurrentRMSPhA = 0x4508,

  ATTRID_ELEC_MEAS_ACAlarmsMask = 0x4800,

  ATTRID_ELEC_MEAS_ActivePowerFundamentalPhB = 0x4900,
  ATTRID_ELEC_MEAS_ReactivePowerFundamentalPhB = 0x4901,
  ATTRID_ELEC_MEAS_RMSVoltageAvgPhB = 0x4902,
  ATTRID_ELEC_MEAS_RMSCurrentAvgPhB = 0x4903,
  ATTRID_ELEC_MEAS_THDCurrentFundPhB = 0x4904,
  ATTRID_ELEC_MEAS_THDCurrentFundPhBMin = 0x4905,
  ATTRID_ELEC_MEAS_THDCurrentFundPhBMax = 0x4906,
  ATTRID_ELEC_MEAS_THDCurrentFundPhBAvg = 0x4907,
  ATTRID_ELEC_MEAS_THDCurrentRMSPhB = 0x4908,

  ATTRID_ELEC_MEAS_ActivePowerFundamentalPhC = 0x4A00,
  ATTRID_ELEC_MEAS_ReactivePowerFundamentalPhC = 0x4A01,
  ATTRID_ELEC_MEAS_RMSVoltageAvgPhC = 0x4A02,
  ATTRID_ELEC_MEAS_RMSCurrentAvgPhC = 0x4A03,
  ATTRID_ELEC_MEAS_THDCurrentFundPhC = 0x4A04,
  ATTRID_ELEC_MEAS_THDCurrentFundPhCMin = 0x4A05,
  ATTRID_ELEC_MEAS_THDCurrentFundPhCMax = 0x4A06,
  ATTRID_ELEC_MEAS_THDCurrentFundPhCAvg = 0x4A07,
  ATTRID_ELEC_MEAS_THDCurrentRMSPhC = 0x4A08,

  ATTRID_ELEC_MEAS_RMSVoltagePhAPhB = 0x4B00,
  ATTRID_ELEC_MEAS_RMSVoltageMinPhAPhB = 0x4B01,
  ATTRID_ELEC_MEAS_RMSVoltageMaxPhAPhB = 0x4B02,
  ATTRID_ELEC_MEAS_RMSVoltageAvgPhAPhB = 0x4B03,

  ATTRID_ELEC_MEAS_RMSVoltagePhBPhC = 0x4C00,
  ATTRID_ELEC_MEAS_RMSVoltageMinPhBPhC = 0x4C01,
  ATTRID_ELEC_MEAS_RMSVoltageMaxPhBPhC = 0x4C02,
  ATTRID_ELEC_MEAS_RMSVoltageAvgPhBPhC = 0x4C03,

  ATTRID_ELEC_MEAS_RMSVoltagePhCPhA = 0x4D00,
  ATTRID_ELEC_MEAS_RMSVoltageMinPhCPhA = 0x4D01,
  ATTRID_ELEC_MEAS_RMSVoltageMaxPhCPhA = 0x4D02,
  ATTRID_ELEC_MEAS_RMSVoltageAvgPhCPhA = 0x4D03,
} ELEC_MEAS_CLUSTER_ATTRIBUTES;  
  
/* Attribute Data storage
 * An instance of this is created per endpoint.
 * Applications may access it via SzlPlugin_ElecMeasCluster_GetAttributePointer() */
typedef struct
{
  szl_uint32  MeasurementType;

  szl_uint16  ACFrequency;
  szl_uint16  ACFrequencyMin;
  szl_uint16  ACFrequencyMax;
  szl_uint16  NeutralCurrent;
  szl_int32   TotalActivePower;
  szl_int32   TotalReActivePower;
  szl_uint32  TotalApparentPower;

  szl_uint16  ACFrequencyMultiplier;
  szl_uint16  ACFrequencyDivisor;
  szl_uint32  PowerMultiplier;
  szl_uint32  PowerDivisor;

  szl_uint16  RMSVoltagePhA;
  szl_uint16  RMSVoltageMinPhA;
  szl_uint16  RMSVoltageMaxPhA;
  szl_uint16  RMSCurrentPhA;
  szl_uint16  RMSCurrentMinPhA;
  szl_uint16  RMSCurrentMaxPhA;
  szl_int16   ActivePowerPhA;
  szl_int16   ActivePowerMinPhA;
  szl_int16   ActivePowerMaxPhA;
  szl_int16   ReActivePowerPhA;
  szl_uint16  ApparentPowerPhA;
  szl_int8    PowerFactorPhA;

  szl_uint16  ACVoltageMultiplierPhA;
  szl_uint16  ACVoltageDivisorPhA;
  szl_uint16  ACCurrentMultiplierPhA;
  szl_uint16  ACCurrentDivisorPhA;
  szl_uint16  ACPowerMultiplierPhA;
  szl_uint16  ACPowerDivisorPhA;

  szl_uint16  RMSVoltagePhB;
  szl_uint16  RMSVoltageMinPhB;
  szl_uint16  RMSVoltageMaxPhB;
  szl_uint16  RMSCurrentPhB;
  szl_uint16  RMSCurrentMinPhB;
  szl_uint16  RMSCurrentMaxPhB;
  szl_int16   ActivePowerPhB;
  szl_int16   ActivePowerMinPhB;
  szl_int16   ActivePowerMaxPhB;
  szl_int16   ReActivePowerPhB;
  szl_uint16  ApparentPowerPhB;
  szl_int8    PowerFactorPhB;

  szl_uint16  RMSVoltagePhC;
  szl_uint16  RMSVoltageMinPhC;
  szl_uint16  RMSVoltageMaxPhC;
  szl_uint16  RMSCurrentPhC;
  szl_uint16  RMSCurrentMinPhC;
  szl_uint16  RMSCurrentMaxPhC;
  szl_int16   ActivePowerPhC;
  szl_int16   ActivePowerMinPhC;
  szl_int16   ActivePowerMaxPhC;
  szl_int16   ReActivePowerPhC;
  szl_uint16  ApparentPowerPhC;
  szl_int8    PowerFactorPhC;

  szl_int32   TotalActivePowerDemand;
  szl_int32   TotalActivePowerMin;
  szl_int32   TotalActivePowerMax;
  szl_int32   TotalReActivePowerDemand;
  szl_int32   TotalActivePowerFundamental;
  szl_int32   TotalReActivePowerFundamental;
  szl_uint16  THDCurrentFun3PhAvg;
  szl_uint16  THDCurrentFundNeutral;
  szl_uint16  THDCurrentFundNeutralMin;
  szl_uint16  THDCurrentFundNeutralMax;
  szl_uint16  THDCurrentFundNeutralAvg;
  szl_uint16  NeutralCurrentMin;
  szl_uint16  NeutralCurrentMax;
  szl_uint16  NeutralCurrentAvg;
  szl_uint16  GroundCurrent;
  szl_uint16  GroundCurrentMin;
  szl_uint16  GroundCurrentMax;
  szl_uint16  GroundCurrentAvg;
  szl_uint16  ACFrequencyAvg;
  szl_uint16  Avg3RMSVoltagePhN;
  szl_uint16  Avg3RMSVoltagePhPh;
  szl_uint16  Avg3RMSCurrent;
  szl_uint16  THDCurrentRMSNeutral;
  szl_uint16  THDCurrentRMS3PhAvg;

  szl_int16   ActivePowerFundamental;
  szl_int16   ReActivePowerFundamental;
  szl_uint16  RMSVoltageAvg;
  szl_uint16  RMSCurrentAvg;
  szl_uint16  THDCurrentFundPhA;
  szl_uint16  THDCurrentFundPhAMin;
  szl_uint16  THDCurrentFundPhAMax;
  szl_uint16  THDCurrentFundPhAAvg;
  szl_uint16  THDCurrentRMSPhA;

  szl_uint16  ACAlarmsMask;

  szl_int16   ActivePowerFundamentalPhB;
  szl_int16   ReActivePowerFundamentalPhB;
  szl_uint16  RMSVoltageAvgPhB;
  szl_uint16  RMSCurrentAvgPhB;
  szl_uint16  THDCurrentFundPhB;
  szl_uint16  THDCurrentFundPhBMin;
  szl_uint16  THDCurrentFundPhBMax;
  szl_uint16  THDCurrentFundPhBAvg;
  szl_uint16  THDCurrentRMSPhB;

  szl_int16   ActivePowerFundamentalPhC;
  szl_int16   ReActivePowerFundamentalPhC;
  szl_uint16  RMSVoltageAvgPhC;
  szl_uint16  RMSCurrentAvgPhC;
  szl_uint16  THDCurrentFundPhC;
  szl_uint16  THDCurrentFundPhCMin;
  szl_uint16  THDCurrentFundPhCMax;
  szl_uint16  THDCurrentFundPhCAvg;
  szl_uint16  THDCurrentRMSPhC;

  szl_uint16  RMSVoltagePhAPhB;
  szl_uint16  RMSVoltageMinPhAPhB;
  szl_uint16  RMSVoltageMaxPhAPhB;
  szl_uint16  RMSVoltageAvgPhAPhB;

  szl_uint16  RMSVoltagePhBPhC;
  szl_uint16  RMSVoltageMinPhBPhC;
  szl_uint16  RMSVoltageMaxPhBPhC;
  szl_uint16  RMSVoltageAvgPhBPhC;

  szl_uint16  RMSVoltagePhCPhA;
  szl_uint16  RMSVoltageMinPhCPhA;
  szl_uint16  RMSVoltageMaxPhCPhA;
  szl_uint16  RMSVoltageAvgPhCPhA;
}SzlPlugin_ElecMeas_Attributes_t;


#ifdef ELEC_MEAS_M_USE_DIRECT_ACCESS
/* Attribute Write Notification Handler format */
typedef void (*SzlPlugin_ElecMeasCluster_AttributeWriteNotification_CB_t) (
                            zabService * Service, 
                            szl_uint8 Endpoint, 
                            szl_uint16 AttributeID);
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Plugin Initialisation
 * This function must be called from the application after the Library has been initialized.
 * 
 * Parameters:
 *  Service: Instance of library
 *  numEndpoints: The length of endpointList
 *  endpointList: The endpoints to be initialised with this cluster
 *  WriteAttributesCallback: Callback will be called to notify application that a writable attribute has been written from ZigBee.
 *                           It is optional. If the app does not want to be notified this may be set to NULL.
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_ElecMeasClusterInit( zabService* Service, 
                                            szl_uint8 numEndpoints, 
                                            szl_uint8 * endpointList, 
#ifdef ELEC_MEAS_M_USE_DIRECT_ACCESS
                                           SzlPlugin_ElecMeasCluster_AttributeWriteNotification_CB_t WriteAttributesCallback
#else
                                           SZL_CB_DataPointRead_t ReadCallback,
                                           SZL_CB_DataPointWrite_t WriteCallback
#endif
);

/******************************************************************************
 * Plugin Destruction
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_ElecMeasCluster_Destroy(zabService* Service);

/******************************************************************************
 * Get pointer to Electrical Measurement Cluster Attributes for an endpoint
 * This can be used to get access to the endpoints parameters for the local 
 *  application to read or write them
 * Return NULL if not found
 ******************************************************************************/
#ifdef ELEC_MEAS_M_USE_DIRECT_ACCESS
extern
SzlPlugin_ElecMeas_Attributes_t* SzlPlugin_ElecMeasCluster_GetAttributePointer(zabService* Service, szl_uint8 endpoint);
#endif

/******************************************************************************
 * Configure reporting for the cluster.
 * Data must be initialised before this function is called to avoid reporting uninitialised values
 * 
 * This function currently only configures a time based default report.
 * If report on change is required the plugin must be extended.
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_ElecMeasCluster_ConfigureReporting(zabService* Service);

/******************************************************************************
 * Disable reporting for the cluster
 ******************************************************************************/
extern
SZL_RESULT_t SzlPlugin_ElecMeasCluster_DisableReporting(zabService* Service);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /*_ELEC_MEAS_CLUSTER_H_*/
