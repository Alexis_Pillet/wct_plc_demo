/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the EZSP Trust Center Commands/Responses - HEADER
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.001  24-May-16   MvdB   Original
 * 000.000.017  26-Jul-16   MvdB   Support ZAB_ACTION_CHANGE_NETWORK_KEY
 * 000.000.021  27-Jul-16   MvdB   General review, commenting and tidy up
 *****************************************************************************/
#ifndef __ZAB_NCP_EZSP_TRUST_CENTER_H__
#define __ZAB_NCP_EZSP_TRUST_CENTER_H__

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Join Notification Handler
 ******************************************************************************/
void zabNcpEzspTrustCenter_JoinHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Broadcast Next Network Key Request + Response Handler
 ******************************************************************************/
void zabNcpEzspTrustCenter_BroadcastNextNetworkKey(erStatus* Status, zabService* Service);
void zabNcpEzspTrustCenter_BroadcastNextNetworkKeyResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

/******************************************************************************
 * Broadcast Next Network Key Switch Request + Response Handler
 ******************************************************************************/
void zabNcpEzspTrustCenter_BroadcastNextNetworkKeySwitch(erStatus* Status, zabService* Service);
void zabNcpEzspTrustCenter_BroadcastNextNetworkKeySwitchResponseHandler(erStatus* Status, zabService* Service, unsigned8 PayloadLength, unsigned8* PayloadData);

#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/