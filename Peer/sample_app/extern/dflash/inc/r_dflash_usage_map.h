/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_dflash_lib.h
*    @version
*        $Rev: 2409 $
*    @last editor
*        $Author: a5089763 $
*    @date  
*        $Date:: 2016-11-09 12:11:44 +0900#$
* Description : 
******************************************************************************/

#ifndef R_DFLASH_USAGE_MAP_H
#define R_DFLASH_USAGE_MAP_H


/******************************************************************************
Macro definitions
******************************************************************************/

#define DFLASH_START_OFST           (0)
#define DFLASH_START_DIV3           (5) 


#define DFLASH_START_ADDR           (DFLASH_BASE_ADDR + (DFLASH_START_OFST * DFLASH_BLOCK_SIZE_LARGE))
#define DF_GET_ADDR(x,y)            (DFLASH_BASE_ADDR + ( ((x) * DFLASH_START_DIV3) * DFLASH_BLOCK_SIZE_LARGE) + ((y) * DFLASH_BLOCK_SIZE_LARGE))

#define DF_USE_BLOCK_MASK           (0xFFFF)

#endif /* R_DFALSH_USAGE_MAP_H */
