/*
 * zabZnpZdoFrame.h
 *
 *  Created on: 8 oct. 2012
 *      Author: SESA236777
 */

#ifndef ZABZNPZDOFRAME_H_
#define ZABZNPZDOFRAME_H_


typedef enum 
{
    DEV_HOLD,                           //0        //Initialized - not started automatically
    DEV_INIT,                           //1        //Initialized - not connected to anything
    DEV_NWK_DISC,                       //2        //Discovering PAN's to join
    DEV_NWK_JOINING,                    //3        //Joining a PAN
    DEV_NWK_REJOIN,                     //4        //ReJoining a PAN, only for end devices
    DEV_END_DEVICE_UNAUTH,              //5        //Joined but not yet authenticated by trust center
    DEV_END_DEVICE,                     //6        //Started as device after authentication
    DEV_ROUTER,                         //7        //Device joined, authenticated and is a router
    DEV_COORD_STARTING,                 //8        //Starting as Zigbee Coordinator
    DEV_ZB_COORD,                       //9        //Started as Zigbee Coordinator
    DEV_NWK_ORPHAN,                     //10       //Device has lost information about its parent
} ZDO_STATE;

#endif /* ZABZNPZDOFRAME_H_ */
