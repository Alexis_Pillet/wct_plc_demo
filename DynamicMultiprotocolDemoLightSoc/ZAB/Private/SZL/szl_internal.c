/*******************************************************************************
  Filename    : szl_internal.c
  $Date       : 2013-09-20                                                    $:
  $Revision   : 1                                                             $:
  $Author     : Michale Thorsoe                                               $:

  Description : This is the internal API functions for the SZL

 * ZAB MODIFICATION HISTORY:
 *   Rev           Date     Author  Change Description
 * 002.001.003  16-Jul-15   MvdB   ARTF132827: Include source address in cluster command handlers
 *                                 ARTF131073: Differentiate by direction when registering Cluster Command Handlers
 * 002.001.004  21-Jul-15   MvdB   ARTF134686: Add Szl_InternalIsLibInitialized() and use before allowing registration.
 *                                             Call zabCoreFreeQueue() in Szl_InternalInitialize() to avoid losing messages on multiple SZL init
 *                                 ARTF136586: Enable GPEP as a valid SZL endpoint if GreenPower is enabled
 * 002.001.006  27-Jul-15   MvdB   ARTF138318: Improve Szl_ClusterCmdFind() and Szl_DataPointFind() to get de-registration working
 * 002.002.002  01-Sep-15   MvdB   Fix issue with status in Szl_InternalInitialize()
 * 002.002.006  15-Sep-15   MvdB   ARTF148943: Limit max read length to local convert buffer size in Szl_DataPointReadPrimitive()
 * 002.002.014  20-Oct-15   MvdB   ARTF151327: Handle invalid string (length = 0xFF) in SZL
 * 
 * ---SILABS DEV---
 * 000.000.007  06-Jul-16   MvdB   Upgrade Szl_BuildClustersListFromDataPoints() to also look at registered commands
*******************************************************************************/
/*   _________      .__                  .__    .___
    /   _____/ ____ |  |__   ____   ____ |__| __| _/___________
    \_____  \_/ ___\|  |  \ /    \_/ __ \|  |/ __ |/ __ \_  __ \   ______
    /        \  \___|   Y  \   |  \  ___/|  / /_/ \  ___/|  | \/  /_____/
   /_______  /\___  >___|  /___|  /\___  >__\____ |\___  >__|
           \/     \/     \/     \/     \/        \/    \/
   __________.__      __________
   \____    /|__| ____\______   \ ____   ____
     /     / |  |/ ___\|    |  _// __ \_/ __ \    ______
    /     /_ |  / /_/  >    |   \  ___/\  ___/   /_____/
   /_______ \|__\___  /|______  /\___  >\___  >
           \/  /_____/        \/     \/     \/
   .____    ._____.
   |    |   |__\_ |______________ _______ ___.__.
   |    |   |  || __ \_  __ \__  \\_  __ <   |  |   ______
   |    |___|  || \_\ \  | \// __ \|  | \/\___  |  /_____/
   |_______ \__||___  /__|  (____  /__|   / ____|
           \/       \/           \/       \/
   .___        __                             .__
   |   | _____/  |_  ___________  ____ _____  |  |
   |   |/    \   __\/ __ \_  __ \/    \\__  \ |  |
   |   |   |  \  | \  ___/|  | \/   |  \/ __ \|  |
   |___|___|  /__|  \___  >__|  |___|  (____  /____/
            \/          \/           \/     \/

     _S_           _Z_           _L_
    (o o)         (o o)         (o o)
ooO--(_)--Ooo-ooO--(_)--Ooo-ooO--(_)--Ooo-

Schneider - ZigBee - Library. (SZL)
Copyright (c) 2013 - Schneider-Electric R&D
(textArt from http://patorjk.com )
*/
#include "szl_types.h"
#include "szl_config.h"
#include "endianness.h"
#include "cb_handler.h"
#include "szl_report.h"
#include "data_converter.h"
#include "sort.h"

#include "szl.h"

#include "zabCorePrivate.h"
#include "szl_zab.h"
#include "zcl_def.h"
#include "szl_internal.h"



/*
 * Configuration value validation
 */
#define SZL_UINT8_MIN 0x00
#define SZL_UINT8_MAX 0xFF
#define SZL_UINT16_MIN 0x0000
#define SZL_UINT16_MAX 0xFFFF
#define SZL_INT_MIN (-32768)
#define SZL_INT_MAX (32767)

#if ( (SZL_CFG_ENDPOINT_CNT <= SZL_UINT8_MIN) || (SZL_CFG_ENDPOINT_CNT >= SZL_UINT8_MAX) )
#error SZL_CFG_ENDPOINT_CNT out of range
#endif

#if ( (SZL_CFG_MAX_CLIENT_CLUSTERS <= SZL_UINT8_MIN) || (SZL_CFG_MAX_CLIENT_CLUSTERS >= SZL_UINT8_MAX) )
#error SZL_CFG_MAX_CLIENT_CLUSTERS out of range
#endif

#if ( (SZL_CFG_MAX_DATAPOINTS <= 0) || (SZL_CFG_MAX_DATAPOINTS >= SZL_INT_MAX) )
#error SZL_CFG_MAX_DATAPOINTS out of range
#endif

#if ( (SZL_CFG_MAX_REPORTS <= SZL_UINT16_MIN) || (SZL_CFG_MAX_REPORTS >= SZL_UINT16_MAX) )
#error SZL_CFG_MAX_REPORTS out of range
#endif

#if ( (SZL_CFG_MAX_REPORTABLE_ATTR_LENGTH <= SZL_UINT8_MIN) || (SZL_CFG_MAX_REPORTABLE_ATTR_LENGTH >= SZL_UINT8_MAX) )
#error SZL_CFG_MAX_REPORTABLE_ATTR_LENGTH out of range
#endif

#if ( (SZL_CFG_MAX_CLUSTER_CMDS <= 0) || (SZL_CFG_MAX_CLUSTER_CMDS >= SZL_INT_MAX) )
#error SZL_CFG_MAX_CLUSTER_CMDS out of range
#endif

#if ( (SZL_CFG_CB_HANDLER_SIZE <= 0) || (SZL_CFG_CB_HANDLER_SIZE >= SZL_INT_MAX) )
#error SZL_CFG_CB_HANDLER_SIZE out of range
#endif

#if ( (SZL_EXTERNAL_TIMER_COUNT <= SZL_UINT8_MIN) || (SZL_EXTERNAL_TIMER_COUNT >= SZL_UINT8_MAX) )
#error SZL_EXTERNAL_TIMER_COUNT out of range
#endif

#if ( (SZL_VLA_INIT != 0) && (SZL_VLA_INIT != 1) )
#error SZL_VLA_INIT out of range
#endif

#if ( (SZL_CFG_PROFILE_ID < SZL_UINT16_MIN) || (SZL_CFG_PROFILE_ID > SZL_UINT16_MAX) )
#error SZL_CFG_PROFILE_ID out of range
#endif
#if (SZL_CFG_PROFILE_ID != 0x0104)
#error Warning SZL_CFG_PROFILE_ID should typically be 0x0104: Home Automation
#endif

#if ( (SZL_CFG_DEVICE_VERSION < SZL_UINT8_MIN) || (SZL_CFG_DEVICE_VERSION > SZL_UINT8_MAX) )
#error SZL_CFG_DEVICE_VERSION out of range
#endif

// Apply a reasonable upper limit that will allow the simple descriptor to fit ina single command.
#if ( (SZL_CFG_MAX_CLUSTERS <= SZL_UINT8_MIN) || (SZL_CFG_MAX_CLUSTERS > 20) )
#error SZL_CFG_MAX_CLUSTERS out of range
#endif

#if ( (SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS <= SZL_UINT8_MIN) || (SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS >= SZL_UINT8_MAX) )
#error SZL_ZAB_M_MAX_SYNCHRONOUS_TRANSACTIONS out of range
#endif

#if (SZL_ZAB_M_SYNCHRONOUS_TRANSACTION_TIMEOUT_S <= 0)
#error SZL_ZAB_M_SYNCHRONOUS_TRANSACTION_TIMEOUT_S out of range
#endif

#if (SZL_ZAB_M_GP_SYNCHRONOUS_TRANSACTION_TIMEOUT_S <= 0)
#error SZL_ZAB_M_SYNCHRONOUS_TRANSACTION_TIMEOUT_S out of range
#endif

#if (SZL_ZAB_M_GP_MULTI_FRAME_RESPONSE_DELAY_S <= 1)
#error SZL_ZAB_M_GP_MULTI_FRAME_RESPONSE_DELAY_S out of range
#endif



/*
.  ________          _____.__       .__  __  .__                               .
.  \______ \   _____/ ____\__| ____ |__|/  |_|__| ____   ____   ______         .
.   |    |  \_/ __ \   __\|  |/    \|  \   __\  |/  _ \ /    \ /  ___/         .
.   |    `   \  ___/|  |  |  |   |  \  ||  | |  (  <_> )   |  \\___ \          .
.  /_______  /\___  >__|  |__|___|  /__||__| |__|\____/|___|  /____            .
.          \/     \/              \/                        \/     \/          .
*/



typedef enum
{
    DATA_FORMAT_NATIVE,
    DATA_FORMAT_ZB_LE
} DATA_FORMAT_t;



// Prototypes
static szl_uint64 DATAPOINT_TO_KEY(SZL_DataPoint_t* dp);
static szl_uint64 DATAPOINT_KEY(szl_uint8 ep, szl_uint16 cID, szl_uint16 aID);
static szl_uint64 CLUSTER_CMD_TO_KEY(SZL_ClusterCmd_t* dp);
static szl_uint64 CLUSTER_CMD_KEY(szl_uint8 cmd, szl_uint16 cID, szl_uint32 data);
static szl_bool Szl_ClusterCommandDefaultOperation(zabService* service, SZL_ClusterCmd_t* clusterCmdEntry, szl_uint8 ep, szl_uint8* leData, szl_uint8 leDataLen,szl_uint8* cmdOut, szl_uint8* leDataReturn, szl_uint8* leDataReturnLen);
static szl_bool NativeDataInc(void* nativeData, szl_uint8 nativeDataSize, szl_uint8 padSize, szl_byte value );
static szl_bool NativeDataDec(void* nativeData, szl_uint8 nativeDataSize, szl_uint8 padSize, szl_byte value );
static szl_bool NativeDataSet(void* nativeData, szl_uint8 nativeDataSize, szl_uint8 padSize, szl_uint32 value );
static szl_bool Uint16ArrayFindKey(szl_uint16 key, szl_uint16* list, szl_uint8 count, szl_uint8* rIdx);
//static szl_bool Szl_BuildClusterBindingList(szl_uint16* List, szl_uint8 ListMaxSize, szl_uint16 searchFromID, szl_uint8* Count);
static szl_bool Szl_BuildClustersListFromDataPoints(zabService* service, ENUM_SZL_DP_DIRECTION_t direction, szl_uint8 ep, szl_uint16 searchFromID, szl_uint8 ListMaxSize, szl_uint16* List, szl_uint8* Count);
static szl_bool Szl_BuildInputClustersList(zabService* service, szl_uint8 ep, szl_uint16 searchFromID, szl_uint8 ListMaxSize, szl_uint16* List, szl_uint8* Count);
static szl_bool Szl_BuildOutputClustersList(zabService* service, szl_uint8 ep, szl_uint16 searchFromID, szl_uint8 ListMaxSize, szl_uint16* List, szl_uint8* Count);
static SZL_STATUS_t Szl_DataPointRead(zabService* service, SZL_EP_t endpoint,szl_bool manufacturerSpecific,szl_uint16 clusterID, szl_uint16 attributeID, szl_uint8* data_type, void* data, szl_uint8* lengthInOut, DATA_FORMAT_t format);
static SZL_STATUS_t Szl_DataPointWrite(zabService* service, SZL_EP_t endpoint, szl_bool manufacturerSpecific, szl_uint16 clusterID, szl_uint16 attributeID, szl_uint8 data_type, void* data, szl_uint8 length, DATA_FORMAT_t format, szl_bool forceWrite);
static SZL_STATUS_t Szl_DataPointReadString(zabService* service, SZL_DataPoint_t* dp, void* data, szl_uint8* lengthInOut, DATA_FORMAT_t format);
static SZL_STATUS_t Szl_DataPointWriteString(zabService* service, SZL_DataPoint_t* dp, void* data, szl_uint8 length, DATA_FORMAT_t format);
static SZL_STATUS_t Szl_DataPointReadPrimitive(zabService* service, SZL_DataPoint_t* dp, void* data, szl_uint8* lengthInOut, DATA_FORMAT_t format);
static SZL_STATUS_t Szl_DataPointWritePrimitive(zabService* service, SZL_DataPoint_t* dp, void* data, szl_uint8 length, DATA_FORMAT_t format);

/*
.  ___________                   __  .__                                       .
.  \_   _____/_ __  ____   _____/  |_|__| ____   ____   ______                 .
.   |    __)|  |  \/    \_/ ___\   __\  |/  _ \ /    \ /  ___/                 .
.   |     \ |  |  /   |  \  \___|  | |  (  <_> )   |  \\___ \                  .
.   \___  / |____/|___|  /\___  >__| |__|\____/|___|  /____  >                 .
.       \/             \/     \/                    \/     \/                  .
*/

/**
 * Initialize
 */
void Szl_InternalInitialize(zabService* service)
{
    erStatus LocalStatus;
    erStatusClear(&LocalStatus, service);
  
    // Queue
    zabCoreFreeQueue(&LocalStatus, &LIB_DATA(service).szlDataInQ);
    qSafeClear(&LIB_DATA(service).szlDataInQ);
    LIB_DATA(service).zcl_RawEventHandler = NULL;
    szl_zab_InitSynchronousTransactionTable(service);
    
    // Init Data
    LIB_DATA(service).initData.NumberOfEndpoints = 0;
    
    // Lib
    LIB_DATA(service).Lib.Initialized = szl_false;
    szl_memset(&LIB_DATA(service).Lib.DataPoints, 0xFF, sizeof(LIB_DATA(service).Lib.DataPoints));
    szl_memset(&LIB_DATA(service).Lib.ClusterCmds, 0xFF, sizeof(LIB_DATA(service).Lib.ClusterCmds));
    szl_memset(&LIB_DATA(service).Lib.ClientClusters, 0xFF, sizeof(LIB_DATA(service).Lib.ClientClusters));
    
    // Plugins
    szl_memset(&LIB_DATA(service).Plugins, 0x00, sizeof(LIB_DATA(service).Plugins));
}

/**
 * Initialized
 */
void Szl_InternalInitialized(zabService* service)
{
    // now we are safe
    LIB_DATA(service).Lib.Initialized = szl_true;
}

/**
 * Is Library Initialized
 * 
 * This is used to confirm the Lib is initialized, so registration etc makes sense.
 */
SZL_RESULT_t Szl_InternalIsLibInitialized(zabService* Service)
{
    if ( LIB_DATA(Service).Lib.Initialized )
    {
        return SZL_RESULT_SUCCESS;
    }

    return SZL_RESULT_LIBRARY_NOT_INITIALIZED;
}



/**
 * szl_uint16 Compare
 *
 * This function compares two szl_uint16 data values
 * returns:
 *           -1 a < b
 *            0 a = b
 *           +1 a > b
 */
int Szl_Uint16Compare(const void* a, const void* b)
{
    szl_uint16 da = *(szl_uint16*) a;
    szl_uint16 db = *(szl_uint16*) b;

    return (da > db) - (da < db);
}

/**
 * Client Cluster Compare
 *
 * This function compares two client cluster entries Key (ep - cluster_id)
 * returns:
 *           -1 a < b
 *            0 a = b
 *           +1 a > b
 */
int Szl_ClientClustersCompare(const void* a, const void* b)
{
    SZL_ClientCluster_t* da = (SZL_ClientCluster_t*) a;
    SZL_ClientCluster_t* db = (SZL_ClientCluster_t*) b;

    if (da->Ep == db->Ep)
    {
        return (da->ClusterID > db->ClusterID) - (da->ClusterID < db->ClusterID);
    }

    return (da->Ep > db->Ep) - (da->Ep < db->Ep);
}

/**
 * Data Point Compare
 *
 * This function compares the keys of two data points
 * returns:
 *           -1 a < b
 *            0 a = b
 *           +1 a > b
 */
int Szl_DatapointsCompare(const void* a, const void* b)
{
    szl_uint64 da = DATAPOINT_TO_KEY((SZL_DataPoint_t*) a);
    szl_uint64 db = DATAPOINT_TO_KEY((SZL_DataPoint_t*) b);

#if !SUPPORTS_64_BIT_DATATYPES
    if (da.hi == db.hi)
        return (da.lo > db.lo) - (da.lo < db.lo);
    else
        return (da.hi > db.hi) - (da.hi < db.hi);
#else
    return (da > db) - (da < db);
#endif
}

/**
 * Converts a datapoint values into a 64bit key value
 */
static szl_uint64 DATAPOINT_TO_KEY(SZL_DataPoint_t* dp)
{
    return DATAPOINT_KEY(dp->Endpoint, dp->ClusterID, dp->AttributeID);
}

/**
 * DataPoint Key
 *
 * Returns a szl_uint64 key in the following format:
 *    xxxx---- -------- Reserved
 *    ----xxxx -------- Endpoint
 *    -------- xxxx---- ClusterID
 *    -------- ----xxxx AttributeID
 */
static szl_uint64 DATAPOINT_KEY(SZL_EP_t ep, szl_uint16 cID, szl_uint16 aID)
{
    szl_uint64 key;

#if !SUPPORTS_64_BIT_DATATYPES
    key.hi = ep;
    key.lo = ((szl_uint32)cID << 16 | aID);
#else
    key = (szl_uint64)ep << 32 | ((szl_uint32)cID << 16 | aID);
#endif

    return key;
}


/**
 * Endpoint Registered
 *
 * Determines
 * returns:
 *          szl_true if endpoint is registered
 *          szl_false if endpoint is unregistered
 */
szl_bool Szl_EndpointRegistered(zabService* service, SZL_EP_t ep)
{
    szl_uint8 currentEp;

#ifdef SZL_CFG_ENABLE_GREEN_POWER
    // If GP is enabled then the GPEP is a valid endpoint
    if (ep == SZL_GP_ENDPOINT)
      {
        return szl_true;
      }
#endif

    for( currentEp = 0; currentEp < LIB_DATA(service).initData.NumberOfEndpoints; currentEp++)
    {
        if(LIB_DATA(service).initData.Endpoints[currentEp] == ep)
        {
            return szl_true;
        }
    }

    return szl_false;
}


/**
 * Data Point Find
 *
 * Find a registered data point in the table
 * returns:
 *          pointer to the data point if found
 *          null if not found
 */
SZL_DataPoint_t* Szl_DataPointFind(zabService* service, SZL_EP_t ep, szl_bool manufacturerSpecific, szl_uint16 clusterID, szl_uint16 attributeID, szl_bool matchEmptyEntry)
{
  int idx;
    SZL_DataPoint_t* gDataPoints = ZAB_SRV(service)->szlService.Lib.DataPoints;
  
    for (idx=0; idx < SZL_CFG_MAX_DATAPOINTS; idx++)
    {
        if ((matchEmptyEntry == szl_true) &&
            gDataPoints[idx].Endpoint == 0xFF &&
            gDataPoints[idx].ClusterID == 0xFFFF &&
            gDataPoints[idx].AttributeID == 0xFFFF )
        {
            return NULL; // empty entry
        }

        if ( (gDataPoints[idx].Endpoint == ep ||                     // specific endpoint match
             (gDataPoints[idx].Endpoint == SZL_ADDRESS_EP_ALL &&
              Szl_EndpointRegistered(service,ep))||    // or all endpoints valid
              ep == SZL_ADDRESS_EP_ALL)                             // or all endpoints valid
             && gDataPoints[idx].ClusterID == clusterID
             && gDataPoints[idx].AttributeID == attributeID 
             && gDataPoints[idx].Property.ManufacturerSpecific == manufacturerSpecific)
        {
            return &gDataPoints[idx]; // match
        }
    }

    return NULL;
}

/**
 * Cluster Cmds Compare
 *
 * This function compares the keys of two Cluster Commands
 * returns:
 *           -1 a < b
 *            0 a = b
 *           +1 a > b
 */
int Szl_ClusterCmdsCompare(const void* a, const void* b)
{
    szl_uint64 da = CLUSTER_CMD_TO_KEY((SZL_ClusterCmd_t*) a);
    szl_uint64 db = CLUSTER_CMD_TO_KEY((SZL_ClusterCmd_t*) b);

#if !SUPPORTS_64_BIT_DATATYPES
    if (da.hi == db.hi)
        return (da.lo > db.lo) - (da.lo < db.lo);
    else
        return (da.hi > db.hi) - (da.hi < db.hi);
#else
    return (da > db) - (da < db);
#endif
}

/**
 * Converts a clusterCmd values into a 64bit key value
 */
static szl_uint64 CLUSTER_CMD_TO_KEY(SZL_ClusterCmd_t* Cc)
{
    //return CLUSTER_CMD_KEY(Cc->CmdID, Cc->ClusterID, (szl_uint32)Cc->OperationAccess.Callback);
    size_t temp = (size_t)Cc->OperationAccess.Callback;
    return CLUSTER_CMD_KEY(Cc->CmdID, Cc->ClusterID, (szl_uint32)temp);
}

/**
 * Cluster Cmd Key
 *
 * Returns a szl_uint64 key in the following format:
 *    xxxx---- -------- ClusterID
 *    ----xxxx -------- cmd
 *    -------- xxxxxxxx AttributeID or CB
 */
static szl_uint64 CLUSTER_CMD_KEY(szl_uint8 cmd, szl_uint16 cID, szl_uint32 data)
{
    szl_uint64 key;

#if !SUPPORTS_64_BIT_DATATYPES
    key.hi =  ((szl_uint32)cID << 16 | cmd);
    key.lo = data;
#else
    key = (szl_uint64)cID << 48 | ((szl_uint64)cmd << 32 | data);
#endif

    return key;
}


/**
 * Reads a string from the App via Callback or DirectAccess
 * 
 * lengthInOut is an in/out parameter:
 *  - In: MaxLength for value written to data
 *  - Out: Length of value written at data
 */
static SZL_STATUS_t Szl_DataPointReadString(zabService* service, SZL_DataPoint_t* dp, void* data, szl_uint8* lengthInOut, DATA_FORMAT_t format)
{
    SZL_STATUS_t status = SZL_STATUS_SUCCESS;
    szl_uint8 maxLength;

    void* tData = NULL;
    
    /* If we have a valid MaxLength then decrement it:
     *  If Native it's for the zero terminator.
     *  If ZB then it's for the length byte */
    maxLength = *lengthInOut;
    if (maxLength > 0)
      {
        maxLength--;
        *lengthInOut = maxLength;
      }
    
    switch (format)
    {
        case DATA_FORMAT_NATIVE:
            tData = data;
            break;

        case DATA_FORMAT_ZB_LE:
            tData = (szl_uint8*)data + 1; // the char is placed in index[1] since length is in index [0]
            break;

        default:
            return SZL_STATUS_UNSUPPORTED_OPERATION;
    }

    if (dp->Property.Access & DP_ACCESS_READ)
      {
        switch (dp->Property.Method)
          {
            case DP_METHOD_CALLBACK:
                // the length only includes the real data, not the zero termination
                if (dp->AccessType.Callback.Read(service, dp->Endpoint, dp->Property.ManufacturerSpecific, dp->ClusterID, dp->AttributeID,  tData, lengthInOut) != SZL_RESULT_SUCCESS)
                  {
                    status = SZL_STATUS_INVALID_VALUE;
                  }
                break;

            case DP_METHOD_DIRECT:
                *lengthInOut = szl_strlen((char*)dp->AccessType.DirectAccess.Data);
                if (*lengthInOut > maxLength)
                  {
                    *lengthInOut = maxLength;
                  }
                szl_memcpy(tData, dp->AccessType.DirectAccess.Data, *lengthInOut);
                break;
          }
      }
    else
      {
        status = SZL_STATUS_INVALID_VALUE;
      }

    if (status == SZL_STATUS_SUCCESS )
    {
        switch (format)
        {
            case DATA_FORMAT_NATIVE:
                *((szl_uint8*)data + *lengthInOut) = 0; // place the zero terminator in the end of the string
                (*lengthInOut) ++; // increase the data length including the zero termination
                break;

            case DATA_FORMAT_ZB_LE:
                *(szl_uint8*)data = *lengthInOut; // place the length in the first szl_byte of the string
                (*lengthInOut) ++; // increase the data length including the length szl_byte
                break;
        }
    }

    return status;
}

/**
 * Writes a string to the App via Callback or DirectAccess
 */
static SZL_STATUS_t Szl_DataPointWriteString(zabService* service, SZL_DataPoint_t* dp, void* data, szl_uint8 length, DATA_FORMAT_t format)
{
    SZL_STATUS_t status = SZL_STATUS_SUCCESS;

    switch (format)
    {
        case DATA_FORMAT_NATIVE:
            break;

        case DATA_FORMAT_ZB_LE:
            // If string is invalid_value, then filter this out and don't bother the application with it
            if (*(szl_uint8*)data == SZL_ZB_DATATYPE_CHAR_OCTET_STR_INVALID_VALUE)
              {
                return SZL_STATUS_INVALID_VALUE;
              }
            else
              {
                length = *(szl_uint8*)data; // the length field is in index[0]
              }
            data = (szl_uint8*)data + 1;// the real data is in index[1]
            break;

        default:
            return SZL_STATUS_UNSUPPORTED_OPERATION;
    }


    
        switch (dp->Property.Method)
        {
            case DP_METHOD_CALLBACK:
                if(dp->AccessType.Callback.Write)
                {
                    if (dp->AccessType.Callback.Write(service, dp->Endpoint, dp->Property.ManufacturerSpecific, dp->ClusterID, dp->AttributeID, data, length) != SZL_RESULT_SUCCESS)
                    {
                        status = SZL_STATUS_INVALID_VALUE;
                    }
                }
                else
                {
                    status = SZL_STATUS_FAILED;
                }
                break;

            case DP_METHOD_DIRECT:
                szl_memcpy(dp->AccessType.DirectAccess.Data, data, length);
                *((szl_uint8*)(dp->AccessType.DirectAccess.Data) + length) = 0;
                Szl_AttributeChangedNtfCB(service, dp);
                break;
        }

    return status;
}

/**
 * Reads a Primitive type from the App via Callback or DirectAccess
 * 
 * lengthInOut is an in/out parameter:
 *  - In: MaxLength for value written to data
 *  - Out: Length of value written at data
 */
static SZL_STATUS_t Szl_DataPointReadPrimitive(zabService* service, SZL_DataPoint_t* dp, void* data, szl_uint8* lengthInOut, DATA_FORMAT_t format)
{
    SZL_STATUS_t status = SZL_STATUS_INVALID_VALUE;
    SZL_RESULT_t result = SZL_RESULT_FAILED;

    NATIVE_DATA_t alignedConvertBuffer;
    szl_uint8 *convertBuffer = alignedConvertBuffer.data.raw; // max 64bit's
    
    // ARTF148943: Limit max length to the size of our local convert buffer
    *lengthInOut = sizeof(NATIVE_DATA_t);

    if (dp->Property.Access & DP_ACCESS_READ)
    {
        switch (dp->Property.Method)
        {
            case DP_METHOD_CALLBACK:
                result = dp->AccessType.Callback.Read(service, dp->Endpoint, dp->Property.ManufacturerSpecific, dp->ClusterID, dp->AttributeID, (void*)convertBuffer, lengthInOut);
                break;

            case DP_METHOD_DIRECT:
                if (dp->DataSize <= *lengthInOut)
                  {
                    *lengthInOut = dp->DataSize;
                    szl_memcpy(convertBuffer, dp->AccessType.DirectAccess.Data, *lengthInOut);
                    result = SZL_RESULT_SUCCESS;
                  }
                else
                  {
                    result = SZL_RESULT_DATA_TOO_BIG;
                  }
                break;
        }
    }

    if (result == SZL_RESULT_SUCCESS )
    {
        status = SZL_STATUS_SUCCESS;

        if (format == DATA_FORMAT_ZB_LE)
        {
            // we read a native data type and needs to convert it into the ZB LE type now
            *lengthInOut = NativeToZb((void*)convertBuffer, *lengthInOut, dp->DataType, data);
        }
        else
        {
            szl_memcpy(data, convertBuffer, *lengthInOut);
        }

        if (*lengthInOut == 0)
        {
            status = SZL_STATUS_INVALID_DATA_TYPE;
        }
    }

    return status;
}

/**
 * Writes a Primitive type to the App via Callback or DirectAccess
 */
static SZL_STATUS_t Szl_DataPointWritePrimitive(zabService* service, SZL_DataPoint_t* dp, void* data, szl_uint8 length, DATA_FORMAT_t format)
{
    SZL_STATUS_t status = SZL_STATUS_SUCCESS;
    NATIVE_DATA_t alignedNativeData;
    szl_uint8 nativeLength;

    if (format == DATA_FORMAT_ZB_LE)
    {
        // we converts the stream from LE into native type and replaces the data pointer
        // the length is converted to the native length
        if ( !ZbToNative( dp->DataType, (szl_uint8*)data, length, &alignedNativeData, &nativeLength ) )
        {
            return SZL_STATUS_INVALID_DATA_TYPE;
        }
    }
    else
    {
        //just copy the data in, using an unaligned safe memcpy
        szl_memcpy(&alignedNativeData,data,length);
        nativeLength = length;
    }

    switch (dp->Property.Method)
    {
        case DP_METHOD_CALLBACK:
            if(dp->AccessType.Callback.Write)
            {
                  if (dp->AccessType.Callback.Write(service, dp->Endpoint, dp->Property.ManufacturerSpecific, dp->ClusterID, dp->AttributeID, &alignedNativeData, nativeLength) != SZL_RESULT_SUCCESS)
              {
                      status = SZL_STATUS_INVALID_VALUE;
              }
            }
            else
            {
              status = SZL_STATUS_FAILED;
            }
            break;

        case DP_METHOD_DIRECT:
            szl_memcpy(dp->AccessType.DirectAccess.Data, &alignedNativeData, nativeLength);
            Szl_AttributeChangedNtfCB(service, dp);
            break;
    }

    return status;
}


/**
 * Data Point Read ZB
 *
 * Read data from a registered data point using the callback
 *
 * NOTE: All data are presented and handled in ZB type and Little Endian
 * 
 * lengthInOut is an in/out parameter:
 *  - In: MaxLength for value written to data
 *  - Out: Length of value written at data
 */
SZL_STATUS_t Szl_DataPointReadZB(zabService* service, SZL_EP_t endpoint, szl_bool manufacturerSpecific,szl_uint16 clusterID, szl_uint16 attributeID, szl_uint8* data_type, void* data, szl_uint8* lengthInOut)
{
    return Szl_DataPointRead(service, endpoint, manufacturerSpecific, clusterID, attributeID, data_type, data, lengthInOut, DATA_FORMAT_ZB_LE);
}

/**
 * Data Point Read Native
 *
 * Read data from a registered data point using the callback
 *
 * NOTE: All data are presented and handled in Native type and Endian
 * 
 * lengthInOut is an in/out parameter:
 *  - In: MaxLength for value written to data
 *  - Out: Length of value written at data
 */
SZL_STATUS_t Szl_DataPointReadNative(zabService* service, SZL_EP_t endpoint, szl_bool manufacturerSpecific,szl_uint16 clusterID, szl_uint16 attributeID, szl_uint8* data_type, void* data, szl_uint8* lengthInOut)
{
    return Szl_DataPointRead(service, endpoint, manufacturerSpecific, clusterID, attributeID, data_type, data, lengthInOut, DATA_FORMAT_NATIVE);
}

/**
 * Data Point Read
 *
 * Read data from a registered data point using the specified method
 *
 * The data can be in NATIVE or ZB little Endianness
 * 
 * lengthInOut is an in/out parameter:
 *  - In: MaxLength for value written to data
 *  - Out: Length of value written at data
 */
static SZL_STATUS_t Szl_DataPointRead(zabService* service, SZL_EP_t endpoint, szl_bool manufacturerSpecific, szl_uint16 clusterID, szl_uint16 attributeID, szl_uint8* data_type, void* data, szl_uint8* lengthInOut, DATA_FORMAT_t format)
{
    SZL_STATUS_t status = SZL_STATUS_UNSUPPORTED_ATTRIBUTE;

    SZL_DataPoint_t* dp = Szl_DataPointFind(service, endpoint, manufacturerSpecific, clusterID, attributeID, szl_true);

    if (dp)
    {
        // check for data type and set pointer to convertBuffer for endian conversion
        switch (dp->DataType)
        {
            case SZL_ZB_DATATYPE_CHAR_STR:
            case SZL_ZB_DATATYPE_OCTET_STR:
                status = Szl_DataPointReadString(service, dp, data, lengthInOut, format);
                break;

            default:
                if (DataTypeIsPrimitive(dp->DataType))
                  {
                    status = Szl_DataPointReadPrimitive(service, dp, data, lengthInOut, format);
                  }
                else
                  {
                    printError(service, "SZL INTERNAL: Szl_DataPointRead - Unsupported data type 0x%02X\n", dp->DataType);
                  }
                break;
        }

        if (status == SZL_STATUS_SUCCESS)
        {
            *data_type = dp->DataType;
        }
    }

    return status;
}


/**
 * Data Point Write
 *
 * Write data to a registered data point using the callback
 *
 * NOTE: All data are presented and handled in ZB type and Little Endian
 */
SZL_STATUS_t Szl_DataPointWriteZB(zabService* service, SZL_EP_t endpoint, szl_bool manufacturerSpecific, szl_uint16 clusterID, szl_uint16 attributeID, szl_uint8 data_type, void* data, szl_uint8 length)
{
  return Szl_DataPointWrite(service, endpoint, manufacturerSpecific, clusterID, attributeID, data_type, data, length, DATA_FORMAT_ZB_LE,szl_false);
}

/**
 * Data Point Write
 *
 * Write data to a registered data point using the callback
 *
 * NOTE: All data are presented and handled in Native type and Endian
 */
SZL_STATUS_t Szl_DataPointWriteNative(zabService* service, SZL_EP_t endpoint, szl_bool manufacturerSpecific, szl_uint16 clusterID, szl_uint16 attributeID, szl_uint8 data_type, void* data, szl_uint8 length)
{
    return Szl_DataPointWrite(service, endpoint, manufacturerSpecific, clusterID, attributeID, data_type, data, length, DATA_FORMAT_NATIVE,szl_true);
}

/**
 * Data Point Write
 *
 * Write data to a registered data point using the callback
 *
 * Data are presented and handled as Native or ZB Little Endian
 */
static SZL_STATUS_t Szl_DataPointWrite(zabService* service, SZL_EP_t endpoint, szl_bool manufacturerSpecific, szl_uint16 clusterID, szl_uint16 attributeID, szl_uint8 data_type, void* data, szl_uint8 length, DATA_FORMAT_t format, szl_bool forceWrite)
{
    SZL_STATUS_t status = SZL_STATUS_UNSUPPORTED_ATTRIBUTE;

    SZL_DataPoint_t* dp = Szl_DataPointFind(service, endpoint, manufacturerSpecific, clusterID, attributeID, szl_true);

    if (dp)
    {
        if (dp->DataType != data_type)
        {
            return SZL_STATUS_INVALID_DATA_TYPE;
        }

        if (length > dp->DataSize)
        {
            return SZL_STATUS_INVALID_VALUE;
        }

        if ( (dp->Property.Access & DP_ACCESS_WRITE) || (forceWrite == szl_true))
        {
            // convert byteStream into a native data type if needed
            switch (dp->DataType)
            {
                case SZL_ZB_DATATYPE_OCTET_STR:
                case SZL_ZB_DATATYPE_CHAR_STR:
                    status = Szl_DataPointWriteString(service, dp, data, length, format);
                    break;

                default:
                    if (DataTypeIsPrimitive(dp->DataType))
                    {
                        status = Szl_DataPointWritePrimitive(service, dp, data, length, format);
                    }
                    break;
            }
        }
        else
        {
            status = SZL_STATUS_READ_ONLY;
        }
    }

    return status;
}

/**
 * Search a szl_uint16 array for a specific key (value)
 */
static szl_bool Uint16ArrayFindKey(szl_uint16 key, szl_uint16* list, szl_uint8 count, szl_uint8* rIdx)
{
  int idx;
    
    for (idx=0; idx < count; idx ++)
    {
        if (list[idx] == key)
        {
            if (rIdx != NULL)
            {
                *rIdx = idx;
            }
            return szl_true;
        }
    }

    return szl_false;
}


/**
 * This function is used to build a list of SERVER or CLIENT clusters based on the datapoints the app has registered
 * Return a array of clusterID's starting from (and including) searchFromID
 *
 * Returns szl_true  - if list is complete,
 *         szl_false - if more clusters remaining
 */
static szl_bool Szl_BuildClustersListFromDataPoints(zabService* service, ENUM_SZL_DP_DIRECTION_t direction, szl_uint8 ep, szl_uint16 searchFromID, szl_uint8 ListMaxSize, szl_uint16* List, szl_uint8* Count)
{
    int idx;

    // Add the clusters for the app data points
    for (idx=0; idx < SZL_CFG_MAX_DATAPOINTS; idx++)
    {
        if (LIB_DATA(service).Lib.DataPoints[idx].Endpoint == 0xFF &&
            LIB_DATA(service).Lib.DataPoints[idx].ClusterID == 0xFFFF &&
            LIB_DATA(service).Lib.DataPoints[idx].AttributeID == 0xFFFF )
            break; // empty entry - leave

        if ( !(LIB_DATA(service).Lib.DataPoints[idx].Endpoint == ep ||                   // specific endpoint match
               LIB_DATA(service).Lib.DataPoints[idx].Endpoint == SZL_ADDRESS_EP_ALL) || //   or all endpoints
               LIB_DATA(service).Lib.DataPoints[idx].Property.Direction != direction  || // direction match
               LIB_DATA(service).Lib.DataPoints[idx].ClusterID < searchFromID )          // ClusterID in range
            continue;

        // check if cluster already in the list
        if (Uint16ArrayFindKey( LIB_DATA(service).Lib.DataPoints[idx].ClusterID, List, *Count, NULL) == szl_false)
        {
            // not in list - add if more room
            if (*Count >= ListMaxSize)
                return szl_false; // list is incomplete

            List[(*Count) ++] = LIB_DATA(service).Lib.DataPoints[idx].ClusterID;
        }
    }

    /* Now add clusters that have commands registered*/
    for (idx = 0; idx < SZL_CFG_MAX_CLUSTER_CMDS; idx++)
      {
        if ( ( (LIB_DATA(service).Lib.ClusterCmds[idx].Endpoint == SZL_ADDRESS_EP_ALL) || (LIB_DATA(service).Lib.ClusterCmds[idx].Endpoint == ep) ) &&
             (LIB_DATA(service).Lib.ClusterCmds[idx].ClusterID != 0xFFFF) && (LIB_DATA(service).Lib.ClusterCmds[idx].CmdID != 0xFF) &&
             ( ( (direction == DP_DIRECTION_SERVER) &&
                 ( (LIB_DATA(service).Lib.ClusterCmds[idx].OperationType == SZL_CLU_OP_TYPE_CUSTOM_CLIENT_TO_SERVER) || 
                   (LIB_DATA(service).Lib.ClusterCmds[idx].OperationType <= SZL_CLU_OP_TYPE_MAX_) ) )
               ||
               ( (direction == DP_DIRECTION_CLIENT) &&
                 (LIB_DATA(service).Lib.ClusterCmds[idx].OperationType == SZL_CLU_OP_TYPE_CUSTOM_SERVER_TO_CLIENT) ) ) )
          {

            // check if cluster already in the list
            if (Uint16ArrayFindKey( LIB_DATA(service).Lib.ClusterCmds[idx].ClusterID, List, *Count, NULL) == szl_false)
            {
                // not in list - add if more room
                if (*Count >= ListMaxSize)
                    return szl_false; // list is incomplete

                List[(*Count) ++] = LIB_DATA(service).Lib.ClusterCmds[idx].ClusterID;
            }
          }
      }
    
    return szl_true;
}

/**
 * This function is used to build a list of "input" endpoints for this app to be bound to.
 * The list contains all the cluster ID's for all datapoints the app has registered
 * Return a array of clusterID's starting from (and including) searchFromID
 *
 * Returns szl_true  - if list is complete,
 *         szl_false - if more clusters remaining
 */
static szl_bool Szl_BuildInputClustersList(zabService* service, SZL_EP_t ep, szl_uint16 searchFromID, szl_uint8 ListMaxSize, szl_uint16* List, szl_uint8* Count)
{
    return Szl_BuildClustersListFromDataPoints(service, DP_DIRECTION_SERVER, ep, searchFromID, ListMaxSize, List, Count);
}

/**
 * This function is used to build a list of "output" endpoints for this app to be bound to.
 * The list contains all the cluster ID's for clientClusters the app has registered
 * Return a array of clusterID's starting from (and including) searchFromID
 *
 * Returns szl_true  - if list is complete,
 *         szl_false - if more clusters remaining
 */
static szl_bool Szl_BuildOutputClustersList(zabService* service, SZL_EP_t ep, szl_uint16 searchFromID, szl_uint8 ListMaxSize, szl_uint16* List, szl_uint8* Count)
{
    szl_uint8 idx;

    for (idx=0; idx < SZL_CFG_MAX_CLIENT_CLUSTERS; idx++)
    {
        if (LIB_DATA(service).Lib.ClientClusters[idx].Ep != ep ||
            LIB_DATA(service).Lib.ClientClusters[idx].ClusterID < searchFromID)
            continue; // below starting point

        if (Uint16ArrayFindKey( LIB_DATA(service).Lib.ClientClusters[idx].ClusterID, List, *Count, NULL) == szl_false)
        {
            if (*Count >= ListMaxSize)
                return szl_false; // list is incomplete
      
            List[(*Count) ++] = LIB_DATA(service).Lib.ClientClusters[idx].ClusterID;
        }
    }
    
    // add from datapoints
    return Szl_BuildClustersListFromDataPoints(service, DP_DIRECTION_CLIENT, ep, searchFromID, ListMaxSize, List, Count);
}

/**
 * This function returns a list of "input" endpoints for this app to be bound to.
 *
 * The list contains all the cluster ID's for all datapoints the app has registered
 */
SZL_STATUS_t Szl_GetInputClusters(zabService* service, SZL_EP_t ep, szl_uint8 ListMaxSize, szl_uint16* List, szl_uint8* Count)
{
    SZL_STATUS_t status = SZL_STATUS_SUCCESS;
    *Count = 0;

    // Todo - rework * Needs to be sorted on clusterID's before adding in case of list overrun
    if (Szl_BuildInputClustersList(service, ep, 0, ListMaxSize, List, Count) == szl_false)
    {
        status = SZL_STATUS_INSUFFICIENT_SPACE;
    }

    // we need to sort the list now
    bubble_sort(List, *Count, sizeof(szl_uint16), Szl_Uint16Compare);

    return status;
}


/**
 * This function returns a list of "output" endpoints for this app to be bound to.
 *
 * The list contains all the cluster ID's for all clientClusters the app has registered
 */
SZL_STATUS_t Szl_GetOutputClusters(zabService* service, SZL_EP_t ep, szl_uint8 ListMaxSize, szl_uint16* List, szl_uint8* Count)
{
    SZL_STATUS_t status = SZL_STATUS_SUCCESS;
    *Count = 0;

    // Todo - rework * Needs to be sorted on clusterID's before adding in case of list overrun
    if (Szl_BuildOutputClustersList(service, ep, 0, ListMaxSize, List, Count) == szl_false)
    {
        status = SZL_STATUS_INSUFFICIENT_SPACE;
    }

    // we need to sort the list now
    bubble_sort(List, *Count, sizeof(szl_uint16), Szl_Uint16Compare);

    return status;
}

/**
 * This function returns a list of attributes for a specific cluster.
 *
 * The list contains all the matching attribute ID's the app/plugins has registered
 */
SZL_STATUS_t Szl_GetAttributes(zabService* service, ENUM_SZL_DP_DIRECTION_t direction, szl_uint16 clusterID, SZL_EP_t ep, szl_bool manufacturerSpecific, szl_uint16 startAttributeID, szl_uint8 listMaxAttributes, szl_bool* listCompleted, void* list, szl_uint8* listCount)
{
    int idx;
    SZL_STATUS_t status = SZL_STATUS_SUCCESS;
    *listCount = 0;
    *listCompleted = szl_true;
    ZCL_DISCOVER_ATTRIBUTE_t* pList = (ZCL_DISCOVER_ATTRIBUTE_t*)list;

    for (idx=0; idx < SZL_CFG_MAX_DATAPOINTS; idx++)
    {
        if (LIB_DATA(service).Lib.DataPoints[idx].Endpoint == 0xFF &&
            LIB_DATA(service).Lib.DataPoints[idx].ClusterID == 0xFFFF &&
            LIB_DATA(service).Lib.DataPoints[idx].AttributeID == 0xFFFF )
            break; // rest is empty entries

        if ( (LIB_DATA(service).Lib.DataPoints[idx].Endpoint == ep ||                     // specific endpoint match
              LIB_DATA(service).Lib.DataPoints[idx].Endpoint == SZL_ADDRESS_EP_ALL)      // or no specific endpoints for the datapoint
             && LIB_DATA(service).Lib.DataPoints[idx].ClusterID == clusterID
             && LIB_DATA(service).Lib.DataPoints[idx].AttributeID >= startAttributeID
             && LIB_DATA(service).Lib.DataPoints[idx].Property.ManufacturerSpecific == manufacturerSpecific
             && LIB_DATA(service).Lib.DataPoints[idx].Property.Direction == direction )
        {
            if (listMaxAttributes <= *listCount)
            {
                *listCompleted = szl_false;
                break;
            }

            pList[*listCount].attribute_id = LIB_DATA(service).Lib.DataPoints[idx].AttributeID;
            pList[*listCount].data_type = LIB_DATA(service).Lib.DataPoints[idx].DataType;
            (*listCount) ++;
        }
    }

    bubble_sort(list, *listCount, sizeof(ZCL_DISCOVER_ATTRIBUTE_t), Szl_Uint16Compare);

    return status;
}


/**
 * Attribute Changed Notification callback
 *
 * This function calls all the registered callbacks for the attribute changed indication
 */
SZL_STATUS_t Szl_AttributeChangedNtfCB(zabService* service, SZL_DataPoint_t* dp)
{
    int searchIdx = -1; // mark first search before index 0
    SZL_CB_AttributeChangedNtf_t cbFunction = NULL;
    SZL_AttributeChangedNtfParams_t Params;
    Params.Endpoint = dp->Endpoint;
    Params.ManufacturerSpecific = dp->Property.ManufacturerSpecific;
    Params.ClusterID = dp->ClusterID;
    Params.AttributeID = dp->AttributeID;

    while ( (cbFunction = (SZL_CB_AttributeChangedNtf_t)CbHandler_Find(service, &searchIdx, CB_FUNC_ATTRIBUTE_CHANGED_NTF)) != NULL)
    {
         cbFunction(service, &Params);
    }

    return SZL_STATUS_SUCCESS;
}

/**
 * Attribute Report Notification callback
 *
 * This function calls all the registered callbacks for the attribute report indication
 */
SZL_STATUS_t Szl_AttributeReportNtfCB(zabService* service, SZL_AttributeReportNtfParams_t* Params)
{
    int searchIdx = -1; // mark first search before index 0
    SZL_CB_AttributeReportNtf_t cbFunction = NULL;

    while ( (cbFunction = (SZL_CB_AttributeReportNtf_t)CbHandler_Find(service, &searchIdx, CB_FUNC_ATTRIBUTE_REPORT_NTF)) != NULL)
    {
         cbFunction(service, Params);
    }

    return SZL_STATUS_SUCCESS;
}

/**
 * Received RSSI Notification callback
 *
 * This function calls all the registered callbacks for the Received RSSI notification
 */
SZL_STATUS_t Szl_ReceivedRssiNtfCB(zabService* Service, SZL_ReceivedSignalStrengthNtfParams_t* Params)
{
    int searchIdx = -1; // mark first search before index 0
    SZL_CB_ReceivedSignalStrengthNtf_t cbFunction = NULL;

    while ( (cbFunction = (SZL_CB_ReceivedSignalStrengthNtf_t)CbHandler_Find(Service, &searchIdx, CB_FUNC_NWK_RX_SIG_STR_NTF)) != NULL)
    {
         cbFunction(Service, Params);
    }

    return SZL_STATUS_SUCCESS;
}


/**
 * GPD Commissioning Notification callback
 *
 * This function calls all the registered callbacks for the GPD Commissioning Notification
 */
#ifdef SZL_CFG_ENABLE_GREEN_POWER
szl_bool Szl_GpdCommissioningNtfCB(zabService* Service, SZL_GP_GpdCommissioningNtfParams_t* Params, SZL_GP_COMMISSIONING_KEY_MODE_t* KeyMode)
{
    int searchIdx = -1; // mark first search before index 0
    SZL_CB_GpdCommissioningNtf_t cbFunction = NULL;
    szl_bool result = szl_false;

    /* If any callbackreturn true then we return true to accept the GPD */
    while ( (cbFunction = (SZL_CB_GpdCommissioningNtf_t)CbHandler_Find(Service, &searchIdx, CB_FUNC_GPD_COMMISSIONING_NTF)) != NULL)
    {
        if (cbFunction(Service, Params, KeyMode) == szl_true)
          {
            result = szl_true;
          }
    }

    return result;
}
#endif

/**
 * GPD Commissioned Notification callback
 *
 * This function calls all the registered callbacks for the GPD Commissioned Notification
 */
#ifdef SZL_CFG_ENABLE_GREEN_POWER
SZL_STATUS_t Szl_GpdCommissionedNtfCB(zabService* Service, SZL_GP_GpdCommissionedNtfParams_t* Params)
{
    int searchIdx = -1; // mark first search before index 0
    SZL_CB_GpdCommissionedNtf_t cbFunction = NULL;

    while ( (cbFunction = (SZL_CB_GpdCommissionedNtf_t)CbHandler_Find(Service, &searchIdx, CB_FUNC_GPD_COMMISSIONED_NTF)) != NULL)
    {
         cbFunction(Service, Params);
    }

    return SZL_STATUS_SUCCESS;
}
#endif

/**
 * Network Leave Notification callback
 *
 * This function calls all the registered callbacks for the network leave indication
 */
SZL_STATUS_t Szl_NwkLeaveNtfCB(zabService* service, SZL_NwkLeaveNtfParams_t* Params)


{
    int searchIdx = -1; // mark first search before index 0
    SZL_CB_NwkLeaveNtf_t cbFunction = NULL;

    while ( (cbFunction = (SZL_CB_NwkLeaveNtf_t)CbHandler_Find(service, &searchIdx, CB_FUNC_NWK_LEAVE_NTF)) != NULL)
    {
         cbFunction(service, Params);
    }

    return SZL_STATUS_SUCCESS;
}


/**
 * Is Request Valid
 *
 * This function is used in the API functions to determine if the App is allowed
 * to perform the specific action
 */
SZL_RESULT_t Szl_IsRequestValid(zabService* service, szl_uint16 appFeature )
{
    if ( !LIB_DATA(service).Lib.Initialized )
    {
        return SZL_RESULT_LIBRARY_NOT_INITIALIZED;
    }
    
    if (zabCoreService_GetZabNetworkReady(service) != zab_true)
      {
        return SZL_RESULT_OPERATION_NOT_POSSIBLE;
      }

//    if ((gLibData.Application.ApplicationType & appFeature) == 0)
//    {
//        return SZL_RESULT_FEATURE_INVALID_FOR_TYPE;
//    }

    return SZL_RESULT_SUCCESS;
}



/**
 * Cluster Cmd Find
 *
 * Find a registered Cluster Cmd in the table
 * returns:
 *          pointer to the Cluster Cmd if found
 *          null if not found
 */
SZL_ClusterCmd_t* Szl_ClusterCmdFind(zabService* service, int* startIdx, SZL_EP_t ep ,szl_bool manufacturerSpecific, szl_uint16 clusterID, szl_uint8 cmdID, szl_bool responseTypesOnly, szl_bool matchEmptyEntry)
{
    int idx = -1;    
    SZL_ClusterCmd_t* gClusterCmds;
    gClusterCmds = ZAB_SRV(service)->szlService.Lib.ClusterCmds;
    
    if (startIdx != NULL)
    {
        idx = *startIdx;
    }

    idx ++; // advance to next idx

    for (; idx < SZL_CFG_MAX_CLUSTER_CMDS; idx++)
    {
        if ((matchEmptyEntry == szl_true) &&
            gClusterCmds[idx].Endpoint == 0xFF &&
            gClusterCmds[idx].ClusterID == 0xFFFF &&
            gClusterCmds[idx].CmdID == 0xFF )
        {
            return NULL; // empty entry
        }

        // check endpoint
        if ( !( (gClusterCmds[idx].Endpoint == ep ||                    // specific endpoint match
                (gClusterCmds[idx].Endpoint == SZL_ADDRESS_EP_ALL &&
                 Szl_EndpointRegistered(service,ep)) ||                 // or all registered endpoints valid
                 ep == SZL_ADDRESS_EP_ALL) ) )                          // or a broadcast to all endpoints
        {
            // wrong endpoint
            continue;
        }
        
        // check manufacturer id
        if (gClusterCmds[idx].ManufacturerSpecific != manufacturerSpecific )
        {
            // wrong manufacture id
            continue;
        }
        
        // check operation type
        if ( (responseTypesOnly == szl_true && gClusterCmds[idx].OperationType != SZL_CLU_OP_TYPE_CUSTOM_SERVER_TO_CLIENT ) ||
             (responseTypesOnly == szl_false && gClusterCmds[idx].OperationType == SZL_CLU_OP_TYPE_CUSTOM_SERVER_TO_CLIENT ) )
        {
            // wrong operation type
            continue;
        }

        // check the rest
        if (gClusterCmds[idx].ClusterID != clusterID ||
            gClusterCmds[idx].CmdID != cmdID )
        {
            // wrong clusterID and cmdId
            continue;
        }

        // we have a match
        if (startIdx)
        {
            *startIdx = idx;
        }
        return &gClusterCmds[idx];
    }

    return NULL;
}


/**
 * This is the Cluster Command Handler
 *
 * @param[in]  clusterID          The clusterID for the command 
 * @param[in]  cmd                  The commandID
 * @param[in]  ep                   The endpoint index
 * @param[in]  ManufacturerSpecific  True if operation is manufacturer specific
 * @param[in]  leDataIn             The pointer to the payload from the command (The data is in LITTLE ENDIAN)
 * @param[in]  leDataInLen          The length of the payload for the command
 * @param[out] cmdOut               The commandID to be send together with the response payload (ignore if no payload to send)
 * @param[out] leDataOut            The pointer to the payload buffer to copy the response payload to (The data is in LITTLE ENDIAN) (ignore if no payload to send)
 * @param[out] leDataOutLen         The length of the response payload (set to zero if no response payload
 * @param[in]  serverToClient       The direction of the command
 * @param[in]  TransactionId        The ZCL Transaction ID of the incoming command
 * @param[in]  sourceAddress        The source of the incoming command
 *
 * @return -
 */
SZL_STATUS_t Szl_ClusterCommandHandler(zabService* service, 
                                       szl_uint16 ClusterID, 
                                       szl_uint8 cmd, 
                                       SZL_EP_t ep, 
                                       szl_bool manufacturerSpecific, 
                                       szl_uint8* leDataIn, szl_uint8 leDataInLen, 
                                       szl_uint8* cmdOut, 
                                       szl_uint8* leDataOut, szl_uint8* leDataOutLen, 
                                       szl_bool serverToClient, 
                                       szl_uint8 TransactionId,
                                       SZL_Addresses_t sourceAddress,
                                       SZL_ADDRESS_MODE_t destAddressMode)
{
    SZL_RESULT_t status = manufacturerSpecific ? SZL_STATUS_UNSUP_MANU_CLUSTER_COMMAND : SZL_STATUS_UNSUP_CLUSTER_COMMAND;


    // find the cluster in the registered cluster commands table
    SZL_ClusterCmd_t* clusterCmdEntry = Szl_ClusterCmdFind(service, NULL, ep, manufacturerSpecific, ClusterID, cmd, serverToClient, szl_true);

    if (clusterCmdEntry == NULL)
        return status;

    // determine if use default handler or CB handler
    if ( (clusterCmdEntry->OperationType == SZL_CLU_OP_TYPE_CUSTOM_CLIENT_TO_SERVER ) ||
         (clusterCmdEntry->OperationType == SZL_CLU_OP_TYPE_CUSTOM_SERVER_TO_CLIENT ) )
    {
        if (clusterCmdEntry->OperationAccess.Callback != NULL &&
            clusterCmdEntry->OperationAccess.Callback(service, ep, manufacturerSpecific, ClusterID, cmd, leDataIn, leDataInLen, cmdOut, leDataOut, leDataOutLen, TransactionId, sourceAddress, destAddressMode) == szl_true)
        {
            status  = SZL_STATUS_SUCCESS;
            // we can not inform the report as we do not know if data is changed - must be done from application
        }
    }
    else
    {
        if (Szl_ClusterCommandDefaultOperation(service, clusterCmdEntry, ep, leDataIn, leDataInLen, cmdOut, leDataOut, leDataOutLen) == szl_true)
        {
            status  = SZL_STATUS_SUCCESS;
        }
    }

    // handle the response data and return success
    return status;
}

/**
 * This is the Default Cluster Command Handler
 *
 * @param[in]  ClusterCmdEntry      The registered clusterCmd
 * @param[in]  ep                   The endpoint index
 * @param[in]  leData               The pointer to the payload from the command (The data is in LITTLE ENDIAN)
 * @param[in]  leDataLen            The length of the payload for the command
 * @param[out] cmdResponse          The commandID to be send together with the response payload (ignore if no payload to send)
 * @param[out] leDataResponse       The pointer to the payload buffer to copy the response payload to (The data is in LITTLE ENDIAN) (ignore if no payload to send)
 * @param[out] leDataResponseLen    The length of the response payload (set to zero if no response payload
 */
static szl_bool Szl_ClusterCommandDefaultOperation(zabService* service, SZL_ClusterCmd_t* clusterCmdEntry, SZL_EP_t ep, szl_uint8* leData, szl_uint8 leDataLen,szl_uint8* cmdResponse, szl_uint8* leDataResponse, szl_uint8* leDataResponseLen)
{
    NATIVE_DATA_t alignedNativeData;
    szl_uint8 nativeDataLen;

    // find the data point
    SZL_DataPoint_t* dp = Szl_DataPointFind(service, ep, clusterCmdEntry->OperationAccess.Attribute.ManufacturerSpecific, clusterCmdEntry->ClusterID, clusterCmdEntry->OperationAccess.Attribute.AttributeID, szl_true);

    if (dp == NULL ||                                                       // data point not found
        !DataTypeIsPrimitive( dp->DataType) ||                              // Only operate on primitive types
        NativeDataSize(dp->DataType, NULL) == 0 ||                          // must have a data size
        (leDataLen > 0 && leDataLen != ZbDataSize(dp->DataType, NULL) )  || // payload length must match data type length
        (dp->DataSize > sizeof(NATIVE_DATA_t)) )                            // no data greater than 8 bytes
        return szl_false;

    // no data greater than 8 bytes
    nativeDataLen = dp->DataSize;
    szl_memset(&alignedNativeData, 0, sizeof(NATIVE_DATA_t));

    // step 1 - do the read
    switch (clusterCmdEntry->OperationType)
    {
        case SZL_CLU_OP_TYPE_INC:
        case SZL_CLU_OP_TYPE_DEC:
        case SZL_CLU_OP_TYPE_GET:
        case SZL_CLU_OP_TYPE_TOGGLE:
            switch (dp->Property.Method)
            {
                case DP_METHOD_CALLBACK:
                    if (dp->AccessType.Callback.Read(service, dp->Endpoint, dp->Property.ManufacturerSpecific, dp->ClusterID, dp->AttributeID, &alignedNativeData, &nativeDataLen) != SZL_RESULT_SUCCESS || nativeDataLen == 0)
                        return szl_false;
                    break;

                case DP_METHOD_DIRECT:
                    nativeDataLen = dp->DataSize;
                    szl_memcpy(&alignedNativeData, dp->AccessType.DirectAccess.Data, nativeDataLen);
                    break;
            }
            break;
    }

    // step 2 - do data processing
    switch (clusterCmdEntry->OperationType)
    {
        case SZL_CLU_OP_TYPE_INC:
            if ( !NativeDataInc(&alignedNativeData, nativeDataLen, NativePadSize(dp->DataType), 1) )
                return szl_false;
            break;

        case SZL_CLU_OP_TYPE_DEC:
            if ( !NativeDataDec(&alignedNativeData, nativeDataLen, NativePadSize(dp->DataType), 1) )
                return szl_false;
            break;

        case SZL_CLU_OP_TYPE_SET:
            {
            if ( !ZbToNative(dp->DataType, leData, leDataLen, &alignedNativeData, &nativeDataLen) )
                return szl_false;
            }
            break;

        case SZL_CLU_OP_TYPE_GET:
            // we already read the data, now convert from native to zigbee
            *cmdResponse = 0; // Todo maybe specify in the ClusterCmdRegister which cmd to reply with
            *leDataResponseLen = NativeToZb(&alignedNativeData, nativeDataLen, dp->DataType, leDataResponse);

            if (*leDataResponseLen == 0)
                return szl_false;
            break;

        case SZL_CLU_OP_TYPE_ON:
            if ( !NativeDataSet(&alignedNativeData, nativeDataLen, NativePadSize(dp->DataType), (szl_uint32)szl_true) )
                return szl_false;
            break;

        case SZL_CLU_OP_TYPE_OFF:
            if ( !NativeDataSet(&alignedNativeData, nativeDataLen, NativePadSize(dp->DataType), (szl_uint32)szl_false) )
                return szl_false;
            break;

        case SZL_CLU_OP_TYPE_TOGGLE:
            if ( !NativeDataSet(&alignedNativeData, nativeDataLen, NativePadSize(dp->DataType), LARGE_DATATYPE_IS_ZERO(alignedNativeData.data.primitive64) ? (szl_uint32) 1 : (szl_uint32) 0) )
                return szl_false;
            break;

        default:
            return szl_false;
    }

    // step 3 - do write
    switch (clusterCmdEntry->OperationType)
    {
        case SZL_CLU_OP_TYPE_INC:
        case SZL_CLU_OP_TYPE_DEC:
        case SZL_CLU_OP_TYPE_SET:
        case SZL_CLU_OP_TYPE_ON:
        case SZL_CLU_OP_TYPE_OFF:
        case SZL_CLU_OP_TYPE_TOGGLE:
            switch (dp->Property.Method)
            {
                case DP_METHOD_CALLBACK:
                    if (dp->AccessType.Callback.Write(service, dp->Endpoint, dp->Property.ManufacturerSpecific, dp->ClusterID, dp->AttributeID, &alignedNativeData, nativeDataLen) != SZL_RESULT_SUCCESS )
                        return szl_false;
                    break;

                case DP_METHOD_DIRECT:
                    szl_memcpy(dp->AccessType.DirectAccess.Data, &alignedNativeData, nativeDataLen);
                    Szl_AttributeChangedNtfCB(service, dp);
                    break;
            }
            //Szl_DatapointChanged(service, dp);
            break;
    }

    // made it so far - everything is OK
    return szl_true;
}


ENUM_SZL_DP_ACCESS_t Szl_ClusterCmdOperation2AccessType(SZL_CLU_OP_TYPE_t opType)
{
    switch (opType)
    {
        case SZL_CLU_OP_TYPE_TOGGLE:
        case SZL_CLU_OP_TYPE_INC:
        case SZL_CLU_OP_TYPE_DEC:
            return DP_ACCESS_RW;

        case SZL_CLU_OP_TYPE_GET:
            return DP_ACCESS_READ;

        case SZL_CLU_OP_TYPE_SET:
        case SZL_CLU_OP_TYPE_ON:
        case SZL_CLU_OP_TYPE_OFF:
            return DP_ACCESS_WRITE;
    }

    return DP_ACCESS_NONE;
}


static szl_bool NativeDataInc(void* nativeData, szl_uint8 nativeDataSize, szl_uint8 padSize, szl_byte value )
{
    switch (nativeDataSize)
    {
        case 1:
            *((szl_uint8*)nativeData) = *((szl_uint8*)nativeData) + value;
            break;

        case 2:
            *((szl_uint16*)nativeData) = *((szl_uint16*)nativeData) + value;
            break;

        case 4:
            *(szl_uint32*)nativeData = *((szl_uint32*)nativeData) + value;
            switch (padSize)
            {
                case 0:
                    break;

                case 1: // 24 bits
                    if ( (*((szl_uint32*)nativeData) >> 24) > 0)
                        *((szl_uint32*)nativeData) = *((szl_uint32*)nativeData) >> (24 - 1);
                    break;

                default:
                    return szl_false;
            }
            break;

//        case 8:
//            // TODO
//            break;

        default:
            return szl_false;
    }

    return szl_true;
}


static szl_bool NativeDataDec(void* nativeData, szl_uint8 nativeDataSize, szl_uint8 padSize, szl_byte value )
{
    switch (nativeDataSize)
    {
        case 1:
            *((szl_uint8*)nativeData) = *((szl_uint8*)nativeData) - value;
            break;

        case 2:
            *((szl_uint16*)nativeData) = *((szl_uint16*)nativeData) - value;
            break;

        case 4:
            *(szl_uint32*)nativeData = *((szl_uint32*)nativeData) - value;
            switch (padSize)
            {
                case 0:
                    break;

                case 1: // 24 bits
                    *((szl_uint32*)nativeData) = *((szl_uint32*)nativeData) & 0x00FFFFFF;
                    break;

                default:
                    return szl_false;
            }
            break;

//        case 8:
//            // TODO
//            break;

        default:
            return szl_false;
    }

    return szl_true;
}


static szl_bool NativeDataSet(void* nativeData, szl_uint8 nativeDataSize, szl_uint8 padSize, szl_uint32 value )
{
    switch (nativeDataSize)
    {
        case 1:
            *((szl_uint8*)nativeData) = (szl_uint8)value;
            break;

        case 2:
            *((szl_uint16*)nativeData) = (szl_uint16)value;
            break;

        case 4:
            switch (padSize)
            {
                case 0:
                    *(szl_uint32*)nativeData = value;
                    break;

                case 1: // 24 bits
                    *(szl_uint32*)nativeData = value & 0x00FFFFFF;
                    break;

                default:
                    return szl_false;
            }
            break;

//        case 8:
//            // TODO
//            break;

        default:
            return szl_false;
    }

    return szl_true;
}
