# *******************************************************************************
#
#   Schneider Electric - Wireless Connectivity Team
#
#   IMPLEMENTATION DESCRIPTION:
#       Module to model ZigBee Green Power Device
#
#   PRODUCT SUPPORT:
#        Win32 - requires python 3.6.3
#
# *******************************************************************************

# ***************************
# Load additional libraries
# ***************************
from abc import abstractmethod
from data_model.zigbee_device import ZigBeeDevice


class ZigBeeGreenPowerDevice(ZigBeeDevice):

    def __init__(self, address, id_wireless_bridge):
        # call super function
        super(ZigBeeGreenPowerDevice, self).__init__(address, id_wireless_bridge)

    def read_attribute(self, cluster_id, attribute_id):
        pass

    def write_attribute(self, cluster_id, attribute_id, value):
        pass

    def discovery(self):
        pass

    @abstractmethod
    def update_data(self, payload):
        pass

