/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the Radio Factory Test functions.
 *
 *    - Initialisation:
 *      - zabNcpRft_Init()
 *      - zabNcpEzspMfglib_Start()        -> StandardResponseCallback()
 *
 *    - Unmodulated Carrier Test:
 *      - zabNcpEzspMfglib_SetChannel()   -> StandardResponseCallback()
 *      - zabNcpEzspMfglib_SetPower()     -> StandardResponseCallback()
 *      - zabNcpEzspMfglib_StartTone()    -> StandardResponseCallback()
 *      - ...
 *      - zabNcpEzspMfglib_StopTone()     -> StandardResponseCallback()
 *
 *    - Modulated Carrier Test:
 *      - zabNcpEzspMfglib_SetChannel()   -> StandardResponseCallback()
 *      - zabNcpEzspMfglib_SetPower()     -> StandardResponseCallback()
 *      - zabNcpEzspMfglib_StartStream()  -> StandardResponseCallback()
 *      - ...
 *      - zabNcpEzspMfglib_StopStream()   -> StandardResponseCallback()
 *
 *    - Packet Receive Test:
 *      - zabNcpEzspMfglib_SetChannel()   -> StandardResponseCallback()
 *      - zabNcpEzspMfglib_SetPower()     -> StandardResponseCallback()
 *      - zabNcpRft_StartPacketReceiveTest()
 *      - ...
 *      - PacketReceivedIndCallback called by ZAB on packet rx
 *      - App responds by calling zabNcpRft_SendPacketReceivedResponse() -> StandardResponseCallback()
 *      - ...
 *      - zabNcpRft_StopPacketReceiveTest()
 *
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 000.000.012  19-Jul-16   MvdB   Original
 *****************************************************************************/

#ifndef _ZAB_NCP_RFT_H_
#define _ZAB_NCP_RFT_H_

#include "ember-types.h"

#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Types of requests initiated by the service
 ******************************************************************************/
typedef enum
{
  zabNcpRft_RequestType_None,
  zabNcpRft_RequestType_GetCtuneMfgToken,
  zabNcpRft_RequestType_SetCtuneMfgToken,
  zabNcpRft_RequestType_PacketTestRunning,
}zabNcpRft_RequestType_t;

/******************************************************************************
 * Prototype for Standard Response Callback
 *
 * Used for all RFT related commands that have a response with just an EmberStatus:
 *  - zabNcpEzspMfglib_Start
 *  - zabNcpEzspMfglib_End
 *  - zabNcpEzspMfglib_StartTone
 *  - zabNcpEzspMfglib_StopTone
 *  - zabNcpEzspMfglib_StartStream
 *  - zabNcpEzspMfglib_StopStream
 *  - zabNcpEzspMfglib_SetChannel
 *  - zabNcpEzspMfglib_SetPower
 *  - zabNcpEzspMfglib_SetCtune
 *  - zabNcpEzspUtilities_SetMfgToken - replace with rft func
 *  - zabZnpRftUtility_PingResponse
 ******************************************************************************/
typedef void (*zabNcpRft_CB_StandardResponse) (zabService* Service, unsigned8 ResponseStatus);

/******************************************************************************
 * Prototype for Received Packet Indication Callback
 ******************************************************************************/
typedef void (*zabNcpRft_CB_PacketReceivedInd) (zabService* Service, unsigned8 RSSI, unsigned32 FrameCounter);

/******************************************************************************
 * Prototype for Get FreqTune Callback
 *
 * Used for all RFT related commands that have a response with a CTune value:
 *  - zabNcpRft_GetCTuneRequest()
 *  - zabNcpRft_GetStoredCTuneRequest()
 ******************************************************************************/
typedef void (*zabNcpRft_CB_GetCTuneResponse) (zabService* Service, unsigned16 CTune);

/******************************************************************************
 * ZAB internal parameter storage
 ******************************************************************************/
typedef struct
{
  zabNcpRft_RequestType_t RequestType;
  unsigned8 MacSequenceNumber;
  unsigned32 SourceId;
  zabNcpRft_CB_StandardResponse StandardResponseCallback;
  zabNcpRft_CB_PacketReceivedInd PacketReceivedIndCallback;
  zabNcpRft_CB_GetCTuneResponse GetCTuneResponseCallback;
} zabNcpRftInfo_t;


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise Radio Factory Test with a source ID and callbacks
 ******************************************************************************/
void zabNcpRft_Init(erStatus* Status, zabService* Service,
                    unsigned32 SourceID,
                    zabNcpRft_CB_StandardResponse StandardResponseCallback,
                    zabNcpRft_CB_PacketReceivedInd PacketReceivedIndCallback,
                    zabNcpRft_CB_GetCTuneResponse GetCTuneResponseCallback);


/******************************************************************************
 * Start the Packet Receive Test
 ******************************************************************************/
void zabNcpRft_StartPacketReceiveTest(erStatus* Status, zabService* Service);

/******************************************************************************
 * Stop the Packet Receive Test
 ******************************************************************************/
void zabNcpRft_StopPacketReceiveTest(erStatus* Status, zabService* Service);

/******************************************************************************
 * Send a Packet Received Response - For responses during receive test
 ******************************************************************************/
void zabNcpRft_SendPacketReceivedResponse(erStatus* Status, zabService* Service,
                                          unsigned32 FrameCounter,
                                          unsigned8 Rssi);

/******************************************************************************
 * Get RAM value of C Tune
 ******************************************************************************/
void zabNcpRft_GetCTuneRequest(erStatus* Status, zabService* Service);

/******************************************************************************
 * Set RAM value of C Tune
 ******************************************************************************/
void zabNcpRft_SetCTuneRequest(erStatus* Status, zabService* Service,
                               unsigned16 CTune);

/******************************************************************************
 * Get Stored (Non-Volatile) value of C Tune
 ******************************************************************************/
void zabNcpRft_GetStoredCTuneRequest(erStatus* Status, zabService* Service);

/******************************************************************************
 * Set Stored (Non-Volatile) value of C Tune
 ******************************************************************************/
void zabNcpRft_SetStoredCTuneRequest(erStatus* Status, zabService* Service,
                                     unsigned16 CTune);


/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                 *****     INTERNAL USE ONLY!!!     *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Create Radio Factory Test service
 ******************************************************************************/
void zabNcpRft_Create(erStatus* Status, zabService* Service);

/******************************************************************************
 * General Response Handler
 * Used for all RFT related commands that have a response with just an EmberStatus:
 *  - zabNcpEzspMfglib_Start
 *  - zabNcpEzspMfglib_End
 *  - zabNcpEzspMfglib_StartTone
 *  - zabNcpEzspMfglib_StopTone
 *  - zabNcpEzspMfglib_StartStream
 *  - zabNcpEzspMfglib_StopStream
 *  - zabNcpEzspMfglib_SetChannel
 *  - zabNcpEzspMfglib_SetPower
 *  - zabNcpEzspMfglib_SetCtune
 *  - zabNcpEzspUtilities_SetMfgToken - replace with rft func
 *  - zabZnpRftUtility_PingResponse
 ******************************************************************************/
void zabNcpRft_GeneralResponseHandler(erStatus* Status, zabService* Service, EmberStatus ResponseStatus);

/******************************************************************************
 * Get CTune Response Handler
 * Used for all RFT related commands that get a CTune value
 *  - zabNcpRft_GetCTuneRequest
 *  - zabNcpRft_GetStoredCTuneRequest
 ******************************************************************************/
void zabNcpRft_GetFreqTuneResponseHandler(erStatus* Status, zabService* Service, unsigned16 Ctune);

/******************************************************************************
 * Get Manufacturing Token Response Handler
 * Used for zabNcpRft_GetStoredCTuneRequest
 ******************************************************************************/
void zabNcpRft_GetMfgTokenResponseHandler(erStatus* Status, zabService* Service, unsigned8 TokenDataLength, unsigned8* TokenData);

/******************************************************************************
 * Set Manufacturing Token Response Handler
 ******************************************************************************/
void zabNcpRft_SetMfgTokenResponseHandler(erStatus* Status, zabService* Service, EmberStatus ResponseStatus);

/******************************************************************************
 * Get Manufacturing Token Response Handler
 * Used for zabNcpRft_GetStoredCTuneRequest
 ******************************************************************************/
void zabNcpRft_RxHandler(erStatus* Status, zabService* Service, unsigned8 Rssi, unsigned8 PayloadLength, unsigned8* PayloadData);

#ifdef __cplusplus
}
#endif

#endif /* _ZAB_NCP_RFT_H_ */
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/