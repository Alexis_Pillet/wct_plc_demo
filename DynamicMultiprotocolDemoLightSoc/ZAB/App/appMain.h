/******************************************************************************
 *                          ZAB TEST APPLICATION
 * 
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the main entry point for the test console, and:
 *    - Functions to launch serial and work threads
 *    - ZAB Initialisation
 *    - Console management
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 *****************************************************************************/
#ifndef _APP_MAIN_H_
#define _APP_MAIN_H_
 
#include "osTypes.h"
#include "zabTypes.h"
#include "szl.h"
#include "szl_api.h"
#include "app/framework/include/af.h"
#include "inter_task.h"

/* Define to include serial debug code. It can then be controlled by EnableSerialDebug*/
#define INCLUDE_SERIAL_DEBUG



typedef enum
{
    ZB_ADDRESS_MODE_INVALID = 0,   /**< The address mode has not been set to a valide mode */
    ZB_ADDRESS_MODE_IEEE_ADDRESS,  /**< using the IEEE Address and the Endpoint field, All other fields ignored. */
    ZB_ADDRESS_MODE_GP_SOURCE_ID,  /**< Green Power Source Id */
} ZB_ADDRESS_MODE_t;
/**
 * IEEE Address mode
 */
typedef struct
{
    uint64_t Address;     /**< The IEEE address for a device also known as the Extended address */
    uint8_t Endpoint;      /**< The Endpoint for the device */
}ZB_ADDR_IEEE_t;

/**
 * Green Power Device - Source Id
 */
typedef struct
{
    uint32_t SourceId;    /**< The Source ID of the Green Power Device */
} ZB_ADDR_GP_SOURCE_ID_t ;
typedef struct
{
    ZB_ADDRESS_MODE_t AddressMode;    /**< The address mode */
    union {
        ZB_ADDR_IEEE_t IEEE;
        ZB_ADDR_GP_SOURCE_ID_t GpSourceId; // Green Power Device using Source ID only.
    } Addresses;
} ZB_Addresses_t;
/**
 * Type for AttributeData.
 * This struct holds the definition for a attribute data value.
 */
typedef struct
{
    uint16_t AttributeID;                 /**< AttributeID for the Attribute */
    uint8_t Status;                    /**< The Status for the attribute - If NOT SUCCESS none of the following data is valid */
    uint8_t DataType;        /**< The data type for the Attribute - if type is not present then set to SZL_ZB_DATATYPE_UNKNOWN */
    uint8_t DataLength;                   /**< The length of the data in bytes - if 0 then no data is available and Data should be null*/
    void* Data;                             /**< pointer to the data - @note all data will be in native endianness and in App data type */
} ZB_AttributeData_t;

typedef struct
{
    ZB_Addresses_t Address;    /**< The address */
	uint16_t ClusterID;						/**< The Cluster ID for attributes */
    uint8_t NumberOfAttributes;                   /**< The number of attributes in the following Attributes array */
    ZB_AttributeData_t Attributes[5];   /**< Attribute's - (extends beyond bounds) */
} ZB_Report_t;

  
/******************************************************************************
 *                      ******************************
 *                 *****       EXTERN VARIABLES       *****
 *                      ******************************
 ******************************************************************************/  

/* Service pointer for our ZAB instance */
extern zabService* appService; 

/* Variable to allow pausing of calling of work function.
 * Useful for testing full queues */
extern zab_bool appMain_WorkEnabled;


#define ZIGBEE_APP_FLAG			0xf
#define ZIGBEE_APP_CLI_FLAG		0x1
#define ZIGBEE_APP_REPORT_FLAG	0x2
#define ZIGBEE_APP_NOTIF_FLAG	0x4


#define NB_MAX_REPORT	10
extern uint8_t nextToWrite;
extern uint8_t nextToRead;
extern SZL_AttributeReportNtfParams_t AttributReport[NB_MAX_REPORT];

extern OS_FLAG_GRP MyEventFlagGrp;

//msgQ management heap and current elements
#define MSG_MAX_SIZE     180
#define MSGQ_HEAP_SIZE   2000
extern uint8_t HeapMsgQ[MSGQ_HEAP_SIZE];
extern uint16_t current;
extern uint8_t asnValue;

//cluster command parser
#define CL_OPT_RSP				0x01
#define CL_OPT_SRC_EP			0x02
#define CL_OPT_DEST_ADR_MASK	0x0C
#define CL_OPT_DEST_ADR_SRCID	0x00
#define CL_OPT_DEST_ADR_IEEE	0x04
#define CL_OPT_DST_EP			0x10
#define CL_OPT_DIRECTION		0x02

#define ON_OFF_CLUSTER_ID		0x0006

#define FORM_NOARG      0x0
#define FORM_ARG_EPID   0x1
#define FORM_ARG_CHAN   0x2
#define FORM_ARG_PANID  0x4
#define FORM_MASK       0x7
/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/
void appMain_WorkRequired(void);
void initAppZigbee(void);
unsigned8 appMain_GetServiceId(zabService* Service);
void * zabProcess(void);
void startSerial(void);
//void uiProcess(char* data);
void printReceive(void);
void serialReader(unsigned8 realRcvLength,unsigned8* rcvBuff);

//void reportProcess(void);
//void notifyProcess(void);


void manageClusterCmd(uint8_t* command, uint8_t cmdSize);

void sendDefaultResp(uint8_t tid, uint8_t res);

uint8_t formNetworkCmd(uint8_t argument,unsigned64 Epid, uint8_t channel, uint16_t panId);
uint8_t permitJoin(uint16_t timeForJoin);

#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/