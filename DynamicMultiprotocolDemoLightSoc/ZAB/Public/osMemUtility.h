/******************************************************************************
 *                        ZigBee Application Brick
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the protoypes for memory mangement functions in ZAB.
 *   Functions must be implemented by the user in the application.
 *
 * AUTHOR:
 *   ZigBee Excellence Center - Mark van den Broeke
 *
 * MODIFICATION HISTORY:
 *  Vendor Rev    Date     Author  Change Description
 * 00.00.02.00  22-Oct-13   MvdB   Tidy up
 *                                 Add OS_MEM_UTILITY_USE_MALLOC_ID
 * 00.00.06.03  01-Oct-14   MvdB   artf104879: Support energy scans via Mgmt Nwk Update Request
 * 002.002.010  09-Oct-15   MvdB   ARTF150980: Support Service ID in memory allocation functions for WTB
 * 002.002.021  21-Apr-16   MvdB   ARTF167807: Support Multi-Cluster Attribute Read/Write for GP
 * 002.002.035  11-Jan-17   MvdB   ARTF170823: Make Read/Write Reporting configuration data (SZL_AttributeReportData_t) consistent with Read/Write data (SZL_AttributeData_t)
 *****************************************************************************/

#ifndef __OS_MEMORY_UTILITY_H__
#define __OS_MEMORY_UTILITY_H__

#include "osTypes.h"
#include "erStatusTypes.h"

#ifdef __cplusplus
extern "C" {
#endif

  /******************************************************************************
 *                      *****************************
 *                 *****          SETTINGS           *****
 *                      *****************************
 ******************************************************************************/

/******************************************************************************
 * Define this to include a unique enumeration in each malloc.
 * This makes it very easy to track Malloc and Free and find any memory leaks.
 * It also adds ServiceId, which used to track what instance an allocation comes from.
 ******************************************************************************/
#define OS_MEM_UTILITY_USE_MALLOC_ID


/******************************************************************************
 *                      *****************************
 *                 *****          TYPEDEFS           *****
 *                      *****************************
 ******************************************************************************/

/* This enum is created to work with the local malloc replacement
 * An id can be assigned for each malloc, then we run a timer to ensure they are free'd*/
typedef enum
{
  MALLOC_ID_NONE,
  MALLOC_ID_ZDO_IEEE_RSP,
  MALLOC_ID_ZDO_IEEE_FAKE_RSP,
  MALLOC_ID_ZDO_NWK_FAKE_RSP,
  MALLOC_ID_ZDO_ACTIVE_EP_RSP,
  MALLOC_ID_ZDO_ACTIVE_EP_FAKE_RSP,
  MALLOC_ID_ZDO_SIMPLE_DESC_FAKE_RSP,
  MALLOC_ID_ZDO_MATCH_DESC_FAKE_RSP,
  MALLOC_ID_ZDO_MGMT_LQI_FAKE_RSP,
  MALLOC_ID_ZDO_MGMT_RTG_FAKE_RSP,
  MALLOC_ID_ZDO_MGMT_BIND_FAKE_RSP,
  MALLOC_ID_ZDO_BIND_FAKE_RSP,
  MALLOC_ID_ZDO_UNBIND_FAKE_RSP,
  MALLOC_ID_ZDO_LEAVE_FAKE_RSP,
  MALLOC_ID_ZDO_USER_DESC_FAKE_RSP,
  MALLOC_ID_ZDO_USER_DESC_SET_FAKE_RSP,
  MALLOC_ID_ZDO_POWER_DESC_FAKE_RSP,
  MALLOC_ID_ZDO_NODE_DESC_FAKE_RSP,
  MALLOC_ID_SZL_READ_ATTR_REQ,
  MALLOC_ID_SZL_READ_ATTR_RSP,
  MALLOC_ID_SZL_READ_ATTR_FAKE_RSP,
  MALLOC_ID_SZL_READ_ATTR_RSP_NATIVE_DATA,
  MALLOC_ID_SZL_READ_ATTR_CFM,
  MALLOC_ID_SZL_MULTI_CLUSTER_READ_ATTR_FAKE_RSP,
  MALLOC_ID_SZL_WRITE_ATTR_REQ,
  MALLOC_ID_SZL_WRITE_ATTR_RSP,
  MALLOC_ID_SZL_WRITE_ATTR_FAKE_RSP,
  MALLOC_ID_SZL_WRITE_ATTR_CFM,
  MALLOC_ID_SZL_MULTI_CLUSTER_WRITE_ATTR_FAKE_RSP,
  MALLOC_ID_SZL_DISCOVER_ATTR_RSP,
  MALLOC_ID_SZL_DISCOVER_ATTR_FAKE_RSP,
  MALLOC_ID_SZL_CFG_REP_CFM,
  MALLOC_ID_SZL_CFG_REP_FAKE_RSP,
  MALLOC_ID_SZL_READ_REP_FAKE_RSP,
  MALLOC_ID_SZL_READ_REP_CFM,
  MALLOC_ID_SZL_READ_REP_CFM_NATIVE_DATA,
  MALLOC_ID_SZL_REP_NTF,
  MALLOC_ID_SZL_REP_NTF_NATIVE_DATA,
  MALLOC_ID_SZL_CLUSTER_CMD_RSP,
  MALLOC_ID_SZL_CLUSTER_CMD_FAKE_RSP,
  MALLOC_ID_SZL_CLUSTER_CMD_CFM,
  MALLOC_ID_SZL_EP_REGISTER,
  MALLOC_ID_SZL_NV_READ,
  MALLOC_ID_APP_SIMP_DESC,
  MALLOC_ID_PLG_DISC_EP_ITEM,
  MALLOC_ID_PLG_DISC_EP_INFO,
  MALLOC_ID_PLG_DISC_READ_ATTR,
  MALLOC_ID_PLG_DISC_EP_LIST,
  MALLOC_ID_SZL_GP_REP_NTF,
  MALLOC_ID_SZL_GP_MAN_EP,
  MALLOC_ID_SZL_GP_TEMP_BUFFER,
  MALLOC_ID_ZDO_MGMT_NWK_UPDATE_FAKE_RSP,
  MALLOC_ID_PLG_GPC_CMD_REQ,

  /* General ID that can be used by the application when it wants to do stuff */
  MALLOC_ID_APP_GENERAL,

  /* Put permanent, configuration data that is only free'd on ZAB destroy after MALLOC_ID_CFG_DATA_MIN.
   * This makes it easy to ignore when we check for malloced data that is not removed */
  MALLOC_ID_CFG_DATA_MIN,
  MALLOC_ID_CFG_VENDOR,
  MALLOC_ID_CFG_CORE,
  MALLOC_ID_CFG_QUEUE,
  MALLOC_ID_CFG_SAP,
  MALLOC_ID_CFG_DPM,
  MALLOC_ID_CFG_APP_SERIAL,
  MALLOC_ID_CFG_PLG_DISC,
  MALLOC_ID_CFG_PLG_EM,
  MALLOC_ID_CFG_PLG_ID,
  MALLOC_ID_CFG_PLG_OO,
  MALLOC_ID_CFG_PLG_BASIC,
  MALLOC_ID_CFG_PLG_METERING,
  MALLOC_ID_CFG_DISC_NODE_INFO,
  MALLOC_ID_CFG_PLG_TIME,
  MALLOC_ID_CFG_APP_MUTEX,
  MALLOC_ID_CFG_PLUGIN,

  MALLOC_ID_CFG_SIMPLE_DESC,

} MallocId;


/******************************************************************************
 *                      *****************************
 *                 *****           MACROS            *****
 *                      *****************************
 ******************************************************************************/

/* These macros should be used for accessing osMemAllocate() and osMemFree() */
#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
#define OS_MEM_MALLOC( _sts, _srv_id, _len, _mal_id ) osMemAllocate( _sts, _srv_id, _len, _mal_id )
#else
#define OS_MEM_MALLOC( _sts, _srv_id, _len, _mal_id ) osMemAllocate( _sts, _len )
#endif

#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
#define OS_MEM_FREE( _sts, _srv_id, _ptr ) osMemFree( _sts, _srv_id, _ptr )
#else
#define OS_MEM_FREE( _sts, _srv_id, _ptr ) osMemFree( _sts, _ptr )
#endif

/******************************************************************************
 *                      ******************************
 *                 *****  EXTERN FUNCTION PROTOTYPES  *****
 *                 ***** TO BE IMPLEMENTED BY THE APP *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Allocate Memory.
 *
 * This function shall:
 *  - Attempt to allocate memory of size Length
 *  - If unsuccessful:
 *    - Set Status->Error to OS_ERROR_MEM_NONE.
 *    - Return NULL.
 *  - If successful:
 *    - Set all bytes in the allocated memory to 0x00.
 *    - Return a pointer to the memory.
 *
 * If OS_MEM_UTILITY_USE_MALLOC_ID is enabled:
 *  - ServiceId = Service ID of ZAB instance
 *  - Id = Identifier of what malloc was for.
 *         Used for tracking leaks during development.
 ******************************************************************************/
#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
void* osMemAllocate( erStatus* Status, unsigned8 ServiceId, size_t Length, MallocId Id);
#else
void* osMemAllocate( erStatus* Status, size_t Length );
#endif


/******************************************************************************
 * Free Memory.
 *
 * This function shall:
 *  - If Buffer is NULL:
 *    - Set Status->Error to OS_ERROR_MEM_INVALID.
 *  - If not NULL:
 *    - Free the memory allocated at Buffer
 ******************************************************************************/
#ifdef OS_MEM_UTILITY_USE_MALLOC_ID
void osMemFree ( erStatus* Status, unsigned8 ServiceId, void* Buffer );
#else
void osMemFree ( erStatus* Status, void* Buffer );
#endif


/******************************************************************************
 * Zero Memory.
 *
 * This function shall:
 *  - If Buffer is NULL:
 *    - Set Status->Error to OS_ERROR_MEM_INVALID.
 *  - If not NULL:
 *    - Set Length bytes to 0x00 starting at Buffer.
 ******************************************************************************/
void osMemZero(erStatus* Status, unsigned8* Buffer, size_t Length);


/******************************************************************************
 * Set Memory.
 *
 * This function shall:
 *  - If Buffer is NULL:
 *    - Set Status->Error to OS_ERROR_MEM_INVALID.
 *  - If not NULL:
 *    - Set Length bytes to Byte starting at Buffer.
 ******************************************************************************/
void osMemSet(erStatus* Status, unsigned8* Buffer, size_t Length, unsigned8 Byte);

/******************************************************************************
 * Copy Memory.
 *
 * This function shall:
 *  - If Dest or Source are NULL:
 *    - Set Status->Error to OS_ERROR_MEM_INVALID.
 *  - If both are not NULL:
 *    - Copy Length bytes from Source to Destination
 ******************************************************************************/
void osMemCopy(erStatus* Status, unsigned8* Dest,   unsigned8* Source, size_t Length);

/******************************************************************************
 * Compare Memory.
 *
 * This function shall:
 *  - If Ptr1 is NULL, set Status->error to OS_ERROR_MEM_INVALID and return -1
 *  - If Ptr2 is NULL, set Status->error to OS_ERROR_MEM_INVALID and return 1
 *  - Else, a return value greater than zero indicates that the first byte that does not
 *    match in both memory blocks has a greater value in ptr1 than in ptr2 as if
 *    evaluated as unsigned char values; And a return value less than zero indicates the opposite.
 ******************************************************************************/
signed8 osMemCmp(erStatus* Status, unsigned8* Ptr1, unsigned8* Ptr2, size_t Length);


#ifdef __cplusplus
}
#endif
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif

