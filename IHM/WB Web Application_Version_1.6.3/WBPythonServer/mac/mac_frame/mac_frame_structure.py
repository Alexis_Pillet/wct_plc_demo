
"""
mac.mac_frame.mac_frame_structure
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to store the structure of mac frames

"""
from enum import IntEnum


class Mac:
    """Contains all possible values of all fields of mac frames"""

    @staticmethod
    def display_ack():
        """Return a string to display an user friendly log"""
        return {Mac.AckStatus.SUCCESS: 'SUCCESS',
                Mac.AckStatus.FAILED: 'FAILED',
                Mac.AckStatus.BAD_CRC: 'BAD_CRC',
                Mac.AckStatus.BAD_FRAME_COUNTER: 'BAD_FRAME_COUNTER',
                Mac.AckStatus.INVALID_COMMAND: 'INVALID_COMMAND',
                Mac.AckStatus.UNKNOWN_ERROR: 'UNKNOWN_ERROR',
                Mac.AckStatus.RETRY_EXPIRED: 'RETRY_EXPIRED',
                Mac.AckStatus.ERROR_NO_ACK: 'ERROR_NO_ACK'}

    class FrameControl(IntEnum):
        """Frame Control field values"""
        CONTROL = 0x00
        DATA = 0x01
        ACK = 0xFF

    class Type(IntEnum):
        """Type field values"""
        STATE = 0x00
        DEVICE_TYPE = 0x01
        ID_NETWORK = 0x02
        CONFIG = 0x03

    class AckStatus(IntEnum):
        """Ack Status field values"""
        SUCCESS = 0x00
        FAILED = 0x01
        BAD_CRC = 0x02
        BAD_FRAME_COUNTER = 0x03
        INVALID_COMMAND = 0x04
        UNKNOWN_ERROR = 0x05
        RETRY_EXPIRED = 0x06
        ERROR_NO_ACK = 0x07

    class Network(IntEnum):
        """Network field values"""
        NETWORK = 0x00
        DISCOVERY = 0x01
        ADVERTISING = 0x02

    class DeviceType(IntEnum):
        """Device Type field values"""
        PEER = 0x00
        CONCENTRATOR = 0x01

    class NetworkState(IntEnum):
        """Network State field values"""
        UNINITIALIZED = 0x00
        NONE = 0x01
        NETWORKING = 0x02
        NETWORKED = 0x03
        NETWORK_LOSS = 0x04

    class DiscoveryState(IntEnum):
        """Discovery State field values"""
        NO_DISCOVERY = 0x00
        DISCOVERING = 0x01
        DISCOVERY_COMPLETE = 0x02

    class AdvertisingState(IntEnum):
        """Advertising State field values"""
        ADVERTISING = 0x00
        ADVERTISING_COMPLETE = 0x01
