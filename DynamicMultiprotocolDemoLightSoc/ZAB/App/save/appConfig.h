/******************************************************************************
 *                          ZAB TEST APPLICATION
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the application configuration settings for the ZAB
 *   Test Application.
 *   This is where we:
 *    - Define compile time settings (Header)
 *    - Create storage for values that may be asked by ZAB, or given to us from ZAB.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.03.00  16-Jan-14   MvdB   Use common header block
 * 00.00.06.01  17-Sep-14   MvdB   artf104182/54399: Support channel change API and notification
 * 00.00.06.05  09-Oct-14   MvdB   ARTF57973: Mutexes need service pointer
 * 00.00.08.01  27-Nov-14   MvdB   Turn on RFT for ConsoleTest as it is used by some test people
 * 01.100.06.00 10-Feb-15   MvdB   ARTF113724: Support network processor antenna control, required for SLIPZ
 * 002.000.003  06-Mar-15   MvdB   ARTF116061: Support run time configuration of Green Power endpoint.
 * 002.000.004  01-Apr-15   MvdB   ARTF56504: Support missed channel change recovery, with actions to get/set parameters.
 *                                 ConsoleTest - add boolean to enable/disable acceptance of GPDs
 * 002.002.002  01-Sep-15   MvdB   ARTF147441: Add appConfig_Config.greenPowerKeyMode
 * 002.002.004  04-Sep-15   MvdB   ARTF147424: Add appConfig_GetDeviceId()
 * 002.002.028  12-Dec-16   MvdB   ConsoleTest: Add runtime configurable options for report and RSSI printing
 * 002.002.029  06-Jan-17   MvdB   ConsoleTest: Support logging reports to reports.csv
 * 002.002.044  23-Jan-17   MvdB   Add greenPowerFilterByModelId
 * 002.002.051  16-Mar-17   MvdB   ConsoleTest: Add selective GP commissioning by source ID
 * 002.002.052  12-Apr-17   SMon   ConsoleTest: Add a decommissioning all command
 *****************************************************************************/

#ifndef _APP_CONFIG_H_
#define _APP_CONFIG_H_

#include "osTypes.h"
#include "zabCoreService.h"

/******************************************************************************
 *                      *****************************
 *                 *****          OPTIONS            *****
 *                      *****************************
 ******************************************************************************/


/* Select which vendor we will be using.
 * These are actually set in the project build options. */
//#define APP_CONFIG_M_VENDOR_TI_ZNP
//#define APP_CONFIG_M_VENDOR_SILABS_NCP

/* Enable Green Power
 * Make sure you also set ZAB_CFG_ENABLE_GREEN_POWER and SZL_CFG_ENABLE_GREEN_POWER.
 * These are actually set in the project build options!!! (Enabled for TI and Disabled for SiLabs */
//#define APP_CONFIG_ENABLE_GREEN_POWER
#define APP_CONFIG_M_DEFAULT_GREEN_POWER_ENDPOINT   ( 0x01 )

/* Define this to use the Radio Factory Test functions in ZNP
 * You must also define ZAB_VND_CFG_ZNP_ENABLE_RFT in zabVendorConfigureZnp.h */
#ifdef APP_CONFIG_M_VENDOR_TI_ZNP
  #define APP_CONFIG_USE_ZNP_RFT
#endif
#ifdef APP_CONFIG_M_VENDOR_SILABS_NCP
  #define APP_CONFIG_USE_SILABS_RFT
#endif

/* Define this to use the Wireless TEst Bench
 * You must also define ZAB_CFG_ENABLE_WIRELESS_TEST_BENCH in zabVendorConfigureZnp.h */
//#define APP_CONFIG_USE_WIRELESS_TEST_BENCH


/* Enable cloning.
 * Currently supported for TI only */
#ifdef APP_CONFIG_M_VENDOR_TI_ZNP
  #define APP_CONFIG_ENABLE_CLONING
#endif

/* MAximum number of ZAB Instances */
#define APP_CONFIG_M_MAX_INSTANCES                  1

/* Service ID of our ZAB Instance */
#define APP_CONFIG_M_SERVICE_ID                     1

// Value for quickly saying "all channels"
#define APP_CONFIG_M_CHANNEL_NUM_ALL 0

/* Default configuration values */
#define APP_CONFIG_M_CHANNEL_NUM_DEFAULT APP_CONFIG_M_CHANNEL_NUM_ALL
#define APP_CONFIG_M_PAN_ID_UNDEFINED 0xFFFF
#define APP_CONFIG_M_EPID_UNDEFINED 0xFFFFFFFFFFFFFFFFULL
#define APP_CONFIG_M_DEFAULT_TX_POWER 10 // 127
#define APP_CONFIG_M_DEFAULT_PJ_TIME 180
#define APP_CONFIG_M_DEFAULT_CHANNEL_TO_CHANGE_TO 11


#define APP_CONFIG_M_MAX_BEACONS 32

/* Size of string used for SZL address printing */
#define APP_CONFIG_MAX_SZL_ADDRESS_STRING_LENGTH 80

/* Max space allocated for a firmware filename */
#define APP_CONFIG_MAX_FIRMWARE_FILENAME_LENGTH 100

/******************************************************************************
 *                      ******************************
 *                 *****            TYPES             *****
 *                      ******************************
 ******************************************************************************/

/* Storage for info given to app */
typedef struct
{
  unsigned8 channel;
  unsigned16 nwkAddress;
  unsigned16 panID;
  unsigned64 epid;
  unsigned64 ieee;
  unsigned8 coreVersion[4];
  unsigned8 vendorVersion[4];
  zabVendorType vendorType;
  zabNetworkProcessorModel hardwareType;
  signed8 actualTxPower;
  zabAntennaSupportedModes antennaSupportedModes;
  zabAntennaOperatingMode antennaOperatingMode;
}appInfo_t;

/* Storage for values that may be asked of the app */
typedef struct
{
  char * port; // Currently only 3 characters is allow to define port (ACM0; S0) WARNING: Not include tty or com!!!
  unsigned8 startNetworkProcessorApplication;

  unsigned8 nwkResume;
  zabNwkSteerOptions nwkSteerOptions;
  unsigned8 channelNumber;
  unsigned16 panId;
  unsigned64 extPanId;
  signed8 txPower;
  unsigned16 permitJoinTime;
  unsigned8 channelToChangeTo;
  zabAntennaOperatingMode newAntennaOperatingMode;
  zab_bool autoDiscoverEndpoints;

  /* Runtime control over level of Report and RSSI printing */
  zab_bool printRssiInfo;
  zab_bool printReportInfo;
  zab_bool printFullReportInfo;
  zab_bool generateReportCsvFile;

  /* GreenPower Endpoint
   * This could be a defined value for most applications, but is dynamic in
   * ConsoleTest to allow range testing. */
  unsigned8 greenPowerEndpoint;

  /* GreenPower Endpoint: Accept/Reject commissioning of GPDs */
  zab_bool greenPowerAcceptGpdCommissioning;

  /* Green Power: Key Mode to use when accepting GPDs. See SZL_GP_COMMISSIONING_KEY_MODE_t */
  unsigned8 greenPowerKeyMode;

  /* GreenPower Commissioning: Only accept defined model or source Id*/
  unsigned16 greenPowerFilterByModelId;
  unsigned32 greenPowerFilterBySourceId;

  /* GreePower DeCommissioning all*/
  zab_bool greenPowerDecommissioning;
  
  /* Network Maintenance Values to Set*/
  unsigned32 nwkMaintSlowPingTimeMs;
  unsigned32 nwkMaintFastPingTimeMs;

  /* Poll Control */
  zab_bool pollControlDataPending;
  unsigned16 pollControlFastPollTime;

  /* Nwk Configure Back stuff - find a nice home for it*/
  unsigned8 beaconSelected;
  unsigned8 beaconCount;
  BeaconFormat_t beaconStorage[APP_CONFIG_M_MAX_BEACONS];
}appConfig_t;

/******************************************************************************
 *                      ******************************
 *                 *****       EXTERN VARIABLES       *****
 *                      ******************************
 ******************************************************************************/

/* Storage for info given to app */
extern appInfo_t appConfig_Info;

/* Storage for values that may be asked of the app */
extern appConfig_t appConfig_Config;

/* Service pointer for our ZAB instance */
extern zabService* appService; 
/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * Initialise the run time configuration database
 ******************************************************************************/
extern void appConfig_Init(void);

/******************************************************************************
 * Get Device ID for Endpoint
 ******************************************************************************/
extern unsigned16 appConfig_GetDeviceId(unsigned8 Endpoint);

/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/
#endif
