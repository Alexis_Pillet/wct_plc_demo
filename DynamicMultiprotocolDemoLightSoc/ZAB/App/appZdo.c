/******************************************************************************
 *                          ZAB TEST APPLICATION
 *
 *     Copyright (c) 2011-20xx by Schneider Electric, all rights reserved
 *
 * DESCRIPTION:
 *   This file contains the ZDO handlers for the test app.
 *
 * AUTHOR:
 *   ZigBee Excellence Center
 *
 * MODIFICATION HISTORY:
 * Vendor Rev     Date     Author  Change Description
 * 00.00.03.00  16-Jan-14   MvdB   Use common header block
 * 00.00.03.01  19-Mar-14   MvdB   artf53864: Support mgmt leave request/response
 * 00.00.06.00  27-Jun-14   MvdB   Add User Descriptor support
 * 00.00.06.03  01-Oct-14   MvdB   artf104879: Support energy scans via Mgmt Nwk Update Request
 * 00.00.06.06  21-Oct-14   MvdB   artf106135: Handle network leave indications
 * 002.000.001  03-Feb-15   MvdB   ARTF115770: Support ZDO Node Descriptor Request
 * 002.001.002  15-Jul-15   MvdB   ARTF136585: Add transaction IDs to over the air ZDO commands/responses
 * 002.002.015  21-Oct-15   MvdB   ARTF104106: Support SZL_ZDO_MatchDescriptorReq()
 * 002.002.047  31-Jan-17   MvdB   appZdo_Discovery_EndpointDiscovered_Handler(): Auto bind and configure checkin for poll control
 * 002.002.049  14-Feb-17   MvdB   Use appZcl_GetClusterString()
 *****************************************************************************/

//#include "appSerialUtility.h"
#include "appMain.h"

#include "zabCoreService.h"

#include "appConfig.h"
#include "appZcl.h"

#include "discovery.h"
#include "device_manager.h"

#include "appZdo.h"
/*#include <assert.h>
#include <string.h>*/

/******************************************************************************
 *                      ******************************
 *                 ***** EXTERN FUNCTION DECLARATIONS *****
 *                      ******************************
 ******************************************************************************/

/******************************************************************************
 * SZL Discovery Plugin - Node Detected Handler
 ******************************************************************************/
void appZdo_Discovery_NodeDetected_Handler(zabService* Service, SzlPlugin_Discovery_NodeInfo_t* NodeInfo)
{
  SZL_RESULT_t res;

  printApp(Service, "App Zdo: Node Detected: NwkAddr = 0x%04X, IeeeAddr = 0x%016llX, NodeFunction = ",
                    NodeInfo->NetworkAddress,
                    NodeInfo->IeeeAddress);
  switch(NodeInfo->NodeFunction)
    {
      case SZLPLUGIN_DISCOVERY_NODEFUNCTION_FULL: printApp(Service, "Full"); break;
      case SZLPLUGIN_DISCOVERY_NODEFUNCTION_REDUCED: printApp(Service, "Reduced"); break;
      default: printApp(Service, "Unknown"); break;
    }
  printApp(Service, ", NodeStatus = ");
  switch(NodeInfo->NodeStatus)
    {
      case SZLPLUGIN_DISCOVERY_NODESTATUS_COMMS_OK: printApp(Service, "Comms OK\n"); break;
      case SZLPLUGIN_DISCOVERY_NODESTATUS_COMMS_FAILED: printApp(Service, "Comms Failed\n"); break;
      default: printApp(Service, "Unknown\n"); break;
    }


  if ( (appConfig_Config.autoDiscoverEndpoints == zab_true) &&
       (NodeInfo->IeeeAddress != ZAB_TYPES_M_IEEE_UNKNOWN) ) // Do not discover GPDs (IEEE = FF's)
    {
      printApp(Service, "Auto discover endpoints for Node 0x%04X\n", NodeInfo->NetworkAddress);
      res = SzlPlugin_Discovery_DiscoverEndpoints(Service,
                                                  appZdo_Discovery_EndpointConfirm_Handler,
                                                  appZdo_Discovery_EndpointDiscovered_Handler,
                                                  NodeInfo->NetworkAddress,
                                                  2);
      printApp(Service, "Auto discover endpoints: Result = 0x%02X %s\n", res,
                                              (res == SZL_RESULT_SUCCESS)?"=SUCCESS":"" );
    }

}

/******************************************************************************
 * SZL Discovery Plugin - Confirm Handler
 ******************************************************************************/
void appZdo_Discovery_NodeConfirm_Handler(zabService* Service, SZL_STATUS_t Status, szl_uint8 NumberOfNodes, SzlPlugin_Discovery_NodeInfo_t* NodeInfo)
{
  unsigned8 i;

  if (Status == SZL_STATUS_SUCCESS)
    {
      printApp(Service, "App Zdo: Discovery Confirm. SZL Sts = 0x%02X, NumNodes = 0x%02X:\n",
                Status,
                NumberOfNodes);
      for (i = 0; i < NumberOfNodes; i++)
        {
          printApp(Service, "\tNode 0x%02X: NwkAddr = 0x%04X, IeeeAddr = 0x%016llX, NodeFunction = ",
                    i,
                    NodeInfo[i].NetworkAddress,
                    NodeInfo[i].IeeeAddress);
          switch(NodeInfo[i].NodeFunction)
            {
              case SZLPLUGIN_DISCOVERY_NODEFUNCTION_FULL: printApp(Service, "Full"); break;
              case SZLPLUGIN_DISCOVERY_NODEFUNCTION_REDUCED: printApp(Service, "Reduced"); break;
              default: printApp(Service, "Unknown"); break;
            }
          printApp(Service, ", NodeStatus = ");
          switch(NodeInfo[i].NodeStatus)
            {
              case SZLPLUGIN_DISCOVERY_NODESTATUS_COMMS_OK: printApp(Service, "Comms OK\n"); break;
              case SZLPLUGIN_DISCOVERY_NODESTATUS_COMMS_FAILED: printApp(Service, "Comms Failed\n"); break;
              default: printApp(Service, "Unknown\n"); break;
            }
        }
    }
  else
    {
      printApp(Service, "App Zdo: Discovery Confirm Failed = 0x%02X\n", Status);
    }
}

void appZdo_Discovery_EndpointDiscovered_Handler(zabService* Service, SzlPlugin_Discovery_EndpointInfo_t* EndpointInfo)
{
  unsigned8 i;
  szl_bool pollControlServer = szl_false;

  printApp(Service, "App Zdo: Endpoint Discovered: NwkAddr = 0x%04X, EP = 0x%02X, prof = 0x%04X, DevId = 0x%04X\n",
            EndpointInfo->SimpleDesc.NetworkAddressOfInterest,
            EndpointInfo->SimpleDesc.Endpoint,
            EndpointInfo->SimpleDesc.ProfileId,
            EndpointInfo->SimpleDesc.DeviceId);

  printApp(Service, "                              InClusters = ");
  for (i = 0; i < EndpointInfo->SimpleDesc.NumInClusters; i++)
    {
      printApp(Service, "0x%04X (%s), ", EndpointInfo->SimpleDesc.InClusterList[i], appZcl_GetClusterString(EndpointInfo->SimpleDesc.InClusterList[i]));

      if (EndpointInfo->SimpleDesc.InClusterList[i] == 0x0020)
        {
          pollControlServer = szl_true;
        }
    }
  printApp(Service, "\n");

  printApp(Service, "                              OutClusters = ");
  for (i = 0; i < EndpointInfo->SimpleDesc.NumOutClusters; i++)
    {
      printApp(Service, "0x%04X (%s), ", EndpointInfo->SimpleDesc.OutClusterList[i], appZcl_GetClusterString(EndpointInfo->SimpleDesc.OutClusterList[i]));
    }
  printApp(Service, "\n");

  printApp(Service, "                              ModelId = %s\n", EndpointInfo->ModelId);
  printApp(Service, "                              ProductId = 0x%04X\n", EndpointInfo->ProductIdentifier);


  /* If the device is an end device with poll control server, bind it it to us and setup checkin for 1 minute.
   * WARNING: This is a but of a hack but it works ok for a demo app.
   *          It assumes endpoint 1 is valid in ZAB and poll control client is running there.
   *          Errors are not well handled.  */
  if (pollControlServer == szl_true)
    {
      SZL_ZdoBindReqParams_t bindReq = { 0 };
      szl_uint64 ieeeAddress = SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN;
      SZL_RESULT_t result;

      result = SzlPlugin_DevMan_GetByNetworkAddress(Service,
                                                    EndpointInfo->SimpleDesc.NetworkAddressOfInterest,
                                                    &ieeeAddress, NULL);
      if ( (result == SZL_RESULT_SUCCESS) &&
           (ieeeAddress != SZL_PLUGIN_DEV_MAN_IEEE_ADDRESS_UNKNOWN) )
        {
          printApp(appService, "appZdo_Discovery_EndpointDiscovered_Handler: Auto binding+configuring poll control to EP1 every 60 seconds\n");

          /* Bind the devices poll control cluster to us */
          bindReq.DestinationAddressMode = SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_UNICAST;
          bindReq.NetworkAddress = EndpointInfo->SimpleDesc.NetworkAddressOfInterest;
          bindReq.SourceIeeeAddress = ieeeAddress;
          bindReq.SourceEndpoint = EndpointInfo->SimpleDesc.Endpoint;
          bindReq.ClusterID = 0x0020;
          bindReq.DestinationIeeeAddress = appConfig_Info.ieee;
          bindReq.DestinationEndpoint = 1; // This is a little lazy, but works for a demo app...
          result = SZL_ZDO_BindReq(appService,
                                   appZdo_SzlBindRspHandler,
                                   &bindReq,
                                   NULL);
          appZcl_PrintSzlResult(appService, "SZL_ZDO_BindReq(Poll Control Auto Bind)", result, 0);


          /* Now write the CheckinTime attribute to 1 minute */
          SZL_AttributeWriteReqParams_t* writeReq;
          szl_uint32 CheckinTimeQuarterSecs = 240;
          writeReq = (SZL_AttributeWriteReqParams_t *)APP_MEM_GLUE_MALLOC(SZL_AttributeWriteReqParams_t_SIZE(1),
                                                                          appMain_GetServiceId(appService),
                                                                          MALLOC_ID_APP_GENERAL);
          if (writeReq == NULL)
            {
              printApp(appService, "Mem Error: SZL_AttributeWriteReqParams_t poll control\n");
              return;
            }

          writeReq->SourceEndpoint = 1;
          writeReq->DestAddrMode.AddressMode = SZL_ADDRESS_MODE_NWK_ADDRESS;
          writeReq->DestAddrMode.Addresses.Nwk.Address = EndpointInfo->SimpleDesc.NetworkAddressOfInterest;
          writeReq->DestAddrMode.Addresses.Nwk.Endpoint = EndpointInfo->SimpleDesc.Endpoint;
          writeReq->ManufacturerSpecific = szl_false;
          writeReq->ClusterID = 0x0020;
          writeReq->NumberOfAttributes = 1;
          writeReq->Attributes[0].AttributeID = 0x0000;
          writeReq->Attributes[0].DataType = SZL_ZB_DATATYPE_UINT32;
          writeReq->Attributes[0].DataLength = 4;
          writeReq->Attributes[0].Data = &CheckinTimeQuarterSecs;

          result = SZL_AttributeWriteReq(appService, appZcl_WriteAttrRspHandler, writeReq, NULL);
          APP_MEM_GLUE_FREE(writeReq, appMain_GetServiceId(appService));
          appZcl_PrintSzlResult(appService, "SZL_AttributeWriteReq", result, 0);
        }
    }
}


void appZdo_Discovery_EndpointConfirm_Handler(zabService* Service, SZL_STATUS_t Status, szl_uint16 NetworkAddress)
{
  printApp(Service, "App Zdo: Endpoint Discovery Confirm. NwkAddr = 0x%04X, SZL Sts = 0x%02X %s\n",
           NetworkAddress,
           Status,
           (Status == SZL_STATUS_SUCCESS)? "Success!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" : "Failed #######################################");
}








/******************************************************************************
 * SZL ZDO Ieee & Nwk Address Response Handler
 ******************************************************************************/
void appZdo_SzlAddrRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoAddrRespParams_t* Params, szl_uint8 TransactionId)
{
  unsigned8 i;

  printApp(Service, "App Zdo: SZL ZDO ADDR Rsp. SZL Sts = 0x%02X, TID = 0x%02X, ",
           Status,
           TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "Sts = 0x%02X, IEEE = 0x%016llX, Nwk 0x%04X, NumAsscDev = 0x%02X, StrtInd = 0x%02X, AsscDevs = ",
                Params->Status,
                Params->IeeeAddr,
                Params->NetworkAddress,
                Params->NumAssocDev,
                Params->StartIndex);
      for (i = 0; i < Params->NumAssocDev; i++)
        {
          printApp(Service, " 0x%04X", Params->AssocDevNetworkAddresses[i]);
        }
      printApp(Service, "\n");
    }
  else
    {
      printApp(Service, "WARNING: Params = NULL\n");
    }
}


/******************************************************************************
 * SZL ZDO Node Descriptor Response Handler
 ******************************************************************************/
void appZdo_SzlNodeDescriptorRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoNodeDescriptorRespParams_t* Params, szl_uint8 TransactionId)
{
  printApp(Service, "App Zdo: SZL ZDO Node Descriptor Rsp. SZL Sts = 0x%02X, TID = 0x%02X, ",
           Status,
           TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "Sts = 0x%02X, NwkAddr = 0x%04X\n",
               Params->Status,
               Params->NetworkAddressOfInterest);
      if (Params->Status == SZL_ZDO_STATUS_SUCCESS)
        {
          printApp(Service, "\tManCode = 0x%04X, MaxBufSize = 0x%02X, MaxInTransSize = 0x%04X, MaxOutTransSize = 0x%04X\n",
                   Params->ManufacturerCode,
                   Params->MaximumBufferSize,
                   Params->MaximumIncomingTransferSize,
                   Params->MaximumOutgoingTransferSize);
          printApp(Service, "\tOptions:\n");
          printApp(Service, "\t\tLogicalType = 0x%02X\n", Params->Option.optBits.LogicalType);
          printApp(Service, "\t\tComplexDescriptorAvailable = %s\n", Params->Option.optBits.ComplexDescriptorAvailable ? "true" : "false");
          printApp(Service, "\t\tUserDescriptorAvailable = %s\n", Params->Option.optBits.UserDescriptorAvailable ? "true" : "false");
          printApp(Service, "\t\tAPSFlag = 0x%02X\n", Params->Option.optBits.APSFlag);
          printApp(Service, "\t\tFrequencyBand = 0x%02X\n", Params->Option.optBits.FrequencyBand);

          printApp(Service, "\tMacCap:\n");
          printApp(Service, "\t\tAlternatePANCoordinator = %s\n", Params->MACCapabilityFlag.optBits.AlternatePANCoordinator ? "true" : "false");
          printApp(Service, "\t\tDeviceType = %s\n", Params->MACCapabilityFlag.optBits.DeviceType ? "Full" : "Reduced");
          printApp(Service, "\t\tPowerSource = %s\n", Params->MACCapabilityFlag.optBits.PowerSource ? "true" : "false");
          printApp(Service, "\t\tReceiverOnWhenIdle = %s\n", Params->MACCapabilityFlag.optBits.ReceiverOnWhenIdle ? "true" : "false");
          printApp(Service, "\t\tSecurityCapability = %s\n", Params->MACCapabilityFlag.optBits.SecurityCapability ? "true" : "false");
          printApp(Service, "\t\tAllocateAddress = %s\n", Params->MACCapabilityFlag.optBits.AllocateAddress ? "true" : "false");

          printApp(Service, "\tSvrMask:\n");
          printApp(Service, "\t\tPrimaryTrustCenter = %s\n", Params->ServerMask.optBits.PrimaryTrustCenter ? "true" : "false");
          printApp(Service, "\t\tBackupTrustCenter = %s\n", Params->ServerMask.optBits.BackupTrustCenter ? "true" : "false");
          printApp(Service, "\t\tPrimaryBindingTableCache = %s\n", Params->ServerMask.optBits.PrimaryBindingTableCache ? "true" : "false");
          printApp(Service, "\t\tBackupBindingTableCache = %s\n", Params->ServerMask.optBits.BackupBindingTableCache ? "true" : "false");
          printApp(Service, "\t\tPrimaryDiscoveryCache = %s\n", Params->ServerMask.optBits.PrimaryDiscoveryCache ? "true" : "false");
          printApp(Service, "\t\tBackupDiscoveryCache = %s\n", Params->ServerMask.optBits.BackupDiscoveryCache ? "true" : "false");
          printApp(Service, "\t\tNetworkManager = %s\n", Params->ServerMask.optBits.NetworkManager ? "true" : "false");

          printApp(Service, "\tDescriptorCapability:\n");
          printApp(Service, "\t\tExtendedActiveEndpointListAvailable = %s\n", Params->DescriptorCapability.optBits.ExtendedActiveEndpointListAvailable ? "true" : "false");
          printApp(Service, "\t\tExtendedSimpleDescriptorListAvailable = %s\n", Params->DescriptorCapability.optBits.ExtendedSimpleDescriptorListAvailable ? "true" : "false");
        }
    }
  else
    {
      printApp(Service, "WARNING: Params = NULL\n");
    }
}


/******************************************************************************
 * SZL ZDO Power Descriptor Response Handler
 ******************************************************************************/
void appZdo_SzlPowerDescriptorRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoPowerDescriptorRespParams_t* Params, szl_uint8 TransactionId)
{
  printApp(Service, "App Zdo: SZL ZDO Power Descriptor Rsp. SZL Sts = 0x%02X, TID = 0x%02X, ",
           Status,
           TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "Sts = 0x%02X, NwkAddr = 0x%04X, AvailPwrSrc = 0x%02X, CurPwrMode = 0x%02X, CurPwrSrc = 0x%02X, CurPwrSrcLvl = 0x%02X\n",
               Params->Status,
               Params->NetworkAddressOfInterest,
               Params->AvailablePowerSources,
               Params->CurrentPowerMode,
               Params->CurrentPowerSource,
               Params->CurrentPowerSourceLevel);
    }
  else
    {
      printApp(Service, "WARNING: Params = NULL\n");
    }
}


/******************************************************************************
 * SZL ZDO Active Endpoint Response Handler
 ******************************************************************************/
void appZdo_SzlActiveEndpointRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoActiveEndpointRespParams_t* Params, szl_uint8 TransactionId)
{
  unsigned8 i;

  printApp(Service, "App Zdo: SZL ZDO Active EP Rsp. SZL Sts = 0x%02X, TID = 0x%02X, ",
           Status,
           TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "Sts = 0x%02X, NwkAddr = 0x%04X, NumEPs = 0x%02X, EPs =",
                  Params->Status,
                  Params->NetworkAddressOfInterest,
                  Params->ActiveEndpointCount);
        for (i = 0; i < Params->ActiveEndpointCount; i++)
          {
            printApp(Service, " 0x%02X", Params->ActiveEndpointList[i]);
          }
        printApp(Service, "\n");
    }
  else
    {
      printApp(Service, "WARNING: Params = NULL\n");
    }
}


/******************************************************************************
 * SZL ZDO Simple Descriptor Response Handler
 ******************************************************************************/
void appZdo_SzlSimpleDescriptorRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoSimpleDescriptorRespParams_t* Params, szl_uint8 TransactionId)
{
  unsigned8 i;

  printApp(Service, "App Zdo: SZL Simple Desc Rsp. SZL Sts = 0x%02X, TID = 0x%02X, ",
           Status,
           TransactionId);

  if (Params != NULL)
    {

        printApp(Service, "Sts = 0x%02X, NwkAddr = 0x%04X, EP = 0x%02X, Prof = 0x%04X, DevID = 0x%04X, DevVer = 0x%02X\n",
                  Params->Status,
                  Params->NetworkAddressOfInterest,
                  Params->Endpoint,
                  Params->ProfileId,
                  Params->DeviceId,
                  Params->DeviceVersion);
        printApp(Service, "                              NumInCl = 0x%02X, InClusters = ",
                  Params->NumInClusters);
        for (i = 0; i < Params->NumInClusters; i++)
          {
            printApp(Service, " 0x%04X (%s),", Params->InClusterList[i], appZcl_GetClusterString(Params->InClusterList[i]));
          }
        printApp(Service, "\n");
        printApp(Service, "                              NumOutCl = 0x%02X, outClusters = ",
                  Params->NumOutClusters);
        for (i = 0; i < Params->NumOutClusters; i++)
          {
            printApp(Service, " 0x%04X (%s),", Params->OutClusterList[i], appZcl_GetClusterString(Params->OutClusterList[i]));
          }
        printApp(Service, "\n ");
    }
  else
    {
      printApp(Service, "WARNING: Params = NULL\n");
    }
}


/******************************************************************************
 * SZL ZDO Match Descriptor Response Handler
 ******************************************************************************/
void appZdo_SzlMatchDescriptorRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoMatchDescriptorRespParams_t* Params, szl_uint8 TransactionId)
{
  unsigned8 i;

  printApp(Service, "App Zdo: SZL Match Desc Rsp. SZL Sts = 0x%02X, TID = 0x%02X, ",
           Status,
           TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "Sts = 0x%02X, NwkAddr = 0x%04X, NumEps = 0x%02X, Eps =",
                Params->Status,
                Params->NetworkAddressOfInterest,
                Params->NumberOfEndpoints);

      for (i = 0; i < Params->NumberOfEndpoints; i++)
        {
          printApp(Service, " 0x%02X", Params->Endpoints[i]);
        }
      printApp(Service, "\n");
    }
  else
    {
      printApp(Service, "WARNING: Params = NULL\n");
    }
}


/******************************************************************************
 * SZL ZDO Bind Response Handler
 ******************************************************************************/
void appZdo_SzlBindRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoBindRespParams_t* Params, szl_uint8 TransactionId)
{
  printApp(Service, "App Zdo: SZL ZDO Bind Rsp. SZL Sts = 0x%02X, TID = 0x%02X, ",
           Status,
           TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "NwkAddr = 0x%04X, Sts = 0x%02X\n",
                  Params->NetworkAddress,
                  Params->Status);
    }
  else
    {
      printApp(Service, "WARNING: Params = NULL\n");
    }
}

/******************************************************************************
 * SZL ZDO UnBind Response Handler
 ******************************************************************************/
void appZdo_SzlUnBindRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoBindRespParams_t* Params, szl_uint8 TransactionId)
{
  printApp(Service, "App Zdo: SZL ZDO UnBind Rsp. SZL Sts = 0x%02X, TID = 0x%02X, ",
           Status,
           TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "NwkAddr = 0x%04X, Sts = 0x%02X\n",
                Params->NetworkAddress,
                Params->Status);
    }
  else
    {
      printApp(Service, "WARNING: Params = NULL\n");
    }
}


/******************************************************************************
 * SZL ZDO User Descriptor Response Handler
 ******************************************************************************/
void appZdo_SzlUserDescriptorRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoUserDescriptorRespParams_t* Params, szl_uint8 TransactionId)
{
  printApp(Service, "App Zdo: SZL ZDO UserDesc Rsp. SZL Sts = 0x%02X, TID = 0x%02X, ",
           Status,
           TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "NwkAddr = 0x%04X, Sts = 0x%02X, UserDesc = %s\n",
                Params->NetworkAddressOfInterest,
                Params->Status,
                Params->UserDescriptor);
    }
  else
    {
      printApp(Service, " WARNING: Params = NULL\n");
    }
}
/******************************************************************************
 * SZL ZDO User Descriptor SetResponse Handler
 ******************************************************************************/
void appZdo_SzlUserDescriptorSetRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoUserDescriptorSetRespParams_t* Params, szl_uint8 TransactionId)
{
  printApp(Service, "App Zdo: SZL ZDO UserDesc Set Rsp. SZL Sts = 0x%02X, TID = 0x%02X, ",
           Status,
           TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "NwkAddr = 0x%04X, Sts = 0x%02X\n",
                Params->NetworkAddressOfInterest,
                Params->Status);
    }
  else
    {
      printApp(Service, " WARNING: Params = NULL\n");
    }
}



char* getDeviceTypeString(ENUM_SZL_ZDO_MGMT_LQI_DEVICE_TYPE deviceType)
{
  switch(deviceType)
    {
    case SZL_ZDO_MGMT_LQI_DEVICE_COORDINATOR: return "Coord";
    case SZL_ZDO_MGMT_LQI_DEVICE_ROUTER: return "Router";
    case SZL_ZDO_MGMT_LQI_DEVICE_END_DEVICE: return "EndDev";
    }
  return "Unknown";
}
char* getRxOnString(ENUM_SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_TYPE rxType)
{
  switch(rxType)
    {
    case SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_FALSE: return "False";
    case SZL_ZDO_MGMT_LQI_RX_ON_WHEN_IDLE_TRUE: return "True";
    }
  return "Unknown";
}
char* getRelationshipString(ENUM_SZL_ZDO_MGMT_LQI_REALATIONSHIP_TYPE relType)
{
  switch(relType)
    {
    case SZL_ZDO_MGMT_LQI_RELATIONSHIP_NEIGHBOR_IS_PARENT: return "Parent";
    case SZL_ZDO_MGMT_LQI_RELATIONSHIP_NEIGHBOR_IS_CHILD: return "Child";
    case SZL_ZDO_MGMT_LQI_RELATIONSHIP_NEIGHBOR_IS_SIBLING: return "Sibling";
    case SZL_ZDO_MGMT_LQI_RELATIONSHIP_NEIGHBOR_IS_NONE: return "None";
    case SZL_ZDO_MGMT_LQI_RELATIONSHIP_NEIGHBOR_IS_PREV_CHILD: return "PrevChild";
    }
  return "Unknown";
}
char* getPermitJoinString(ENUM_SZL_ZDO_MGMT_LQI_PERMIT_JOIN_TYPE pjType)
{
  switch(pjType)
    {
    case SZL_ZDO_MGMT_LQI_PERMIT_JOIN_FALSE: return "False";
    case SZL_ZDO_MGMT_LQI_PERMIT_JOIN_TRUE: return "True";
    }
  return "Unknown";
}


/******************************************************************************
 * SZL ZDO MGMT LQI Response Handler
 ******************************************************************************/
void appZdo_SzlMgmtLqiRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoMgmtLqiRespParams_t* Params, szl_uint8 TransactionId)
{
  unsigned8 i;

  printApp(Service, "App Zdo: SZL ZDO Mgmt Lqi Rsp. SZL Sts = 0x%02X, TID = 0x%02X, ",
           Status,
           TransactionId);

  if (Params != NULL)
    {
        printApp(Service, "Sts = 0x%02X, NwkAddr = 0x%04X, Entries = 0x%02X, StartInd = 0x%02X, ListCnt = 0x%02X:\n",
                  Params->Status,
                  Params->NetworkAddress,
                  Params->NeighborTableEntries,
                  Params->StartIndex,
                  Params->NeighborTableListCount);
        for (i = 0; i < Params->NeighborTableListCount; i++)
          {
            printApp(Service, "\t\tItem 0x%02X: NwkAddr = 0x%04X, IEEE = 0x%016llX, EPID = 0x%016llX, DevType = %s, RxOn = %s, Rel = %s, PJ = %s, Depth = 0x%02X, LQI = 0x%02X\n",
                      Params->StartIndex + i,
                      Params->NeighborTableList[i].NetworkAddress,
                      Params->NeighborTableList[i].Ieee,
                      Params->NeighborTableList[i].Epid,
                      getDeviceTypeString(Params->NeighborTableList[i].DeviceType),
                      getRxOnString(Params->NeighborTableList[i].RxOnWhenIdle),
                      getRelationshipString(Params->NeighborTableList[i].Relationship),
                      getPermitJoinString(Params->NeighborTableList[i].PermitJoin),
                      Params->NeighborTableList[i].Depth,
                      Params->NeighborTableList[i].Lqi);
          }
    }
  else
    {
      printApp(Service, "WARNING: Params = NULL\n");
    }
}

/******************************************************************************
 * SZL ZDO Mgmt Network Update Response Handler
 ******************************************************************************/
void appZdo_SzlMgmtNwkUpdateRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoMgmtNwkUpdateResp_EnergyScan_Params_t* Params, szl_uint8 TransactionId)
{
  unsigned8 i;

  printApp(Service, "App Zdo: SZL Mgmt Nwk Update Rsp. SZL Sts = 0x%02X, TID = 0x%02X, ",
           Status,
           TransactionId);

  if (Params != NULL)
    {
      printApp(Service, "Sts = 0x%02X, Nwk 0x%04X, ChannelMask = 0x%04X, TotTrans = 0x%04X, TotFails = 0x%04X, ListCnt = 0x%02X, EnergyValues =",
                Params->Status,
                Params->NetworkAddress,
                Params->ScannedChannelsMask,
                Params->TotalTransmisisons,
                Params->TransmisisonFailures,
                Params->ScannedChannelListCount);
      for (i = 0; i < Params->ScannedChannelListCount; i++)
        {
          printApp(Service, " 0x%02X,", Params->EnergyValues[i]);
        }
      printApp(Service, "\n");
    }
  else
    {
      printApp(Service, "WARNING: Params = NULL\n");
    }
}

/******************************************************************************
 * SZL ZDO MGMT RTG Response Handler
 ******************************************************************************/
void appZdo_SzlMgmtRtgRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoMgmtRtgRespParams_t* Params, szl_uint8 TransactionId)
{
  unsigned8 i;

  printApp(Service, "App Zdo: SZL ZDO Mgmt Rtg Rsp. SZL Sts = 0x%02X, TID = 0x%02X, ",
           Status,
           TransactionId);

  if (Params != NULL)
    {

        printApp(Service, "Sts = 0x%02X, NwkAddr = 0x%04X, Entries = 0x%02X, StartInd = 0x%02X, ListCnt = 0x%02X:\n",
                  Params->Status,
                  Params->SourceNetworkAddress,
                  Params->RoutingTableEntries,
                  Params->StartIndex,
                  Params->RoutingTableListCount);
        for (i = 0; i < Params->RoutingTableListCount; i++)
          {
            printApp(Service, "\t\tItem 0x%02X: DestNwkAddr = 0x%04X, Sts = 0x%02X, NextHop = 0x%04X\n",
                       i,
                       Params->RoutingTableList[i].DestinationNetworkAddress,
                       Params->RoutingTableList[i].RtgStatus,
                       Params->RoutingTableList[i].NextHopNetworkAddress);
          }
    }
  else
    {
      printApp(Service, "WARNING: Params = NULL\n");
    }
}


/******************************************************************************
 * SZL ZDO MGMT Bind Response Handler
 ******************************************************************************/
void appZdo_SzlMgmtBindRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_ZdoMgmtBindRespParams_t* Params, szl_uint8 TransactionId)
{
  unsigned8 i;

  printApp(Service, "App Zdo: SZL ZDO Mgmt Bind Rsp. SZL Sts = 0x%02X, TID = 0x%02X, ",
           Status,
           TransactionId);

  if (Params != NULL)
    {
        printApp(Service, "Sts = 0x%02X, NwkAddr = 0x%04X, Entries = 0x%02X, StartInd = 0x%02X, ListCnt = 0x%02X:\n",
                  Params->Status,
                  Params->SourceNetworkAddress,
                  Params->BindingTableEntries,
                  Params->StartIndex,
                  Params->BindingTableListCount);
        for (i = 0; i < Params->BindingTableListCount; i++)
          {
            printApp(Service, "\t\tItem 0x%02X: SourceIeeeAddress = 0x%016llX, SourceEndpoint = 0x%02X, ClusterID = 0x%04X (%s), ",
                       i,
                       Params->BindingTableList[i].SourceIeeeAddress,
                       Params->BindingTableList[i].SourceEndpoint,
                       Params->BindingTableList[i].ClusterID, appZcl_GetClusterString(Params->BindingTableList[i].ClusterID));


            switch(Params->BindingTableList[i].DestinationAddressMode)
              {
                case SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_GROUP:
                  printApp(Service, "Mode = Group, DestGroup = 0x%04X\n", Params->BindingTableList[i].DestinationGroupAddress);
                  break;

                case SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_UNICAST:
                  printApp(Service, "Mode = Unicast, DestIeee = 0x%016llX, DestEp = 0x%02X\n", Params->BindingTableList[i].DestinationIeeeAddress, Params->BindingTableList[i].DestinationEndpoint);
                  break;

                case SZL_ZDO_BIND_DESTINATION_ADDRESS_MODE_UNKNOWN:
                  printApp(Service, "Mode = Unknown - ERROR!!!\n");
                  break;
              }
          }
    }
  else
    {
      printApp(Service, "WARNING: Params = NULL\n");
    }
}

/******************************************************************************
 * SZL Leave Response Handler
 ******************************************************************************/
void appZdo_SzlLeaveRspHandler(zabService* Service, SZL_STATUS_t Status, SZL_NwkLeaveRespParams_t* Params, szl_uint8 TransactionId)
{
  printApp(Service, "App Zdo: SZL Leave Rsp. SZL Sts = 0x%02X, TID = 0x%02X, Nwk Addr = 0x%04X, ZDO Sts = 0x%02X\n",
           Status,
           TransactionId,
           Params->DeviceNwkAddress,
           (szl_uint8)Params->Status);
}



/******************************************************************************
 * Network Leave Indication Handler
 ******************************************************************************/
void appZdo_NetworkLeaveIndicationHandler(zabService* Service, SZL_NwkLeaveNtfParams_t* Params)
{
  printApp(Service, "appZdo: Network Leave Indication Handler: NwkAddr = 0x%04X\n",
           Params->NwkAddress);
}
/******************************************************************************
 *                      ******************************
 *                 *****          END OF FILE         *****
 *                      ******************************
 ******************************************************************************/