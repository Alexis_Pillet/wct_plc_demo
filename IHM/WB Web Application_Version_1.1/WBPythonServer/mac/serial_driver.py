
"""
mac.serial_driver
~~~~~~~~~~~~
Schneider Electric - Wireless Connectivity Team

Module to manage the serial connection

"""

import codecs
import sys
import serial
import types
from threading import Lock, Thread
from datetime import datetime, timedelta
from serial.tools import hexlify_codec
from serial.tools.list_ports import comports
from apscheduler.schedulers.background import BackgroundScheduler
from logger import LoggerManager

from .mac_frame import Mac
from .mac_frame import check_crc, compute_crc

codecs.register(lambda c: hexlify_codec.getregentry() if c == 'hexlify' else None)
log = LoggerManager.get_logger(__name__)


def ask_for_port():
    """
    Show a list of ports and ask the user for a choice. To make selection
    easier on systems with long device names, also allow the input of an
    index.
    """
    sys.stderr.write('\n--- Available ports:\n')
    ports = []
    for n, (port, desc, hwid) in enumerate(sorted(comports()), 1):
        sys.stderr.write('--- {:2}: {:20} {!r}\n'.format(n, port, desc))
        ports.append(port)
    while True:
        port = input('--- Enter port index or full name: ')
        try:
            index = int(port) - 1
            if not 0 <= index < len(ports):
                sys.stderr.write('--- Invalid index!\n')
                continue
        except ValueError:
            pass
        else:
            port = ports[index]
        return port


class SerialDriver(object):
    """Get data from serial port to application and vice versa."""

    def __init__(self, serial_instance, queue_mac_process):
        """Initialization of the serial process"""
        self._serial_instance = serial_instance
        self._input_encoding = 'UTF-8'
        self._output_encoding = 'UTF-8'
        # Thread
        self._reader_alive = None
        self._reader = None
        self._rx_decoder = None
        self._tx_decoder = None
        self._tx_encoder = None
        # Read/Write management
        self._local_mac_sequence_number = 0
        self._external_mac_sequence_number = -1
        self._starter_frame = 0xFE
        self._queue = queue_mac_process
        # Send command/data management
        self._mutex = Lock()
        self._mutex_no_multiple_send = Lock()
        self._mac_scheduler = BackgroundScheduler()
        self._mac_frame_in_progress = []

    @property
    def serial_instance(self):
        return self._serial_instance

    @serial_instance.setter
    def serial_instance(self, serial_instance):
        self._serial_instance = serial_instance

    def _start_reader(self):
        """Start reader thread"""
        self._reader_alive = True
        self._reader = Thread(target=self._reader_loop, name='mac_rx', args=())
        self._reader.daemon = True
        self._reader.start()

    def _stop_reader(self):
        """Stop reader thread only, wait for clean exit of thread"""
        self._reader_alive = False

    def start(self):
        """start worker thread"""
        self._start_reader()
        self._mac_scheduler.start()

    def stop(self):
        """Stop receiver thread/scheduler"""
        self._stop_reader()
        self._reader.join()
        self._mac_scheduler.remove_all_jobs()
        self._mac_scheduler.shutdown()

    def close(self):
        self._serial_instance.close()

    def _reader_loop(self):
        """read serial port"""
        try:
            while self._reader_alive:

                # Read the start of frame
                start_of_frame = self._serial_instance.read(1)

                if len(start_of_frame) != 1:
                    continue

                if start_of_frame[0] != self._starter_frame:
                    continue

                # Read length
                length = self._serial_instance.read(1)

                if (len(length) != 1) & (length[0] <= 3):
                    continue

                # Acquire mutex => we received one data; we need to treat it before sending other data
                self._mutex.acquire()

                # Read payload from serial
                length_payload_expected = length[0] - 1
                data = self._serial_instance.read(length_payload_expected)

                if len(data) != length_payload_expected:
                    continue

                # Parse received frame
                result = self._parse_ack_frame(data)
                if result["is_ack"]:
                    # Check if the ack corresponding to a command/data frames sent
                    if self._mac_scheduler.get_job(str(result["mac_sequence_number"])) is None:
                        break
                    # Remove retry_jobs/metadata since we received the network ack
                    self._mac_scheduler.remove_job(str(result["mac_sequence_number"]))
                    self._mac_frame_in_progress.pop(0)
                    # Check if there are retry_jobs pending => if yes start it
                    if len(self._mac_frame_in_progress) >= 1:
                        self._mac_scheduler.resume_job(self._mac_frame_in_progress[0].id_job)
                        self._mac_scheduler.modify_job(self._mac_frame_in_progress[0].id_job,
                                                       next_run_time=(datetime.now() + timedelta(milliseconds=10)))
                    self._queue.put(data[:len(data) - 1])
                    self._mutex.release()
                else:
                    result = self._parse_frame(data)
                    if result["is_data_control_frame"]:
                        self._queue.put(data[:len(data) - 1])
                    self._send_ack(result["ack_status_to_send"])
                    if result["ack_status_to_send"] != Mac.AckStatus.SUCCESS:
                        log.error('ACK_SEND - {}'.format(Mac.display_ack()[result["ack_status_to_send"]]))
                    self._mutex.release()

        except serial.SerialException:
            self._reader_alive = False
            raise  # XXX handle instead of re-raise?

    def _parse_ack_frame(self, data):
        """Check is the frame is a mac ack

        Args:
            data: data of the frame

        Returns:
            A dict with the result of the control
            "is_ack": If the frame is a mac ack or not
            "mac_sequence_number": mac sequence number of the frame
            example:
                >>> {"is_ack": True, "mac_sequence_number": 0}
        """
        result = {"is_ack": False, "mac_sequence_number": -1}

        if not check_crc(data):
            return result

        if int(data[0]) != Mac.FrameControl.ACK:
            return result

        # Check if we are waiting an acknowledgment
        if len(self._mac_frame_in_progress) == 0:
            return result

        if int(data[1]) != self._mac_frame_in_progress[0].mac_sequence_number:
            return result

        # Check if Error = CRC
        # if int(data[2]) != MacAckStatus.ACK_BAD_CRC.value:
        # Good frame
        result["is_ack"] = True
        result["mac_sequence_number"] = int(data[1])
        return result

    def _parse_frame(self, data):
        """Check is the data is a data or control frame

        Args:
            data: data of the frame

        Returns:
            A dict with the result of the control
            "is_data_control_frame": If the frame is a control or data frame
            "ack_status_to_send": status of the ack to send back
            example:
                >>> {"is_data_control_frame": True, "ack_status_to_send": 0x00}
        """
        result = {"is_data_control_frame": False, "ack_status_to_send": Mac.AckStatus.ERROR_NO_ACK}

        if not check_crc(data):
            result["ack_status_to_send"] = Mac.AckStatus.BAD_CRC
            return result

        if (int(data[0]) != Mac.FrameControl.CONTROL) & (int(data[0]) != Mac.FrameControl.DATA):
            if int(data[1]) == Mac.FrameControl.ACK:
                result["ack_status_to_send"] = Mac.AckStatus.ERROR_NO_ACK
            else:
                result["ack_status_to_send"] = Mac.AckStatus.INVALID_COMMAND
            return result

        # Sequence number must be different of the previous one
        if int(data[1]) == self._external_mac_sequence_number:
            result["ack_status_to_send"] = Mac.AckStatus.BAD_FRAME_COUNTER
            return result

        # Good frame
        self._external_mac_sequence_number = int(data[1])
        result["is_data_control_frame"] = True
        result["ack_status_to_send"] = Mac.AckStatus.SUCCESS

        return result

    def _send_ack(self, result):
        """Send a mac ack

        Args:
            result: ack status to send
        """
        # In some cases we don't want to send a ack
        # Bad CRC & Received an undesired ack
        if (result != Mac.AckStatus.ERROR_NO_ACK) & (result != Mac.AckStatus.BAD_CRC):

            # length - ACK - mac_frame_counter - status - CRC
            length = 5
            frame = bytearray([self._starter_frame, length, Mac.FrameControl.ACK, self._external_mac_sequence_number])

            frame.append(result)

            crc = compute_crc(frame[2:], len(frame[2:]))
            frame.append(crc)

            self._serial_instance.write(frame)

    def write_data(self, command, payload):
        """Send mac frame to serial port

        Args:
            command: frame control field
            payload: payload of the frame
        """

        # Length + Command + mac_frame_counter + payload + CRC
        length = len(payload) + 4

        if self._local_mac_sequence_number == 255:
            self._local_mac_sequence_number = 0
        else:
            self._local_mac_sequence_number += 1

        frame = bytearray([self._starter_frame, length, command, self._local_mac_sequence_number])

        for element in payload:
            frame.append(element)

        crc = compute_crc(frame[2:], len(frame[2:]))
        frame.append(crc)

        # Save metadata of the frame to be able to manage the retry process of it
        metadata_frame = types.SimpleNamespace()
        metadata_frame.mac_sequence_number = self._local_mac_sequence_number
        metadata_frame.id_job = str(self._local_mac_sequence_number)
        metadata_frame.number_retry = 0
        self._mac_frame_in_progress.append(metadata_frame)

        self._mac_scheduler.add_job(self._writer, trigger='interval', args=(frame,), seconds=0.2,
                                    id=str(self._local_mac_sequence_number))
        self._mac_scheduler.pause_job(metadata_frame.id_job)

        # If there are no retry ob running => start it immediately
        if len(self._mac_frame_in_progress) == 1:
            self._mac_scheduler.resume_job(self._mac_frame_in_progress[0].id_job)
            self._mac_scheduler.modify_job(self._mac_frame_in_progress[0].id_job, next_run_time=datetime.now())

    def _writer(self, frame):
        """retry job function

        We are waiting 0.2 millisecond to receive an mac ack
        If you don't receive one, we retry to send the frame
        After 3 retry, we raise an error

        Args:
            frame: frame to send to serial port
        """

        # We don't send multiple frame at one
        self._mutex_no_multiple_send.acquire()

        try:

            if self._mac_frame_in_progress[0].number_retry < 3:

                self._mac_frame_in_progress[0].number_retry += 1
                self._mutex.acquire()
                self._serial_instance.write(frame)
                self._mutex.release()
            else:
                # Send error to applicative
                error = bytearray([Mac.FrameControl.ACK, 0x00, Mac.AckStatus.RETRY_EXPIRED])
                self._queue.put(error)
                # Stop the retry process for this frame
                self._mac_scheduler.remove_job(self._mac_frame_in_progress[0].id_job)
                self._mac_frame_in_progress.pop(0)
                # Check if there are retry_jobs pending => if yes start it
                if len(self._mac_frame_in_progress) >= 1:
                    self._mac_scheduler.resume_job(self._mac_frame_in_progress[0].id_job)
                    self._mac_scheduler.modify_job(self._mac_frame_in_progress[0].id_job,
                                                   next_run_time=(datetime.now() + timedelta(milliseconds=10)))
        except Exception:
            raise

        # We finish to send the current frame
        self._mutex_no_multiple_send.release()

    def set_rx_encoding(self, encoding, errors='replace'):
        """set encoding for received data"""
        self._input_encoding = encoding
        self._rx_decoder = codecs.getincrementaldecoder(encoding)(errors)

    def set_tx_encoding(self, encoding, errors='replace'):
        """set encoding for transmitted data"""
        self._output_encoding = encoding
        self._tx_encoder = codecs.getincrementalencoder(encoding)(errors)

    def change_encoding(self):
        """change encoding on the serial port"""
        sys.stderr.write('\n--- Enter new encoding name [{}]: '.format(self._input_encoding))
        new_encoding = sys.stdin.readline().strip()
        if new_encoding:
            try:
                codecs.lookup(new_encoding)
            except LookupError:
                sys.stderr.write('--- invalid encoding name: {}\n'.format(new_encoding))
            else:
                self.set_rx_encoding(new_encoding)
                self.set_tx_encoding(new_encoding)
            sys.stderr.write('--- serial input encoding: {}\n'.format(self._input_encoding))
            sys.stderr.write('--- serial output encoding: {}\n'.format(self._output_encoding))

    def change_port(self):
        """Have a conversation with the user to change the serial port"""
        try:
            port = ask_for_port()
        except KeyboardInterrupt:
            port = None
        if port and port != self._serial_instance.port:
            # reader thread needs to be shut down
            self._stop_reader()
            # save settings
            settings = self._serial_instance.getSettingsDict()
            new_serial = None
            try:
                new_serial = serial.serial_for_url(port, do_not_open=True)
                # restore settings and open
                new_serial.applySettingsDict(settings)
                new_serial.rts = self._serial_instance.rts
                new_serial.dtr = self._serial_instance.dtr
                new_serial.open()
                new_serial.break_condition = self._serial_instance.break_condition
            except Exception as e:
                sys.stderr.write('--- ERROR opening new port: {} ---\n'.format(e))
                new_serial.close()
            else:
                self._serial_instance.close()
                self._serial_instance = new_serial
                sys.stderr.write('--- Port changed to: {} ---\n'.format(self._serial_instance.port))
            # and restart the reader thread
            self._start_reader()
